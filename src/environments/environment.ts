// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    baseUrl: 'http://cxi.web.beesightsoft.com',
    baseCmsUrl: 'http://cms.cxi.web.beesightsoft.com',
    baseApiUrl: 'http://cxi.web.beesightsoft.com/api/v1',
    baseApiBookingUrl: 'http://api.cxi-booking-dev.web.beesightsoft.com/api/v1',

    pusher: {
        production: false,
        app_id: '859138',
        key: '4502959f71cf9e83573f',
        cluster: 'ap1',
        authEndpoint: 'http://cms.cxi.web.beesightsoft.com/broadcasting/auth',
    }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

