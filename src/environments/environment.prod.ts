export const environment = {
    production: true,
    baseUrl: 'http://cxi.web.beesightsoft.com',
    baseCmsUrl: 'http://cms.cxi.web.beesightsoft.com',
    baseApiUrl: 'http://cxi.web.beesightsoft.com/api/v1',
    baseApiBookingUrl: 'http://api.cxi-booking-dev.web.beesightsoft.com/api/v1',

    pusher: {
        production: true,
        app_id: '859138',
        key: '4502959f71cf9e83573f',
        cluster: 'ap1',
        authEndpoint: 'http://cms.cxi.web.beesightsoft.com/broadcasting/auth',
    }
};
