const helper = {
    showNotification(message, icon, type, time) {
        icon = icon == null ? 'info' : icon;
        type = type == null ? 'success' : type;
        $.notify({
            icon: icon,
            message: message
        }, {
            type: type,
            timer: time,
            delay: 500,
            newest_on_top: true,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutRight'
            },
        });
    },
    objToString(obj) {
        let str = '';
        let _str = "";
        for (var p in obj) {
            if (obj.hasOwnProperty(p)) {
                str += ":" + obj[p];
            }
        }
        _str = str.slice(1);
        return _str;
    },
    formatTime(item) {
        if (item.hour < 9 && item.minute < 9 && item.second < 9) {
            return item = {
                hour: "0" + item.hour,
                minute: "0" + item.minute,
                second: "0" + item.second,
            }
        }
        if (item.hour > 9 && item.minute > 9 && item.second < 9) {
            return item = {
                hour: JSON.stringify(item.hour),
                minute: JSON.stringify(item.minute),
                second: "0" + item.second,
            }
        }
        if (item.hour > 9 && item.minute < 9) {
            return item = {
                hour: JSON.stringify(item.hour),
                minute: "0" + item.minute,
                second: "0" + item.second,
            }
        }
        if (item.hour < 9 && item.minute > 9) {
            return item = {
                hour: "0" + item.hour,
                minute: JSON.stringify(item.minute),
                second: "0" + item.second,
            }
        }
    },
    incrementQuantity(product, increment = true, callback) {
        if (!product) { return; }
        if (!product.hasOwnProperty('quantity') || isNaN(product.quantity)) {
            product.quantity = 1;
        }
        if (increment) {
            product.quantity++;
        } else if (product.quantity > 1) {
            product.quantity--;
        }
        if (typeof callback === 'function') {
            try {
                callback()
            } catch (e) { }
        }
    },
    filterMoneys() {
        $(function () {

            var $form = $('#form');
            var $input = $form.find('.ng-money');

            $input.on('keyup', function (event) {
                // When user select text in the document, also abort.
                var selection = window.getSelection().toString();
                if (selection !== '') {
                    return;
                }
                // When the arrow keys are pressed, abort.
                if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
                    return;
                }

                var $this = $(this);

                // Get the value.
                var input = $this.val();

                var input = input.replace(/[\D\s\._\-]+/g, '');
                input = input ? parseInt(input, 10) : 0;

                $this.val(function () {
                    return (input === 0) ? '' : input.toLocaleString('en-US');
                });
            });

            $form.on('submit', function (event) {

                var $this = $(this);
                var arr = $this.serializeArray();

                for (var i = 0; i < arr.length; i++) {
                    arr[i].value = arr[i].value.replace(/[($)\s\._\-]+/g, ''); // Sanitize the values.
                };
                event.preventDefault();
            });

        });
    },
    percentInput() {
        $(function () {
            document.querySelector('.percent').addEventListener('input', function (e) {
                let int = e.target.value;
                int = Number(int);
                if (int > 100 || int.lenth >= 3) {
                    e.target.value = '';
                    int = 0
                } else if (!Number(int)) {
                    e.target.value = '';
                    int = 0;
                }
                else {
                    //e.target.value = int + '%';
                    // e.target.setSelectionRange(e.target.value.length - 1, e.target.value.length - 1);
                }
                //console.log(int)
            });
        })
    },
    formatPhone() {
        $(function () {
            $(".phone-number").text(function (i, text) {
                text = text.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "$1-$2-****");
                return text;
            });
        })
    },
    formatPrice(n, currency) {
        return n.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + ' ' + currency;
    },
    formatNumber(n) {
        //return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        return n.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }
}
