import {Component, OnInit} from '@angular/core';
import {Organization} from 'src/app/shared/entities/organization';
import {Router, ActivatedRoute} from '@angular/router';
import {OrganizationsRepository} from 'src/app/shared/repositories/organizations.repository';
import {filter, map, switchMap} from 'rxjs/operators';

@Component({
    selector: 'app-edit-organization',
    templateUrl: './edit-organization.component.html',
    styleUrls: ['./edit-organization.component.scss']
})
export class EditOrganizationComponent implements OnInit {
    organization: Organization = new Organization();
    scope = [{name: 'FB', value: 'fb'},
        {name: 'Booking', value: 'booking'},
    ];

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private organizationsRepository: OrganizationsRepository,
    ) {
    }

    ngOnInit() {
        this.getOrganizationDetail();
    }

    getOrganizationDetail() {
        this.route.params
            .pipe(
                filter(data => data.id),
                map(data => data.id),
                switchMap(
                    (id: number) => this.organizationsRepository.find(id)
                )
            )
            .subscribe(
                (organization: Organization) => {
                    this.organization = Organization.create(organization);
                }
            );
    }

    cancel() {
        this.router.navigate(['../..', 'list'], {relativeTo: this.route});
    }

    onSubmit() {
        this.organizationsRepository.save(this.organization)
            .subscribe(
                () => this.cancel()
            );
    }
}
