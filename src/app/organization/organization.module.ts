import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OrganizationRoutingModule} from './organization-routing.module';
import {CreateOrganizationComponent} from './create-organization/create-organization.component';
import {EditOrganizationComponent} from './edit-organization/edit-organization.component';
import {ListOrganizationComponent} from './list-organization/list-organization.component';

// Module custom
import {SharedModule} from 'src/app/shared/shared.module';
import {FormsModule} from '@angular/forms';
import {QRCodeModule} from 'angularx-qrcode';

@NgModule({
    declarations: [CreateOrganizationComponent, EditOrganizationComponent, ListOrganizationComponent],
    imports: [
        CommonModule,
        FormsModule,
        QRCodeModule,
        SharedModule,
        OrganizationRoutingModule,
    ]
})
export class OrganizationModule {
}
