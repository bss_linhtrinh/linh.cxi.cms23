import { Component, OnInit } from '@angular/core';
import { Organization } from 'src/app/shared/entities/organization';
import { Router, ActivatedRoute } from '@angular/router';
import { OrganizationsRepository } from 'src/app/shared/repositories/organizations.repository';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-create-organization',
    templateUrl: './create-organization.component.html',
    styleUrls: ['./create-organization.component.scss']
})
export class CreateOrganizationComponent implements OnInit {
    organization: Organization = new Organization();
    selectedFile: File = null;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private organizationsRepository: OrganizationsRepository,
    ) {
    }

    ngOnInit() {
    }
    cancel() {
        this.router.navigate(['..', 'list'], { relativeTo: this.route });
    }

    onSubmit() {
        this.organizationsRepository.save(this.organization)
        .subscribe(
            () => this.cancel()
        );
    }
}
