import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListOrganizationComponent} from './list-organization/list-organization.component';
import {EditOrganizationComponent} from './edit-organization/edit-organization.component';
import {CreateOrganizationComponent} from './create-organization/create-organization.component';
import {CheckPermissionsGuard} from '../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: 'list', component: ListOrganizationComponent, data: {breadcrumb: 'List organizations'}},
            {path: 'create', component: CreateOrganizationComponent, data: {breadcrumb: 'Create new organization'}},
            {path: ':id/edit', component: EditOrganizationComponent, data: {breadcrumb: 'Edit organization'}},
            {path: '', redirectTo: 'list', pathMatch: 'full'},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OrganizationRoutingModule {
}
