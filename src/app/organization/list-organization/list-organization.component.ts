import {Component, OnInit} from '@angular/core';
import {Organization} from '../../shared/entities/organization';
import {OrganizationsRepository} from '../../shared/repositories/organizations.repository';
import {Router, ActivatedRoute} from '@angular/router';
import {User} from 'src/app/shared/entities/user';
import {Filter} from 'src/app/shared/entities/filter';
import {CxiGridConfig} from '../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../shared/components/cxi-grid/cxi-grid-column';
import {LocalStorage} from 'ngx-webstorage';
import {Provider} from '../../shared/types/providers';

@Component({
    selector: 'app-list-organization',
    templateUrl: './list-organization.component.html',
    styleUrls: ['./list-organization.component.scss']
})
export class ListOrganizationComponent implements OnInit {

    // Grid
    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        className: false,
        pagingServer: true,
        repository: this.organizationsRepository,
        actions: {
            onEdit: (organization: Organization) => {
                this.onEdit(organization);
            },
            onActivate: (organization: Organization) => {
                this.onActivate(organization);
            },
            onDeactivate: (organization: Organization) => {
                this.onDeactivate(organization);
            },
            onDelete: (organization: Organization) => {
                this.onDelete(organization);
            },
        }
    });

    columns = CxiGridColumn.from([
        {title: 'Organization name', name: 'name'},
        {title: 'Taxcode', name: 'tax_code'},
        {title: 'Phone', name: 'phone'},
        {title: 'Max Brands', name: 'max_brands'}
    ]);

    @LocalStorage('auth.provider') provider: Provider;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private organizationsRepository: OrganizationsRepository,
    ) {
    }

    ngOnInit() {
        this.getOrganizations();
    }

    getOrganizations() {
        if (this.provider === 'super-admin') {
            this.cxiGridConfig.repository = this.organizationsRepository;
        }
    }

    onEdit(organization: Organization) {
        this.router.navigate(['..', organization.id, 'edit'], {relativeTo: this.route});
    }

    onActivate(organization: Organization) {
        this.organizationsRepository.activate(organization)
            .subscribe();
    }

    onDeactivate(organization: Organization) {
        this.organizationsRepository.deactivate(organization)
            .subscribe();
    }

    onDelete(organization: Organization) {
        this.organizationsRepository.destroy(organization)
            .subscribe();
    }

}
