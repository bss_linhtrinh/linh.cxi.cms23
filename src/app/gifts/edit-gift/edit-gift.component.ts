import { Component, OnInit } from '@angular/core';
import { Gift } from 'src/app/shared/entities/gift';
import { GiftRepository } from 'src/app/shared/repositories/gifts.repository';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, map, switchMap } from 'rxjs/operators';
import { GiftCategoriesRepository } from 'src/app/shared/repositories/gift-categories.repository';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';

@Component({
  selector: 'app-edit-gift',
  templateUrl: './edit-gift.component.html',
  styleUrls: ['./edit-gift.component.scss']
})
export class EditGiftComponent implements OnInit {
  public gift: Gift = new Gift();
  cate_gift: string = 'Please choose';
  selectedFile: File = null;
  gift_categories: any;
  userScope: UserScope;
  public brand = [];
  tab: string = 'en';
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private giftRepository: GiftRepository,
    private giftCategoriesRepository: GiftCategoriesRepository,
    private scopeService: ScopeService,
    private brandRepository: BrandsRepository,
  ) { }

  ngOnInit() {
    this.loadGift();
    this.loadListBrand();
  }
  onFileChanged(selectedFile: File) {
    this.selectedFile = selectedFile;
  }
  loadGift() {
    this.activatedRoute.params
      .pipe(
        filter(data => data.id),
        map(data => data.id),
        switchMap(
          (id) => this.giftRepository.find(id)
        )
      )
      .subscribe((res?: any) => {
        this.gift = res;
        this.gift.translations.forEach(item => {
          if (item.locale == 'en') {
            this.gift.en = item;
          } else if (item.locale == 'vi') {
            this.gift.vi = item;
          }
        });
        this.getGiftCategories();
      });
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.gift.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  loadListBrand() {
    this.brandRepository.all()
      .subscribe(
        (res) => {
          this.brand = res;
          this.getListUserScope();
        }
      );
  }
  changeGiftCate() {
    this.getGiftCategories();
  }
  getGiftCategories() {
    this.giftCategoriesRepository.all({ status: 1, brand_id: this.gift.brand_id }).subscribe(res => {
      this.gift_categories = res;
    })
  }
  onSubmit() {
    this.gift.translations = null;
    this.giftRepository.save(this.gift)
      .subscribe((res) => {
        this.cancel();
      });
  }
  cancel() {
    this.router.navigate(['../..', 'list'], { relativeTo: this.activatedRoute });
  }
}
