import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateGiftComponent } from './create-gift/create-gift.component';
import { ListGiftComponent } from './list-gift/list-gift.component';
import { EditGiftComponent } from './edit-gift/edit-gift.component';
import {CheckPermissionsGuard} from '../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            { path: 'create', component: CreateGiftComponent, data: { breadcrumb: 'Create new gift' } },
            { path: 'list', component: ListGiftComponent, data: { breadcrumb: 'List gifts' } },
            { path: ':id/edit', component: EditGiftComponent, data: { breadcrumb: 'Edit gift' } },
            { path: '', redirectTo: 'list', pathMatch: 'full' },
        ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GiftsRoutingModule { }
