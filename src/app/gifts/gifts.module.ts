import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GiftsRoutingModule} from './gifts-routing.module';
import {CreateGiftComponent} from './create-gift/create-gift.component';
import {EditGiftComponent} from './edit-gift/edit-gift.component';
import {ListGiftComponent} from './list-gift/list-gift.component';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';

@NgModule({
    declarations: [CreateGiftComponent, EditGiftComponent, ListGiftComponent],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        GiftsRoutingModule,
    ]
})
export class GiftsModule {
}
