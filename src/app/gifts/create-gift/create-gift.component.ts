import { Component, OnInit } from '@angular/core';
import { Gift } from 'src/app/shared/entities/gift';
import { GiftRepository } from 'src/app/shared/repositories/gifts.repository';
import { Router, ActivatedRoute } from '@angular/router';
import { GiftCategoriesRepository } from 'src/app/shared/repositories/gift-categories.repository';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
import { UserScope } from 'src/app/shared/entities/user-scope';

@Component({
  selector: 'app-create-gift',
  templateUrl: './create-gift.component.html',
  styleUrls: ['./create-gift.component.scss']
})
export class CreateGiftComponent implements OnInit {
  public gift: Gift = new Gift();
  selectedFile: File = null;
  gift_categories: any;
  userScope: UserScope;
  cate_gift: string = 'Please choose';
  tab: string = 'en';
  public brand = [];
  constructor(
    private giftRepository: GiftRepository,
    private router: Router,
    private routeractivated: ActivatedRoute,
    private giftCategoriesRepository: GiftCategoriesRepository,
    private scopeService: ScopeService,
    private brandRepository: BrandsRepository,
  ) { }

  ngOnInit() {
    this.loadListBrand();
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.gift.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  loadListBrand() {
    this.brandRepository.all()
      .subscribe(
        (res) => {
          this.brand = res;
          this.gift.brand_id = this.brand[0].id;
          this.getListUserScope();
          this.getGiftCategories();
        }
      );
  }
  changeGiftCate() {
    this.getGiftCategories();
  }
  getGiftCategories() {
    this.giftCategoriesRepository.all({ status: 1, brand_id: this.gift.brand_id }).subscribe(res => {
      this.gift_categories = res;
      (this.gift_categories.length > 0) ? this.gift.category_id = this.gift_categories[0].id : null;
    })
  }
  onFileChanged(selectedFile: File) {
    this.selectedFile = selectedFile;
  }
  onSubmit() {
    this.giftRepository.save(this.gift)
      .subscribe(res => {
        this.router.navigate(['../list'], { relativeTo: this.routeractivated });
      });
  }

}
