import { Component, OnInit } from '@angular/core';
import { Gift } from 'src/app/shared/entities/gift';
import { GiftRepository } from 'src/app/shared/repositories/gifts.repository';
import { Router, ActivatedRoute } from '@angular/router';
import { CxiGridConfig } from '../../shared/components/cxi-grid/cxi-grid-config';
import { CxiGridColumn } from '../../shared/components/cxi-grid/cxi-grid-column';
import {LocalStorage} from 'ngx-webstorage';

@Component({
    selector: 'app-list-gift',
    templateUrl: './list-gift.component.html',
    styleUrls: ['./list-gift.component.scss']
})
export class ListGiftComponent implements OnInit {

    public gifts: Gift[] = [];

    baseTableConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        className: false,
        selecting: true,
        pagingServer: true,
        repository: this.giftRepository,
        translatable: true,
        actions: {
            onEdit: (gift: Gift) => {
                return this.onEdit(gift);
            },
            onDelete: (gift: Gift) => {
                return this.onDelete(gift);
            },
            onActivate: (gift: Gift) => {
                this.onActivate(gift);
            },
            onDeactivate: (gift: Gift) => {
                this.onDeactivate(gift);
            },
        }
    });
    baseTableColumns = CxiGridColumn.from([
        { title: 'name', name: 'name', avatar: 'images' },
        { title: 'description', name: 'description', },
    ]);

    @LocalStorage('scope.organization_id') organizationId: number;
    @LocalStorage('scope.brand_id') brandId: number;
    @LocalStorage('scope.outlet_id') outletId: number;

    constructor(
        private giftRepository: GiftRepository,
        private router: Router,
        public activatedRoute: ActivatedRoute,
    ) {
    }

    ngOnInit() {
    }

    onEdit(gift: Gift) {
        this.router.navigate(['..', gift.id, 'edit'], { relativeTo: this.activatedRoute });
    }

    onDelete(gift: Gift) {
        this.giftRepository.destroy(gift).subscribe();
    }
    onActivate(gift: Gift) {
        this.giftRepository.activate(gift).subscribe();
    }
    onDeactivate(gift: Gift) {
        this.giftRepository.deactivate(gift).subscribe();
    }

}
