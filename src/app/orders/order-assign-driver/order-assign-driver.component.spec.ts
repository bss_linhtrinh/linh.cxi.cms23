import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderAssignDriverComponent } from './order-assign-driver.component';

describe('OrderAssignDriverComponent', () => {
  let component: OrderAssignDriverComponent;
  let fixture: ComponentFixture<OrderAssignDriverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderAssignDriverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderAssignDriverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
