import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {tap} from 'rxjs/operators';
import {OrderDriver} from '../../shared/entities/order-driver';
import {OrderDelivery} from '../../shared/entities/order-delivery';
import {DeliversRepository} from '../../shared/repositories/delivers.repository';
import {MessagesService} from '../../messages/messages.service';
import {Deliver} from '../../shared/entities/deliver';

@Component({
    selector: 'app-order-assign-driver',
    templateUrl: './order-assign-driver.component.html',
    styleUrls: ['./order-assign-driver.component.scss']
})
export class OrderAssignDriverComponent implements OnInit, OnChanges {

    @Input() name: string;
    @Input() orderStatus: string;
    @Input() brandId: number;
    @Input() outletId: number;

    @Input() orderDriver: OrderDriver;
    @Output() orderDriverChange = new EventEmitter<OrderDriver>();

    @Input() delivery: OrderDelivery;

    // delivers: Deliver[] = [];

    constructor(
        private deliversRepository: DeliversRepository,
        private messagesService: MessagesService
    ) {
    }

    ngOnInit() {
        this.getData();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (
            (changes.brandId && changes.brandId.currentValue !== changes.brandId.previousValue) ||
            (changes.outletId && changes.outletId.currentValue !== changes.outletId.previousValue)
        ) {
            this.getData();
        }
    }

    getData() {
    }

    addDriver() {
        const columns = [
            {title: 'name', name: 'name', avatar: 'avatar'},
            {title: 'phone', name: 'phone'},
            {title: 'email', name: 'email'},
            {title: 'outlet', name: 'outlet_address'},
            {
                title: '',
                name: '',
                action: {
                    label: 'Select',
                }
            },
        ];
        const options = {
            columns: columns,
            repository: this.deliversRepository
        };
        this.messagesService.selectItemFromList(options)
            .pipe(
                tap(
                    (deliver: Deliver) => this.handleSelectedDeliver(deliver)
                )
            )
            .subscribe(
            );
    }

    handleSelectedDeliver(deliver: Deliver) {
        if (deliver) {
            this.orderDriver.id = deliver.id;
            this.orderDriver.name = deliver.name;
            this.orderDriver.phone = deliver.phone;
            this.orderDriver.email = deliver.email;
            this.orderDriver.outlet = deliver.outlet_address;

            this.orderDriverChange.emit(this.orderDriver);
        }
    }

    isDisabledAddDriver() {
        return (
            this.orderStatus !== 'pending' &&
            this.orderStatus !== 'confirmed' &&
            this.orderStatus !== 'food_ready'
        ) || (
            this.delivery && this.delivery.driver_confirmed
        );
    }
}
