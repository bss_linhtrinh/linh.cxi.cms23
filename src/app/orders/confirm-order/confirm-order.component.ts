import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter, } from '@angular/core';
import { OutletsRepository } from 'src/app/shared/repositories/outlets.repository';
import { OrdersRepository } from 'src/app/shared/repositories/orders.repository';
declare var jQuery: any;

@Component({
    selector: 'app-confirm-order',
    templateUrl: './confirm-order.component.html',
    styleUrls: ['./confirm-order.component.scss']
})
export class ConfirmOrderComponent implements OnInit, OnChanges {

    confirmOrder = {
        outlet_id: null
    };
    @Input() order: any;
    outlets: any;
    @Output() messageConfirm = new EventEmitter();

    constructor(
        private ordersRepository: OrdersRepository,
        private outletsRepository: OutletsRepository,
    ) {
    }

    ngOnInit() {
    }
    ngOnChanges(simpleChanges: SimpleChanges) {
        if (simpleChanges.order) {
            this.getOutlets();
        }
    }

    getOutlets() {
        if (this.order !== undefined) {
            this.outletsRepository.getOutletsByBrand(this.order.brand.id)
                .subscribe((res?: any) => {
                    this.outlets = res;
                    this.confirmOrder.outlet_id = this.outlets[0].id;
                })
        }
    }

    yes() {
        const data = {
            outlet_id: this.confirmOrder.outlet_id
        };
        this.ordersRepository.changeStatusOrders(this.order, data)
            .subscribe(res => {
                this.toTop();
                this.confirmOrder.outlet_id = this.outlets[0].id;
                this.messageConfirm.emit(this.order);
            });
    }

    toTop() {
        (function ($) {
            $('#confirm-order').modal('hide');
            $('#scroll-board').animate({
                scrollTop: 0
            }, 900);
            return false;
        })(jQuery);
    }
}
