import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {of} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {Category} from '../../../shared/entities/category';
import {Product} from '../../../shared/entities/product';
import {CategoriesRepository} from '../../../shared/repositories/categories.repository';

@Component({
    selector: 'app-select-product',
    templateUrl: './select-product.component.html',
    styleUrls: ['./select-product.component.scss']
})
export class SelectProductComponent implements OnInit {

    category_id: number;

    @Input() categories: Category[];
    @Output() product = new EventEmitter<Product>();

    subCategories: Category[] = [];
    products: Product[] = [];

    level = 1;

    constructor(
        private categoriesRepository: CategoriesRepository
    ) {
    }

    ngOnInit() {
        this.getCategories();
    }

    getCategories() {
        if (this.categories &&
            this.categories.length > 0) {
            this.category_id = this.categories[0].id;
        }
    }

    getLabel() {
        const pageName = `Order`;
        if (this.level === 1) {
            return `${pageName}.Select category`;
        } else if (this.level === 2) {
            return `${pageName}.Select sub-category`;
        } else if (this.level === 3) {
            return `${pageName}.Select product`;
        }
    }

    up() {
        if (this.level > 1) {
            this.level -= 1;
        }

        if (this.level === 2 && (!this.subCategories || this.subCategories.length === 0)) {
            this.level -= 1;
        }
    }

    onSelect(item: any) {
        if (item instanceof Category) {
            if (item.children && item.children.length > 0) {
                // get sub-category
                of(item.children)
                    .subscribe(
                        (subCategories: Category[]) => {
                            if (subCategories &&
                                subCategories.length > 0) {
                                this.subCategories = subCategories;
                                this.level = 2;
                            }
                        }
                    );
            } else {
                // get products by category
                this.categoriesRepository.getProductsByCategory(item.id)
                    .subscribe(
                        (products: Product[]) => {
                            if (products &&
                                products.length > 0) {
                                this.products = products;
                                this.level = 3;
                            }
                        }
                    );
            }
        } else if (item instanceof Product) {
            this.product.emit(item);
        }

    }
}
