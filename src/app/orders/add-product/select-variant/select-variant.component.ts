import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {switchMap} from 'rxjs/operators';
import {of} from 'rxjs';
import {Product} from '../../../shared/entities/product';
import {OrderProduct} from '../../../shared/entities/order-product';
import {ProductVariant} from '../../../shared/entities/product-variant';
import {Attribute} from '../../../shared/entities/attribute';
import {AddOn} from '../../../shared/entities/add-on';
import {AddOnType} from '../../../shared/entities/add-on-type';
import {AttributesRepository} from '../../../shared/repositories/attributes.repository';
import {ProductAddonRepository} from '../../../shared/repositories/product-addon';
import {ProductAddonTypeRepository} from '../../../shared/repositories/product-addon-type';
import {ProductType} from '../../../shared/types/product-type';
import {OrderProductAddOn} from '../../../shared/entities/order-product-add-on';
import {ProductAddOnType} from '../../../shared/entities/product-add-on-type';
import {ProductAddOnTypeAddOn} from '../../../shared/entities/product-add-on-type-add-on';

@Component({
    selector: 'app-select-variant',
    templateUrl: './select-variant.component.html',
    styleUrls: ['./select-variant.component.scss']
})
export class SelectVariantComponent implements OnInit {
    @Input() product: Product;
    @Input() orderProduct: OrderProduct;
    @Input() variant: ProductVariant;

    @Output() orderProductChange = new EventEmitter<OrderProduct>();
    @Output() variantChange = new EventEmitter<ProductVariant>();
    @Output() add = new EventEmitter<OrderProduct>();

    selections = {};
    currentOptionId: number;
    currentSuperAttributeId: number;

    superAttributes: Attribute[] = [];
    availableVariants: ProductVariant[] = [];

    // add-ons
    addOns: AddOn[] = [];
    addOnTypes: AddOnType[] = [];
    isSelectAddOn = false;

    constructor(
        private attributesRepository: AttributesRepository,
        private addOnsRepository: ProductAddonRepository,
        private addOnTypesRepository: ProductAddonTypeRepository
    ) {
    }

    ngOnInit() {
        this.getSuperAttributes();
        this.getAddOns();
        this.getAddOnTypes();
    }

    getSuperAttributes() {
        this.attributesRepository.all()
            .pipe(
                switchMap(
                    (attributes: Attribute[]) => {
                        if (attributes &&
                            attributes.length > 0) {

                            const superAttributes = [];
                            this.product.super_attributes.forEach(
                                (attributeId: number) => {
                                    const superAttribute = attributes.find(attr => attr.id === attributeId);
                                    if (superAttribute) {
                                        superAttributes.push(superAttribute);
                                    }
                                }
                            );

                            return of(superAttributes);
                        }
                    }
                )
            )
            .subscribe(
                (superAttributes: Attribute[]) => {
                    this.superAttributes = superAttributes;

                    // init
                    if (this.selections) {
                        this.superAttributes.forEach(
                            (superAttribute: Attribute) => {
                                if (typeof this.selections[superAttribute.id] === 'undefined') {
                                    this.selections[superAttribute.id] = null;
                                }
                            }
                        );
                    }

                    this.checkVariants();
                }
            );
    }

    onChangeOption(optionId: number, superAttributeId: number) {
        if (optionId) {
            this.currentOptionId = optionId;
            this.currentSuperAttributeId = superAttributeId;
        } else {
            this.currentOptionId = null;
            this.currentSuperAttributeId = null;
        }

        this.checkVariants();
    }

    checkVariants() {
        if (!this.superAttributes || this.superAttributes.length === 0) {
            return;
        }

        const countSelectedSuperAttribute = this.superAttributes
            .filter(
                (superAttribute: Attribute) => {
                    return this.selections[superAttribute.id] !== null;
                }
            )
            .length;


        // Filter available variants
        if (countSelectedSuperAttribute > 0) {
            this.availableVariants = this.product.variants.filter(
                (variant) => {
                    let isMatchSelection = true;

                    for (const key in this.selections) {
                        const val = this.selections[key];

                        if (val) {
                            isMatchSelection = isMatchSelection && (
                                variant.attributes.findIndex(
                                    attribute => {
                                        let existsAttr = false;
                                        existsAttr = existsAttr || (attribute.id === parseInt(key, 10) && attribute.value === val);
                                        return existsAttr;
                                    }
                                ) !== -1
                            );
                        }
                    }

                    return isMatchSelection;
                }
            );
        } else {
            this.availableVariants = this.product.variants;
        }


        if (this.availableVariants.length === 1 &&
            countSelectedSuperAttribute === this.superAttributes.length) {

            // If available variant have 1 items,
            // stop
            this.variant = this.availableVariants[0];
            this.variantChange.emit(this.variant);

            // assign child.
            this.orderProduct.child = OrderProduct.create({
                product_id: this.variant.id,
                name: this.variant.name,
                qty_ordered: this.orderProduct.qty_ordered,
                price: this.variant.price,
                total: (this.orderProduct.qty_ordered * this.variant.price),
            });


            //
            this.getProductAddOnTypeOptions();

            // calculate total
            this.calculateTotalPriceOrderProduct();

        } else {

            // If available variant more than 1 items,
            // stop
            this.variant = null;
            this.variantChange.emit(this.variant);
        }


        // superAttributes which not selected
        const willCheckAttributes = this.superAttributes
            .filter(
                (superAttribute: Attribute) => {

                    // return this.selections[superAttribute.id] === null; // filter superAttributes haven't selected


                    // filter superAttributes is not current.
                    if (this.currentSuperAttributeId) {
                        return superAttribute.id !== this.currentSuperAttributeId;
                    } else {
                        return this.selections[superAttribute.id] === null;
                    }
                }
            );

        willCheckAttributes.forEach(
            (superAttribute: Attribute) => {
                superAttribute.options.forEach(
                    (option: any) => {
                        // option.id

                        const existVariant = this.availableVariants.findIndex(
                            (variant) => {
                                return variant.attributes.findIndex(
                                    attribute => {
                                        return attribute.id === superAttribute.id && attribute.value === option.id;
                                    }
                                ) !== -1;
                            }
                        ) !== -1;

                        option.disabled = !existVariant;
                    }
                );
            }
        );
    }

    getAddOns() {
        this.addOnsRepository.all()
            .subscribe(
                (addOns: AddOn[]) => {
                    this.addOns = addOns;
                }
            );
    }

    getAddOnTypes() {
        this.addOnTypesRepository.all()
            .subscribe(
                (addOnTypes: AddOnType[]) => {
                    this.addOnTypes = addOnTypes;
                }
            );
    }

    // ADD-ONS

    addAddOn() {
        this.isSelectAddOn = true;
        this.variant.addon_types.forEach(
            addOnType => {
                if (addOnType.single_add_on_id) {
                    addOnType.single_add_on_id = null;
                }

                addOnType.addons.forEach(
                    (addOn) => {
                        addOn.selected = false;
                        addOn.quantity = 1;
                    }
                );
            }
        );
    }

    backToSelectVariant() {
        this.isSelectAddOn = false;
    }

    getProductName() {
        if (this.variant && this.variant.id) {
            return this.variant.name;
        } else {
            return this.product.name;
        }
    }

    getNameOfProductAddOnType(productAddOnType: ProductAddOnType) {
        const prAddOnType = this.addOnTypes.find(addOnType => addOnType.id === productAddOnType.id);
        if (prAddOnType) {
            return prAddOnType.name;
        }
        return null;
    }

    getNameOfProductAddOn(productAddOn: ProductAddOnTypeAddOn) {
        const prAddOn = this.addOns.find(addOn => addOn.id === productAddOn.id);
        if (prAddOn) {
            return prAddOn.name;
        }
        return null;
    }

    getDisplayTypeOfProductAddOnType(productAddOnType: ProductAddOnType) {
        const prAddOnType = this.addOnTypes.find(addOnType => addOnType.id === productAddOnType.id);
        if (prAddOnType) {
            return prAddOnType.display_type;
        }
        return null;
    }

    getNameOfOrderProductAddOn(orderProductAddOn: OrderProductAddOn) {
        const opAddOn = this.addOns.find(addOn => addOn.id === orderProductAddOn.addon_id);
        if (opAddOn) {
            return opAddOn.name;
        }
        return null;
    }

    getProductAddOnTypeOptions() {
        if (this.variant &&
            this.variant.addon_types &&
            this.variant.addon_types.length > 0) {

            this.variant.addon_types.forEach(
                (productAddOnType: ProductAddOnType) => {
                    if (productAddOnType &&
                        productAddOnType.addons &&
                        productAddOnType.addons.length > 0) {
                        productAddOnType.single_options = productAddOnType.addons.map(
                            addOn => {
                                return {
                                    id: addOn.id,
                                    name: this.getNameOfProductAddOn(addOn)
                                };
                            }
                        );
                    } else {
                        productAddOnType.single_options = [];
                    }
                }
            );

        }
    }

    getPriceProductOrVariant() {
        if (this.product.type === ProductType.Configurable &&
            this.product.variants &&
            this.product.variants.length > 0) {
            if (this.variant) {
                return this.orderProduct.child.price;
            } else {
                return 0;
            }
        } else if (this.product.type === 'simple') {
            return this.orderProduct.price;
        } else if (this.product.type === 'combo') {
            // later
        } else {
            return 0;
        }
    }

    getTotalProductOrVariant() {
        if (this.product.type === ProductType.Configurable &&
            this.product.variants &&
            this.product.variants.length > 0) {
            if (this.variant) {
                return this.orderProduct.child.total;
            } else {
                return 0;
            }
        } else if (this.product.type === 'simple') {
            return this.orderProduct.total;
        } else if (this.product.type === 'combo') {
            // later
        } else {
            return 0;
        }
    }

    calculatePriceAddOn(productAddOn: ProductAddOnTypeAddOn) {
        const selected = productAddOn.selected ? 1 : 0;
        const qty = (typeof productAddOn.quantity !== 'undefined' && productAddOn.quantity)
            ? productAddOn.quantity
            : 1;
        const price = productAddOn.price;

        let total = 0;
        total += selected * (price * qty);

        return total;
    }

    calculatePriceAddOnTypeSingle(productAddOnType: ProductAddOnType) {
        if (!productAddOnType.single_add_on_id) {
            return 0;
        }

        const addOn = productAddOnType.addons.find(x => x.id === productAddOnType.single_add_on_id);
        if (!addOn || !addOn.price) {
            return 0;
        }

        return addOn.price;
    }

    doneAddAddOn() {
        if (!this.orderProduct.addons) {
            this.orderProduct.addons = [];
        }

        // transform
        this.variant.addon_types.forEach(
            (addOnType) => {

                const displayType = this.getDisplayTypeOfProductAddOnType(addOnType);

                switch (displayType) {
                    case 'multiple':
                        addOnType.addons.forEach(
                            (addOn) => {
                                if (addOn && addOn.selected) {
                                    this.orderProduct.addons.push(OrderProductAddOn.create({
                                        addon_id: addOn.id,
                                        name: addOn.name,
                                        qty_ordered: addOn.quantity,
                                        price: addOn.price,
                                        total: this.calculatePriceAddOn(addOn),
                                    }));
                                }
                            }
                        );
                        break;
                    case 'true-false':
                        addOnType.addons.forEach(
                            (addOn) => {
                                if (addOn && addOn.selected) {
                                    this.orderProduct.addons.push(OrderProductAddOn.create({
                                        addon_id: addOn.id,
                                        name: addOn.name,
                                        qty_ordered: 1,
                                        price: addOn.price,
                                        total: this.calculatePriceAddOn(addOn),
                                    }));
                                }
                            }
                        );
                        break;
                    case 'single':
                        const addOn = this.addOns.find(x => x.id === addOnType.single_add_on_id);
                        this.orderProduct.addons.push(OrderProductAddOn.create({
                            addon_id: addOnType.single_add_on_id,
                            name: addOn ? addOn.name : null,
                            qty_ordered: 1,
                            price: this.calculatePriceAddOnTypeSingle(addOnType),
                            total: this.calculatePriceAddOnTypeSingle(addOnType),
                        }));
                        break;
                }
            }
        );

        // back to select variant
        this.isSelectAddOn = false;
        this.calculateTotalPriceOrderProduct();
    }

    calculatePriceOrderProductAddOns() {
        if (!this.orderProduct ||
            !this.orderProduct.addons ||
            !this.orderProduct.addons.length) {
            return 0;
        }

        let totalPrice = 0;

        this.orderProduct.addons.forEach(
            (orderAddOn: OrderProductAddOn) => {
                if (orderAddOn && orderAddOn.total) {
                    totalPrice += orderAddOn.total;
                }
            }
        );

        return totalPrice;
    }

    removeAddOn(orderProductAddOn: OrderProductAddOn) {
        const indexOrderAddOn = this.orderProduct.addons.findIndex(x => x.addon_id === orderProductAddOn.addon_id);
        if (indexOrderAddOn !== -1) {
            this.orderProduct.addons.splice(indexOrderAddOn, 1);

            this.calculateTotalPriceOrderProduct();
        }
    }

    calculateTotalPriceOrderProduct() {
        let totalPrice = 0;

        const price = this.getPriceProductOrVariant();
        const qty_ordered = this.orderProduct.qty_ordered;
        const totalPriceAddOns = this.calculatePriceOrderProductAddOns();

        totalPrice += qty_ordered * (price + totalPriceAddOns);

        if (this.variant) {
            this.orderProduct.child.total = totalPrice;
        } else {
            this.orderProduct.total = totalPrice;
        }

        return totalPrice;
    }

    onChangeQuantity(event) {
        this.calculateTotalPriceOrderProduct();
    }

    doneAddOrderProduct() {
        this.orderProductChange.emit(this.orderProduct);
        this.add.emit(this.orderProduct);
    }

    disableButtonAddOrderProduct() {
        let isDisabled: boolean;

        switch (this.product.type) {
            case ProductType.Configurable:
            default:
                isDisabled = !this.variant ||
                    (
                        !this.orderProduct ||
                        !this.orderProduct.child ||
                        !this.orderProduct.child.product_id ||
                        this.orderProduct.child.total === null
                    );
                break;
            case ProductType.Simple:
                isDisabled = (
                    !this.orderProduct ||
                    !this.orderProduct.product_id ||
                    this.orderProduct.total === null
                );
                break;
        }

        return isDisabled;
    }
}
