import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Category} from '../../shared/entities/category';
import {OrderProduct} from '../../shared/entities/order-product';
import {Product} from '../../shared/entities/product';
import {ProductVariant} from '../../shared/entities/product-variant';

@Component({
    selector: 'app-add-product',
    templateUrl: './add-product.component.html',
    styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
    orderProduct: OrderProduct = new OrderProduct();

    @Input() categories: Category[];

    selectedProduct: Product;
    selectedVariant: ProductVariant;

    constructor(private ngbActiveModal: NgbActiveModal) {
    }

    ngOnInit() {
    }

    cancel() {
    }

    create() {
    }

    onSelectProduct(product: Product) {
        this.selectedProduct = product;

        // product_type
        if (this.selectedProduct) {
            this.orderProduct.product_id = this.selectedProduct.id;
            this.orderProduct.name = this.selectedProduct.name;
            this.orderProduct.qty_ordered = 1;
            this.orderProduct.price = this.selectedProduct.price;
            this.orderProduct.total = this.selectedProduct.price;
        }
    }

    onAddVariant(orderProduct: OrderProduct) {
        if (orderProduct) {
            this.ngbActiveModal.close(orderProduct);
        }
    }
}
