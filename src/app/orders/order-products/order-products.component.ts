import {
    AfterViewChecked,
    Component,
    EventEmitter,
    Inject,
    Input,
    OnChanges,
    OnInit,
    Optional,
    Output,
    SimpleChanges,
    ViewChild
} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {AddProductComponent} from '../add-product/add-product.component';
import {from, timer} from 'rxjs';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {tap} from 'rxjs/operators';
import {OrderProduct} from '../../shared/entities/order-product';
import {ElementBase} from '../../shared/custom-form/element-base/element-base';
import {Category} from '../../shared/entities/category';
import {Order} from '../../shared/entities/order';

@Component({
    selector: 'app-order-products',
    templateUrl: './order-products.component.html',
    styleUrls: ['./order-products.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: OrderProductsComponent, multi: true}
    ]
})
export class OrderProductsComponent extends ElementBase<OrderProduct[]> implements OnInit, OnChanges, AfterViewChecked {
    @Input() name: string;
    @Input() categories: Category[];
    @Input() order: Order;

    @Output() orderChange = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    productInfoSize: string;

    reverse = false;
    columnName = 'name';

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private ngbModal: NgbModal
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
        this.initSize();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.order) {
            this.initSize();
        }
    }

    ngAfterViewChecked(): void {
        this.initSize();
    }

    initSize() {
        let sizeComponent: string;
        if (this.value &&
            this.value.length > 0) {
            sizeComponent = 'xl';
        } else {
            sizeComponent = 'md';
        }
        timer(1)
            .pipe(tap(() => this.productInfoSize = sizeComponent))
            .subscribe();
    }

    addProduct() {
        const modalRef = this.ngbModal.open(AddProductComponent, {windowClass: 'orders-add-product'});
        modalRef.componentInstance.categories = this.categories;
        from(modalRef.result)
            .subscribe(
                (orderProduct: OrderProduct) => {

                    if (orderProduct) {
                        if (!this.value) {
                            this.value = [];
                        }


                        // product or variants
                        if (orderProduct.child && orderProduct.child.id) {
                            this.value.push(orderProduct.child);
                        } else {
                            this.value.push(orderProduct);
                        }


                        // check duplicate


                        // calculate sub_total
                        this.calculateTotalOrder();
                    }


                    this.initSize();

                    this.orderChange.emit(this.order);
                }
            );
    }

    // ======== Get value ==========
    getValue(orderProduct: OrderProduct, prop: string) {
        let currentOrderProduct: OrderProduct;

        if (
            orderProduct.child &&
            orderProduct.child.id &&
            prop !== 'addons'
        ) {
            currentOrderProduct = orderProduct.child;
        } else {
            currentOrderProduct = orderProduct;
        }

        if (prop && currentOrderProduct[prop]) {
            return currentOrderProduct[prop];
        }
    }


    // ======== Custom Table Order - products

    orderBy(columnName: string) {
        if (this.columnName === columnName) {
            this.reverse = !this.reverse;
        } else {
            this.columnName = columnName;
            this.reverse = false;
        }
    }

    removeOrderProduct(orderProduct: OrderProduct) {
        const index = this.value.findIndex(item => item.id === orderProduct.id);
        if (index !== -1) {
            this.value.splice(index, 1);
        }

        // calculate sub_total
        this.calculateTotalOrder();
    }

    // ======== Calculate ====

    changePriceOfProduct(orderProduct: OrderProduct) {
        this.calculatePriceOfProduct(orderProduct);
    }

    calculatePriceOfProduct(orderProduct: OrderProduct) {
        orderProduct.total = orderProduct.price * orderProduct.qty_ordered;

        this.calculateTotalOrder();
    }

    calculateTotalOrder() {
        // calculate sub_total
        let sub_total = 0;
        this.value
            .filter(x => x.total)
            .forEach(x => {
                if (x.total) {
                    sub_total += x.total;
                }
            });

        this.order.sub_total = sub_total;
        this.order.discount_amount = 0;
        this.order.total_item_count = (this.order && this.value) ? this.value.length : 0;
        this.order.total_qty_ordered = (this.order && this.value) ? this.value.length : 0;
    }

    existItemHasAddons() {
        let exists = false;
        if (this.value) {
            this.value.forEach(
                (orderProduct: OrderProduct) => {
                    if (
                        orderProduct &&
                        orderProduct.addons &&
                        orderProduct.addons.length > 0
                    ) {
                        exists = true;
                    }
                }
            );
        }
        return exists;
    }
}
