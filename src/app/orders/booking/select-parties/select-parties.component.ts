import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {CustomerBooking} from '../../../shared/entities/customer-booking';

@Component({
    selector: 'app-select-parties',
    templateUrl: './select-parties.component.html',
    styleUrls: ['./select-parties.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectPartiesComponent, multi: true}
    ]
})
export class SelectPartiesComponent extends ElementBase<CustomerBooking[]> implements OnInit {

    private _name: string;
    @Input() highlight: any[];
    private _label: string;

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        this._label = value.replace('_', ' ');
    }

    @Input() public size = 'sm';
    @Input() public width = '';
    @Input() public action = {};
    @Input() public listCountry = [];

    @Output() addGroup = new EventEmitter();
    @Output() removeGroup = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }


    // add-on types

    addAddOnType() {
        this.value.push(new CustomerBooking());
    }

    onRemove(index: number) {
        this.value.splice(index, 1);

        // check empty
        this.checkEmpty();
    }

    onChangeVariantAddOnType() {
    }

    checkEmpty() {
        if (!this.value || this.value.length === 0) {
            this.addAddOnType();
        }
    }
    isDisabledAddNew() {
        return this.highlight && this.value && this.highlight.length === this.value.length;
    }

}
