import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../shared/custom-form/element-base/element-base';
import {OrderPromotion} from '../../shared/entities/order-promotion';
import {Promotion} from '../../shared/entities/promotion';

@Component({
    selector: 'app-order-promotions',
    templateUrl: './order-promotions.component.html',
    styleUrls: ['./order-promotions.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: OrderPromotionsComponent, multi: true}
    ]
})
export class OrderPromotionsComponent extends ElementBase<OrderPromotion> implements OnInit {

    @Input() name: string;
    @Input() promotions: Promotion[] = [];
    @Output() change = new EventEmitter<OrderPromotion>();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    selectedPromotion: Promotion;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onChangePromotion(promotionId: number) {
        const promotion = this.promotions.find(p => p.id === promotionId);
        if (promotion) {
            this.selectedPromotion = promotion;

            this.value.code = promotion.code;
            this.value.name = promotion.name;
            this.value.discount_amount = promotion.discount_amount;
            this.value.maximum_discount = promotion.maximum_discount;

            this.change.emit(this.value);
        }
    }

    isDisabledDiscount() {
        return (this.selectedPromotion && this.selectedPromotion.discount_type === 'free_gift');
    }
}
