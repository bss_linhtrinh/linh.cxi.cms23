import {Component, OnInit, Input} from '@angular/core';
import {OrdersRepository} from 'src/app/shared/repositories/orders.repository';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Order} from 'src/app/shared/entities/order';

declare var jQuery: any;

@Component({
    selector: 'app-change-status',
    templateUrl: './change-status.component.html',
    styleUrls: ['./change-status.component.scss']
})
export class ChangeStatusComponent implements OnInit {
    @Input() order: Order;
    public confirmed_change_status = ['pending', 'delivered', 'ordered', 'food_ready', 'on_the_way', 'completed'];
    public processing_change_status = ['pending', 'confirmed', 'completed'];
    public completed_change_status = ['pending', 'confirmed', 'delivered', 'ordered', 'food_ready', 'on_the_way'];
    public cancel_change_status = ['pending', 'confirmed', 'completed', 'delivered', 'ordered', 'food_ready', 'on_the_way'];
    public status: string;

    constructor(
        private ngbActiveModal: NgbActiveModal,
        private ordersRepository: OrdersRepository,
    ) {
    }

    ngOnInit() {
    }

    no() {
        this.ngbActiveModal.close(false);
    }

    cancel() {
        this.ngbActiveModal.dismiss('cancel');
    }

    yes() {
        const data = {status: this.status};
        this.ordersRepository.updateStatusOrders(this.order, data)
            .subscribe((res) => {
                this.ngbActiveModal.close();
                this.toTop(data);
            });
    }

    toTop(data?: any) {
        (function ($) {
            $('#scroll-board').animate({
                scrollTop: 0
            }, 900);
            return false;
        })(jQuery);
    }
}
