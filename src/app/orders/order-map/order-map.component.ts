import {Component, OnInit} from '@angular/core';
import {MouseEvent} from '@agm/core';

@Component({
    selector: 'app-order-map',
    templateUrl: './order-map.component.html',
    styleUrls: ['./order-map.component.scss']
})
export class OrderMapComponent implements OnInit {
    // google maps zoom level
    zoom: number = 1;

    // initial center position for the map
    lat: number = 10.802818858803388;
    lng: number = 106.64770662337605;

    origin: any;
    destination: any;


    constructor() {
    }

    ngOnInit() {
        this.origin = {
            lat: 10.803243253755316,
            lng: 106.62381788110349
        };
        this.destination = {
            lat: 10.802818858803388,
            lng: 106.64770662337605
        };
    }

    clickedMarker(label: string, index: number) {
        console.log(`clicked the marker: ${label || index}`);
    }

    mapClicked($event: MouseEvent) {
        this.markers.push({
            lat: $event.coords.lat,
            lng: $event.coords.lng,
            draggable: true
        });
    }

    markerDragEnd(m: marker, $event: MouseEvent) {
        console.log('dragEnd', m, $event);
    }

    markers: marker[] = [];
}

interface marker {
    lat: number;
    lng: number;
    label?: string;
    draggable: boolean;
}
