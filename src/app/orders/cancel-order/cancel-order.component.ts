import {Component, OnInit, Input} from '@angular/core';
import {Order} from 'src/app/shared/entities/order';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {OrdersRepository} from 'src/app/shared/repositories/orders.repository';

declare var jQuery: any;

@Component({
    selector: 'app-cancel-order',
    templateUrl: './cancel-order.component.html',
    styleUrls: ['./cancel-order.component.scss']
})
export class CancelOrderComponent implements OnInit {

    @Input() order: Order;
    @Input() orderCanceled;

    constructor(
        private ngbActiveModal: NgbActiveModal,
        private ordersRepository: OrdersRepository,
    ) {
    }

    ngOnInit() {
    }

    no() {
        this.ngbActiveModal.close(false);
    }

    cancel() {
        this.ngbActiveModal.dismiss('cancel');
    }

    yes() {
        const data = {status: 'canceled'};
        this.ordersRepository.updateStatusOrders(this.order, data)
            .subscribe((res) => {
                this.ngbActiveModal.close();
                this.toTop();
                this.orderCanceled = this.orderCanceled.concat(this.order);
                console.log(this.orderCanceled);
            });
    }

    toTop() {
        (function ($) {
            $('#scroll-board').animate({
                scrollTop: 0
            }, 900);
            return false;
        })(jQuery);
    }
}
