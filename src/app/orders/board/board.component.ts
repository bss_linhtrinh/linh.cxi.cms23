import { Component, OnInit, Input } from '@angular/core';
import { Order } from 'src/app/shared/entities/order';
import { OrdersRepository } from 'src/app/shared/repositories/orders.repository';
import { BaseFilterCriteria } from 'src/app/shared/entities/base-filter-criteria';
import { OutletsRepository } from 'src/app/shared/repositories/outlets.repository';
import { PusherService } from 'src/app/shared/services/pusher/pusher.service';
import { FilterStorageService } from 'src/app/shared/services/filterStorage/filterStorage.service';
import { NotificationOrder } from 'src/app/shared/entities/notification-order';
import { from } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OrderDetailComponent } from '../order-detail/order-detail.component';

import { Router, ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';
import {ScopeService} from '../../shared/services/scope/scope.service';
import {UserScope} from '../../shared/entities/user-scope';

declare const helper: any;
declare var jQuery: any;

@Component({
    selector: 'app-board',
    templateUrl: './board.component.html',
    styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {

    @Input() page = 1;
    @Input() pageSize = 8;

    get from(): number {
        return (this.page - 1) * this.pageSize;
    }

    get to(): number {
        return this.from + this.pageSize;
    }

    public orders: Order[] = [];
    public orderPending = [];
    public orderConfirmed = [];
    public orderDelivered = [];
    public orderDelivering = [];
    public orderCanceled = [];
    public completed = [];
    public processing = [];
    pendingSize = 8;
    confirmedSize = 8;
    cancelledSize = 8;
    completedSize = 8;
    processingSize = 8;

    filterAttrs: BaseFilterCriteria[];
    filterLocalStore: any;
    data_confirm: any;
    note_cancel: string = '';
    filter = {
        keyword: '',
        keywordConfirmed: '',
        keywordprocessing: '',
        keywordcompleted: '',
        keywordCanceled: '',
        order: 'descending'
    };
    paginations: any = {
        current_page: 1
    };
    today = new Date();
    jstoday = '';
    cancal_status: any;
    status_order: any;

    toggleSearch: boolean = false;
    toggleConfirmed: boolean = false;
    toggleProcessing: boolean = false;
    toggleCanceled: boolean = false;
    toggleCompleted: boolean = false;
    //changerStatus
    public confirmed_change_status = [
        { name: 'Pending', value: 'pending' },
        { name: 'Delivered', value: 'delivered' },
        { name: 'Ordered', value: 'ordered' },
        { name: 'Food ready', value: 'food_ready' },
        { name: 'On the way', value: 'on_the_way' },
        { name: 'Completed', value: 'completed' },
    ];
    public processing_change_status = [
        { name: 'Pending', value: 'pending' },
        { name: 'Confirmed', value: 'confirmed' },
        { name: 'Delivered', value: 'delivered' },
        { name: 'Ordered', value: 'ordered' },
        { name: 'Food ready', value: 'food_ready' },
        { name: 'On the way', value: 'on_the_way' },
        { name: 'Completed', value: 'completed' },
    ];
    public completed_change_status = [
        { name: 'Pending', value: 'pending' },
        { name: 'Confirmed', value: 'confirmed' },
        { name: 'Delivered', value: 'delivered' },
        { name: 'Ordered', value: 'ordered' },
        { name: 'Food ready', value: 'food_ready' },
        { name: 'On the way', value: 'on_the_way' },
    ];
    public cancel_change_status = [
        { name: 'Pending', value: 'pending' },
        { name: 'Confirmed', value: 'confirmed' },
        { name: 'Delivered', value: 'delivered' },
        { name: 'Ordered', value: 'ordered' },
        { name: 'Food ready', value: 'food_ready' },
        { name: 'On the way', value: 'on_the_way' },
        { name: 'Completed', value: 'completed' },
    ];
    public status: string = 'choose_status';

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private ordersRepository: OrdersRepository,
        private outletsRepository: OutletsRepository,
        private ngbModal: NgbModal,
        private pusherService: PusherService,
        private filterStorage: FilterStorageService,
        private scopeService: ScopeService,
    ) {
        this.jstoday = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');
    }

    ngOnInit() {

        this.getListPendingOrders();
        this.getListConfirmedOrders();
        this.getListProcessing()
        this.getListCanceledOrders();
        this.getListCompleted();
        this.listenNotifications();
    }

    getListPendingOrders() {
        const query = JSON.stringify({
            'match': 'and',
            'rules': [
                {
                    'match': 'or',
                    'rules': [
                        {
                            'field': 'status',
                            'operator': '=',
                            'value': 'pending'
                        },
                    ]

                }
            ]
        });
        this.ordersRepository.all({ filters: query, created_at: this.jstoday, search: this.filter.keyword })
            .subscribe(
                (res: any) => {
                    this.orderPending = [];
                    this.orderPending = this.orderPending.concat(res);
                },
                (error) => {
                    helper.showNotification('Failed !!!', 'error', 'danger', 900);
                }
            );
    }

    getListProcessing() {
        const query = JSON.stringify({
            'match': 'and',
            'rules': [
                {
                    'match': 'or',
                    'rules': [
                        {
                            'field': 'status',
                            'operator': '=',
                            'value': 'delivered'
                        },
                        {
                            'field': 'created_at',
                            'operator': '=',
                            'value': this.jstoday
                        },
                        {
                            'field': 'status',
                            'operator': '=',
                            'value': 'ordered'
                        },
                        {
                            'field': 'status',
                            'operator': '=',
                            'value': 'food_ready'
                        },
                        {
                            'field': 'status',
                            'operator': '=',
                            'value': 'on_the_way'
                        }
                    ]

                }
            ]
        });
        this.ordersRepository.all({ filters: query, created_at: this.jstoday, search: this.filter.keywordprocessing })
            .subscribe(
                (res: any) => {
                    this.processing = [];
                    this.processing = this.processing.concat(res);
                },
                (error) => {
                    helper.showNotification('Failed !!!', 'error', 'danger', 900);
                }
            );
    }

    getListConfirmedOrders() {
        this.ordersRepository.all({ status: 'Confirmed', created_at: this.jstoday, search: this.filter.keywordConfirmed })
            .subscribe(
                (res: any) => {
                    this.orderConfirmed = [];
                    this.orderConfirmed = this.orderConfirmed.concat(res);
                },
                (error) => {
                    helper.showNotification('Failed !!!', 'error', 'danger', 900);
                }
            );
    }

    getListCanceledOrders() {
        this.ordersRepository.all({ status: 'Canceled', created_at: this.jstoday, search: this.filter.keywordCanceled })
            .subscribe(
                (res: any) => {
                    this.orderCanceled = [];
                    this.orderCanceled = this.orderCanceled.concat(res);
                },
                (error) => {
                    helper.showNotification('Failed !!!', 'error', 'danger', 900);
                }
            );
    }

    getListCompleted() {
        this.ordersRepository.all({ status: 'completed', created_at: this.jstoday, search: this.filter.keywordcompleted })
            .subscribe(
                (res: any) => {
                    this.completed = [];
                    this.completed = this.completed.concat(res);
                },
                (error) => {
                    helper.showNotification('Failed !!!', 'error', 'danger', 900);
                }
            );
    }
    onCancel(order: Order, event) {
        event.stopPropagation();
        this.showModal();
        this.cancal_status = order;
    }
    CancaledStatus() {
        let vm = this;
        const data = { status: "canceled" }
        this.ordersRepository.updateStatusOrders(this.cancal_status, data)
            .subscribe((res) => {
                this.orderCanceled = this.orderCanceled.concat(this.cancal_status);
                if (vm.cancal_status.status == "confirmed") {
                    this.orderConfirmed = this.orderConfirmed.filter(function (o) {
                        vm.cancal_status.status = 'canceled';
                        return o.id !== vm.cancal_status.id;
                    });
                }
                if (vm.cancal_status.status == "delivered" || vm.cancal_status.status == "ordered" || vm.cancal_status.status == "food_ready" || vm.cancal_status.status == "on_the_way") {
                    this.processing = this.processing.filter(function (o) {
                        vm.cancal_status.status = 'canceled';
                        return o.id !== vm.cancal_status.id;
                    });
                }
                if (vm.cancal_status.status == "pending") {
                    this.orderPending = this.orderPending.filter(function (o) {
                        vm.cancal_status.status = 'canceled';
                        return o.id !== vm.cancal_status.id;
                    });
                }
                this.note_cancel = ' ';
                this.toTop();
            });
    }
    changeStatus(order: Order, event) {
        event.stopPropagation();
        this.showModalChangeStatus();
        this.status_order = order;
    }
    changeStatusOrder() {
        let vm = this;
        const data = { status: this.status }
        this.ordersRepository.updateStatusOrders(this.status_order, data)
            .subscribe(() => {
                switch (this.status) {
                    case 'pending': {
                        this.orderPending = this.orderPending.concat(this.status_order);
                        if (vm.status_order.status == "confirmed") {
                            this.orderConfirmed = this.orderConfirmed.filter(function (o) {
                                vm.status_order.status = 'pending';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        if (vm.status_order.status == "canceled") {
                            this.orderCanceled = this.orderCanceled.filter(function (o) {
                                vm.status_order.status = 'pending';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        if (vm.status_order.status == "completed") {
                            this.completed = this.completed.filter(function (o) {
                                vm.status_order.status = 'pending';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        if (vm.status_order.status == 'delivered' || vm.status_order.status == 'ordered' || vm.status_order.status == 'food_ready' || vm.status_order.status == 'on_the_way') {
                            this.processing = this.processing.filter(function (o) {
                                vm.status_order.status = 'pending';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        break;
                    }
                    case 'completed': {
                        this.completed = this.completed.concat(this.status_order);
                        if (vm.status_order.status == "confirmed") {
                            this.orderConfirmed = this.orderConfirmed.filter(function (o) {
                                vm.status_order.status = 'completed';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        if (vm.status_order.status == "canceled") {
                            this.orderCanceled = this.orderCanceled.filter(function (o) {
                                vm.status_order.status = 'completed';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        if (vm.status_order.status == 'delivered' || vm.status_order.status == 'ordered' || vm.status_order.status == 'food_ready' || vm.status_order.status == 'on_the_way') {
                            this.processing = this.processing.filter(function (o) {
                                vm.status_order.status = 'completed';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        break;
                    }
                    case 'delivered': {
                        this.getListProcessing();
                        // this.processing = this.processing.concat(this.status_order);
                        if (vm.status_order.status == "confirmed") {
                            this.orderConfirmed = this.orderConfirmed.filter(function (o) {
                                vm.status_order.status = 'delivered';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        if (vm.status_order.status == "canceled") {
                            this.orderCanceled = this.orderCanceled.filter(function (o) {
                                vm.status_order.status = 'delivered';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        if (vm.status_order.status == "completed") {
                            this.completed = this.completed.filter(function (o) {
                                vm.status_order.status = 'delivered';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        break;
                    }
                    case 'ordered': {
                        this.getListProcessing();
                        // this.processing = this.processing.concat(this.status_order);
                        if (vm.status_order.status == "confirmed") {
                            this.orderConfirmed = this.orderConfirmed.filter(function (o) {
                                vm.status_order.status = 'ordered';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        if (vm.status_order.status == "canceled") {
                            this.orderCanceled = this.orderCanceled.filter(function (o) {
                                vm.status_order.status = 'ordered';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        if (vm.status_order.status == "completed") {
                            this.completed = this.completed.filter(function (o) {
                                vm.status_order.status = 'ordered';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        break;
                    }
                    case 'food_ready': {
                        this.getListProcessing();
                        // this.processing = this.processing.concat(this.status_order);
                        if (vm.status_order.status == "confirmed") {
                            this.orderConfirmed = this.orderConfirmed.filter(function (o) {
                                vm.status_order.status = 'food_ready';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        if (vm.status_order.status == "canceled") {
                            this.orderCanceled = this.orderCanceled.filter(function (o) {
                                vm.status_order.status = 'food_ready';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        if (vm.status_order.status == "completed") {
                            this.completed = this.completed.filter(function (o) {
                                vm.status_order.status = 'food_ready';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        break;
                    }
                    case 'on_the_way': {
                        this.getListProcessing();
                        //this.processing = this.processing.concat(this.status_order);
                        if (vm.status_order.status == "confirmed") {
                            this.orderConfirmed = this.orderConfirmed.filter(function (o) {
                                vm.status_order.status = 'on_the_way';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        if (vm.status_order.status == "canceled") {
                            this.orderCanceled = this.orderCanceled.filter(function (o) {
                                vm.status_order.status = 'on_the_way';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        if (vm.status_order.status == "completed") {
                            this.completed = this.completed.filter(function (o) {
                                vm.status_order.status = 'on_the_way';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        break;
                    }
                    case 'confirmed': {
                        this.orderConfirmed = this.orderConfirmed.concat(this.status_order);
                        if (vm.status_order.status == "canceled") {
                            this.orderCanceled = this.orderCanceled.filter(function (o) {
                                vm.status_order.status = 'confirmed';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        if (vm.status_order.status == "completed") {
                            this.completed = this.completed.filter(function (o) {
                                vm.status_order.status = 'confirmed';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        if (vm.status_order.status == 'delivered' || vm.status_order.status == 'ordered' || vm.status_order.status == 'food_ready' || vm.status_order.status == 'on_the_way') {
                            this.processing = this.processing.filter(function (o) {
                                vm.status_order.status = 'confirmed';
                                return o.id !== vm.status_order.id;
                            });
                        }
                        break;
                    }
                }
                this.toTop();
                this.status = 'choose_status';
            });
    }
    onConfirm(order: any) {
        this.data_confirm = order;
        event.stopPropagation();
        (function ($) {
            $('#confirm-order').modal('show');
        })(jQuery);
    }
    confirmOrdersStatus(order?: any) {
        this.orderConfirmed = this.orderConfirmed.concat(order);
        this.orderPending = this.orderPending.filter(function (o) {
            order.status = 'confirmed';
            return o.id !== order.id;
        });
    }
    onViewDetail(order: Order) {
        if (!order.brand || !order.brand.id) {
            return;
        }
        const modalRef = this.ngbModal.open(OrderDetailComponent, { centered: true, windowClass: 'order-detail-popup' });
        modalRef.componentInstance.order = order;
        modalRef.result.then(
            (success: any) => {
                console.log(success)
                this.cancal_status = success;
                this.status_order = success;
                this.data_confirm = success;
            }
        );
        return from(modalRef.result);
    }

    listenNotifications() {
        let channelName = null;
        this.filterLocalStore = this.filterStorage.retrieve();
        if (this.filterLocalStore) {
            if (this.filterLocalStore.outlet && this.filterLocalStore.outlet.id) {
                channelName = 'private-order.on.outlet.' + this.filterLocalStore.outlet.id;
            } else if (this.filterLocalStore.brand && this.filterLocalStore.brand.id) {
                channelName = 'private-order.on.brand.' + this.filterLocalStore.brand.id;
            } else if (this.filterLocalStore.organization && this.filterLocalStore.organization.id) {
                channelName = 'private-order.on.organization.' + this.filterLocalStore.organization.id;
            }
        } else {
            this.scopeService.getUserScope$()
                .subscribe(
                    (userScope: UserScope) => {
                        if (userScope.outlet && userScope.outlet.id) {
                            channelName = 'private-order.on.outlet.' + userScope.outlet.id;
                        } else if (userScope.brand && userScope.brand.id) {
                            channelName = 'private-order.on.brand.' + userScope.brand.id;
                        } else if (userScope.organization && userScope.organization.id) {
                            channelName = 'private-order.on.organization.' + userScope.organization.id;
                        }
                    }
                );
        }
        this.pusherService.subscribe(channelName)
            .bind('orders.created', (data: NotificationOrder) => {
                console.log(134)
                this.ordersRepository.all({ id: data.order_id })
                    .subscribe(
                        (res: any) => {
                            this.orderPending = this.orderPending.concat(res);
                        },
                        (error) => {
                            helper.showNotification('Failed !!!', 'error', 'danger', 900);
                        }
                    );
            });
        this.pusherService.subscribe(channelName)
            .bind('orders.updated', (data: NotificationOrder) => {
                console.log(data);
                switch (data.status_from) {
                    case 'pending':
                        const pendingOrder = this.orderPending.filter(function (o) {
                            return o.id === data.order_id;
                        });
                        if (data.status_to === 'confirmed') {
                            this.orderConfirmed = this.orderConfirmed.concat(pendingOrder);
                            this.orderPending = this.orderPending.filter(function (o) {
                                return o.id !== data.order_id;
                            });
                        } else if (data.status_to === 'canceled') {
                            this.orderCanceled = this.orderCanceled.concat(pendingOrder);
                            this.orderPending = this.orderPending.filter(function (o) {
                                return o.id !== data.order_id;
                            });
                        }
                        break;
                    case 'confirmed':
                        const confirmedOrder = this.orderConfirmed.filter(function (o) {
                            return o.id === data.order_id;
                        });
                        if (data.status_to === 'pending') {
                            this.orderPending = this.orderPending.concat(confirmedOrder);
                            this.orderConfirmed = this.orderConfirmed.filter(function (o) {
                                return o.id !== data.order_id;
                            });
                        } else if (data.status_to === 'canceled') {
                            this.orderCanceled = this.orderCanceled.concat(confirmedOrder);
                            this.orderConfirmed = this.orderConfirmed.filter(function (o) {
                                return o.id !== data.order_id;
                            });
                        } else if (data.status_to === 'delivered' || data.status_to === 'completed') {
                            this.orderDelivered = this.orderDelivered.concat(confirmedOrder);
                            this.orderConfirmed = this.orderConfirmed.filter(function (o) {
                                return o.id !== data.order_id;
                            });
                        } else if (data.status_to === 'food_ready' || data.status_to === 'on_the_way') {
                            this.orderDelivering = this.orderDelivering.concat(confirmedOrder);
                            this.orderConfirmed = this.orderConfirmed.filter(function (o) {
                                return o.id !== data.order_id;
                            });
                        }
                        break;
                    case 'hold_order':
                        break;
                    case 'food_ready':
                    case 'on_the_way':
                        const deliveringOrder = this.orderDelivering.filter(function (o) {
                            return o.id === data.order_id;
                        });
                        if (data.status_to === 'pending') {
                            this.orderPending = this.orderPending.concat(deliveringOrder);
                            this.orderDelivering = this.orderDelivering.filter(function (o) {
                                return o.id !== data.order_id;
                            });
                        } else if (data.status_to === 'canceled') {
                            this.orderCanceled = this.orderCanceled.concat(deliveringOrder);
                            this.orderDelivering = this.orderDelivering.filter(function (o) {
                                return o.id !== data.order_id;
                            });
                        } else if (data.status_to === 'delivered' || data.status_to === 'completed') {
                            this.orderDelivered = this.orderDelivered.concat(deliveringOrder);
                            this.orderDelivering = this.orderDelivering.filter(function (o) {
                                return o.id !== data.order_id;
                            });
                        } else if (data.status_to === 'food_ready' || data.status_to === 'on_the_way') {
                            this.orderDelivering = this.orderDelivering.concat(deliveringOrder);
                            this.orderDelivering = this.orderDelivering.filter(function (o) {
                                return o.id !== data.order_id;
                            });
                        }
                        break;
                    case 'ordered':
                    case 'completed':
                    case 'delivered':
                        const deliveredOrder = this.orderDelivered.filter(function (o) {
                            return o.id === data.order_id;
                        });
                        if (data.status_to === 'canceled') {
                            this.orderCanceled = this.orderCanceled.concat(deliveredOrder);
                            this.orderDelivered = this.orderDelivered.filter(function (o) {
                                return o.id !== data.order_id;
                            });
                        }
                        break;
                    default:
                        break;
                }
            });
    }

    increaseShow(status) {
        switch (status) {
            case 'pending':
                this.pendingSize += 8;
                break;
            case 'confirmed':
                this.confirmedSize += 8;
                break;
            case 'processing':
                this.processingSize += 8;
                break;
            case 'cancelled':
                this.cancelledSize += 8;
                break;
            case 'completed':
                this.completedSize += 8;
                break;
        }
    }
    //search
    onSearch(_item?: string) {
        switch (_item) {
            case 'pendding': {
                this.toggleSearch = !this.toggleSearch;
                break;
            }
            case 'confirmed': {
                this.toggleConfirmed = !this.toggleConfirmed;
                break;
            }
            case 'processing': {
                this.toggleProcessing = !this.toggleProcessing;
                break;
            }
            case 'canceled': {
                this.toggleCanceled = !this.toggleCanceled;
                break;
            }
            case 'completed': {
                this.toggleCompleted = !this.toggleCompleted;
                break;
            }
        }
    }
    //edit order
    onEdit(order: Order) {
        this.router.navigate(['..', order.id, 'edit'], { relativeTo: this.activatedRoute });
    }
    //key search
    onKey() {
        this.getListPendingOrders();
    }
    searchConfirmed() {
        this.getListConfirmedOrders();
    }
    searchprocessing() {
        this.getListProcessing();
    }
    searchcompleted() {
        this.getListCompleted();
    }
    searchCanceled() {
        this.getListCanceledOrders();
    }
    //top
    showModal() {
        (function ($) {
            $('#canceled-Board').modal('show');
        })(jQuery);
    }
    showModalChangeStatus() {
        (function ($) {
            $('#changeStatus-Board').modal('show');
        })(jQuery);
    }
    toTop() {
        (function ($) {
            $('#changeStatus-Board').modal('hide');
            $('#canceled-Board').modal('hide');
            $('#confirm-cancel').modal('hide');
            $('#scroll-board').animate({
                scrollTop: 0
            }, 900);
            return false;
        })(jQuery);
    }
}
