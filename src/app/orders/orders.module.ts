import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OrdersRoutingModule} from './orders-routing.module';
import {FormsModule} from '@angular/forms';
import {AgmCoreModule} from '@agm/core';
import {AgmDirectionModule} from 'agm-direction';
import {QRCodeModule} from 'angularx-qrcode';
import {SharedModule} from '../shared/shared.module';
import { SelectProductComponent } from './add-product/select-product/select-product.component';
import { SelectVariantComponent } from './add-product/select-variant/select-variant.component';
import { AddProductComponent } from './add-product/add-product.component';
import { ApplicationScopeComponent } from './application-scope/application-scope.component';
import { BoardComponent } from './board/board.component';
import { SelectPartiesComponent } from './booking/select-parties/select-parties.component';
import { SelectPartyComponent } from './booking/select-party/select-party.component';
import { ChangeStatusComponent } from './change-status/change-status.component';
import { ConfirmOrderComponent } from './confirm-order/confirm-order.component';
import { CreateOrderComponent } from './create-order/create-order.component';
import { EditOrderComponent } from './edit-order/edit-order.component';
import { ListOrdersComponent } from './list-orders/list-orders.component';
import { OrderAssignDriverComponent } from './order-assign-driver/order-assign-driver.component';
import { OrderBookingDetailComponent } from './order-booking-detail/order-booking-detail.component';
import { OrderDeliveryComponent } from './order-delivery/order-delivery.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { OrderMapComponent } from './order-map/order-map.component';
import { OrderProductsComponent } from './order-products/order-products.component';
import { OrderPromotionsComponent } from './order-promotions/order-promotions.component';
import { SelectCustomerComponent } from './select-customer/select-customer.component';
import {CancelOrderComponent} from './cancel-order/cancel-order.component';

const components = [
    ListOrdersComponent,
    BoardComponent,
    CreateOrderComponent,
    EditOrderComponent,
    OrderDetailComponent,

    ConfirmOrderComponent,
    AddProductComponent,
    SelectProductComponent,
    SelectVariantComponent,
    CancelOrderComponent,
    ChangeStatusComponent,
    OrderProductsComponent,
    SelectCustomerComponent,
    ApplicationScopeComponent,

    OrderPromotionsComponent,
    OrderMapComponent,
    OrderDeliveryComponent,
    OrderAssignDriverComponent,

    // booking
    OrderBookingDetailComponent,
    SelectPartiesComponent,
    SelectPartyComponent,
];

@NgModule({
    declarations: [
        ...components,
    ],
    imports: [
        CommonModule,
        FormsModule,
        AgmCoreModule,
        AgmDirectionModule,
        QRCodeModule,
        SharedModule,
        OrdersRoutingModule
    ],
    entryComponents: [
        // ConfirmOrderComponent,
        OrderDetailComponent,
        AddProductComponent,
        ChangeStatusComponent
    ]
})
export class OrdersModule {
}
