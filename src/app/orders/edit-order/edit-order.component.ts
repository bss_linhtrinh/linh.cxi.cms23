import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {filter, map, switchMap, tap} from 'rxjs/operators';
import {Filter} from '../../shared/entities/filter';
import {Outlet} from '../../shared/entities/outlets';
import {Customer} from '../../shared/entities/customer';
import {Category} from '../../shared/entities/category';
import {Deliver} from '../../shared/entities/deliver';
import {PaymentMethod} from '../../shared/entities/payment-method';
import {Promotion} from '../../shared/entities/promotion';
import {ORDER_METHODS} from '../../shared/constants/order-methods';
import {ORDER_STATUSES} from '../../shared/constants/order-statuses';
import {ORDER_PAYMENT_STATUSES} from '../../shared/constants/order-payment-statuses';
import {OrderType} from '../../shared/types/order-type';
import {CxiGridConfig} from '../../shared/components/cxi-grid/cxi-grid-config';
import {Order} from '../../shared/entities/order';
import {OutletsRepository} from '../../shared/repositories/outlets.repository';
import {CustomerRepository} from '../../shared/repositories/customers.repository';
import {CategoriesRepository} from '../../shared/repositories/categories.repository';
import {DeliversRepository} from '../../shared/repositories/delivers.repository';
import {OrdersRepository} from '../../shared/repositories/orders.repository';
import {MessagesService} from '../../messages/messages.service';
import {NotificationService} from '../../shared/services/noitification/notification.service';
import {PaymentMethodsRepository} from '../../shared/repositories/payment-methods.repository';
import {PromotionRepository} from '../../shared/repositories/promotion.repository';
import {Address} from '../../shared/entities/address';
import {User} from '../../shared/entities/user';
import {UsersRepository} from '../../shared/repositories/users.repository';

@Component({
    selector: 'app-edit-order',
    templateUrl: './edit-order.component.html',
    styleUrls: ['./edit-order.component.scss']
})
export class EditOrderComponent implements OnInit {
    order: Order = new Order();
    filter: Filter;
    selectedFile: File = null;

    outlets: Outlet[] = [];
    customers: Customer[] = [];
    categories: Category[] = [];
    delivers: Deliver[];
    paymentMethods: PaymentMethod[] = [];
    promotions: Promotion[] = [];

    orderMethods = ORDER_METHODS;
    orderStatuses = ORDER_STATUSES;
    paymentStatuses = ORDER_PAYMENT_STATUSES;

    deliveryOrderType = OrderType.Delivery;

    cxiTableConfig = CxiGridConfig.from({
        sorting: true,
    });

    users: User[];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private outletsRepository: OutletsRepository,
        private customersRepository: CustomerRepository,
        private categoriesRepository: CategoriesRepository,
        private deliversRepository: DeliversRepository,
        private ordersRepository: OrdersRepository,
        private messagesService: MessagesService,
        private ngbModal: NgbModal,
        private notificationService: NotificationService,
        private paymentMethodsRepository: PaymentMethodsRepository,
        private promotionsRepository: PromotionRepository,
        private usersRepository: UsersRepository
    ) {
    }

    ngOnInit() {
        this.getOrderDetail();
        this.getData();
    }

    getOrderDetail() {
        this.activatedRoute.params
            .pipe(
                filter(data => data.id),
                map(data => data.id),
                switchMap(
                    (id: number) => this.ordersRepository.find(id)
                )
            )
            .subscribe(
                (order: Order) => {
                    this.order = order;
                }
            );
    }

    getData() {
        this.getOutlets();
        this.getCustomers();
        this.getCategories();
        this.getDelivers();
        this.getPaymentMethods();
        this.getAvailablePromotions();
        this.getUsers();
    }

    getOutlets() {
        this.outletsRepository.allActivated()
            .subscribe(
                (outlets: Outlet[]) => this.outlets = outlets
            );
    }

    getCustomers() {
        this.customersRepository.all()
            .subscribe(
                (customers: Customer[]) => this.customers = customers
            );
    }

    getCategories() {
        const params = {status: 1};
        this.categoriesRepository.allAsTree()
            .subscribe(
                (categories: Category[]) => this.categories = categories
            );
    }

    getDelivers() {
        this.deliversRepository.all()
            .subscribe(
                (delivers: Deliver[]) => this.delivers = delivers
            );
    }

    getPaymentMethods() {
        this.paymentMethodsRepository.all()
            .subscribe(
                (paymentMethods: PaymentMethod[]) => this.paymentMethods = paymentMethods
            );
    }

    getAvailablePromotions() {
        this.promotionsRepository.getAvailable()
            .subscribe(
                (promotions: Promotion[]) => this.promotions = promotions
            );
    }

    getUsers() {
        this.usersRepository.all({pagination: 0})
            .pipe(
                tap(
                    (users: User[]) => this.users = users
                )
            )
            .subscribe();
    }

    cancel() {
        this.back();
    }

    back() {
        this.router.navigate(['../..', 'list'], {relativeTo: this.activatedRoute});
    }

    save() {
        if (!this.order.customer && !this.order.is_guest) {
            this.notificationService.error(['order_customer_required']);
            return;
        }
        if (!this.order.items || this.order.items.length === 0) {
            this.notificationService.error(['order_products_at_least_one_item']);
            return;
        }
        if (this.order.order_type === OrderType.Delivery) {
            if (this.order.shipping_address && this.order.shipping_address.name && !this.order.shipping_address.address1) {
                this.notificationService.error([`order_address_delivery_required`]);
                return;
            }
        }
        if (!this.order.brand_id) {
            this.notificationService.error([`order_brand_required`]);
            return;
        }

        this.ordersRepository.save(this.order)
            .subscribe(
                () => this.back()
            );
    }

    onSelectCustomer(customer: Customer) {
    }

    // add address from address book
    addAddress() {
        this.messagesService.selectAddresses(this.order.customer.addresses)
            .subscribe(
                (address: Address) => {
                    if (address) {
                        this.order.shipping_address.name = address ? address.name : null;
                        this.order.shipping_address.phone = address ? address.phone : null;
                        this.order.shipping_address.address1 = address ? address.address1 : null;
                        this.order.shipping_address.city = address ? address.city : null;
                        this.order.shipping_address.district = address ? address.district : null;
                        this.order.shipping_address.ward = address ? address.ward : null;
                    }
                }
            );
    }

    handleAddressChange(address) {
        if (address.formatted_address) {
            this.order.shipping_address.address1 = address.formatted_address;
            this.order.shipping_address.address2 = address.formatted_address;
            this.order.shipping_address.country = address.country;
            this.order.shipping_address.postcode = address.postcode;
            this.order.shipping_address.state = address.state;
            this.order.shipping_address.city = address.city;
            this.order.shipping_address.district = address.district;
            this.order.shipping_address.ward = address.ward;
            this.order.shipping_address.latitude = address.latitude;
            this.order.shipping_address.longitude = address.longitude;
        }
    }

    onChangePromotion(promotion: Promotion) {

    }
}
