import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListOrdersComponent} from './list-orders/list-orders.component';
import {BoardComponent} from './board/board.component';
import {CreateOrderComponent} from './create-order/create-order.component';
import {EditOrderComponent} from './edit-order/edit-order.component';
import {OrderBookingDetailComponent} from './order-booking-detail/order-booking-detail.component';
import {CheckPermissionsGuard} from '../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: '', redirectTo: 'list', pathMatch: 'full'},
            {path: 'list', component: ListOrdersComponent, data: {breadcrumb: 'List orders'}},
            {path: 'board', component: BoardComponent, data: {breadcrumb: 'Board'}},
            {path: 'create', component: CreateOrderComponent, data: {breadcrumb: 'Create new order'}},
            {path: ':id/edit', component: EditOrderComponent, data: {breadcrumb: 'Edit order'}},
            {
                path: ':id/detail',
                component: OrderBookingDetailComponent,
                data: {breadcrumb: 'Order detail'}
            },
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OrdersRoutingModule {
}
