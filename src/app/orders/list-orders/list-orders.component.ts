import {Component, OnInit} from '@angular/core';
import {OrdersRepository} from 'src/app/shared/repositories/orders.repository';
import {OutletsRepository} from 'src/app/shared/repositories/outlets.repository';
import {Outlet} from 'src/app/shared/entities/outlets';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ConfirmOrderComponent} from '../confirm-order/confirm-order.component';
import {from} from 'rxjs';
import {switchMap, tap} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {LocalStorage} from 'ngx-webstorage';
import {CxiGridConfig} from '../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../shared/components/cxi-grid/cxi-grid-column';
import {BrandScope} from '../../shared/types/brand-scope';
import {OrderStatus} from '../../shared/types/order-status';
import {CxiGridService} from '../../shared/components/cxi-grid/cxi-grid.service';
import {MessagesService} from '../../messages/messages.service';
import {AuthService} from '../../shared/services/auth/auth.service';
import {OrdersBookingRepository} from '../../shared/repositories/orders-booking.repository';
import {Order} from '../../shared/entities/order';

@Component({
    selector: 'app-list-orders',
    templateUrl: './list-orders.component.html',
    styleUrls: ['./list-orders.component.scss']
})
export class ListOrdersComponent implements OnInit {

    // public orders: Order[] = [];

    cxiGridConfig: CxiGridConfig;
    cxiGridColumns: CxiGridColumn[];
    @LocalStorage('auth.brandScope') brandScope: BrandScope;

    cxiGridConfigFB = {
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        multitab: true,
        pagingServer: true,
        repository: this.ordersRepository,
        actions: {
            onConfirm: (row: any) => {
                this.onConfirm(row);
            },
            onCancel: (row: any) => {
                this.onCancel(row);
            },
            onEdit: (row: any) => {
                this.onEdit(row);
            },
            onActivate: (row: any) => {
                this.onActivate(row);
            },
            onDeactivate: (row: any) => {
                this.onDeactivate(row);
            },
            onDelete: (row: any) => {
                this.onDelete(row);
            },
        },
        tabs: [
            {title: 'Pending', name: 'status', value: OrderStatus.Pending},
            {title: 'Confirmed', name: 'status', value: OrderStatus.Confirmed},
            {title: 'Hold order', name: 'status', value: OrderStatus.HoldOrder},
            {title: 'Ordered', name: 'status', value: OrderStatus.Ordered},
            {title: 'Ordering', name: 'status', value: OrderStatus.Ordering},
            {title: 'Food Ready', name: 'status', value: OrderStatus.FoodReady},
            {title: 'On The Way', name: 'status', value: OrderStatus.OnTheWay},
            {title: 'Completed', name: 'status', value: OrderStatus.Completed},
            {title: 'Delivered', name: 'status', value: OrderStatus.Delivered},
            {title: 'Canceled', name: 'status', value: OrderStatus.Canceled},
        ]
    }
    cxiGridColumnsFB = [
        {title: 'order', name: 'id'},
        {title: 'customer', name: 'customer_name'},
        {title: 'total', name: 'final_total', format: 'monetary'},
        {title: 'pay. status', name: 'status'},
    ];

    cxiGridConfigBooking = {
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        multitab: true,
        pagingServer: true,
        repository: this.ordersBookingRepository,
        actions: {
            onConfirm: (row: any) => {
                this.onConfirm(row);
            },
            onCancel: (row: any) => {
                this.onCancel(row);
            },
            onDetail: (row: any) => {
                this.onDetail(row);
            },
        },
        tabs: [
            {title: 'Pending', name: 'status', value: OrderStatus.Pending},
            {title: 'Confirmed', name: 'status', value: OrderStatus.Confirmed},
            {title: 'Canceled', name: 'status', value: OrderStatus.Canceled},
        ]
    };
    cxiGridColumnsBooking = ([
        {title: 'Order Code ', name: 'bill_id'},
        {title: 'Ordering Date', name: 'created_at'},
        {title: 'Event Code', name: 'resource.code'},
        {title: 'Registration', name: 'items', format: 'object'},
        {title: 'Ordered By', name: 'customer_name'},
        {title: 'Ticket Price', name: 'items', format: 'ticket-price'},
        {title: 'Quantity', name: 'items', format: 'quantity'},
        {title: 'Total Price', name: 'sub_total', format: 'monetary'},
    ]);


    pagination = {
        page: 1, per_page: 5
    };
    status = 'pending';
    filter: any;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private ordersRepository: OrdersRepository,
        private outletsRepository: OutletsRepository,
        private ngbModal: NgbModal,
        private cxiGridService: CxiGridService,
        private messagesService: MessagesService,
        private authService: AuthService,
        private ordersBookingRepository: OrdersBookingRepository
    ) {
    }

    ngOnInit() {
        this.filter = this.authService.getCurrentFilter();
        this.initCxiGrid();
    }

    initCxiGrid() {
        switch (this.brandScope) {
            case BrandScope.FB:
            default:
                this.cxiGridConfig = CxiGridConfig.from(this.cxiGridConfigFB);
                this.cxiGridColumns = CxiGridColumn.from(this.cxiGridColumnsFB);
                break;
            case BrandScope.Booking:
                this.cxiGridConfig = CxiGridConfig.from(this.cxiGridConfigBooking);
                this.cxiGridColumns = CxiGridColumn.from(this.cxiGridColumnsBooking);
                break;
        }
    }

    onConfirm(order: Order) {
        switch (this.brandScope) {
            case BrandScope.FB:
            default:
                this.onConfirmFB(order);
                break;
            case BrandScope.Booking:
                this.onConfirmBooking(order);
                break;
        }
    }

    onConfirmFB(order: Order) {
        if (!order.brand || !order.brand.id) {
            return;
        }
        this.outletsRepository.getOutletsByBrand(order.brand.id)
            .pipe(
                switchMap(
                    (outlets: Outlet[]) => {
                        const modalRef = this.ngbModal.open(ConfirmOrderComponent, {centered: true, windowClass: 'status-confirmation'});
                        modalRef.componentInstance.order = order;
                        modalRef.componentInstance.outlets = outlets;

                        return from(modalRef.result)
                            .pipe(
                                tap(
                                    () => this.cxiGridService.fireEventRowChanged(order)
                                )
                            );
                    }
                ),
            )
            .subscribe(
            );
    }

    onConfirmBooking(order: Order) {
        const message = `Are you sure you want to confirm selected order?`;
        this.messagesService.statusConfirmation(message)
            .pipe(
                switchMap(() => this.handleConfirm(order))
            )
            .subscribe();

    }

    onCancel(order: Order) {
        const message = `Are you sure you want to cancel selected order?`;
        this.messagesService.statusConfirmation(message)
            .pipe(
                switchMap(() => this.handleCancel(order))
            )
            .subscribe();

    }

    handleConfirm(order: Order) {
        const data = {
            outlet_id: null
        };
        return this.ordersRepository.changeStatusOrders(order, data)
            .pipe(
                tap(
                    (item) => this.cxiGridService.fireEventRowChanged(item)
                )
            );
    }

    handleCancel(order: Order) {
        const data = {status: 'canceled'};
        return this.ordersRepository.updateStatusOrders(order, data)
            .pipe(
                tap(
                    (item) => this.cxiGridService.fireEventRowChanged(item)
                )
            );
    }

    onEdit(order: Order) {
        this.router.navigate(['..', order.id, 'edit'], {relativeTo: this.activatedRoute});
    }

    onActivate(row: any) {
    }

    onDeactivate(row: any) {
    }

    onDelete(row: any) {
    }

    onDetail(order) {
        this.router.navigate(['..', order.id, 'detail'], {relativeTo: this.activatedRoute});
    }
}
