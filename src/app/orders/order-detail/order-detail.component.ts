import {Component, OnInit, Input} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Order} from 'src/app/shared/entities/order';

declare var jQuery: any;
declare const helper: any;

@Component({
    selector: 'app-order-detail',
    templateUrl: './order-detail.component.html',
    styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {

    @Input() order: Order;
    public add_on: any = [];
    public showAddOn: number;
    public qtyAddon: number;

    constructor(
        private ngbActiveModal: NgbActiveModal,
    ) {
    }

    ngOnInit() {
        this.listOrder();
    }

    listOrder() {
        let a = 0;
        let b = 0;
        this.order.items.forEach(_i => {
            _i.total = helper.formatNumber(_i.total, 'VND');
            this.qtyAddon = _i.qty_ordered;
            (_i.child) ? _i.child.price = helper.formatNumber(_i.child.price, 'VND') : null;
            this.add_on = _i.addons;
            this.add_on.forEach((_i1, index) => {
                a = _i.child.total;
                b += _i1.total;
            });
        });
        this.showAddOn = (a + b) * this.qtyAddon;
        this.showAddOn = helper.formatNumber(this.showAddOn, 'VND');
        this.order.sub_total = helper.formatNumber(this.order.sub_total, 'VND');
    }

    no() {
        (function ($) {
            $('#canceled-Board').modal('show');
        })(jQuery);
        this.ngbActiveModal.close(this.order);
    }

    cancel() {
        this.ngbActiveModal.dismiss('cancel');
    }

    yes() {
        (function ($) {
            $('#changeStatus-Board').modal('show');
        })(jQuery);
        this.ngbActiveModal.close(this.order);
    }

    confimOrder() {
        this.ngbActiveModal.close(this.order);
        (function ($) {
            $('#confirm-order').modal('show');
        })(jQuery);
    }
}
