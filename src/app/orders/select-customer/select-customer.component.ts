import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {tap} from 'rxjs/operators';
import {ElementBase} from '../../shared/custom-form/element-base/element-base';
import {Customer} from '../../shared/entities/customer';
import {MessagesService} from '../../messages/messages.service';
import {CustomerRepository} from '../../shared/repositories/customers.repository';

@Component({
    selector: 'cxi-order-select-customer',
    templateUrl: './select-customer.component.html',
    styleUrls: ['./select-customer.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectCustomerComponent, multi: true}
    ]
})
export class SelectCustomerComponent extends ElementBase<Customer> implements OnInit {

    @Input() name: string;
    @Input() isGuest: boolean;

    @Output() isGuestChange = new EventEmitter<boolean>();
    @Output() select = new EventEmitter<Customer>();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private messagesService: MessagesService,
        private customersRepository: CustomerRepository
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    addCustomer() {
        const columns = [
            {title: 'name', name: 'name', avatar: 'avatar'},
            {title: 'email', name: 'email'},
            {title: 'address', name: 'address'},
            {
                title: '',
                name: '',
                action: {
                    label: 'Select',
                }
            },
        ];
        const options = {
            columns: columns,
            repository: this.customersRepository
        };
        this.messagesService.selectItemFromList(options)
            .pipe(
                tap(
                    (customer: Customer) => this.handleSelectedCustomer(customer)
                )
            )
            .subscribe(
            );
    }

    handleSelectedCustomer(customer: Customer) {
        if (customer) {
            this.value = customer;

            // is_guest
            this.isGuest = false;
            this.isGuestChange.emit(this.isGuest);

            // select customer
            this.select.emit(customer);
        }
    }

    markAsVisitor() {
        if (!this.isGuest) {
            this.isGuest = true;
            this.value = null;
        } else {
            this.isGuest = false;
        }

        this.isGuestChange.emit(this.isGuest);
    }

    handleCustomerAddressChange(address: any) {

    }
}
