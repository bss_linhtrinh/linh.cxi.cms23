import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {tap} from 'rxjs/operators';
import {LocalStorage} from 'ngx-webstorage';
import {Brand} from '../../shared/entities/brand';
import {Outlet} from '../../shared/entities/outlets';
import {ApplicationScope} from '../../shared/entities/application-scope';
import {AuthService} from '../../shared/services/auth/auth.service';
import {BrandsRepository} from '../../shared/repositories/brands.repository';
import {Scope} from '../../shared/types/scopes';
import {OutletsRepository} from '../../shared/repositories/outlets.repository';

@Component({
    selector: 'app-order-application-scope',
    templateUrl: './application-scope.component.html',
    styleUrls: ['./application-scope.component.scss']
})
export class ApplicationScopeComponent implements OnInit {

    brands: Brand[] = [];
    outlets: Outlet[] = [];

    // application scope
    isHiddenBrand: boolean;
    isHiddenOutlet: boolean;
    isHiddenApplicationScope: boolean;

    @Input() name: string;
    @Input() applicationScope: ApplicationScope;
    @Output() applicationScopeChange = new EventEmitter<ApplicationScope>();

    @LocalStorage('scope.organization_id') scopeOrganizationId: number;
    @LocalStorage('scope.brand_id') scopeBrandId: number;
    @LocalStorage('scope.outlet_id') scopeOutletId: number;
    @LocalStorage('scope.current') scopeCurrent: Scope;

    constructor(
        private authService: AuthService,
        private brandsRepository: BrandsRepository,
        private outletsRepository: OutletsRepository
    ) {
    }

    ngOnInit() {
        this.getApplicationScope();
    }

    getApplicationScope() {
        switch (this.scopeCurrent) {
            case Scope.Organization:
                this.isHiddenBrand = false;
                this.isHiddenOutlet = false;
                if (this.scopeOrganizationId) {
                    this.getBrandsByOrganization(this.scopeOrganizationId);
                }
                break;

            case Scope.Brand:
                this.isHiddenBrand = true;
                this.isHiddenOutlet = false;
                if (this.scopeBrandId) {
                    this.applicationScope.brand_id = this.scopeBrandId;
                }
                this.getOutletsByBrand(this.applicationScope.brand_id);
                break;

            case Scope.Outlet:
                this.isHiddenBrand = true;
                this.isHiddenOutlet = true;
                this.isHiddenApplicationScope = true;
                if (this.scopeBrandId) {
                    this.applicationScope.brand_id = this.scopeBrandId;
                }
                if (this.scopeOutletId) {
                    this.applicationScope.outlet_id = this.scopeOutletId;
                }
                break;
        }
    }

    getBrandsByOrganization(organizationId: number) {
        const params = {organization_id: organizationId};
        this.brandsRepository.all(params)
            .subscribe(
                (brands: Brand[]) => {
                    this.brands = brands;

                    if (this.applicationScope.id) { // edit
                        this.getOutletsByBrand(this.applicationScope.brand_id);
                    }
                }
            );
    }

    getOutletsByBrand(brandId: number) {
        this.outletsRepository.getOutletsByBrand(brandId)
            .pipe(
                tap(
                    (outlets: Outlet[]) => {
                        // refresh & remove
                        const outletIndex = outlets.findIndex(x => x.id === this.applicationScope.outlet_id);
                        if (outletIndex === -1) {
                            this.applicationScope.outlet_id = null;
                        }
                    }
                )
            )
            .subscribe(
                (outlets: Outlet[]) => this.outlets = outlets
            );
    }

    onSelectBrand(brandId: number) {
        this.getOutletsByBrand(brandId);
    }

    onSelectOutlet(outletId: number) {
    }

    onChangeBrand() {
    }
}
