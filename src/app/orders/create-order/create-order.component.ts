import {Component, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import * as _ from 'lodash';
import {LocalStorage} from 'ngx-webstorage';
import {BrandScope} from '../../shared/types/brand-scope';
import {Order} from '../../shared/entities/order';
import {Booking} from '../../shared/entities/booking';
import {Filter} from '../../shared/entities/filter';
import {Outlet} from '../../shared/entities/outlets';
import {Customer} from '../../shared/entities/customer';
import {Category} from '../../shared/entities/category';
import {PaymentMethod} from '../../shared/entities/payment-method';
import {Promotion} from '../../shared/entities/promotion';
import {ORDER_METHODS} from '../../shared/constants/order-methods';
import {ORDER_STATUSES} from '../../shared/constants/order-statuses';
import {ORDER_PAYMENT_STATUSES} from '../../shared/constants/order-payment-statuses';
import {OrderType} from '../../shared/types/order-type';
import {CxiGridConfig} from '../../shared/components/cxi-grid/cxi-grid-config';
import {OutletsRepository} from '../../shared/repositories/outlets.repository';
import {CustomerRepository} from '../../shared/repositories/customers.repository';
import {CategoriesRepository} from '../../shared/repositories/categories.repository';
import {OrdersRepository} from '../../shared/repositories/orders.repository';
import {BookingRepository} from '../../shared/repositories/booking.repository';
import {MessagesService} from '../../messages/messages.service';
import {NotificationService} from '../../shared/services/noitification/notification.service';
import {PaymentMethodsRepository} from '../../shared/repositories/payment-methods.repository';
import {PromotionRepository} from '../../shared/repositories/promotion.repository';
import {AuthService} from '../../shared/services/auth/auth.service';
import {EventRepository} from '../../shared/repositories/event.repository';
import {CustomerBooking} from '../../shared/entities/customer-booking';
import {BookingTicket} from '../../shared/entities/booking-ticket';
import {tap} from 'rxjs/operators';
import {User} from '../../shared/entities/user';
import {UsersRepository} from '../../shared/repositories/users.repository';
import {StorageService} from '../../shared/services/storage/storage.service';

@Component({
    selector: 'app-create-order',
    templateUrl: './create-order.component.html',
    styleUrls: ['./create-order.component.scss']
})
export class CreateOrderComponent implements OnInit {

    @LocalStorage('auth.brandScope') brandScope: BrandScope;

    order: Order = new Order();
    eventsFilter = [];
    bookingType = ['Individual', 'Group'];
    booking: Booking = new Booking();
    filter: Filter;
    tickets = [];
    quantity = 1;
    total = 0;
    selectedFile: File = null;

    outlets: Outlet[] = [];
    customers: Customer[] = [];
    categories: Category[] = [];
    paymentMethods: PaymentMethod[] = [];
    promotions: Promotion[] = [];

    orderMethods = ORDER_METHODS;
    orderStatuses = ORDER_STATUSES;
    paymentStatuses = ORDER_PAYMENT_STATUSES;

    deliveryOrderType = OrderType.Delivery;


    cxiTableConfig = CxiGridConfig.from({
        sorting: true,
    });
    types = [
        {name: 'Event', value: 'event'},
        {name: 'Education', value: 'education', disabled: true},
    ];
    registrationTypes = [
        {name: 'Visitor', value: 'visitor'},
        {name: 'Speaker', value: 'speaker'},
        {name: 'Sponsor', value: 'sponsor'},
        {name: 'Advertise', value: 'advertise'},
        {name: 'Exhibition', value: 'exhibition'},
        {name: 'Booth', value: 'booth'},
    ];

    users: User[];

    schemaOrder: any;

    constructor(
        private router: Router,
        private ngbModal: NgbModal,
        private activatedRoute: ActivatedRoute,
        private outletsRepository: OutletsRepository,
        private customersRepository: CustomerRepository,
        private categoriesRepository: CategoriesRepository,
        private ordersRepository: OrdersRepository,
        private bookingRepository: BookingRepository,
        private messagesService: MessagesService,
        private notificationService: NotificationService,
        private paymentMethodsRepository: PaymentMethodsRepository,
        private promotionsRepository: PromotionRepository,
        private authService: AuthService,
        private eventRepository: EventRepository,
        private usersRepository: UsersRepository,
        private storageService: StorageService
    ) {
        const systemConfig = this.storageService.retrieve('system.config');
        this.schemaOrder = systemConfig && systemConfig.schemas && systemConfig.schemas.order
            ? systemConfig.schemas.order
            : null;
    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        this.getOutlets();
        this.getCustomers();
        this.getCategories();
        this.getPaymentMethods();
        this.getAvailablePromotions();
        this.getEvents();
        this.getUsers();
    }

    getOutlets() {
        this.outletsRepository.allActivated()
            .subscribe(
                (outlets: Outlet[]) => this.outlets = outlets
            );
    }

    getEvents() {
        this.eventRepository.all({with_tickets: true})
            .subscribe(
                (events) => {
                    events.map(event => {
                        if (event.allow_booking === 1) {
                            this.eventsFilter.push({name: event.title, value: event.id});
                        }
                    });
                    this.setEvent();
                }
            );
    }

    getUsers() {
        this.usersRepository.all({pagination: 0})
            .pipe(
                tap(
                    (users: User[]) => this.users = users
                )
            )
            .subscribe();
    }

    setEvent() {
        if (this.eventsFilter && this.eventsFilter.length > 0) {
            this.booking.resource_id = this.eventsFilter[0].value;
            this.getTicketByEvent(this.eventsFilter[0].value);
        }
    }

    getTicketByEvent(eventId) {
        this.eventRepository.getTicketByEvent(eventId, {type: this.booking.register_type})
            .subscribe(
                (tickets) => {
                    this.tickets = tickets;
                });
    }

    onChangeEvent(eventId) {
        this.booking.ticket_apply = [];
        this.quantity = 1;
        this.eventRepository.getTicketByEvent(eventId, {type: this.booking.register_type})
            .subscribe(
                (tickets) => {
                    this.tickets = tickets;
                });
    }


    onChangeType(type) {

    }


    onChangeTypeRegister(type) {
        this.booking.ticket_apply = [];
        this.quantity = 1;
        this.eventRepository.getTicketByEvent(this.booking.resource_id, {type: type})
            .subscribe(
                (tickets) => {
                    this.tickets = tickets;
                });
    }


    getCustomers() {
        this.customersRepository.all()
            .subscribe(
                (customers: Customer[]) => this.customers = customers
            );
    }

    getCategories() {
        const params = {status: 1};
        this.categoriesRepository.allAsTree(params)
            .subscribe(
                (categories: Category[]) => this.categories = categories
            );
    }


    getPaymentMethods() {
        this.paymentMethodsRepository.all()
            .subscribe(
                (paymentMethods: PaymentMethod[]) => this.paymentMethods = paymentMethods
            );
    }

    getAvailablePromotions() {
        this.promotionsRepository.getAvailable()
            .subscribe(
                (promotions: Promotion[]) => this.promotions = promotions
            );
    }


    cancel() {
        this.back();
    }

    back() {
        this.router.navigate(['..', 'list'], {relativeTo: this.activatedRoute});
    }

    save() {
        if (this.brandScope === 'fb') {
            if (!this.order.customer && !this.order.is_guest) {
                this.notificationService.error(['order_customer_required']);
                return;
            }
            if (!this.order.items || this.order.items.length === 0) {
                this.notificationService.error(['order_products_at_least_one_item']);
                return;
            }
            if (this.order.order_type === OrderType.Delivery) {
                if (this.order.shipping_address && this.order.shipping_address.name && !this.order.shipping_address.address1) {
                    this.notificationService.error([`order_address_delivery_required`]);
                    return;
                }
            }
            if (!this.order.brand_id) {
                this.notificationService.error([`order_brand_required`]);
                return;
            }
            this.ordersRepository.save(this.order)
                .subscribe(
                    () => this.back()
                );
        }
        if (this.brandScope === 'booking') {

            if (!this.booking.tickets) {
                this.notificationService.error([`ticket_required`]);
                return;
            }

            this.bookingRepository.createBooking(this.booking)
                .subscribe(
                    () => this.back()
                );
        }

    }

    onSelectCustomer(customer: Customer) {
        if (customer) {
            this.order.customer_id = customer.id;
            this.order.customer_email = customer.email;
            this.order.customer_first_name = customer.first_name;
            this.order.customer_last_name = customer.last_name;
            this.order.customer_phone = customer.phone;
        }
    }

    onChangePromotion(promotion: Promotion) {

    }

    filterChild(array, haveChild): any[] {
        if (haveChild) {
            return array.filter(i => i.children.length > 0);
        } else {
            return array.filter(i => i.children.length === 0);
        }
    }


    onRemove(ticketId: any) {
        this.booking.tickets.map(item => {
            if (item.ticketId === ticketId) {
                item.party.splice(parseInt(item.quantity, 10), 1);
            }
        });
    }

    handleQuantity(ticketId, child) {
        let tempArr = _.cloneDeep(this.booking.ticket_apply);
        if (child === null) {
            if (this.booking.ticket_type === 'Individual') {
                const eventTicket = this.tickets.filter(item => item.id === parseInt(ticketId, 10));
                tempArr.map(item => {
                    if (item.ticketId === eventTicket[0].id) {
                        item.quantity = eventTicket[0].quantity;
                    }
                });
            }
            if (this.booking.ticket_type === 'Group') {
                tempArr.map(item => {
                    item.quantity = this.quantity;
                });
                this.addCustomer();
            }
        } else {
            const eventTicket = this.tickets.filter(item => item.id === child);
            if (this.booking.ticket_type === 'Individual') {
                const childTicket = eventTicket[0].children.filter(child => child.id === parseInt(ticketId, 10));
                tempArr.map(item => {
                    if (item.ticketId === childTicket[0].id) {
                        item.quantity = childTicket[0].quantity;
                    }
                });
            }
            if (this.booking.ticket_type === 'Group') {
                tempArr.map(item => {
                    item.quantity = this.quantity;
                });
                this.addCustomer();
            }
        }
        this.booking.ticket_apply = _.cloneDeep(tempArr);
    }

    handleCheckbox(event: boolean, parent, childId: number) {
        if (typeof event !== 'undefined' && event && parent === null) {
            const eventTicket = this.tickets.filter(item => item.id === childId);
            if (this.booking.ticket_type === 'Individual') {
                this.booking.ticket_apply.push({
                    ticketId: eventTicket[0].id, price: eventTicket[0].price,
                    quantity: 1, name: eventTicket[0].name
                });
                if (this.booking.party.length === 0) {
                    this.addCustomer();
                }
            }
            if (this.booking.ticket_type === 'Group') {
                this.booking.ticket_apply.push({
                    ticketId: eventTicket[0].id, price: eventTicket[0].price,
                    quantity: this.quantity, name: eventTicket[0].name
                });
                this.addCustomer();
            }
            this.calcTotal();
        }
        if (typeof event !== 'undefined' && event && parent !== null) {
            const eventTicket = this.tickets.filter(item => item.id === parent);
            const eventChild = eventTicket[0].children.filter(child => child.id === childId);
            if (this.booking.ticket_type === 'Individual') {
                this.booking.ticket_apply.push({
                    ticketId: eventChild[0].id, price: eventChild[0].price,
                    quantity: 1, name: eventChild[0].name
                });
                if (this.booking.party.length === 0) {
                    this.addCustomer();
                }
            }
            if (this.booking.ticket_type === 'Group') {
                this.booking.ticket_apply.push({
                    ticketId: eventChild[0].id, price: eventChild[0].price,
                    quantity: this.quantity, name: eventChild[0].name
                });
                this.addCustomer();
            }
            this.calcTotal();
        } else if (typeof event !== 'undefined' && !event) {
            this.booking.ticket_apply = this.booking.ticket_apply.filter(item => item.ticketId !== childId);
            this.removeQuantity(this.tickets, childId, parent);
            this.calcTotal();
            if (this.booking.ticket_type === 'Individual') {
                if (this.booking.party.length > 0) {
                    this.onRemoveCustomer();
                }
            }
        }

        this.booking.tickets = this.booking.ticket_apply.map(bookingTicket => BookingTicket.create(bookingTicket));
        this.checkMaxQuantity(this.tickets);
    }

    removeQuantity(tickets, ticketId, parent) {
        if (!parent) {
            tickets.forEach(ticket => {
                if (ticket.id === ticketId) {
                    ticket.quantity = 1;
                }
            });
        } else {
            tickets.forEach(ticket => {
                ticket.children.forEach(child => {
                    if (child.id === ticketId) {
                        child.quantity = 1;
                    }
                });
            });
        }
    }

    calcTotal() {
        this.total = 0;
        this.booking.ticket_apply.map(ticket => {
            this.total = this.total + (ticket.price * ticket.quantity);
        });
    }

    onChangeBookingType() {
        this.total = 0;
        if (this.booking.ticket_type === 'Individual') {
            this.quantity = 1;
        }
        this.booking.ticket_apply = [];
    }

    checkMaxQuantity(tickets) {
        tickets.forEach(ticket => {
            if (ticket.children && ticket.children.length > 0) {
                ticket.children.forEach(child => {
                    if (child.maximum_purchase > (child.limit - child.sold)) {
                        child.maximum_purchase = (child.limit - child.sold);
                    }
                });
            } else {
                if (ticket.maximum_purchase > (ticket.limit - ticket.sold)) {
                    ticket.maximum_purchase = (ticket.limit - ticket.sold);
                }
            }
        });
    }

    quantityChange(event, ticketId, child) {
        if (event === 'inc') {
            // this.addCustomer(ticketId);
            this.handleQuantity(ticketId, child);
            if (this.booking.ticket_apply[0]) {
                this.calcTotal();
            }

        } else if (event === 'dec') {
            if (this.booking.ticket_type === 'Group') {
                this.onRemoveCustomer();
            }
            this.onRemove(ticketId);
            this.handleQuantity(ticketId, child);
            this.calcTotal();
        }
    }

    onRemoveCustomer() {
        if (this.booking.ticket_type === 'Individual') {
            this.booking.party.splice(0, 1);
        }
        if (this.booking.ticket_type === 'Group') {
            this.booking.party.splice(this.quantity, 1);
        }
    }

    addCustomer() {
        if (this.booking.ticket_type === 'Individual') {
            this.booking.party.push(new CustomerBooking());
        }

        if (this.booking.ticket_type === 'Group') {
            var items = [];
            for (var i = 1; i <= this.quantity; i++) {
                items.push(new CustomerBooking());
            }
            this.booking.party = _.cloneDeep(items);
        }
    }

    addCustomers() {
        const columns = [
            {title: 'name', name: 'name', avatar: 'avatar'},
            {title: 'email', name: 'email'},
            {title: 'address', name: 'address'},
            {
                title: '',
                name: '',
                action: {
                    label: 'Select',
                }
            },
        ];
        const options = {
            columns: columns,
            repository: this.customersRepository
        };
        this.messagesService.selectItemFromList(options)
            .pipe(
                tap(
                    (customer: Customer) => this.handleSelectedCustomer(customer)
                )
            )
            .subscribe(
            );
    }

    handleSelectedCustomer(customer: Customer) {
        if (customer) {
            this.booking.customer_first_name = customer.first_name;
            this.booking.customer_last_name = customer.last_name;
            this.booking.customer_phone = customer.phone;
            this.booking.customer_email = customer.email;
            this.booking.customer_id = customer.id;
        }
    }

    markAsVisitors() {
        if (this.booking.customer_id) {
            this.booking.customer_id = null;
            this.booking.customer_first_name = null;
            this.booking.customer_last_name = null;
            this.booking.customer_phone = null;
            this.booking.customer_email = null;
        }
    }
}

