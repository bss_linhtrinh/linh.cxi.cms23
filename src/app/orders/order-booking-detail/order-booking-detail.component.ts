import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {switchMap, tap} from 'rxjs/operators';
import {BookingList} from '../../shared/entities/booking-list';
import {OrdersBookingRepository} from '../../shared/repositories/orders-booking.repository';
import {MessagesService} from '../../messages/messages.service';
import {OrdersRepository} from '../../shared/repositories/orders.repository';
import {Image} from '../../shared/entities/image';

@Component({
    selector: 'app-order-booking-detail',
    templateUrl: './order-booking-detail.component.html',
    styleUrls: ['./order-booking-detail.component.scss']
})
export class OrderBookingDetailComponent implements OnInit {

    order: BookingList = new BookingList();
    isLoaded = false;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private bookingRepository: OrdersBookingRepository,
        private messagesService: MessagesService,
        private ordersRepository: OrdersRepository
    ) {
    }

    ngOnInit() {
        this.loadOrder();
    }


    loadOrder() {
        this.activatedRoute.params.subscribe((data) => {
            const id = data.id;
            this.bookingRepository.find(id).subscribe((res) => {
                this.order = res;
                this.prepareImage();
                this.isLoaded = true;
            });
        });
    }

    onConfirm() {
        const message = `Are you sure you want to confirm selected order?`;
        this.messagesService.statusConfirmation(message)
            .pipe(
                switchMap(() => this.handleConfirm(this.order))
            )
            .subscribe();

    }

    onCancel() {
        const message = `Are you sure you want to cancel selected order?`;
        this.messagesService.statusConfirmation(message)
            .pipe(
                switchMap(() => this.handleCancel(this.order))
            )
            .subscribe();
    }


    handleConfirm(order) {
        const data = {
            outlet_id: null
        };
        return this.ordersRepository.changeStatusOrders(order, data)
            .pipe(
                tap(
                    (item) => this.router.navigate(['../../list'], {relativeTo: this.activatedRoute})
                )
            );
    }

    handleCancel(order) {
        const data = {status: 'canceled'};
        return this.ordersRepository.updateStatusOrders(order, data)
            .pipe(
                tap(
                    (item) => this.router.navigate(['../../list'], {relativeTo: this.activatedRoute})
                )
            );
    }

    onPrint() {
        this.ordersRepository.printPdf(this.order).subscribe((res) => {
            // saveAs(res, 'order');
            var fileName = 'nameForFile' + '.extension';
            if (window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(res, fileName);
            } else {
                var link = document.createElement('a');
                link.setAttribute('type', 'hidden');
                link.download = fileName;
                link.href = window.URL.createObjectURL(res);
                document.body.appendChild(link);
                link.click();
            }
        });
    }

    prepareImage() {
        this.order.items.forEach(item => {
            item.items.forEach(val => {
                val.customer_images_of_certification = val.customer_images_of_certification.map(image => Image.create(image));
            });
        });
    }

}
