import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderBookingDetailComponent } from './order-booking-detail.component';

describe('OrderBookingDetailComponent', () => {
  let component: OrderBookingDetailComponent;
  let fixture: ComponentFixture<OrderBookingDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderBookingDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderBookingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
