import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Customer} from '../../shared/entities/customer';
import {Address} from '../../shared/entities/address';
import {MessagesService} from '../../messages/messages.service';
import * as _ from 'lodash';

@Component({
    selector: 'cxi-order-delivery',
    templateUrl: './order-delivery.component.html',
    styleUrls: ['./order-delivery.component.scss']
})
export class OrderDeliveryComponent implements OnInit {

    @Input() name: string;
    @Input() customer: Customer;


    private _billingAddress;
    private _shippingAddress;

    @Input()
    get billingAddress(): Address {
        return this._billingAddress;
    }

    @Input()
    get shippingAddress(): Address {
        return this._shippingAddress;
    }

    @Output() billingAddressChange = new EventEmitter<Address>();

    set billingAddress(address: Address) {
        this._billingAddress = address;
        this.billingAddressChange.emit(this.billingAddress);
    }

    @Output() shippingAddressChange = new EventEmitter<Address>();

    set shippingAddress(address: Address) {
        this._shippingAddress = address;
        this.shippingAddressChange.emit(this.shippingAddress);
    }

    constructor(
        private messagesService: MessagesService
    ) {
    }

    ngOnInit() {
    }

    addAddress() {
        this.messagesService.selectAddresses(this.customer.addresses)
            .subscribe(
                (address: Address) => {
                    if (address) {
                        this.shippingAddress.name = address ? address.name : null;
                        this.shippingAddress.phone = address ? address.phone : null;
                        this.shippingAddress.address1 = address ? address.address1 : null;
                        this.shippingAddress.address2 = address ? address.address2 : null;
                        this.shippingAddress.country = address ? address.country : null;
                        this.shippingAddress.state = address ? address.state : null;
                        this.shippingAddress.city = address ? address.city : null;
                        this.shippingAddress.district = address ? address.district : null;
                        this.shippingAddress.ward = address ? address.ward : null;
                        this.shippingAddress.postcode = address ? address.postcode : null;
                        this.shippingAddress.latitude = address ? address.latitude : null;
                        this.shippingAddress.longitude = address ? address.longitude : null;

                        this.cloneToBillingAddress();
                    }
                }
            );
    }

    // handle typing google address
    handleAddressChange(address) {
        if (address.formatted_address) {
            this.shippingAddress.address1 = address.formatted_address;
            this.shippingAddress.address2 = address.formatted_address;
            this.shippingAddress.country = address.country;
            this.shippingAddress.postcode = address.postcode;
            this.shippingAddress.state = address.state;
            this.shippingAddress.city = address.city;
            this.shippingAddress.district = address.district;
            this.shippingAddress.ward = address.ward;
            this.shippingAddress.latitude = address.latitude;
            this.shippingAddress.longitude = address.longitude;

            this.cloneToBillingAddress();
        }
    }

    cloneToBillingAddress() {
        this.billingAddress = _.cloneDeep(this.shippingAddress);
    }
}
