import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {UsersRepository} from '../../shared/repositories/users.repository';
import {tap} from 'rxjs/operators';
import {NotificationService} from '../../shared/services/noitification/notification.service';

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
    changePasswordObj = {
        current_password: '',
        password: '',
        password_confirmation: '',
    };

    constructor(private ngbActiveModal: NgbActiveModal,
                private usersRepository: UsersRepository,
                private notificationService: NotificationService) {
    }

    ngOnInit() {
    }

    changePassword() {
        this.usersRepository.changePasswordCurrentUser(this.changePasswordObj)
            .pipe(
                tap(
                    () => this.ngbActiveModal.close()
                )
            )
            .subscribe(
                () => this.notificationService.success(`Change your password success.`)
            );
    }

    cancel() {
        this.ngbActiveModal.dismiss('cancel');
    }
}
