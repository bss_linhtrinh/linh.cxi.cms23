import { Component, OnInit } from '@angular/core';
import { User } from '../shared/entities/user';
import { UsersRepository } from '../shared/repositories/users.repository';
import { LocalStorageService } from 'ngx-webstorage';
import { AuthService } from '../shared/services/auth/auth.service';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { Provider } from '../shared/types/providers';
import { Gender } from '../shared/entities/gender';
import { from, of } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ChangePasswordComponent } from './change-password/change-password.component';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
    profile: User;
    baseSelectGenders = Gender.init();
    selectedFile: File = null;
    reader: FileReader;

    isEditing = false;

    constructor(
        private localStore: LocalStorageService,
        private userRepository: UsersRepository,
        private authService: AuthService,
        private ngbModal: NgbModal
    ) {
    }

    ngOnInit() {
        this.getProfile();
    }

    getProfile() {
        this.profile = this.localStore.retrieve('user');
        const params = this.profile.provider === Provider.SuperAdmin
            ? {
                organization_id: -1
            } :
            {};
        this.userRepository.find(this.profile.id, params)
            .subscribe(
                (user: User) => this.updateProfile(user)
            );
    }

    editProfile() {
        this.isEditing = true;
    }

    cancel() {
        this.getProfile();
        this.isEditing = false;
    }

    onFileChanged(event) {
        if (event.target.files && event.target.files[0]) {
            this.selectedFile = event.target.files[0];
            this.reader = new FileReader();
            this.reader.readAsDataURL(event.target.files[0]); // read file as data url

            this.reader.onload = (evt: Event) => { // called once readAsDataURL is completed
                const oldAvatar = this.profile.avatar;
                this.profile.avatar = this.reader.result;
                this.userRepository.uploadAvatarCurrentUser(this.selectedFile)
                    .pipe(
                        catchError(
                            () => this.profile.avatar = oldAvatar
                        )
                    )
                    .subscribe();
            };
        }
    }

    errorHandler(event) {
        event.target.src = 'assets/img/avatar-default.jpg';
    }

    onSubmit() {
        this.userRepository.save(this.profile)
            .pipe(
                catchError(
                    (err: any) => this.userRepository.find(this.profile.id)
                ),
                switchMap(
                    () => this.userRepository.find(this.profile.id)
                )
            )
            .subscribe(
                (user: User) => {
                    this.selectedFile = null;

                    this.updateProfile(user);
                    this.userRepository.fireEventChangeProfile(user);

                    this.cancel();
                }
            );
    }

    remove() {
        this.userRepository.removeAvatarCurrentUser()
            .subscribe((res: any) => {
                if (res.error == false) {
                    this.selectedFile = null;
                    this.profile.avatar = null;
                }
            });
    }

    changePassword() {
        // alert('change password coming soon!!');
        const modalRef = this.ngbModal.open(ChangePasswordComponent, { centered: true, windowClass: 'change-password' });
        return from(modalRef.result)
            .pipe(catchError(() => of(false)));
    }

    updateProfile(user) {
        if (user) {
            this.profile.first_name = user.first_name;
            this.profile.last_name = user.last_name;
            this.profile.dob = user.dob;
            this.profile.gender = user.gender;
            this.profile.email = user.email;
            this.profile.phone = user.phone;
            this.profile.address = user.address;
        }
        this.localStore.store('user', this.profile);
    }
}
