import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProfileRoutingModule} from './profile-routing.module';
import {ProfileComponent} from './profile.component';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {ChangePasswordComponent} from './change-password/change-password.component';

@NgModule({
    declarations: [
        ProfileComponent,
        ChangePasswordComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        ProfileRoutingModule,
    ],
    entryComponents: [
        ChangePasswordComponent
    ]
})
export class ProfileModule {
}
