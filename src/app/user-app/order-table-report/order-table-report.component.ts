import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { orderListDeliveryAppRepository } from 'src/app/shared/repositories/order-list-delivery-app';
declare const helper: any;

@Component({
  selector: 'app-order-table-report',
  templateUrl: './order-table-report.component.html',
  styleUrls: ['./order-table-report.component.scss']
})
export class OrderTableReportComponent implements OnInit, OnChanges {
  public orderListDeliveryApp: any = [];
  public filter_order: string = 'asc';
  public isShow: boolean = false;
  public filterTable = [
    { name: 'ORDER', isShow: true, isShowAsc: true, isShowDesc: false },
    { name: 'DATE', isShow: true, isShowAsc: true, isShowDesc: false },
    { name: 'CUSTOMER', isShow: true, isShowAsc: true, isShowDesc: false },
    { name: 'APPLICATION SCOPE', isShow: true, isShowAsc: true, isShowDesc: false },
    { name: 'PAYMENT TOTAL', isShow: true, isShowAsc: true, isShowDesc: false },
  ]
  @Input() tabOrder: string;
  @Input() dateStartOrder: string;
  @Input() dataEndOrder: string;
  @Input() timeSelected: string;
  // pagination
  @LocalStorage('pagination.current_page') current_page: number = 1;
  @LocalStorage('pagination.per_page') per_page: number = 5;
  @LocalStorage('pagination.total') total: number;
  constructor(
    private orderListRepository: orderListDeliveryAppRepository,
  ) { }

  ngOnInit() {
    //this.getOrderList();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dateStartOrder || changes.dataEndOrder || changes.timeSelected) {
      this.getOrderList();
    }
  }
  getOrderList() {
    let sen_data = {
      date_start: this.dateStartOrder,
      date_end: this.dataEndOrder,
      platform: this.tabOrder,
      page: this.current_page,
      per_page: this.per_page,
      group_by: this.timeSelected,
      order: this.filter_order,
    }
    this.orderListRepository.all(sen_data)
      .subscribe(
        (res: any) => {
          console.log(res)
          this.orderListDeliveryApp = res;
          this.orderListDeliveryApp.forEach(_i => {
            _i.payment_total = helper.formatPrice(_i.payment_total, 'VND');
          });
        },
        (error) => {
          console.log(error);
        }
      );
  }
  onChangePagination() {
    this.getOrderList();
  }
  showListOrder(item?: any, index?: number) {
    this.filterTable.forEach((o) => {
      if (o.name.toUpperCase() == item.name) {
        item.isShowDesc = true;
        item.isShowAsc = false;
        item.isShow = !item.isShow;
      } else {
        o.isShowAsc = true;
        o.isShowDesc = false;
        o.isShow = true;
      }
    });
    if (item.isShow == true) {
      this.filter_order = 'desc';
      this.getOrderList();
    } else {
      this.filter_order = 'asc';
      this.getOrderList();
    }
  }
}
