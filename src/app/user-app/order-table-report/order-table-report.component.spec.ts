import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderTableReportComponent } from './order-table-report.component';

describe('OrderTableReportComponent', () => {
  let component: OrderTableReportComponent;
  let fixture: ComponentFixture<OrderTableReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderTableReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderTableReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
