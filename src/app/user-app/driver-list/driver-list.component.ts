import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { driverListDeliveryAppRepository } from 'src/app/shared/repositories/driver-list-delivery-app';

@Component({
  selector: 'app-driver-list',
  templateUrl: './driver-list.component.html',
  styleUrls: ['./driver-list.component.scss']
})
export class DriverListComponent implements OnInit, OnChanges {
  public driverListDeliveryApp: any = [];
  public filter_order: string = 'asc';
  public isShow: boolean = false;
  public filterTable = [
    { name: 'EMPLOYEE ID ', isShow: true, isShowAsc: true, isShowDesc: false },
    { name: 'NAME', isShow: true, isShowAsc: true, isShowDesc: false },
    { name: 'TOTAL ORDER', isShow: true, isShowAsc: true, isShowDesc: false },
    { name: 'APP. SCOPE', isShow: true, isShowAsc: true, isShowDesc: false },
  ]
  @Input() tabOrder: string;
  @Input() dateStartOrder: string;
  @Input() dataEndOrder: string;
  @Input() timeSelected: string;
  // pagination
  @LocalStorage('pagination.current_page') current_page: number = 1;
  @LocalStorage('pagination.per_page') per_page: number = 5;
  @LocalStorage('pagination.total') total: number;

  constructor(
    private driverListRepository: driverListDeliveryAppRepository,
  ) { }

  ngOnInit() {
    //this.getDriverList();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dateStartOrder || changes.dataEndOrder || changes.timeSelected) {
      this.getDriverList();
    }
  }
  getDriverList() {
    let sen_data = {
      date_start: this.dateStartOrder,
      date_end: this.dataEndOrder,
      platform: this.tabOrder,
      page: this.current_page,
      per_page: this.per_page,
      group_by: this.timeSelected,
      order: this.filter_order,
    }
    this.driverListRepository.all(sen_data)
      .subscribe(
        (res: any) => {
          this.driverListDeliveryApp = res;
        },
        (error) => {
          console.log(error);
        }
      );
  }
  onChangePagination() {
    this.getDriverList();
  }
  showListOrder(item?: any, index?: number) {
    this.filterTable.forEach((o) => {
      if (o.name.toUpperCase() == item.name) {
        item.isShowDesc = true;
        item.isShowAsc = false;
        item.isShow = !item.isShow;
      } else {
        o.isShowAsc = true;
        o.isShowDesc = false;
        o.isShow = true;
      }
    });
    if (item.isShow == true) {
      this.filter_order = 'desc';
      this.getDriverList();
    } else {
      this.filter_order = 'asc';
      this.getDriverList();
    }
  }
}
