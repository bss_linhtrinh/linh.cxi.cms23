import { Component, OnInit } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { formatDate } from '@angular/common';
@Component({
  selector: 'app-delivery-app',
  templateUrl: './delivery-app.component.html',
  styleUrls: ['./delivery-app.component.scss']
})
export class DeliveryAppComponent implements OnInit {

  today = new Date();
  public dateEnd = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');
  public dateStart = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');
  @LocalStorage('dashboard.timeSelected') timeSelected: string;
  // pagination
  @LocalStorage('pagination.current_page') current_page: number = 1;
  @LocalStorage('pagination.per_page') per_page: number;
  @LocalStorage('pagination.total') total: number;
  //
  stepy = ['deliveryOrder', 'orderList', 'driverLists']
  stepy_info = {
    deliveryOrder: 'Delivery Orders',
    orderList: 'Order Lists',
    driverLists: 'Driver Lists',
  }
  tab = "deliveryOrder";
  public userScope: UserScope;

  constructor(
    private scopeService: ScopeService,
  ) { }

  ngOnInit() {
    this.getListUserScope();
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          console.log(this.userScope)
        }
      );
  }
  change(tab) {
    var vm = this;
    switch (tab) {
      case 'deliveryOrder': {
        vm.tab = tab;
        this.dateEnd = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');
        this.dateStart = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');
        break;
      }
      case 'orderList': {
        vm.tab = tab;
        vm.current_page = 1;
        this.dateEnd = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');
        this.dateStart = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');
        break;
      }
      case 'driverLists': {
        vm.tab = tab;
        vm.current_page = 1;
        this.dateEnd = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');
        this.dateStart = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');
        break;
      }

    }
  }
  openDate() {
  }
  selectTime(timeSelected: string) {
    this.timeSelected = timeSelected;
  }
}
