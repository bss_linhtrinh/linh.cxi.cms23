import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CheckPermissionsGuard} from 'src/app/shared/guards/check-permissions/check-permissions.guard';
import {DeliveryAppComponent} from './delivery-app/delivery-app.component';

const routes: Routes = [
    {path: '', redirectTo: 'delivery-app', pathMatch: 'full'},
    {path: 'delivery-app', component: DeliveryAppComponent, canActivate: [CheckPermissionsGuard]},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserAppRoutingModule {
}
