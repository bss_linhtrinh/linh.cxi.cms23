import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderStatisticsForChartComponent } from './order-statistics-for-chart.component';

describe('OrderStatisticsForChartComponent', () => {
  let component: OrderStatisticsForChartComponent;
  let fixture: ComponentFixture<OrderStatisticsForChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderStatisticsForChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderStatisticsForChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
