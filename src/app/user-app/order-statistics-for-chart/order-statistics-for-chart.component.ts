import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Label } from 'ng2-charts';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { orderStaticForChartDeliveryAppRepository } from 'src/app/shared/repositories/order-statistics-for-chart';
import { CHART_TYPE } from '../../shared/constants/charts';

@Component({
  selector: 'app-order-statistics-for-chart',
  templateUrl: './order-statistics-for-chart.component.html',
  styleUrls: ['./order-statistics-for-chart.component.scss']
})
export class OrderStatisticsForChartComponent implements OnInit, OnChanges {

  public orderStatics: any = [];
  @Input() dateStartStores: string;
  @Input() dateEndStores: string;
  @Input() timeSelected: string;
  //chart
  public chartType: string = 'bar';

  public chartDatasets: Array<any> = [
    { data: [], label: 'My First dataset' }
  ];

  public chartLabels: Array<any> = [];

  public chartColors: Array<any> = [
    {
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

  constructor(
    private orderStaticRepository: orderStaticForChartDeliveryAppRepository,
  ) { }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dateStartStores || changes.dateEndStores || changes.timeSelected) {
      this.getListorderStaticRepository();
    }
  }
  getListorderStaticRepository() {
    let sen_data = {
      date_start: this.dateStartStores,
      date_end: this.dateEndStores,
      group_by: this.timeSelected
    }
    this.orderStaticRepository.all(sen_data)
      .subscribe(
        (res: any) => {
          this.orderStatics = res;
          this.orderStatics.forEach(_i => {
            _i.borderColor = '#15a5de';
            _i.backgroundColor = '#15a5de';
          });
          //this.chartData = this.orderStatics;
          this.chartColors = this.orderStatics;
          this.chartLabels = this.orderStatics.map(_i1 => {
            return this.chartLabels = _i1.label;
          })
          this.chartDatasets.forEach(_c => {
            _c.data = this.orderStatics.map(_i2 => {
              return _c.data = _i2.total;
            })
          });
        },
        (error) => {
          console.log(error);
        }
      );
  }
}
