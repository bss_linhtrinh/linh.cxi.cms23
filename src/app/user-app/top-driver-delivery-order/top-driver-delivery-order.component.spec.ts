import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopDriverDeliveryOrderComponent } from './top-driver-delivery-order.component';

describe('TopDriverDeliveryOrderComponent', () => {
  let component: TopDriverDeliveryOrderComponent;
  let fixture: ComponentFixture<TopDriverDeliveryOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopDriverDeliveryOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopDriverDeliveryOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
