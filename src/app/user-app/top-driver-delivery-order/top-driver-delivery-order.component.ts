import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Label } from 'ng2-charts';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { topDriverDeliveryOrderRepository } from 'src/app/shared/repositories/top-driver-delivery-order.repository';

@Component({
  selector: 'app-top-driver-delivery-order',
  templateUrl: './top-driver-delivery-order.component.html',
  styleUrls: ['./top-driver-delivery-order.component.scss']
})
export class TopDriverDeliveryOrderComponent implements OnInit, OnChanges {
  public chartTopDriverDeliveryOrder: any = [];
  @Input() dateStartStores: string;
  @Input() dateEndStores: string;
  @Input() timeSelected: string;
  //chart
  public chartType: string = 'horizontalBar';

  public chartDatasets: Array<any> = [
    { data: [], label: 'My First dataset' }
  ];

  public chartLabels: Array<any> = [];

  public chartColors: Array<any> = [
    {
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
      ],
      borderColor: [
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
      ],
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }
  constructor(
    private topDriverDeliveryOrderRepository: topDriverDeliveryOrderRepository,
  ) { }

  ngOnInit() {
    //this.getListtopDriverDeliveryOrderRepository();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dateStartStores || changes.dateEndStores || changes.timeSelected) {
      this.getListtopDriverDeliveryOrderRepository();
    }
  }
  getListtopDriverDeliveryOrderRepository() {
    let sen_data = {
      date_start: this.dateStartStores,
      date_end: this.dateEndStores,
      group_by: this.timeSelected
    }
    this.topDriverDeliveryOrderRepository.all(sen_data)
      .subscribe(
        (res: any) => {
          this.chartTopDriverDeliveryOrder = res;
          this.chartTopDriverDeliveryOrder.forEach(_i => {
            if (!_i.last_name) {
              _i.label = _i.first_name;
            }
            if (!_i.first_name) {
              _i.label = _i.last_name;
            }
            if (_i.last_name && _i.first_name) {
              _i.label = `${_i.first_name} ${_i.last_name}`;
            }
            _i.label = 'Order';
            _i.borderColor = '#15a5de';
            _i.backgroundColor = '#15a5de';
          });
          this.chartLabels = this.chartTopDriverDeliveryOrder.map(_i1 => {
            return this.chartLabels = _i1.name;
          })
          this.chartColors = this.chartTopDriverDeliveryOrder;
          this.chartDatasets.forEach(_c => {
            _c.data = this.chartTopDriverDeliveryOrder.map(_i2 => {
              return _c.data = _i2.count;
            })
          });
        },
        (error) => {
          console.log(error);
        }
      );
  }
}
