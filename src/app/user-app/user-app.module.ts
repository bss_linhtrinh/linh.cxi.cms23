import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from 'src/app/shared/shared.module';
import {ChartsModule} from 'ng2-charts';
import {FormsModule} from '@angular/forms';
import {UserAppRoutingModule} from './user-app-routing.module';
import {DeliveryAppComponent} from './delivery-app/delivery-app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TopStoresDeliveryOrderComponent} from './top-stores-delivery-order/top-stores-delivery-order.component';
import {TopDriverDeliveryOrderComponent} from './top-driver-delivery-order/top-driver-delivery-order.component';
import {OrderStatisticsForChartComponent} from './order-statistics-for-chart/order-statistics-for-chart.component';
import {OrderTableReportComponent} from './order-table-report/order-table-report.component';
import {DriverListComponent} from './driver-list/driver-list.component';

export const components = [
    DeliveryAppComponent,
    TopStoresDeliveryOrderComponent,
    TopDriverDeliveryOrderComponent,
    OrderStatisticsForChartComponent,
    OrderTableReportComponent,
    DriverListComponent
];

@NgModule({
    declarations: [
        ...components,
    ],
    imports: [
        NgbModule,
        CommonModule,
        SharedModule,
        ChartsModule,
        FormsModule,
        UserAppRoutingModule
    ]
})
export class UserAppModule {
}
