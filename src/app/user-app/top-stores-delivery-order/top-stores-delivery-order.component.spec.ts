import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopStoresDeliveryOrderComponent } from './top-stores-delivery-order.component';

describe('TopStoresDeliveryOrderComponent', () => {
  let component: TopStoresDeliveryOrderComponent;
  let fixture: ComponentFixture<TopStoresDeliveryOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopStoresDeliveryOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopStoresDeliveryOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
