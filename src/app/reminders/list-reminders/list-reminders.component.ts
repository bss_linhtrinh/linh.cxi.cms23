import { Component, OnInit } from '@angular/core';
import { CxiGridColumn } from '../../shared/components/cxi-grid/cxi-grid-column';
import { LocalStorage } from 'ngx-webstorage';
import { Router, ActivatedRoute } from '@angular/router';
import { Reminder } from 'src/app/shared/entities/reminder';
import { ReminderRepository } from 'src/app/shared/repositories/reminder.repository';
import { ColumnFilterType } from '../../shared/types/column-filter-type';

@Component({
  selector: 'app-list-reminders',
  templateUrl: './list-reminders.component.html',
  styleUrls: ['./list-reminders.component.scss']
})
export class ListRemindersComponent implements OnInit {
  // table
  cxiTableConfig = {
    paging: true,
    sorting: true,
    filtering: true,
    pagingServer: true,
    selecting: true,
    filterOutlet: true,
    repository: this.reminderRepository,
    translatable: true,
    actions: {
      onEdit: (reminder: Reminder) => {
        this.onEdit(reminder);
      },
      onActivate: (reminder: Reminder) => {
        this.onActivate(reminder);
      },
      onDeactivate: (reminder: Reminder) => {
        this.onDeactivate(reminder);
      },
      onDelete: (reminder: Reminder) => {
        this.onDelete(reminder);
      },
    },
  };
  cxiTableColumns = CxiGridColumn.from([
    { title: 'Title', name: 'title', sort: true, className: 'pageName' },
    { title: 'Created date', name: 'createdAt', sort: true, className: 'content', format: 'date', filterType: ColumnFilterType.Datepicker, },
  ]);
  @LocalStorage('scope.organization_id') organizationId: number;
  @LocalStorage('scope.brand_id') brandId: number;
  @LocalStorage('scope.outlet_id') outletId: number;
  constructor(
    private reminderRepository: ReminderRepository,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
  }
  onEdit(reminder: Reminder) {
    console.log(reminder)
    this.router.navigate(['..', reminder.id, 'edit'], { relativeTo: this.route });
  }
  onActivate(reminder: Reminder) {
    const data = [reminder];
    this.reminderRepository.changeStatus(data, { active: true })
      .subscribe();
  }

  onDeactivate(reminder: Reminder) {
    const data = [reminder];
    this.reminderRepository.changeStatus(data, { active: false })
      .subscribe();
  }

  onDelete(reminder: Reminder) {
    const data = [reminder];
    this.reminderRepository.destroyMultiple(data)
      .subscribe();
  }
}
