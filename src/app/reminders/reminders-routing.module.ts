import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CheckPermissionsGuard} from '../shared/guards/check-permissions/check-permissions.guard';
import {ListRemindersComponent} from './list-reminders/list-reminders.component';
import {CreateReminderComponent} from './create-reminder/create-reminder.component';
import {EditReminderComponent} from './edit-reminder/edit-reminder.component';


const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: 'list', component: ListRemindersComponent, data: {breadcrumb: 'List reminder'}},
            {path: 'create', component: CreateReminderComponent, data: {breadcrumb: 'Create new reminder'}},
            {path: ':id/edit', component: EditReminderComponent, data: {breadcrumb: 'Edit reminder'}},
            {path: '', redirectTo: 'list', pathMatch: 'full'},
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RemindersRoutingModule { }
