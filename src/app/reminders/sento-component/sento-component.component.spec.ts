import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentoComponentComponent } from './sento-component.component';

describe('SentoComponentComponent', () => {
  let component: SentoComponentComponent;
  let fixture: ComponentFixture<SentoComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentoComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentoComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
