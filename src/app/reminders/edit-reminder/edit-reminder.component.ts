import { Component, OnInit } from '@angular/core';
import { Reminder } from 'src/app/shared/entities/reminder';
import { User } from 'src/app/shared/entities/user';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { UsersRepository } from 'src/app/shared/repositories/users.repository';
import { Router, ActivatedRoute } from '@angular/router';
import { map, filter, switchMap } from 'rxjs/operators';
import { ReminderRepository } from 'src/app/shared/repositories/reminder.repository';
import * as moment from 'moment';
import { Moment } from 'moment';
import { LocalStorage } from 'ngx-webstorage';
@Component({
  selector: 'app-edit-reminder',
  templateUrl: './edit-reminder.component.html',
  styleUrls: ['./edit-reminder.component.scss']
})
export class EditReminderComponent implements OnInit {
  public reminder: Reminder = new Reminder();
  public _outletId;
  tab: any;
  public users: User;
  userScope: UserScope;
  public brands: any = [];
  public outlets: any = [];
  public minDate;
  public repeatTypes: any = [
    { name: 'Daily', value: 'daily' },
    { name: 'Weekly', value: 'weekly' },
    { name: 'Monthly', value: 'monthly' },
    { name: 'None', value: 'none' },
  ];
  public days: any = [
    { name: 'Monday', value: 0 },
    { name: 'Friday', value: 4 },
    { name: 'Tuesday', value: 1 },
    { name: 'Saturday', value: 5 },
    { name: 'Wednesday', value: 2 },
    { name: 'Sunday', value: 6 },
    { name: 'Thursday', value: 3 },
  ];
  hours = [
    { name: '00', value: 0 },
    { name: '01', value: 1 },
    { name: '02', value: 2 },
    { name: '03', value: 3 },
    { name: '04', value: 4 },
    { name: '05', value: 5 },
    { name: '06', value: 6 },
    { name: '07', value: 7 },
    { name: '08', value: 8 },
    { name: '09', value: 9 },
    { name: '10', value: 10 },
    { name: '11', value: 11 },
    { name: '12', value: 12 }
  ];
  minutes = [
    { name: '00', value: 0 },
    { name: '05', value: 5 },
    { name: '10', value: 10 },
    { name: '15', value: 15 },
    { name: '20', value: 20 },
    { name: '25', value: 25 },
    { name: '30', value: 30 },
    { name: '35', value: 35 },
    { name: '40', value: 40 },
    { name: '45', value: 45 },
    { name: '50', value: 50 },
    { name: '55', value: 55 }
  ];
  meridiems = ['AM', 'PM'];
  daycheck: any = [];
  @LocalStorage('scope.organization_id') organizationId: number;
  @LocalStorage('scope.brand_id') brandId: number;
  @LocalStorage('scope.outlet_id') outletId: number;

  constructor(
    private reminderRepository: ReminderRepository,
    private userRepository: UsersRepository,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.loadReminder();
  }
  loadReminder() {
    this.activatedRoute.params
      .pipe(
        filter(data => data.id),
        map(data => data.id),
        switchMap(
          (id) => this.reminderRepository.find(id)
        )
      )
      .subscribe((reminder: Reminder) => {
        this.reminder = reminder;
        this.reminder.hour = this.reminder.timeHour;
        this.reminder.minute = this.reminder.timeMinute;
        this.reminder.meridiem = this.reminder.timeMeridiem;
        this.reminder.days.forEach(item => {
          this.days.forEach(item1 => {
            if (item == item1.value) {
              this.daycheck.push(item1);
            }
          });
        });
        this.minDate = new Date(this.reminder.startDate);
        this._outletId = this.reminder.outletId;
        this.loadUser();
      });
  }
  loadUser() {
    this.userRepository.all({ status: 1, pagination: 0, outlet_id: this._outletId })
      .subscribe(res => {
        this.users = res;
      })
  }
  checkDays(item?: any) {
    let index = this.daycheck.indexOf(item);
    if (index == -1) {
      this.daycheck.push(item);
      this.reminder.days.push(item.value);
    } else {
      this.daycheck.splice(index, 1);
      this.reminder.days.splice(index, 1);
    }
  }
  onSubmit() {
    this.reminderRepository.save(this.reminder)
      .subscribe((res) => {
        this.cancel();
      });
  }
  cancel() {
    this.router.navigate(['../..', 'list'], { relativeTo: this.activatedRoute });
  }
  getMaxLengthEndAfter() {
    let maxLength: number;

    switch (this.reminder.repeatType) {
      case 'daily':
      default:
        maxLength = 3;
        break;

      case 'weekly':
        maxLength = 2;
        break;

      case 'monthly':
        maxLength = 1;
        break;
    }

    return 3;
  }
  isDisabledEndAfter() {
    return this.reminder.repeatType === 'none';
  }
  calculateOccurrences() {
    let occurrenceDate: Moment;
    let endAfter: number;

    occurrenceDate = moment(this.reminder.startDate);
    endAfter = this.reminder.endAfter;

    if (endAfter && endAfter > 0) {
      switch (this.reminder.repeatType) {
        case 'daily':
          occurrenceDate = occurrenceDate.add(endAfter, 'days');
          break;

        case 'weekly':
          const day = moment().day();
          occurrenceDate = occurrenceDate.add(endAfter, 'weeks');

          if (this.reminder.days && this.reminder.days.length > 0) {
            const dayINeed = Math.max(...this.reminder.days);
            occurrenceDate = occurrenceDate.subtract(day - dayINeed, 'days');
          }

          break;

        case 'monthly':
          occurrenceDate = occurrenceDate.add(endAfter, 'months');
          break;

        case 'none':
        default:
          break;
      }
    }
    return occurrenceDate.toDate();
  }
  repeatReminder() {
    if (this.reminder.repeatType == 'none') {
      this.reminder.endAfter = 0;
    } else {
      this.reminder.endAfter = 1;
    }
  }
  changeOutlet(item) {
    this._outletId = item.join('');
    this.loadUser();
  }
}
