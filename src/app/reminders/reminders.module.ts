import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RemindersRoutingModule} from './reminders-routing.module';
import {ListRemindersComponent} from './list-reminders/list-reminders.component';
import {CreateReminderComponent} from './create-reminder/create-reminder.component';
import {EditReminderComponent} from './edit-reminder/edit-reminder.component';
import {SharedModule} from 'src/app/shared/shared.module';
import {FormsModule} from '@angular/forms';
import { ScopeComponent } from './scope/scope.component';
import { SentoComponentComponent } from './sento-component/sento-component.component';

@NgModule({
    declarations: [ListRemindersComponent, CreateReminderComponent, EditReminderComponent, ScopeComponent, SentoComponentComponent],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        RemindersRoutingModule
    ]
})
export class RemindersModule {
}
