import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedemptionSettingComponent } from './redemption-setting.component';

describe('RedemptionSettingComponent', () => {
  let component: RedemptionSettingComponent;
  let fixture: ComponentFixture<RedemptionSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedemptionSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedemptionSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
