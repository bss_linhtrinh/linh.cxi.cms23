import {Component, OnInit} from '@angular/core';
import {RedemptionSetting} from '../../shared/entities/redemption-setting';

@Component({
    selector: 'app-redemption-setting',
    templateUrl: './redemption-setting.component.html',
    styleUrls: ['./redemption-setting.component.scss']
})
export class RedemptionSettingComponent implements OnInit {
    redemptionSetting: RedemptionSetting = new RedemptionSetting();

    config = {
        canRemove: true
    };
    columns = [
        {title: 'Name of Product', name: 'name', is_read_only: true},
        {title: 'Point to redempt', name: 'point_to_redempt'},
    ];

    constructor() {
    }

    ngOnInit() {
    }

    cancel() {
    }

    save() {
    }
}
