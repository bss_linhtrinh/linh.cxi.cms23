import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CheckPermissionsGuard} from '../../shared/guards/check-permissions/check-permissions.guard';
import {ListCustomerLevelIconsComponent} from './list-customer-level-icons/list-customer-level-icons.component';


const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: 'list', component: ListCustomerLevelIconsComponent, data: {breadcrumb: 'List Customer Level Icons'}},
            {path: '', redirectTo: 'list', pathMatch: 'full'},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CustomerLevelIconsRoutingModule {
}
