import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CustomerLevelIconsRoutingModule} from './customer-level-icons-routing.module';
import {ListCustomerLevelIconsComponent} from './list-customer-level-icons/list-customer-level-icons.component';
import {CreateCustomerLevelIconComponent} from './create-customer-level-icon/create-customer-level-icon.component';
import {EditCustomerLevelIconComponent} from './edit-customer-level-icon/edit-customer-level-icon.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
    declarations: [
        ListCustomerLevelIconsComponent,
        CreateCustomerLevelIconComponent,
        EditCustomerLevelIconComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,

        CustomerLevelIconsRoutingModule
    ],
    entryComponents: [
        CreateCustomerLevelIconComponent,
        EditCustomerLevelIconComponent
    ]
})
export class CustomerLevelIconsModule {
}
