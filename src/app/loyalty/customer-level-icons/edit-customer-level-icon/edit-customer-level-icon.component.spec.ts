import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCustomerLevelIconComponent } from './edit-customer-level-icon.component';

describe('EditCustomerLevelIconComponent', () => {
  let component: EditCustomerLevelIconComponent;
  let fixture: ComponentFixture<EditCustomerLevelIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCustomerLevelIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCustomerLevelIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
