import {Component, OnInit} from '@angular/core';
import {CustomerLevel} from '../../../shared/entities/customer-level';
import {Brand} from '../../../shared/entities/brand';
import {CustomerLevelIcon} from '../../../shared/entities/customer-level-icon';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {CustomerLevelsRepository} from '../../../shared/repositories/customer-levels.repository';
import {BrandsRepository} from '../../../shared/repositories/brands.repository';
import {CustomerLevelIconsRepository} from '../../../shared/repositories/customer-level-icons.repository';
import {tap} from 'rxjs/operators';
import {LocalStorage} from 'ngx-webstorage';
import {Scope} from '../../../shared/types/scopes';

@Component({
    selector: 'app-edit-customer-level-icon',
    templateUrl: './edit-customer-level-icon.component.html',
    styleUrls: ['./edit-customer-level-icon.component.scss']
})
export class EditCustomerLevelIconComponent implements OnInit {
    customerLevels: CustomerLevel[] = [];
    brands: Brand[] = [];
    customerLevelIcon: CustomerLevelIcon = new CustomerLevelIcon();
    uploadFile: File;
    isHideApplicationScope: boolean;

    @LocalStorage('scope.brand_id') brandId: number;
    @LocalStorage('scope.current') currentScope: Scope;

    constructor(
        private ngbActiveModal: NgbActiveModal,
        private customerLevelsRepository: CustomerLevelsRepository,
        private brandsRepository: BrandsRepository,
        private customerLevelIconsRepository: CustomerLevelIconsRepository
    ) {
    }

    ngOnInit() {
        this.getApplicationScope();
        this.getCustomerLevels();
        this.getBrands();
    }

    getApplicationScope() {
        switch (this.currentScope) {
            case Scope.Organization:
                this.isHideApplicationScope = false;
                break;
            case Scope.Brand:
            default:
                this.isHideApplicationScope = true;
                if (this.brandId) {
                    this.customerLevelIcon.brand_id = this.brandId;
                }
                break;
            case Scope.Outlet:
                this.isHideApplicationScope = true;
                break;
        }
    }

    getCustomerLevels() {
        this.customerLevelsRepository.all()
            .pipe(tap((customerLevels: CustomerLevel[]) => this.customerLevels = customerLevels))
            .subscribe();
    }

    getBrands() {
        this.brandsRepository.all()
            .pipe(tap((brands: Brand[]) => this.brands = brands))
            .subscribe();
    }

    cancel() {
        this.ngbActiveModal.dismiss('cancel');
    }

    onFileChanged(file: any) {
        this.uploadFile = file;
    }

    onChangeBrand(brandId: number) {

    }

    reset() {
    }

    add() {
        this.customerLevelIconsRepository.create(this.customerLevelIcon)
            .pipe(
                tap(() => this.ngbActiveModal.close())
            )
            .subscribe();
    }
}
