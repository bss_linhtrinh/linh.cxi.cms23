import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCustomerLevelIconComponent } from './create-customer-level-icon.component';

describe('CreateCustomerLevelIconComponent', () => {
  let component: CreateCustomerLevelIconComponent;
  let fixture: ComponentFixture<CreateCustomerLevelIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCustomerLevelIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCustomerLevelIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
