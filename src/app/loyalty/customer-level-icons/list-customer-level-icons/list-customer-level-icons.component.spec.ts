import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCustomerLevelIconsComponent } from './list-customer-level-icons.component';

describe('ListCustomerLevelIconsComponent', () => {
  let component: ListCustomerLevelIconsComponent;
  let fixture: ComponentFixture<ListCustomerLevelIconsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCustomerLevelIconsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCustomerLevelIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
