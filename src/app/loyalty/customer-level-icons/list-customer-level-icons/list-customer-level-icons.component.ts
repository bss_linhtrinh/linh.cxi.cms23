import {Component, OnInit} from '@angular/core';
import {CxiGridConfig} from '../../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../../shared/components/cxi-grid/cxi-grid-column';
import {ActivatedRoute, Router} from '@angular/router';
import {CustomerLevelIcon} from '../../../shared/entities/customer-level-icon';
import {CustomerLevelIconsRepository} from '../../../shared/repositories/customer-level-icons.repository';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CreateCustomerLevelIconComponent} from '../create-customer-level-icon/create-customer-level-icon.component';
import {from, throwError} from 'rxjs';
import {catchError, filter} from 'rxjs/operators';
import {EditCustomerLevelIconComponent} from '../edit-customer-level-icon/edit-customer-level-icon.component';

@Component({
    selector: 'app-list-customer-level-icons',
    templateUrl: './list-customer-level-icons.component.html',
    styleUrls: ['./list-customer-level-icons.component.scss']
})
export class ListCustomerLevelIconsComponent implements OnInit {

    // table
    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        className: false,
        pagingServer: true,
        repository: this.customerLevelIconsRepository,
        actions: {
            onEdit: (customerLevelIcon: CustomerLevelIcon) => this.onEdit(customerLevelIcon),
            onDelete: (customerLevelIcon: CustomerLevelIcon) => this.onDelete(customerLevelIcon),
        }
    });

    cxiGridColumns = CxiGridColumn.from([
        {title: 'No.', name: 'id'},
        {title: 'badge images', avatar: 'image'},
        {title: 'level name', name: 'customer_level.name'}
    ]);

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private ngbModal: NgbModal,
        private customerLevelIconsRepository: CustomerLevelIconsRepository
    ) {
    }

    ngOnInit() {
    }

    onEdit(customerLevelIcon: CustomerLevelIcon) {
        const modalRef = this.ngbModal.open(EditCustomerLevelIconComponent, {centered: true, windowClass: 'customer-level-icons'});
        modalRef.componentInstance.customerLevelIcon = customerLevelIcon;
        return from(modalRef.result)
            .pipe(
                catchError((e) => throwError(e)),
                filter((isConfirmed: boolean) => isConfirmed)
            );
    }

    onDelete(customerLevelIcon: CustomerLevelIcon) {
        this.customerLevelIconsRepository.destroy(customerLevelIcon).subscribe();
    }

    addNewIcon() {
        const modalRef = this.ngbModal.open(CreateCustomerLevelIconComponent, {centered: true, windowClass: 'customer-level-icons'});
        return from(modalRef.result)
            .pipe(
                catchError((e) => throwError(e)),
                filter((isConfirmed: boolean) => isConfirmed)
            );
    }
}
