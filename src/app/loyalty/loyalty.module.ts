import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoyaltyRoutingModule} from './loyalty-routing.module';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {RewardsSettingComponent} from './rewards-setting/rewards-setting.component';
import {RedemptionSettingComponent} from './redemption-setting/redemption-setting.component';
import { MatTableModule } from '@angular/material/table';
import {RewardsSettingResolver} from './rewards-setting/rewards-setting.resolver';
import { RewardsCurrencyRatesComponent } from './rewards-setting/rewards-currency-rates/rewards-currency-rates.component';
import { ExchangeRateModalComponent } from './rewards-setting/exchange-rate-modal/exchange-rate-modal.component';

@NgModule({
    declarations: [
        RewardsSettingComponent,
        RedemptionSettingComponent,
        RewardsCurrencyRatesComponent,
        ExchangeRateModalComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        MatTableModule,
        LoyaltyRoutingModule,
    ],
    providers: [
        RewardsSettingResolver
    ],
    entryComponents: [
        ExchangeRateModalComponent
    ]
})
export class LoyaltyModule {
}
