import {Component, Inject, Input, OnInit, Optional, ViewChild} from '@angular/core';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {Condition} from '../../../shared/entities/condition';

@Component({
    selector: 'base-group-conditions',
    templateUrl: './base-group-conditions.component.html',
    styleUrls: ['./base-group-conditions.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: BaseGroupConditionsComponent, multi: true}
    ]
})
export class BaseGroupConditionsComponent extends ElementBase<Condition[][]> implements OnInit {

    private _name: string;
    private _label: string;

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        if (value) {
            this._label = value.replace('_', ' ');
        } else {
            this._label = '';
        }
    }

    @Input() public size = 'sm';

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onChange() {
        //
    }

    onAddNewGroup(index: number) {
        const newGroupCondition = [new Condition()];
        this.value.splice(index + 1, 0, newGroupCondition);
    }

    onRemoveGroup(index: number) {
        this.value.splice(index, 1);

        // check
        if (this.value.length === 0) {
            this.addDefault();
        }
    }

    addDefault() {
        const newGroupCondition = [new Condition()];
        this.value.push(newGroupCondition);
    }
}
