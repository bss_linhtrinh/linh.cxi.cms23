import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseGroupConditionsComponent } from './base-group-conditions.component';

describe('BaseGroupConditionsComponent', () => {
  let component: BaseGroupConditionsComponent;
  let fixture: ComponentFixture<BaseGroupConditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseGroupConditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseGroupConditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
