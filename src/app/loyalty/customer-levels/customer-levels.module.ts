import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CustomerLevelsRoutingModule} from './customer-levels-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {ListCustomerLevelsComponent} from './list-customer-levels/list-customer-levels.component';
import {CreateCustomerLevelComponent} from './create-customer-level/create-customer-level.component';
import {EditCustomerLevelComponent} from './edit-customer-level/edit-customer-level.component';
import {BaseConditionsComponent} from './base-conditions/base-conditions.component';
import { BaseConditionComponent } from './base-condition/base-condition.component';
import { BaseGroupConditionsComponent } from './base-group-conditions/base-group-conditions.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgSelectModule} from '@ng-select/ng-select';

@NgModule({
    declarations: [
        ListCustomerLevelsComponent,
        CreateCustomerLevelComponent,
        EditCustomerLevelComponent,
        BaseConditionsComponent,
        BaseConditionComponent,
        BaseGroupConditionsComponent
    ],
    imports: [
        CommonModule,
        NgbModule,
        NgSelectModule,
        FormsModule,
        SharedModule,
        CustomerLevelsRoutingModule
    ]
})
export class CustomerLevelsModule {
}
