import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CustomerLevel} from '../../../shared/entities/customer-level';
import {CustomerLevelsRepository} from '../../../shared/repositories/customer-levels.repository';
import {CxiGridConfig} from '../../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../../shared/components/cxi-grid/cxi-grid-column';
import {Brand} from '../../../shared/entities/brand';

@Component({
    selector: 'app-list-customer-levels',
    templateUrl: './list-customer-levels.component.html',
    styleUrls: ['./list-customer-levels.component.scss']
})
export class ListCustomerLevelsComponent implements OnInit {

    // table
    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        className: false,
        pagingServer: true,
        repository: this.customerLevelsRepository,
        actions: {
            onEdit: (customerLevel: CustomerLevel) => {
                this.onEdit(customerLevel);
            },
            onActivate: (customerLevel: CustomerLevel) => {
                this.onActivate(customerLevel);
            },
            onDeactivate: (customerLevel: CustomerLevel) => {
                this.onDeactivate(customerLevel);
            },
            onDelete: (customerLevel: CustomerLevel) => {
                this.onDelete(customerLevel);
            },
        }
    });
    cxiGridColumns = CxiGridColumn.from([
        {title: 'No.', name: 'id'},
        {title: 'level name', name: 'name'},
        {title: 'description', name: 'description'},
    ]);

    constructor(private router: Router,
                private route: ActivatedRoute,
                private customerLevelsRepository: CustomerLevelsRepository) {
    }

    ngOnInit() {
    }

    onEdit(customerLevel: CustomerLevel) {
        this.router.navigate(['..', customerLevel.id, 'edit'], {relativeTo: this.route});
    }

    onActivate(customerLevel: CustomerLevel) {
        this.customerLevelsRepository.activate(customerLevel)
            .subscribe();
    }

    onDeactivate(customerLevel: CustomerLevel) {
        this.customerLevelsRepository.deactivate(customerLevel)
            .subscribe();
    }

    onDelete(customerLevel: CustomerLevel) {
        this.customerLevelsRepository.destroy(customerLevel)
            .subscribe();
    }
}
