import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCustomerLevelsComponent } from './list-customer-levels.component';

describe('ListCustomerLevelsComponent', () => {
  let component: ListCustomerLevelsComponent;
  let fixture: ComponentFixture<ListCustomerLevelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCustomerLevelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCustomerLevelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
