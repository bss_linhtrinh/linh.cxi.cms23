import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListCustomerLevelsComponent} from './list-customer-levels/list-customer-levels.component';
import {CreateCustomerLevelComponent} from './create-customer-level/create-customer-level.component';
import {EditCustomerLevelComponent} from './edit-customer-level/edit-customer-level.component';
import {CheckPermissionsGuard} from '../../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: '', redirectTo: 'list', pathMatch: 'full'},
            {
                path: 'list',
                component: ListCustomerLevelsComponent,
                canActivate: [CheckPermissionsGuard],
                data: {breadcrumb: 'List Customer Levels'}
            },
            {
                path: 'create',
                component: CreateCustomerLevelComponent,
                canActivate: [CheckPermissionsGuard],
                data: {breadcrumb: 'Create new Customer Level'}
            },
            {
                path: ':id/edit',
                component: EditCustomerLevelComponent,
                canActivate: [CheckPermissionsGuard],
                data: {breadcrumb: 'Edit Customer Levels'}
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CustomerLevelsRoutingModule {
}
