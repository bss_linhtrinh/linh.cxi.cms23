import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCustomerLevelComponent } from './edit-customer-level.component';

describe('EditCustomerLevelComponent', () => {
  let component: EditCustomerLevelComponent;
  let fixture: ComponentFixture<EditCustomerLevelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCustomerLevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCustomerLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
