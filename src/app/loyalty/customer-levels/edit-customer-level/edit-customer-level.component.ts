import {Component, OnInit} from '@angular/core';
import {CustomerLevel} from '../../../shared/entities/customer-level';
import {CustomerLevelsRepository} from '../../../shared/repositories/customer-levels.repository';
import {ActivatedRoute, Router} from '@angular/router';
import {filter, map, switchMap, tap} from 'rxjs/operators';
import {UserScope} from '../../../shared/entities/user-scope';
import {of} from 'rxjs';
import {Brand} from '../../../shared/entities/brand';
import {ScopeService} from '../../../shared/services/scope/scope.service';
import {BrandsRepository} from '../../../shared/repositories/brands.repository';

@Component({
    selector: 'app-edit-customer-level',
    templateUrl: './edit-customer-level.component.html',
    styleUrls: ['./edit-customer-level.component.scss']
})
export class EditCustomerLevelComponent implements OnInit {
    customerLevel: CustomerLevel = new CustomerLevel();
    brands: Brand[] = [];
    customerAvailable = [];
    isBrand = false;

    constructor(private customerLevelsRepository: CustomerLevelsRepository,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                private scopeService: ScopeService,
                private brandsRepository: BrandsRepository) {
    }

    ngOnInit() {
        this.getCustomerLevelDetail();
        this.getBrands();
        this.getListCustomerLevelAvailable();
        this.setDefaultCustomerLevel();
    }

    getCustomerLevelDetail() {
        this.activatedRoute.params
            .pipe(
                filter((data) => data.id),
                map((data) => data.id),
                switchMap(
                    id => this.customerLevelsRepository.find(id)
                )
            )
            .subscribe(
                (customerLevel: CustomerLevel) => {
                    this.customerLevel = customerLevel;
                }
            );
    }

    getBrands() {
        this.scopeService.getUserScope$()
            .pipe(
                switchMap(
                    (userScope: UserScope) => {
                        if (userScope && userScope.brand && userScope.brand.id) {
                            this.isBrand = true;
                            return of([userScope.brand]);
                        } else {
                            return this.brandsRepository.all();
                        }
                    }
                )
            )
            .subscribe(
                (brands: Brand[]) => {
                    this.brands = brands;
                }
            );
    }

    getListCustomerLevelAvailable() {
        this.customerLevelsRepository.getCustomerLevelAvailable()
            .subscribe(
                (customerAvailable: []) => {
                    this.customerAvailable = customerAvailable;
                    this.setDefaultCustomerLevel();
                },
                (error) => {
                }
            );
    }

    back() {
        this.router.navigate(['../../list'], {relativeTo: this.activatedRoute});
    }

    cancel() {
        this.back();
    }

    save() {
        this.customerLevelsRepository.save(this.customerLevel)
            .pipe(
                tap(
                    () => this.back()
                )
            )
            .subscribe();
    }

    setDefaultCustomerLevel () {
        if (this.customerLevel.previous_id) {
            this.customerAvailable.push(this.customerLevel.previous_level);
        } else {
            return ;
        }
    }
}
