import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCustomerLevelComponent } from './create-customer-level.component';

describe('CreateCustomerLevelComponent', () => {
  let component: CreateCustomerLevelComponent;
  let fixture: ComponentFixture<CreateCustomerLevelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCustomerLevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCustomerLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
