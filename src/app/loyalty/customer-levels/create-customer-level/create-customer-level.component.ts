import {Component, OnInit} from '@angular/core';
import {CustomerLevel} from '../../../shared/entities/customer-level';
import {switchMap, tap} from 'rxjs/operators';
import {CustomerLevelsRepository} from '../../../shared/repositories/customer-levels.repository';
import {ActivatedRoute, Router} from '@angular/router';
import {UserScope} from '../../../shared/entities/user-scope';
import {of} from 'rxjs';
import {Brand} from '../../../shared/entities/brand';
import {ScopeService} from '../../../shared/services/scope/scope.service';
import {BrandsRepository} from '../../../shared/repositories/brands.repository';
import {Order} from '../../../shared/entities/order';

@Component({
    selector: 'app-create-customer-level',
    templateUrl: './create-customer-level.component.html',
    styleUrls: ['./create-customer-level.component.scss']
})
export class CreateCustomerLevelComponent implements OnInit {
    customerLevel: CustomerLevel = new CustomerLevel();
    brands: Brand[] = [];
    customerAvailable = [];
    isBrand = false;

    constructor(private customerLevelsRepository: CustomerLevelsRepository,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                private scopeService: ScopeService,
                private brandsRepository: BrandsRepository) {
    }

    ngOnInit() {
        this.getBrands();
        this.getListCustomerLevelAvailable();
    }

    back() {
        this.router.navigate(['../list'], {relativeTo: this.activatedRoute});
    }

    cancel() {
        this.back();
    }

    getBrands() {
        this.scopeService.getUserScope$()
            .pipe(
                switchMap(
                    (userScope: UserScope) => {
                        if (userScope && userScope.brand && userScope.brand.id) {
                            this.customerLevel.brand_id = userScope.brand.id;
                            this.isBrand = true;
                            return of([userScope.brand]);
                        } else {
                            return this.brandsRepository.all();
                        }
                    }
                )
            )
            .subscribe(
                (brands: Brand[]) => {
                    this.brands = brands;
                }
            );
    }

    getListCustomerLevelAvailable() {
        this.customerLevelsRepository.getCustomerLevelAvailable()
            .subscribe(
                (customerAvailables: []) => {
                    this.customerAvailable = customerAvailables;
                },
                (error) => {
                }
            );
    }

    save() {
        this.customerLevelsRepository.save(this.customerLevel)
            .pipe(
                tap(
                    () => this.back()
                )
            )
            .subscribe();
    }
}
