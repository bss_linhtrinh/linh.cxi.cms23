import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseConditionComponent } from './base-condition.component';

describe('BaseConditionComponent', () => {
  let component: BaseConditionComponent;
  let fixture: ComponentFixture<BaseConditionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseConditionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
