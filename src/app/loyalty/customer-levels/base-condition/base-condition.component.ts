import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {Condition} from '../../../shared/entities/condition';
import {Attributes} from '../attributes';
import {Operators} from '../operator';

@Component({
    selector: 'base-condition',
    templateUrl: './base-condition.component.html',
    styleUrls: ['./base-condition.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: BaseConditionComponent, multi: true}
    ]
})
export class BaseConditionComponent extends ElementBase<Condition> implements OnInit {

    private _name: string;
    private _label: string;

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        if (value) {
            this._label = value.replace('_', ' ');
        } else {
            this._label = '';
        }
    }

    @Input() public size = 'sm';
    @Input() public width = '';

    @Output() add = new EventEmitter();
    @Output() remove = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    btnDisabled: boolean;
    btnLoading: boolean;
    attributes: any[];
    operators: any[];

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
        this.attributes = Attributes;
        this.operators = Operators;
    }

    onChange() {
        //
    }

    addItem() {
        this.add.emit();
    }

    removeItem() {
        this.remove.emit();
    }
}
