export const Operators = [
    {id: 1, name: '>', value: '>'},
    {id: 2, name: '<', value: '<'},
    {id: 3, name: '>=', value: '>='},
    {id: 4, name: '<=', value: '<='},
    {id: 5, name: '=', value: '='},
];
