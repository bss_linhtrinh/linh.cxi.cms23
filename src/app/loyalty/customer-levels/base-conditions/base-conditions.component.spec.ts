import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseConditionsComponent } from './base-condition.component';

describe('BaseConditionComponent', () => {
  let component: BaseConditionsComponent;
  let fixture: ComponentFixture<BaseConditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseConditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseConditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
