import {Component, EventEmitter, Inject, Input, OnChanges, OnInit, Optional, Output, SimpleChanges, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {Condition} from '../../../shared/entities/condition';
import {BaseFilterCriteria} from '../../../shared/entities/base-filter-criteria';

@Component({
    selector: 'base-conditions',
    templateUrl: './base-conditions.component.html',
    styleUrls: ['./base-conditions.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: BaseConditionsComponent, multi: true}
    ]
})
export class BaseConditionsComponent extends ElementBase<Condition[]> implements OnInit {

    private _name: string;
    private _label: string;

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        if (value) {
            this._label = value.replace('_', ' ');
        } else {
            this._label = '';
        }
    }

    @Input() public size = 'sm';
    @Input() public width = '';

    @Output() addGroup = new EventEmitter();
    @Output() removeGroup = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onChange() {
        //
    }

    onAdd(index: number) {
        const newCondition = new Condition();
        this.value.splice(index + 1, 0, newCondition);
    }

    onRemove(index: number) {
        this.value.splice(index, 1);

        // check
        if (this.value.length === 0) {
            this.removeThisGroup();
        }
    }

    addNewGroup() {
        this.addGroup.emit();
    }

    removeThisGroup() {
        this.removeGroup.emit();
    }
}
