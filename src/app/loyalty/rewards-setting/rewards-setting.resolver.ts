import {Injectable} from '@angular/core';
import {Resolve} from '@angular/router';
import {RewardSetting} from '../../shared/entities/reward-setting';
import {RewardsSettingRepository} from '../../shared/repositories/rewards-setting.repository';

@Injectable()
export class RewardsSettingResolver implements Resolve<RewardSetting> {

    constructor(public rewardsSettingRepository: RewardsSettingRepository) {
    }

    resolve() {
        return this.rewardsSettingRepository.all();
    }

}
