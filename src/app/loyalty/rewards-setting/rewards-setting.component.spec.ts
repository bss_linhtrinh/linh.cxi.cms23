import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RewardsSettingComponent } from './rewards-setting.component';

describe('RewardsSettingComponent', () => {
  let component: RewardsSettingComponent;
  let fixture: ComponentFixture<RewardsSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RewardsSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RewardsSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
