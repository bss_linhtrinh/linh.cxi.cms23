import {Component, OnInit} from '@angular/core';
import {Currency} from '../../shared/entities/currency';
import {CurrenciesRepository} from '../../shared/repositories/currencies.repository';
import {RewardSetting} from '../../shared/entities/reward-setting';
import {RewardsSettingRepository} from '../../shared/repositories/rewards-setting.repository';
import {Outlet} from '../../shared/entities/outlets';
import {OutletsRepository} from '../../shared/repositories/outlets.repository';
import {UtilitiesService} from '../../shared/services/utilities/utilities.service';
import {MessagesService} from '../../messages/messages.service';
import {NgForm} from '@angular/forms';
import {map, switchMap, tap, toArray} from 'rxjs/operators';
import {ScopeService} from '../../shared/services/scope/scope.service';
import {UserScope} from '../../shared/entities/user-scope';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ExchangeRateModalComponent} from './exchange-rate-modal/exchange-rate-modal.component';
import {ExchangeRate} from '../../shared/entities/exchange-rate';
import {from} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {CurrencyRate} from '../../shared/entities/currency-rate';
import {SessionStorage} from 'ngx-webstorage';
import {CustomerLevel} from '../../shared/entities/customer-level';
import {CustomerLevelsRepository} from '../../shared/repositories/customer-levels.repository';
import {NotificationService} from '../../shared/services/noitification/notification.service';

@Component({
    selector: 'app-rewards-setting',
    templateUrl: './rewards-setting.component.html',
    styleUrls: ['./rewards-setting.component.scss']
})
export class RewardsSettingComponent implements OnInit {
    @SessionStorage('currencies') currencies: Currency[];
    rewardsSetting: RewardSetting = new RewardSetting();
    cloneRewardSetting: RewardSetting;
    outlets: Outlet[];
    customerLevels: CustomerLevel[];

    config = {
        canRemove: false
    };
    columns = [
        {title: 'CUS. LEVEL', name: 'name', is_read_only: true},
        {title: 'Accumulated rate', name: 'accumulated_rate'},
        {title: 'Maximum amount', name: 'maximum_amount'},
    ];
    pointAccumulatedOptions = [
        {name: 'All', value: 'all_stores'},
        {name: 'Specific outlet', value: 'specific_stores'}
    ];
    alwaysOneTimeOptions = [
        {name: 'Always', value: 'always'},
        {name: 'One time', value: 'one-time'}
    ];

    constructor(
        private currenciesRepository: CurrenciesRepository,
        private rewardsSettingRepository: RewardsSettingRepository,
        private outletsRepository: OutletsRepository,
        private utilitiesService: UtilitiesService,
        private messagesService: MessagesService,
        private userScopeService: ScopeService,
        private ngbModal: NgbModal,
        private translateService: TranslateService,
        private customerLevelsRepository: CustomerLevelsRepository,
        private notificationService: NotificationService
    ) {
    }

    ngOnInit() {
        this.getData();
        this.getRewardsSetting();
        this.translate();
    }

    getRewardsSetting() {
        this.rewardsSettingRepository.all()
            .pipe(
                map(
                    (rewardsSettings: RewardSetting[]) => {
                        if (rewardsSettings instanceof Array) {
                            if (rewardsSettings.length > 0) {
                                return rewardsSettings[0];
                            }
                            return null;
                        }
                        return rewardsSettings;
                    }
                ),
                map((rewardsSetting: RewardSetting) => {
                    if (rewardsSetting) {
                        rewardsSetting.currency_rates.map(
                            (currencyRate: CurrencyRate) => {
                                const currency = this.currencies.find(x => x.code === currencyRate.currency_code);
                                if (currency) {
                                    currencyRate.currency_id = currency.id;
                                }
                                return currencyRate;
                            }
                        );
                    }
                    return rewardsSetting;
                })
            )
            .subscribe(
                (rewardsSetting: RewardSetting) => {
                    if (rewardsSetting) {
                        this.rewardsSetting = rewardsSetting;
                        this.cloneRewardSetting = this.utilitiesService.cloneDeep(this.rewardsSetting);
                    }
                }
            );
    }

    getData() {
        this.getCurrencies();
        this.getOutlets();
        this.getUserScope();
        this.getCustomerLevels();
    }

    translate() {
        from(this.pointAccumulatedOptions)
            .pipe(
                switchMap(
                    (option: { name: string, value: string }) =>
                        this.translateService.get(`scope.${option.name}`)
                            .pipe(
                                map(
                                    (translatedName: string) => {
                                        option.name = translatedName;
                                        return option;
                                    }
                                )
                            )
                ),
                toArray(),
                tap(
                    (newPointAccumulatedOptions: any[]) => this.pointAccumulatedOptions = newPointAccumulatedOptions
                )
            )
            .subscribe();

        from(this.alwaysOneTimeOptions)
            .pipe(
                switchMap(
                    (option: { name: string, value: string }) =>
                        this.translateService.get(`Loyalty Program.${option.name}`)
                            .pipe(
                                map(
                                    (translatedName: string) => {
                                        option.name = translatedName;
                                        return option;
                                    }
                                )
                            )
                ),
                toArray(),
                tap(
                    (newAlwaysOneTimeOptions: any[]) => this.alwaysOneTimeOptions = newAlwaysOneTimeOptions
                )
            )
            .subscribe();
    }

    getCurrencies() {
        this.currenciesRepository.all()
            .pipe(
                tap(
                    (currencies: Currency[]) => {
                        this.currencies = currencies;
                        if (
                            currencies &&
                            currencies.length > 0 &&
                            this.rewardsSetting &&
                            this.rewardsSetting.currency_rates &&
                            this.rewardsSetting.currency_rates.length === 1 &&
                            !this.rewardsSetting.currency_rates[0].currency_id ||
                            !this.rewardsSetting.currency_rates[0].currency_code
                        ) {
                            this.rewardsSetting.currency_rates[0].currency_id = currencies[0].id;
                            this.rewardsSetting.currency_rates[0].currency_code = currencies[0].code;
                        }
                    }
                )
            )
            .subscribe(
            );
    }

    getOutlets() {
        this.outletsRepository.all()
            .subscribe(
                (outlets: Outlet[]) => this.outlets = outlets
            );
    }

    getUserScope() {
        this.userScopeService.getUserScope$()
            .subscribe(
                (userScope: UserScope) => {
                    if (userScope.brand && userScope.brand.id) {
                        if (!this.rewardsSetting.brand) {
                            this.rewardsSetting.brand = userScope.brand;
                        }
                        if (!this.rewardsSetting.brand_id) {
                            this.rewardsSetting.brand_id = userScope.brand.id;
                        }
                    }
                }
            );
    }

    getCustomerLevels() {
        this.customerLevelsRepository.all()
            .pipe(
                tap((customerLevels: CustomerLevel[]) => this.customerLevels = customerLevels)
            )
            .subscribe();
    }

    cancel(form: NgForm) {
        this.messagesService.discardChanges()
            .subscribe(
                () => {
                    form.reset();
                    this.rewardsSetting = this.utilitiesService.cloneDeep(this.cloneRewardSetting);
                }
            );
    }

    validate() {
        const msg = [];
        this.rewardsSetting.currency_rates.forEach(
            (currencyRate: CurrencyRate) => {
                if (!currencyRate.currency_id) {
                    msg.push(`rewards_currency_id_required`);
                }
                if (!currencyRate.amount) {
                    msg.push(`rewards_amount_required`);
                }
            }
        );
        const valid = !(msg && msg.length > 0);
        if (!valid) {
            this.notificationService.error(msg);
        }
        return valid;
    }

    save() {
        if (!this.validate()) {
            return;
        }
        this.rewardsSettingRepository.save(this.rewardsSetting)
            .pipe(
                tap(() => this.cloneRewardSetting = this.utilitiesService.cloneDeep(this.rewardsSetting))
            )
            .subscribe(
            );
    }

    canDeactivate() {
        if (this.isChangedData()) {
            return this.messagesService.unsavedChanges();
        }
        return true;
    }

    isChangedData(): boolean {
        return !this.utilitiesService.isEqual(this.rewardsSetting, this.cloneRewardSetting);
    }

    openModalAdd() {
        const availableCustomerLevels = this.getAvailableCustomerLevels();
        if (availableCustomerLevels &&
            availableCustomerLevels.length > 0) {
            const modalRef = this.ngbModal.open(ExchangeRateModalComponent, {windowClass: 'exchange-modal'});
            modalRef.componentInstance.customerLevels = availableCustomerLevels;
            modalRef.result
                .then(
                    (exchangeRate: ExchangeRate) => this.addItem(exchangeRate)
                );
        }
    }

    getAvailableCustomerLevels() {
        return this.customerLevels.filter(c => {
            return this.rewardsSetting.customer_level_exchange_rate.findIndex(exchange_rate => exchange_rate.name === c.name) === -1;
        });
    }

    addItem(exchangeRate) {
        const obj = this.rewardsSetting.customer_level_exchange_rate.filter(function (item) {
            return item.name === exchangeRate.name;
        });
        if (!obj[0]) {
            this.rewardsSetting.customer_level_exchange_rate.push(exchangeRate);
        }
    }

    removeItem(id) {
        this.rewardsSetting.customer_level_exchange_rate.splice(id, 1);
    }
}
