import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Currency} from '../../../shared/entities/currency';
import {CurrencyRate} from '../../../shared/entities/currency-rate';
import {SessionStorage} from 'ngx-webstorage';

@Component({
    selector: 'app-rewards-currency-rates',
    templateUrl: './rewards-currency-rates.component.html',
    styleUrls: ['./rewards-currency-rates.component.scss']
})
export class RewardsCurrencyRatesComponent implements OnInit {

    @Input() name: string;
    @Input() currencies: Currency[];
    @Input() currencyRate: CurrencyRate = new CurrencyRate();

    @Output() currencyRateChange = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

    onChangeCurrency(currencyCode: string) {
        const currency = this.currencies.find(c => c.code === currencyCode);
        if (currency) {
            this.currencyRate.currency_id = currency.id;
        }
    }
}
