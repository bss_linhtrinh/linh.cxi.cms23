import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RewardsCurrencyRatesComponent } from './rewards-currency-rates.component';

describe('RewardsCurrencyRatesComponent', () => {
  let component: RewardsCurrencyRatesComponent;
  let fixture: ComponentFixture<RewardsCurrencyRatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RewardsCurrencyRatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RewardsCurrencyRatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
