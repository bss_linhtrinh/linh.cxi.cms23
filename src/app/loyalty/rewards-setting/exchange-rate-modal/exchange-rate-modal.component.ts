import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {tap} from 'rxjs/operators';
import {CustomerLevel} from '../../../shared/entities/customer-level';
import {CustomerLevelsRepository} from '../../../shared/repositories/customer-levels.repository';
import {ExchangeRate} from '../../../shared/entities/exchange-rate';

@Component({
    selector: 'app-exchange-rate-modal',
    templateUrl: './exchange-rate-modal.component.html',
    styleUrls: ['./exchange-rate-modal.component.scss']
})
export class ExchangeRateModalComponent implements OnInit {
    @Input() customerLevels: CustomerLevel[];
    exchangeRate: ExchangeRate = new ExchangeRate();

    constructor(
        private ngbActiveModal: NgbActiveModal
    ) {
    }

    ngOnInit() {

    }

    no() {
        this.ngbActiveModal.close(false);
    }

    yes() {
        this.ngbActiveModal.close(this.exchangeRate);
    }

    cancel() {
        this.ngbActiveModal.dismiss('cancel');
    }

}
