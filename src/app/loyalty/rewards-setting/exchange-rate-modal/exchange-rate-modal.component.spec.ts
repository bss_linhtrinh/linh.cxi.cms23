import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExchangeRateModalComponent } from './exchange-rate-modal.component';

describe('ExchangeRateModalComponent', () => {
  let component: ExchangeRateModalComponent;
  let fixture: ComponentFixture<ExchangeRateModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExchangeRateModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExchangeRateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
