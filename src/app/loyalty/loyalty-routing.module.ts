import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CheckPermissionsGuard} from '../shared/guards/check-permissions/check-permissions.guard';
import {RewardsSettingComponent} from './rewards-setting/rewards-setting.component';
import {RedemptionSettingComponent} from './redemption-setting/redemption-setting.component';
import {CanDeactivateGuard} from '../shared/guards/can-deactivate/can-deactivate.guard';

const routes: Routes = [
    {path: '', redirectTo: 'customer-levels', pathMatch: 'full'},
    {
        path: 'customer-levels',
        loadChildren: () => import('./customer-levels/customer-levels.module').then(mod => mod.CustomerLevelsModule),
        data: {breadcrumb: 'Customer Levels'}
    },
    {
        path: 'customer-level-icons',
        loadChildren: () => import('./customer-level-icons/customer-level-icons.module').then(mod => mod.CustomerLevelIconsModule),
        data: {breadcrumb: 'Customer Level Icons'}
    },
    {
        path: 'redemption-setting',
        canActivate: [CheckPermissionsGuard],
        component: RedemptionSettingComponent,
        data: {breadcrumb: 'Redemption Setting'}
    },
    {
        path: 'rewards-setting',
        canActivate: [CheckPermissionsGuard],
        canDeactivate: [CanDeactivateGuard],
        component: RewardsSettingComponent,
        data: {breadcrumb: 'Rewards Setting'}
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LoyaltyRoutingModule {
}
