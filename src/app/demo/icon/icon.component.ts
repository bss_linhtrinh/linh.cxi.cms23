import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-icon',
    templateUrl: './icon.component.html',
    styleUrls: ['./icon.component.scss']
})
export class IconComponent implements OnInit {

    icons = [
        {name: 'icon-add-new', html: `<i class="icon-add-new"></i>`},
        {name: 'icon-arrow-down', html: `<i class="icon-arrow-down"></i>`},
        {name: 'icon-arrow-left', html: `<i class="icon-arrow-left"></i>`},
        {name: 'icon-navigation-left', html: `<i class="icon-navigation-left"></i>`},
        {name: 'icon-navigation-right', html: `<i class="icon-navigation-right"></i>`},
        {name: 'icon-navigation-up', html: `<i class="icon-navigation-up"></i>`},
        {name: 'icon-chevron-left', html: `<i class="icon-chevron-left"></i>`},
        {name: 'icon-chevron-right', html: `<i class="icon-chevron-right"></i>`},
        {name: 'icon-chevron-up', html: `<i class="icon-chevron-up"></i>`},
        {name: 'icon-chevron-down', html: `<i class="icon-chevron-down"></i>`},
        {name: 'icon-close', html: `<i class="icon-close"></i>`},
        {name: 'icon-checkbox-blank', html: `<i class="icon-checkbox-blank"></i>`},
        {name: 'icon-checkbox', html: `<i class="icon-checkbox"></i>`},
        {name: 'icon-checkbox-minus', html: `<i class="icon-checkbox-minus"></i>`},
        {name: 'icon-brand', html: `<i class="icon-brand"></i>`},
        {name: 'icon-organization', html: `<i class="icon-organization"></i>`},
        {name: 'icon-outlet', html: `<i class="icon-outlet"></i>`},
        {name: 'icon-product', html: `<i class="icon-product"></i>`},
        {name: 'icon-customer', html: `<i class="icon-customer"></i>`},
        {name: 'icon-dashboard', html: `<i class="icon-dashboard"></i>`},
        {name: 'icon-calendar', html: `<i class="icon-calendar"></i>`},
        {name: 'icon-delete', html: `<i class="icon-delete"></i>`},
        {name: 'icon-diamond', html: `<i class="icon-diamond"></i>`},
        {name: 'icon-silver-gold', html: `<i class="icon-silver-gold"></i>`},
        {name: 'icon-edit', html: `<i class="icon-edit"></i>`},
        {name: 'icon-filter', html: `<i class="icon-filter"></i>`},
        {name: 'icon-image', html: `<i class="icon-image"></i>`},
        {name: 'icon-increase', html: `<i class="icon-increase"></i>`},
        {name: 'icon-lock', html: `<i class="icon-lock"></i>`},
        {name: 'icon-loyalty', html: `<i class="icon-loyalty"></i>`},
        {name: 'icon-menu', html: `<i class="icon-menu"></i>`},
        {name: 'icon-more', html: `<i class="icon-more"></i>`},
        {name: 'icon-new-user', html: `<i class="icon-new-user"></i>`},
        {name: 'icon-notification', html: `<i class="icon-notification"></i>`},
        {name: 'icon-order', html: `<i class="icon-order"></i>`},
        {name: 'icon-promotion', html: `<i class="icon-promotion"></i>`},
        {name: 'icon-reply', html: `<i class="icon-reply"></i>`},
        {name: 'icon-search', html: `<i class="icon-search"></i>`},
        {name: 'icon-settings', html: `<i class="icon-settings"></i>`},
        {name: 'icon-trash', html: `<i class="icon-trash"></i>`},
        {name: 'icon-up-level', html: `<i class="icon-up-level"></i>`},
        {name: 'icon-view', html: `<i class="icon-view"></i>`},
        {name: 'icon-view-close', html: `<i class="icon-view-close"></i>`},
        {name: 'icon-window', html: `<i class="icon-window"></i>`},
        {name: 'icon-facebook', html: `<i class="icon-facebook"></i>`},
        {name: 'icon-instagram', html: `<i class="icon-instagram"></i>`},
        {name: 'icon-active', html: `<i class="icon-active"></i>`},
        {name: 'icon-inactive', html: `<i class="icon-inactive"></i>`},
        {name: 'icon-auto', html: `<i class="icon-auto"></i>`},
        {name: 'icon-voucher', html: `<i class="icon-voucher"></i>`},
        {name: 'icon-gift', html: `<i class="icon-gift"></i>`},
        {name: 'icon-star', html: `<i class="icon-star"></i>`},
        {name: 'icon-star-o', html: `<i class="icon-star-o"></i>`},
        {name: 'icon-check', html: `<i class="icon-check"></i>`},
        {name: 'icon-report', html: `<i class="icon-report"></i>`},
        {name: 'icon-print', html: `<i class="icon-print"></i>`},
        {name: 'icon-mobile', html: `<i class="icon-mobile"></i>`},
        {name: 'icon-pos', html: `<i class="icon-pos"></i>`},
        {name: 'icon-user-active', html: `<span class="icon-user-active"><span class="path1"></span><span class="path2"></span></span>`},
        {
            name: 'icon-user-inactive',
            html: `<span class="icon-user-inactive"><span class="path1"></span><span class="path2"></span></span>`
        },
        {name: 'icon-history', html: `<i class="icon-history"></i>`},
        {name: 'icon-location', html: `<i class="icon-location"></i>`},
        {name: 'icon-info', html: `<i class="icon-info"></i>`},
    ];

    constructor() {
    }

    ngOnInit() {
        this.initIcons();
    }

    initIcons() {
        this.icons.map(
            (icon: any) => {
                icon.collapsed = true;
                return icon;
            }
        );
    }
}
