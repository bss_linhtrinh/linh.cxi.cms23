import {Component, OnInit} from '@angular/core';
import {Product} from '../../shared/entities/product';
import {Category} from '../../shared/entities/category';
import {CategoriesRepository} from '../../shared/repositories/categories.repository';
import {pipe} from 'rxjs';
import {tap} from 'rxjs/operators';
import {UsersRepository} from '../../shared/repositories/users.repository';
import {User} from '../../shared/entities/user';

@Component({
    selector: 'app-selection',
    templateUrl: './selection.component.html',
    styleUrls: ['./selection.component.scss']
})
export class SelectionComponent implements OnInit {
    selectionProductIds: number[] = [];

    baseSelectItems = [
        {id: 1, name: 'The golden Spoon 1'},
        {id: 2, name: 'The golden Spoon 2'},
        {id: 3, name: 'The golden Spoon 3'},
        {id: 4, name: 'The golden Spoon 4'},
        {id: 5, name: 'The golden Spoon 5'}
    ];
    baseSelectTwoColumns: number[] = [1, 3];


    baseSelectDemo = 1;
    baseMultiselectDemo = [1];

    // cxi select simple
    cxiSelectSimpleDemo: number[] = [];
    cxiSelectSimpleDemo2: number[] = [];
    cxiSelectSimpleDemo3: number[] = [3, 11, 1];
    selectedUsers: number[] = [];

    categories: Category[];
    users: User[];

    constructor(
        private categoriesRepository: CategoriesRepository,
        private usersRepository: UsersRepository
    ) {
    }

    ngOnInit() {
        this.getCategories();
        this.getUsers();
    }

    getCategories() {
        this.categoriesRepository.all()
            .pipe(
                tap(
                    (categories: Category[]) => this.categories = categories
                )
            )
            .subscribe();
    }

    getUsers() {
        this.usersRepository.all()
            .pipe(
                tap(
                    (users: User[]) => this.users = users
                )
            )
            .subscribe();
    }
}
