import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-colors',
    templateUrl: './colors.component.html',
    styleUrls: ['./colors.component.scss']
})
export class ColorsComponent implements OnInit {

    colors = [
        // primary
        [
            {name: 'Primary 900', hex: '#171C41', rgb: 'RGB 23/28/65'},
            {name: 'Primary 800', hex: '#20275a', rgb: 'RGB 23/28/65'},
            {name: 'Primary 700', hex: '#293273', rgb: 'RGB 23/28/65'},
            {name: 'Primary 600', hex: '#3b48a5', rgb: 'RGB 23/28/65'},
            {name: 'Primary 500', hex: '#3b48a5', rgb: 'RGB 23/28/65'},
            {name: 'Primary 400', hex: '#5e6bc6', rgb: 'RGB 23/28/65'},
            {name: 'Primary 300', hex: '#7782cf', rgb: 'RGB 23/28/65'},
            {name: 'Primary 200', hex: '#9099d8', rgb: 'RGB 23/28/65'},
            {name: 'Primary 100', hex: '#a9b0e1', rgb: 'RGB 23/28/65'},
            {name: 'Primary 050', hex: '#c2c7e9', rgb: 'RGB 23/28/65'},
        ],
        // accent
        [
            {name: 'Accent 900', hex: '#063243', rgb: 'RGB 23/28/65'},
            {name: 'Accent 800', hex: '#094962', rgb: 'RGB 23/28/65'},
            {name: 'Accent 700', hex: '#0c6081', rgb: 'RGB 23/28/65'},
            {name: 'Accent 600', hex: '#0F77A0', rgb: 'RGB 23/28/65'},
            {name: 'Accent 500', hex: '#15a5de', rgb: 'RGB 23/28/65'},
            {name: 'Accent 400', hex: '#49bfee', rgb: 'RGB 23/28/65'},
            {name: 'Accent 300', hex: '#68caf1', rgb: 'RGB 23/28/65'},
            {name: 'Accent 200', hex: '#87d5f4', rgb: 'RGB 23/28/65'},
            {name: 'Accent 100', hex: '#a6e0f7', rgb: 'RGB 23/28/65'},
            {name: 'Accent 050', hex: '#c5ebfa', rgb: 'RGB 23/28/65'},
        ],
        // blue
        [
            {name: 'Blue 900', hex: '#273ECD', rgb: 'RGB 23/28/65'},
            {name: 'Blue 800', hex: '#1B61EC', rgb: 'RGB 23/28/65'},
            {name: 'Blue 700', hex: '#0A75FF', rgb: 'RGB 23/28/65'},
            {name: 'Blue 600', hex: '#0088FF', rgb: 'RGB 23/28/65'},
            {name: 'Blue 500', hex: '#0097ff', rgb: 'RGB 23/28/65'},
            {name: 'Blue 400', hex: '#12A7FF', rgb: 'RGB 23/28/65'},
            {name: 'Blue 300', hex: '#52b8ff', rgb: 'RGB 23/28/65'},
            {name: 'Blue 200', hex: '#8ACCFF', rgb: 'RGB 23/28/65'},
            {name: 'Blue 100', hex: '#BADFFF', rgb: 'RGB 23/28/65'},
            {name: 'Blue 050', hex: '#E2F3FF', rgb: 'RGB 23/28/65'},
        ],
        // green
        [
            {name: 'Green 900', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Green 800', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Green 700', hex: '#00A900', rgb: 'RGB 23/28/65'},
            {name: 'Green 600', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Green 500', hex: '#00cc00', rgb: 'RGB 23/28/65'},
            {name: 'Green 400', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Green 300', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Green 200', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Green 100', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Green 050', hex: '#e5fae6', rgb: 'RGB 23/28/65'},
        ],
        // green
        [
            {name: 'Green 900', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Green 800', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Green 700', hex: '#00A900', rgb: 'RGB 23/28/65'},
            {name: 'Green 600', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Green 500', hex: '#00cc00', rgb: 'RGB 23/28/65'},
            {name: 'Green 400', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Green 300', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Green 200', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Green 100', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Green 050', hex: '#e5fae6', rgb: 'RGB 23/28/65'},
        ],
        // orange
        [
            {name: 'Orange 900', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Orange 800', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Orange 700', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Orange 600', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Orange 500', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Orange 400', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Orange 300', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Orange 200', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Orange 100', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Orange 050', hex: '#000', rgb: 'RGB 23/28/65'},
        ],
        // red
        [
            {name: 'Red 900', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Red 800', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Red 700', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Red 600', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Red 500', hex: '#f1295c', rgb: 'RGB 23/28/65'},
            {name: 'Red 400', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Red 300', hex: '#f8658d', rgb: 'RGB 23/28/65'},
            {name: 'Red 200', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Red 100', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Red 050', hex: '#fee5eb', rgb: 'RGB 23/28/65'},
        ],
        // purple
        [
            {name: 'Purple 900', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Purple 800', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Purple 700', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Purple 600', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Purple 500', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Purple 400', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Purple 300', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Purple 200', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Purple 100', hex: '#000', rgb: 'RGB 23/28/65'},
            {name: 'Purple 050', hex: '#000', rgb: 'RGB 23/28/65'},
        ],
        // yellow
        [
            {name: 'Yellow 900', hex: '#866700', rgb: 'RGB 23/28/65'},
            {name: 'Yellow 800', hex: '#A88100', rgb: 'RGB 23/28/65'},
            {name: 'Yellow 700', hex: '#CA9B00', rgb: 'RGB 23/28/65'},
            {name: 'Yellow 600', hex: '#ECB500', rgb: 'RGB 23/28/65'},
            {name: 'Yellow 500', hex: '#FFC70F', rgb: 'RGB 23/28/65'},
            {name: 'Yellow 400', hex: '#FFCF31', rgb: 'RGB 23/28/65'},
            {name: 'Yellow 300', hex: '#FFD753', rgb: 'RGB 23/28/65'},
            {name: 'Yellow 200', hex: '#FFDF75', rgb: 'RGB 23/28/65'},
            {name: 'Yellow 100', hex: '#FFE797', rgb: 'RGB 23/28/65'},
            {name: 'Yellow 050', hex: '#FFF3CA', rgb: 'RGB 23/28/65'},
        ]
    ];

    constructor() {
    }

    ngOnInit() {
    }

}
