import {Component, OnInit} from '@angular/core';
import {CxiGridConfig} from '../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../shared/components/cxi-grid/cxi-grid-column';
import {AttributesRepository} from '../../shared/repositories/attributes.repository';
import {ProductAddonRepository} from '../../shared/repositories/product-addon';

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

    // grid 1
    cxiGridConfig1 = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        className: 'demo 1',
        actions: {
            onEdit: (row: any) => alert('on edit'),
            onActivate: (row: any) => alert('on activate'),
            onDeactivate: (row: any) => alert('on deactivate'),
            onDelete: (row: any) => alert('on delete'),
        }
    });
    cxiGridRows1 = [
        {name: 'Mark', description: 'Otto', meta_title: '@mdo', meta_description: 'Otto', meta_keywords: '@mdo'},
        {name: 'Peter', description: 'Otto', meta_title: '@mdo', meta_description: 'Otto', meta_keywords: '@mdo'},
        {name: 'Jobs', description: 'Otto', meta_title: '@mdo', meta_description: 'Otto', meta_keywords: '@mdo'},
    ];
    cxiGridColumns1 = CxiGridColumn.from([
        {title: 'name', name: 'name'},
        {title: 'description', name: 'description'},
        {title: 'meta title', name: 'meta_title'},
        {title: 'meta description', name: 'meta_description'},
        {title: 'meta keywords', name: 'meta_keywords'},
    ]);

    // grid 2
    cxiGridConfig2 = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        translatable: true,
        className: 'demo 2',
        pagingServer: true,
        repository: this.addonsRepository,
        actions: {
            onEdit: (row: any) => alert('on edit'),
            onActivate: (row: any) => alert('on activate'),
            onDeactivate: (row: any) => alert('on deactivate'),
            onDelete: (row: any) => alert('on delete'),
        }
    });
    cxiGridColumns2 = CxiGridColumn.from([
        {title: 'name', name: 'name'},
        {title: 'description', name: 'description'}
    ]);


    // edit table
    cxiEditTableConfig = {
        canRemove: true,
    };
    cxiEditTableRows = [
        {name: 'Mark', description: 'Otto', meta_title: '@mdo', meta_description: 'Otto', meta_keywords: '@mdo'},
        {name: 'Peter', description: 'Otto', meta_title: '@mdo', meta_description: 'Otto', meta_keywords: '@mdo'},
        {name: 'Jobs', description: 'Otto', meta_title: '@mdo', meta_description: 'Otto', meta_keywords: '@mdo'},
    ];
    cxiEditTableColumns = [
        {title: 'name', name: 'name', is_read_only: true},
        {title: 'description', name: 'description'},
        {title: 'meta title', name: 'meta_title'},
        {title: 'meta description', name: 'meta_description'},
        {title: 'meta keywords', name: 'meta_keywords'},
    ];

    constructor(
        private addonsRepository: ProductAddonRepository
    ) {
    }

    ngOnInit() {
    }

}
