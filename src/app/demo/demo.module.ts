import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DemoRoutingModule} from './demo-routing.module';
import {DemoComponent} from './demo/demo.component';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {MessagesModule} from '../messages/messages.module';
import {PusherComponent} from './pusher/pusher.component';
import {NavComponent} from './nav/nav.component';
import {IconComponent} from './icon/icon.component';
import {TableComponent} from './table/table.component';
import {PopupsComponent} from './popups/popups.component';
import {BadgeComponent} from './badge/badge.component';
import {ComponentsComponent} from './components/components.component';
import {SelectionComponent} from './selection/selection.component';
import {CalendarComponent} from './calendar/calendar.component';
import {AvatarComponent} from './avatar/avatar.component';
import {NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import {ColorsComponent} from './colors/colors.component';

@NgModule({
    declarations: [
        DemoComponent,
        PusherComponent,
        NavComponent,
        IconComponent,
        TableComponent,
        PopupsComponent,
        BadgeComponent,
        ComponentsComponent,
        SelectionComponent,
        CalendarComponent,
        AvatarComponent,
        ColorsComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        MessagesModule,
        NgbCollapseModule,
        SharedModule,
        DemoRoutingModule
    ]
})
export class DemoModule {
}
