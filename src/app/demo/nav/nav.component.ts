import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'demo-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
    navItems = [
        {title: 'Colors', link: ['/demo/colors']},
        {title: 'Components', link: ['/demo/components']},
        {title: 'Pusher', link: ['/demo/pusher']},
        {title: 'Icons', link: ['/demo/icons']},
        {title: 'Table', link: ['/demo/table']},
        {title: 'Popup', link: ['/demo/popup']},
        {title: 'Badge', link: ['/demo/badge']},
        {title: 'Selection', link: ['/demo/selection']},
        {title: 'Calendar', link: ['/demo/calendar']},
        {title: 'Avatar', link: ['/demo/avatar']},
    ];

    constructor() {
    }

    ngOnInit() {
    }
}
