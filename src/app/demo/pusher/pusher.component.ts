import {Component, OnInit} from '@angular/core';
import {PusherService} from '../../shared/services/pusher/pusher.service';

@Component({
    selector: 'app-pusher',
    templateUrl: './pusher.component.html',
    styleUrls: ['./pusher.component.scss']
})
export class PusherComponent implements OnInit {
    likes: any = 10;
    order = 0;

    constructor(private pusherService: PusherService) {
    }

    ngOnInit() {
        this.pusherService.channel
            .bind('liked', data => {
                this.liked();
            });
        this.pusherService.subscribe('orders.created')
            .bind('orders.created', data => {
                this.order += 1;
            });
        this.pusherService.subscribe('orders.updated')
            .bind('orders.updated', data => {
                this.order += 1;
            });
    }

    liked() {
        this.likes = parseInt(this.likes, 10) + 1;
    }
}
