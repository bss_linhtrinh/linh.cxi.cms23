import {Component, OnInit} from '@angular/core';
import {MessagesService} from '../../messages/messages.service';
import {NotificationService} from '../../shared/services/noitification/notification.service';
import {CustomerRepository} from '../../shared/repositories/customers.repository';
import {DeliversRepository} from '../../shared/repositories/delivers.repository';

declare const helper: any;

@Component({
    selector: 'app-popups',
    templateUrl: './popups.component.html',
    styleUrls: ['./popups.component.scss']
})
export class PopupsComponent implements OnInit {

    constructor(
        private messagesService: MessagesService,
        private notification: NotificationService,
        private customersRepository: CustomerRepository,
        private deliversRepository: DeliversRepository
    ) {
    }

    ngOnInit() {

    }

    showConfirmation() {
        this.messagesService.statusConfirmation()
            .subscribe();
    }

    showDiscardChanges() {
        this.messagesService.discardChanges()
            .subscribe();
    }

    showUnsavedChanges() {
        this.messagesService.unsavedChanges()
            .subscribe();
    }

    showDeactivateUser() {
        this.messagesService.deactivateUser()
            .subscribe();
    }


    // dialog
    selectItemFromList(key: string) {
        // let itemsSource: Observable<any[]>;
        const options: any = {};
        let columns = [];
        let repository: any;
        switch (key) {
            case 'customer':
                // itemsSource = this.customersRepository.all();
                columns = [
                    {title: 'name', name: 'name', avatar: 'avatar'},
                    {title: 'email', name: 'email'},
                    {title: 'address', name: 'address'},
                    {
                        title: '',
                        name: '',
                        action: {
                            label: 'Select',
                        }
                    },
                ];
                repository = this.customersRepository;
                break;
            case 'deliver':
                // itemsSource = this.deliversRepository.all();
                columns = [
                    {title: 'name', name: 'name', avatar: 'avatar'},
                    {title: 'phone', name: 'phone'},
                    {title: 'email', name: 'email'},
                    {title: 'outlet', name: 'outlet_address'},
                    {
                        title: '',
                        name: '',
                        action: {
                            label: 'Select',
                        }
                    },
                ];
                repository = this.deliversRepository;
                break;
            default:
                break;
        }

        if (columns) {
            options.columns = columns;
        }
        if (repository) {
            options.repository = repository;
        }

        // itemsSource
        //     .pipe(
        //         switchMap(
        //             (items: any[]) => this.messagesService.selectItemFromList(items, options)
        //         )
        //     )
        //     .subscribe(
        //     );


        this.messagesService.selectItemFromList(options);
    }

    showNotificationSuccess() {
        helper.showNotification('Update successful!!', 'done', 'success');
    }

    showNotificationDanger() {
        helper.showNotification('Update failed', 'error', 'danger', 900);
    }

    showToastSuccess() {
        this.notification.success(`Update successful`, 'Title');
    }

    showToastError() {
        this.notification.error(`Update successful`, 'Title');
    }
}
