import {Component, OnInit} from '@angular/core';
import {Image} from '../../shared/entities/image';

@Component({
    selector: 'app-avatar',
    templateUrl: './avatar.component.html',
    styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent implements OnInit {
    avatar: Image;

    constructor() {
    }

    ngOnInit() {
        this.avatar = Image.create(
            {
                small_thumb: '/assets/media/salmon_3_small_thumb.png',
                medium_thumb: '/assets/media/salmon_3_medium_thumb.png',
                original_image: '/assets/media/salmon_3.png'
            }
        );
    }

}
