import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PusherComponent} from './pusher/pusher.component';
import {IconComponent} from './icon/icon.component';
import {TableComponent} from './table/table.component';
import {PopupsComponent} from './popups/popups.component';
import {BadgeComponent} from './badge/badge.component';
import {ComponentsComponent} from './components/components.component';
import {DemoComponent} from './demo/demo.component';
import {SelectionComponent} from './selection/selection.component';
import {CalendarComponent} from './calendar/calendar.component';
import {AvatarComponent} from './avatar/avatar.component';
import {ColorsComponent} from './colors/colors.component';

const routes: Routes = [
    {
        path: '',
        component: DemoComponent,
        children: [
            {path: 'colors', component: ColorsComponent},
            {path: 'components', component: ComponentsComponent},
            {path: 'pusher', component: PusherComponent},
            {path: 'icons', component: IconComponent},
            {path: 'table', component: TableComponent},
            {path: 'popup', component: PopupsComponent},
            {path: 'badge', component: BadgeComponent},
            {path: 'selection', component: SelectionComponent},
            {path: 'calendar', component: CalendarComponent},
            {path: 'avatar', component: AvatarComponent},

            {path: '', redirectTo: 'colors', pathMatch: 'full'}
        ]
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DemoRoutingModule {
}
