import {Component, OnInit} from '@angular/core';
import {BaseFilterCriteria} from '../../shared/entities/base-filter-criteria';
import {Address} from 'ngx-google-places-autocomplete/objects/address';
import {SidebarService} from '../../cxi/sidebar.service';
import {PeriodDate} from '../../shared/components/cxi-period-date/period-date';

@Component({
    selector: 'app-components',
    templateUrl: './components.component.html',
    styleUrls: ['./components.component.scss']
})
export class ComponentsComponent implements OnInit {
    baseInputTextDemo: string;
    baseCheckboxDemo1: boolean;
    baseSwitchDemo: boolean;
    baseButtonDisabled: boolean;
    baseButtonLoading: boolean;


    baseDatepickerDemo = '2019-07-12';
    minDate = new Date('1990-01-01');
    maxDate = new Date();

    cxiTimePickerDemo1 = {
        hour: 13,
        minute: 30,
        second: 30
    };
    cxiTimePickerDemo2 = {
        hour: 7,
        minute: 15,
        second: 40
    };
    cxiPeriodDate = new PeriodDate();
    cxiDaysOfWeek = [2, 3, 5];

    baseRadio: string = 'linh';
    baseRadioValues = ['linh', 'thai', 'tuan', 'hong', 'khanh', 'hau'];
    baseRadioValues3 = [
        {name: 'linh dev', value: 'linh'},
        {name: 'Thái dev', value: 'thai'},
        {name: 'Tuấn dev', value: 'tuan'},
        {name: 'Hồng QA', value: 'hong'},
        {name: 'Khánh dev', value: 'khanh'},
        {name: 'Hậu dev', value: 'hau'},
    ];
    baseMonetaryDemo: number;
    baseIntegerDemo: number;
    baseUploadFile: string | ArrayBuffer;
    cxiIncrease = 0;


    // filter
    baseFilterDemo = BaseFilterCriteria.parserList(
        [
            {name: 'Organization', type: 'text'},
            {name: 'Brand', type: 'text'},
            {name: 'Outlet', type: 'text'}
        ]
    );

    // ngx-google-places-autocomplete
    baseGooglePlace: any;
    baseAddress: any;

    demoOptions = [
        {id: 1, name: 'Option 1'},
        {id: 2, name: 'Option 2', disabled: true},
        {id: 3, name: 'Option 3'},
        {id: 4, name: 'Option 4'},
    ];
    demoOptionId = 1;

    public handleAddressChange(address: Address) {
        this.baseAddress = address;
    }

    // Constructor
    constructor(
        private sideBarService: SidebarService
    ) {
    }

    ngOnInit() {
    }

    toggleSidebar() {
        this.sideBarService.toggle();
    }

    buttonClick() {
    }

    onFileChanged(file) {
    }

}
