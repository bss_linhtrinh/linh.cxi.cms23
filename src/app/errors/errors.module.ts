import {ErrorHandler, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ErrorsComponent} from './errors/errors.component';
import {RouterModule} from '@angular/router';
import {ErrorRoutingModule} from './errors-routing.module';
import {ErrorsHandler} from './errors-handler/errors-handler';
import {ServerErrorsInterceptor} from './server-errors-interceptor/server-errors.interceptor';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {SharedModule} from '../shared/shared.module';
import {ErrorsService} from './errors-service/errors.service';
import {PageNotFoundComponent} from './errors/page-not-found/page-not-found.component';
import {DomainInvalidComponent} from './errors/domain-invalid/domain-invalid.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        SharedModule,
        ErrorRoutingModule,
    ],
    declarations: [
        ErrorsComponent,
        PageNotFoundComponent,
        DomainInvalidComponent
    ],
    providers: [
        ErrorsService,
        {
            provide: ErrorHandler,
            useClass: ErrorsHandler,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ServerErrorsInterceptor,
            multi: true
        },
    ],
    exports: [
        ErrorsComponent
    ]
})
export class ErrorsModule {
}
