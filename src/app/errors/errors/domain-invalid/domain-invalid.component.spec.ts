import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomainInvalidComponent } from './domain-invalid.component';

describe('DomainInvalidComponent', () => {
  let component: DomainInvalidComponent;
  let fixture: ComponentFixture<DomainInvalidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomainInvalidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomainInvalidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
