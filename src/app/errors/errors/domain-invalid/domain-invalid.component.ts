import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {filter, map, switchMap} from 'rxjs/operators';

@Component({
    selector: 'app-domain-invalid',
    templateUrl: './domain-invalid.component.html',
    styleUrls: ['./domain-invalid.component.scss']
})
export class DomainInvalidComponent implements OnInit {
    routeParams: any;
    data: any;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.routeParams = this.activatedRoute.snapshot.queryParams;
        this.data = this.activatedRoute.snapshot.data;
    }
}
