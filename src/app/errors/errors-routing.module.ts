import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ErrorsComponent} from './errors/errors.component';
import {PageNotFoundComponent} from './errors/page-not-found/page-not-found.component';
import {DomainInvalidComponent} from './errors/domain-invalid/domain-invalid.component';

const routes: Routes = [
    {
        path: 'errors',
        component: ErrorsComponent,
        children: [
            {path: 'page-not-found', component: PageNotFoundComponent},
            {path: 'domain-invalid', component: DomainInvalidComponent},
            // {path: '', redirectTo: 'page-not-found'},
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ErrorRoutingModule {
}
