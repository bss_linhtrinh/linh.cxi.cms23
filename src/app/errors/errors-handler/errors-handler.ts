// errors-handler.ts
import {ErrorHandler, Injectable, Injector} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {ErrorsService} from '../errors-service/errors.service';
import {NotificationService} from '../../shared/services/noitification/notification.service';

@Injectable({
    providedIn: 'root'
})
export class ErrorsHandler implements ErrorHandler {

    constructor(
        // Because the ErrorHandler is created before the providers, we’ll have to use the Injector to get them.
        private injector: Injector,
        private notification: NotificationService) {
    }

    handleError(error: Error | HttpErrorResponse) {
        const errorsService = this.injector.get(ErrorsService);
        const router = this.injector.get(Router);

        if (error instanceof HttpErrorResponse) {
            // Server or connection error happened
            if (!navigator.onLine) {
                // Handle offline error
                this.notify('No Internet Connection');
            } else {
                // Handle Http Error (error.status === 403, 404...)

                // if (error.status === 401) {
                //     router.navigateByUrl('/auth/login');
                // } else if (error.status === 400 ||
                //     error.status === 401 ||
                //     error.status === 403 ||
                //     error.status === 404 ||
                //     error.status === 422) {
                //     this.notify(error);
                // }

                // disabled
                // this.notify(error);
            }
        } else {

        }

        // Log the error anyway
        console.error('CMS CXI BSS errors handler: ', error);
    }


    notify(error) {
        const notificationService = this.injector.get(NotificationService);
        let message = '';
        const errorStatus = error.status;
        const errorData = error.error;

        if (typeof error === 'string') {
            message += error;
        } else {
            switch (errorStatus) {
                case 400:
                case 401:
                case 402:
                case 403:
                case 404:
                case 422:
                case 429:
                    if (errorData) {
                        if (errorData.errors) {
                            const errors = errorData.errors;
                            for (const e of errors) {
                                if (typeof e === 'string') {
                                    message += `${e}`;
                                } else if (e.message) {
                                    message += `<p>${e.message}</p>`;
                                }
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        if (message) {
            const options = {
                translate: false
            };
            this.notification.error(message, null, options);
        }
    }
}
