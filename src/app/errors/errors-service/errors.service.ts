import {Injectable, Injector} from '@angular/core';
import {LocationStrategy, PathLocationStrategy} from '@angular/common';
import {HttpErrorResponse} from '@angular/common/http';
import * as StackTraceParser from 'error-stack-parser';
import {NavigationError, Event, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {ApiService} from '../../shared/services/api/api.service';

@Injectable({
    providedIn: 'root'
})
export class ErrorsService {

    constructor(private injector: Injector,
                private router: Router,
                private apiService: ApiService) {
        // Listen to the navigation errors
        this.router
            .events
            .subscribe((event: Event) => {
                // Redirect to the ErrorComponent
                if (event instanceof NavigationError) {
                    if (!navigator.onLine) {
                        return;
                    }
                    // Redirect to the ErrorComponent
                    // this.log(event.error)
                    //     .subscribe((errorWithContext) => {
                    //         this.router.navigate(['/error'], { queryParams: errorWithContext });
                    //     });
                }
            });
    }


    log(error): Observable<any> {
        // Log the error to the console
        console.error(error);

        // Send error to server
        const errorToSend = this.addContextInfo(error);

        return this.apiService.post('supports/bugs', errorToSend);
    }

    addContextInfo(error) {
        // All the context details that you want (usually coming from other services; Constants, UserService...)
        const name = error.name || null;
        const appId = '';
        const user = 'ShthppnsUser';
        const time = new Date().getTime();
        const id = `${appId}-${user}-${time}`;
        const location = this.injector.get(LocationStrategy);
        const url = location instanceof PathLocationStrategy ? location.path() : '';
        const status = error.status || null;
        const message = error.message || error.toString();
        const stack = error instanceof HttpErrorResponse ? null : StackTraceParser.parse(error);

        const errorToSend = {
            name,
            appId,
            user,
            time,
            id,
            url,
            status,
            message,
            stack
        };

        return errorToSend;
    }
}
