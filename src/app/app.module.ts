import {LOCALE_ID, NgModule} from '@angular/core';
import {AppComponent, BaseDateNativeAdapter, BaseParserFormatter} from './app.component';
import {AppRoutingModule} from './app.routing';
import {SharedModule} from './shared/shared.module';
import {APIInterceptor} from './app.httpClient';
import {AuthResolver} from './auth.resolver';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {NgbDateAdapter, NgbDateParserFormatter, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgSelectModule} from '@ng-select/ng-select';
import {CookieService} from 'ngx-cookie-service';
import {NgxMaskModule} from 'ngx-mask';
import {NgxWebstorageModule} from 'ngx-webstorage';
import {CxiModule} from './cxi/cxi.module';
import {ErrorsModule} from './errors/errors.module';
import {ErrorsService} from './errors/errors-service/errors.service';
import {NgxSpinnerModule} from 'ngx-spinner';
import {GooglePlaceModule} from 'ngx-google-places-autocomplete';
import {MessagesModule} from './messages/messages.module';
import {ChartsModule} from 'ng2-charts';
import {ToastrModule} from 'ngx-toastr';
import {BrowserModule} from '@angular/platform-browser';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {registerLocaleData} from '@angular/common';
import localeVi from '@angular/common/locales/vi';
import {QRCodeModule} from 'angularx-qrcode';
import {AgmCoreModule} from '@agm/core';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import {PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface, PerfectScrollbarModule} from 'ngx-perfect-scrollbar';

// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient) {
    let prefix: string;
    const hostname = window.location.hostname;
    if (hostname === 'localhost') {
        const port = window.location.port;
        prefix = `http://localhost:${port}/assets/i18n/`;
    } else {
        const protocol = window.location.protocol;
        prefix = `${protocol}//${hostname}/assets/i18n/`;
    }

    return new TranslateHttpLoader(http, prefix, '.json');
}

// the second parameter 'vi' is optional
registerLocaleData(localeVi, 'vi');

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: false,
    suppressScrollY: false
};

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        // libs
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        NgbModule,
        NgSelectModule,
        NgxMaskModule.forRoot(),
        NgxWebstorageModule.forRoot({prefix: 'cxi'}),
        NgxSpinnerModule,
        GooglePlaceModule,
        ChartsModule,
        ToastrModule.forRoot({
            enableHtml: true,
            timeOut: 10000,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        QRCodeModule,
        AgmCoreModule.forRoot({
            // please get your own API key here:
            // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
            apiKey: 'AIzaSyCvuwRpNWXNnlLoAm3So0n8sjNlYJX_D3o'
        }),
        SlickCarouselModule,
        PerfectScrollbarModule,

        // ..
        ErrorsModule,
        SharedModule,
        MessagesModule,
        CxiModule,
        AppRoutingModule,

    ],
    providers: [
        CookieService,
        ErrorsService,
        {provide: NgbDateAdapter, useClass: BaseDateNativeAdapter},
        {provide: NgbDateParserFormatter, useClass: BaseParserFormatter},
        {
            provide: HTTP_INTERCEPTORS,
            useClass: APIInterceptor,
            multi: true,
        },
        {provide: LOCALE_ID, useValue: 'en-GB'},
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        },
        AuthResolver,
    ],
    bootstrap: [
        AppComponent
    ],
})

export class AppModule {
}
