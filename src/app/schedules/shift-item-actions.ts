import {ShiftItemAction} from './shift-item-action';

export const SHIFT_ITEM_ACTIONS = [
    ShiftItemAction.ClockedIn,
    ShiftItemAction.ClockedOut,
    ShiftItemAction.Leave,
    ShiftItemAction.Absent,
    ShiftItemAction.Remove,
];
