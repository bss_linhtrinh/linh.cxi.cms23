import {BaseEntity} from '../shared/entities/base.entity';

export class ShiftCalendarDayUser extends BaseEntity {
    id: any;
    first_name: string;
    last_name: string;

    _avatar: string;

    get name(): string {
        const fn = this.capitalize(this.first_name);
        const ln = this.capitalize(this.last_name);

        if (!fn) {
            return `${ln}`;
        }

        if (!ln) {
            return `${fn}`;
        }

        return `${fn} ${ln}`;
    }

    get avatar(): string | any {
        return this._avatar;
    }

    set avatar(val: string | any) {
        this._avatar = this.parseAvatar(val);
    }

    constructor() {
        super();
        this.id = null;
        this.first_name = null;
        this.last_name = null;
        this._avatar = null;
    }

    static create(data) {
        return (new ShiftCalendarDayUser()).fill(data);
    }

    fill(data): this {
        this.avatar = data.avatar ? data.avatar : null;
        return super.fill(data);
    }
}
