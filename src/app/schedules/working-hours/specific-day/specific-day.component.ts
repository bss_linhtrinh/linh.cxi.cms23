import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {WorkingHourExcludeSpecificDay} from '../../working-hour-exclude-specific-day';

@Component({
    selector: 'specific-day',
    templateUrl: './specific-day.component.html',
    styleUrls: ['./specific-day.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SpecificDayComponent, multi: true}
    ]
})
export class SpecificDayComponent extends ElementBase<WorkingHourExcludeSpecificDay> implements OnInit {
    @Input() name: string;
    @Input() label: string;
    @Output() remove = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    removeThisSpecificDay() {
        this.remove.emit();
    }
}
