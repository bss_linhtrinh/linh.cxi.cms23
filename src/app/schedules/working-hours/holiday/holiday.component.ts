import {Component, Inject, Input, OnDestroy, OnInit, Optional, ViewChild} from '@angular/core';
import {delay, map, retryWhen, tap} from 'rxjs/operators';
import {WorkingHourHoliday} from '../../working-hour-holiday';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {WorkingHoursHolidayRepository} from '../../../shared/repositories/working-hours-holiday.repository';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {of, Subscription} from 'rxjs';

@Component({
    selector: 'working-hours-holiday',
    templateUrl: './holiday.component.html',
    styleUrls: ['./holiday.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: HolidayComponent, multi: true}
    ]
})
export class HolidayComponent extends ElementBase<string[]> implements OnInit, OnDestroy {

    @Input() name: string;

    @ViewChild(NgModel, {static: true}) model: NgModel;

    workingHourHolidays: WorkingHourHoliday[] = [];
    workingHourHolidayValues = [];
    subscriptions: Subscription[] = [];

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private workingHoursHolidayRepository: WorkingHoursHolidayRepository
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
        this.initValue();
        this.getWorkingHoursHolidays();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    initValue() {
        const source = of(null);
        const example = source.pipe(
            map(() => {
                if (!this.value || this.value.length === 0) {
                    throw 1;
                }
                return 1;
            }),
            retryWhen(errors => errors.pipe(delay(500))),
            delay(100),
        );
        const parseValue = example.subscribe(() => this.parseValue());
        this.subscriptions.push(parseValue);
    }

    parseValue() {
        if (this.value && this.value.length > 0) {
            const workingHourHolidayValues = [];
            this.workingHourHolidays.forEach(
                (workingHourHoliday: WorkingHourHoliday, index: number) => {
                    workingHourHolidayValues[index] = this.value.findIndex(v => v === workingHourHoliday.id) !== -1;
                }
            );
            this.workingHourHolidayValues = workingHourHolidayValues;
        }
    }

    // holidays
    getWorkingHoursHolidays() {
        this.workingHoursHolidayRepository.all()
            .pipe(
                tap((workingHourHolidays: WorkingHourHoliday[]) => this.workingHourHolidays = workingHourHolidays),
            )
            .subscribe();
    }

    onCheckOccasion() {
        const selectedHolidays = this.workingHourHolidays.filter(
            (holiday: WorkingHourHoliday, index: number) => this.workingHourHolidayValues[index]
        );
        this.value = selectedHolidays.map(holiday => holiday.id);
    }
}
