import {Component, OnInit} from '@angular/core';
import {WorkingHour} from '../working-hour';
import {WorkingHoursRepository} from '../../shared/repositories/working-hours.repository';
import {map, tap} from 'rxjs/operators';
import {NotificationService} from '../../shared/services/noitification/notification.service';
import * as _ from 'lodash';
import {MessagesService} from '../../messages/messages.service';
import {LocalStorage} from 'ngx-webstorage';

@Component({
    selector: 'app-working-hours',
    templateUrl: './working-hours.component.html',
    styleUrls: ['./working-hours.component.scss']
})
export class WorkingHoursComponent implements OnInit {
    @LocalStorage('scope.brand_id') brandId: any;
    @LocalStorage('scope.outlet_id') outletId: any;
    workingHour: WorkingHour = new WorkingHour();
    clonedWorkingHour: WorkingHour;
    isEdit = false;

    constructor(
        private workingHoursRepository: WorkingHoursRepository,
        private notificationService: NotificationService,
        private messagesService: MessagesService
    ) {
    }

    ngOnInit() {
        this.getWorkingHours();
    }

    getWorkingHours() {
        this.workingHoursRepository.all()
            .pipe(
                map((workingHours: WorkingHour[]): WorkingHour => workingHours && workingHours.length > 0 ? workingHours[0] : null),
                tap((workingHour: WorkingHour) => this.workingHour = workingHour),
                tap(() => this.cloneDeep()),
                tap(() => this.isEdit = true)
            )
            .subscribe();
    }

    onSubmit() {
        let source: any;
        if (this.brandId && this.outletId) {
            this.workingHour.brandId = this.brandId;
            this.workingHour.outletId = [this.outletId];
        }
        if (this.isEdit) {
            source = this.workingHoursRepository.update('', this.workingHour);
        } else {
            source = this.workingHoursRepository.create(this.workingHour);
        }

        source
            .pipe(
                tap(() => this.notificationService.success('Update successful')),
                tap(() => this.cloneDeep())
            )
            .subscribe();
    }

    cancel() {
    }

    canDeactivate() {
        if (this.isChangedData()) {
            return this.messagesService.unsavedChanges();
        }
        return true;
    }

    cloneDeep() {
        this.clonedWorkingHour = _.cloneDeep(this.workingHour);
    }

    isChangedData(): boolean {
        return !(_.isEqual(this.clonedWorkingHour, this.workingHour));
    }
}
