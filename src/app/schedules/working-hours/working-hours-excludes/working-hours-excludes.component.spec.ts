import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkingHoursExcludesComponent } from './working-hours-excludes.component';

describe('WorkingHoursExcludesComponent', () => {
  let component: WorkingHoursExcludesComponent;
  let fixture: ComponentFixture<WorkingHoursExcludesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkingHoursExcludesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkingHoursExcludesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
