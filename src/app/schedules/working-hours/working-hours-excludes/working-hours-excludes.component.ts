import {Component, Inject, Input, OnInit, Optional, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {WorkingHourExclude} from '../../working-hour-exclude';
import {WorkingHourExcludeSpecificDay} from '../../working-hour-exclude-specific-day';

@Component({
    selector: 'working-hours-excludes',
    templateUrl: './working-hours-excludes.component.html',
    styleUrls: ['./working-hours-excludes.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: WorkingHoursExcludesComponent, multi: true}
    ]
})
export class WorkingHoursExcludesComponent extends ElementBase<WorkingHourExclude> implements OnInit {

    @Input() name: string;

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }


    // specific day
    addSpecificDay() {
        if (!this.value) {
            return;
        }
        if (!this.value.specificDays) {
            this.value.specificDays = [];
        }

        this.value.specificDays.push(new WorkingHourExcludeSpecificDay());
    }

    onRemoveSpecificDay(index: number) {
        if (index < 0 ||
            !this.value.specificDays ||
            this.value.specificDays.length === 0) {
            return;
        }

        this.value.specificDays.splice(index, 1);

        this.checkEmptySpecificDay();
    }

    checkEmptySpecificDay() {
        if (!this.value) {
            return;
        }
        if (!this.value.specificDays || this.value.specificDays.length === 0) {
            this.addSpecificDay();
        }
    }
}
