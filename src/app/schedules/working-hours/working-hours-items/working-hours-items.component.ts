import {Component, Inject, Input, OnDestroy, OnInit, Optional, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {WorkingHourItem} from '../../working-hour-item';
import {of, Subscription} from 'rxjs';
import {delay, map, retryWhen} from 'rxjs/operators';

@Component({
    selector: 'working-hours-items',
    templateUrl: './working-hours-items.component.html',
    styleUrls: ['./working-hours-items.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: WorkingHoursItemsComponent, multi: true}
    ]
})
export class WorkingHoursItemsComponent extends ElementBase<WorkingHourItem[]> implements OnInit, OnDestroy {
    @Input() name: string;

    @ViewChild(NgModel, {static: true}) model: NgModel;

    disabledDays: number[] = [];
    subscriptions: Subscription[] = [];

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
        this.waitValue();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    waitValue() {
        const source = of(null);
        const example = source.pipe(
            map(() => {
                if (!this.value || this.value.length === 0) {
                    throw 1;
                }
                return 1;
            }),
            retryWhen(errors => errors.pipe(delay(500))),
            delay(100),
        );
        const doValueSub = example.subscribe(
            () => this.setDisabledDays()
        );
        this.subscriptions.push(doValueSub);
    }

    addWorkingHourItem() {
        if (!this.value) {
            this.value = [];
        }

        this.value.push(new WorkingHourItem());
    }

    onRemove(index: number) {
        if (!this.value) {
            return;
        }
        this.value.splice(index, 1);
        this.checkEmpty();

        this.setDisabledDays();
    }

    checkEmpty() {
        if (this.value &&
            this.value.length === 0) {
            this.addWorkingHourItem();
        }
    }

    // days of week
    onChangeDaysOfWeek() {
        if (!this.value) {
            return;
        }
        this.setDisabledDays();
    }

    setDisabledDays() {
        if (!this.value) {
            return;
        }

        const disabledDays = [];
        for (const workingHourItem of this.value) {
            for (const day of workingHourItem.days) {
                if (disabledDays.findIndex(x => x === day) === -1) {
                    disabledDays.push(day);
                }
            }
        }
        this.disabledDays = disabledDays.sort();
    }
}
