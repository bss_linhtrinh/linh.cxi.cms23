import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkingHoursItemsComponent } from './working-hours-items.component';

describe('WorkingHoursItemsComponent', () => {
  let component: WorkingHoursItemsComponent;
  let fixture: ComponentFixture<WorkingHoursItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkingHoursItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkingHoursItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
