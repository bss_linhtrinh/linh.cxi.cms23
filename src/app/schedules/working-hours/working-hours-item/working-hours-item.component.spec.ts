import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkingHoursItemComponent } from './working-hours-item.component';

describe('WorkingHoursItemComponent', () => {
  let component: WorkingHoursItemComponent;
  let fixture: ComponentFixture<WorkingHoursItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkingHoursItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkingHoursItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
