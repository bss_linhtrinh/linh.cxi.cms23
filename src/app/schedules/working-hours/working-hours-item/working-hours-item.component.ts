import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {WorkingHourItem} from '../../working-hour-item';
import {WorkingHourItemPeriod} from '../../working-hour-item-period';

@Component({
    selector: 'working-hours-item',
    templateUrl: './working-hours-item.component.html',
    styleUrls: ['./working-hours-item.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: WorkingHoursItemComponent, multi: true}
    ]
})
export class WorkingHoursItemComponent extends ElementBase<WorkingHourItem> implements OnInit {

    @Input() name: string;
    @Input() disabledDays: number[];
    @Output() remove = new EventEmitter();
    @Output() daysOfWeekChange = new EventEmitter<number[]>();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    removeThisWorkingHourItem() {
        this.remove.emit();
    }

    // days of week
    onChangeDaysOfWeek() {
        this.daysOfWeekChange.emit(this.value.days);
    }

    // PERIOd

    addPeriod() {
        if (!this.value.periods) {
            this.value.periods = [];
        }

        this.value.periods.push(new WorkingHourItemPeriod());
    }

    removePeriod(index: number) {
        if (!this.value || !this.value.periods) {
            return;
        }

        this.value.periods.splice(index, 1);

        this.checkEmpty();
    }

    checkEmpty() {
        if (this.value &&
            this.value.periods &&
            this.value.periods.length === 0) {
            this.addPeriod();
        }
    }
}
