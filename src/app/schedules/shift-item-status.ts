export enum ShiftItemStatus {
    ClockedIn = 'clockedIn',
    ClockedOut = 'clockedOut',
    ApprovedOff = 'approvedOff',
    OffWithoutApproved = 'offWithoutApproved',
}


// ClockedIn ->
// Leave
// Absent
// Remove


// Clock out
// Leave
// Absent
