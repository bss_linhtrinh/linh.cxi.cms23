import {ShiftItem} from './shift-item';
import {BaseEntity} from '../shared/entities/base.entity';

export class ShiftCalendarDay extends BaseEntity {

    id: string;

    first_name: string;
    last_name: string;
    _avatar: any;
    shiftsItem: ShiftItem[];

    get name(): string {
        const fn = this.capitalize(this.first_name);
        const ln = this.capitalize(this.last_name);

        if (!fn) {
            return `${ln}`;
        }

        if (!ln) {
            return `${fn}`;
        }

        return `${fn} ${ln}`;
    }

    get avatar(): string | any {
        return this._avatar;
    }

    set avatar(val: string | any) {
        this._avatar = this.parseAvatar(val);
    }

    role: { id: string, name: string, color: string };

    get roleColor(): string {
        if (!this.role || !this.role.color) {
            return '#ccc';
        }
        return this.role.color;
    }

    constructor() {
        super();
        this.id = null;
        this.first_name = null;
        this.last_name = null;
        this._avatar = null;
        this.shiftsItem = [];

        this.role = null;
    }

    static create(data): ShiftCalendarDay {
        return (new ShiftCalendarDay()).fill(data);
    }

    fill(data): this {
        if (data.shiftsItem && data.shiftsItem.length > 0) {
            data.shiftsItem = data.shiftsItem.map(shiftItem => ShiftItem.create(shiftItem));
        }
        this.avatar = data.avatar;
        return super.fill(data);
    }
}
