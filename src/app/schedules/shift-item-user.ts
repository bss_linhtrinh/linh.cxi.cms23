import {BaseEntity} from '../shared/entities/base.entity';

export class ShiftItemUser extends BaseEntity {
    _id: any;
    first_name: string;
    last_name: string;

    get id(): any {
        return this._id;
    }

    get name(): string {
        const fn = this.capitalize(this.first_name);
        const ln = this.capitalize(this.last_name);

        if (!fn) {
            return `${ln}`;
        }

        if (!ln) {
            return `${fn}`;
        }

        return `${fn} ${ln}`;
    }

    _avatar: string;

    get avatar(): string | any {
        return this._avatar;
    }

    set avatar(val: string | any) {
        this._avatar = this.parseAvatar(val);
    }

    role: { id: string, name: string, color: string };

    constructor() {
        super();
        this._id = null;
        this.first_name = null;
        this.last_name = null;
        this._avatar = null;
        this.role = {id: null, name: null, color: '#ccc'};
    }

    static create(data) {
        return (new ShiftItemUser()).fill(data);
    }

    fill(data): this {
        this.avatar = data.avatar ? data.avatar : null;
        return super.fill(data);
    }
}
