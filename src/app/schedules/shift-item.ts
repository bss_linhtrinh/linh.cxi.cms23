import {BaseEntity} from '../shared/entities/base.entity';
import * as moment from 'moment';
import {ShiftItemUser} from './shift-item-user';
import {ShiftItemStatus} from './shift-item-status';
import {ShiftItemChecklistData} from './shift-item-checklist-data';
import * as _ from 'lodash';

export class ShiftItem extends BaseEntity {
    _id: string;
    appointment: any[];
    backgroundColor: string;
    char: string;
    shift: string;

    date: Date;
    from: string;
    to: string;

    get id(): string {
        return this._id;
    }

    get fromTime(): string {
        return this.formatHour(this.from);
    }

    get toTime(): string {
        return this.formatHour(this.to);
    }

    get fromToTime(): string {
        return `${this.fromTime} - ${this.toTime}`;
    }

    user: ShiftItemUser;

    // status
    status: ShiftItemStatus;
    clockedInTime: string;
    clockedOutTime: string;

    get statusLabel(): string {
        let label: string;
        if (this.status === 'offWithoutApproved') {
            label = 'Leave';
        } else if (this.status === 'approvedOff') {
            label = 'Absent';
        } else {
            label = this.status;
        }
        return _.capitalize(_.startCase(label));
    }

    get clockedInAt(): string {
        return this.clockedInTime
            ? moment(this.clockedInTime).format('hh:mm A')
            : null;
    }

    get clockedOutAt(): string {
        return this.clockedOutTime
            ? moment(this.clockedOutTime).format('hh:mm A')
            : null;
    }

    // checklist & tasks
    checkList: any;
    checkListData: ShiftItemChecklistData[];

    get totalListTasks() {
        return this.checkListData && this.checkListData.length
            ? this.checkListData.length
            : 0;
    }

    get numberOfDoneTasks() {
        return this.checkListData && this.checkListData.length
            ? (
                this.checkListData
                    .filter(checklistData => checklistData && checklistData.status)
                    .length
            )
            : 0;
    }

    // statistics
    statistics?: {
        totalTask: number
        totalTaskDone: number
        totalTaskDoneAfter: number
        totalTaskDoneBefore: number
    };

    constructor() {
        super();
        this._id = null;
        this.backgroundColor = null;
        this.char = null;
        this.shift = null;

        this.appointment = [];
        this.date = new Date();
        this.from = null;
        this.to = null;

        this.checkList = null;
        this.checkListData = [];

        this.user = null;

        this.status = null;
        this.clockedInTime = null;
        this.clockedOutTime = null;

        this.statistics = {
            totalTask: null,
            totalTaskDone: null,
            totalTaskDoneAfter: null,
            totalTaskDoneBefore: null
        };
    }

    static create(data): ShiftItem {
        return (new ShiftItem()).fill(data);
    }

    fill(data): this {
        if (data.date) {
            data.date = new Date(data.date);
        }

        if (data.user) {
            data.user = ShiftItemUser.create(data.user);
        }

        return super.fill(data);
    }

    parse(): any {
        return {
            appointment: this.appointment,
            date: this.parseDate(this.date),
            from: this.parseDate(this.from),
            to: this.parseDate(this.to)
        };
    }
}
