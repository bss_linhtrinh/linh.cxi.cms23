import {BaseEntity} from '../shared/entities/base.entity';
import {PeriodDate} from '../shared/components/cxi-period-date/period-date';

export class WorkingHourExcludeSpecificDay extends BaseEntity {

    date: string;

    periodDate: PeriodDate;
    fromHour: number;
    fromMinute: number;
    fromMeridiem: string;
    toHour: number;
    toMinute: number;
    toMeridiem: string;

    constructor() {
        super();
        this.date = null;

        this.periodDate = new PeriodDate();
        this.fromHour = 0;
        this.fromMinute = 0;
        this.fromMeridiem = 'AM';
        this.toHour = 0;
        this.toMinute = 0;
        this.toMeridiem = 'AM';
    }

    static create(data): WorkingHourExcludeSpecificDay {
        return (new WorkingHourExcludeSpecificDay()).fill(data);
    }

    fill(data): this {
        if (
            typeof data.fromHour !== 'undefined' ||
            typeof data.fromMinute !== 'undefined' ||
            typeof data.fromMeridiem !== 'undefined' ||
            typeof data.toHour !== 'undefined' ||
            typeof data.toMinute !== 'undefined' ||
            typeof data.toMeridiem !== 'undefined'
        ) {
            data.periodDate = PeriodDate.create(data);
        }
        return super.fill(data);
    }

    parse() {
        // period
        if (this.periodDate) {
            this.fromHour = this.periodDate.fromHour;
            this.fromMinute = this.periodDate.fromMinute;
            this.fromMeridiem = this.periodDate.fromMeridiem;
            this.toHour = this.periodDate.toHour;
            this.toMinute = this.periodDate.toMinute;
            this.toMeridiem = this.periodDate.toMeridiem;
        }

        return {
            date: this.date,
            fromHour: this.fromHour,
            fromMinute: this.fromMinute,
            fromMeridiem: this.fromMeridiem,
            toHour: this.toHour,
            toMinute: this.toMinute,
            toMeridiem: this.toMeridiem,
        };
    }
}
