import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ShiftBlock} from '../../shift-block';
import {NgbModal, NgbPopover} from '@ng-bootstrap/ng-bootstrap';
import {CxiCalendarWeekAddStaffComponent} from '../cxi-calendar-week-add-staff/cxi-calendar-week-add-staff.component';
import {from, throwError} from 'rxjs';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import {ShiftsRepository} from '../../../shared/repositories/shifts.repositories';
import {ShiftItem} from '../../shift-item';
import * as _ from 'lodash';
import * as moment from 'moment';
import {ShiftItemAction} from '../../shift-item-action';
import {Role} from '../../../shared/entities/role';
import {SHIFT_ITEM_ACTIONS} from '../../shift-item-actions';
import {ShiftItemStatus} from '../../shift-item-status';

@Component({
    selector: 'cxi-calendar-event-vertical',
    templateUrl: './cxi-calendar-event-vertical.component.html',
    styleUrls: ['./cxi-calendar-event-vertical.component.scss']
})
export class CxiCalendarEventVerticalComponent implements OnInit, OnChanges {
    @Input() name: string;
    @Input() backgroundColor: string;
    @Input('height.rem') heightRem: number; // rem

    @Input() dayOfWeek: number;

    // shiftRoles
    @Input() shiftBlock: ShiftBlock;
    @Input() roles: Role[];

    // assign
    @Output() assign = new EventEmitter<string[]>();
    @Output() removedShiftItem = new EventEmitter<ShiftItem>();

    internalShiftItems: ShiftItem[];
    internalRoles: Role[];

    selectedAll: boolean;
    selectedShiftItems = [];

    search: string;
    roleID: number;

    shiftItemActions = SHIFT_ITEM_ACTIONS;

    perfectScrollbarConfigListShiftItems = {
        useBothWheelAxes: false,
        suppressScrollX: true,
        suppressScrollY: false,
    };

    isDisabledAddStaffs: boolean;

    constructor(
        private ngbModal: NgbModal,
        private shiftsRepository: ShiftsRepository
    ) {
    }

    ngOnInit() {
        this.checkIsDisabledAddStaff();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes) {
            // shiftBlock
            if (changes.shiftBlock &&
                changes.shiftBlock.currentValue !== changes.shiftBlock.previousValue) {
                this.getInternalItemsOfShiftBlock();
            }

            // roles
            if (changes.roles &&
                changes.roles.currentValue !== changes.roles.previousValue) {
                this.getInternalRoles();
            }
        }
    }

    getInternalRoles() {
        if (this.roles) {
            this.internalRoles = _.cloneDeep(this.roles);
            this.internalRoles = this.internalRoles
                .filter(
                    (role: Role) => {
                        const index = this.shiftBlock.items
                            .findIndex(
                                shiftItem => shiftItem &&
                                    shiftItem.user &&
                                    shiftItem.user.role &&
                                    parseInt(shiftItem.user.role.id, 10) === role.id
                            );

                        return index !== -1;
                    }
                );
        }
    }

    getInternalItemsOfShiftBlock() {
        if (this.shiftBlock &&
            this.shiftBlock.items &&
            this.shiftBlock.items instanceof Array) {

            this.internalShiftItems = _.cloneDeep(this.shiftBlock.items);

            if (this.search || this.roleID) {
                this.internalShiftItems = this.internalShiftItems
                    .filter(
                        (shiftItem: ShiftItem) => {
                            let match = false;

                            // search
                            const matchSearch = (
                                    !!shiftItem.user.first_name &&
                                    _.lowerCase(shiftItem.user.first_name)
                                        .includes(
                                            _.lowerCase(this.search)
                                        )
                                ) ||
                                (
                                    !!shiftItem.user.last_name &&
                                    _.lowerCase(shiftItem.user.last_name)
                                        .includes(
                                            _.lowerCase(this.search)
                                        )
                                ) ||
                                (
                                    !!shiftItem.user.first_name &&
                                    !!shiftItem.user.last_name &&
                                    _.lowerCase(`${shiftItem.user.first_name} ${shiftItem.user.last_name}`)
                                        .includes(
                                            _.lowerCase(this.search)
                                        )
                                );

                            // role
                            const matchRole = !this.roleID || (
                                shiftItem.user &&
                                shiftItem.user.role &&
                                parseInt(shiftItem.user.role.id, 10) === this.roleID
                            );

                            match = (matchSearch && matchRole);

                            return match;
                        }
                    );
            }
        }
    }

    getHeight() {
        return this.heightRem ? this.heightRem : (240 / 14);
    }

    // == add staff

    checkIsDisabledAddStaff() {
        this.isDisabledAddStaffs = moment().isSameOrAfter(moment(this.shiftBlock.from));
    }

    isShow(currentAct: string, shiftItem: ShiftItem): boolean {
        let isShow = true;

        if (this.shiftItemActions.findIndex(act => act === currentAct) === -1) {
            return false;
        }

        const actions = this.getActionsByStatus(shiftItem.status);
        isShow = actions.findIndex(action => action === currentAct) !== -1;

        return isShow;
    }

    getActionsByStatus(status: string) {
        let actions: ShiftItemAction[];
        switch (status) {
            case null:
            default:
                actions = [
                    ShiftItemAction.ClockedIn,
                    ShiftItemAction.Leave,
                    ShiftItemAction.Absent,
                    ShiftItemAction.Remove,
                ];
                break;

            case ShiftItemStatus.ClockedIn:
                actions = [
                    ShiftItemAction.ClockedOut,
                    ShiftItemAction.Leave,
                    ShiftItemAction.Absent
                ];
                break;

            case ShiftItemStatus.ClockedOut:
                actions = [];
                break;

            case ShiftItemStatus.ApprovedOff:
                actions = [];
                break;

            case ShiftItemStatus.OffWithoutApproved:
                actions = [];
                break;
        }
        return actions;
    }

    onClickAddStaff(ngbPopover?: NgbPopover) {
        if (ngbPopover && typeof ngbPopover.close === 'function') {
            ngbPopover.close();
        }

        this.showAddStaff();
    }

    onChangeSearch() {
        this.getInternalItemsOfShiftBlock();
    }

    onChangeRole() {
        this.getInternalItemsOfShiftBlock();
    }

    onChangeSelectedAll() {
        if (this.selectedAll) {
            if (this.internalShiftItems && this.internalShiftItems.length > 0) {
                for (const index in this.internalShiftItems) {
                    this.selectedShiftItems[index] = true;
                }
            }
        } else {
            if (this.internalShiftItems && this.internalShiftItems.length > 0) {
                for (const index in this.internalShiftItems) {
                    this.selectedShiftItems[index] = false;
                }
            }
        }
    }

    onChangeSelectedShiftItem() {
        let selectedAll = true;
        for (const index in this.internalShiftItems) {
            if (!this.selectedShiftItems[index]) {
                selectedAll = false;
                break;
            }
        }
        this.selectedAll = selectedAll;
    }

    // == popover show detail users ==

    getPopoverPlacement() {
        return (
            this.dayOfWeek === 1 ||
            this.dayOfWeek === 2 ||
            this.dayOfWeek === 3
        )
            ? ['right', 'bottom']
            : ['left', 'bottom'];
    }

    onClickPopover(ngbPopover?: NgbPopover) {
        if (this.shiftBlock &&
            this.shiftBlock.items &&
            this.shiftBlock.items.length > 0) {
            this.toggle(ngbPopover);
        } else {
            if (this.isDisabledAddStaffs) {
                this.toggle(ngbPopover);
            } else {
                this.showAddStaff();
            }
        }
    }

    toggle(ngbPopover: NgbPopover) {
        if (ngbPopover) {
            if (ngbPopover.isOpen()) {
                this.closePopover(ngbPopover);
            } else {
                ngbPopover.open();
            }
        }
    }

    showAddStaff() {


        const modalRef = this.ngbModal.open(CxiCalendarWeekAddStaffComponent, {centered: true, windowClass: 'cxi-calendar-week-add-staff'});
        modalRef.componentInstance.shiftBlock = this.shiftBlock;
        from(modalRef.result)
            .pipe(
                catchError((e) => throwError(e)),
                switchMap(
                    (staffIds: string[]) => this.addStaff$(staffIds)
                )
            )
            .subscribe(
            );
    }

    addStaff$(staffIds: string[]) {
        return this.shiftsRepository.assignShiftForStaffs(this.shiftBlock, staffIds)
            .pipe(
                tap((shiftBlock: ShiftBlock) => this.shiftBlock = shiftBlock),
                tap(() => this.assign.emit(staffIds))
            );
    }

    closePopover(ngbPopover: NgbPopover) {
        ngbPopover.close();

        // reset form in popover
        this.search = null;
        this.roleID = null;
    }


    // == popover show more ==

    onClickOnAction(action, shiftItem: ShiftItem) {
        if (action === ShiftItemAction.Remove) {
            this.removeStaff(shiftItem);
        } else {
            this.callApiForChangeStatus(action, shiftItem);
            console.log();
        }
    }

    removeStaff(shiftItem: ShiftItem) {
        this.shiftsRepository.unassignShiftForStaffs(shiftItem)
            .pipe(
                tap(() => this.handleAfterRemovedStaff(shiftItem))
            )
            .subscribe();
    }

    handleAfterRemovedStaff(shiftItem: ShiftItem) {
        this.shiftBlock.items =
            this.shiftBlock.items
                .filter(
                    item => item.id !== shiftItem.id
                );

        this.getInternalItemsOfShiftBlock();

        // emit
        this.removedShiftItem.emit(shiftItem);

        // close..
    }

    callApiForChangeStatus(action: string, shiftItem: ShiftItem) {
        this.shiftsRepository.changeStatusShiftItems([shiftItem], action)
            .pipe(
                map(
                    (shiftItems: ShiftItem[]): ShiftItem => shiftItems && shiftItems.length > 0 ? shiftItems[0] : null
                ),
                tap(newShiftItem => {
                    const user = _.cloneDeep(shiftItem.user);
                    shiftItem.fill(newShiftItem);
                    shiftItem.user = user;
                })
            )
            .subscribe();
    }
}
