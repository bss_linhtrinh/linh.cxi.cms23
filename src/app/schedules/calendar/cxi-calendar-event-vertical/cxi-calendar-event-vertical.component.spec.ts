import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiCalendarEventVerticalComponent } from './cxi-calendar-event-vertical.component';

describe('CxiCalendarEventVerticalComponent', () => {
  let component: CxiCalendarEventVerticalComponent;
  let fixture: ComponentFixture<CxiCalendarEventVerticalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiCalendarEventVerticalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiCalendarEventVerticalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
