import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiCalendarComponent } from './cxi-calendar.component';

describe('CxiCalendarComponent', () => {
  let component: CxiCalendarComponent;
  let fixture: ComponentFixture<CxiCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
