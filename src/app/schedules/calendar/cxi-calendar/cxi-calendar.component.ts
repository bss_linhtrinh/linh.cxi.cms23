import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LocalStorage} from 'ngx-webstorage';
import * as moment from 'moment';
import {Moment} from 'moment';
import {timer} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {Role} from '../../../shared/entities/role';
import {RolesRepository} from '../../../shared/repositories/roles.repository';

@Component({
    selector: 'app-cxi-calendar',
    templateUrl: './cxi-calendar.component.html',
    styleUrls: ['./cxi-calendar.component.scss']
})
export class CxiCalendarComponent implements OnInit, AfterViewInit {
    @Input() name: string;

    shiftData: any[] = [];

    filterShiftIds: string[];
    filterRoleIds: number[];

    roles: Role[] = [];

    @Output() nextDate = new EventEmitter();
    @Output() prevDate = new EventEmitter();
    @Output() typeViewChange = new EventEmitter<string>();


    @LocalStorage('calendar.isIsoWeek') isIsoWeek: boolean;
    @LocalStorage('calendar.typeView') typeView: string;
    @LocalStorage('calendar.fromDate') fromDate: string;
    @LocalStorage('calendar.toDate') toDate: string;
    @LocalStorage('calendar.selectedDate') selectedDate: string;
    @LocalStorage('calendar.isCurrentTime') isCurrentTime: boolean;

    isInitializing = true;
    isChangedDateWhileInit = false;

    constructor(
        private rolesRepository: RolesRepository
    ) {
        this.isIsoWeek = true;
    }

    ngOnInit() {
        this.getRoles();
        this.initCalendarDate();
    }

    ngAfterViewInit(): void {
    }

    initCalendarDate() {

        // typeView
        if (!this.typeView) {
            this.typeView = 'week';
        }

        const todayDay = moment().format('YYYY-MM-DD');

        // formDate
        switch (this.typeView) {
            case 'day':
                if (
                    !this.fromDate ||
                    (this.isInitializing && this.fromDate !== todayDay)
                ) {
                    this.fromDate = todayDay;
                    this.isChangedDateWhileInit = true;
                }
                // if (this.selectedDate !== this.fromDate) {
                //     this.selectedDate = this.fromDate;
                // }
                break;

            case 'week':
                if (!this.fromDate || this.fromDate === 'Invalid date') {
                    this.fromDate = todayDay;
                }
                if (this.isIsoWeek) {
                    this.fromDate = moment(this.fromDate).startOf('isoWeek').format('YYYY-MM-DD');
                } else {
                    this.fromDate = moment(this.fromDate).startOf('weeks').format('YYYY-MM-DD');
                }
                break;

            case 'month':
                break;
        }

        // toDate
        this.calcToDate();

        this.isInitializing = false;
    }

    getRoles() {
        this.rolesRepository.all({pagination: 0})
            .pipe(
                tap((roles: Role[]) => this.roles = roles)
            )
            .subscribe();
    }

    today() {
        this.handleNextPrev(0);
    }

    next() {
        this.handleNextPrev(1);
        this.nextDate.emit();
    }

    prev() {
        this.handleNextPrev(-1);
        this.prevDate.emit();
    }

    handleNextPrev(next: number) { // -1, 0, 1
        let fromDate: string;
        let momentDate: Moment;

        if (!next) {
            switch (this.typeView) {
                case 'day':
                default:
                    fromDate = moment().format('YYYY-MM-DD');
                    break;

                case 'week':
                    if (this.isIsoWeek) {
                        momentDate = moment().startOf('isoWeek');  // Sunday first
                    } else {
                        momentDate = moment().startOf('weeks'); // Sunday first
                    }
                    fromDate = momentDate.format('YYYY-MM-DD');
                    break;

                case 'month':
                    break;
            }
        } else {
            switch (this.typeView) {
                case 'day':
                default:
                    fromDate = moment(this.fromDate)
                        .add(next, 'days')
                        .format('YYYY-MM-DD');
                    break;

                case 'week':
                    momentDate = moment(this.fromDate).add(next, 'weeks');
                    if (this.isIsoWeek) {
                        momentDate = momentDate.startOf('isoWeek');  // Sunday first
                    } else {
                        momentDate = momentDate.startOf('weeks'); // Sunday first
                    }
                    fromDate = momentDate.format('YYYY-MM-DD');
                    break;

                case 'month':
                    momentDate = moment(this.fromDate)
                        .add(next, 'months')
                        .startOf('months');
                    fromDate = momentDate
                        .format('YYYY-MM-DD');
                    break;
            }
        }

        if (this.fromDate !== fromDate) {
            this.fromDate = fromDate;

            this.calcToDate();
        }
    }

    calcToDate() {
        switch (this.typeView) {
            case 'day':
            default:
                this.toDate = this.fromDate;
                break;

            case 'week':
                this.toDate = moment(this.fromDate)
                    .add(1, 'weeks')
                    .subtract(1, 'days')
                    .format('YYYY-MM-DD');
                break;

            case 'month':
                this.toDate = moment(this.fromDate)
                    .add(1, 'months')
                    .startOf('months')
                    .format('YYYY-MM-DD');
                break;
        }

        // check is Current Time
        // day: today
        // week: this week
        // month: this month
        this.checkIsCurrentTime$()
            .subscribe();

        // Is change while init => reload data
        if (this.isChangedDateWhileInit) {
            this.isChangedDateWhileInit = false;
        }
    }

    checkIsCurrentTime$() {
        return timer(1)
            .pipe(
                map(
                    () => this.checkIsCurrentTime()
                ),
                tap(
                    (isCurrentTime: boolean) => this.setDefaultSelectedDateByIsCurrentTime(isCurrentTime)
                )
            );
    }

    checkIsCurrentTime(): boolean {
        const today = moment();
        const fromDate = moment(this.fromDate);
        const toDate = moment(this.toDate);

        let isCurrentTime: boolean;

        switch (this.typeView) {
            case 'day':
                isCurrentTime = (today.format('YYYY-MM-DD') === this.fromDate);
                break;

            case 'week':
                isCurrentTime = today.isBetween(fromDate, toDate);
                break;

            case 'month':
                isCurrentTime = today.isBetween(fromDate, toDate);
                break;
        }

        this.isCurrentTime = isCurrentTime;

        return isCurrentTime;
    }

    onChangeTypeView(typeView: string) {
        this.initCalendarDate();

        console.log('- onchange type view', typeView);
        console.log('- onchange type view', this.typeView);
        console.log('- onchange type view', this.fromDate);
        console.log('- onchange type view', this.toDate);
    }

    setDefaultSelectedDateByIsCurrentTime(isCurrentTime: boolean) {
        let defaultSelectedDate: string;

        // selectedDate
        if (isCurrentTime) {
            defaultSelectedDate = moment().format('YYYY-MM-DD');

            switch (this.typeView) {
                case 'day':
                    if (this.selectedDate !== defaultSelectedDate) {
                        this.selectedDate = defaultSelectedDate;
                        this.onChangeSelectedDate();
                    }
                    break;
                case 'week':
                default:
                    break;
                case 'month':
                    break;
            }


        } else {
            defaultSelectedDate = this.fromDate;

            if (this.selectedDate !== defaultSelectedDate) {
                this.selectedDate = defaultSelectedDate;
            }
        }
    }

    onChangeSelectedDate(selectedDate?: string) {
        this.initCalendarDate();
    }

    // shift -role
    onChangeShiftIds(shiftIds: string[]) {
        if (this.filterShiftIds !== shiftIds) {
            this.filterShiftIds = shiftIds;
        }
    }

    onChangeRoleIds(roleIds: number[]) {
        if (this.filterRoleIds !== roleIds) {
            this.filterRoleIds = roleIds;
        }
    }
}
