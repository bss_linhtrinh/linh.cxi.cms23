import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {tap} from 'rxjs/operators';
import {ShiftBlock} from '../../shift-block';
import {ShiftsRepository} from '../../../shared/repositories/shifts.repositories';
import {Salesman} from '../../salesman';

@Component({
    selector: 'app-cxi-calendar-week-add-staff',
    templateUrl: './cxi-calendar-week-add-staff.component.html',
    styleUrls: ['./cxi-calendar-week-add-staff.component.scss']
})
export class CxiCalendarWeekAddStaffComponent implements OnInit, OnChanges {

    selectedAll: any;
    selectedStaffs = [];

    @Input() search: any;
    @Input() role: any;
    @Input() shiftBlock: ShiftBlock;
    staffs: Salesman[] = [];

    isDisabledAddStaff: boolean;

    constructor(
        private ngbActiveModal: NgbActiveModal,
        private shiftsRepository: ShiftsRepository
    ) {
    }

    ngOnInit() {
        this.getFreeStaffsForThisShiftBlock();
        this.checkIsDisabledAddStaff();
    }

    ngOnChanges(changes: SimpleChanges): void {
        //
    }

    getFreeStaffsForThisShiftBlock() {

        // params
        const params: any = {};
        if (this.search) {
            params.name = this.search;
        }
        if (this.role) {
            params.role = this.role;
        }

        const freeStaff$ = this.shiftsRepository.getFreeStaffsForShiftBlock(this.shiftBlock, params)
            .pipe(
                tap((staffs: Salesman[]) => this.staffs = staffs)
            );

        freeStaff$
            .subscribe();

        return freeStaff$;
    }

    close() {
        this.ngbActiveModal.dismiss('cancel');
    }

    add() {
        const staffIds = this.staffs
            .filter(
                (staff: Salesman, index: number) => {
                    return this.selectedStaffs[index];
                }
            )
            .map(
                (staff: Salesman) => {
                    return staff.id;
                }
            );

        this.ngbActiveModal.close(staffIds);
    }

    onChangeSelectedAll(selectedAll: boolean) {
        if (this.selectedAll) {
            for (const index in this.staffs) {
                this.selectedStaffs[index] = true;
            }
        } else {
            for (const index in this.staffs) {
                this.selectedStaffs[index] = false;
            }
        }

        this.checkIsDisabledAddStaff();
    }

    onSelectedStaff() {
        let selectedAll = true;
        for (const index in this.staffs) {
            if (!this.selectedStaffs[index]) {
                selectedAll = false;
                break;
            }
        }
        this.selectedAll = selectedAll;

        this.checkIsDisabledAddStaff();
    }

    onChangeSearch() {
        this.getFreeStaffsForThisShiftBlock();
    }

    onChangeRole() {
        this.getFreeStaffsForThisShiftBlock();
    }

    checkIsDisabledAddStaff() {
        this.isDisabledAddStaff = !this.selectedStaffs ||
            (
                !this.selectedStaffs.filter(selection => !!selection)
                    .length
            );
    }
}
