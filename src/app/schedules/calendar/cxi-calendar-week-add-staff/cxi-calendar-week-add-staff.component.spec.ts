import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiCalendarWeekAddStaffComponent } from './cxi-calendar-week-add-staff.component';

describe('CxiCalendarWeekAddStaffComponent', () => {
  let component: CxiCalendarWeekAddStaffComponent;
  let fixture: ComponentFixture<CxiCalendarWeekAddStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiCalendarWeekAddStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiCalendarWeekAddStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
