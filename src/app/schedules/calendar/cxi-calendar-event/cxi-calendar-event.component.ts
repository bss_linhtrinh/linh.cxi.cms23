import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ShiftItemStatus} from '../../shift-item-status';
import {ShiftItem} from '../../shift-item';
import {SHIFT_ITEM_ACTIONS} from '../../shift-item-actions';
import {ShiftItemAction} from '../../shift-item-action';
import {ShiftsRepository} from '../../../shared/repositories/shifts.repositories';
import {filter, map, tap} from 'rxjs/operators';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import * as _ from 'lodash';

@Component({
    selector: 'cxi-calendar-event',
    templateUrl: './cxi-calendar-event.component.html',
    styleUrls: ['./cxi-calendar-event.component.scss']
})
export class CxiCalendarEventComponent implements OnInit {
    @Input() name: string;
    @Input() shiftItem: ShiftItem;

    @Input('width.rem') widthRem: number; // rem

    @Output() removedShiftItem = new EventEmitter<ShiftItem>();

    shiftItemActions = SHIFT_ITEM_ACTIONS;

    constructor(
        private shiftsRepository: ShiftsRepository,
        private utils: UtilitiesService
    ) {
    }

    ngOnInit() {
    }

    getWidth() {
        return this.widthRem ? this.widthRem : (250 / 14);
    }

    getBackground() {
        let background: string;

        const backgroundColor = this.shiftItem.backgroundColor
            ? this.shiftItem.backgroundColor
            : '#fff';

        switch (this.shiftItem.status) {
            case ShiftItemStatus.ClockedIn:
            case ShiftItemStatus.ClockedOut:
            default:
                background = backgroundColor;
                break;

            case ShiftItemStatus.ApprovedOff:
                const lightenColor = (backgroundColor !== '#fff' && backgroundColor !== 'white')
                    ? this.utils.lightenDarkenColor(backgroundColor, 20)
                    : '#000';

                const stripeColor = '#e4e4e6'; // #e0e0e0
                const lightStripeColor = '#f4f4f6';
                const stripeWidth = 6;

                background = `repeating-linear-gradient(` +
                    ` 135deg,` +
                    ` ${stripeColor},` +
                    ` ${stripeColor} ${stripeWidth}px,` +
                    ` ${lightStripeColor} ${stripeWidth}px,` +
                    ` ${lightStripeColor} ${2 * stripeWidth}px` +
                    `)`;
                break;

            case ShiftItemStatus.OffWithoutApproved:
                background = '#f4f4f6';
                break;
        }


        return background;
    }

    onClickOnAction(clickedAction: string) {
        const actions = this.getActionsByStatus(this.shiftItem.status);
        if (actions.findIndex(action => action === clickedAction) === -1) {
            return;
        }

        if (clickedAction === ShiftItemAction.Remove) {
            this.removeShiftItemFor();
        } else {
            this.callApiForChangeStatus(clickedAction);
        }
    }

    isShow(currentAct: string): boolean {
        let isShow = true;

        if (this.shiftItemActions.findIndex(act => act === currentAct) === -1) {
            return false;
        }

        const actions = this.getActionsByStatus(this.shiftItem.status);
        isShow = actions.findIndex(action => action === currentAct) !== -1;

        return isShow;
    }

    getActionsByStatus(status: string) {
        let actions: ShiftItemAction[];
        switch (status) {
            case null:
            default:
                actions = [
                    ShiftItemAction.ClockedIn,
                    ShiftItemAction.Leave,
                    ShiftItemAction.Absent,
                    ShiftItemAction.Remove,
                ];
                break;

            case ShiftItemStatus.ClockedIn:
                actions = [
                    ShiftItemAction.ClockedOut,
                    ShiftItemAction.Leave,
                    ShiftItemAction.Absent
                ];
                break;

            case ShiftItemStatus.ClockedOut:
                actions = [];
                break;

            case ShiftItemStatus.ApprovedOff:
                actions = [];
                break;

            case ShiftItemStatus.OffWithoutApproved:
                actions = [];
                break;
        }
        return actions;
    }

    removeShiftItemFor() {
        this.shiftsRepository.unassignShiftForStaffs(this.shiftItem)
            .pipe(
                tap(() => this.removedShiftItem.emit(this.shiftItem))
            )
            .subscribe();
    }

    callApiForChangeStatus(action: string) {
        this.shiftsRepository.changeStatusShiftItems([this.shiftItem], action)
            .pipe(
                map(
                    (shiftItems: ShiftItem[]): ShiftItem => shiftItems && shiftItems.length > 0 ? shiftItems[0] : null
                ),
                filter((shiftItem: ShiftItem) => !!shiftItem),
                tap((shiftItem: ShiftItem) => {
                    const statistics = _.cloneDeep(this.shiftItem.statistics);

                    this.shiftItem = shiftItem;
                    this.shiftItem.statistics = statistics;
                })
            )
            .subscribe();
    }
}

