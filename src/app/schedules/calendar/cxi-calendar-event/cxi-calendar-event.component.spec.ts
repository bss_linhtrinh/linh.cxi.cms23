import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiCalendarEventComponent } from './cxi-calendar-event.component';

describe('CxiCalendarEventComponent', () => {
  let component: CxiCalendarEventComponent;
  let fixture: ComponentFixture<CxiCalendarEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiCalendarEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiCalendarEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
