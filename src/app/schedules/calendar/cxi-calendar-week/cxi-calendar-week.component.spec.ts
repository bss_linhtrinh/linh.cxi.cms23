import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiCalendarWeekComponent } from './cxi-calendar-week.component';

describe('CxiCalendarWeekComponent', () => {
  let component: CxiCalendarWeekComponent;
  let fixture: ComponentFixture<CxiCalendarWeekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiCalendarWeekComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiCalendarWeekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
