import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Moment} from 'moment';
import * as moment from 'moment';
import {LocalStorage, LocalStorageService} from 'ngx-webstorage';
import {delay, tap} from 'rxjs/operators';
import {ShiftCalendarWeek} from '../../shift-calendar-week';
import {Role} from '../../../shared/entities/role';
import {ShiftBlock} from '../../shift-block';
import {ShiftItem} from '../../shift-item';
import {REM} from '../../../shared/constants/app';
import {ShiftCalendarRepository} from '../../../shared/repositories/shift-calendar.repository';
import * as _ from 'lodash';

@Component({
    selector: 'app-cxi-calendar-week',
    templateUrl: './cxi-calendar-week.component.html',
    styleUrls: ['./cxi-calendar-week.component.scss']
})
export class CxiCalendarWeekComponent implements OnInit, OnChanges {

    @Input() name: string;
    @Input() roles: Role[] = [];

    daysOfWeek: any;
    shiftCalendarsWeek: ShiftCalendarWeek = null;

    internalShiftCalendarsWeek: ShiftCalendarWeek;

    @Input() fromDate: string;
    @Input() toDate: string;
    @LocalStorage('calendar.isIsoWeek') isIsoWeek: boolean;
    @LocalStorage('calendar.typeView') typeView: string;

    @Input() filterShiftIds: string[];
    @Input() filterRoleIds: number[];

    _shiftData: any[];
    @Input()
    get shiftData(): any[] {
        return this._shiftData;
    }

    @Output() shiftDataChange = new EventEmitter<any[]>();

    set shiftData(shiftData: any[]) {
        this._shiftData = shiftData;
        this.shiftDataChange.emit(this.shiftData);
    }

    hours = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];

    @Output() assignedStaffs = new EventEmitter();

    cellSizeRem = 56 / REM;

    constructor(
        private localStorageService: LocalStorageService,
        private shiftCalendarRepository: ShiftCalendarRepository
    ) {
        this.observeToDate();
    }

    ngOnInit() {
        this.getDaysOfWeek();
        this.loadData();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes) {
            if (
                (changes.filterShiftIds && changes.filterShiftIds.previousValue !== changes.filterShiftIds.currentValue) ||
                (changes.filterRoleIds && changes.filterRoleIds.previousValue !== changes.filterRoleIds.currentValue)
            ) {
                this.getInternalShiftCalendars();
            }
            if (
                (
                    changes.fromDate &&
                    !changes.fromDate.firstChange &&
                    changes.fromDate.previousValue !== changes.fromDate.currentValue
                ) ||
                (
                    changes.toDate &&
                    !changes.toDate.firstChange &&
                    changes.toDate.previousValue !== changes.toDate.currentValue
                )
            ) {
                this.loadData();
            }
        }
    }

    observeToDate() {
        this.localStorageService.observe('calendar.toDate')
            .pipe(
                delay(1),
                tap(
                    () => this.getDaysOfWeek()
                )
            )
            .subscribe((value) => console.log('new value', value));
    }

    getDaysOfWeek() {
        const daysOfWeek: any[] = [];
        let startOfWeek: Moment;

        if (this.isIsoWeek) {
            startOfWeek = moment(this.fromDate).startOf('isoWeek');
        } else {
            startOfWeek = moment(this.fromDate).startOf('weeks');
        }

        for (let i = 0; i < 7; i++) {
            const d = startOfWeek.clone().add(i, 'day');
            daysOfWeek.push(d);
        }

        this.daysOfWeek = daysOfWeek;
    }

    loadData() {
        this.getShiftCalendars();
    }

    getShiftCalendars() {
        if (!this.fromDate || !this.toDate) {
            return;
        }

        const typeView = this.typeView;
        const params = {
            fromDate: moment(this.fromDate).format('YYYY-MM-DD'),
            toDate: moment(this.toDate).format('YYYY-MM-DD'),
            type: typeView
        };
        this.shiftCalendarRepository.getCalendar(params)
            .pipe(
                tap(
                    (data: any) => {
                        if (data.shiftData) {
                            this.shiftData = data.shiftData;
                        }

                        if (data.shiftCalendarsWeek) {
                            this.shiftCalendarsWeek = data.shiftCalendarsWeek;
                        }
                        this.getInternalShiftCalendars();
                    }
                )
            )
            .subscribe();
    }

    getInternalShiftCalendars() {

        const internalShiftCalendarsWeek = _.cloneDeep(this.shiftCalendarsWeek);

        // filter by Shift
        for (const i in internalShiftCalendarsWeek) {
            const dayOfWeekShiftBlocks = internalShiftCalendarsWeek[i];
            if (this.filterShiftIds) {
                internalShiftCalendarsWeek[i] =
                    dayOfWeekShiftBlocks
                        .filter(
                            (shiftBlock: ShiftBlock) => {
                                return shiftBlock.shift &&
                                    this.filterShiftIds.includes(shiftBlock.shift);
                            }
                        );
            }
        }


        // filter by Roles
        for (const i in internalShiftCalendarsWeek) {
            const dayOfWeekShiftBlocks = internalShiftCalendarsWeek[i];
            if (this.filterRoleIds) {
                for (const j in dayOfWeekShiftBlocks) {
                    const shiftBlock = dayOfWeekShiftBlocks[j];
                    if (shiftBlock.items) {
                        internalShiftCalendarsWeek[i][j].items =
                            shiftBlock.items
                                .filter(
                                    (shiftItem: ShiftItem) => {
                                        if (shiftItem.user &&
                                            shiftItem.user.role &&
                                            shiftItem.user.role.id) {
                                            const roleId = parseInt(shiftItem.user.role.id, 10);
                                            return this.filterRoleIds.includes(roleId);
                                        }
                                        return false;
                                    }
                                );
                    }
                }
            }
        }


        if (
            !(_.isEqual(internalShiftCalendarsWeek, this.internalShiftCalendarsWeek))
        ) {
            this.internalShiftCalendarsWeek = internalShiftCalendarsWeek;
        }
    }

    isToday(date: Date): boolean {
        const today = moment().format('YYYY-MM-DD');
        const momentDate = moment(date).format('YYYY-MM-DD');
        return momentDate === today;
    }

    getHeightHoursRange() {
        return 12 * this.cellSizeRem;
    }

    hourTransform(h: number) {
        if (h > 12 && h < 24) {
            h = h - 12;
        }
        if (h === 24) {
            h = h - 24;
        }

        if (h < 10) {
            return `0${h}`;
        } else {
            return `${h}`;
        }
    }

    daylight(h: number) {
        if (h < 12 || h >= 24) {
            return `AM`;
        } else {
            return `PM`;
        }
    }

    getPositionTopEventVertical(shiftBlock: ShiftBlock) {
        let top = 0;
        const startOfDate = moment(shiftBlock.from).startOf('day').clone();
        const from = moment(shiftBlock.from);
        const difHour = from.diff(startOfDate, 'minutes');
        top = difHour ? ((difHour / 60) * this.cellSizeRem) : 0;
        return top;
    }

    getPositionLeftEventVertical(dayOfWeek: Moment) {
        let left = 0;

        // sunday first
        // left += dayOfWeek / 7 * 100;

        // monday first
        const day = dayOfWeek.day();
        left += (day - 1 >= 0 ? day - 1 : 6) / 7 * 100;

        return left;
    }

    getHeightEventVertical(shiftBlock: ShiftBlock) {
        let height = 0;
        const fromMoment = moment(shiftBlock.from);
        const toMoment = moment(shiftBlock.to);
        const difHour = toMoment.diff(fromMoment, 'minutes');
        height = difHour
            ? (difHour / 60) * this.cellSizeRem
            : 0;
        return height;
    }

    getPositionTopNowAxisVertical() {
        let top = 0;
        const startOfDate = moment().startOf('day').clone();
        const now = moment();
        const diffMinutes = now.diff(startOfDate, 'minutes');
        top += (diffMinutes / 60) * this.cellSizeRem;
        return top;
    }

    getPositionLeftNowAxisVertical(index: number) {
        let left = 0;
        left = index / 7 * 100;
        return left;
    }

    getPositionLeftNowAxisCircleVertical(index: number) {
        let top = 0;
        top += (index ? 1 : 0) * this.cellSizeRem;
        top -= (8 / 14);
        return top;
    }

    onAssignStaffs() {
        this.loadData();
    }

    onRemovedShiftItem(shiftItem: ShiftItem, shiftBlock?: ShiftBlock) {
        if (shiftItem) {
            for (const index in this.internalShiftCalendarsWeek) {
                const shiftCalendarWeekShiftBlocks = this.internalShiftCalendarsWeek[index];

                if (shiftCalendarWeekShiftBlocks &&
                    shiftCalendarWeekShiftBlocks.length) {
                    this.internalShiftCalendarsWeek[index] =
                        shiftCalendarWeekShiftBlocks
                            .map(
                                (block: ShiftBlock) => {
                                    if (block &&
                                        block.items &&
                                        block.items.length &&
                                        block.shift === shiftBlock.shift &&
                                        block.date >= shiftBlock.date) {

                                        const index2 = block.items
                                            .findIndex(
                                                item => {
                                                    return shiftItem.user &&
                                                        item.user &&
                                                        item.user.first_name === shiftItem.user.first_name &&
                                                        item.user.last_name === shiftItem.user.last_name;
                                                }
                                            );
                                        if (index2 !== -1) {
                                            block.items.splice(index2, 1);
                                        }
                                    }

                                    return block;
                                }
                            );
                }
            }
        }
    }
}
