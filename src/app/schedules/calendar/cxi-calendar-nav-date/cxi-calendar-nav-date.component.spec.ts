import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiCalendarNavDateComponent } from './cxi-calendar-nav-date.component';

describe('CxiCalendarNavDateComponent', () => {
  let component: CxiCalendarNavDateComponent;
  let fixture: ComponentFixture<CxiCalendarNavDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiCalendarNavDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiCalendarNavDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
