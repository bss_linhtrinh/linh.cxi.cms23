import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {LocalStorage} from 'ngx-webstorage';
import {Moment} from 'moment';
import * as moment from 'moment';

@Component({
    selector: 'cxi-calendar-nav-date',
    templateUrl: './cxi-calendar-nav-date.component.html',
    styleUrls: ['./cxi-calendar-nav-date.component.scss']
})
export class CxiCalendarNavDateComponent implements OnInit {

    @Output() changeTypeView = new EventEmitter<string>();
    @Output() changeSelectedDate = new EventEmitter<string>();

    @LocalStorage('calendar.isIsoWeek') isIsoWeek: boolean;
    @LocalStorage('calendar.typeView') typeView: string;
    @LocalStorage('calendar.fromDate') fromDate: string;
    @LocalStorage('calendar.toDate') toDate: string;
    @LocalStorage('calendar.selectedDate') selectedDate: string;
    @LocalStorage('calendar.isCurrentTime') isCurrentTime: boolean;

    constructor() {
    }

    ngOnInit() {
    }


    selectTypeView(typeView: string) {
        if (typeView !== this.typeView) {
            this.typeView = typeView;
            this.changeTypeView.emit();

            console.log('--on select type view');
        }
    }

    onChangeSelectedDate() {
        console.log('--on change selected date');

        switch (this.typeView) {
            case 'day':
                if (this.fromDate !== this.selectedDate) {
                    this.fromDate = this.selectedDate;

                    this.changeSelectedDate.emit(this.selectedDate);
                }
                break;

            case 'week':
            default:
                let adjustFromDate: Moment;
                let adjustToDate: Moment;

                adjustFromDate = moment(this.fromDate)
                    .startOf(
                        this.isIsoWeek
                            ? 'isoWeek'
                            : 'weeks'
                    );
                adjustToDate = adjustFromDate
                    .clone()
                    .endOf(
                        this.isIsoWeek
                            ? 'isoWeek'
                            : 'weeks'
                    );

                while (
                    !moment(this.selectedDate).isBetween(
                        adjustFromDate.format('YYYY-MM-DD'),
                        adjustToDate.format('YYYY-MM-DD')
                    ) &&
                    !moment(this.selectedDate).isSame(adjustFromDate.format('YYYY-MM-DD')) &&
                    !moment(this.selectedDate).isSame(adjustToDate.format('YYYY-MM-DD'))
                    ) {

                    // before
                    if (
                        moment(this.selectedDate).isBefore(adjustFromDate)
                    ) {
                        adjustFromDate = adjustFromDate.subtract(1, 'weeks')
                            .startOf(
                                this.isIsoWeek
                                    ? 'isoWeek'
                                    : 'weeks'
                            );
                    } else {

                        // after
                        if (moment(this.selectedDate).isAfter(adjustToDate)) {
                            adjustFromDate = adjustFromDate.add(1, 'weeks')
                                .startOf(
                                    this.isIsoWeek
                                        ? 'isoWeek'
                                        : 'weeks'
                                );
                        }
                    }

                    // adjust
                    adjustToDate = adjustFromDate
                        .clone()
                        .endOf(
                            this.isIsoWeek
                                ? 'isoWeek'
                                : 'weeks'
                        );
                }

                if (this.fromDate !== adjustFromDate.format('YYYY-MM-DD')) {
                    this.fromDate = adjustFromDate.format('YYYY-MM-DD');

                    this.changeSelectedDate.emit(this.selectedDate);
                }
                break;

            case 'month':
                break;

        }
    }

    openDate() {
    }
}
