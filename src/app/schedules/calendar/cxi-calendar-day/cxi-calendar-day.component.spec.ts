import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiCalendarDayComponent } from './cxi-calendar-day.component';

describe('CxiCalendarDayComponent', () => {
  let component: CxiCalendarDayComponent;
  let fixture: ComponentFixture<CxiCalendarDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiCalendarDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiCalendarDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
