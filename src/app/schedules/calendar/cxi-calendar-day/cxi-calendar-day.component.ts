import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {ShiftCalendarDayUser} from '../../shift-calendar-day-user';
import * as _ from 'lodash';
import {ShiftCalendarDay} from '../../shift-calendar-day';
import {ShiftItem} from '../../shift-item';
import {ShiftBlock} from '../../shift-block';
import {PerfectScrollbarComponent} from 'ngx-perfect-scrollbar';
import * as moment from 'moment';
import {REM} from '../../../shared/constants/app';
import {ShiftCalendar} from '../../shift-calendar';
import {tap} from 'rxjs/operators';
import {ShiftCalendarRepository} from '../../../shared/repositories/shift-calendar.repository';
import {ShiftsRepository} from '../../../shared/repositories/shifts.repositories';
import {ShiftBlocksRepository} from '../../../shared/repositories/shift-blocks.repository';
import {LocalStorage} from 'ngx-webstorage';

@Component({
    selector: 'app-cxi-calendar-day',
    templateUrl: './cxi-calendar-day.component.html',
    styleUrls: ['./cxi-calendar-day.component.scss']
})
export class CxiCalendarDayComponent implements OnInit, OnChanges {
    @Input() name: string;

    hours = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24];

    shiftCalendarsDay: ShiftCalendarDay[] = [];
    shiftBlocks: ShiftBlock[] = [];

    internalShiftCalendarsDay: ShiftCalendarDay[];
    internalShiftItemDayUsers: ShiftCalendarDayUser[] = [];

    filterUserId: string;
    @Input() filterShiftIds: string[];
    @Input() filterRoleIds: number[];

    _shiftData: any[];
    @Input()
    get shiftData(): any[] {
        return this._shiftData;
    }

    @Output() shiftDataChange = new EventEmitter<any[]>();

    set shiftData(shiftData: any[]) {
        this._shiftData = shiftData;
        this.shiftDataChange.emit(this.shiftData);
    }


    @Input() fromDate: string;
    @Input() toDate: string;
    @LocalStorage('calendar.isIsoWeek') isIsoWeek: boolean;
    @LocalStorage('calendar.typeView') typeView: string;

    scrollDayX = 0;
    scrollDayY = 0;
    perfectScrollbarConfigDayHour = {
        useBothWheelAxes: false,
        suppressScrollX: false,
        suppressScrollY: true,
    };
    perfectScrollbarConfigSidebarUser = {
        useBothWheelAxes: false,
        suppressScrollX: true,
        suppressScrollY: false,
    };
    @ViewChild('perfectScrollbarDayHour', {static: false}) perfectScrollbarDayHour: PerfectScrollbarComponent;
    @ViewChild('perfectScrollbarSidebarUser', {static: false}) perfectScrollbarSidebarUser: PerfectScrollbarComponent;


    cellGapLeft = 28 / REM; // rem
    cellSizeRem = 56 / REM;
    MIN_ROWS = 10;

    constructor(
        private shiftCalendarRepository: ShiftCalendarRepository,
        private shiftsRepository: ShiftsRepository,
        private shiftBlocksRepository: ShiftBlocksRepository
    ) {
    }

    ngOnInit() {
        this.loadData();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes) {
            if (
                (changes.filterShiftIds && changes.filterShiftIds.previousValue !== changes.filterShiftIds.currentValue) ||
                (changes.filterRoleIds && changes.filterRoleIds.previousValue !== changes.filterRoleIds.currentValue)
            ) {
                this.getInternalShiftCalendars();
            }
            if (
                (
                    changes.fromDate &&
                    !changes.fromDate.firstChange &&
                    changes.fromDate.previousValue !== changes.fromDate.currentValue
                ) ||
                (
                    changes.toDate &&
                    !changes.toDate.firstChange &&
                    changes.toDate.previousValue !== changes.toDate.currentValue
                )
            ) {
                this.loadData();
            }
        }
    }

    loadData() {
        this.getShiftCalendars();
        this.getShiftBlocks();
    }

    getShiftCalendars() {
        if (!this.fromDate || !this.toDate) {
            return;
        }

        const typeView = this.typeView;
        const params = {
            fromDate: moment(this.fromDate).format('YYYY-MM-DD'),
            toDate: moment(this.toDate).format('YYYY-MM-DD'),
            type: typeView
        };
        this.shiftCalendarRepository.getCalendar(params)
            .pipe(
                tap(
                    (data: any) => {
                        if (data.shiftData) {
                            this.shiftData = data.shiftData;
                        }

                        if (data.shiftCalendarsDay) {
                            this.shiftCalendarsDay = data.shiftCalendarsDay;
                        }
                        this.getInternalShiftCalendars();
                        this.getUsersByType();
                    }
                )
            )
            .subscribe();
    }

    getInternalShiftCalendars() {
        let internalShiftCalendarsDay = _.cloneDeep(this.shiftCalendarsDay);

        // filter by user
        if (this.filterUserId &&
            internalShiftCalendarsDay &&
            internalShiftCalendarsDay instanceof Array &&
            internalShiftCalendarsDay.length > 0) {
            internalShiftCalendarsDay =
                internalShiftCalendarsDay.filter(
                    (shiftCalendarDay: ShiftCalendarDay) => shiftCalendarDay.id === this.filterUserId
                );
        }

        // filter by shift
        internalShiftCalendarsDay
            .map(
                (shiftCalendarsDay: ShiftCalendarDay) => {
                    if (this.filterShiftIds) {
                        shiftCalendarsDay.shiftsItem =
                            shiftCalendarsDay.shiftsItem
                                .filter(
                                    (shiftItem: ShiftItem) => {
                                        return this.filterShiftIds.includes(shiftItem.shift);
                                    }
                                );
                    }
                    return shiftCalendarsDay;
                }
            );

        // filter by Role
        if (this.filterRoleIds !== null &&
            this.filterRoleIds instanceof Array) {
            internalShiftCalendarsDay =
                internalShiftCalendarsDay
                    .filter(
                        (shiftCalendarsDay: ShiftCalendarDay) => {
                            if (this.filterRoleIds.length === 0 ||
                                !shiftCalendarsDay.role ||
                                !shiftCalendarsDay.role.id) {
                                return false;
                            }
                            const roleId = parseInt(shiftCalendarsDay.role.id, 10);
                            return this.filterRoleIds.includes(roleId);
                        }
                    );
        }


        if (
            !(_.isEqual(internalShiftCalendarsDay, this.internalShiftCalendarsDay))
        ) {
            this.internalShiftCalendarsDay = internalShiftCalendarsDay;
        }
    }

    getUsersByType() {
        if (this.shiftCalendarsDay &&
            this.shiftCalendarsDay.length > 0) {
            this.internalShiftItemDayUsers = this.shiftCalendarsDay.map(
                shiftCalendarDay => ShiftCalendarDayUser.create(shiftCalendarDay)
            );
        } else {
            this.internalShiftItemDayUsers = [];
        }
    }

    getShiftBlocks() {
        const params = {
            // fromDate: moment(this.fromDate).format('YYYY-MM-DD'),
            // toDate: moment(this.toDate).format('YYYY-MM-DD'),
            date: this.fromDate,
        };
        this.shiftBlocksRepository.all(params)
            .pipe(
                tap((shiftBlocks: any[]) => this.shiftBlocks = shiftBlocks)
            )
            .subscribe();
    }

    getAvailableShiftBlocks(shiftCalendar: ShiftCalendar): ShiftBlock[] {
        // const availableShiftBlocks: ShiftBlock[] = [];
        let shiftBlocks = _.cloneDeep(this.shiftBlocks);
        const nowTime = moment();

        shiftBlocks = shiftBlocks
            .filter(
                (shiftBlock: ShiftBlock) => {
                    const fromDate = moment(shiftBlock.from);
                    return fromDate.isAfter(nowTime);
                }
            )
            .map(
                (shiftBlock: ShiftBlock) => {
                    shiftBlock._disabled = (
                        shiftCalendar.shiftsItem
                            .findIndex(
                                (shiftItem: ShiftItem) => shiftItem.shift === shiftBlock.shift
                            ) !== -1
                    );

                    return shiftBlock;
                }
            );

        return shiftBlocks;
    }

    onChangeFilterUser() {
        this.getInternalShiftCalendars();
    }

    onPsScrollX(data: any) {
        this.scrollDayX = data.target.scrollLeft;
        this.perfectScrollbarDayHour.directiveRef.scrollToX(this.scrollDayX);
    }

    onPsScrollY(data: any) {
        this.scrollDayY = data.target.scrollTop;
        this.perfectScrollbarSidebarUser.directiveRef.scrollToY(this.scrollDayY);
    }

    hourTransform(h: number) {
        if (h > 12 && h < 24) {
            h = h - 12;
        }
        if (h === 24) {
            h = h - 24;
        }

        if (h < 10) {
            return `0${h}`;
        } else {
            return `${h}`;
        }
    }

    daylight(h: number) {
        if (h < 12 || h >= 24) {
            return `AM`;
        } else {
            return `PM`;
        }
    }

    getPositionTopNowAxis(index: number) {
        let top = 0;
        top = (index + 1) * this.cellSizeRem;
        return top;
    }

    getPositionLeftNowAxis() {
        let left = 0;
        const startOfDate = moment(`0:0 AM`, 'h:m A');
        const now = moment();
        const diffMinutes = now.diff(startOfDate, 'minutes');
        left += (diffMinutes / 60) * this.cellSizeRem;
        left += this.cellGapLeft;
        return left;
    }

    getPositionTopNowAxisCircle(index: number) {
        let top = 0;
        top += (index ? 1 : 0) * this.cellSizeRem;
        top -= (8 / 14);
        return top;
    }

    getBodyViewRange() {
        let height = 0;
        height = (this.MIN_ROWS) * this.cellSizeRem;
        return height;
    }

    getStyleRoleColor(shiftCalendar: ShiftCalendarDay) {
        const borderLeft = shiftCalendar
            ? (3 / REM) + 'rem solid ' + shiftCalendar.roleColor
            : '1px solid #ccc';
        return {
            'border-left': borderLeft
        };
    }

    getHeightHoursRange() {
        let height = 0;
        if (this.internalShiftCalendarsDay instanceof Array) {
            height = (this.internalShiftCalendarsDay.length) * this.cellSizeRem;
        } else {
            height = this.cellSizeRem;
        }
        return height;
    }

    getEventsHeight() {
        let height = 0;
        if (this.internalShiftCalendarsDay instanceof Array) {
            height = (this.internalShiftCalendarsDay.length) * this.cellSizeRem;
        }
        return height;
    }

    getPositionTopEvent(shift: ShiftItem, index: number) {
        let top = 0;
        top = index * this.cellSizeRem;
        return top;
    }

    getPositionLeftEvent(shift: ShiftItem) {
        let left = 0;

        const startOfDate = moment(shift.from).startOf('day');
        const from = moment(shift.from);
        const difHour = from.diff(startOfDate, 'minutes');

        left = difHour ? ((difHour / 60) * this.cellSizeRem) : 0;

        left += this.cellGapLeft;

        return left;
    }

    getWidthEvent(shift: ShiftItem) {
        let width = 0;
        const fromMoment = moment(shift.from);
        const toMoment = moment(shift.to);
        const difHour = toMoment.diff(fromMoment, 'hours');
        width = difHour
            ? difHour * this.cellSizeRem
            : 0;
        return width;
    }

    assignShiftForUser(shiftBlock: ShiftBlock, shiftCalendar: any) {
        const shiftAssignData = {
            shiftBlock: shiftBlock,
            shiftCalendar: shiftCalendar
        };
        // this.assign.emit(shiftAssignData);
        this.onAssign(shiftAssignData);
    }

    onAssign(shiftData) {
        const shiftBlock = shiftData.shiftBlock;
        const shiftCalendar = shiftData.shiftCalendar;
        if (!shiftBlock || !shiftBlock.id || !shiftCalendar || !shiftCalendar.id) {
            return;
        }

        this.shiftsRepository.assignShiftForStaffs(shiftBlock, [shiftCalendar.id])
            .pipe(
                tap(() => this.loadData()) // reload data
            )
            .subscribe();
    }

    onRemovedShiftItem(shiftItem: ShiftItem, shiftBlock?: ShiftBlock) {
        if (shiftItem) {
            this.internalShiftCalendarsDay =
                this.internalShiftCalendarsDay
                    .map(
                        (shiftCalendarDay: ShiftCalendarDay) => {
                            const index = shiftCalendarDay.shiftsItem.findIndex(item => item.id === shiftItem.id);
                            if (index !== -1) {
                                shiftCalendarDay.shiftsItem.splice(index, 1);
                            }

                            return shiftCalendarDay;
                        }
                    )
                    .filter(
                        (shiftCalendarDay: ShiftCalendarDay) => (shiftCalendarDay.shiftsItem && shiftCalendarDay.shiftsItem.length)
                    );
        }
    }
}
