import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {of, Subscription} from 'rxjs';
import * as moment from 'moment';
import {delay, map, retryWhen} from 'rxjs/operators';

@Component({
    selector: 'app-cxi-days-of-week',
    templateUrl: './cxi-days-of-week.component.html',
    styleUrls: ['./cxi-days-of-week.component.scss']
})
export class CxiDaysOfWeekComponent implements OnInit, OnDestroy, OnChanges {

    @Input() name: string;
    @Input() label: string;

    // disabled days
    @Input() disabledDays: number[] = [];
    internalDisabledDays: number[] = [];

    @Input() cols: number = 2;
    @Input() bordered = true;
    @Input() transparentBackground = true;
    @Output() daysOfWeekChange = new EventEmitter();

    // days
    private _days: number[];
    @Input()
    get days(): number[] {
        return this._days;
    }

    set days(days: number[]) {
        this._days = days;
        this.daysChange.emit(this.days);
    }

    @Output() daysChange = new EventEmitter<number[]>();

    isoWeek = false;
    daysOfWeekSelections = [
        {name: 'Monday', value: 1},
        {name: 'Tuesday', value: 2},
        {name: 'Wednesday', value: 3},
        {name: 'Thursday', value: 4},
        {name: 'Friday', value: 5},
        {name: 'Saturday', value: 6},
        {name: 'Sunday', value: 0},
    ];

    internalValues = [];
    subscriptions: Subscription[] = [];

    private _disabled: any;
    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(disabled: boolean) {
        this._disabled = disabled;
    }

    constructor() {
    }


    ngOnInit() {
        this.initDaysOfWeek();
        this.initValue();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.disabledDays &&
            changes.disabledDays.currentValue &&
            changes.disabledDays.currentValue !== changes.disabledDays.previousValue) {
            if (this.days) {
                this.internalDisabledDays = this.disabledDays.filter(d => this.days.indexOf(d) < 0);
            } else {
                this.internalDisabledDays = this.disabledDays;
            }
        }
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    initDaysOfWeek() {
        let daysOfWeek = [];
        const d = moment().startOf(
            this.isoWeek ? 'isoWeek' : 'weeks'
        );
        for (
            let i = 0;
            i < 7;
            i++, d.add(1, 'days')
        ) {
            const dayName = d.format('dddd');
            daysOfWeek.push(
                {
                    name: dayName,
                    value: this.isoWeek
                        ? (
                            moment(d).days() !== 0
                                ? moment(d).days()
                                : 7
                        )
                        : moment(d).days()
                }
            );
        }

        // sort
        daysOfWeek = daysOfWeek.sort(
            (a, b) => {
                if (a.name === 'Sunday') {
                    return 1;
                }
                if (b.name === 'Sunday') {
                    return -1;
                }
            }
        );

        this.daysOfWeekSelections = daysOfWeek;
    }

    initValue() {
        const source = of(null);
        const example = source.pipe(
            map(() => {
                if (!this.days || this.days.length === 0) {
                    throw 1;
                }
                return 1;
            }),
            retryWhen(errors =>
                errors.pipe(
                    // restart in 1 seconds
                    delay(500)
                )
            ),
            delay(100),
        );
        const doValueSub = example.subscribe(
            () => this.parseValue()
        );
        this.subscriptions.push(doValueSub);
    }

    parseValue() {
        if (this.days && this.days.length > 0) {
            const internalValues = [];

            // init false
            this.daysOfWeekSelections.forEach(
                (dayOfWeek: any) => {
                    internalValues[dayOfWeek.value] = this.days.findIndex(v => v === dayOfWeek.value) !== -1;
                }
            );

            this.internalValues = internalValues;
        }
    }

    getColClass() {
        if (!this.cols) {
            return {};
        }
        return {
            'col-12': this.cols === 1,
            'col-6': this.cols === 2,
            'col-4': this.cols === 3,
            'col-3': this.cols === 4
        };
    }

    getStyleDay(daysOfWeek: any) {
        let order: number;

        switch (this.cols) {
            case 1:
            case 3:
                break;

            case 2:
            default:
                switch (daysOfWeek.name) {
                    case 'Monday':
                        order = 1;
                        break;
                    case 'Tuesday':
                        order = 3;
                        break;
                    case 'Wednesday':
                        order = 5;
                        break;
                    case 'Thursday':
                        order = 7;
                        break;
                    case 'Friday':
                        order = 2;
                        break;
                    case 'Saturday':
                        order = 4;
                        break;
                    case 'Sunday':
                        order = 5;
                        break;
                }
                break;
        }

        return {
            'order': order,
        };
    }

    onChange() {
        if (!this.days) {
            return;
        }

        this.setValue();
        this.daysOfWeekChange.emit();
    }

    setValue() {
        if (this.internalValues) {
            const values: number[] = [];
            this.internalValues.forEach(
                (check: boolean, index: number) => {
                    if (check) {
                        values.push(index);
                    }
                }
            );
            this.days = values;
        }
    }

    // disabled item
    isDisabledItem(index: number): boolean {
        return this.internalDisabledDays.findIndex(v => v === index) !== -1;
    }
}
