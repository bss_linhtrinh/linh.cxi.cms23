import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiDaysOfWeekComponent } from './cxi-days-of-week.component';

describe('CxiDaysOfWeekComponent', () => {
  let component: CxiDaysOfWeekComponent;
  let fixture: ComponentFixture<CxiDaysOfWeekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiDaysOfWeekComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiDaysOfWeekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
