import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Shift} from '../../shift';
import {timer} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Role} from '../../../shared/entities/role';
import {LocalStorage} from 'ngx-webstorage';

@Component({
    selector: 'cxi-calendar-filter-shift-role',
    templateUrl: './cxi-calendar-filter-shift-role.component.html',
    styleUrls: ['./cxi-calendar-filter-shift-role.component.scss']
})
export class CxiCalendarFilterShiftRoleComponent implements OnInit, OnChanges {
    @Input() name: string;
    @Input() shifts: Shift[];
    @Input() roles: Role[];

    @Output() shiftIdsChange = new EventEmitter<string[]>();
    @Output() roleIdsChange = new EventEmitter<number[]>();


    // shift-role
    initialized = false;

    selectedAllShifts: boolean;
    selectedShifts: any[] = [];

    selectedAllRoles: boolean;
    selectedRoles: any[] = [];

    shiftIds: string[];
    roleIds: number[];

    @LocalStorage('calendar.typeView') typeView: string;

    constructor() {
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes &&
            changes.shifts &&
            !changes.shifts.firstChange &&
            changes.shifts.currentValue !== changes.shifts.previousValue) {
            this.initShiftRole();
        }
        if (changes &&
            changes.roles &&
            !changes.roles.firstChange &&
            changes.roles.currentValue !== changes.roles.previousValue) {
            this.initFilterRoles();
        }
    }

    initShiftRole() {
        timer(1)
            .pipe(
                tap(() => {
                    if (!this.initialized) {
                        this.initialized = true;
                        this.initFilterShifts();
                    }
                })
            )
            .subscribe();
    }

    initFilterShifts() {
        // check all
        this.selectedAllShifts = true;
        if (this.shifts && this.shifts.length > 0) {
            for (const index in this.shifts) {
                this.selectedShifts[index] = true;
            }
            this.selectedShifts['leave'] = true;
            this.selectedShifts['absent'] = true;
        }
    }

    initFilterRoles() {
        // check all
        this.selectedAllRoles = true;
        if (this.roles && this.roles.length > 0) {
            for (const index in this.roles) {
                this.selectedRoles[index] = true;
            }
        }
    }

    onChangeSelectAllShifts() {
        if (this.selectedAllShifts) {
            for (const index in this.shifts) {
                this.selectedShifts[index] = true;
            }
            this.selectedShifts['leave'] = true;
            this.selectedShifts['absent'] = true;
        } else {
            for (const index in this.shifts) {
                this.selectedShifts[index] = false;
            }
            this.selectedShifts['leave'] = false;
            this.selectedShifts['absent'] = false;
        }
        this.parseValueShiftIds();
    }

    onChangeSelectAllRoles() {
        if (this.selectedAllRoles) {
            for (const index in this.roles) {
                this.selectedRoles[index] = true;
            }
        } else {
            for (const index in this.roles) {
                this.selectedRoles[index] = false;
            }
        }
        this.parseValueRoleIds();
    }

    onChangeSelectShift() {
        this.checkAllSelectedShifts();
        this.parseValueShiftIds();
    }

    onChangeSelectRole() {
        this.checkAllSelectedRoles();
        this.parseValueRoleIds();
    }

    checkAllSelectedShifts(): boolean {
        let allSelectedShifts = true;
        for (const index in this.shifts) {
            if (!this.selectedShifts[index]) {
                allSelectedShifts = false;
                break;
            }
        }
        if (!this.selectedShifts['leave']) {
            allSelectedShifts = false;
        }
        if (!this.selectedShifts['absent']) {
            allSelectedShifts = false;
        }
        this.selectedAllShifts = !!allSelectedShifts;
        return allSelectedShifts;
    }

    checkAllSelectedRoles(): boolean {
        let allSelectedRoles = true;
        for (const index in this.roles) {
            if (!this.selectedRoles[index]) {
                allSelectedRoles = false;
                break;
            }
        }
        this.selectedAllRoles = !!allSelectedRoles;
        return allSelectedRoles;
    }

    parseValueShiftIds() {
        const shiftIds =
            this.shifts
                .filter((shift, i) => this.selectedShifts[parseInt(`${i}`, 10)] && shift && shift.id)
                .map(shift => shift.id);

        if (this.shiftIds !== shiftIds) {
            this.shiftIds = shiftIds;
            this.shiftIdsChange.emit(shiftIds);
            console.log('--filter shifts: ', shiftIds);
        }
    }

    parseValueRoleIds() {
        const roleIds =
            this.roles
                .filter((role, i) => this.selectedRoles[parseInt(`${i}`, 10)] && role && role.id)
                .map(role => role.id);

        if (this.roleIds !== roleIds) {
            this.roleIds = roleIds;
            this.roleIdsChange.emit(roleIds);
            console.log('--filter roles: ', roleIds);
        }
    }
}
