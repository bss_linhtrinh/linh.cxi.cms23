import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiCalendarFilterShiftRoleComponent } from './cxi-calendar-filter-shift-role.component';

describe('CxiCalendarFilterShiftRoleComponent', () => {
  let component: CxiCalendarFilterShiftRoleComponent;
  let fixture: ComponentFixture<CxiCalendarFilterShiftRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiCalendarFilterShiftRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiCalendarFilterShiftRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
