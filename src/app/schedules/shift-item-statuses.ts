import {ShiftItemStatus} from './shift-item-status';

export const SHIFT_ITEM_STATUSES = [
    {name: 'Clocked In', value: ShiftItemStatus.ClockedIn},
    {name: 'Clocked Out', value: ShiftItemStatus.ClockedOut},
    {name: 'ApprovedOff', value: ShiftItemStatus.ApprovedOff},
    {name: 'OffWithoutApproved', value: ShiftItemStatus.OffWithoutApproved}
];
