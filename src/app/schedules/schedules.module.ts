import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SchedulesRoutingModule} from './schedules-routing.module';
import {WorkingHoursComponent} from './working-hours/working-hours.component';
import {ShiftManagementComponent} from './shift-management/shift-management.component';
import {CalendarComponent} from './calendar/calendar.component';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {WorkingHoursItemsComponent} from './working-hours/working-hours-items/working-hours-items.component';
import {WorkingHoursItemComponent} from './working-hours/working-hours-item/working-hours-item.component';
import {HolidayComponent} from './working-hours/holiday/holiday.component';
import {SpecificDayComponent} from './working-hours/specific-day/specific-day.component';
import {CreateShiftComponent} from './shift-management/create-shift/create-shift.component';
import {EditShiftComponent} from './shift-management/edit-shift/edit-shift.component';
import {WorkingHoursExcludesComponent} from './working-hours/working-hours-excludes/working-hours-excludes.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {CxiCalendarWeekAddStaffComponent} from './calendar/cxi-calendar-week-add-staff/cxi-calendar-week-add-staff.component';
import {CxiCalendarComponent} from './calendar/cxi-calendar/cxi-calendar.component';
import {CxiCalendarEventComponent} from './calendar/cxi-calendar-event/cxi-calendar-event.component';
import {CxiCalendarEventVerticalComponent} from './calendar/cxi-calendar-event-vertical/cxi-calendar-event-vertical.component';
import {CxiCalendarFilterShiftRoleComponent} from './calendar/cxi-calendar-filter-shift-role/cxi-calendar-filter-shift-role.component';
import {CxiCalendarNavDateComponent} from './calendar/cxi-calendar-nav-date/cxi-calendar-nav-date.component';
import {CxiDaysOfWeekComponent} from './calendar/cxi-days-of-week/cxi-days-of-week.component';
import { CxiCalendarDayComponent } from './calendar/cxi-calendar-day/cxi-calendar-day.component';
import { CxiCalendarWeekComponent } from './calendar/cxi-calendar-week/cxi-calendar-week.component';

@NgModule({
    declarations: [
        WorkingHoursComponent,
        WorkingHoursItemsComponent,
        WorkingHoursItemComponent,
        HolidayComponent,
        SpecificDayComponent,
        WorkingHoursExcludesComponent,

        ShiftManagementComponent,
        CreateShiftComponent,
        EditShiftComponent,

        CalendarComponent,
        CxiCalendarWeekAddStaffComponent,
        CxiCalendarComponent,
        CxiCalendarEventComponent,
        CxiCalendarEventVerticalComponent,
        CxiCalendarFilterShiftRoleComponent,
        CxiCalendarNavDateComponent,
        CxiDaysOfWeekComponent,
        CxiCalendarDayComponent,
        CxiCalendarWeekComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        NgbModule,
        PerfectScrollbarModule,
        SchedulesRoutingModule
    ],
    entryComponents: [
        CxiCalendarWeekAddStaffComponent
    ],
    exports: [
        CxiDaysOfWeekComponent
    ]
})
export class SchedulesModule {
}
