export enum ShiftItemAction {
    ClockedIn = 'clockedIn',
    ClockedOut = 'clockedOut',
    Leave = 'leave',
    Absent = 'absent',
    Remove = 'remove',
}
