import {BaseEntity} from '../shared/entities/base.entity';
import {PeriodDate} from '../shared/components/cxi-period-date/period-date';
import * as _ from 'lodash';
import * as moment from 'moment';

export class Shift extends BaseEntity {

    _id: string;
    name: string;
    backgroundColor: string;
    repeatType: string;
    char: string;

    startDate: string;
    estimatedEndTime: string;

    // period
    fromHour: number;
    fromMinute: number;
    fromMeridiem: string;
    toHour: number;
    toMinute: number;
    toMeridiem: string;
    periodDate: PeriodDate;

    brandId: number;
    outletId: number[];

    time?: {
        fromHour: number;
        fromMinute: number;
        fromMeridiem: string;
        toHour: number;
        toMinute: number;
        toMeridiem: string;
        periodDate: PeriodDate;
    };

    get fromTime(): string {
        let fromTime: any;
        let stringTime: string;

        if (this.time) {
            stringTime = `${this.time.fromHour}:${this.time.fromMinute} ${this.time.fromMeridiem}`;
        } else {
            stringTime = `${this.fromHour}:${this.fromMinute} ${this.fromMeridiem}`;
        }

        fromTime = this.formatHour(stringTime, 'h:m A');

        return fromTime;
    }

    get toTime(): string {
        let toTime: string;
        let stringTime: string;

        if (this.time) {
            stringTime = `${this.time.toHour}:${this.time.toMinute} ${this.time.toMeridiem}`;
        } else {
            stringTime = `${this.toHour}:${this.toMinute} ${this.toMeridiem}`;
        }

        toTime = this.formatHour(stringTime, 'h:m A');

        return toTime;
    }

    get fromToTime(): string {
        return `${this.fromTime} - ${this.toTime}`;
    }

    get startTime(): string {
        if (!this.startDate) {
            return null;
        }

        const startDate = moment(this.startDate);

        const fromHourFirstShift = this.fromMeridiem === 'AM'
            ? this.fromHour
            : this.fromHour === 12
                ? 12
                : this.fromHour + 12;
        const fromMinuteFirstShift = this.fromMinute;

        return startDate
            .set({
                hours: fromHourFirstShift,
                minutes: fromMinuteFirstShift
            })
            .format('DD/MM/YYYY hh:mm A');
    }

    get endTime(): string {
        if (!this.estimatedEndTime) {
            return null;
        }

        const estimatedEndTime = moment(this.estimatedEndTime);

        const toHourLastShift = this.toMeridiem === 'AM'
            ? this.toHour
            : this.toHour === 12
                ? 12
                : this.toHour + 12;
        const toMinuteLastShift = this.toMinute;

        return estimatedEndTime
            .set({
                hours: toHourLastShift,
                minutes: toMinuteLastShift
            })
            .format('DD/MM/YYYY hh:mm A');
    }

    // days
    days: number[];
    lockedInMonday: boolean;
    lockedInTuesday: boolean;
    lockedInWednesday: boolean;
    lockedInThursday: boolean;
    lockedInFriday: boolean;
    lockedInSaturday: boolean;
    lockedInSunday: boolean;

    endAfter: number;

    active: boolean;
    status: boolean;

    get id(): string {
        return this._id;
    }

    get repeatTypeLabel(): string {
        return _.capitalize(this.repeatType);
    }

    get is_activated(): boolean {
        return this.active;
    }

    set is_activated(is_activated: boolean) {
        this.active = is_activated;
    }

    translations: any;

    constructor() {
        super();
        this._id = null;
        this.name = null;
        this.backgroundColor = '#ffffff';
        this.repeatType = 'daily';
        this.char = null;

        this.startDate = null;
        this.estimatedEndTime = null;

        this.fromHour = 0;
        this.fromMinute = 0;
        this.fromMeridiem = 'AM';
        this.toHour = 0;
        this.toMinute = 0;
        this.toMeridiem = 'AM';
        this.periodDate = new PeriodDate();
        this.time = null;

        // days
        this.days = [];
        this.lockedInMonday = false;
        this.lockedInTuesday = false;
        this.lockedInWednesday = false;
        this.lockedInThursday = false;
        this.lockedInFriday = false;
        this.lockedInSaturday = false;
        this.lockedInSunday = false;

        this.endAfter = 1;

        this.active = false;
        this.status = false;

        this.translations = [
            {name: null, locale: 'en'},
            {name: null, locale: 'vi'},
        ];

        this.brandId = null;
        this.outletId = [];

    }

    static create(data): Shift {
        return (new Shift()).fill(data);
    }

    fill(data): this {
        if (data.fromHour || data.fromMinute || data.fromMeridiem || data.toHour || data.toMinute || data.toMeridiem) {
            data.periodDate = PeriodDate.create(data);
        }

        // days
        const days = [];
        if (data.lockedInMonday) {
            days.push(1);
        }
        if (data.lockedInTuesday) {
            days.push(2);
        }
        if (data.lockedInWednesday) {
            days.push(3);
        }
        if (data.lockedInThursday) {
            days.push(4);
        }
        if (data.lockedInFriday) {
            days.push(5);
        }
        if (data.lockedInSaturday) {
            days.push(6);
        }
        if (data.lockedInSunday) {
            days.push(0);
        }
        data.days = days;

        return super.fill(data);
    }

    parse() {
        // period
        if (this.periodDate) {
            this.fromHour = this.periodDate.fromHour;
            this.fromMinute = this.periodDate.fromMinute;
            this.fromMeridiem = this.periodDate.fromMeridiem;
            this.toHour = this.periodDate.toHour;
            this.toMinute = this.periodDate.toMinute;
            this.toMeridiem = this.periodDate.toMeridiem;
        }

        return {
            name: this.name,
            repeatType: this.repeatType,
            backgroundColor: this.backgroundColor,
            char: this.char,

            startDate: this.parseDate(this.startDate),

            fromHour: this.fromHour,
            fromMinute: this.fromMinute,
            fromMeridiem: this.fromMeridiem,
            toHour: this.toHour,
            toMinute: this.toMinute,
            toMeridiem: this.toMeridiem,

            days: this.days,

            endAfter: this.endAfter,

            translations: this.translations,

            brandId: this.brandId,
            outletId: this.outletId
        };
    }
}
