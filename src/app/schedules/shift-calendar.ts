import {BaseEntity} from '../shared/entities/base.entity';
import {ShiftItem} from './shift-item';

export class ShiftCalendar extends BaseEntity {

    shiftsItem: ShiftItem[];

    _disabled: boolean;

    constructor() {
        super();
        this.shiftsItem = null;

        this._disabled = false;
    }

    static create(data): ShiftCalendar {
        return (new ShiftCalendar()).fill(data);
    }

    fill(data): this {

        return super.fill(data);
    }
}
