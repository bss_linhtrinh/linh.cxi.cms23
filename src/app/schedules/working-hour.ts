import {BaseEntity} from '../shared/entities/base.entity';
import {WorkingHourItem} from './working-hour-item';
import {WorkingHourExclude} from './working-hour-exclude';

export class WorkingHour extends BaseEntity {

    data: WorkingHourItem[];
    excludeList: WorkingHourExclude;
    brandId: number;
    outletId: number[];

    constructor() {
        super();
        this.data = [new WorkingHourItem()];
        this.excludeList = new WorkingHourExclude();
        this.brandId = null;
        this.outletId = [];
    }

    static create(data): WorkingHour {
        return (new WorkingHour()).fill(data);
    }

    fill(obj): this {
        if (obj.data) {
            obj.data = obj.data.map(wh => WorkingHourItem.create(wh));
        }
        if (obj.excludeList) {
            obj.excludeList = WorkingHourExclude.create(obj.excludeList);
        }
        return super.fill(obj);
    }

    parse() {
        return {
            data: this.data.map(workingHourItem => workingHourItem.parse()),
            excludeList: this.excludeList.parse(),
            brandId: this.brandId,
            outletId: this.outletId
        };
    }
}
