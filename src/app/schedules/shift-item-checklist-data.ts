import {BaseEntity} from '../shared/entities/base.entity';

export class ShiftItemChecklistData extends BaseEntity {
    task: string;
    status: boolean;

    constructor() {
        super();
        this.task = null;
        this.status = false;
    }

    static create(data): ShiftItemChecklistData {
        return (new ShiftItemChecklistData()).fill(data);
    }
}
