import {BaseEntity} from '../shared/entities/base.entity';
import {WorkingHourExcludeSpecificDay} from './working-hour-exclude-specific-day';

export class WorkingHourExclude extends BaseEntity {

    holidayId: string[];
    specificDays: WorkingHourExcludeSpecificDay[];

    constructor() {
        super();
        this.holidayId = [];
        this.specificDays = [new WorkingHourExcludeSpecificDay()];
    }

    static create(data): WorkingHourExclude {
        return (new WorkingHourExclude()).fill(data);
    }

    fill(data): this {
        if (data.holidayId) {
            data.holidayId = data.holidayId
                ? data.holidayId.map(
                    holidayId => typeof holidayId === 'object'
                        ? holidayId._id
                        : holidayId
                )
                : null;
        }
        if (data.specificDays) {
            data.specificDays = data.specificDays.map(
                specificDay => WorkingHourExcludeSpecificDay.create(specificDay)
            );
        }
        return super.fill(data);
    }

    parse() {
        return {
            holidayId: this.holidayId,
            specificDays: this.specificDays
                .filter(specificDay => specificDay.date)
                .map(specificDay => specificDay.parse()),
        };
    }
}
