import {BaseEntity} from '../shared/entities/base.entity';
import {WorkingHourItemPeriod} from './working-hour-item-period';

export class WorkingHourItem extends BaseEntity {

    days: number[];
    periods: WorkingHourItemPeriod[];

    constructor() {
        super();
        this.days = [];
        this.periods = [new WorkingHourItemPeriod()];
    }

    static create(data): WorkingHourItem {
        return (new WorkingHourItem()).fill(data);
    }

    fill(data): this {
        if (data.periods) {
            data.periods = data.periods.map(period => WorkingHourItemPeriod.create(period));
        }
        return super.fill(data);
    }

    parse() {
        return {
            days: this.days,
            periods: this.periods.map(day => day.parse()),
        };
    }
}
