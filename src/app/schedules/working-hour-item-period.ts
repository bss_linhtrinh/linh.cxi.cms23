import {BaseEntity} from '../shared/entities/base.entity';

export class WorkingHourItemPeriod extends BaseEntity {

    fromHour: number;
    fromMinute: number;
    fromMeridiem: string;
    toHour: number;
    toMinute: number;
    toMeridiem: string;

    constructor() {
        super();
        this.fromHour = 0;
        this.fromMinute = 0;
        this.fromMeridiem = 'AM';
        this.toHour = 0;
        this.toMinute = 0;
        this.toMeridiem = 'AM';
    }

    static create(data) {
        return (new WorkingHourItemPeriod()).fill(data);
    }

    fill(data): this {
        return super.fill(data);
    }

    parse() {
        return {
            fromHour: this.fromHour,
            fromMinute: this.fromMinute,
            fromMeridiem: this.fromMeridiem,
            toHour: this.toHour,
            toMinute: this.toMinute,
            toMeridiem: this.toMeridiem,
        };
    }
}
