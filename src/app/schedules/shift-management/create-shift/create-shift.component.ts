import {Component, OnInit} from '@angular/core';
import {Shift} from '../../shift';
import {ShiftsRepository} from '../../../shared/repositories/shifts.repositories';
import {ActivatedRoute, Router} from '@angular/router';
import {tap} from 'rxjs/operators';
import * as moment from 'moment';
import {Moment} from 'moment';
import {Language} from '../../../shared/entities/language';
import {LocalStorage, SessionStorage} from 'ngx-webstorage';
import {CheckListRepository} from '../../../shared/repositories/check-list.repository';

@Component({
    selector: 'app-create-shift',
    templateUrl: './create-shift.component.html',
    styleUrls: ['./create-shift.component.scss']
})
export class CreateShiftComponent implements OnInit {

    @LocalStorage('scope.brand_id') brandId: any;
    @LocalStorage('scope.outlet_id') outletId: any;
    shift: Shift = new Shift();
    repeats = [
        {name: 'Daily', value: 'daily'},
        {name: 'Weekly', value: 'weekly'},
        {name: 'Monthly', value: 'monthly'},
        {name: 'None', value: 'none'}
    ];
    today = new Date();
    tab: any;

    @SessionStorage('languages') languages: Language[];

    estimate: string;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private shiftsRepository: ShiftsRepository,
        private checklistRepository: CheckListRepository
    ) {
    }

    ngOnInit() {
    }

    onSubmit() {
        if (this.brandId && this.outletId) {
            this.shift.brandId = this.brandId;
            this.shift.outletId = [this.outletId];
        }
        this.shiftsRepository.save(this.shift)
            .pipe(
                tap(() => this.back())
            )
            .subscribe();
    }

    cancel() {
        this.back();
    }

    back() {
        this.router.navigate(['..', 'list'], {relativeTo: this.route});
    }

    onChangeRepeatType() {
        if (this.shift.repeatType === 'none') {
            this.shift.endAfter = 0;
            this.estimate = this.shift.startDate;
        } else {
            this.shift.endAfter = 1;
        }

        this.estimateTime();
    }

    isCollapseShiftDaysOfWeek(): boolean {
        return this.shift.repeatType !== 'weekly';
    }

    isDisabledEndAfter() {
        return this.shift.repeatType === 'none';
    }

    getMaxLengthEndAfter() {
        let maxLength: number;

        switch (this.shift.repeatType) {
            case 'daily':
            default:
                maxLength = 3;
                break;

            case 'weekly':
                maxLength = 2;
                break;

            case 'monthly':
                maxLength = 1;
                break;
        }

        return 3;
    }

    estimateTime() {
        const params = {
            startDate: this.shift.startDate,
            repeatType: this.shift.repeatType,
            endAfter: this.shift.endAfter,
            days: this.shift.days
        };
        if (this.shift.repeatType !== 'none') {
            this.checklistRepository.estimateTime(params)
                .subscribe(
                    (res) => {
                        this.estimate = res.estimatedEndTime;
                    },
                    (err) => {
                        this.estimate = this.shift.startDate;
                    }
                );
        }
    }

    onEndAfterChange() {
        this.estimateTime();
    }

    onHandleChange() {
        if (this.shift.repeatType === 'none') {
            this.shift.endAfter = 0;
            this.estimate = this.shift.startDate;
        }
        if (this.shift.repeatType !== 'weekly') {
            this.shift.days = [];
            this.estimateTime();
        } else {
            this.estimateTime();
        }
    }
}
