import {Component, OnInit} from '@angular/core';
import {Shift} from '../../shift';
import {ActivatedRoute, Router} from '@angular/router';
import {ShiftsRepository} from '../../../shared/repositories/shifts.repositories';
import {filter, map, switchMap, tap} from 'rxjs/operators';
import * as moment from 'moment';
import {Moment} from 'moment';
import {SessionStorage} from 'ngx-webstorage';
import {Language} from '../../../shared/entities/language';
import {CheckListRepository} from '../../../shared/repositories/check-list.repository';

@Component({
    selector: 'app-edit-shift',
    templateUrl: './edit-shift.component.html',
    styleUrls: ['./edit-shift.component.scss']
})
export class EditShiftComponent implements OnInit {
    shift: Shift = new Shift();
    repeats = [
        {name: 'Daily', value: 'daily'},
        {name: 'Weekly', value: 'weekly'},
        {name: 'Monthly', value: 'monthly'},
        {name: 'None', value: 'none'}
    ];
    today = new Date();
    tab: any;

    @SessionStorage('languages') languages: Language[];

    estimate: string;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private shiftsRepository: ShiftsRepository,
        private checklistRepository: CheckListRepository
    ) {
    }

    ngOnInit() {
        this.getShift();
    }

    getShift() {
        this.route.params
            .pipe(
                filter(data => data.id),
                map(data => data.id),
                switchMap((id: number) => this.shiftsRepository.find(id)),
                tap((shift: Shift) => this.shift = shift)
            )
            .subscribe();
    }

    onSubmit() {
        this.shiftsRepository.save(this.shift)
            .pipe(
                tap(() => this.back())
            )
            .subscribe();
    }


    cancel() {
        this.back();
    }

    onChangeRepeatType() {
        if (this.shift.repeatType === 'none') {
            this.shift.endAfter = 0;
            this.estimate = this.shift.startDate;
        } else {
            this.shift.endAfter = 1;
        }

        this.estimateTime();
    }

    back() {
        this.router.navigate(['../..', 'list'], {relativeTo: this.route});
    }

    isCollapseShiftDaysOfWeek(): boolean {
        return this.shift.repeatType !== 'weekly';
    }

    isDisabledEndAfter() {
        return this.shift.repeatType === 'none';
    }

    getMaxLengthEndAfter() {
        let maxLength: number;

        switch (this.shift.repeatType) {
            case 'daily':
            default:
                maxLength = 3;
                break;

            case 'weekly':
                maxLength = 2;
                break;

            case 'monthly':
                maxLength = 1;
                break;
        }

        return 3;
    }

    estimateTime() {
        const params = {
            startDate: this.shift.startDate,
            repeatType: this.shift.repeatType,
            endAfter: this.shift.endAfter,
            days: this.shift.days
        };
        if (this.shift.repeatType !== 'none') {
            this.checklistRepository.estimateTime(params)
                .subscribe(
                    (res) => {
                        this.estimate = res.estimatedEndTime;
                    },
                    (err) => {
                        this.estimate = this.shift.startDate;
                    }
                );
        }
    }

    onEndAfterChange() {
        this.estimateTime();
    }

    onHandleChange() {
        if (this.shift.repeatType === 'none') {
            this.shift.endAfter = 0;
            this.estimate = this.shift.startDate;
        }
        if (this.shift.repeatType !== 'weekly') {
            this.shift.days = [];
            this.estimateTime();
        } else {
            this.estimateTime();
        }
    }
}
