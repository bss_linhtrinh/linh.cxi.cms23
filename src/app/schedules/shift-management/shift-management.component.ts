import {Component, OnInit} from '@angular/core';
import {CxiGridConfig} from '../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../shared/components/cxi-grid/cxi-grid-column';
import {ShiftsRepository} from '../../shared/repositories/shifts.repositories';
import {Shift} from '../shift';
import {ActivatedRoute, Router} from '@angular/router';
import {ColumnFilterType} from '../../shared/types/column-filter-type';

@Component({
    selector: 'app-shift-management',
    templateUrl: './shift-management.component.html',
    styleUrls: ['./shift-management.component.scss']
})
export class ShiftManagementComponent implements OnInit {

    // table
    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        pagingServer: true,
        translatable: true,
        repository: this.shiftsRepository,
        actions: {
            onEdit: (shift: Shift) => {
                this.onEdit(shift);
            },
            onActivate: (shift: Shift) => {
                this.onActivate(shift);
            },
            onDeactivate: (shift: Shift) => {
                this.onDeactivate(shift);
            },
            onDelete: (shift: Shift) => {
                this.onDelete(shift);
            },
        }
    });
    cxiGridColumns = CxiGridColumn.from([
        {title: 'Name', name: 'name'},
        {
            title: 'Start time',
            name: 'startDate',
            transformedName: 'startTime',
            filterType: ColumnFilterType.Datepicker
        },
        {
            title: 'End time',
            name: 'estimatedEndTime',
            transformedName: 'endTime',
            filterType: ColumnFilterType.Datepicker
        },
        {
            title: 'Repeat type',
            name: 'repeatType',
            transformedName: 'repeatTypeLabel',
            filterType: ColumnFilterType.Select,
            filterData: [
                {id: 'none', name: 'None'},
                {id: 'daily', name: 'Daily'},
                {id: 'weekly', name: 'Weekly'},
                {id: 'monthly', name: 'Monthly'},
            ]
        },
    ]);

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private shiftsRepository: ShiftsRepository
    ) {
    }


    ngOnInit() {
    }

    onSubmit() {
    }

    cancel() {
    }

    onEdit(shift: Shift) {
        this.router.navigate(['..', shift.id, 'edit'], {relativeTo: this.route});
    }

    onActivate(shift: Shift) {
        this.shiftsRepository.changeStatus(shift, {active: true})
            .subscribe();
    }

    onDeactivate(shift: Shift) {
        this.shiftsRepository.changeStatus(shift, {active: false})
            .subscribe();
    }

    onDelete(shift: Shift) {
        const data = [shift];
        this.shiftsRepository.destroyMultiple(data)
            .subscribe();
    }
}
