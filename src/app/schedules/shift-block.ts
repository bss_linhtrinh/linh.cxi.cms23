import {BaseEntity} from '../shared/entities/base.entity';
import {PeriodDate} from '../shared/components/cxi-period-date/period-date';
import * as moment from 'moment';
import {ShiftItem} from './shift-item';

export class ShiftBlock extends BaseEntity {

    _id: string;
    name: string;
    backgroundColor: string;
    char: string;

    date: Date;
    day: number;

    from: string;
    to: string;

    get fromTime(): string {
        return this.formatHour(this.from);
    }

    get toTime(): string {
        return this.formatHour(this.to);
    }

    shift: string;
    checkList: number;
    checkListData: any[];

    get id(): string {
        return this._id;
    }

    items: ShiftItem[];

    _disabled: boolean;

    constructor() {
        super();
        this._id = null;
        this.name = null;
        this.backgroundColor = '#ffffff';
        this.char = null;

        this.date = new Date();
        this.day = 0;
        this.from = null;
        this.to = null;

        this.shift = null;
        this.checkList = null;
        this.checkListData = null;

        this.items = [];

        this._disabled = false;
    }

    static create(data): ShiftBlock {
        return (new ShiftBlock()).fill(data);
    }

    fill(data): this {
        if (data.date) {
            data.date = new Date(data.date);
        }
        if (data.fromHour || data.fromMinute || data.fromMeridiem || data.toHour || data.toMinute || data.toMeridiem) {
            data.periodDate = PeriodDate.create(data);
        }

        // days
        const days = [];
        if (data.lockedInMonday) {
            days.push(1);
        }
        if (data.lockedInTuesday) {
            days.push(2);
        }
        if (data.lockedInWednesday) {
            days.push(3);
        }
        if (data.lockedInThursday) {
            days.push(4);
        }
        if (data.lockedInFriday) {
            days.push(5);
        }
        if (data.lockedInSaturday) {
            days.push(6);
        }
        if (data.lockedInSunday) {
            days.push(0);
        }
        data.days = days;

        // items
        if (data.items) {
            data.items = data.items.map(shiftItem => ShiftItem.create(shiftItem));
        }

        return super.fill(data);
    }

    parse() {
        return {
            name: this.name,
            backgroundColor: this.backgroundColor,
            char: this.char
        };
    }
}
