import {BaseEntity} from '../shared/entities/base.entity';
import {ShiftBlock} from './shift-block';

export class ShiftCalendarWeek extends BaseEntity {

    0: ShiftBlock[];
    1: ShiftBlock[];
    2: ShiftBlock[];
    3: ShiftBlock[];
    4: ShiftBlock[];
    5: ShiftBlock[];
    6: ShiftBlock[];

    constructor() {
        super();
        this[0] = [];
        this[1] = [];
        this[2] = [];
        this[3] = [];
        this[4] = [];
        this[5] = [];
        this[6] = [];
    }

    static create(data): ShiftCalendarWeek {
        return (new ShiftCalendarWeek()).fill(data);
    }

    fill(data): this {
        for (let i = 0; i <= 6; i++) {
            if (typeof data[i] !== 'undefined' && data[i]) {
                data[i] = data[i].map(shiftBlock => ShiftBlock.create(shiftBlock));
            }
        }
        return super.fill(data);
    }
}
