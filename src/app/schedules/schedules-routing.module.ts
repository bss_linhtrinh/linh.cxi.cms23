import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CheckPermissionsGuard} from '../shared/guards/check-permissions/check-permissions.guard';
import {WorkingHoursComponent} from './working-hours/working-hours.component';
import {ShiftManagementComponent} from './shift-management/shift-management.component';
import {CalendarComponent} from './calendar/calendar.component';
import {CreateShiftComponent} from './shift-management/create-shift/create-shift.component';
import {EditShiftComponent} from './shift-management/edit-shift/edit-shift.component';
import {CanDeactivateGuard} from '../shared/guards/can-deactivate/can-deactivate.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {
                path: 'working-hours',
                canDeactivate: [CanDeactivateGuard],
                component: WorkingHoursComponent,
                data: {breadcrumb: 'Working hours'}
            },

            {path: 'shift-management', redirectTo: '/schedules/shift-management/list', pathMatch: 'full'},
            {path: 'shift-management/list', component: ShiftManagementComponent, data: {breadcrumb: 'Shift management'}},
            {path: 'shift-management/create', component: CreateShiftComponent, data: {breadcrumb: 'Create new shift'}},
            {path: 'shift-management/:id/edit', component: EditShiftComponent, data: {breadcrumb: 'Edit shift'}},

            {path: 'calendar', component: CalendarComponent, data: {breadcrumb: 'Calendar'}},
            {path: '', redirectTo: 'working-hours', pathMatch: 'full'},
        ]
    },


];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SchedulesRoutingModule {
}
