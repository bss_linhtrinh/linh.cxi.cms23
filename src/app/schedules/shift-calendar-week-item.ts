import {BaseEntity} from '../shared/entities/base.entity';

export class ShiftCalendarWeekItem extends BaseEntity {

    0: string;
    1: string;
    2: string;
    3: string;
    4: string;
    5: string;
    6: string;

    constructor() {
        super();
        this[0] = null;
        this[1] = null;
        this[2] = null;
        this[3] = null;
        this[4] = null;
        this[5] = null;
        this[6] = null;
    }

    static create(data): ShiftCalendarWeekItem {
        return (new ShiftCalendarWeekItem()).fill(data);
    }

    fill(data): this {
        return super.fill(data);
    }
}
