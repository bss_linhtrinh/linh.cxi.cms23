import {BaseEntity} from '../shared/entities/base.entity';

export class WorkingHourHoliday extends BaseEntity {

    _id: string;
    name: string;

    get id(): string {
        return this._id;
    }

    constructor() {
        super();
        this._id = null;
        this.name = null;
    }

    static create(data): WorkingHourHoliday {
        return (new WorkingHourHoliday()).fill(data);
    }

    fill(obj): this {
        return super.fill(obj);
    }

    parse() {
        return {
            name: this.name
        };
    }
}
