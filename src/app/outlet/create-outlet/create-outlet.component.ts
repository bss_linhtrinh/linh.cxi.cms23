import {Component, OnInit} from '@angular/core';
import {Outlet} from 'src/app/shared/entities/outlets';
import {ActivatedRoute, Router} from '@angular/router';
import {OutletsRepository} from 'src/app/shared/repositories/outlets.repository';
import {Brand} from 'src/app/shared/entities/brand';
import {BrandsRepository} from 'src/app/shared/repositories/brands.repository';
import {LocalStorage, SessionStorage} from 'ngx-webstorage';
import {Scope} from '../../shared/types/scopes';
import {tap} from 'rxjs/operators';
import {Language} from '../../shared/entities/language';

@Component({
    selector: 'app-create-outlet',
    templateUrl: './create-outlet.component.html',
    styleUrls: ['./create-outlet.component.scss']
})
export class CreateOutletComponent implements OnInit {
    outlet: Outlet = new Outlet();
    brands: Brand[] = [];

    // application scope
    isHiddenBrand = false;

    @LocalStorage('scope.current') currentScope: Scope;
    @LocalStorage('scope.brand_id') brandId: number;
    @SessionStorage('languages') languages: Language[];

    tab: any;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private brandsRepository: BrandsRepository,
        private outletsRepository: OutletsRepository
    ) {
    }

    ngOnInit() {
        this.getData();
    }

    getData() {
        this.getApplicationScope();
    }

    // -----------LOGIC HANDLE APPLICATION SCOPE
    getApplicationScope() {
        switch (this.currentScope) {
            case Scope.Organization:
                this.getBrands();
                break;

            case Scope.Brand:
            default:
                this.isHiddenBrand = true;
                if (!this.outlet.parent_id) {
                    this.outlet.parent_id = this.brandId;
                }
                break;

            case Scope.Outlet:
                break;
        }
    }

    getBrands() {
        this.brandsRepository.all()
            .subscribe(
                (brands: Brand[]) => {
                    this.brands = brands;

                    if (!this.outlet.parent_id && this.brands[0] && this.brands[0].id) {
                        this.outlet.parent_id = this.brands[0].id;
                    }
                }
            );
    }

    onSelectBrand(brandId?: any) {
    }

    handleAddressChange(address: any) {
        this.outlet.country = address.country;
        this.outlet.state = address.state;
        this.outlet.city = address.city;
        this.outlet.street = address.formatted_address;
        this.outlet.postcode = address.postcode;
        // this.outlet.city = address.formatted_address;
        this.outlet.latitude = address.latitude;
        this.outlet.longitude = address.longitude;
        // this.outlet.city = address.place_id;
        // this.outlet.city = address.ward;
    }

    // ----------------------------------------------
    cancel() {
        this.back();
    }

    onSubmit() {
        this.outletsRepository.save(this.outlet)
            .pipe(
                tap(() => this.back())
            )
            .subscribe(
            );
    }

    back() {
        this.router.navigate(['..', 'list'], {relativeTo: this.route});
    }

    onTabChange(language: string) {
        this.allByLanguage(language);
    }

    allByLanguage(language: string) {
        this.brandsRepository.allByLanguage(language)
            .subscribe(
                (brands: Brand[]) => {
                    this.brands = brands;

                    if (!this.outlet.parent_id && this.brands[0] && this.brands[0].id) {
                        this.outlet.parent_id = this.brands[0].id;
                    }
                }
            );
    }
}
