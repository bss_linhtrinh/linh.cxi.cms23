import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CreateOutletComponent} from './create-outlet/create-outlet.component';
import {EditOutletComponent} from './edit-outlet/edit-outlet.component';
import {ListOutletComponent} from './list-outlet/list-outlet.component';
import {CheckPermissionsGuard} from '../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: 'list', component: ListOutletComponent, data: {breadcrumb: 'List outlets'}},
            {path: 'create', component: CreateOutletComponent, data: {breadcrumb: 'Create new outlet'}},
            {path: ':id/edit', component: EditOutletComponent, data: {breadcrumb: 'Edit outlet'}},
            {path: '', redirectTo: 'list', pathMatch: 'full'},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OutletRoutingModule {
}
