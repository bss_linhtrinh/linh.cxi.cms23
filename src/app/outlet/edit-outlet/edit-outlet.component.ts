import {Component, OnInit} from '@angular/core';
import {Outlet} from 'src/app/shared/entities/outlets';
import {Router, ActivatedRoute} from '@angular/router';
import {OutletsRepository} from 'src/app/shared/repositories/outlets.repository';
import {BrandsRepository} from 'src/app/shared/repositories/brands.repository';
import {Organization} from 'src/app/shared/entities/organization';
import {Brand} from 'src/app/shared/entities/brand';
import {filter, map, switchMap, tap} from 'rxjs/operators';
import {Scope} from '../../shared/types/scopes';
import {LocalStorage, SessionStorage} from 'ngx-webstorage';
import {Language} from '../../shared/entities/language';

@Component({
    selector: 'app-edit-outlet',
    templateUrl: './edit-outlet.component.html',
    styleUrls: ['./edit-outlet.component.scss']
})
export class EditOutletComponent implements OnInit {
    outlet: Outlet = new Outlet();
    organizations: Organization[] = [];
    brands: Brand[] = [];

    // application scope
    isHiddenBrand: boolean;

    @LocalStorage('scope.current') currentScope: Scope;
    @LocalStorage('scope.brand_id') brandId: number;
    @SessionStorage('languages') languages: Language[];

    tab: any;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private brandsRepository: BrandsRepository,
        private outletsRepository: OutletsRepository
    ) {
    }

    ngOnInit() {
        this.getData();
        this.getOutletDetail();
    }

    getOutletDetail() {
        this.activatedRoute.params
            .pipe(
                filter(data => data.id),
                map(data => data.id),
                switchMap((id: number) => this.outletsRepository.find(id)),
                tap((outlet: Outlet) => this.outlet = outlet),
                tap(() => this.getApplicationScope())
            )
            .subscribe(
            );
    }

    getData() {
    }

    getApplicationScope() {
        switch (this.currentScope) {
            case Scope.Organization:
                this.getBrands();
                break;

            case Scope.Brand:
            default:
                this.isHiddenBrand = true;
                if (!this.outlet.parent_id) {
                    this.outlet.parent_id = this.brandId;
                }
                break;

            case Scope.Outlet:
                break;
        }
    }

    getBrands() {
        this.brandsRepository.all()
            .subscribe(
                (brands: Brand[]) => {
                    this.brands = brands;

                    if (!this.outlet.parent_id && this.brands[0] && this.brands[0].id) {
                        this.outlet.parent_id = this.brands[0].id;
                    }
                }
            );
    }

    onSelectBrand(brandId?: any) {
    }

    handleAddressChange(address: any) {
        this.outlet.country = address.country;
        this.outlet.state = address.state;
        this.outlet.city = address.city;
        this.outlet.street = address.formatted_address;
        this.outlet.postcode = address.postcode;
        // this.outlet.city = address.formatted_address;
        this.outlet.latitude = address.latitude;
        this.outlet.longitude = address.longitude;
        // this.outlet.city = address.place_id;
        // this.outlet.city = address.ward;
    }

    // ----------------------------------------------

    cancel() {
        this.router.navigate(['../..', 'list'], {relativeTo: this.activatedRoute});
    }

    onSubmit() {
        this.outletsRepository.save(this.outlet)
            .subscribe(
                () => this.router.navigate(['../..', 'list'], {relativeTo: this.activatedRoute})
            );
    }

    onTabChange(language: string) {
        this.allByLanguage(language);
    }

    allByLanguage(language: string) {
        this.brandsRepository.allByLanguage(language)
            .subscribe(
                (brands: Brand[]) => {
                    this.brands = brands;

                    if (!this.outlet.parent_id && this.brands[0] && this.brands[0].id) {
                        this.outlet.parent_id = this.brands[0].id;
                    }
                }
            );
    }
}
