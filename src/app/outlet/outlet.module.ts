import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OutletRoutingModule} from './outlet-routing.module';
import {EditOutletComponent} from './edit-outlet/edit-outlet.component';
import {ListOutletComponent} from './list-outlet/list-outlet.component';
import {CreateOutletComponent} from './create-outlet/create-outlet.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from 'src/app/shared/shared.module';

@NgModule({
    declarations: [
        EditOutletComponent,
        ListOutletComponent,
        CreateOutletComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        OutletRoutingModule,
    ]
})
export class OutletModule {
}
