import {Component, OnInit} from '@angular/core';
import {OutletsRepository} from 'src/app/shared/repositories/outlets.repository';
import {Router, ActivatedRoute} from '@angular/router';
import {Outlet} from 'src/app/shared/entities/outlets';
import {CxiGridColumn} from '../../shared/components/cxi-grid/cxi-grid-column';
import {CxiGridConfig} from '../../shared/components/cxi-grid/cxi-grid-config';
import {ColumnFilterType} from '../../shared/types/column-filter-type';
import {BrandsRepository} from '../../shared/repositories/brands.repository';

@Component({
    selector: 'app-list-outlet',
    templateUrl: './list-outlet.component.html',
    styleUrls: ['./list-outlet.component.scss']
})
export class ListOutletComponent implements OnInit {
    // table
    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        pagingServer: true,
        translatable: true,
        repository: this.outletsRepository,
        actions: {
            onEdit: (outlet: Outlet) => {
                this.onEdit(outlet);
            },
            onActivate: (outlet: Outlet) => {
                this.onActivate(outlet);
            },
            onDeactivate: (outlet: Outlet) => {
                this.onDeactivate(outlet);
            },
            onDelete: (outlet: Outlet) => {
                this.onDelete(outlet);
            },
        }
    });
    cxiGridColumns = CxiGridColumn.from([
        {title: 'Name', name: 'name'},
        {title: 'Address', name: 'street'},
        {title: 'Hotline', name: 'contact_number'},
        {
            title: 'Brand',
            name: 'parent_id',
            transformedName: 'brand_name',
            filterType: ColumnFilterType.Select,
            filterData: this.brandsRepository
        }
    ]);

    outlets: Outlet[] = [];

    constructor(
        private outletsRepository: OutletsRepository,
        private brandsRepository: BrandsRepository,
        private router: Router,
        private route: ActivatedRoute
    ) {
        const column = this.cxiGridColumns.find(c => c.title === 'Brand');
        if (column) {
            column.filterData = this.brandsRepository;
        }
    }

    ngOnInit() {
    }

    onEdit(outlet: Outlet) {
        this.router.navigate(['..', outlet.id, 'edit'], {relativeTo: this.route});
    }

    onActivate(outlet: Outlet) {
        this.outletsRepository.activate(outlet)
            .subscribe();
    }

    onDeactivate(outlet: Outlet) {
        this.outletsRepository.deactivate(outlet)
            .subscribe();
    }

    onDelete(outlet: Outlet) {
        this.outletsRepository.destroy(outlet)
            .subscribe();
    }
}
