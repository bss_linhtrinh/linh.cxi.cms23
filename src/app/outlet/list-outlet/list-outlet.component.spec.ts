import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOutletComponent } from './list-outlet.component';

describe('ListOutletComponent', () => {
  let component: ListOutletComponent;
  let fixture: ComponentFixture<ListOutletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOutletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOutletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
