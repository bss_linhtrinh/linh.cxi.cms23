export enum Levels {
    BSS = 'bss', // Super Admin

    Organization = 'organization',
    Brand = 'brand',
    Outlet = 'outlet',
}
