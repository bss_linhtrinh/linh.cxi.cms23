export enum ColumnFormat {
    None = 'none',
    Boolean = 'boolean',
    Percentage = 'percentage',

    Date = 'date',
    Monetary = 'monetary',
    PhoneNumber = 'phone-number',

    Gender = 'gender',
    Level = 'level',
    CustomerTypes = 'customer-types',
    // tag
    Tag = 'tag',
    Fullname = 'fullname',
}
