export enum OrderStatus {
    Pending = 'pending',
    Confirmed = 'confirmed',
    Ordered = 'ordered',
    HoldOrder = 'hold_order',
    FoodReady = 'food_ready',
    OnTheWay = 'on_the_way',
    Ordering = 'ordering',
    // Delivering = 'delivering',
    // Arrived = 'arrived',
    Completed = 'completed',
    Delivered = 'delivered',
    Canceled = 'canceled',
    Refund = 'refund',
}


// 'pending'       => 'Pending',
//     'confirmed'     => 'Confirmed',
//     'ordered'       => 'Ordered',
//     'hold_order'    => 'Hold Order',
//     'food_ready'    => 'Food Ready',
//     'on_the_way'    => 'On The Way',
//     'completed'     => 'Completed',
//     'delivered'     => 'Delivered',
//     'canceled'      => 'Canceled',
//     'refund'        => 'Refund'
