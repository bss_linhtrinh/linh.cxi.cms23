export enum Scope {
    BSS = 'bss',
    Organization = 'organizations',
    Brand = 'brands',
    Outlet = 'outlets',
}
