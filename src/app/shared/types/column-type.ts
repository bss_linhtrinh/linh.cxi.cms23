export enum ColumnType {
    Text = 'text',
    Select = 'select',
    Switch = 'switch'
}
