export enum OrderType {
    DineIn = 'dine_in',
    PickOnOutlet = 'pick_on_outlet',
    Delivery = 'delivery',
}
