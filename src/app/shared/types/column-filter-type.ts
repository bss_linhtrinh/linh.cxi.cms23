export enum ColumnFilterType {
    Text = 'text',
    Select = 'select',
    MultiSelect = 'multi-select',
    Datepicker = 'datepicker',
}
