export enum DisplayType {
    Single = 'single',
    Multiple = 'multiple',
    TrueFalse = 'true-false',
}
