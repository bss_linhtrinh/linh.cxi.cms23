export enum ProductType {
    Simple = 'simple',
    Configurable = 'configurable',
    Combo = 'combo',
}

