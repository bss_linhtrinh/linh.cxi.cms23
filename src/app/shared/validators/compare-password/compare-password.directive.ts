import {Attribute, Directive} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, Validator} from '@angular/forms';

@Directive({
    selector: '[appComparePassword]',
    providers: [{provide: NG_VALIDATORS, useExisting: ComparePasswordValidator, multi: true}]
})
export class ComparePasswordValidator implements Validator {

    constructor(@Attribute('appComparePassword') public comparePassword: string,
                @Attribute('reverse') public reverse: string) {
    }

    private get isReverse() {
        if (!this.reverse) {
            return false;
        }
        return this.reverse === 'true' ? true : false;
    }

    validate(c: AbstractControl): { [key: string]: any } | null {
        // self value (e.g. retype password)
        let v = c.value;

        // control value (e.g. password)
        let e = c.root.get(this.comparePassword);

        // value not equal
        if (e && v !== e.value && !this.isReverse) {
            return {
                comparePassword: false
            };
        }

        // value equal and reverse
        if (e && v === e.value && this.isReverse) {
            delete e.errors['comparePassword'];
            if (!Object.keys(e.errors).length) {
                e.setErrors(null);
            }
        }

        // value not equal and reverse
        if (e && v !== e.value && this.isReverse) {
            e.setErrors({comparePassword: false});
        }

        return null;
    }
}
