import {Attribute, Directive} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, Validator} from '@angular/forms';

@Directive({
    selector: '[appCompareDate]',
    providers: [{provide: NG_VALIDATORS, useExisting: CompareDateValidator, multi: true}]
})

export class CompareDateValidator implements Validator {

    constructor(@Attribute('greaterThan') public greaterThan: string,
                @Attribute('reverse') public reverse: string) {
    }

    private get isReverse() {
        if (!this.reverse) {
            return false;
        }
        return this.reverse === 'true';
    }

    validate(c: AbstractControl): { [key: string]: any } | null {
        // self value (e.g. end)
        const v = c.value;

        // control value (e.g. start)
        const e = c.root.get(this.greaterThan);

        // value not equal
        if (e && v <= e.value && !this.isReverse) {
            return {
                compareDate: false
            };
        }

        // value equal and reverse
        if (e && v < e.value && this.isReverse) {
            delete e.errors['compareDate'];
            if (!Object.keys(e.errors).length) {
                e.setErrors(null);
            }
        }

        // value not equal and reverse
        if (e && v >= e.value && this.isReverse) {
            e.setErrors({compareDate: false});
        }

        return null;
    }
}