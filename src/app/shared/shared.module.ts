import {NgModule} from '@angular/core';
import {CommonModule, CurrencyPipe, DatePipe, DecimalPipe, PercentPipe} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BaseButtonComponent} from './components/base-button/base-button.component';
import {StorageService} from './services/storage/storage.service';
import {CountUpDirective} from './directives/count-up/countup.directive';
import {BaseMonetaryDirective} from './directives/base-monetary/base-monetary.directive';
import {BtSearchPipePipe} from './pipes/bt-search/bt-search-pipe.pipe';
import {BaseCurrencyPipe} from './pipes/base-currency/base-currency.pipe';
import {BasePercentagePipe} from './pipes/base-percentage/base-percentage.pipe';
import {BasePagingPipe} from './pipes/base-paging/base-paging.pipe';
import {BaseSearchPipe} from './pipes/base-search/base-search.pipe';
import {BaseDatePipe} from './pipes/base-date/base-date.pipe';
import {BaseSubstringPipe} from './pipes/base-substring/base-substring.pipe';
import {BaseMonetaryPipe} from './pipes/base-monetary/base-monetary.pipe';
import {BaseYesTypePipe} from './pipes/base-yes-type/base-yes-type.pipe';
import {BaseSortPipe} from './pipes/base-sort/base-sort.pipe';
import {BaseDynamicPipePipe} from './pipes/base-dynamic-pipe/base-dynamic-pipe.pipe';
import {ComparePasswordValidator} from './validators/compare-password/compare-password.directive';
import {CompareDateValidator} from './validators/compare-date/compare-date.directive';
import {ApiService} from './services/api/api.service';
import {BaseSelectComponent} from './components/base-select/base-select.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {BaseCheckboxComponent} from './components/base-checkbox/base-checkbox.component';
import {BaseInputTextComponent} from './components/base-input-text/base-input-text.component';
import {BaseDataRawComponent} from './components/base-data-raw/base-data-raw.component';
import {NgxMaskModule} from 'ngx-mask';
import {BaseDatepickerComponent} from './components/base-datepicker/base-datepicker.component';
import {BaseIntegerComponent} from './components/base-integer/base-integer.component';
import {ValidationMessagesComponent} from './custom-form/validation-messages/validation-messages.component';
import {BaseFilterComponent} from './components/base-filter/base-filter.component';
import {BaseFilterInputComponent} from './components/base-filter-input/base-filter-input.component';
import {BaseTableComponent} from './components/base-table/base-table.component';
import {BaseFilterPipe} from './pipes/base-filter/base-filter.pipe';
import {FilterStorageService} from './services/filterStorage/filterStorage.service';
import {BaseInputGooglePlacesComponent} from './components/base-input-google-places/base-input-google-places.component';
import {GooglePlaceModule} from 'ngx-google-places-autocomplete';
import {BaseGenderPipe} from './pipes/base-gender/base-gender.pipe';
import {BaseEditTableComponent} from './components/base-edit-table/base-edit-table.component';
import {BaseRadioComponent} from './components/base-radio/base-radio.component';
import {BaseBooleanTypePipe} from './pipes/base-true-false/base-boolean-type.pipe';
import {BaseSelectionTwoColumnsComponent} from './components/base-selection-two-columns/base-selection-two-columns.component';
import {BaseMakeTitlePipe} from './pipes/base-make-title/base-make-title.pipe';
import {BaseFilterIncludeIdPipe} from './pipes/base-filter-include-id/base-filter-include-id.pipe';
import {BaseFilterNotIncludeIdPipe} from './pipes/base-filter-not-include-id/base-filter-not-include-id.pipe';
import {BaseIntegerDirective} from './directives/base-integer/base-integer.directive';
import {BaseIntegerPipe} from './pipes/base-integer/base-integer.pipe';
import {RequiredComponent} from './components/required/required.component';
import {CxiRatingSmallComponent} from './components/cxi-rating-small/cxi-rating-small.component';
import {BaseSwitchComponent} from './components/base-switch/base-switch.component';
import {CxiUploadFileComponent} from './components/cxi-upload-file/cxi-upload-file.component';
import {CxiSelectSimpleComponent} from './components/cxi-select-simple/cxi-select-simple.component';
import {CxiIconCloseComponent} from './components/cxi-icon-close/cxi-icon-close.component';
import {CxiInputPasswordComponent} from './components/cxi-input-password/cxi-input-password.component';
import {CxiAvatarCircleComponent} from './components/cxi-avatar-circle/cxi-avatar-circle.component';
import {CxiCardComponent} from './components/cxi-card/cxi-card.component';
import {CxiCustomerLevelComponent} from './components/cxi-customer-level/cxi-customer-level.component';
import {BaseMonetaryComponent} from './components/base-monetary/base-monetary.component';
import {CxiIncreaseDecreaseComponent} from './components/cxi-increase-decrease/cxi-increase-decrease.component';
import {CxiCardItemsComponent} from './components/cxi-card-items/cxi-card-items.component';
import {CxiImageComponent} from './components/cxi-image/cxi-image.component';
import {CxiOptionsComponent} from './components/cxi-options/cxi-options.component';
import {ReportService} from './services/report/report.service';
import {CxiTimePickerComponent} from './components/cxi-time-picker/cxi-time-picker.component';
import {CxiPaginationComponent} from './components/cxi-pagination/cxi-pagination.component';
import {CxiGridComponent} from './components/cxi-grid/cxi-grid.component';
import {TranslateModule} from '@ngx-translate/core';
import {BasePercentageComponent} from './components/base-percentage/base-percentage.component';
import {BasePercentageDirective} from './directives/base-percentage/base-percentage.directive';
import {CxiUploadFilesComponent} from './components/cxi-upload-files/cxi-upload-files.component';
import {CxiPhoneNumberPipe} from './pipes/cxi-phone-number/cxi-phone-number.pipe';
import {BaseTextareaComponent} from './components/base-textarea/base-textarea.component';
import {CxiCapitalizePipe} from './pipes/cxi-capitalize/cxi-capitalize.pipe';
import {CxiLabelComponent} from './components/cxi-label/cxi-label.component';
import {CxiImagePopupComponent} from './components/cxi-image-popup/cxi-image-popup.component';
import {CxiRadioComponent} from './components/cxi-radio/cxi-radio.component';
import {CxiDatetimepickerComponent} from './components/cxi-datetimepicker/cxi-datetimepicker.component';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import {BaseDateTimePipe} from './pipes/base-date-time/base-date-time.pipe';
import {SafeHtml} from './pipes/safe-html/cxi-safe-html.pipe';
import {CxiInputEditorComponent} from './components/cxi-input-editor/cxi-input-editor.component';
import {EditorModule} from '@tinymce/tinymce-angular';
import {CxiBookingUploadMultiImagesComponent} from './components/cxi-booking-upload-multi-images/cxi-booking-upload-multi-images.component';
import {CxiBadgeComponent} from './components/cxi-badge/cxi-badge.component';
import {CxiSelectProductsComponent} from './components/cxi-select-products/cxi-select-products.component';
import {CxiGridTranslationsComponent} from './components/cxi-grid-translations/cxi-grid-translations.component';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import {CxiSelectionWithCheckboxComponent} from './components/cxi-selection-with-checkbox/cxi-selection-with-checkbox.component';
import {CxiColorPickerComponent} from './components/cxi-color-picker/cxi-color-picker.component';
import {ColorPickerModule} from 'ngx-color-picker';
import {CxiPeriodDateComponent} from './components/cxi-period-date/cxi-period-date.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {CxiLanguageLeftSidebarComponent} from './components/cxi-language-left-sidebar/cxi-language-left-sidebar.component';
import {CxiStripHtmlPipe} from './pipes/cxi-strip-html/cxi-strip-html.pipe';

export const pipes = [
    BtSearchPipePipe,
    BaseMonetaryPipe,
    BaseYesTypePipe,
    BaseCurrencyPipe,
    BasePercentagePipe,
    BasePagingPipe,
    BaseSearchPipe,
    BaseSortPipe,
    BaseDatePipe,
    BaseSubstringPipe,
    BaseDynamicPipePipe,
    BaseFilterPipe,
    BaseGenderPipe,
    BaseMakeTitlePipe,
    BaseBooleanTypePipe,
    BaseIntegerPipe,
    BaseFilterIncludeIdPipe,
    BaseFilterNotIncludeIdPipe,
    CxiCapitalizePipe,
    CxiPhoneNumberPipe,
    BaseDateTimePipe,
    SafeHtml,
    CxiStripHtmlPipe
];
export const pipesAsServices = [
    CurrencyPipe,
    DecimalPipe,
    DatePipe,
    PercentPipe,
    BaseMonetaryPipe,
    BaseIntegerPipe,
    BasePercentagePipe,
    BaseFilterPipe,
    BaseSortPipe,
    BasePagingPipe,
];
export const components = [
        BaseButtonComponent,
        BaseMonetaryComponent,
        BaseSelectComponent,
        BaseCheckboxComponent,
        BaseInputTextComponent,
        BaseTextareaComponent,
        BaseDatepickerComponent,
        BaseIntegerComponent,
        ValidationMessagesComponent,
        BaseFilterComponent,
        BaseFilterInputComponent,
        BaseTableComponent,
        BaseInputGooglePlacesComponent,
        BaseEditTableComponent,
        BaseRadioComponent,
        BaseSelectionTwoColumnsComponent,
        BaseSwitchComponent,
        BaseDataRawComponent,
        CxiRatingSmallComponent,
        CxiUploadFileComponent,
        CxiUploadFilesComponent,
        CxiSelectSimpleComponent,
        CxiIconCloseComponent,
        CxiInputPasswordComponent,
        RequiredComponent,
        CxiAvatarCircleComponent,
        CxiCardComponent,
        CxiCustomerLevelComponent,
        CxiIncreaseDecreaseComponent,
        CxiCardItemsComponent,
        CxiImageComponent,
        CxiOptionsComponent,
        CxiTimePickerComponent,
        CxiPaginationComponent,
        CxiGridComponent,
        CxiGridTranslationsComponent,
        BasePercentageComponent,
        CxiLabelComponent,
        CxiImagePopupComponent,
        CxiRadioComponent,
        CxiDatetimepickerComponent,
        CxiInputEditorComponent,
        CxiBookingUploadMultiImagesComponent,
        CxiBadgeComponent,
        CxiSelectProductsComponent,
        CxiSelectionWithCheckboxComponent,
        CxiColorPickerComponent,
        CxiPeriodDateComponent,
        CxiLanguageLeftSidebarComponent
    ]
;
export const directives = [
    CountUpDirective,
    BaseMonetaryDirective,
    BaseIntegerDirective,
    BasePercentageDirective,
];
export const validators = [
    ComparePasswordValidator,
    CompareDateValidator,
];
export const repositories = [];
export const services = [
    ApiService,
    StorageService,
    FilterStorageService,
    ReportService
];

@NgModule({
    imports: [
        CommonModule,
        // BrowserAnimationsModule,
        FormsModule,
        RouterModule,
        NgbModule,
        NgSelectModule,
        NgxMaskModule,
        GooglePlaceModule,
        TranslateModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        EditorModule,
        SlickCarouselModule,
        ColorPickerModule,
        PerfectScrollbarModule
    ],
    declarations: [
        ...components,
        ...pipes,
        ...directives,
        ...validators,
    ],
    providers: [
        ...pipesAsServices,
        ...repositories,
        ...services,
    ],
    exports: [
        TranslateModule,

        ...components,
        ...pipes,
        ...validators,
        ...directives,
    ]
})

export class SharedModule {
}
