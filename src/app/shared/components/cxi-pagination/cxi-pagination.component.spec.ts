import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiPaginationComponent } from './cxi-pagination.component';

describe('CxiPaginationComponent', () => {
  let component: CxiPaginationComponent;
  let fixture: ComponentFixture<CxiPaginationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiPaginationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
