import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {SettingsRepository} from '../../repositories/settings.repository';
import {LocalStorage} from 'ngx-webstorage';
import {StorageService} from '../../services/storage/storage.service';
import {debounceTime, distinctUntilChanged, filter, tap} from 'rxjs/operators';
import {Subscription, timer} from 'rxjs';
import {CxiGridService} from '../cxi-grid/cxi-grid.service';

@Component({
    selector: 'app-cxi-pagination',
    templateUrl: './cxi-pagination.component.html',
    styleUrls: ['./cxi-pagination.component.scss']
})
export class CxiPaginationComponent implements OnInit, OnDestroy {
    cxiPerPageItems = [
        {id: 20, name: '20', value: 20},
        {id: 50, name: '50', value: 50},
        {id: 100, name: '100', value: 100},
    ];

    @Input() name: string;
    @Input() gridName: string;

    private _currentPage: number;
    @Input()
    get currentPage(): number {
        return this._currentPage;
    }

    @Output() currentPageChange = new EventEmitter<number>();

    set currentPage(currentPage: number) {
        if (currentPage !== this._currentPage) {
            this._currentPage = currentPage;
            this.currentPageChange.emit(this.currentPage);
        }
    }

    private _perPage: number;
    @Input()
    get perPage(): number {
        return this._perPage;
    }

    @Output() perPageChange = new EventEmitter<number>();

    set perPage(perPage: number) {
        if (perPage !== this._perPage) {
            this._perPage = perPage;
            this.perPageChange.emit(this.perPage);
        }
    }

    private _total: number;
    @Input()
    get total(): number {
        return this._total;
    }

    @Output() totalChange = new EventEmitter<number>();

    set total(total: number) {
        if (total !== this._total) {
            this._total = total;
            this.totalChange.emit(this.total);
        }
    }

    @Output() change = new EventEmitter();

    subscriptions: Subscription[] = [];

    constructor(
        private settingsRepository: SettingsRepository,
        private cxiGridService: CxiGridService
    ) {
        let totalSub: Subscription;
        totalSub = cxiGridService.gridTotal$
            .pipe(
                filter((data) => data && (data.grid_name === this.gridName)),
                tap((data) => {
                    if (typeof data.total !== 'undefined') {
                        this.total = data.total;
                    }
                })
            )
            .subscribe();
        this.subscriptions.push(totalSub);
    }

    ngOnInit() {
        this.initPerPageOptions();
        // this.initPagination();
    }

    ngOnDestroy() {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    // initPagination() {
    //     if (!this.currentPage) {
    //         this.currentPage = 1;
    //     }
    // }

    initPerPageOptions() {
        const perPagesOptions = this.settingsRepository.findValueByKey('PER_PAGES_OPTIONS');
        if (
            perPagesOptions &&
            Array.isArray(perPagesOptions) &&
            perPagesOptions.length > 0
        ) {
            const items = [];
            for (const item of perPagesOptions) {
                items.push({id: parseInt(item, 10), name: item, value: parseInt(item, 10)});
            }
            if (items && items.length > 0) {
                this.cxiPerPageItems = items;
            }
        }

        // Select default first item
        this.selectDefaultFirstItem();
    }

    selectDefaultFirstItem() {
        if (this.cxiPerPageItems &&
            this.cxiPerPageItems.length > 0 &&
            !this.perPage) {
            timer(100)
                .pipe(
                    filter(() => !this.perPage),
                    tap(() => this.perPage = this.cxiPerPageItems[0].id)
                )
                .subscribe();
        }
    }

    onChange() {
        this.change.emit();
    }
}
