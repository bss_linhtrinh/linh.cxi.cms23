import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseFilterInputComponent } from './base-filter-input.component';

describe('BaseFilterInputComponent', () => {
  let component: BaseFilterInputComponent;
  let fixture: ComponentFixture<BaseFilterInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseFilterInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseFilterInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
