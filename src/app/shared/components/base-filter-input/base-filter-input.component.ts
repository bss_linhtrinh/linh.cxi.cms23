import { Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild } from '@angular/core';
import { NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel } from '@angular/forms';
import { ElementBase } from '../../custom-form/element-base/element-base';
import { Gender } from '../../entities/gender';
import { BOOLEAN_ITEMS } from '../../constants/boolean-items';
import { ColumnFilterType } from '../../types/column-filter-type';
import { ColumnFormat } from '../../types/column-format';
import { BaseRepository } from '../../repositories/base.repository';
import { filter, tap } from 'rxjs/operators';

@Component({
    selector: 'base-filter-input',
    templateUrl: './base-filter-input.component.html',
    styleUrls: ['./base-filter-input.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: BaseFilterInputComponent, multi: true }
    ]
})
export class BaseFilterInputComponent extends ElementBase<any> implements OnInit {

    @Input() public name: string;
    @Input() public filterType: ColumnFilterType;
    @Input() public filterData?: any[] | BaseRepository;
    @Input() public format?: ColumnFormat;
    @Input() public placeholder: string = 'Add something';

    @ViewChild(NgModel, { static: true }) model: NgModel;

    @Output() change = new EventEmitter();

    internalFilterData: any[] = [];

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
        this.getFilterData();
    }

    getFilterData() {
        if (
            !this.filterData ||
            (this.filterData instanceof Array && this.filterData.length === 0)
        ) {
            // Enum data
            this.getEnumData();
        } else {

            if (this.filterData instanceof BaseRepository) {

                // async data from repository
                this.getAsyncDataFromRepository();
            } else {

                // static data
                this.getStaticData();
            }
        }

    }

    getEnumData() {
        // Enum data
        if (this.filterType) {
            if (this.filterType === ColumnFilterType.Select ||
                this.filterType === ColumnFilterType.MultiSelect) {
                if (this.format) {
                    let enumItems = [];
                    switch (this.format) {
                        case ColumnFormat.Boolean:
                        default:
                            enumItems = BOOLEAN_ITEMS;
                            break;
                        case ColumnFormat.Gender:
                            enumItems = Gender.init();
                            break;
                    }
                    this.internalFilterData = enumItems;
                }
            }
        }
    }

    getAsyncDataFromRepository() {
        if (this.filterData instanceof BaseRepository) {
            this.filterData.all()
                .pipe(
                    filter((items: any[]) => items && items.length > 0),
                    tap((items: any[]) => this.internalFilterData = items)
                )
                .subscribe(
                );
        }
    }

    getStaticData() {
        if (this.filterData instanceof Array &&
            this.filterData.length > 0) {
            this.internalFilterData = this.filterData;
        }
    }

    onChange() {
        this.change.emit();
    }


    // ==== select
    getBindValue() {
        let valueName = 'value';
        if (this.internalFilterData &&
            this.internalFilterData.length > 0 &&
            this.internalFilterData[0] &&
            !this.internalFilterData.hasOwnProperty(valueName)) {
            valueName = 'id';
        }
        return valueName;
    }

    getBindLabel() {
        return 'name';
    }
}
