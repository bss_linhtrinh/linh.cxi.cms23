import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseInputTextComponent } from './base-input-text.component';

describe('BaseInputTextComponent', () => {
  let component: BaseInputTextComponent;
  let fixture: ComponentFixture<BaseInputTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseInputTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseInputTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
