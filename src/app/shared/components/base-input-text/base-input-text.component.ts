import {Component, EventEmitter, Inject, Input, OnDestroy, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';
import {Subject, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';

@Component({
    selector: 'base-input-text',
    templateUrl: './base-input-text.component.html',
    styleUrls: ['./base-input-text.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: BaseInputTextComponent, multi: true}
    ]
})
export class BaseInputTextComponent extends ElementBase<number> implements OnInit, OnDestroy {
    private _name: string;

    @Input('name')
    get name(): string {
        return this._name;
    }

    pattern = '';

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input() public size = 'sm';
    @Input() public width = '';
    @Input() public label: string;
    @Input() public type = 'text';
    @Input() public placeholder = '';
    @Input() public inputClass = '';
    @Input() icon: string;
    @Input() icon_alignment = 'right';

    private _required: any;

    @Input()
    get required(): any {
        return this._required;
    }

    set required(required: any) {
        this._required = typeof required !== 'undefined';
    }

    private _disabled: any;

    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(disabled: boolean) {
        this._disabled = disabled;
    }

    @Input() maxLength = 65;

    ngModelValues = new Subject<any>();
    ngModelValues$ = this.ngModelValues.asObservable();
    debounceSub: Subscription;
    @Input() debounceTime = 500;
    @Output() debounceChange = new EventEmitter<any>();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);

        this.debounceSub = this.ngModelValues$
            .pipe(
                debounceTime(this.debounceTime),
                distinctUntilChanged(),
                tap((value) => this.debounceChange.emit(value))
            ).subscribe();
    }

    ngOnInit() {
    }

    ngOnDestroy(): void {
        this.debounceSub.unsubscribe();
    }

    onChange() {
        //
    }

    onNgModelChange() {
        if (this.debounceChange) {
            this.ngModelValues.next(this.value);
        }
    }
}
