import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {timer} from 'rxjs';
import {REM} from '../../constants/app';

@Component({
    selector: 'cxi-card',
    templateUrl: './cxi-card.component.html',
    styleUrls: ['./cxi-card.component.scss']
})

export class CxiCardComponent implements OnInit, OnChanges {
    @Input() size = 'md';
    @Input() name: string;
    @Input() title: string;
    @Input() badge: string;
    @Input() className: string;
    @Input() isCollapsed = false;
    @Input() hideCollapse = false;
    @Input() isClose = false;

    // style
    @Input() justify = 'center'; // {left center right}
    @Input() marginBottom = (10 / REM); // 10px

    @Output() delete = new EventEmitter();


    internalSize: string;

    constructor() {

    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.size) {
            if (changes.size.currentValue !== changes.size.previousValue) {
                timer(1).subscribe(() => this.internalSize = this.size);
            }
        }
    }

    collapse() {
        this.isCollapsed = !this.isCollapsed;
    }

    getPanelClassName() {
        return {
            'sm': this.internalSize === 'sm' && !this.isCollapsed,
            'md': !this.internalSize || this.internalSize === 'md' || this.isCollapsed,
            'lg': this.internalSize === 'lg' && !this.isCollapsed,
            'xl': this.internalSize === 'xl',
            'full': this.internalSize === 'full' && !this.isCollapsed
        };
    }

    close() {
        this.delete.emit(this.className);
    }
}
