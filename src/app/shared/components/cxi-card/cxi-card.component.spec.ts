import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiCardComponent } from './cxi-card.component';

describe('CxiCardComponent', () => {
  let component: CxiCardComponent;
  let fixture: ComponentFixture<CxiCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
