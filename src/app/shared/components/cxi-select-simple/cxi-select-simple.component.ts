import {
    AfterViewChecked,
    Component,
    EventEmitter,
    Inject,
    Input,
    OnChanges, OnDestroy,
    OnInit,
    Optional,
    Output,
    SimpleChanges,
    ViewChild
} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';
import {UtilitiesService} from '../../services/utilities/utilities.service';
import {of, Subscription, timer} from 'rxjs';
import {delay, map, retryWhen, tap} from 'rxjs/operators';

@Component({
    selector: 'cxi-select-simple',
    templateUrl: './cxi-select-simple.component.html',
    styleUrls: ['./cxi-select-simple.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: CxiSelectSimpleComponent, multi: true}
    ]
})
export class CxiSelectSimpleComponent extends ElementBase<any[]> implements OnInit, OnChanges, OnDestroy {
    @Input() name: string;
    @Input() label: string;
    @Input() subLabel: string;
    @Input() items: any;
    @Input() disabled: boolean;
    @Input() bindLabel = null;
    @Input() bindValue = null;
    @Input() clearable = false;
    @Input() searchable = false;
    @Input() width = '';
    @Input() public type: string;
    @Input() public placeholder: string;

    // customize
    @Input() isTree = false;
    @Input() isSelectItem = false;

    // add new
    @Input() autoAddNew = false;
    @Input() btnAddStyle = 'btn btn-primary';
    @Input() btnAddLabel = 'Add new item';
    @Input() btnAddSubStyle = 'btn btn-default';
    @Input() btnAddSubLabel = 'Add new sub item';

    private _required: any;

    @Input()
    get required(): any {
        return this._required;
    }

    set required(value: any) {
        this._required = typeof value !== 'undefined';
    }

    @Output() change = new EventEmitter();

    newVal: any;
    isAddNew = true;

    currentValues: any[] = [];
    subValues: any[] = [];
    selectCurrentValues: any[] = [];
    selectSubValues: any[] = [];

    @ViewChild(NgModel, {static: true}) model: NgModel;

    subscriptions: Subscription[] = [];

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private utilitiesService: UtilitiesService
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
        this.parseValue();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.items) {
            const changeItems = changes.items;
            if (changeItems.currentValue !== changeItems.previousValue) {

                const items = changeItems.currentValue;

                // remove if not exist in new list.
                if (this.value) {
                    this.utilitiesService.removeItem(this.value, (val: any) => {
                        let isFound = items.findIndex(item => item[this.bindValue] === val) !== -1;
                        if (!isFound) {
                            items.forEach(
                                item => {
                                    if (isFound) {
                                        return;
                                    }
                                    isFound = item.children.findIndex(subItem => subItem[this.bindValue] === val) !== -1;
                                }
                            );
                        }

                        return !isFound;
                    });

                    // check empty
                    this.checkEmpty();

                }

                // check disabled
                this.checkDisabled();
            }
        }
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    parseValue() {
        let count = 0;
        const source = of(null);
        const example = source.pipe(
            map(() => {
                if (!this.value || this.value.length === 0 ||
                    !this.items || this.items.length === 0) {
                    throw 1;
                }
                return 1;
            }),
            retryWhen(errors =>
                errors.pipe(
                    // restart in 1 seconds
                    delay(500)
                )
            ),
            delay(100),
        );
        const parseValueSub = example.subscribe(() => {
            if (this.isTree) {
                this.doParseValueWithSubItem();
            } else {
                this.doParseValueNotSubItem();
            }
        });
        this.subscriptions.push(parseValueSub);
    }

    doParseValueWithSubItem() {
        // value
        let tempIndex = -1;

        this.currentValues = [];
        this.subValues = [];
        this.selectCurrentValues = [];
        this.selectSubValues = [];

        this.value.forEach(
            (value, index) => {
                const item = this.items.find(x => x[this.bindValue] === value);
                if (item) { // is Parent Item
                    if (this.currentValues.findIndex(v => v === value) === -1) {
                        this.currentValues.push(value);
                        this.selectCurrentValues.push(true);
                        this.subValues.push(null);
                    }
                    tempIndex = index + 1;
                } else { // is Child Item
                    this.items.forEach(parentItem => {

                        if (parentItem.children && parentItem.children.length > 0) {
                            const subItem = parentItem.children.find(x => x[this.bindValue] === value);
                            if (subItem) {

                                let indexParent: number;
                                indexParent = this.currentValues.findIndex(v => v === parentItem[this.bindValue]);
                                if (indexParent === -1) {
                                    this.currentValues.push(parentItem[this.bindValue]);
                                    indexParent = this.currentValues.findIndex(v => v === parentItem[this.bindValue]);
                                }

                                // check duplicate sub-item & parent-item
                                const isCheckParent = this.value.findIndex(val => val === parentItem[this.bindValue]) !== -1;
                                if (isCheckParent) {
                                    // this.value = this.value.filter(val => val !== parentItem[this.bindValue]);
                                    // this.doParseValue(); // run parse value again.
                                    //
                                    // isStop = true;
                                    // return;

                                    this.selectCurrentValues[indexParent] = true;

                                } else {
                                    this.selectCurrentValues[indexParent] = false;

                                }

                                // push subValues
                                if (!this.subValues[indexParent]) {
                                    this.subValues[indexParent] = [];
                                }
                                this.subValues[indexParent].push(subItem[this.bindValue]);

                                // push selectSubValues
                                if (!this.selectSubValues[indexParent]) {
                                    this.selectSubValues[indexParent] = [];
                                }
                                this.selectSubValues[indexParent].push(true);

                            }
                        }
                    });
                }
            }
        );

        // check Disabled
        this.checkDisabled();
    }

    doParseValueNotSubItem() {
        this.currentValues = [];
        this.subValues = [];
        this.selectCurrentValues = [];
        this.selectSubValues = [];

        this.value.forEach(
            (value) => {
                const item = this.items.find(x => x[this.bindValue] === value);
                if (item) {
                    this.currentValues.push(value);
                    this.selectCurrentValues.push(true);
                }
            }
        );

        // check Disabled
        this.checkDisabled();
    }

    onChangeNewVal(val: any) {
        if (!val) {
            return;
        }

        if (!this.currentValues) {
            this.currentValues = [];
        }

        if (!this.currentValues.includes(val)) {
            this.currentValues.push(val);
        }

        const index = this.currentValues.findIndex(v => v === val);
        if (index !== -1) {
            this.selectCurrentValues[index] = true;
        }

        if (!this.autoAddNew) {
            this.newVal = null;
            this.isAddNew = false;
        } else {
            this.newVal = null;

            this.isAddNew = false;
            timer(100).subscribe(() => this.isAddNew = true);
        }

        this.model.control.markAsTouched();
        this.checkDisabled();
        this.checkSubValues();
        this.flattenValue();

        this.change.emit(this.value);
    }

    onChangeSubVal(subVal: any) {
        this.flattenValue();
    }

    checkDisabled() {
        if (!this.currentValues) {
            return;
        }

        this.items.forEach(x => {
            x.disabled = this.currentValues.includes(x[this.bindValue]);
        });
    }

    checkEmpty() {
        if (this.currentValues.length === 0) {
            this.addNew();
        }
    }

    getSubItems(itemVal: any) {
        const item = this.items.find(x => x[this.bindValue] === itemVal);
        if (item &&
            item.children &&
            item.children.length > 0) {
            return item.children;
        }

        return false;
    }

    checkSubValues() {
        if (this.isTree) {
            this.currentValues.forEach(
                (val, index) => {
                    const newItem = this.items.find(x => x[this.bindValue] === val);
                    if (
                        !(newItem && newItem.children && newItem.children.length > 0) ||
                        !this.subValues[index]
                    ) {
                        this.subValues[index] = [];
                    }
                }
            );
        }
    }

    flattenValue() {
        let modelValue = [];
        const currentValues = this.utilitiesService.cloneDeep(this.currentValues);
        const subValues = this.utilitiesService.cloneDeep(this.subValues);
        const selectCurrentValues = this.utilitiesService.cloneDeep(this.selectCurrentValues);

        if (currentValues) {
            currentValues.forEach(
                (value, index) => {
                    let appendValues = [];

                    // parent
                    const selectedValue = selectCurrentValues[index];
                    if (selectedValue) {
                        appendValues.push(value);
                    }

                    // children
                    if (subValues[index] &&
                        subValues[index].length) {
                        appendValues = appendValues.concat(subValues[index]);
                    }

                    if (appendValues &&
                        appendValues.length > 0) {
                        modelValue = modelValue.concat(appendValues);
                    }
                }
            );
        }

        this.value = modelValue;
    }

    addNew() {
        this.isAddNew = true;
    }

    removeParent(itemVal: any) {
        if (!this.currentValues) {
            return;
        }

        const index = this.currentValues.findIndex(v => v === itemVal);
        if (index !== -1) {
            this.currentValues.splice(index, 1);
        }

        if (typeof this.subValues[index] !== 'undefined' && this.subValues[index]) {
            this.subValues.splice(index, 1);
        }

        this.checkEmpty();
        this.checkDisabled();
        this.flattenValue();

        this.change.emit(this.value);
    }

    removeAddNew() {
        this.newVal = null;
        this.isAddNew = false;
    }

    isDisabledAddNew(): boolean {
        return this.currentValues &&
            this.items &&
            this.currentValues.length === this.items.length;
    }

    onChangeSelectItem(index: number) {
        this.flattenValue();
    }
}
