import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiSelectSimpleComponent } from './cxi-select-simple.component';

describe('CxiSelectSimpleComponent', () => {
  let component: CxiSelectSimpleComponent;
  let fixture: ComponentFixture<CxiSelectSimpleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiSelectSimpleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiSelectSimpleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
