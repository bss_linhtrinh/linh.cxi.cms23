import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiPeriodDateComponent } from './cxi-period-date.component';

describe('CxiPeriodDateComponent', () => {
  let component: CxiPeriodDateComponent;
  let fixture: ComponentFixture<CxiPeriodDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiPeriodDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiPeriodDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
