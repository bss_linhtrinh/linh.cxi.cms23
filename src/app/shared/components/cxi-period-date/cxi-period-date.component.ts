import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';
import {PeriodDate} from './period-date';

@Component({
    selector: 'cxi-period-date',
    templateUrl: './cxi-period-date.component.html',
    styleUrls: ['./cxi-period-date.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: CxiPeriodDateComponent, multi: true}
    ]
})
export class CxiPeriodDateComponent extends ElementBase<PeriodDate> implements OnInit {
    private _name: string;

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }


    private _required: any;

    @Input()
    get required(): any {
        return this._required;
    }

    set required(required: any) {
        this._required = typeof required !== 'undefined';
    }

    @Input() label: string;
    @Input() labelFrom: string;
    @Input() labelTo: string;

    _width: number; // rem
    @Input()
    get width(): number {
        return this._width; // rem
    }

    set width(width: number) {
        this._width = width / 14;
    }

    @Output() change = new EventEmitter<PeriodDate>();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    hours = [
        {name: '00', value: 0},
        {name: '01', value: 1},
        {name: '02', value: 2},
        {name: '03', value: 3},
        {name: '04', value: 4},
        {name: '05', value: 5},
        {name: '06', value: 6},
        {name: '07', value: 7},
        {name: '08', value: 8},
        {name: '09', value: 9},
        {name: '10', value: 10},
        {name: '11', value: 11},
        {name: '12', value: 12}
    ];
    minutes = [
        {name: '00', value: 0},
        {name: '05', value: 5},
        {name: '10', value: 10},
        {name: '15', value: 15},
        {name: '20', value: 20},
        {name: '25', value: 25},
        {name: '30', value: 30},
        {name: '35', value: 35},
        {name: '40', value: 40},
        {name: '45', value: 45},
        {name: '50', value: 50},
        {name: '55', value: 55}
    ];
    meridiems = ['AM', 'PM'];

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onChange() {
        this.change.emit(this.value);
    }

    show() {
        const fh = this.pad(this.value.fromHour);
        const fm = this.pad(this.value.fromMinute);
        const fn = this.value.fromMeridiem;
        const th = this.pad(this.value.toHour);
        const tm = this.pad(this.value.toMinute);
        const tn = this.value.toMeridiem;

        return `${fh}:${fm} ${fn} - ${th}:${tm} ${tn}`;
    }

    pad(n: number) {
        return n > 9 ? `${n}` : `0${n}`;
    }
}
