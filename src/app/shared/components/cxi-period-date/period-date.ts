import {BaseEntity} from '../../entities/base.entity';

export class PeriodDate extends BaseEntity {
    fromHour: number;
    fromMinute: number;
    fromMeridiem: string;
    toHour: number;
    toMinute: number;
    toMeridiem: string;

    constructor() {
        super();
        this.fromHour = 0;
        this.fromMinute = 0;
        this.fromMeridiem = 'AM';
        this.toHour = 0;
        this.toMinute = 0;
        this.toMeridiem = 'AM';
    }

    static create(data): PeriodDate {
        return (new PeriodDate()).fill(data);
    }

    fill(obj): this {
        return super.fill(obj);
    }

    parse() {
        return {
            fromHour: this.fromHour,
            fromMinute: this.fromMinute,
            fromMeridiem: this.fromMeridiem,
            toHour: this.toHour,
            toMinute: this.toMinute,
            toMeridiem: this.toMeridiem,
        };
    }
}
