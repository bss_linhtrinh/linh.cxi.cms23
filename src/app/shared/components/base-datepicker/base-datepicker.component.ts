import {Component, EventEmitter, Inject, Input, OnChanges, OnInit, Optional, Output, SimpleChanges, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'base-datepicker',
    templateUrl: './base-datepicker.component.html',
    styleUrls: ['./base-datepicker.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: BaseDatepickerComponent, multi: true}
    ]
})
export class BaseDatepickerComponent extends ElementBase<string> implements OnInit, OnChanges {
    @Input() public name: string;
    @Input() public label: string;
    @Input() public type: string;
    @Input() public width = '';

    private _disabled: any;

    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(disabled: boolean) {
        this._disabled = disabled;
    }

    @ViewChild(NgModel, {static: true}) model: NgModel;

    internalMinDate: NgbDateStruct = {year: 1970, month: 1, day: 1};
    internalMaxDate: NgbDateStruct = {year: 2099, month: 12, day: 31};

    MIN_DATE = new Date('1970-01-01');
    MAX_DATE = new Date('2099-12-31');

    @Input() public minDate: Date;
    @Input() public maxDate: Date;

    @Output() change = new EventEmitter();

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
        //
    }

    ngOnChanges(changes: SimpleChanges) {
        for (const propName in changes) {
            const change = changes[propName];
            if (change.currentValue) {
                if (propName === 'minDate') {
                    const minDate = this.minDate < this.MIN_DATE
                        ? this.MIN_DATE
                        : this.minDate;
                    this.internalMinDate = this.parse(minDate);
                }
                if (propName === 'maxDate') {
                    const maxDate = this.maxDate > this.MAX_DATE
                        ? this.MAX_DATE
                        : this.maxDate;
                    this.internalMaxDate = this.parse(maxDate);
                }
            }
        }
    }

    parse(objDate: Date): NgbDateStruct {
        return (objDate && objDate.getFullYear)
            ? {
                year: objDate.getFullYear(),
                month: objDate.getMonth() + 1,
                day: objDate.getDate()
            }
            : null;
    }

    onChangeDatepickerValue(date) {
        this.change.emit();
    }
}
