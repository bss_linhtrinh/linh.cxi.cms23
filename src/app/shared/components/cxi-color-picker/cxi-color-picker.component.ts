import {Component, Inject, Input, OnInit, Optional, ViewChild} from '@angular/core';
import {ElementBase} from '../../custom-form/element-base/element-base';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';

@Component({
    selector: 'cxi-color-picker',
    templateUrl: './cxi-color-picker.component.html',
    styleUrls: ['./cxi-color-picker.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: CxiColorPickerComponent, multi: true}
    ]
})
export class CxiColorPickerComponent extends ElementBase<number> implements OnInit {

    @Input() name: string;
    @Input() label: string;

    private _required: any;

    @Input()
    get required(): any {
        return this._required;
    }

    set required(required: any) {
        this._required = typeof required !== 'undefined';
    }

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

}
