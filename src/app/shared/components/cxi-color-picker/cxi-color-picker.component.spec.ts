import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiColorPickerComponent } from './cxi-color-picker.component';

describe('CxiColorPickerComponent', () => {
  let component: CxiColorPickerComponent;
  let fixture: ComponentFixture<CxiColorPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiColorPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiColorPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
