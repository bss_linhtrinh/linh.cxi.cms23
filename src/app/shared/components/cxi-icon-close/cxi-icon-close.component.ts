import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'cxi-icon-close',
    templateUrl: './cxi-icon-close.component.html',
    styleUrls: ['./cxi-icon-close.component.scss']
})
export class CxiIconCloseComponent implements OnInit {
    @Input() name: string;
    @Input() title = 'Remove';

    // @Output() click = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

    // onClick() {
    //     this.click.emit();
    // }
}
