import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiIconCloseComponent } from './cxi-icon-close.component';

describe('CxiIconCloseComponent', () => {
  let component: CxiIconCloseComponent;
  let fixture: ComponentFixture<CxiIconCloseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiIconCloseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiIconCloseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
