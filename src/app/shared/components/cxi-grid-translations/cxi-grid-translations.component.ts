import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LocalStorage} from 'ngx-webstorage';

@Component({
    selector: 'cxi-grid-translations',
    templateUrl: './cxi-grid-translations.component.html',
    styleUrls: ['./cxi-grid-translations.component.scss']
})
export class CxiGridTranslationsComponent implements OnInit {
    @Input() name: string;
    @Output() change = new EventEmitter<string>();

    @LocalStorage('settings.locale') selectedLocale: string;

    DEFAULT_LOCALE = 'en';
    locales: any[] = [
        {name: 'en', image: 'assets/img/english.png'},
        {name: 'vi', image: 'assets/img/vn.png'},
    ];

    constructor() {
    }

    ngOnInit() {
        this.getDefaultLocale();
    }

    getDefaultLocale() {
        if (!this.selectedLocale) {
            this.selectedLocale = this.DEFAULT_LOCALE;
        }
    }

    select(locale: any) {
        if (
            locale &&
            locale.name &&
            this.selectedLocale !== locale.name
        ) {
            this.selectedLocale = locale.name;
            this.change.emit(this.selectedLocale);
        }
    }
}
