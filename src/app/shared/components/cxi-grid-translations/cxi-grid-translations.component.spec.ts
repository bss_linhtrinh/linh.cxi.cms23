import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiGridTranslationsComponent } from './cxi-grid-translations.component';

describe('CxiGridTranslationsComponent', () => {
  let component: CxiGridTranslationsComponent;
  let fixture: ComponentFixture<CxiGridTranslationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiGridTranslationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiGridTranslationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
