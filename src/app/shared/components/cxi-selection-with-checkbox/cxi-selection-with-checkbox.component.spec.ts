import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiSelectionWithCheckboxComponent } from './cxi-selection-with-checkbox.component';

describe('CxiSelectionWithCheckboxComponent', () => {
  let component: CxiSelectionWithCheckboxComponent;
  let fixture: ComponentFixture<CxiSelectionWithCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiSelectionWithCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiSelectionWithCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
