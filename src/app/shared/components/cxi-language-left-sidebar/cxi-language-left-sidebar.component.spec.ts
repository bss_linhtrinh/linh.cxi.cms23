import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiLanguageLeftSidebarComponent } from './cxi-language-left-sidebar.component';

describe('CxiLanguageLeftSidebarComponent', () => {
  let component: CxiLanguageLeftSidebarComponent;
  let fixture: ComponentFixture<CxiLanguageLeftSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiLanguageLeftSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiLanguageLeftSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
