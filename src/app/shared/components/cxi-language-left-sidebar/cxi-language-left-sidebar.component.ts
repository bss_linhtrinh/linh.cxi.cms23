import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Language} from '../../entities/language';
import {LANGUAGES} from '../../constants/languages';
import {LanguageService} from '../../services/language/language.service';
import {tap} from 'rxjs/operators';
import {REM} from '../../constants/app';
import {LocalStorage} from 'ngx-webstorage';
import {StorageService} from '../../services/storage/storage.service';

@Component({
    selector: 'cxi-language-left-sidebar',
    templateUrl: './cxi-language-left-sidebar.component.html',
    styleUrls: ['./cxi-language-left-sidebar.component.scss']
})
export class CxiLanguageLeftSidebarComponent implements OnInit, OnDestroy {
    @Input() name: string;

    private _language: string;
    @Input('tab')
    get language(): string {
        return this._language;
    }

    @Output('tabChange') languageChange = new EventEmitter<string>();

    set language(language: string) {
        if (language !== this._language) {
            this._language = language;
            this.languageChange.emit(this.language);
        }
    }

    languages: Language[] = [];

    @LocalStorage('language-left-sidebar.locale') internalLocale: string;

    constructor(
        private languageService: LanguageService,
        private storageService: StorageService
    ) {

    }

    ngOnInit() {
        this.languages = LANGUAGES.map(l => Language.create(l));
        this.languageService.get()
            .pipe(
                tap((lang) => this.language = lang),
                tap((lang) => this.internalLocale = lang)
            )
            .subscribe();
    }

    ngOnDestroy() {
        this.storageService.clear('language-left-sidebar.locale');
    }

    select(lang: Language) {
        if (lang.code !== this.language) {
            this.internalLocale = lang.code;
            this.language = lang.code;
        }
    }

    getTopArrow() {
        const findIndex = this.languages.findIndex(l => l.code === this.language);

        let px: number;
        let rem: number;
        const h = 60;
        const size = 24;
        if (findIndex !== -1) {
            px = (h - size) / 2;
            px += findIndex * h;
        } else {
            px = -9999;
        }

        rem = px / REM;
        return rem;
    }
}
