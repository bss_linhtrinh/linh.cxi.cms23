import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseSelectionTwoColumnsComponent } from './base-selection-two-columns.component';

describe('BaseSelectionTwoColumnsComponent', () => {
  let component: BaseSelectionTwoColumnsComponent;
  let fixture: ComponentFixture<BaseSelectionTwoColumnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseSelectionTwoColumnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseSelectionTwoColumnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
