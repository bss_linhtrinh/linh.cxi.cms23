import {Component, Inject, Input, OnInit, Optional, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';
import {BaseFilterCriteria} from '../../entities/base-filter-criteria';

@Component({
    selector: 'base-selection-two-columns',
    templateUrl: './base-selection-two-columns.component.html',
    styleUrls: ['./base-selection-two-columns.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: BaseSelectionTwoColumnsComponent, multi: true}
    ]
})
export class BaseSelectionTwoColumnsComponent extends ElementBase<number[]> implements OnInit {
    @Input() name: string;
    @Input() label: string;
    @Input() public type: string;
    @Input() public placeholder: string;
    @Input() bindLabel = null;
    @Input() bindValue = null;
    @Input() public items: any[];

    @ViewChild(NgModel, {static: true}) model: NgModel;

    filterAttrs: BaseFilterCriteria = BaseFilterCriteria.create({name: 'name', type: 'text', value: ''});

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    select(item: any) {
        this.value.push(item[this.bindValue]);
    }

    unselect(item: any) {
        const index = this.value.findIndex(x => x === item.id);
        if (index !== -1) {
            this.value.splice(index, 1);
        }
    }
}
