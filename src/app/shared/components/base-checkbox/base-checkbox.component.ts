import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';
import {AuthService} from '../../services/auth/auth.service';

@Component({
    selector: 'base-checkbox',
    templateUrl: './base-checkbox.component.html',
    styleUrls: ['./base-checkbox.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: BaseCheckboxComponent, multi: true}
    ]
})

export class BaseCheckboxComponent extends ElementBase<boolean> implements OnInit {

    inputText: string;
    @Input() public name: string;
    @Input() public title: string;
    @Input() public label: string;
    @Input() public placeholder: string;
    @Input() public textEllipsis: boolean;

    @Input() public type: string;
    @Input() public allowInput = false;

    @Input() public isSquare = false;
    @Input() public isCircle = false;
    @Input() public color = '';
    @Input() public bulletStripeColor1 = '';
    @Input() public bulletStripeColor2 = '';
    @Input() public bulletStripeWidth = '';

    @Output() change = new EventEmitter<boolean>();
    @Output() inputChange = new EventEmitter<any>();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
    ) {
        super(validators, asyncValidators);
    }

    private _disabled: any;

    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(disabled: boolean) {
        this._disabled = disabled;
    }

    ngOnInit() {
    }

    onChange() {
        this.change.emit(this.value);
    }

    onChangeInput(event) {
        if (event.key === 'Enter') {
            this.inputChange.emit(this.inputText);
            this.inputText = '';
        }
    }

    getBackGroundStripeColor() {
        let background: string;

        const bulletStripeWidth = parseInt(this.bulletStripeWidth, 10);

        background = `repeating-linear-gradient(` +
            ` 135deg,` +
            ` ${this.bulletStripeColor1},` +
            ` ${this.bulletStripeColor1} ${bulletStripeWidth}px,` +
            ` ${this.bulletStripeColor2} ${bulletStripeWidth}px,` +
            ` ${this.bulletStripeColor2} ${2 * bulletStripeWidth}px` +
            `)`;

        return background;
    }
}

