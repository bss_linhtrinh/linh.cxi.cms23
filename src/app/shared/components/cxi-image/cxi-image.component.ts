import {Component, Input, OnInit} from '@angular/core';
import {Image} from '../../entities/image';

@Component({
    selector: 'cxi-image',
    templateUrl: './cxi-image.component.html',
    styleUrls: ['./cxi-image.component.scss']
})
export class CxiImageComponent implements OnInit {
    private _src: string | ArrayBuffer | Image;

    @Input('src')

    get src(): string | ArrayBuffer | Image {
        return this._src;
    }

    set src(value: string | ArrayBuffer | Image) {
        if (typeof value === 'string') {
            this._src = value;
        } else if (value instanceof ArrayBuffer) {
            this._src = value;
        } else if (value instanceof Image) {
            this._src = value.original_image;
        } else {
            this._src = null;
        }
    }

    protected baseSize = 14; // px
    @Input() borderRadius = 8; // px
    @Input() width = 126; // px
    @Input() height = 126; // px

    constructor() {
    }

    ngOnInit() {
    }

    // image
    errorHandler(event) {
        event.target.src = 'assets/img/image-default.jpg';
    }

    getBorderRadius() {
        return `${this.borderRadius / this.baseSize}`;
    }

    getWidth() {
        return `${this.width / this.baseSize}`;
    }

    getHeight() {
        return `${this.height / this.baseSize}`;
    }
}
