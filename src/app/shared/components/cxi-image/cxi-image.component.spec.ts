import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiImageComponent } from './cxi-image.component';

describe('CxiImageComponent', () => {
  let component: CxiImageComponent;
  let fixture: ComponentFixture<CxiImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
