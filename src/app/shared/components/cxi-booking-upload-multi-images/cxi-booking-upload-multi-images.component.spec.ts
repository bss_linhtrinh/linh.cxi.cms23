import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiBookingUploadMultiImagesComponent } from './cxi-booking-upload-multi-images.component';

describe('CxiBookingUploadMultiImagesComponent', () => {
  let component: CxiBookingUploadMultiImagesComponent;
  let fixture: ComponentFixture<CxiBookingUploadMultiImagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiBookingUploadMultiImagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiBookingUploadMultiImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
