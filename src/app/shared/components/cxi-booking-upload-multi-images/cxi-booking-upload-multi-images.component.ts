import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';
import {Image} from '../../entities/image';

@Component({
  selector: 'app-cxi-booking-upload-multi-images',
  templateUrl: './cxi-booking-upload-multi-images.component.html',
  styleUrls: ['./cxi-booking-upload-multi-images.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: CxiBookingUploadMultiImagesComponent, multi: true}
    ]
})
export class CxiBookingUploadMultiImagesComponent extends ElementBase<Image[]> implements OnInit {

    private _name: string;
    private _label: string;

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(label: string) {
        this._label = label.replace('_', ' ');
    }

    @Input() public size = 'sm';
    @Input() public width = '';
    @Input() public type = 'text';
    @Input() public placeholder: string;

    private _required: any;

    @Input()
    get required(): any {
        return this._required;
    }

    set required(required: any) {
        this._required = typeof required !== 'undefined';
    }

    @Output() file = new EventEmitter();

    @Input() imageLink: any = '';

    @Input() images = [];

    selectedFiles = [];
    @Input() oldImageLength;

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onChange() {
    }

    onFileChanged(event, el?: any) {
        if (event.target.files && event.target.files[0]) {
            for (let i = 0; i < event.target.files.length; i++) {
                const reader = new FileReader();

                reader.onload = (event: Event) => {
                    this.images.push({url: reader.result});
                };
                this.selectedFiles.push(event.target.files[i]);
                this.file.emit({oldVal: this.images, newVal: this.selectedFiles});

                reader.readAsDataURL(event.target.files[i]);
            }
        }
    }

    removeImage(index: number, el?: any) {
        if (this.oldImageLength >= 1) {
            if (this.images && this.images.length) {
                this.images.splice(index, 1);
                if (index >= this.oldImageLength) {
                    this.selectedFiles.splice(index - this.oldImageLength, 1);
                }
                if (index < this.oldImageLength && !this.selectedFiles.length) {
                    this.oldImageLength = this.images.length;
                }
                if (index < this.oldImageLength && this.selectedFiles.length) {
                    this.oldImageLength = this.images.length - this.selectedFiles.length;
                }
                this.file.emit({oldVal: this.images, newVal: this.selectedFiles});
            }
        } else {
            if (this.images && this.images.length) {
                this.images.splice(index, 1);
                this.selectedFiles.splice(index, 1);
                this.file.emit({oldVal: this.images, newVal: this.selectedFiles});
            }
        }

    }
}
