import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';
import {UtilitiesService} from '../../services/utilities/utilities.service';

@Component({
  selector: 'app-cxi-radio',
  templateUrl: './cxi-radio.component.html',
  styleUrls: ['./cxi-radio.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: CxiRadioComponent, multi: true}
    ]
})
export class CxiRadioComponent extends ElementBase<any> implements OnInit {


    @Input() public name: string;
    @Input() public label: string;
    @Input() public type: string;
    @Input() public placeholder: string;
    @Input() bindLabel = null;
    @Input() bindValue = null;

    @Input() public values: any[];

    @ViewChild(NgModel, {static: true}) model: NgModel;

    @Output() change = new EventEmitter();

    private _disabled: any;
    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(disabled: boolean) {
        this._disabled = disabled;
    }

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private utilitiesService: UtilitiesService
    ) {
        super(validators, asyncValidators);
    }


    ngOnInit() {
    }

    onChange() {
        this.change.emit(this.value);
    }

    makeTitle(slug: string) {
        return this.utilitiesService.makeTitle(slug);
    }
}
