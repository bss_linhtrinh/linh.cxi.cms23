import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiRadioComponent } from './cxi-radio.component';

describe('CxiRadioComponent', () => {
  let component: CxiRadioComponent;
  let fixture: ComponentFixture<CxiRadioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiRadioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiRadioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
