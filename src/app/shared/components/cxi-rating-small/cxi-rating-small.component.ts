import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'cxi-rating-small',
    templateUrl: './cxi-rating-small.component.html',
    styleUrls: ['./cxi-rating-small.component.scss']
})
export class CxiRatingSmallComponent implements OnInit {
    _rating: number = 0;
    percent: number;
    @Input()
    set rating(val: number) {
        this._rating = val;
        if (this._rating) {
            this.getPercentRemainder();
        }
    }
    get rating(): number {
        return this._rating;
    }
    @Input() onlyIcon: boolean = false;
    @Input() color: string = '#FFC70F';

    constructor() {
    }

    ngOnInit() {
    }

    private getPercentRemainder() {
        // tslint:disable-next-line:radix
        const iNum = parseInt(this._rating.toString());
        // tslint:disable-next-line:radix
        this.percent = parseInt(((this._rating - iNum) * 100).toString());
    }

}
