import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiRatingSmallComponent } from './cxi-rating-small.component';

describe('CxiRatingSmallComponent', () => {
  let component: CxiRatingSmallComponent;
  let fixture: ComponentFixture<CxiRatingSmallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiRatingSmallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiRatingSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
