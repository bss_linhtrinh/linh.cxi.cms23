import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiInputPasswordComponent } from './cxi-input-password.component';

describe('CxiInputPasswordComponent', () => {
  let component: CxiInputPasswordComponent;
  let fixture: ComponentFixture<CxiInputPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiInputPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiInputPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
