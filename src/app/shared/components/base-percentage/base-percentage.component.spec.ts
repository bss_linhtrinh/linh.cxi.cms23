import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasePercentageComponent } from './base-percentage.component';

describe('BasePercentageComponent', () => {
  let component: BasePercentageComponent;
  let fixture: ComponentFixture<BasePercentageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasePercentageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasePercentageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
