import {Component, Inject, Input, OnInit, Optional, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';

@Component({
    selector: 'base-percentage',
    templateUrl: './base-percentage.component.html',
    styleUrls: ['./base-percentage.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: BasePercentageComponent, multi: true}
    ]
})
export class BasePercentageComponent extends ElementBase<number> implements OnInit {
    @Input() name: string;
    @Input() label: string;
    @Input() public type = 'text';
    @Input() public placeholder = '';
    @Input() showIncreasement: boolean;

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onChange() {

    }
}
