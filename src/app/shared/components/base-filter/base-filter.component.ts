import { Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild } from '@angular/core';
import { BaseFilterCriteria } from '../../entities/base-filter-criteria';
import { ElementBase } from '../../custom-form/element-base/element-base';
import { NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel } from '@angular/forms';

@Component({
    selector: 'base-filter',
    templateUrl: './base-filter.component.html',
    styleUrls: ['./base-filter.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: BaseFilterComponent, multi: true }
    ]
})
export class BaseFilterComponent extends ElementBase<BaseFilterCriteria[]> implements OnInit {
    public isShow: boolean = false;
    @Input() name: string;

    @ViewChild(NgModel, { static: true }) model: NgModel;

    @Output() change = new EventEmitter<BaseFilterCriteria[]>();
    @Output() messageFilterCustomer = new EventEmitter();
    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    // is Disabled
    isDisabledAddAFilter(): boolean {
        return this.value && (this.value.filter(f => !f.selected).length === 0);
    }

    addAFilter(filter) {
        (filter.name == "without_salesman") ? filter.isShow = true : filter.isShow = false;
        this.messageFilterCustomer.emit(filter);
        filter.selected = true;
    }

    remove(index) {
        this.value[index].selected = false;
        this.value[index].value = null;

        this.change.emit(this.value);
    }

    // on change
    onChangeFilterInput() {
        this.change.emit(this.value);
    }
}
