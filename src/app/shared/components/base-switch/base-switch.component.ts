import {Component, Inject, Input, OnInit, Optional, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';

@Component({
    selector: 'base-switch',
    templateUrl: './base-switch.component.html',
    styleUrls: ['./base-switch.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: BaseSwitchComponent, multi: true}
    ]
})
export class BaseSwitchComponent extends ElementBase<boolean> implements OnInit {
    @Input() public name: string;
    @Input() public label: string;
    @Input() public yesLabel: string;
    @Input() public noLabel: string;
    @Input() public placeholder: string;
    @Input() public type: string;

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {

    }

    onChange() {

    }
}
