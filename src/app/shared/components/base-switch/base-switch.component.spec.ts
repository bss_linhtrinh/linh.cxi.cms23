import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseSwitchComponent } from './base-switch.component';

describe('BaseSwitchComponent', () => {
  let component: BaseSwitchComponent;
  let fixture: ComponentFixture<BaseSwitchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseSwitchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseSwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
