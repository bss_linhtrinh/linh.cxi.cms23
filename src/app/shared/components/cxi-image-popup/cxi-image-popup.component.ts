import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-cxi-image-popup',
    templateUrl: './cxi-image-popup.component.html',
    styleUrls: ['./cxi-image-popup.component.scss']
})
export class CxiImagePopupComponent implements OnInit {

    @Input() image = {url: ''};

    constructor(private ngbActiveModal: NgbActiveModal) {
    }

    ngOnInit() {
    }

    close() {
        this.ngbActiveModal.dismiss('cancel');
    }

}
