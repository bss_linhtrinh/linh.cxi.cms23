import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiImagePopupComponent } from './cxi-image-popup.component';

describe('CxiImagePopupComponent', () => {
  let component: CxiImagePopupComponent;
  let fixture: ComponentFixture<CxiImagePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiImagePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiImagePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
