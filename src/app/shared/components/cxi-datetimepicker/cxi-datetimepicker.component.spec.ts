import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiDatetimepickerComponent } from './cxi-datetimepicker.component';

describe('CxiDatetimepickerComponent', () => {
  let component: CxiDatetimepickerComponent;
  let fixture: ComponentFixture<CxiDatetimepickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiDatetimepickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiDatetimepickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
