import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';

@Component({
    selector: 'app-cxi-datetimepicker',
    templateUrl: './cxi-datetimepicker.component.html',
    styleUrls: ['./cxi-datetimepicker.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: CxiDatetimepickerComponent, multi: true}
    ]
})
export class CxiDatetimepickerComponent extends ElementBase<string> implements OnInit {

    @Input() public type = 'both';
    @Input() public minDate;
    @Input() public maxDate;

    @Input() public name: string;
    @Input() public label: string;
    @Output() onChangeStartTime = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    private _disabled: any;

    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(disabled: boolean) {
        this._disabled = disabled;
    }

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onChange() {
        this.onChangeStartTime.emit(this.value);
    }

}
