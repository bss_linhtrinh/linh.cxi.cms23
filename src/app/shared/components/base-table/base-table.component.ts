import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {BaseFilterCriteria} from '../../entities/base-filter-criteria';
import {from, Observable, of, Subject, Subscription} from 'rxjs';
import {BaseTableService} from './base-table.service';
import {BaseFilterPipe} from '../../pipes/base-filter/base-filter.pipe';
import {BasePagingPipe} from '../../pipes/base-paging/base-paging.pipe';
import {BaseSortPipe} from '../../pipes/base-sort/base-sort.pipe';
import {environment} from '../../../../environments/environment';
import {BaseColumn} from './base-column';
import {BaseTableConfig} from './base-table-config';
import {User} from '../../entities/user';
import {AuthService} from '../../services/auth/auth.service';
import {ColumnFormat} from '../../types/column-format';
import {ColumnFilterType} from '../../types/column-filter-type';
import {OrderStatus} from '../../types/order-status';
import {LocalStorage, SessionStorageService} from 'ngx-webstorage';
import {debounceTime, tap} from 'rxjs/operators';
import {StorageService} from '../../services/storage/storage.service';

enum SelectedRowStatus {
    All = 2,
    Something = 1,
    None = 0
}

@Component({
    selector: 'base-table',
    templateUrl: './base-table.component.html',
    styleUrls: ['./base-table.component.scss']
})
export class BaseTableComponent implements OnInit, OnChanges, OnDestroy {
    @Input() name: string;

    // @Input() page = 1;
    // @Input() pageSize = 8;

    @Input() config?: BaseTableConfig;
    @Input() rows?: Array<any> = [];
    @Input() columns: BaseColumn[];
    @Input() filterAttrs?: BaseFilterCriteria[] = [];
    @Input() search = '';

    @Output() actionColumn = new EventEmitter<any>();
    @Output() actionTab = new EventEmitter<any>();

    selectedAll: boolean;
    private selectedStatus: SelectedRowStatus;

    columnName: string;
    reverse: boolean;

    @LocalStorage('pagination.current_page') current_page: number;
    @LocalStorage('pagination.per_page') per_page: number;
    @LocalStorage('pagination.from') fromPage: number;
    @LocalStorage('pagination.to') toPage: number;
    @LocalStorage('pagination.total') total: number;

    // get from(): number {
    //     return (this.pagination.page - 1) * this.pagination.pageSize;
    // }
    get from(): number {
        return this.fromPage;
    }

    // get to(): number {
    //     return this.from + this.pagination.pageSize;
    // }
    get to(): number {
        return this.toPage;
    }

    subscriptions: Subscription[] = [];
    repositorySubscription: Subscription;
    baseUrl: string = environment.baseUrl;

    currentUser: any;

    // tabs
    selectedTab: string;
    selectedTabValue = 'pending';

    constructor(
        private baseSortPipe: BaseSortPipe,
        private baseFilterPipe: BaseFilterPipe,
        private basePagingPipe: BasePagingPipe,
        private baseTableService: BaseTableService,
        private authService: AuthService,
    ) {

        const removeSubscriber = baseTableService.removedRow$
            .subscribe(
                (row: any) => this.onEventRowRemoved(row)
            );

        const updateSubscriber = baseTableService.updatedRow$
            .subscribe(
                (row: any) => this.onEventRowUpdated(row)
            );

        this.subscriptions.push(removeSubscriber);
        this.subscriptions.push(updateSubscriber);
    }

    ngOnInit() {
        this.currentUser = this.authService.currentUser();
        this.getTabs();
        this.setPaginationDefault();
        this.transformedRows();
    }

    ngOnDestroy() {
        this.subscriptions.forEach(s => s.unsubscribe());
        if (this.repositorySubscription) {
            this.repositorySubscription.unsubscribe();
        }
    }

    setPaginationDefault() {
        if (!this.current_page || !this.per_page) {
            this.current_page = 1;
            this.per_page = 5;
        }
    }

    onChangePagination() {
        this.transformedRows();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.columns) {
            this.filterAttrs = this.columns.map((column) => {
                return BaseFilterCriteria.create({
                    name: column.name,
                    title: column.title,
                    filterType: column.filterType ? column.filterType : ColumnFilterType.Text,
                    format: column.format ? column.format : ColumnFormat.None,
                });
            });

            this.filterAttributesByTabs();
        }

        if (changes.search) {
            const searchCriteria = BaseFilterCriteria.create({
                name: '*',
                title: '*',
                filterType: ColumnFilterType.Text,
                format: ColumnFormat.None,
                value: changes.search.currentValue
            });

            if (this.filterAttrs) {
                const indexSearchCriteria = this.filterAttrs.findIndex(attr => attr.name === searchCriteria.name);
                if (indexSearchCriteria === -1) {
                    this.filterAttrs.push(searchCriteria);
                } else {
                    this.filterAttrs[indexSearchCriteria] = searchCriteria;
                }
            }
        }
    }

    filterAttributesByTabs() {
        // filter by tabs
        if (this.config.multitab && this.config.tabs && this.config.tabs.length > 0) {
            const tab = this.config.tabs.find(t => t.title === this.selectedTab);
            if (tab) {
                const criteriaByTabs = BaseFilterCriteria.create({
                    name: tab.name,
                    value: tab.value,
                });

                const findIndex = this.filterAttrs.findIndex(attr => attr.name === tab.name);
                if (findIndex !== -1) {
                    this.filterAttrs.splice(findIndex, 1);
                }

                this.filterAttrs.push(criteriaByTabs);
            }
        }
    }

    selectAll() {
        if (this.selectedAll) {
            this.selectedStatus = SelectedRowStatus.All;
        } else {
            this.selectedStatus = SelectedRowStatus.None;
        }

        // sort, filter, paging
        this.rows.forEach((r: any) => {
            r.selected = !!this.selectedStatus;
        });
    }

    /**
     * Select row
     */
    selectRow() {
        let selectStatus = SelectedRowStatus.None;

        const rows = this.rows;

        // + 1
        const selectedItem = rows.find((r: any) => {
            return r.selected;
        });
        if (selectedItem) {
            selectStatus = SelectedRowStatus.Something;

            // + 1
            const unselectedItem = rows.find((r: any) => {
                return !r.selected;
            });
            if (!unselectedItem) {
                selectStatus = SelectedRowStatus.All;
            }
        }

        this.selectedStatus = selectStatus;
    }

    // select
    isSelectedAll() {
        return this.selectedStatus === SelectedRowStatus.All;
    }

    isSelectedSomething() {
        return this.selectedStatus === SelectedRowStatus.Something;
    }

    isSelectedNone() {
        return this.selectedStatus === SelectedRowStatus.None;
    }

    // onSort change
    orderBy(columnName: string) {
        if (!this.config.sorting) {
            return;
        }

        if (this.columnName === columnName) {
            this.reverse = !this.reverse;
        } else {
            this.columnName = columnName;
            this.reverse = false;
        }

        this.resetSelectionItems();
    }

    // onPaging change
    onPageChange() {
        // this.resetSelectionItems();
    }

    // onFilterData change
    onChangeFilterData(filter) {
        this.resetSelectionItems();
    }

    resetSelectionItems() {
        this.selectedStatus = SelectedRowStatus.None;
        this.rows.forEach(r => r.selected = false);
    }

    // Rows transformed
    transformedRows(sorting?: boolean, filtering?: boolean, paging?: boolean) {

        if (!this.config.pagingServer) {

            let rows = this.rows;

            const isSorting = typeof sorting !== 'undefined' ? sorting : this.config.sorting;
            const isFiltering = typeof filtering !== 'undefined' ? filtering : this.config.filtering;
            const isPaging = typeof paging !== 'undefined' ? paging : this.config.paging;

            // sorting
            if (isSorting) {
                rows = this.baseSortPipe.transform(rows, this.columnName, this.reverse);
            }

            // filtering
            if (isFiltering) {
                rows = this.baseFilterPipe.transform(rows, this.filterAttrs);
            }

            // paging
            if (isPaging) {
                rows = this.basePagingPipe.transform(rows, this.current_page, this.per_page);
            }

            this.rows = rows;

        } else {
            if (this.config.repository) {
                const params: any = {};
                params.page = this.current_page;
                params.per_page = this.per_page;
                if (this.config.multitab && this.selectedTabValue) {
                    params.filters = this.parseQuery(this.selectedTabValue);
                }

                this.repositorySubscription = this.config.repository.all(params)
                    .pipe(
                        tap(
                            (listItem: any) => this.rows = listItem
                        ),
                        debounceTime(100),
                    )
                    .subscribe();
            } else {
                this.rows = [];
            }
        }
    }

    parseQuery(status) {
        return JSON.stringify({
            'match': 'and',
            'rules': [
                {
                    'match': 'or',
                    'rules': [
                        {
                            'field': 'status',
                            'operator': '=',
                            'value': status
                        }
                    ]

                }
            ]
        });
    }

    isActivateRowAction(action: string, row?: any): boolean {
        let isActivate: boolean;

        isActivate = (this.config.actions[action] && typeof this.config.actions[action] === 'function');

        // activate/ deactivate
        if (action === 'onDeactivate') {
            isActivate = isActivate && (typeof row.is_activated !== 'undefined') && row.is_activated;
        }
        if (action === 'onActivate') {
            isActivate = isActivate && (typeof row.is_activated !== 'undefined') && !row.is_activated;
        }


        // action on status:
        // - confirm
        // - cancel
        if (action === 'onConfirm') {
            isActivate = isActivate && (typeof row.status !== 'undefined' && row.status === OrderStatus.Pending);
        }
        if (action === 'onCancel') {
            const statusOnCancel = [
                OrderStatus.Pending,
                OrderStatus.Confirmed,
                OrderStatus.Ordered,
                OrderStatus.FoodReady,
                // OrderStatus.Delivering,
                // OrderStatus.Arrived,
                OrderStatus.Delivered,
            ];
            isActivate = isActivate && (typeof row.status !== 'undefined' && statusOnCancel.includes(row.status));
        }


        // cannot delete/activate/deactivate myself
        if (row instanceof User &&
            (action === 'onDelete' || action === 'onActivate' || action === 'onDeactivate')) {
            isActivate = isActivate && (row.id !== this.currentUser.id);
        }

        return isActivate;
    }

    // actions
    handleRowAction(action: string, row: any) {
        if (this.config.actions
            && this.config.actions[action]
            && typeof this.config.actions[action] === 'function') {

            // call action Fn
            this.config.actions[action](row);
        }
    }

    onEventRowRemoved(row: any) {
        this.rows = this.rows.filter(r => r.id !== row.id);
    }

    onEventRowUpdated(row: any) {
        const updateRow = this.rows.find(r => r.id === row.id);
        if (updateRow) {
            updateRow.fill(row);
        }
    }

    // image
    errorHandler(event) {
        event.target.src = 'assets/img/image-default.jpg';
    }

    // value
    getValue(row: any, columnName: string) {
        if (!row) {
            return null;
        }

        if (columnName.includes('.')) {
            const arrayNames = columnName.split('.');

            let val = row;
            for (const name of arrayNames) {
                val = this.getValue(val, name);
            }
            return val;
        } else {
            return row[columnName];
        }
    }

    getImage(row: any, columnName: string) {
        let imageSrc: any | any[];
        if (!row) {
            return null;
        }

        if (columnName.includes('.')) {
            const arrayNames = columnName.split('.');

            imageSrc = row;
            for (const name of arrayNames) {
                imageSrc = this.getImage(imageSrc, name);
            }
        } else {
            imageSrc = row[columnName];
        }

        // imageSrc is Array of Image
        if (imageSrc instanceof Array &&
            imageSrc.length > 0) {
            imageSrc = imageSrc[0];
        }

        return imageSrc;
    }

    getDefaultMaxWidth() {
        // const screenWidth = 1920;
        // const scrollWidth = 17;
        // const sidebar = 210;
        // const padding = 30;
        //
        // const px = (screenWidth - scrollWidth - sidebar - (padding * 2)) / (this.columns.length - 1);
        //
        // // convert to rem
        // const rem = px / 14;
        //
        // return rem;

        return 0;
    }


    /////////////////////////// TABS

    getTabs() {
        if (!this.config.multitab) {
            return;
        }

        if (!this.config.tabs || this.config.tabs.length === 0) {
            return;
        }

        if (this.config.tabs[0].title) {
            this.selectedTab = this.config.tabs[0].title;
        }

        this.filterAttributesByTabs();
    }

    isActiveTab(tab): boolean {
        return this.selectedTab === tab.title;
    }

    selectTab(tab) {
        this.selectedTab = tab.title;
        this.selectedTabValue = tab.value;
        this.actionTab.emit(tab.value);
        this.transformedRows();
        this.filterAttributesByTabs();
    }

    // action column
    onActionColumn(row: any) {
        this.actionColumn.emit(row);
    }
}
