import {BaseEntity} from '../../entities/base.entity';
import {ColumnFormat} from '../../types/column-format';
import {ColumnType} from '../../types/column-type';
import {ColumnFilterType} from '../../types/column-filter-type';

export class BaseColumn extends BaseEntity {
    name: string;
    title: string;
    avatar?: string;
    type?: ColumnType;
    className?: string;

    filterType?: ColumnFilterType;
    filterData?: any;

    format?: ColumnFormat;
    fieldOption?: string;

    action?: {
        label: string,
        callback: (item: any) => any
    };

    // onAction
    onSwitch?: (item: any) => void;

    constructor() {
        super();
        this.name = null;
        this.title = null;
        this.type = ColumnType.Text;
        this.filterType = ColumnFilterType.Text;
        this.filterData = null;
        this.format = ColumnFormat.None;
        this.avatar = null;
        this.className = null;
        this.fieldOption = null;
        this.action = null;

        this.onSwitch = null;
    }

    static create(data) {
        return (new BaseColumn()).fill(data);
    }

    static from(data) {
        return data.map(col => this.create(col));
    }
}
