import {BaseEntity} from '../../entities/base.entity';
import {Observable} from 'rxjs';
import {Injector} from '@angular/core';
import {BaseRepository} from '../../repositories/base.repository';

export class BaseTableConfig extends BaseEntity {
    paging?: boolean;
    sorting?: boolean;
    filtering?: boolean;
    selecting?: boolean;
    multitab?: boolean;

    // pagingServer
    pagingServer: boolean;
    repository: BaseRepository;

    className: string;

    actions?: {
        onEdit?: (row: any) => Observable<any> | Promise<any> | any,
        onActivate?: (row: any) => Observable<any> | Promise<any> | any,
        onDeactivate?: (row: any) => Observable<any> | Promise<any> | any,
        onDelete?: (row: any) => Observable<any> | Promise<any> | any,

        onConfirm?: (row: any) => Observable<any> | Promise<any> | any,
        onCancel?: (row: any) => Observable<any> | Promise<any> | any,
    };
    tabs?: { title: string, name?: string, value?: any }[];

    constructor() {
        super();
        this.paging = false;
        this.sorting = false;
        this.filtering = false;
        this.selecting = false;
        this.multitab = false;

        // pagingServer
        this.pagingServer = false;
        this.repository = null;

        this.className = null;

        this.actions = null;
        this.tabs = null;
    }

    static create(data) {
        return (new BaseTableConfig()).fill(data);
    }

    static from(data) {
        return this.create(data);
    }
}
