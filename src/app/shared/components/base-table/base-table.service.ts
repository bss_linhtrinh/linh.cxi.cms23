import {Injectable} from '@angular/core';
import {LocalStorageService} from 'ngx-webstorage';
import {Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class BaseTableService {

    private createdRowSource = new Subject<any>();
    private updatedRowSource = new Subject<any>();
    private removedRowSource = new Subject<any>();
    createdRow$ = this.createdRowSource.asObservable();
    updatedRow$ = this.updatedRowSource.asObservable();
    removedRow$ = this.removedRowSource.asObservable();

    constructor() {
    }

    fireEventRowCreated(item: any) {
        this.createdRowSource.next(item);
    }

    fireEventRowUpdated(item: any) {
        this.updatedRowSource.next(item);
    }

    fireEventRowRemoved(id: number) {
        this.removedRowSource.next(id);
    }
}
