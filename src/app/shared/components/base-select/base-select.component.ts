import {Component, EventEmitter, Inject, Input, OnChanges, OnInit, Optional, Output, SimpleChanges, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';
import {timer} from 'rxjs';
import {UtilitiesService} from '../../services/utilities/utilities.service';
import {tap} from 'rxjs/operators';
import {Image} from '../../entities/image';
import {LocalStorage} from 'ngx-webstorage';

@Component({
    selector: 'base-select',
    templateUrl: './base-select.component.html',
    styleUrls: ['./base-select.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: BaseSelectComponent, multi: true}
    ]
})
export class BaseSelectComponent extends ElementBase<any> implements OnInit, OnChanges {
    @Input() name: string;
    @Input() label: string;
    @Input() items: any;
    @Input() bindLabel = null;
    @Input() bindValue = null;
    @Input() bindAvatar = null;
    @Input() operator = false;
    @Input() clearable = false;
    @Input() multiple = false;
    @Input() closeOnSelect = true;
    @Input() searchable = false;
    @Input() width = '';
    @Input() custom = false;
    @Input() public type: string;
    @Input() public placeholder = '';
    // style
    @Input() display = 'block';

    private _required: any;

    @Input()
    get required(): any {
        return this._required;
    }

    set required(value: any) {
        this._required = typeof value !== 'undefined';
    }

    private _disabled: any;

    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(disabled: boolean) {
        this._disabled = disabled;
    }

    @Output() change = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    internalItems: any[];
    @LocalStorage('settings.locale') locale: string;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private utils: UtilitiesService
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.items) {
            if (changes.items.currentValue !== changes.items.previousValue) {
                timer(100)
                    .pipe(
                        tap(() => this.internalItems = this.items)
                    )
                    .subscribe();
            }
        }
    }

    onChange() {
        this.change.emit(this.value);
    }


    getValueItem(item: any): any {
        let bindValueItem: any;

        if (this.bindValue) {
            if (typeof item[this.bindValue] !== 'undefined') {
                bindValueItem = item[this.bindValue];
            }
        } else {
            bindValueItem = item;
        }

        return bindValueItem;
    }

    getLabelItem(item: any): string {
        let bindLabelItem: any;

        if (this.bindLabel) {
            // if (typeof item['translators'] !== 'undefined') {
            //     console.log(item['translators']);
            // }
            if (typeof item[this.bindLabel] !== 'undefined') {
                bindLabelItem = item[this.bindLabel];
            }
        } else {
            bindLabelItem = item;
        }

        return bindLabelItem;
    }

    getAvatarItem(item: any): Image {
        let avatarItem: Image;

        if (this.bindAvatar) {
            if (typeof item[this.bindAvatar] !== 'undefined') {
                avatarItem = item[this.bindAvatar];
            }
        } else {
            avatarItem = null;
        }

        return avatarItem;
    }

    isDisabledItem(item: any): boolean {
        return (typeof item['disabled'] !== 'undefined') && this.utils.boolval(item['disabled']);
    }
}
