import {Component, EventEmitter, Inject, Input, OnChanges, OnInit, Optional, Output, SimpleChanges, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';
import {UtilitiesService} from '../../services/utilities/utilities.service';

@Component({
    selector: 'base-radio',
    templateUrl: './base-radio.component.html',
    styleUrls: ['./base-radio.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: BaseRadioComponent, multi: true}
    ]
})
export class BaseRadioComponent extends ElementBase<any> implements OnInit, OnChanges {

    @Input() public name: string;
    @Input() public label: string;
    @Input() public type: string;
    @Input() public placeholder: string;
    @Input() public values: any[];
    @Input() cols: number;

    @Output() change = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    internalData: any[] = [];
    radioStyle = {};

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private utilitiesService: UtilitiesService
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
        this.initInternalData();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes &&
            changes.cols &&
            changes.cols.previousValue !== changes.cols.currentValue
        ) {
            this.updateStyle();
        }
    }

    initInternalData() {
        if (this.values) {
            this.values.forEach(item => {
                if (typeof item === 'string') {
                    this.internalData.push({name: item, value: item});
                } else if (typeof item === 'object'
                    && typeof item.value !== 'undefined'
                    && typeof item.name !== 'undefined') {
                    this.internalData.push({name: item.name, value: item.value});
                }
            });
        }
    }

    updateStyle() {
        if (this.cols && this.cols > 1) {
            this.radioStyle = {
                width: (100 / this.cols) + '%',
                display: 'inline-flex'
            };
        }
    }

    onChange() {
        this.change.emit(this.value);
    }

    getLabel(label: string) {
        return this.utilitiesService.makeTitle(label);
    }


}
