import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiIncreaseDecreaseComponent } from './cxi-increase-decrease.component';

describe('CxiIncreaseDecreaseComponent', () => {
  let component: CxiIncreaseDecreaseComponent;
  let fixture: ComponentFixture<CxiIncreaseDecreaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiIncreaseDecreaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiIncreaseDecreaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
