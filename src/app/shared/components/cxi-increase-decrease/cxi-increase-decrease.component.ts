import {AfterViewChecked, Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';
import {timer} from 'rxjs';
import {AuthService} from '../../services/auth/auth.service';
import {LocalStorage} from 'ngx-webstorage';
import {BrandScope} from '../../types/brand-scope';

@Component({
    selector: 'cxi-increase-decrease',
    templateUrl: './cxi-increase-decrease.component.html',
    styleUrls: ['./cxi-increase-decrease.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: CxiIncreaseDecreaseComponent, multi: true}
    ]
})
export class CxiIncreaseDecreaseComponent extends ElementBase<number> implements OnInit, AfterViewChecked {

    @LocalStorage('auth.brandScope') brandScope: BrandScope;

    @Input() public name: string;
    @Input() public label = '';

    @Input() public min = 0;
    @Input() public max = 99;
    @Input() showValue = true;
    filter: any;

    private _disabled: any;

    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(disabled: boolean) {
        this._disabled = disabled;
    }

    @Output() change = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private authService: AuthService,
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
        this.filter = this.authService.getCurrentFilter();
    }

    ngAfterViewChecked(): void {
        switch (this.brandScope) {
            case BrandScope.FB:
            default:
                if (this.value) {
                    timer(500).subscribe(() => this.handleData());
                }
                break;
            case BrandScope.Booking:
                if (!this.value) {
                    this.value = 1;
                }
                break;
        }
    }

    inc() {
        switch (this.brandScope) {
            case BrandScope.FB:
            default:
                if (!this.value) {
                    this.value = this.min;
                }

                this.parse();

                if (this.value < this.max) {
                    this.value += 1;
                    this.change.emit(this.value);
                } else {
                    this.value = this.max;
                }
                break;
            case BrandScope.Booking:
                if (!this.disabled) {
                    if (this.value < this.max) {
                        this.value += 1;
                        this.change.emit('inc');
                    } else {
                        this.value = this.max;
                    }
                }
                break;
        }
    }

    dec() {

        switch (this.brandScope) {
            case BrandScope.FB:
            default:
                if (!this.value) {
                    this.value = this.max;
                }
                this.parse();

                if (this.value > this.min) {
                    this.value -= 1;
                    this.change.emit(this.value);
                } else {
                    this.value = this.min;
                }
                break;
            case BrandScope.Booking:
                if (!this.disabled) {
                    if (this.value > this.min) {
                        this.value -= 1;
                        this.change.emit('dec');
                    } else {
                        this.value = this.min;
                    }
                }
                break;
        }
    }

    parse() {
        if (typeof this.value === 'string') {
            const parsedValue = parseInt(this.value, 10);
            if (isNaN(parsedValue)) {
                return this.min;
            }
            this.value = parsedValue;
        }
    }

    handleData() {
        this.parse();

        if (this.value < this.min) {
            this.value = this.min;
        } else if (this.value > this.max) {
            this.value = this.max;
        }
    }
}
