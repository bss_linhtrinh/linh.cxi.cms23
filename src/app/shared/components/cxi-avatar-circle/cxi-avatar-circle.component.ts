import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Image} from '../../entities/image';
import {timer} from 'rxjs';
import {tap} from 'rxjs/operators';
import {REM} from '../../constants/app';

@Component({
    selector: 'cxi-avatar-circle',
    templateUrl: './cxi-avatar-circle.component.html',
    styleUrls: ['./cxi-avatar-circle.component.scss']
})
export class CxiAvatarCircleComponent implements OnInit, OnChanges, AfterViewInit {
    private _src: string | ArrayBuffer | Image;

    @Input('src')

    get src(): string | ArrayBuffer | Image | Image[] {
        return this._src;
    }

    set src(value: string | ArrayBuffer | Image | Image[]) {
        if (typeof value === 'string') {
            this._src = value;
        } else if (value instanceof ArrayBuffer) {
            this._src = value;
        } else if (value instanceof Image) {
            this._src = value.original_image;
        } else if (value instanceof Array) {
            if (value.length > 0) {
                this._src = value[0].original_image;
            }
        } else {
            this._src = null;
        }
    }

    @Input() name: string;
    @Input() size: number;

    @Input() fullName: string;
    @Input() firstName: string;
    @Input() lastName: string;

    remHeight: number;
    remWidth: number;
    DEFAULT_SIZE = 48;

    // style
    @Input() height: any;
    @Input() width: any;
    @Input() borderColor: string;

    constructor() {
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (
            (changes.height && changes.height.previousValue !== changes.height.currentValue) ||
            (changes.width && changes.width.previousValue !== changes.width.currentValue)
        ) {
            this.convertRem();
        }
    }

    ngAfterViewInit(): void {
        timer(1)
            .pipe(
                tap(
                    () => this.convertRem()
                )
            )
            .subscribe();
    }

    convertRem() {
        if (!this.height) {
            this.height = this.DEFAULT_SIZE;
        }
        if (!this.width) {
            this.width = this.DEFAULT_SIZE;
        }

        // px to rem
        this.remHeight = (this.height / REM);
        this.remWidth = (this.width / REM);
    }

    // image
    errorHandler(event) {
        event.target.src = 'assets/img/image-default.jpg';
    }

    // style
    getStyleImgAvatar() {
        const style: any = {};

        // width (default 48px)
        if (this.remWidth) {
            style.width = `${this.remWidth}rem`;
        }

        // height (default 48px)
        if (this.remHeight) {
            style.height = `${this.remHeight}rem`;
        }

        // border
        if (this.borderColor) {
            const borderWidth = 3;
            style.border = `${borderWidth / REM}rem solid ${this.borderColor}`;

            if (borderWidth && borderWidth > 0) {
                const lineHeight = (this.height - borderWidth * 2) / REM;
                style['line-height'] = `${lineHeight}rem`;
            }
        }

        return style;
    }
}
