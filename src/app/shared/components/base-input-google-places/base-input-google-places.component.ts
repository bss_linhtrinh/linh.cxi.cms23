import {AfterViewInit, Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {GooglePlaceDirective} from 'ngx-google-places-autocomplete';
import {Address} from 'ngx-google-places-autocomplete/objects/address';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';
import * as _ from 'lodash';
import {SettingsRepository} from '../../repositories/settings.repository';

@Component({
    selector: 'base-input-google-places',
    templateUrl: './base-input-google-places.component.html',
    styleUrls: ['./base-input-google-places.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: BaseInputGooglePlacesComponent, multi: true}
    ]
})
export class BaseInputGooglePlacesComponent extends ElementBase<number> implements OnInit {

    @Input() name: string;

    private _label: string;

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        if (value) {
            this._label = value.replace('_', ' ');
        } else {
            this._label = '';
        }
    }

    @Input() public size = 'sm';
    @Input() public width = '';
    @Input() public type = 'text';
    @Input() public placeholder: string;

    private _required: any;

    @Input()
    get required(): any {
        return this._required;
    }

    set required(value: any) {
        this._required = typeof value !== 'undefined';
    }

    private _disabled: any;
    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(disabled: boolean) {
        this._disabled = disabled;
    }

    @ViewChild(NgModel, {static: true}) model: NgModel;

    // ngx-google-places-autocomplete
    options = {
        types: [],
        componentRestrictions: {country: []}
    };

    @ViewChild('placesRef', {static: true}) placesRef: GooglePlaceDirective;

    @Output() addressChange = new EventEmitter();

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private settingsRepository: SettingsRepository
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
        this.getGooglePlaceSetting();
    }

    getGooglePlaceSetting() {
        const key = 'GOOGLE_PLACE_COMPONENT_RESTRICTIONS_COUNTRY';
        const countries = this.settingsRepository.findValueByKey(key);
        if (countries) {
            this.options.componentRestrictions.country = countries
                ? (
                    Array.isArray(countries)
                        ? countries
                        : (
                            countries.includes(',')
                                ? countries.split(',')
                                : []
                        )
                )
                : ['vn'];
        }
    }

    public handleAddressChange(address: Address) {
        const data = {
            place_id: address.place_id,
            formatted_address: address.formatted_address,
            country: this.getDetail(address.address_components, ['country']),
            state: this.getDetail(address.address_components, ['administrative_area_level_1']),
            city: this.getDetail(address.address_components, ['administrative_area_level_1']),
            district: this.getDetail(address.address_components, ['administrative_area_level_2']),
            ward: this.getDetail(address.address_components, ['administrative_area_level_3']),
            street: this.getDetail(address.address_components, ['route']),
            postcode: this.getDetail(address.address_components, ['postal_code']),
            latitude: address.geometry.location.lat(),
            longitude: address.geometry.location.lng(),
        };

        // extract info
        const extractData = this.extractDataFromAddressInfo(address.formatted_address);
        if (!data.ward && extractData.ward) {
            data.ward = extractData.ward;
        }


        // get postcode
        // const url = `https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyATNMhMj6ANj6ElT8EKKFAwVNmbM7zLxs4&placeid=ChIJfZ8sFzovdTERWE0ocl-7O9E`;
        // this.httpClient.get(url)
        //     .subscribe(
        //         (res) => console.log(res)
        //     );


        this.addressChange.emit(data);
    }

    getDetail(addressList: Array<any>, type: Array<string>) {
        const result = addressList.filter(
            (address) => this.getType(address, type).length > 0
        );
        return result[0] ? result[0].long_name : '';
    }

    getType(address: any, _type: Array<string>) {
        return address.types.filter(
            (type: any) => _.indexOf(_type, type) >= 0
        );
    }

    extractDataFromAddressInfo(formatted_address) {
        if (!formatted_address) {
            return null;
        }

        const addressArr = formatted_address.split(',');

        const data = {
            city: null,
            district: null,
            ward: null,
            street: null
        };

        let indexStreet: number;
        let indexWard: number;
        let indexDistrict: number;

        for (const i in addressArr) {
            let addressItem = addressArr[i];

            addressItem = addressItem.trim();

            if (parseInt(i, 10) === 0 ||
                (addressItem.includes('Đường') || addressItem.includes('Street'))
            ) {
                data.street = addressItem.trim();
                indexStreet = parseInt(i, 10);
            }

            if (parseInt(i, 10) === indexStreet + 1 ||
                (addressItem.includes('Phường') || addressItem.includes('phường'))
            ) {
                data.ward = addressItem.replace('Phường', '').replace('phường', '').trim();
                indexWard = parseInt(i, 10);
            }

            if (parseInt(i, 10) === indexWard + 1 ||
                addressItem.includes('Quận') || addressItem.includes('quận')) {
                data.district = addressItem.replace('Quận', '').replace('quận', '').trim();
                indexDistrict = parseInt(i, 10);
            }

            if (parseInt(i, 10) === indexDistrict + 1 &&
                (!addressItem.includes('Vietnam') && !addressItem.includes('vietnam'))
            ) {
                data.city = addressItem.trim();
            }
        }

        return data;
    }
}
