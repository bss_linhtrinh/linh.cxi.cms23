import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseInputGooglePlacesComponent } from './base-input-google-places.component';

describe('BaseInputGooglePlacesComponent', () => {
  let component: BaseInputGooglePlacesComponent;
  let fixture: ComponentFixture<BaseInputGooglePlacesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseInputGooglePlacesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseInputGooglePlacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
