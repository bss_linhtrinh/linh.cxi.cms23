import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseIntegerComponent } from './base-integer.component';

describe('BaseIntegerComponent', () => {
  let component: BaseIntegerComponent;
  let fixture: ComponentFixture<BaseIntegerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseIntegerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseIntegerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
