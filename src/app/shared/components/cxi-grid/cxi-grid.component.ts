import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {BaseFilterCriteria} from '../../entities/base-filter-criteria';
import {Subscription} from 'rxjs';
import {BaseFilterPipe} from '../../pipes/base-filter/base-filter.pipe';
import {BasePagingPipe} from '../../pipes/base-paging/base-paging.pipe';
import {BaseSortPipe} from '../../pipes/base-sort/base-sort.pipe';
import {environment} from '../../../../environments/environment';
import {User} from '../../entities/user';
import {AuthService} from '../../services/auth/auth.service';
import {ColumnFormat} from '../../types/column-format';
import {ColumnFilterType} from '../../types/column-filter-type';
import {OrderStatus} from '../../types/order-status';
import {LocalStorage} from 'ngx-webstorage';
import {debounceTime, finalize, tap} from 'rxjs/operators';
import {CxiGridColumn} from './cxi-grid-column';
import {CxiGridConfig} from './cxi-grid-config';
import {CxiGridService} from './cxi-grid.service';
import {BrandScope} from '../../types/brand-scope';
import {StorageService} from '../../services/storage/storage.service';

enum SelectedRowStatus {
    All = 2,
    Something = 1,
    None = 0
}

@Component({
    selector: 'app-cxi-grid',
    templateUrl: './cxi-grid.component.html',
    styleUrls: ['./cxi-grid.component.scss']
})
export class CxiGridComponent implements OnInit, OnChanges, OnDestroy {

    public dataHobbySearch: Array<any> = [];
    @Input() name: string;
    @Input() dataHobby: any[] = [];
    @Input() showHobby: boolean;
    @Input() config?: CxiGridConfig;
    @Input() rows?: Array<any> = [];
    @Input() columns: CxiGridColumn[];
    @Input() filterAttrs?: BaseFilterCriteria[] = [];
    @Input() search = '';
    @Output() actionColumn = new EventEmitter<any>();
    @Output() actionTab = new EventEmitter<any>();
    @Output() mesFilterCustomer = new EventEmitter<any>();
    @Output() clearFilterCustomer = new EventEmitter<any>();
    @Output() checkedItem = new EventEmitter<any>();
    @Output() checkedAllItem = new EventEmitter<any>();

    selectedAll: boolean;
    private selectedStatus: SelectedRowStatus;

    orderByName: string;
    orderReverse: boolean;

    filter: any;

    isLoading = 0;

    private _currentPage: number;
    @Input()
    get currentPage(): number {
        return this._currentPage;
    }

    @Output() currentPageChange = new EventEmitter<number>();

    set currentPage(currentPage: number) {
        if (currentPage !== this._currentPage) {
            this._currentPage = currentPage;
            this.currentPageChange.emit(this.currentPage);
        }
    }

    private _perPage: number;
    @Input()
    get perPage(): number {
        return this._perPage;
    }

    @Output() perPageChange = new EventEmitter<number>();

    set perPage(perPage: number) {
        if (perPage !== this._perPage) {
            this._perPage = perPage;
            this.perPageChange.emit(this.perPage);
        }
    }

    // @LocalStorage('pagination.from') fromPage: number;
    // @LocalStorage('pagination.to') toPage: number;
    @LocalStorage('pagination.total') total: number;

    @LocalStorage('auth.brandScope') brandScope: BrandScope;
    @LocalStorage('scope.outlet_id') outletId: number;

    // get from(): number {
    //     return this.fromPage;
    // }

    // get to(): number {
    //     return this.toPage;
    // }

    subscriptions: Subscription[] = [];
    repositorySubscription: Subscription;
    baseUrl: string = environment.baseUrl;

    currentUser: any;

    // tabs
    selectedTab: string;
    selectedTabValue = 'pending';

    constructor(
        private baseSortPipe: BaseSortPipe,
        private baseFilterPipe: BaseFilterPipe,
        private basePagingPipe: BasePagingPipe,
        private cxiGridService: CxiGridService,
        private authService: AuthService,
        private storageService: StorageService
    ) {
        const createdSubscriber = cxiGridService.createdRow$
            .subscribe(
                (row: any) => this.onEventRowCreated(row)
            );
        const removeSubscriber = cxiGridService.removedRow$
            .subscribe(
                (row: any) => this.onEventRowRemoved(row)
            );
        const updateSubscriber = cxiGridService.updatedRow$
            .subscribe(
                (row: any) => this.onEventRowUpdated(row)
            );
        const changeSubscriber = cxiGridService.changedRow$
            .subscribe(
                (row: any) => this.onEventRowChanged(row)
            );
        const activatedSubscriber = cxiGridService.activatedRow$
            .subscribe(
                (row: any) => this.onEventRowActivated(row)
            );
        const deactivatedSubscriber = cxiGridService.deactivatedRow$
            .subscribe(
                (row: any) => this.onEventRowDeactivated(row)
            );

        this.subscriptions.push(createdSubscriber);
        this.subscriptions.push(removeSubscriber);
        this.subscriptions.push(updateSubscriber);
        this.subscriptions.push(changeSubscriber);
        this.subscriptions.push(activatedSubscriber);
        this.subscriptions.push(deactivatedSubscriber);
    }

    ngOnInit() {
        this.currentUser = this.authService.currentUser();
        // this.filter = this.authService.getCurrentFilter();
        this.getTabs();
        this.initPagination();
        this.transformedRows();
        // this.listenPaginationChange();
    }

    ngOnDestroy() {
        this.subscriptions.forEach(s => s.unsubscribe());
        if (this.repositorySubscription) {
            this.repositorySubscription.unsubscribe();
        }

        this.storageService.clear('settings.locale');
    }

    initPagination() {
        if (!this.currentPage) {
            this.currentPage = 1;
        }
        if (!this.perPage) {
            this.perPage = 20;
        }
    }

    onChangePagination(data: any) {
        console.log('--on change pagination', data);
    }

    onChangeCurrentPage(currentPage: number) {
        if (!this.isLoading) {
            this.transformedRows();
        }
    }

    onChangePerPage(perPage: number) {
        if (!this.isLoading) {
            this.transformedRows();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.columns) {
            this.filterAttrs = this.columns.map((column) => {
                return BaseFilterCriteria.create({
                    name: column.name,
                    title: column.title,
                    filterType: column.filterType ? column.filterType : ColumnFilterType.Text,
                    filterData: column.filterData ? column.filterData : null,
                    format: column.format ? column.format : ColumnFormat.None,
                });
            });

            this.filterAttributesByTabs();
        }
        if (changes.search) {
            const searchCriteria = BaseFilterCriteria.create({
                name: '*',
                title: '*',
                filterType: ColumnFilterType.Text,
                format: ColumnFormat.None,
                value: changes.search.currentValue
            });

            if (this.filterAttrs) {
                const indexSearchCriteria = this.filterAttrs.findIndex(attr => attr.name === searchCriteria.name);
                if (indexSearchCriteria === -1) {
                    this.filterAttrs.push(searchCriteria);
                } else {
                    this.filterAttrs[indexSearchCriteria] = searchCriteria;
                }
            }
        }
        if (changes.showHobby) {
            this.dataHobbySearch = this.dataHobby;
            this.transformedRows();
        }
        // update pagination
        // this.total = this.calcRowsLength(false, true, false);
    }

    filterAttributesByTabs() {
        // filter by tabs
        if (this.config.multitab && this.config.tabs && this.config.tabs.length > 0) {
            const tab = this.config.tabs.find(t => t.title === this.selectedTab);
            if (tab) {
                const criteriaByTabs = BaseFilterCriteria.create({
                    name: tab.name,
                    value: tab.value,
                });

                const findIndex = this.filterAttrs.findIndex(attr => attr.name === tab.name);
                if (findIndex !== -1) {
                    this.filterAttrs.splice(findIndex, 1);
                }

                this.filterAttrs.push(criteriaByTabs);
            }
        }
    }

    selectAll() {
        if (this.selectedAll) {
            this.selectedStatus = SelectedRowStatus.All;
        } else {
            this.selectedStatus = SelectedRowStatus.None;
        }

        // sort, filter, paging
        this.rows.forEach((r: any) => {
            r.selected = !!this.selectedStatus;
        });
        this.checkedAllItem.emit({check_all_item: this.selectedAll, data_item: this.rows});
    }

    /**
     * Select row
     */
    selectRow(item?: any) {
        this.checkedItem.emit(item);

        let selectStatus = SelectedRowStatus.None;

        const rows = this.rows;

        // + 1
        const selectedItem = rows.find((r: any) => {
            return r.selected;
        });
        if (selectedItem) {
            selectStatus = SelectedRowStatus.Something;

            // + 1
            const unselectedItem = rows.find((r: any) => {
                return !r.selected;
            });
            if (!unselectedItem) {
                selectStatus = SelectedRowStatus.All;
            }
        }

        this.selectedStatus = selectStatus;
    }

    // select
    isSelectedAll() {
        return this.selectedStatus === SelectedRowStatus.All;
    }

    isSelectedSomething() {
        return this.selectedStatus === SelectedRowStatus.Something;
    }

    isSelectedNone() {
        return this.selectedStatus === SelectedRowStatus.None;
    }

    // onSort change
    orderBy(columnName: string) {
        if (!this.config.sorting) {
            return;
        }

        const column = this.columns.find(c => c.name === columnName);
        if (!column.sortable) {
            return;
        }

        if (this.orderByName === columnName) {
            this.orderReverse = !this.orderReverse;
        } else {
            this.orderByName = columnName;
            this.orderReverse = false;
        }

        this.resetSelectionItems();

        this.transformedRows();
    }

    // onPaging change
    onPageChange() {
        // this.resetSelectionItems();
    }

    // onFilterData changethis.resetSelectionItems();
    onChangeFilterData() {
        this.config.withoutsaleman = false;
        //this.dataHobbySearch = [];
        // transform rows
        this.transformedRows();
        this.resetSelectionItems();
        //this.clearFilterCustomer.emit(this.dataHobby);
    }

    resetSelectionItems() {
        this.selectedStatus = SelectedRowStatus.None;
        this.rows.forEach(r => r.selected = false);
    }

    // Rows transformed
    transformedRows(sorting?: boolean, filtering?: boolean, paging?: boolean) {
        if (!this.config.pagingServer) {

            let rows = this.rows;

            const isSorting = typeof sorting !== 'undefined' ? sorting : this.config.sorting;
            const isFiltering = typeof filtering !== 'undefined' ? filtering : this.config.filtering;
            const isPaging = typeof paging !== 'undefined' ? paging : this.config.paging;

            // sorting
            if (isSorting) {
                rows = this.baseSortPipe.transform(rows, this.orderByName, this.orderReverse);
            }

            // filtering
            if (isFiltering) {
                rows = this.baseFilterPipe.transform(rows, this.filterAttrs);
            }

            // paging
            if (isPaging) {
                rows = this.basePagingPipe.transform(rows, this.currentPage, this.perPage);
            }

            this.rows = rows;

        } else {
            if (this.config.repository) {
                const params: any = {};
                params.page = this.currentPage;
                params.per_page = this.perPage;
                if (this.orderByName) {
                    params.order_by = this.orderByName;
                    params.order = this.orderReverse ? 'desc' : 'asc';
                } else {
                    params.order_by = 'created_at';
                    params.order = 'desc';
                }
                if (this.dataHobbySearch && this.dataHobbySearch.length > 0) {
                    params.hobbies = JSON.stringify(this.dataHobbySearch);
                }
                if (this.config.backlist) {
                    params.blacklist = 1;
                }
                if (this.config.type) {
                    params.type = this.config.type;
                }
                if (this.config.withoutsaleman) {
                    this.buildQuery();
                }
                params.filters = this.buildQuery();
                params.grid_name = this.name;

                this.isLoading++;
                this.repositorySubscription = this.config.repository.all(params)
                    .pipe(
                        finalize(
                            () => {
                                this.isLoading--;
                            }
                        ),
                        tap(
                            (listItem: any) => this.rows = listItem
                        ),
                        debounceTime(100),
                    )
                    .subscribe();
            } else {
                this.rows = [];
            }
        }
    }

    buildQuery(): string {
        let queryStatus: any;
        let queryFilter: any;

        if (this.config.multitab && this.selectedTabValue) {
            // by tab
            queryStatus = {
                'match': 'or',
                'rules': [
                    {
                        'field': 'status',
                        'operator': '=',
                        'value': this.selectedTabValue
                    }
                ]
            };
        }
        if (this.config.filterOutlet && this.outletId) {
            queryStatus = {
                'field': 'outletId',
                'operator': 'LIKE',
                'value': `%${this.outletId}%`
            };
        }
        // filterAttributes
        if (this.filterAttrs && this.filterAttrs.length > 0) {
            queryFilter = {
                match: 'and',
                rules: []
            };
            for (const filterAttr of this.filterAttrs) {
                if (filterAttr.selected &&
                    filterAttr.name &&
                    filterAttr.value) {
                    let rule: any;
                    switch (filterAttr.filterType) {
                        case ColumnFilterType.Select:
                        case ColumnFilterType.MultiSelect:
                            rule = {field: filterAttr.name, operator: '=', value: filterAttr.value};
                            break;
                        default:
                            switch (filterAttr.format) {
                                case 'none':
                                default:
                                    rule = {field: filterAttr.name, operator: 'LIKE', value: `%${filterAttr.value}%`};
                                    break;
                                case 'date':
                                    const startDate = `${filterAttr.value} 00:00:00`;
                                    const endDate = `${filterAttr.value} 23:59:59`;
                                    rule = [
                                        {field: filterAttr.name, operator: '>=', value: startDate},
                                        {field: filterAttr.name, operator: '<=', value: endDate}
                                    ];
                                    break;
                            }
                    }
                    if (rule instanceof Array) {
                        queryFilter.rules = queryFilter.rules.concat(rule);
                    } else {
                        queryFilter.rules.push(rule);
                    }

                }
            }
        }

        if (this.config.contentType) {
            if (!queryFilter) {
                queryFilter = {
                    'match': 'and',
                    'rules': [
                        {
                            'field': 'type',
                            'operator': '=',
                            'value': this.config.contentType
                        }
                    ]
                };
            }
            if (queryFilter) {
                const rule = {
                    'field': 'type',
                    'operator': '=',
                    'value': this.config.contentType
                };
                queryFilter.rules.push(rule);
            }
        }
        if (this.config.withoutsaleman) {
            // by tab
            queryFilter = {
                'match': 'or',
                'rules': [
                    {
                        'field': 'salesman_customer',
                        'operator': '=',
                        'value': null
                    }
                ]
            };
        }
        let query: any;

        if (queryStatus && queryFilter) {
            query = {match: 'and', rules: []};
            if (queryStatus) {
                query.rules.push(queryStatus);
            }
            if (queryFilter) {
                query.rules.push(queryFilter);
            }
        } else if (queryFilter) {
            query = queryFilter;
        } else if (queryStatus) {
            query = queryStatus;
        }

        return JSON.stringify(query);
    }

    // checkExistQueryAttribute(filterAttribute: BaseFilterCriteria): boolean {
    //     return (this.config.repository &&
    //         this.config.repository.model &&
    //         this.config.repository.model.hasOwnProperty(filterAttribute.name)
    //     );
    // }

    isActivateRowAction(action: string, row?: any): boolean {
        let isActivate: boolean;

        isActivate = (this.config.actions[action] && typeof this.config.actions[action] === 'function');

        // activate/ deactivate
        if (action === 'onDeactivate') {
            isActivate = isActivate && (typeof row.is_activated !== 'undefined') && row.is_activated;
        }
        if (action === 'onActivate') {
            isActivate = isActivate && (typeof row.is_activated !== 'undefined') && !row.is_activated;
        }

        // stand out
        if (action === 'onStandOut') {
            isActivate = isActivate && (typeof row.is_stand_out !== 'undefined');
        }


        // action on status:
        // - confirm
        // - cancel
        if (action === 'onConfirm') {
            isActivate = isActivate && (typeof row.status !== 'undefined' && row.status === OrderStatus.Pending);
        }
        if (action === 'onCancel') {
            const statusOnCancel = [
                OrderStatus.Pending,
                OrderStatus.Confirmed,
                OrderStatus.Ordered,
                OrderStatus.FoodReady,
                // OrderStatus.Delivering,
                // OrderStatus.Arrived,
                OrderStatus.Delivered,
            ];
            switch (this.brandScope) {
                case BrandScope.FB:
                default:
                    isActivate = isActivate && (typeof row.status !== 'undefined' && statusOnCancel.includes(row.status));
                    break;
                case BrandScope.Booking:
                    isActivate = isActivate && (typeof row.status !== 'undefined' && row.status === OrderStatus.Pending);
                    break;
            }
        }


        // cannot delete/activate/deactivate myself
        if (row instanceof User &&
            (action === 'onDelete' || action === 'onActivate' || action === 'onDeactivate')) {
            isActivate = isActivate && (row.id !== this.currentUser.id);
        }

        return isActivate;
    }

    // actions
    handleRowAction(action: string, row: any) {
        if (this.config.actions
            && this.config.actions[action]
            && typeof this.config.actions[action] === 'function') {

            // call action Fn
            this.config.actions[action](row);
        }
    }

    handleSwitchAction(row: any, column: CxiGridColumn) {
        if (column
            && column.onSwitch
            && typeof column.onSwitch === 'function') {

            // call action Fn
            column.onSwitch(row);
        }
    }

    onEventRowCreated(row: any) {
        this.transformedRows();
    }

    onEventRowRemoved(row: any) {
        this.rows = this.rows.filter(r => r.id !== row.id);
    }

    onEventRowUpdated(row: any) {
        const updateRow = this.rows.find(r => r.id === row.id);
        if (updateRow) {
            updateRow.fill(row);
        }
    }

    onEventRowChanged(row: any) {
        this.transformedRows();
    }

    onEventRowActivated(row: any) {
        this.transformedRows();
    }

    onEventRowDeactivated(row: any) {
        this.transformedRows();
    }


    // image
    errorHandler(event) {
        event.target.src = 'assets/img/image-default.jpg';
    }

    // value
    getValue(row: any, column: CxiGridColumn | string) {
        if (!row) {
            return null;
        }

        if (
            typeof column === 'string' ||
            (column instanceof CxiGridColumn && column.name)
        ) {
            const columnName = (column instanceof CxiGridColumn)
                ? column.name
                : column;

            if (columnName.includes('.')) {
                const arrayNames = columnName.split('.');
                let val = row;
                for (const name of arrayNames) {
                    val = this.getValue(val, name);
                }

                return val;
            } else {
                const columnTransformedName = (column instanceof CxiGridColumn)
                    ? (
                        column.transformedName
                            ? column.transformedName
                            : column.name
                    )
                    : column;

                return row[columnTransformedName];
            }
        }
    }

    getImage(row: any, columnName: string) {
        let imageSrc: any | any[];
        if (!row) {
            return null;
        }

        if (columnName.includes('.')) {
            const arrayNames = columnName.split('.');

            imageSrc = row;
            for (const name of arrayNames) {
                imageSrc = this.getImage(imageSrc, name);
            }
        } else {
            imageSrc = row[columnName];
        }

        // imageSrc is Array of Image
        if (imageSrc instanceof Array &&
            imageSrc.length > 0) {
            imageSrc = imageSrc[0];
        }

        return imageSrc;
    }

    getDefaultMaxWidth() {
        // const screenWidth = 1920;
        // const scrollWidth = 17;
        // const sidebar = 210;
        // const padding = 30;
        //
        // const px = (screenWidth - scrollWidth - sidebar - (padding * 2)) / (this.columns.length - 1);
        //
        // // convert to rem
        // const rem = px / 14;
        //
        // return rem;

        return 0;
    }

    /////////////////////////// TABS

    getTabs() {
        if (!this.config.multitab) {
            return;
        }

        if (!this.config.tabs || this.config.tabs.length === 0) {
            return;
        }

        if (this.config.tabs[0].title) {
            this.selectedTab = this.config.tabs[0].title;
        }

        this.filterAttributesByTabs();
    }

    isActiveTab(tab): boolean {
        return this.selectedTab === tab.title;
    }

    selectTab(tab) {
        this.selectedTab = tab.title;
        this.selectedTabValue = tab.value;
        this.actionTab.emit(tab.value);
        this.transformedRows();
        this.filterAttributesByTabs();
    }

    // action column
    onActionColumn(row: any) {
        this.actionColumn.emit(row);
    }

    onChangeLocale(locale: string) {
        this.transformedRows();
    }

    FilterCustomer(event?: any) {
        this.mesFilterCustomer.emit(event);
        if (event.name == 'without_salesman') {
            this.config.withoutsaleman = true;
            this.transformedRows();
        }
    }
}
