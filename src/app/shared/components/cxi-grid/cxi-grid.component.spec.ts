import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiGridComponent } from './cxi-grid.component';

describe('CxiGridComponent', () => {
  let component: CxiGridComponent;
  let fixture: ComponentFixture<CxiGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
