import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CxiGridService {

    private createdRowSource = new Subject<any>();
    private updatedRowSource = new Subject<any>();
    private removedRowSource = new Subject<any>();
    private changedRowSource = new Subject<any>();
    private activatedRowSource = new Subject<any>();
    private deactivatedRowSource = new Subject<any>();
    private gridTotalSource = new Subject<any>();
    createdRow$ = this.createdRowSource.asObservable();
    updatedRow$ = this.updatedRowSource.asObservable();
    removedRow$ = this.removedRowSource.asObservable();
    changedRow$ = this.changedRowSource.asObservable();
    activatedRow$ = this.activatedRowSource.asObservable();
    deactivatedRow$ = this.deactivatedRowSource.asObservable();

    gridTotal$ = this.gridTotalSource.asObservable();

    constructor() {
    }

    fireEventRowCreated(item: any) {
        this.createdRowSource.next(item);
    }

    fireEventRowUpdated(item: any) {
        this.updatedRowSource.next(item);
    }

    fireEventRowRemoved(item: any) {
        this.removedRowSource.next(item);
    }

    fireEventRowChanged(item: any) {
        this.changedRowSource.next(item);
    }

    fireEventRowActivated(item: any) {
        this.activatedRowSource.next(item);
    }

    fireEventRowDeactivated(item: any) {
        this.deactivatedRowSource.next(item);
    }

    sendMeta(grid_name: string, meta?: any) {
        const data = {
            grid_name: grid_name,
            current_page: meta.current_page ? meta.current_page : null,
            per_page: meta.current_page ? meta.current_page : null,
            total: meta.total ? meta.total : null
        };
        this.gridTotalSource.next(data);
    }
}
