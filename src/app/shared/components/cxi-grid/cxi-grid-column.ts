import {BaseEntity} from '../../entities/base.entity';
import {ColumnFormat} from '../../types/column-format';
import {ColumnType} from '../../types/column-type';
import {ColumnFilterType} from '../../types/column-filter-type';

export class CxiGridColumn extends BaseEntity {
    name: string;
    secondary_name?: string;
    transformedName?: string;

    title: string;
    avatar?: string;
    type?: ColumnType;
    className?: string;

    sortable: boolean;
    filterType?: ColumnFilterType;
    filterData?: any;

    format?: ColumnFormat;
    fieldOption?: string;
    fieldOption1?: string;

    action?: {
        label: string,
        callback: (item: any) => any
    };

    // onAction
    onSwitch?: (item: any) => void;

    constructor() {
        super();
        this.name = null;
        this.secondary_name = null;
        this.transformedName = null;
        this.title = null;
        this.avatar = null;
        this.className = null;

        this.sortable = true;
        this.type = ColumnType.Text;

        this.filterType = ColumnFilterType.Text;
        this.filterData = null;

        this.format = ColumnFormat.None;
        this.fieldOption = null;
        this.fieldOption1 = null;

        this.action = null;

        this.onSwitch = null;
    }

    static create(data) {
        return (new CxiGridColumn()).fill(data);
    }

    static from(data) {
        return data.map(col => this.create(col));
    }
}
