import { BaseEntity } from '../../entities/base.entity';
import { Observable } from 'rxjs';
import { BaseRepository } from '../../repositories/base.repository';

export class CxiGridConfig extends BaseEntity {
    paging?: boolean;
    sorting?: boolean;
    filtering?: boolean;
    selecting?: boolean;
    translatable?: boolean;
    multitab?: boolean;
    type: string;
    filterOutlet: boolean;
    backlist: number;
    withoutsaleman: boolean;
    // pagingServer
    pagingServer: boolean;
    contentType: string;
    orderBy: boolean;
    repository: BaseRepository;

    className: string;

    actions?: {
        onEdit?: (row: any) => Observable<any> | Promise<any> | any,
        onActivate?: (row: any) => Observable<any> | Promise<any> | any,
        onDeactivate?: (row: any) => Observable<any> | Promise<any> | any,
        onDelete?: (row: any) => Observable<any> | Promise<any> | any,

        onConfirm?: (row: any) => Observable<any> | Promise<any> | any,
        onCancel?: (row: any) => Observable<any> | Promise<any> | any,
    };
    tabs?: { title: string, name?: string, value?: any }[];

    constructor() {
        super();
        this.paging = false;
        this.sorting = false;
        this.filtering = false;
        this.selecting = false;
        this.translatable = false;
        this.multitab = false;

        // pagingServer
        this.pagingServer = false;
        this.orderBy = false;
        this.contentType = null;
        this.repository = null;
        this.type = null;
        this.filterOutlet = false;
        this.backlist = null;
        this.className = null;

        this.actions = null;
        this.tabs = null;
        this.withoutsaleman = false;
    }

    static create(data) {
        return (new CxiGridConfig()).fill(data);
    }

    static from(data) {
        return this.create(data);
    }
}
