import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiOptionsComponent } from './cxi-options.component';

describe('CxiOptionsComponent', () => {
  let component: CxiOptionsComponent;
  let fixture: ComponentFixture<CxiOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
