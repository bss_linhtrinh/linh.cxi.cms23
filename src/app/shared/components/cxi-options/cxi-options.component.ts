import {AfterViewChecked, Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {ElementBase} from '../../custom-form/element-base/element-base';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';

@Component({
    selector: 'cxi-options',
    templateUrl: './cxi-options.component.html',
    styleUrls: ['./cxi-options.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: CxiOptionsComponent, multi: true}
    ]
})
export class CxiOptionsComponent extends ElementBase<number> implements OnInit, AfterViewChecked {

    @Input() name: string;
    @Input() label: string;
    @Input() options: any[];

    @Output() change = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }


    ngOnInit() {
    }

    ngAfterViewChecked(): void {
        if (!this.value) {
            this.value = null;
        }
    }

    selectOption(optionId: number) {
        if (this.value !== optionId) {
            this.value = optionId;
            this.change.emit(optionId);
        } else {
            this.value = null;
            this.change.emit(null);
        }
    }
}
