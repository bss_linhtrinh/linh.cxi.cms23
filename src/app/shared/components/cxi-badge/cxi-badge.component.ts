import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'cxi-badge',
    templateUrl: './cxi-badge.component.html',
    styleUrls: ['./cxi-badge.component.scss']
})
export class CxiBadgeComponent implements OnInit {
    @Input() name: string;
    @Input() label: string;
    @Input() color = 'yellow';

    constructor() {
    }

    ngOnInit() {
    }

    trim(value: string): string {
        let output: string;
        if (typeof value !== 'undefined' &&
            value !== null) {
            output = value.trim();
        }
        return output;
    }
}
