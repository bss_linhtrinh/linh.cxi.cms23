import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiBadgeComponent } from './cxi-badge.component';

describe('CxiBadgeComponent', () => {
  let component: CxiBadgeComponent;
  let fixture: ComponentFixture<CxiBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiBadgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
