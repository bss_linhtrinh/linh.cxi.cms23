import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'base-button',
    templateUrl: './base-button.component.html',
    styleUrls: ['./base-button.component.scss']
})
export class BaseButtonComponent implements OnInit {

    @Input() name: string;
    @Input() type = 'button';

    @Input() buttonStyle: string;

    @Input() fullWidth: boolean; // px
    @Input() padding = 8; // px
    @Input() paddingTop = 8; // px
    @Input() paddingBottom = 8; // px
    @Input() paddingLeft = 20; // px
    @Input() paddingRight = 20; // px


    @Input() icon: string;
    @Input() loading: boolean;
    @Input() pressed = false;
    @Input() disabled = false;

    // @Output() click = new EventEmitter();

    iconClass = {};

    ngOnInit() {
        if (this.icon) {
            this.iconClass['icon-' + this.icon] = true;
        }
        if (!this.type) {
            this.type = 'submit';
        }
    }

    // convert to rem
    rem(value: number) {
        return value / 14;
    }
}
