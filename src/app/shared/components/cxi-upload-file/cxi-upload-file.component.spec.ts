import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiUploadFileComponent } from './cxi-upload-file.component';

describe('CxiUploadFileComponent', () => {
  let component: CxiUploadFileComponent;
  let fixture: ComponentFixture<CxiUploadFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiUploadFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiUploadFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
