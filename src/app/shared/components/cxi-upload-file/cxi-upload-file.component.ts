import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';
import {MediaFilesRepository} from '../../repositories/media-files.repository';
import {MediaFile} from '../../entities/media-file';
import {timer} from 'rxjs';
import {Image} from '../../entities/image';
import {map} from 'rxjs/operators';

@Component({
    selector: 'app-cxi-upload-file',
    templateUrl: './cxi-upload-file.component.html',
    styleUrls: ['./cxi-upload-file.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: CxiUploadFileComponent, multi: true}
    ]
})
export class CxiUploadFileComponent extends ElementBase<Image> implements OnInit {

    private _name: string;
    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name.replace('_', ' ');
    }

    private _label: string;
    @Input()
    get label(): string {
        return this._label;
    }

    set label(label: string) {
        this._label = label.replace('_', ' ');
    }

    @Input() public size = 'sm';
    @Input() public width = '';
    @Input() public type = 'text';
    @Input() public placeholder: string;

    private _required: any;

    @Input()
    get required(): any {
        return this._required;
    }

    set required(required: any) {
        this._required = typeof required !== 'undefined';
    }

    @Output() file = new EventEmitter();

    selectedFile: File;

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private mediaFilesRepository: MediaFilesRepository
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onChange() {
    }

    onFileChanged(event, el?: any) {
        if (event.target.files && event.target.files[0]) {
            this.selectedFile = event.target.files[0];
            this.file.emit(this.selectedFile);

            const reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]); // read file as data url
            reader.onload = (evt: Event) => { // called once readAsDataURL is completed
                this.uploadFile(this.selectedFile, el);
            };
        }
    }

    uploadFile(file: File, el?: any) {
        this.mediaFilesRepository.uploadFile(file)
            .pipe(
                map(
                    (mediaFile: MediaFile): Image => {
                        const img = Image.create(mediaFile);

                        if (img.path_string) {
                            img.original_image = img.path_string;
                        }

                        return img;
                    }
                )
            )
            .subscribe(
                (image: Image) => this.value = image,
                () => {
                    timer(500)
                        .subscribe(() => this.resetUploadFile(el));
                }
            );
    }

    removeImage(el?: any) {
        this.resetUploadFile(el);
    }

    resetUploadFile(el?: any) {
        this.selectedFile = null;
        this.file.emit(null);
        el.value = null;
        this.value = null;
    }
}
