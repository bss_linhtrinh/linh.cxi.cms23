import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'cxi-label',
    templateUrl: './cxi-label.component.html',
    styleUrls: ['./cxi-label.component.scss']
})
export class CxiLabelComponent implements OnInit {

    private _name: string;
    private _label: string;
    private _required: any;

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        if (value) {
            value = value.replace('_', ' ');
        }
        value = this.trim(value);
        this._label = value;
    }

    @Input()
    get required(): any {
        return this._required;
    }

    set required(value: any) {
        this._required = typeof value !== 'undefined';
    }

    constructor() {
    }

    ngOnInit() {
    }

    trim(value: string): string {
        let output: string;
        if (typeof value !== 'undefined' &&
            value !== null) {
            output = value.trim();
        }
        return output;
    }

    getType(v) {
        return typeof v;
    }
}
