import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiLabelComponent } from './cxi-label.component';

describe('CxiLabelComponent', () => {
  let component: CxiLabelComponent;
  let fixture: ComponentFixture<CxiLabelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiLabelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
