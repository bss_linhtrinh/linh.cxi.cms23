import {Component, Inject, Input, OnChanges, OnInit, Optional, SimpleChanges, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';

@Component({
    selector: 'cxi-time-picker',
    templateUrl: './cxi-time-picker.component.html',
    styleUrls: ['./cxi-time-picker.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: CxiTimePickerComponent, multi: true}
    ]
})
export class CxiTimePickerComponent extends ElementBase<string> implements OnInit, OnChanges {
    @Input() name: string;
    @Input() label: string;
    @Input() spinners = false;

    private _disabled: any;

    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(disabled: boolean) {
        this._disabled = disabled;
    }

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
        //
    }

    ngOnChanges(changes: SimpleChanges) {

    }
}
