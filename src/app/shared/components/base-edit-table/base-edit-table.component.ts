import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';

@Component({
    selector: 'base-edit-table',
    templateUrl: './base-edit-table.component.html',
    styleUrls: ['./base-edit-table.component.scss']
})
export class BaseEditTableComponent implements OnInit {
    @Input() name: string;
    @Input() label: string;
    @Input() config?: {
        canRemove?: boolean,
    };
    @Input() rows: Array<any>;
    @Input() columns: { title: string, name: string, is_read_only?: boolean }[];

    constructor() {

    }

    ngOnInit() {

    }

    remove(row: any, index?: number) {
        // const column = this.columns[0];
        // const index = this.rows.findIndex(r => r[column.name]);

        this.rows.splice(index, 1);
    }
}
