import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseEditTableComponent } from './base-edit-table.component';

describe('BaseEditTableComponent', () => {
  let component: BaseEditTableComponent;
  let fixture: ComponentFixture<BaseEditTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseEditTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseEditTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
