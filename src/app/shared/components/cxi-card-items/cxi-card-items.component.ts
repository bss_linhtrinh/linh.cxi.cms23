import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'cxi-card-items',
    templateUrl: './cxi-card-items.component.html',
    styleUrls: ['./cxi-card-items.component.scss']
})
export class CxiCardItemsComponent implements OnInit {

    @Input() items: any[];
    @Output() select = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

    selectItem(item: any) {
        this.select.emit(item);
    }
}

