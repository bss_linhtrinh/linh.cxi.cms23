import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiCardItemsComponent } from './cxi-card-items.component';

describe('CxiCardItemsComponent', () => {
  let component: CxiCardItemsComponent;
  let fixture: ComponentFixture<CxiCardItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiCardItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiCardItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
