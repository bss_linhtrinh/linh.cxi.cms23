import {Component, Inject, Input, OnInit, Optional, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';
import {MediaFilesRepository} from '../../repositories/media-files.repository';
import {map} from 'rxjs/operators';
import {MediaFile} from '../../entities/media-file';
import {Image} from '../../entities/image';
import {environment} from '../../../../environments/environment';

@Component({
    selector: 'app-cxi-input-editor',
    templateUrl: './cxi-input-editor.component.html',
    styleUrls: ['./cxi-input-editor.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: CxiInputEditorComponent, multi: true}
    ]
})
export class CxiInputEditorComponent extends ElementBase<number> implements OnInit {

    private _name: string;
    private _label: string;

    selectedFile: File = null;
    reader: FileReader;
    self = this;
    config: any;



    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        this._label = value.replace('_', ' ');
    }

    @Input() public size = 'sm';
    @Input() public width = '';
    @Input() public type = 'text';
    @Input() public placeholder = '';
    @Input() public inputClass = '';
    @Input() icon: string;
    @Input() icon_alignment = 'right';

    private _required: any;

    @Input()
    get required(): any {
        return this._required;
    }

    set required(required: any) {
        this._required = typeof required !== 'undefined';
    }

    private _disabled: any;

    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(disabled: boolean) {
        this._disabled = disabled;
    }

    @Input() maxLength = 65;

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private mediaFilesRepository: MediaFilesRepository
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
        this.initEditor();
    }

    initEditor() {
        var self = this;
        this.config = {
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste autoresize'
            ],
            paste_data_images: true,
            // plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            image_advtab: true,
            file_picker_callback: function(this, cb, value, meta, ) {
                var input = document.createElement('input') as HTMLInputElement;
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                input.onchange = function() {
                    var res = <HTMLInputElement>this;
                    var file: File = res.files[0];
                    var reader = new FileReader();
                    reader.onload = function () {
                        // var base64 = (<string>reader.result).split(',')[1];
                        // // call the callback and populate the Title field with the file name
                        // cb('data:image/png;base64,' + base64, { title: file.name });
                        self.mediaFilesRepository.uploadFile(file)
                            .pipe(
                                map(
                                    (mediaFile: MediaFile): Image => {
                                        const img = Image.create(mediaFile);

                                        if (img.path_string) {
                                            cb(`${environment.baseUrl}${img.path_string}`);
                                        }

                                        return img;
                                    }
                                )
                            )
                            .subscribe(
                                (image: Image) => cb(image)
                            );
                    };
                    reader.readAsDataURL(file);
                };
                input.click();
            }
        };
    }

    onChange() {
        //
    }

}
