import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiInputEditorComponent } from './cxi-input-editor.component';

describe('CxiInputEditorComponent', () => {
  let component: CxiInputEditorComponent;
  let fixture: ComponentFixture<CxiInputEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiInputEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiInputEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
