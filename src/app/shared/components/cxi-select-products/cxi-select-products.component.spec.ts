import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiSelectProductsComponent } from './cxi-select-products.component';

describe('CxiSelectProductsComponent', () => {
  let component: CxiSelectProductsComponent;
  let fixture: ComponentFixture<CxiSelectProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiSelectProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiSelectProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
