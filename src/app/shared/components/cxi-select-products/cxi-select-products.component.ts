import {Component, Inject, Input, OnInit, Optional, ViewChild} from '@angular/core';
import {ElementBase} from '../../custom-form/element-base/element-base';
import {Product} from '../../entities/product';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {Category} from '../../entities/category';
import {tap} from 'rxjs/operators';
import {CategoriesRepository} from '../../repositories/categories.repository';

@Component({
    selector: 'cxi-select-products',
    templateUrl: './cxi-select-products.component.html',
    styleUrls: ['./cxi-select-products.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: CxiSelectProductsComponent, multi: true}
    ]
})
export class CxiSelectProductsComponent extends ElementBase<number[]> implements OnInit {
    @Input() name: string;

    categories: Category[];
    products: Product[];
    selectedProducts: Product[] = [];

    // filter
    categoryId: number;
    subCategoryId: number;
    category: Category;
    subCategory: Category;

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private categoriesRepository: CategoriesRepository
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
        this.getCategories();
    }

    getCategories() {
        this.categoriesRepository.all()
            .pipe(
                tap((categories: Category[]) => this.categories = categories)
            )
            .subscribe();
    }

    getProductsByCategory(categoryId: number) {
        this.categoriesRepository.getProductsByCategory(categoryId)
            .pipe(
                tap((products: Product[]) => this.products = products)
            )
            .subscribe();
    }

    onChangeCategory(categoryId: number) {
        const category = this.categories.find(c => c.id === categoryId);
        if (category) {
            this.category = category;
            this.categoryId = categoryId;

            this.getProductsByCategory(categoryId);
        }
    }

    onChangeSubCategory(subCategoryId: number) {
        const subCategory = this.categories.find(c => c.id === subCategoryId);
        if (subCategory) {
            this.subCategory = subCategory;
            this.subCategoryId = subCategoryId;

            this.getProductsByCategory(subCategoryId);
        }
    }

    select(product: Product) {
        if (!this.value) {
            this.value = [];
        }

        if (!product ||
            !product.id ||
            !Array.isArray(this.value)
        ) {
            return;
        }

        this.value.push(product.id);
        this.selectedProducts.push(product);
    }

    unselect(product: Product) {
        if (!product ||
            !product.id ||
            !this.value ||
            !Array.isArray(this.value)
        ) {
            return;
        }

        const removeIndex = this.selectedProducts.findIndex(p => p.id === product.id);
        if (removeIndex !== -1) {
            this.selectedProducts.splice(removeIndex, 1);
        }

        const valueIndex = this.value.findIndex(v => v === product.id);
        if (valueIndex !== -1) {
            this.value.splice(valueIndex, 1);
        }
    }
}
