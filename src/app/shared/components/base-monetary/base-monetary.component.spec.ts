import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseMonetaryComponent } from './base-monetary.component';

describe('BaseMonetaryComponent', () => {
  let component: BaseMonetaryComponent;
  let fixture: ComponentFixture<BaseMonetaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseMonetaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseMonetaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
