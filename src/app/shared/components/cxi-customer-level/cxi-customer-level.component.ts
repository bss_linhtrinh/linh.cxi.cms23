import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'cxi-customer-level',
    templateUrl: './cxi-customer-level.component.html',
    styleUrls: ['./cxi-customer-level.component.scss']
})
export class CxiCustomerLevelComponent implements OnInit {

    @Input() name: string;
    @Input() label: string = 'customer level';

    @Input() level: string;
    @Input() value: number;
    @Input() maxLevel: number;

    constructor() {
    }

    ngOnInit() {
    }
}
