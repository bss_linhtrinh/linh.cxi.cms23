import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiCustomerLevelComponent } from './cxi-customer-level.component';

describe('CxiCustomerLevelComponent', () => {
  let component: CxiCustomerLevelComponent;
  let fixture: ComponentFixture<CxiCustomerLevelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiCustomerLevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiCustomerLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
