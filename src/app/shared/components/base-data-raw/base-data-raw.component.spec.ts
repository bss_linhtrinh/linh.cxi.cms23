import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseDataRawComponent } from './base-data-raw.component';

describe('BaseDataRawComponent', () => {
  let component: BaseDataRawComponent;
  let fixture: ComponentFixture<BaseDataRawComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseDataRawComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseDataRawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
