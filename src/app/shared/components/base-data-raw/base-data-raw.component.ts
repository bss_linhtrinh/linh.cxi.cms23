import {Component, Inject, Input, OnInit, Optional, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../custom-form/element-base/element-base';

@Component({
    selector: 'base-data-raw',
    templateUrl: './base-data-raw.component.html',
    styleUrls: ['./base-data-raw.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: BaseDataRawComponent, multi: true}
    ]
})
export class BaseDataRawComponent extends ElementBase<number> implements OnInit {
    private _name: string;
    private _label: string;

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        if (value) {
            this._label = value.replace('_', ' ');
        } else {
            this._label = '';
        }
    }

    @Input() public size = 'sm';
    @Input() public width = '';
    @Input() public type = 'text';
    @Input() public placeholder = '';
    @Input() icon: string;
    @Input() icon_alignment = 'right';

    private _required: any;

    @Input()
    get required(): any {
        return this._required;
    }

    set required(required: any) {
        this._required = typeof required !== 'undefined';
    }

    private _disabled: any;

    @Input()
    get disabled(): boolean {
        return this._disabled;
    }

    set disabled(disabled: boolean) {
        this._disabled = disabled;
    }

    @Input() maxLength = 65;

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onChange() {
        //
    }
}
