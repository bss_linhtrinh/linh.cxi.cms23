import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {MediaFilesRepository} from '../../repositories/media-files.repository';
import {map} from 'rxjs/operators';
import {MediaFile} from '../../entities/media-file';
import {Image} from '../../entities/image';
import {timer} from 'rxjs';
import {ElementBase} from '../../custom-form/element-base/element-base';

@Component({
    selector: 'app-cxi-upload-files',
    templateUrl: './cxi-upload-files.component.html',
    styleUrls: ['./cxi-upload-files.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: CxiUploadFilesComponent, multi: true}
    ]
})
export class CxiUploadFilesComponent extends ElementBase<Image[]> implements OnInit {

    private _name: string;

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name.replace('_', ' ');
    }

    private _label: string;
    @Input()
    get label(): string {
        return this._label;
    }

    set label(label: string) {
        this._label = label.replace('_', ' ');
    }

    @Input() public size = 'sm';
    @Input() public width = '';
    @Input() public type = 'text';
    @Input() public placeholder: string;
    @Input() public multiUpload = false;
    @Input() public uploadLimit = 4;

    private _required: any;

    @Input()
    get required(): any {
        return this._required;
    }

    set required(required: any) {
        this._required = typeof required !== 'undefined';
    }

    @Output() file = new EventEmitter();

    selectedFile: File;

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private mediaFilesRepository: MediaFilesRepository
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onChange() {
    }

    onFileChanged(event, el?: any) {
        if (event.target.files && event.target.files[0]) {
            this.selectedFile = event.target.files[0];
            this.file.emit(this.selectedFile);

            const reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]); // read file as data url
            reader.onload = (evt: Event) => { // called once readAsDataURL is completed
                this.uploadFile(this.selectedFile, el);
            };
        }
    }

    uploadFile(file: File, el?: any) {
        this.mediaFilesRepository.uploadFile(file)
            .pipe(
                map(
                    (mediaFile: MediaFile): Image => {
                        const img = Image.create(mediaFile);

                        if (img.path_string) {
                            img.original_image = img.path_string;
                        }

                        return img;
                    }
                )
            )
            .subscribe(
                (image: Image) => {

                    // add media-file
                    if (!this.value || this.value.length === 0) {
                        this.value = [];
                    }

                    // new Image
                    this.value.push(image);
                },
                () => {
                    timer(500)
                        .subscribe(() => this.resetUploadFile(el));
                }
            );
    }

    removeImage(index: number, el?: any) {
        this.selectedFile = null;
        this.file.emit(null);
        el.value = null;

        if (this.value && this.value.length) {
            this.value.splice(index, 1);
        }
    }

    resetUploadFile(el?: any) {
        this.selectedFile = null;
        this.file.emit(null);
        el.value = null;

        if (this.value && this.value.length) {
            this.value.pop();
        }
    }
}
