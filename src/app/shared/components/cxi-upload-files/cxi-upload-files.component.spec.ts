import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiUploadFilesComponent } from './cxi-upload-files.component';

describe('CxiUploadFilesComponent', () => {
  let component: CxiUploadFilesComponent;
  let fixture: ComponentFixture<CxiUploadFilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiUploadFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiUploadFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
