import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'btSearchPipe'
})
export class BtSearchPipePipe implements PipeTransform {

  transform(value: any, props?: any): any {
      let out = [];
      if (Array.isArray(value)) {
          value.forEach(function (item) {
              let itemMatches = false;
              let keys = Object.keys(props);
              for (let i = 0; i < keys.length; i++) {
                  let prop = keys[i];
                  let text = props[prop].toLowerCase();
                  if (item[prop]) {
                      if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                          itemMatches = true;
                          break;
                      }
                  }
              }

              if (itemMatches) {
                  out.push(item);
              }
          });
      } else {
          out = value;
      }
      return out;
  }

}
