import {Pipe, PipeTransform} from '@angular/core';
import {DatePipe} from '@angular/common';

@Pipe({
    name: 'baseDateTime'
})
export class BaseDateTimePipe implements PipeTransform {
    constructor(private datePipe: DatePipe) {
    }

    transform(value: any, args?: any): any {
        if (value) {
            let format = args;
            if (!format) {
                format = 'dd/MM/yyyy HH:mm:ss';
            }
            value = this.datePipe.transform(value, format);
        }
        return value;
    }
}
