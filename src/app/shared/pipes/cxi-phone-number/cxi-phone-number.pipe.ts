import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'cxiPhoneNumber'
})
export class CxiPhoneNumberPipe implements PipeTransform {

    transform(value: any): any {
        let text = `${value}`;
        text = text.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "$1-$2-****");

        return text;
    }

    parse(value: string) {
        return value;
    }

}
