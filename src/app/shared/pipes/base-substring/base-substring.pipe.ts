import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'baseSubstring'
})
export class BaseSubstringPipe implements PipeTransform {

    transform(value: any, args?: any): any {
        let max = 100;
        if (args) {
            max = args;
        }

        return typeof value === 'string'
            ? value.length <= max
                ? value
                : value.substring(0, max) + '...'
            : value;
    }

}
