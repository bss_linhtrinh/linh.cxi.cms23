import {Injector, Pipe, PipeTransform} from '@angular/core';
import {MaskService} from 'ngx-mask';

@Pipe({
    name: 'baseDynamicPipe'
})
export class BaseDynamicPipePipe implements PipeTransform {
    public constructor(private injector: Injector) {
    }

    transform(value: any, pipeToken: any, pipeArgs: any[]): any {
        if (!pipeToken) {
            return value;
        } else {
            let pipe = this.injector.get(pipeToken);
            return pipe.transform(value, ...pipeArgs);
        }
    }

}
