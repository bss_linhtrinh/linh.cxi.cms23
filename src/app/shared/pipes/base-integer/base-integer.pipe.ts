import {Pipe, PipeTransform} from '@angular/core';
import {DecimalPipe} from '@angular/common';

@Pipe({
    name: 'baseInteger'
})
export class BaseIntegerPipe implements PipeTransform {
    constructor(private decimalPipe: DecimalPipe) {
    }

    transform(value: any): any {
        if (!value || isNaN(parseFloat(value))) {
            return value = '';
        }

        value = this.decimalPipe.transform(value, '1.0-0');

        return value;
    }
}
