import {Pipe, PipeTransform} from '@angular/core';
declare var jQuery: any;

@Pipe({
    name: 'cxiStripHtml'
})
export class CxiStripHtmlPipe implements PipeTransform {

    transform(value: any): any {
        let text = `${value}`;
        text = jQuery(text).text();

        return text;
    }

    parse(value: string) {
        return value;
    }

}
