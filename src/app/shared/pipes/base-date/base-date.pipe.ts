import {Pipe, PipeTransform} from '@angular/core';
import {DatePipe} from '@angular/common';
import * as moment from 'moment';

@Pipe({
    name: 'baseDate'
})
export class BaseDatePipe implements PipeTransform {
    constructor(private datePipe: DatePipe) {
    }

    transform(value: any, args?: any): any {
        if (value) {
            let format = args;
            if (!format) {
                format = 'DD/MM/YYYY';
            }
            value = moment(value).format(format);
        }
        return value;
    }
}
