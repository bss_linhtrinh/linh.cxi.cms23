import {Pipe, PipeTransform} from '@angular/core';
import {DecimalPipe} from '@angular/common';

@Pipe({
    name: 'basePercentage'
})
export class BasePercentagePipe implements PipeTransform {
    constructor(private decimalPipe: DecimalPipe) {

    }

    transform(value: any, args?: any): any {
        if (!value || isNaN(parseFloat(value))) {
            return value = '';
        }

        let transformValue = this.decimalPipe.transform(value, '1.0-0');

        // + %
        transformValue += '%';

        return transformValue;
    }

    parse(value: string): number {
        let parsed = (value || '');

        // replace £
        parsed = parsed.replace('%', '');

        return parseInt(parsed, 10);
    }
}
