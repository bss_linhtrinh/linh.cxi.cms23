import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'baseFilterIncludeId',
    pure: false
})
export class BaseFilterIncludeIdPipe implements PipeTransform {

    transform(value: any[], args: number[]): any {
        if (!value) {
            return value;
        }
        if (!args || !args.length) {
            return [];
        }

        return value.filter((item: any) => args.includes(item.id));
    }

}
