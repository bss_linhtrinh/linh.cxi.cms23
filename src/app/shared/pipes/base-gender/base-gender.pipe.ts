import {Pipe, PipeTransform} from '@angular/core';
import {Gender} from '../../entities/gender';

@Pipe({
    name: 'baseGender'
})
export class BaseGenderPipe implements PipeTransform {

    transform(value: any, args?: any): any {
        if (value) {
            const genders = Gender.init();
            const gender = genders.find(g => g.value === value);
            if (!gender) {
                return null;
            }

            return gender.name;
        }
    }
}
