import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'basePaging'
})
export class BasePagingPipe implements PipeTransform {

    transform(value: any[], page?: number, pageSize?: number): any[] {
        const begin = (page - 1) * pageSize;
        const end = page * pageSize;

        return value.slice(begin, end);
    }
}
