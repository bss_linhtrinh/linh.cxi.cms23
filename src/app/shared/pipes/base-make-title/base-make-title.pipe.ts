import {Pipe, PipeTransform} from '@angular/core';
import {UtilitiesService} from '../../services/utilities/utilities.service';

@Pipe({
    name: 'baseMakeTitle'
})
export class BaseMakeTitlePipe implements PipeTransform {
    constructor(private utilities: UtilitiesService) {

    }

    transform(value: any, args?: any): any {
        return value
            ? this.utilities.makeTitle(value)
            : value;
    }

}
