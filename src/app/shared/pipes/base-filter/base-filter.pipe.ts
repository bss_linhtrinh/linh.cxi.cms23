import {Pipe, PipeTransform} from '@angular/core';
import {BaseFilterCriteria} from '../../entities/base-filter-criteria';
import {UtilitiesService} from '../../services/utilities/utilities.service';
import {ColumnFormat} from '../../types/column-format';

@Pipe({
    name: 'baseFilter',
    pure: false
})
export class BaseFilterPipe implements PipeTransform {

    constructor(private utilitiesService: UtilitiesService) {

    }

    transform(items: any[], args: BaseFilterCriteria[] | BaseFilterCriteria): any {
        if (!items || !args) {
            return items;
        }

        let filteredCriterions: BaseFilterCriteria[];
        if (args instanceof Array) {
            filteredCriterions = args.filter(c => c.value !== null);
        } else {
            filteredCriterions = [args].filter(c => c.value !== null);
        }

        if (!items) {
            return items;
        }

        return items.filter(item => {
            let isSelected = true;
            if (filteredCriterions && filteredCriterions.length > 0) {
                let isSatisfyCriteria = true;
                filteredCriterions.forEach((criteria) => {
                    isSatisfyCriteria = isSatisfyCriteria && this.isSatisfyCriteria(item, criteria);
                });
                isSelected = isSatisfyCriteria;
            }
            return isSelected;
        });
    }

    isSatisfyCriteria(item: any, criteria: BaseFilterCriteria): boolean {
        let isSatisfyCriteria: boolean;

        let itemVal: any;
        let criteriaVal: any;

        if (!criteria.format || criteria.format === ColumnFormat.None) {
            if (criteria.name === '*') {
                criteriaVal = `${criteria.value}`;
                // all ~ *
                let anySatisfyCriteria = false;
                for (const i in item) {
                    itemVal = `${item[i]}`;
                    if (typeof itemVal === 'string') {
                        const itemValLower = itemVal.trim().toLowerCase();
                        const criteriaValLower = criteriaVal.trim().toLowerCase();
                        anySatisfyCriteria = (anySatisfyCriteria || itemValLower.includes(criteriaValLower));
                    }
                }
                isSatisfyCriteria = anySatisfyCriteria;
            } else {
                itemVal = `${item[criteria.name]}`;
                criteriaVal = `${criteria.value}`;
                const itemValLower = itemVal.trim().toLowerCase();
                const criteriaValLower = criteriaVal.trim().toLowerCase();
                isSatisfyCriteria = (itemVal && itemValLower.includes(criteriaValLower));
            }
        } else if (criteria.format === ColumnFormat.Boolean) {
            itemVal = item[criteria.name];
            criteriaVal = criteria.value;
            const itemValParsed = this.utilitiesService.boolval(itemVal);
            const criteriaValParsed = this.utilitiesService.boolval(criteriaVal);
            isSatisfyCriteria = this.utilitiesService.isEqual(itemValParsed, criteriaValParsed);
        } else if (criteria.format === ColumnFormat.Gender) {
            itemVal = item[criteria.name];
            criteriaVal = criteria.value;
            isSatisfyCriteria = this.utilitiesService.isEqual(itemVal, criteriaVal);
        } else if (criteria.format === ColumnFormat.Monetary) {
            itemVal = this.utilitiesService.intval(item[criteria.name]);
            criteriaVal = this.utilitiesService.intval(criteria.value);
            isSatisfyCriteria = this.utilitiesService.isEqual(itemVal, criteriaVal);
        }

        return isSatisfyCriteria;
    }

}
