import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'baseSort'
})
export class BaseSortPipe implements PipeTransform {
    orderBy: string;
    reverse: boolean;

    transform(value: any[], orderBy?: string, reverse?: boolean): any[] {
        this.orderBy = orderBy;
        this.reverse = reverse;

        if (value instanceof Array) {
            return value.concat().sort(this.compareFn);
        } else {
            return value;
        }
    }

    compareFn = (a: any, b: any) => {
        // compare
        let compare: number;

        // String
        if (typeof a === 'string' && typeof b === 'string') {
            compare = this.compareStringFn(a, b);
        }

        // Object
        if (typeof a === 'object' && typeof b === 'object') {
            compare = this.compareObjectFn(a, b);
        }

        // reverse
        if (this.reverse) {
            compare *= -1;
        }

        return compare;
    };

    compareObjectFn = (a: any, b: any) => {
        let compare: number;

        if (!this.orderBy) {
            if (a['name'] && b['name']) {
                this.orderBy = 'name';
            } else {
                this.orderBy = 'id';
            }
        }

        const propA = a[this.orderBy];
        const propB = b[this.orderBy];

        compare = this.compareValue(propA, propB);

        return compare;
    };

    compareValue(a: string | number, b: string | number): number {
        let compare: number;

        if (a === null) {
            compare = -1;
        } else if (b === null) {
            compare = 1;
        } else {
            if (typeof a === 'string'
                && typeof b === 'string') {
                compare = this.compareStringFn(a, b);
            } else if (typeof a === 'number'
                && typeof b === 'number') {
                compare = this.compareNumberFn(a, b);
            }
        }

        return compare;
    }

    compareStringFn = (a: string, b: string) => {
        let compare: number;

        a = a.toLowerCase();
        b = b.toLowerCase();

        if (a < b) {
            compare = -1;
        } else if (a > b) {
            compare = 1;
        } else {
            compare = 0;
        }

        return compare;
    };

    compareNumberFn = (a: number, b: number) => {
        let compare: number;

        compare = a - b;

        return compare;
    };
}
