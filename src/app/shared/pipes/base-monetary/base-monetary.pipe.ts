import {Pipe, PipeTransform} from '@angular/core';
import {CurrencyPipe} from '@angular/common';
import {Currency} from '../../entities/currency';
import {CurrenciesRepository} from '../../repositories/currencies.repository';
import {SessionStorage} from 'ngx-webstorage';

@Pipe({
    name: 'baseMonetary'
})
export class BaseMonetaryPipe implements PipeTransform {



    constructor(
        private currencyPipe: CurrencyPipe,
    ) {

    }

    transform(value: any, currency: Currency): any {

        if (value !== null && !isNaN(parseFloat(value))) {
            const display = 'code';  // code, symbol, symbol-narrow, String, Boolean
            const digitsInfo = '1.' +
                (
                    currency
                        ? (currency.decimal_places ? currency.decimal_places : 0) + '-' + (currency.decimal_places ? currency.decimal_places : 0)
                        : '0-0'
                );
            const locale = 'vi';
            //
            value = this.currencyPipe.transform(value, 'VND', display, digitsInfo, locale);
            // value = this.currencyPipe.transform(value, '', '', digitsInfo);
        } else {
            value = '';
        }

        // + VND
        // value += ' VND';

        return value;
    }

    parse(value: string, currency: Currency): string {
        let parsed = (value || '');

        // replace £
        parsed = parsed.replace('£', '');

        // replace (, )
        if (value.indexOf('(') !== -1 || value.indexOf(')') !== -1) {
            parsed = parsed.replace('(', '')
                .replace(')', '');
            parsed = '-' + parsed;
        }

        // split .
        let [integer, fraction = ''] = parsed.split(currency.decimal_separator);

        // replace ,
        integer = integer.replace(new RegExp(currency.thousands_separator, 'g'), '');

        fraction = parseInt(fraction, 10) > 0 && currency.decimal_places > 0
            ? currency.decimal_separator + (fraction).substring(0, currency.decimal_places)
            : '';

        return integer + fraction;
    }
}
