import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'baseBoolean'
})
export class BaseBooleanTypePipe implements PipeTransform {

    transform(value: any): any {
        return value ? 'True' : 'False';
    }
}
