import {Pipe, PipeTransform} from '@angular/core';
import {CurrencyPipe} from '@angular/common';

@Pipe({
    name: 'baseCurrency'
})
export class BaseCurrencyPipe implements PipeTransform {
    private decimal_separator: string;
    private thousands_separator: string;

    constructor(private currencyPipe: CurrencyPipe) {
        this.decimal_separator = '.';
        this.thousands_separator = ',';
    }

    transform(value: any, currencyCode: any = 'GBP', fractionSize: any = 2): any {
        const currency = currencyCode ? currencyCode : 'GBP';
        if (value !== null && !isNaN(parseFloat(value))) {
            value = this.currencyPipe.transform(value, currency);
            if (value.charAt(0) === '-') {
                value = '(' + value.substring(1, value.length) + ')';
                value.replace('-', '');
            }
        } else {
            value = '';
        }

        return value;
    }

    parse(value: string, fractionSize: number = 2): string {
        let parsed = (value || '');

        // replace £
        parsed = parsed.replace('£', '');

        // replace (, )
        if (value.indexOf('(') !== -1 || value.indexOf(')') !== -1) {
            parsed = parsed.replace('(', '')
                .replace(')', '');
            parsed = '-' + parsed;
        }

        // split .
        let [integer, fraction = ''] = parsed.split(this.decimal_separator);

        // replace ,
        integer = integer.replace(new RegExp(this.thousands_separator, 'g'), '');

        fraction = parseInt(fraction, 10) > 0 && fractionSize > 0
            ? this.decimal_separator + (fraction).substring(0, fractionSize)
            : '';

        return integer + fraction;
    }
}
