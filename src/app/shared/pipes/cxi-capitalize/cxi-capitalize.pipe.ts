import {Pipe, PipeTransform} from '@angular/core';
import {UtilitiesService} from '../../services/utilities/utilities.service';

@Pipe({
    name: 'cxiCapitalize'
})
export class CxiCapitalizePipe implements PipeTransform {
    constructor(private utilities: UtilitiesService) {
    }

    transform(value: any, args?: any): any {
        return this.utilities.capitalize(value);
    }
}
