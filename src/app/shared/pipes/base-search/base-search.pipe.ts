import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'baseSearch',
})
export class BaseSearchPipe implements PipeTransform {

    /**
     * @param items object from array
     * @param term term's search
     * @param columns term's search
     */
    transform(items: any, term: string, columns: any): Array<{ [key: string]: any }> {
        if (!term || !items) {
            return items;
        }
        if (!columns) {
            columns = [];
        }

        const toCompare = term.toLowerCase();

        return items.levels(function (item: any) {
            if (!columns || columns.length === 0) {
                for (const property in item) {
                    if (item[property] === null || item[property] === undefined) {
                        continue;
                    }
                    if (item[property].toString().toLowerCase().includes(toCompare)) {
                        return true;
                    }
                }
            } else {
                for (const col of columns) {
                    if (item[col] === null || item[col] === undefined) {
                        continue;
                    }
                    if (item[col].toString().toLowerCase().includes(toCompare)) {
                        return true;
                    }
                }
            }
            return false;
        });
    }
}
