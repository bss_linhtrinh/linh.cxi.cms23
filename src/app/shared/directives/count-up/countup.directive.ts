import {AfterViewInit, Directive, ElementRef, HostListener, Input} from '@angular/core';
import * as CountUp from './countUp.js';

@Directive({
    selector: '[cxiCountUp]'
})

export class CountUpDirective implements AfterViewInit {
    private _endVal;

    countUp: any;

    @Input('countUp') options;
    @Input() startVal;

    @Input()
    /**
     * @return {?}
     */
    get endVal() {
        return this._endVal;
    }

    /**
     * @param {?} value
     * @return {?}
     */
    set endVal(value) {
        this._endVal = value;
        if (isNaN(value)) {
            return;
        }
        if (!this.countUp) {
            return;
        }
        this.countUp.update(value);
    }

    @Input() duration: number;
    @Input() decimals: number;
    @Input() reanimateOnClick: boolean;

    @HostListener('click') onClick() {
        if (this.reanimateOnClick) {
            this.animate();
        }
    }

    constructor(private el: ElementRef) {
        this.el = el;
    }


    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.countUp = this.createCountUp(this.startVal, this.endVal, this.decimals, this.duration);
        this.animate();
    }

    /**
     * Re-animate if preference is set.
     * @return {?}
     */


    /**
     * @param {?} sta
     * @param {?} end
     * @param {?} dec
     * @param {?} dur
     * @return {?}
     */
    createCountUp(sta, end, dec, dur) {
        sta = sta || 0;
        // strip non-numerical characters
        if (isNaN(sta)) {
            sta = Number(sta.match(/[\d\-\.]+/g).join(''));
        }
        end = end || 0;
        if (isNaN(end)) {
            end = Number(end.match(/[\d\-\.]+/g).join(''));
        }
        dur = Number(dur) || 2;
        dec = Number(dec) || 0;
        // construct countUp
        let /** @type {?} */ countUp = new CountUp(this.el.nativeElement, sta, end, dec, dur, this.options);
        const /** @type {?} */ diff = Math.abs(end - sta);
        // make easing smoother for large numbers
        if (diff > 999) {
            const /** @type {?} */ up = (end > sta) ? -1 : 1;
            countUp = new CountUp(this.el.nativeElement, sta, end + (up * 100), dec, dur / 2, this.options);
        }
        return countUp;
    }

    /**
     * @return {?}
     */
    animate() {
        this.countUp.reset();
        if (this.endVal > 999) {
            this.countUp.start(() => this.countUp.update(this.endVal));
        } else {
            this.countUp.start();
        }
    }
}
