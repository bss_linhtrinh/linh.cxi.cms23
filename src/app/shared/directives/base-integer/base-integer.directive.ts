import {Directive, ElementRef, HostListener, OnInit} from '@angular/core';
import {BaseIntegerPipe} from '../../pipes/base-integer/base-integer.pipe';

@Directive({
    selector: '[baseInteger]'
})
export class BaseIntegerDirective implements OnInit {
    private el: any;

    constructor(private elementRef: ElementRef,
                private baseIntegerPipe: BaseIntegerPipe) {
        this.el = this.elementRef.nativeElement;
    }

    ngOnInit(): void {
        this.el.value = this.baseIntegerPipe.transform(this.el.value);
    }

    @HostListener('focus', ['$event.target.value', '$event'])
    onFocus(value, event) {
        this.el.value = value; // opposite of transform
        if (event.which === 9) {
            return false;
        }
        this.el.select();
    }

    @HostListener('blur', ['$event.target.value'])
    onBlur(value) {
        this.el.value = this.baseIntegerPipe.transform(value);
    }

    @HostListener('keydown', ['$event']) onKeyDown(event) {
        const e = <KeyboardEvent>event;
        const preventCodes = [
            8, // backspace
            9, // tab
            13, // enter
            27, // escape
            46, // delete
        ];
        if (preventCodes.indexOf(e.keyCode) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
            // Allow: Ctrl+C
            (e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
            // Allow: Ctrl+V
            (e.keyCode === 86 && (e.ctrlKey || e.metaKey)) ||
            // Allow: Ctrl+X
            (e.keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {

            // let it happen, don't do anything
            return;
        }

        // Ensure that it is a number and stop the keypress
        const alphabetKey = [
            'KeyA', 'KeyB', 'KeyC', 'KeyD', 'KeyE',
            'KeyF', 'KeyG', 'KeyH', 'KeyI', 'KeyJ',
            'KeyK', 'KeyL', 'KeyM', 'KeyN', 'KeyO',
            'KeyP', 'KeyQ', 'KeyR', 'KeyS', 'KeyT',
            'KeyU', 'KeyV', 'KeyW', 'KeyX', 'KeyY',
            'KeyZ',
        ];
        const specialCharKey = [
            'Slash', // Slash /
            'Comma', // comma
            'Period', // period
            'Quote', // period
            'Semicolon', // period
            'BracketRight', // period
            'BracketLeft', // period
            'Backslash', // period
            'Backquote', // period
        ];
        const digitKey = [
            'Digit1',
            'Digit2',
            'Digit3',
            'Digit4',
            'Digit5',
            'Digit6',
            'Digit7',
            'Digit8',
            'Digit9',
            'Digit0',
            'Minus',
            'Equal',
        ];
        const numPadKey = [
            'NumpadDecimal',
            'NumpadDivide',
            'NumpadMultiply',
            'NumpadSubtract',
            'NumpadAdd',
            'NumLock',
        ];
        const blackListKey = [
            ...alphabetKey,
            ...specialCharKey,
            ...numPadKey,
        ];

        if (
            (
                blackListKey.includes(e.code) ||
                (
                    e.shiftKey &&
                    digitKey.includes(e.code)
                )
            )
        ) {
            e.preventDefault();
        }
    }
}
