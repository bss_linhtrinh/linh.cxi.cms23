import {AfterViewInit, Directive, ElementRef, HostListener, OnInit} from '@angular/core';
import {BasePercentagePipe} from '../../pipes/base-percentage/base-percentage.pipe';

@Directive({
    selector: '[basePercentage]'
})
export class BasePercentageDirective implements OnInit {
    private el: any;

    constructor(private elementRef: ElementRef,
                private basePercentagePipe: BasePercentagePipe) {
        this.el = this.elementRef.nativeElement;
    }

    ngOnInit() {
        this.el.value = this.basePercentagePipe.transform(this.el.value);
    }

    @HostListener('focusin', ['$event.target.value', '$event'])
    onFocus(value, event) {
        this.el.value = this.basePercentagePipe.parse(value); // opposite of transform
        if (event.which === 9) {
            return false;
        }
        this.el.select();
    }

    @HostListener('focusout', ['$event.target.value'])
    onBlur(value) {
        this.el.value = this.basePercentagePipe.transform(value);
    }

    @HostListener('keydown', ['$event']) onKeyDown(event) {
        const e = <KeyboardEvent>event;
        if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
            // Allow: Ctrl+C
            (e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
            // Allow: Ctrl+V
            (e.keyCode === 86 && (e.ctrlKey || e.metaKey)) ||
            // Allow: Ctrl+X
            (e.keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }

        // Ensure that it is a number and stop the keypress
        if (
            (
                e.shiftKey ||
                (e.keyCode < 48 || e.keyCode > 57)
            ) &&
            (e.keyCode < 96 || e.keyCode > 105) &&
            (
                e.key !== '0' &&
                e.key !== '1' &&
                e.key !== '2' &&
                e.key !== '3' &&
                e.key !== '4' &&
                e.key !== '5' &&
                e.key !== '6' &&
                e.key !== '7' &&
                e.key !== '8' &&
                e.key !== '9'
            )
            && e.keyCode !== 109 // - subtract
            && e.keyCode !== 189 // - dash
        ) {
            e.preventDefault();
        }
    }
}
