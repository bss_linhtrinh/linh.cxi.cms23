import {
    AbstractControl,
    AsyncValidatorFn,
    NgModel,
    Validator,
    ValidatorFn,
    Validators
} from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ValueAccessorBase } from '../value-access-base/value-accessor-base';
import { map } from 'rxjs/operators';


export interface ValidationResult { [validator: string]: string | boolean };

export type AsyncValidatorArray = Array<Validator | AsyncValidatorFn>;

export type ValidatorArray = Array<Validator | ValidatorFn>;

const normalizeValidator =
    (validator: Validator | ValidatorFn): ValidatorFn | AsyncValidatorFn => {
        const func = (validator as Validator).validate.bind(validator);
        if (typeof func === 'function') {
            return (c: AbstractControl) => func(c);
        } else {
            return <ValidatorFn | AsyncValidatorFn>validator;
        }
    };

export const composeValidators =
    (validators: ValidatorArray): AsyncValidatorFn | ValidatorFn => {
        if (validators == null || validators.length === 0) {
            return null;
        }
        return Validators.compose(validators.map(normalizeValidator));
    };

export const validate =
    (validators: ValidatorArray, asyncValidators: AsyncValidatorArray) => {
        return (control: AbstractControl) => {
            const synchronousValid = () => composeValidators(validators)(control);
            if (asyncValidators) {
                const asyncValidator = composeValidators(asyncValidators);
                return asyncValidator(control).map(v => {
                    const secondary = synchronousValid();
                    if (secondary || v) { // compose async and sync validator results
                        return Object.assign({}, secondary, v);
                    }
                });
            }
            if (validators) {
                return of(synchronousValid());
            }
            return of(null);
        };
    };

export const message = (validator: ValidationResult, key: string): string => {

    switch (key) {
        case 'required':
            return 'Please enter a value';
        case 'pattern':
            return 'Value does not match required pattern';
        case 'minlength':
            return 'Value must be N characters';
        case 'maxlength':
            return 'Value must be a maximum of N characters';
    }

    switch (typeof validator[key]) {
        default:
        case 'undefined':
            return `Validation failed: ${key}`;
        case 'string':
            return <string>validator[key];
    }
};

export abstract class ElementBase<T> extends ValueAccessorBase<T> {
    protected abstract model: NgModel;

    // we will ultimately get these arguments from @Inject on the derived class
    constructor(
        private validators: ValidatorArray,
        private asyncValidators: AsyncValidatorArray
    ) {
        super();
    }

    protected validate(): Observable<ValidationResult> {
        return validate(this.validators, this.asyncValidators)(this.model.control);
    }

    public get invalid(): Observable<boolean> {
        return this.validate().pipe(
            map(v => Object.keys(v || {}).length > 0)
        );
    }

    public get failures(): Observable<Array<string>> {
        return this.validate().pipe(
            map(v => Object.keys(v).map(k => message(v, k)))
        );
    }
}
