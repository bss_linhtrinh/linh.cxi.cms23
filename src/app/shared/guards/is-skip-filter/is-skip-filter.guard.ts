import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {Observable, of} from 'rxjs';
import {AuthService} from '../../services/auth/auth.service';
import {Provider} from '../../types/providers';
import {User} from '../../entities/user';
import {catchError, map} from 'rxjs/operators';
import {Scope} from '../../types/scopes';

@Injectable({
    providedIn: 'root'
})
export class IsSkipFilter implements CanActivate {

    constructor(
        private router: Router,
        private authService: AuthService
    ) {

    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

        const authUser = this.authService.currentUser();

        if (authUser) {
            return this.isSkipFilter(authUser);
        } else {
            return this.authService.authorized()
                .pipe(
                    map(
                        (user: User) => {
                            return this.isSkipFilter(user);
                        }
                    ),
                    catchError(
                        () => {
                            this.router.navigate(['auth/login']);
                            return of(false);
                        }
                    )
                );
        }
    }

    isSkipFilter(authUser: User): boolean {
        if (authUser.provider === Provider.Admin &&
            authUser.role && authUser.role.scope === Scope.Outlet) {
            this.router.navigateByUrl('/');
            return false;
        }

        return true;
    }
}
