import { TestBed, async, inject } from '@angular/core/testing';

import { IsSkipFilter } from './is-skip-filter.guard';

describe('IsSkipFilter', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsSkipFilter]
    });
  });

  it('should ...', inject([IsSkipFilter], (guard: IsSkipFilter) => {
    expect(guard).toBeTruthy();
  }));
});
