import { TestBed, async, inject } from '@angular/core/testing';

import { IsShopAdminGuard } from './is-shop-admin.guard';

describe('IsShopAdminGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsShopAdminGuard]
    });
  });

  it('should ...', inject([IsShopAdminGuard], (guard: IsShopAdminGuard) => {
    expect(guard).toBeTruthy();
  }));
});
