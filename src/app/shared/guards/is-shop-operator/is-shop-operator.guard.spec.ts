import { TestBed, async, inject } from '@angular/core/testing';

import { IsShopOperatorGuard } from './is-shop-operator.guard';

describe('IsShopOperatorGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsShopOperatorGuard]
    });
  });

  it('should ...', inject([IsShopOperatorGuard], (guard: IsShopOperatorGuard) => {
    expect(guard).toBeTruthy();
  }));
});
