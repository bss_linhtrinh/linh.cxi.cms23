import { TestBed, async, inject } from '@angular/core/testing';

import { IsBusinessAdminGuard } from './is-business-admin.guard';

describe('IsBusinessAdminGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsBusinessAdminGuard]
    });
  });

  it('should ...', inject([IsBusinessAdminGuard], (guard: IsBusinessAdminGuard) => {
    expect(guard).toBeTruthy();
  }));
});
