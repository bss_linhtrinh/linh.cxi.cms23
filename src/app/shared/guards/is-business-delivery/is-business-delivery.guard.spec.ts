import { TestBed, async, inject } from '@angular/core/testing';

import { IsBusinessDeliveryGuard } from './is-business-delivery.guard';

describe('IsBusinessDeliveryGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsBusinessDeliveryGuard]
    });
  });

  it('should ...', inject([IsBusinessDeliveryGuard], (guard: IsBusinessDeliveryGuard) => {
    expect(guard).toBeTruthy();
  }));
});
