import { TestBed, async, inject } from '@angular/core/testing';

import { IsOrganisationAdminGuard } from './is-organisation-admin.guard';

describe('IsOrganisationAdminGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsOrganisationAdminGuard]
    });
  });

  it('should ...', inject([IsOrganisationAdminGuard], (guard: IsOrganisationAdminGuard) => {
    expect(guard).toBeTruthy();
  }));
});
