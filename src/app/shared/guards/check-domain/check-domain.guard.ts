import {Injectable} from '@angular/core';
import {CanLoad, Route, UrlSegment} from '@angular/router';
import {Observable} from 'rxjs';
import {CheckDomainService} from '../../services/check-domain/check-domain.service';
import {tap} from 'rxjs/operators';
import {NotificationService} from '../../services/noitification/notification.service';

@Injectable({
    providedIn: 'root'
})
export class CheckDomainGuard implements CanLoad {
    constructor(private checkDomainService: CheckDomainService,
                private notificationService: NotificationService) {
    }

    canLoad(
        route: Route,
        segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
        return this.checkDomainService.check()
            .pipe(
                tap((isActivate: boolean) => {
                    if (!isActivate) {
                        this.notificationService.error([`Domain invalid.`]);
                        throw makeCancelingError(
                            new Error('domain_invalid')
                        );
                    }
                })
            );
    }
}

/**
 * Copied over from Angular Router
 * @see https://goo.gl/8qUsNa
 */
const NAVIGATION_CANCELING_ERROR = 'ngNavigationCancelingError';

/**
 * Similar to navigationCancelingError
 * @see https://goo.gl/nNd9TX
 */
function makeCancelingError(error: Error) {
    (error as any)[NAVIGATION_CANCELING_ERROR] = true;
    return error;
}
