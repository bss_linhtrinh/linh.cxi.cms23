import { TestBed, async, inject } from '@angular/core/testing';

import { CheckDomainGuard } from './check-domain.guard';

describe('CheckDomainGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CheckDomainGuard]
    });
  });

  it('should ...', inject([CheckDomainGuard], (guard: CheckDomainGuard) => {
    expect(guard).toBeTruthy();
  }));
});
