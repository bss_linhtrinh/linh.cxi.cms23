import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {Observable, of} from 'rxjs';
import {AuthService} from '../../services/auth/auth.service';
import {catchError, map, tap} from 'rxjs/operators';
import {User} from '../../entities/user';

@Injectable({
    providedIn: 'root'
})
export class IsLoggedInGuard implements CanActivate {

    constructor(private authService: AuthService,
                private router: Router) {
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot)
        : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {


        return this.authService.authorized()
            .pipe(
                map(
                    (user: User) => !user
                ),
                catchError(
                    () => of(true)
                ),
                tap(
                    (canActivate: boolean) => {
                        if (!canActivate) {
                            this.router.navigate(['/']);
                        }
                    }
                )
            );
    }
}
