import {TestBed, inject} from '@angular/core/testing';
import {IsLoggedInGuard} from './is-loggedin.guard';

describe('IsLoggedInGuard', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [IsLoggedInGuard]
        });
    });

    it('should ...', inject([IsLoggedInGuard], (guard: IsLoggedInGuard) => {
        expect(guard).toBeTruthy();
    }));
});
