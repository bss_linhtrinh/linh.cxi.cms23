import { TestBed, async, inject } from '@angular/core/testing';

import { IsShopEditorGuard } from './is-shop-editor.guard';

describe('IsShopEditorGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsShopEditorGuard]
    });
  });

  it('should ...', inject([IsShopEditorGuard], (guard: IsShopEditorGuard) => {
    expect(guard).toBeTruthy();
  }));
});
