import { TestBed, async, inject } from '@angular/core/testing';

import { IsBusinessOperatorGuard } from './is-business-operator.guard';

describe('IsBusinessOperatorGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsBusinessOperatorGuard]
    });
  });

  it('should ...', inject([IsBusinessOperatorGuard], (guard: IsBusinessOperatorGuard) => {
    expect(guard).toBeTruthy();
  }));
});
