import { TestBed, async, inject } from '@angular/core/testing';

import { HasModulesGuard } from './has-modules.guard';

describe('HasModulesGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HasModulesGuard]
    });
  });

  it('should ...', inject([HasModulesGuard], (guard: HasModulesGuard) => {
    expect(guard).toBeTruthy();
  }));
});
