import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {ModulesService} from '../../services/modules/modules.service';

@Injectable({
    providedIn: 'root'
})
export class HasModulesGuard implements CanActivate {
    constructor(
        private modulesService: ModulesService
    ) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.modulesService.loadMenuItems()
            .pipe(
                map(() => true),
                catchError(() => of(false))
            );
    }

}
