import { TestBed, async, inject } from '@angular/core/testing';

import { IsOrganisationManagementGuard } from './is-organisation-management.guard';

describe('IsOrganisationManagementGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsOrganisationManagementGuard]
    });
  });

  it('should ...', inject([IsOrganisationManagementGuard], (guard: IsOrganisationManagementGuard) => {
    expect(guard).toBeTruthy();
  }));
});
