import { TestBed, async, inject } from '@angular/core/testing';

import { IsOrganisationAccountantGuard } from './is-organisation-accountant.guard';

describe('IsOrganisationAccountantGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsOrganisationAccountantGuard]
    });
  });

  it('should ...', inject([IsOrganisationAccountantGuard], (guard: IsOrganisationAccountantGuard) => {
    expect(guard).toBeTruthy();
  }));
});
