import { TestBed, async, inject } from '@angular/core/testing';

import { HasProviderGuard } from './has-provider.guard';

describe('HasProviderGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HasProviderGuard]
    });
  });

  it('should ...', inject([HasProviderGuard], (guard: HasProviderGuard) => {
    expect(guard).toBeTruthy();
  }));
});
