import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanLoad, Route, RouterStateSnapshot, UrlSegment, UrlTree} from '@angular/router';
import {Observable, of} from 'rxjs';
import {AuthService} from '../../services/auth/auth.service';
import {Provider} from '../../types/providers';
import {CheckDomainService} from '../../services/check-domain/check-domain.service';
import {switchMap, tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class HasProviderGuard implements CanLoad, CanActivate {
    constructor(
        private authService: AuthService,
        private checkDomainService: CheckDomainService
    ) {
    }

    canLoad(
        route: Route,
        segments: UrlSegment[]
    ): Observable<boolean> | Promise<boolean> | boolean {
        return this.checkDomainService.isCxiDomain$()
            .pipe(
                tap(
                    (is_domain_bss: boolean) => {
                        const provider = is_domain_bss ? Provider.SuperAdmin : Provider.Admin;
                        this.authService.setCurrentProvider(provider);
                    }
                ),
                switchMap(
                    () => {
                        const provider = !!this.authService.getCurrentProvider();
                        return of(!!provider);
                    }
                )
            );
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.checkDomainService.isCxiDomain$()
            .pipe(
                tap(
                    (is_domain_bss: boolean) => {
                        const provider = is_domain_bss ? Provider.SuperAdmin : Provider.Admin;
                        this.authService.setCurrentProvider(provider);
                    }
                ),
                switchMap(
                    () => {
                        const authProvider = this.authService.getCurrentProvider();
                        return of(!!authProvider);
                    }
                )
            );
    }
}
