import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanLoad, Route, UrlSegment, Router} from '@angular/router';
import {Observable, of} from 'rxjs';
import {AuthService} from '../../services/auth/auth.service';
import {MenuItem} from '../../entities/menu-item';
import {filter, map, mergeMap, switchMap, tap} from 'rxjs/operators';
import {fromArray} from 'rxjs/internal/observable/fromArray';
import {NotificationService} from '../../services/noitification/notification.service';
import {ModulesService} from '../../services/modules/modules.service';

@Injectable({
    providedIn: 'root'
})
export class CheckPermissionsGuard implements CanActivate, CanLoad {
    constructor(
        private router: Router,
        private authService: AuthService,
        private notificationService: NotificationService,
        private modules: ModulesService
    ) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.checkPermission$(state);
    }

    canLoad(
        route: Route,
        segments: UrlSegment[]
    ): Observable<boolean> | Promise<boolean> | boolean {
        return this.checkPermission$();
    }

    checkPermission$(state?: RouterStateSnapshot) {
        const menuItems = this.modules.getMenuItems();
        const provider = this.authService.getCurrentProvider();
        const level = this.authService.getCurrentLevel();
        if (!state) {
            state = this.router.routerState.snapshot;
        }

        const isActivate$ = fromArray(menuItems)
            .pipe(
                // flatten children
                mergeMap((menu: MenuItem) => {
                    if (menu.children && menu.children.length > 0) {
                        return fromArray(
                            menu.children.concat(
                                menu
                            )
                        );
                    } else {
                        return of(menu);
                    }
                }),

                // filter menu url match
                filter((menu: MenuItem) => {
                    return state.url.includes(menu.link) || state.url === menu.link;
                }),

                switchMap(
                    (menu: MenuItem) => {
                        if (state.url === '/home') {
                            return of(true);
                        }

                        return of(menu)
                            .pipe(
                                map((menuItem: MenuItem) => {
                                    let isActivate = true;
                                    if (menuItem.allow_providers) {
                                        isActivate = isActivate && menuItem.allow_providers.includes(provider);
                                    }
                                    if (menuItem.allow_levels) {
                                        isActivate = isActivate && menuItem.allow_levels.includes(level);
                                    }
                                    return isActivate;
                                }),
                            );
                    }
                ),

                /*is activated*/
                tap((isActivate: boolean) => {
                    if (!isActivate) {
                        this.notificationService.error([`You do not have permission to access this module.`]);
                        throw makeCancelingError(
                            new Error('not_permission')
                        );
                    }
                })
            );

        return isActivate$;
    }
}


/**
 * Copied over from Angular Router
 * @see https://goo.gl/8qUsNa
 */
const NAVIGATION_CANCELING_ERROR = 'ngNavigationCancelingError';

/**
 * Similar to navigationCancelingError
 * @see https://goo.gl/nNd9TX
 */
function makeCancelingError(error: Error) {
    (error as any)[NAVIGATION_CANCELING_ERROR] = true;
    return error;
}
