import { TestBed, async, inject } from '@angular/core/testing';

import { IsBusinessEditorGuard } from './is-business-editor.guard';

describe('IsBusinessEditorGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsBusinessEditorGuard]
    });
  });

  it('should ...', inject([IsBusinessEditorGuard], (guard: IsBusinessEditorGuard) => {
    expect(guard).toBeTruthy();
  }));
});
