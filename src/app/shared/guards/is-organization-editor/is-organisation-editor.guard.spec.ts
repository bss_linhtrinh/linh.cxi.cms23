import { TestBed, async, inject } from '@angular/core/testing';

import { IsOrganisationEditorGuard } from './is-organisation-editor.guard';

describe('IsOrganisationEditorGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsOrganisationEditorGuard]
    });
  });

  it('should ...', inject([IsOrganisationEditorGuard], (guard: IsOrganisationEditorGuard) => {
    expect(guard).toBeTruthy();
  }));
});
