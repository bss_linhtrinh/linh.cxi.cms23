import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Role} from '../entities/role';
import {Customer} from '../entities/customer';

@Injectable({
    providedIn: 'root'
})
export class RolesRepository extends BaseRepository {
    protected _model: Customer = new Customer();

    constructor(injector: Injector) {
        super((item: any) => (new Role()).fill(item), 'admin/roles', injector);
    }
}
