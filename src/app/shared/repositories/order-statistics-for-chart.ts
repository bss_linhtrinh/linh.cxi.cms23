import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { orderStaticForChartDeliveryApp } from '../entities/order-statistics-for-chart-delivery-app';

@Injectable({
    providedIn: 'root',
})
export class orderStaticForChartDeliveryAppRepository extends BaseRepository {

    constructor(injector: Injector) {
        super((item: any) => (new orderStaticForChartDeliveryApp()).fill(item), 'admin/report/delivery-app/order-statistics-for-chart', injector);
    }
}
