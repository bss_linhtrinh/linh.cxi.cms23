import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { Tables } from '../entities/tables';

@Injectable({
    providedIn: 'root',
})
export class TablesRepository extends BaseRepository {

    protected _model: Tables = new Tables();

    constructor(injector: Injector) {
        super((item: any) => (new Tables()).fill(item), 'admin/tables', injector);
    }
}
