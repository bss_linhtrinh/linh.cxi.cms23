import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Notification} from '../entities/notification';

@Injectable({
    providedIn: 'root',
})
export class NotificationsRepository extends BaseRepository {
    protected _model: Notification = new Notification();

    constructor(injector: Injector) {
        super((item: any) => (new Notification()).fill(item), 'admin/notifications', injector);
    }
}
