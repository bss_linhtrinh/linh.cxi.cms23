import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {environment} from '../../../environments/environment';
import {ShiftCalendar} from '../../schedules/shift-calendar';
import {map} from 'rxjs/operators';
import {ShiftCalendarDay} from '../../schedules/shift-calendar-day';
import {ShiftCalendarWeek} from '../../schedules/shift-calendar-week';
import {Shift} from '../../schedules/shift';

@Injectable({
    providedIn: 'root',
})
export class ShiftCalendarRepository extends BaseRepository {
    isMultipleDelete = true;
    isBookingApi = true;

    constructor(injector: Injector) {
        super(
            (item: any) => ShiftCalendar.create(item),
            `${environment.baseApiBookingUrl}/admin/shiftCalendar`,
            injector
        );
    }

    getCalendar(queryParams?: any) {
        return this.apiService.get(this._resource, queryParams)
            .pipe(
                map((res) => res.data),
                map((items: any[]) => this.createModelShiftCalendar(items, queryParams.type)),
            );
    }

    createModelShiftCalendar(data: any, type: string) {
        const mapData: any = {};

        if (data.shiftData) {
            mapData.shiftData = data.shiftData.map(
                item => Shift.create(item)
            );
        }

        if (type === 'day') {
            if (data.data) {
                mapData.shiftCalendarsDay = data.data.map(
                    item => ShiftCalendarDay.create(item)
                );
            }
        } else {
            if (data.data) {
                mapData.shiftCalendarsWeek = ShiftCalendarWeek.create(data.data);
            }
        }

        return mapData;
    }
}
