import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { Customer } from '../entities/customer';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class CustomerRepository extends BaseRepository {

    protected _model: Customer = new Customer();

    constructor(injector: Injector) {
        super((item: any) => (new Customer()).fill(item), 'admin/customers', injector);
    }

    uploadAvatar(customer: Customer, file: File) {
        return this.apiService.uploadFile(`admin/customers/${customer.id}/upload-avatar`, file);
    }

    uploadCreate(customer: Customer, file: File) {
        return this.apiService.uploadFile(`admin/file`, file);
    }

    getLatestCustomers(params: object) {
        return this.apiService.get(`admin/report/latest-customers`, params)
            .pipe(
                map(res => res.data),
                map((customers: any) => customers.map(c => this._createModel(c)))
            );
    }
    //order-history
    getOrderHistory(customer: Customer, sen_data?: any) {
        return this.apiService.get(`admin/customers/${customer.id}/order-history`, sen_data);
    }
    //backlist
    backlistCustomer(sen_data?: any) {
        return this.apiService.post(`admin/customers/blacklist`, sen_data);
    }
}
