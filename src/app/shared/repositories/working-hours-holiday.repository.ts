import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {environment} from '../../../environments/environment';
import {WorkingHourHoliday} from '../../schedules/working-hour-holiday';

@Injectable({
    providedIn: 'root',
})
export class WorkingHoursHolidayRepository extends BaseRepository {
    isBookingApi = true;

    constructor(injector: Injector) {
        super((item: any) => (new WorkingHourHoliday()).fill(item), `${environment.baseApiBookingUrl}/workingHour/holiday`, injector);
    }
}
