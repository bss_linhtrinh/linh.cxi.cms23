import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Commission} from '../entities/commission';

@Injectable({
    providedIn: 'root',
})
export class CommissionsRepository extends BaseRepository {

    constructor(injector: Injector) {
        super((item: any) => (new Commission()).fill(item), 'admin/sales/commissions', injector);
    }
}
