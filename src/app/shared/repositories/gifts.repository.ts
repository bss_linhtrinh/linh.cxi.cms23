import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { Gift } from '../entities/gift';

@Injectable({
    providedIn: 'root',
})
export class GiftRepository extends BaseRepository {

    protected _model: Gift = new Gift();

    constructor(injector: Injector) {
        super((item: any) => (new Gift()).fill(item), 'admin/gifts', injector);
    }
}
