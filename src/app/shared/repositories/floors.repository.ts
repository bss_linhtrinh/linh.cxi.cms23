import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { Floors } from '../entities/floors';

@Injectable({
    providedIn: 'root',
})
export class FloorsRepository extends BaseRepository {

    protected _model: Floors = new Floors();

    constructor(injector: Injector) {
        super((item: any) => (new Floors()).fill(item), 'admin/floors', injector);
    }
}
