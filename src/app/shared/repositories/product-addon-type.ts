import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {AddOnType} from '../entities/add-on-type';

@Injectable({
    providedIn: 'root',
})
export class ProductAddonTypeRepository extends BaseRepository {

    protected _model: AddOnType = new AddOnType();

    constructor(injector: Injector) {
        super((item: any) => (new AddOnType()).fill(item), 'admin/catalog/addon-type', injector);
    }
}
