import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Order} from '../entities/order';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class OrdersRepository extends BaseRepository {

    protected _model: Order = new Order();

    constructor(injector: Injector) {
        super((item: any) => (new Order()).fill(item), 'admin/sales/orders', injector);
    }

    changeStatusOrders(order: Order, data: object) {
        return this.apiService.post(`admin/sales/orders/${order.id}/confirm`, data);
    }

    updateStatusOrders(order: Order, data: object) {
        return this.apiService.post(`admin/sales/orders/${order.id}/update-status`, data);
    }

    getTrendingOrders(params) {
        return this.apiService.get(`admin/report/trending-orders`, params)
            .pipe(
                map(res => res.data),
                map(
                    (orders: any[]) => orders.map(ord => this._createModel(ord))
                )
            );
    }

    printPdf(order) {
        return this.apiService.getPdf(`admin/sales/orders/${order.id}/export-pdf`, {responseType: 'blob'})
            .pipe(
                map(res => {
                    return new Blob([res.blob()], { type: 'application/pdf' });
                })
            );
    }
}
