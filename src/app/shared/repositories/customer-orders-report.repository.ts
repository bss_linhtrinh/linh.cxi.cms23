import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { customerOrdersReport } from '../entities/customer-orders-report';

@Injectable({
    providedIn: 'root',
})
export class customerOrdersReportRepository extends BaseRepository {

    constructor(injector: Injector) {
        super((item: any) => (new customerOrdersReport()).fill(item), 'admin/report/customer-orders', injector);
    }
}
