import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {environment} from '../../../environments/environment';
import {WorkingHour} from '../../schedules/working-hour';

@Injectable({
    providedIn: 'root',
})
export class WorkingHoursRepository extends BaseRepository {
    isBookingApi = true;

    constructor(injector: Injector) {
        super((item: any) => (new WorkingHour()).fill(item), `${environment.baseApiBookingUrl}/workingHour`, injector);
    }
}
