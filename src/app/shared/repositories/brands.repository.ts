import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Brand} from '../entities/brand';

@Injectable({
    providedIn: 'root'
})
export class BrandsRepository extends BaseRepository {

    protected _model: Brand = new Brand();

    constructor(injector: Injector) {
        super( (item: any) => (new Brand()).fill(item), 'admin/brands', injector);
    }
}
