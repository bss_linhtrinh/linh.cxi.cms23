import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Organization} from '../entities/organization';
import {map, tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class OrganizationsRepository extends BaseRepository {
    protected _model: Organization = new Organization();

    constructor(injector: Injector) {
        super((item: any) => (new Organization()).fill(item), 'organizations', injector);
    }

    selfUpdate(data: any) {
        const parsedData = this.parse(data);
        const url = `admin/profiles/${this._resource}`;
        return this.apiService.put(url, parsedData)
            .pipe(
                map(res => res.data),
                map((item: any) => this._createModel(item)),
                tap((item: any) => this.updateInList(item)),
                tap((item: any) => this.fireEventUpdated(item)),
                tap((item: any) => this.cxiGridService.fireEventRowUpdated(item)),
            );
    }
}
