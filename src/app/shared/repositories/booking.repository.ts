import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {map, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Object} from 'fabric/fabric-impl';
import {Booking} from '../entities/booking';
import {LocalStorage} from 'ngx-webstorage';

@Injectable({
    providedIn: 'root',
})
export class BookingRepository extends BaseRepository {

    protected _model: Booking = new Booking();
    @LocalStorage('scope.organization_id') organizationId;
    @LocalStorage('scope.brand_id') brandId;

    constructor(injector: Injector) {
        super((item: any) => (new Booking()).fill(item), 'admin/sales/orders', injector);
    }

    createBooking(data: any, params?: Object): Observable<any> {
        const parsedData = this.parse(data);
        return this.apiService.postWithFormData(`${environment.baseApiBookingUrl}/admin/order/event`, parsedData, params)
            .pipe(
                map(res => res.data),
                map((item: any) => this._createModel(item)),
                tap((item: any) => this.appendInList(item)),
                tap((item: any) => this.fireEventCreated(item)),
                tap((item: any) => this.cxiGridService.fireEventRowCreated(item)),
            );
    }
}
