import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {environment} from '../../../environments/environment';
import {ShiftBlock} from '../../schedules/shift-block';

@Injectable({
    providedIn: 'root',
})
export class ShiftBlocksRepository extends BaseRepository {
    isMultipleDelete = true;
    isBookingApi = true;

    constructor(injector: Injector) {
        super((item: any) => ShiftBlock.create(item), `${environment.baseApiBookingUrl}/admin/shiftBlock`, injector);
    }
}
