import {BaseRepositoryInterface} from './base.repository.interface';
import {ApiService} from '../services/api/api.service';
import {StorageService} from '../services/storage/storage.service';
import {Observable, of} from 'rxjs';
import {map, switchMap, tap} from 'rxjs/operators';
import {Subject} from 'rxjs/index';
import {Injector} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UtilitiesService} from '../services/utilities/utilities.service';
import {MessagesService} from '../../messages/messages.service';
import {User} from '../entities/user';
import {LocalStorage, SessionStorageService} from 'ngx-webstorage';
import {CxiGridService} from '../components/cxi-grid/cxi-grid.service';
import {Customer} from '../entities/customer';
import * as _ from 'lodash';
import {TranslateService} from '@ngx-translate/core';

export abstract class BaseRepository implements BaseRepositoryInterface {
    protected apiService: ApiService;
    protected storageService: StorageService;
    protected sessionStorageService: SessionStorageService;
    protected ngbModal: NgbModal;
    protected utilities: UtilitiesService;
    protected cxiGridService: CxiGridService;
    protected messagesService: MessagesService;
    protected translateService: TranslateService;

    protected singular_resource: string;
    protected _model: any;
    get model() {
        return this._model;
    }

    isCached = false;
    isMultipleDelete = false;
    isBookingApi = false;

    CONFIRM_MSG = `Are you sure you want to {{action}} selected {{object}}?`;

    constructor(protected _createModel: any,
                protected _resource: string,
                protected injector: Injector) {

        this.apiService = injector.get(ApiService);
        this.storageService = injector.get(StorageService);
        this.sessionStorageService = injector.get(SessionStorageService);
        this.ngbModal = injector.get(NgbModal);
        this.utilities = injector.get(UtilitiesService);
        this.cxiGridService = injector.get(CxiGridService);
        this.messagesService = injector.get(MessagesService);
        this.translateService = injector.get(TranslateService);

        this.singular_resource = this.getSingularResource();

        try {
            this._createModel();
        } catch (e) {
            // console.error('Model of ' + this.constructor.name + ' does not exist.');
        }
        if (!this._resource) {
            console.error('Resource of ' + this.constructor.name + ' does not exist');
        }
    }

    private createdSource = new Subject<any>();
    private updatedSource = new Subject<any>();
    private deletedSource = new Subject<any>();
    private removedSource = new Subject<any>();
    private activatedSource = new Subject<any>();
    private deactivatedSource = new Subject<any>();
    created$ = this.createdSource.asObservable();
    updated$ = this.updatedSource.asObservable();
    deleted$ = this.deletedSource.asObservable();
    removed$ = this.removedSource.asObservable();
    activated$ = this.activatedSource.asObservable();
    deactivated$ = this.deactivatedSource.asObservable();

    @LocalStorage('pagination.current_page') current_page: number;
    @LocalStorage('pagination.per_page') per_page: number;
    @LocalStorage('pagination.total') total: number;

    getSingularResource() {
        let output: string;
        output = this._resource.split('')
            .splice(this._resource.lastIndexOf('/') + 1)
            .join('');

        // replace: -
        output = output.split('-').join(' ');

        output = this.utilities.singularize(
            output
        );

        return output;
    }

    parse(model: any) {
        let parsedObject = this.utilities.cloneDeep(model);
        if (typeof parsedObject.parse === 'function') {
            parsedObject = parsedObject.parse();
        }

        return parsedObject;
    }

    fireEventCreated(item: any) {
        this.createdSource.next(item);
    }

    fireEventUpdated(item: any) {
        this.updatedSource.next(this._createModel(item));
    }

    fireEventDeleted(item: any) {
        this.deletedSource.next(this._createModel(item));
        this.current_page = 1;
    }

    fireEventRemoved(id: number) {
        this.removedSource.next(id);
    }

    fireEventActivated(id: number) {
        this.activatedSource.next(id);
    }

    fireEventDeactivated(id: number) {
        this.deactivatedSource.next(id);
    }

    listName(): string {
        let output = 'listAll';

        // split
        const resourceNames = this._resource.split('-');
        resourceNames.map(item => this.ucfirst(item));
        output += resourceNames.join('');

        return output;
    }

    listNameWithParams(params: object): string {
        let listNameWithParams = this.listName();
        let first = true;
        for (const index in params) {
            if (params[index]) {
                listNameWithParams += ((first ? '?' : '&') + index + '=' + params[index]);
                first = false;
            }
        }
        return listNameWithParams;
    }

    /**
     * Find a resource by an id
     *
     * @param id
     * @param data
     * @param {boolean} reload
     * @returns {Observable<any>}
     */
    find(id, data?: any, reload?: boolean): Observable<any> {
        return this.apiService.get(this._resource + '/' + id, data)
            .pipe(
                map(res => res.data),
                map(item => this._createModel(item)),
            );
    }

    /**
     * Return a array of all elements of the resource
     */
    all(params?: any): Observable<any[] | any> {
        let source: any;

        // if (!params) {
        //     params = {pagination: 0};
        // } else if (params && !params.page) {
        //     params.pagination = 0;
        // }

        if (this.isCached) {
            source = this.localAll$(params)
                .pipe(
                    switchMap(
                        (data: any) => {
                            if (data) {
                                return of(data);
                            } else {
                                return this.remoteAll$(this._resource, params);
                            }
                        }
                    )
                );
        } else {
            source = this.remoteAll$(this._resource, params);
        }

        return source;
    }

    localAll$(params?: any): Observable<any> {
        let source: any;
        const resources = this.sessionStorageService.retrieve(this.resourceName());
        if (
            resources &&
            Array.isArray(resources) &&
            resources.length > 0
        ) {
            source = of(resources)
                .pipe(
                    // pagination local
                    map((data: any[]) => {
                        if (params && params.pagination) {
                            this.total = data.length;
                            const from_page = (this.current_page - 1) * this.per_page;
                            const to_page = this.current_page * this.per_page;
                            return data.slice(from_page, to_page);
                        } else {
                            return data;
                        }
                    }),
                    map(
                        (data: any[]) => {
                            if (data && Array.isArray(data)) {
                                return data.map(item => this._createModel(item));
                            } else {
                                return data;
                            }
                        }
                    )
                );
        } else {
            source = of();
        }
        return source;
    }

    remoteAll$(url: string, params?: string): Observable<any> {
        return this.apiService.get(url, params)
            .pipe(
                tap(res => {
                    this.handleSaveMetaPagination(res, params);
                }),
                map(res => this.mapResponseData(res)),
                map((data: any[] | any) => {
                    if (data instanceof Array) {
                        return data.map(item => this._createModel(item));
                    } else {
                        return this._createModel(data);
                    }
                }),
                tap((data: any) => this.storeList(data))
            );
    }

    allByLanguage(langCode: string, params?: object) {
        return this.localAll$(params)
            .pipe(
                map(
                    (data: any) => {
                        return data.map(item => {
                            const objectTranslator = _.find(item.translations, ['locale', langCode]);
                            if (objectTranslator) {
                                for (const j in item.translateProps) {
                                    const prop = item.translateProps[j];
                                    if (objectTranslator[prop]) {
                                        item[prop] = objectTranslator[prop];
                                    }
                                }
                            }
                            return item;
                        });
                    }
                )
            );
    }

    allActivated(params?: any) {
        if (params) {
            if (typeof params.is_activated === 'undefined') {
                params.is_activated = true;
            }
        } else {
            params = {is_activated: true};
        }

        return this.all(params);
    }

    /**
     * Return a array of all elements of the resource
     */
    get(params?: Object): Observable<any[]> {
        return new Observable((observer) => {
            const listNameWithParams = this.listNameWithParams(params);
            const list = this.storageService.retrieve(listNameWithParams);
            if (list && list.length > 0) {
                observer.next(list.map(item => this._createModel(item)));
                observer.complete();
            } else {
                const queryParams = params ? params : {};
                this.apiService.get(this._resource, queryParams)
                    .pipe(
                        map(items => items.map(item => this._createModel(item))),
                        tap(items => this.storageService.store(listNameWithParams, items))
                    )
                    .subscribe(
                        (response: any) => observer.next(response),
                        errors => observer.error(errors),
                        () => observer.complete()
                    );
            }
        });
    }

    /**
     * Save a resource
     *
     * @param {Object} data
     * @param queryParam
     * @returns {Observable<any>}
     */
    save(data: any, queryParam?: Object): Observable<any> {
        if (
            (data.hasOwnProperty('id') && data.id) ||
            (data.hasOwnProperty('_id') && data._id)
        ) {
            return this.update(data.id, data, queryParam);
        } else {
            return this.create(data, queryParam);
        }
    }

    /**
     * Create a resource
     *
     * @param {Object} data
     * @param params
     */
    create(data: any, params?: Object): Observable<any> {
        const parsedData = this.parse(data);
        return this.apiService.post(this._resource, parsedData, params)
            .pipe(
                map(res => res.data),
                map((item: any) => this._createModel(item)),
                tap((item: any) => this.appendInList(item)),
                tap((item: any) => this.fireEventCreated(item)),
                tap((item: any) => this.cxiGridService.fireEventRowCreated(item)),
            );
    }

    /**
     * Update a resource
     *
     * @param id
     * @param {Object} data
     * @param queryParams
     * @returns {Observable<any>
     */
    update(id, data: any, queryParams?: Object): Observable<any> {
        const parsedData = this.parse(data);
        return this.apiService.put(`${this._resource}/${id}`, parsedData, queryParams)
            .pipe(
                map(res => res.data),
                map((item: any) => this._createModel(item)),
                tap((item: any) => this.updateInList(item)),
                tap((item: any) => this.fireEventUpdated(item)),
                tap((item: any) => this.cxiGridService.fireEventRowUpdated(item)),
            );
    }

    /**
     * Destroy a resource
     *
     * @param model
     * @param params
     * @returns {Observable<any>}
     */
    destroy(model: any, params?: Object): Observable<any> {
        const confirm = this.showConfirmationMessage$(this.CONFIRM_MSG, `delete`, this.singular_resource);

        const delete$ = this.singleDelete$(`${this._resource}/${model.id}`)
            .pipe(
                tap(() => this.removeFromList(model)),
                tap((item) => this.fireEventDeleted(item)),
                tap(() => this.cxiGridService.fireEventRowRemoved(model)),
            );

        return confirm
            .pipe(
                switchMap(() => delete$)
            );
    }

    /**
     * Destroy a resource
     *
     * @param models
     * @param params
     * @returns {Observable<any>}
     */
    destroyMultiple(models: any[], params?: any): Observable<any> {
        const confirm = this.showConfirmationMessage$(this.CONFIRM_MSG, `delete`, this.singular_resource);

        const arrId = (models && models.length > 0)
            ? models.map(model => model.id).join(',')
            : null;
        if (!params) {
            params = {arrId: arrId};
        } else {
            params.arrId = arrId;
        }

        const delete$ = this.multipleDelete$(`${this._resource}`, params)
            .pipe(
                // tap(() => this.removeFromList(model)),
                tap(() => models.forEach(model => this.fireEventDeleted(model))),
                tap(() => models.forEach(model => this.cxiGridService.fireEventRowRemoved(model))),
            );

        return confirm
            .pipe(
                switchMap(() => delete$)
            );
    }

    singleDelete$(url: string) {
        return this.apiService.delete(url);
    }

    multipleDelete$(url: string, params?: object) {
        return this.apiService.delete(url, params);
    }


    /**
     * Activate a resource
     *
     * @param model
     * @param {Object} params
     */
    activate(model: any, params?: any): Observable<any> {
        const activateConfirm = this.showConfirmationMessage$(this.CONFIRM_MSG, `activate`, this.singular_resource);

        const activate$ = this.apiService.get(`${this._resource}/${model.id}/active`, params)
            .pipe(
                tap(() => model.is_activated = true),
                tap(() => this.cxiGridService.fireEventRowActivated(model))
            );

        if (params &&
            params.do_not_confirm) {
            return activate$;
        } else {
            return activateConfirm
                .pipe(
                    switchMap(() => activate$)
                );
        }
    }

    /**
     * Deactivate a resource
     *
     * @param model
     * @param {Object} params
     */
    deactivate(model: any, params?: Object): Observable<any> {
        let deactivateConfirm: Observable<any>;

        if (model instanceof User || model instanceof Customer) {
            deactivateConfirm = this.messagesService.deactivateUser();
        } else {
            deactivateConfirm = this.showConfirmationMessage$(this.CONFIRM_MSG, `deactivate`, this.singular_resource);
        }

        const deactivate$ = this.apiService.get(`${this._resource}/${model.id}/deactive`, params)
            .pipe(
                tap(() => model.is_activated = false),
                tap(() => this.cxiGridService.fireEventRowDeactivated(model)),
            );

        return deactivateConfirm
            .pipe(
                switchMap(() => deactivate$)
            );
    }

    /**
     * Set/unset stand out
     * @param model
     * @param params
     */
    standOut(model: any, params?: any): Observable<any> {
        return this.apiService.get(`${this._resource}/${model.id}/stand-out`, params)
            .pipe(
                tap(() => model.is_stand_out = !model.is_stand_out),
                tap(() => this.cxiGridService.fireEventRowChanged(model))
            );
    }

    uploadFile(file: File) {
        return this.apiService.uploadFile(`admin/file`, file)
            .pipe(
                map((res) => res.data),
            );
    }

    handleSaveMetaPagination(res: any, params: any) {
        let meta: any;

        // map meta
        if (this.isBookingApi) {
            meta = (res.data && res.data.meta) ? res.data.meta : null;
        } else {
            meta = res.meta;
        }

        if (meta) {
            if (
                meta.current_page &&
                meta.current_page !== this.current_page
            ) {
                this.current_page = meta.current_page;
            }

            if (
                meta.per_page &&
                meta.per_page !== this.per_page
            ) {
                this.per_page = Number(meta.per_page);
            }

            if (meta.total) {
                this.total = meta.total;
            }

            if (params && params.grid_name) {
                this.cxiGridService.sendMeta(params.grid_name, meta);
            }
        }
    }

    mapResponseData(res: any) {
        let data: any;
        if (this.isBookingApi) {
            data = (res.data && res.data.data) ? res.data.data : res.data;
        } else {
            data = res.data;
        }
        return data;
    }

    resourceName(): string {
        const resourceNames = this._resource.split('/');
        const resourceName = resourceNames[resourceNames.length - 1];

        resourceName
            .split('-')
            .map(item => this.ucfirst(item))
            .join('');

        return resourceName;
    }

    showConfirmationMessage$(message: string, action: string, object: string): Observable<string> {
        object = _.capitalize(object);
        const msg = {
            message: message,
            params: {
                action: action,
                object: object
            }
        };
        const confirm = of(msg)
            .pipe(
                switchMap((messages: any) => {
                    return this.translatedResource$(object)
                        .pipe(
                            map(
                                (translatedResource: string) => {
                                    messages.params.object = translatedResource;
                                    return messages;
                                }
                            )
                        );
                }),
                switchMap((messages: any) => {
                    return this.translatedAction$(action)
                        .pipe(
                            map(
                                (translatedAction: string) => {
                                    messages.params.action = translatedAction;
                                    return messages;
                                }
                            )
                        );
                }),
                switchMap(
                    (messages: any) => this.messagesService.statusConfirmation(messages)
                )
            );
        return confirm;
    }

    translatedResource$(name: string): Observable<string> {
        return this.translateService.get(`${name}.${name}`)
            .pipe(
                map((translatedResource: string) => translatedResource.charAt(0).toLocaleLowerCase() + translatedResource.slice(1)),
            );
    }

    translatedAction$(name: string): Observable<string> {
        return this.translateService.get(`actions.${name}`);
    }

    ucfirst(str: string) {
        return _.startCase(_.toLower(str));
    }

    storeList(data: any) {
        const resourceName = this.resourceName();
        if (data instanceof Array) {
            this.sessionStorageService.store(resourceName, data);
        }
    }

    appendInList(item) {
        const resourceName = this.resourceName();
        this.handleAppendInList(resourceName, item);
    }

    handleAppendInList(key, item) {
        const list = this.sessionStorageService.retrieve(key);
        if (list && Array.isArray(list)) {
            const index = list.findIndex(x => x.id === item.id);
            if (index === -1) {
                list.push(item);
                this.sessionStorageService.store(key, list);
            }
        }
    }

    updateInList(item) {
        const resourceName = this.resourceName();
        this.handleUpdateInList(resourceName, item);
    }

    handleUpdateInList(key: string, item: any) {
        const list = this.sessionStorageService.retrieve(key);
        if (list && Array.isArray(list)) {
            const index = list.findIndex(x => x.id === item.id);
            if (index !== -1) {
                list[index] = item;
                this.sessionStorageService.store(key, list);
            }
        }
    }

    removeFromList(item: any) {
        const resourceName = this.resourceName();
        this.handleRemoveFormList(resourceName, item.id);
    }

    handleRemoveFormList(key, id) {
        const list = this.sessionStorageService.retrieve(key);
        if (list && Array.isArray(list)) {
            const index = list.findIndex(x => x.id === id);
            if (index !== -1) {
                list.splice(index, 1);
                this.sessionStorageService.store(key, list);
            }
        }
    }
}
