import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { topStoresDeliveryOrder } from '../entities/top-stores-delivery-order';

@Injectable({
    providedIn: 'root',
})
export class topStoresDeliveryOrderRepository extends BaseRepository {

    constructor(injector: Injector) {
        super((item: any) => (new topStoresDeliveryOrder()).fill(item), 'admin/report/delivery-app/top-stores', injector);
    }
}
