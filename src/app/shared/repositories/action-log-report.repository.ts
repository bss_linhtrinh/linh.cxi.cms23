import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { actionLogReport } from '../entities/action-log';

@Injectable({
    providedIn: 'root',
})
export class actionLogReportRepository extends BaseRepository {

    constructor(injector: Injector) {
        super((item: any) => (new actionLogReport()).fill(item), 'admin/report/products', injector);
    }
}
