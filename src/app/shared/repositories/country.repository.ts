import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { Country } from '../entities/country';
import { City } from '../entities/city';

@Injectable({
    providedIn: 'root',
})
export class CountryRepository extends BaseRepository {
    isCached = false;
    constructor(injector: Injector) {
        super((item: any) => (new Country()).fill(item), 'countries', injector);
    }
    getCityByCountry(country: Country) {
        return this.apiService.get(`countries/${country.id}/cities`);
    }
    getDistrictsByCountry(city: City) {
        return this.apiService.get(`cities/${city.id}/districts`);
    }

}
