import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { platformReport } from '../entities/platform-report';

@Injectable({
    providedIn: 'root',
})
export class platformReportRepository extends BaseRepository {

    constructor(injector: Injector) {
        super((item: any) => (new platformReport()).fill(item), 'admin/report/platform', injector);
    }
}
