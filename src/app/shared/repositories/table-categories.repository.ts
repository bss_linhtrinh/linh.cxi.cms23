import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { TableCategories } from '../entities/table-categories';

@Injectable({
    providedIn: 'root',
})
export class TableCategoriesRepository extends BaseRepository {

    protected _model: TableCategories = new TableCategories();

    constructor(injector: Injector) {
        super((item: any) => (new TableCategories()).fill(item), 'admin/table-categories', injector);
    }
}
