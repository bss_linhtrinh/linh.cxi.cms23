import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {CaseComment} from '../entities/comment-case';

@Injectable({
    providedIn: 'root',
})
export class CaseCommentRepository extends BaseRepository {
    protected _model: CaseComment = new CaseComment();

    constructor(injector: Injector) {
        super((item: any) => (new CaseComment()).fill(item), 'admin/case-comments', injector);
    }
}
