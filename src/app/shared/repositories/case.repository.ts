import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {CasesManagement} from '../entities/case-management';

@Injectable({
    providedIn: 'root',
})
export class CaseRepository extends BaseRepository {
    protected _model: CasesManagement = new CasesManagement();

    constructor(injector: Injector) {
        super((item: any) => (new CasesManagement()).fill(item), 'admin/cases', injector);
    }
}
