import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {CaseComment} from '../entities/case-comment';

@Injectable({
    providedIn: 'root',
})
export class CaseCommentTemplateRepository extends BaseRepository {
    protected _model: CaseComment = new CaseComment();

    constructor(injector: Injector) {
        super((item: any) => (new CaseComment()).fill(item), 'admin/case-comment-templates', injector);
    }
}
