import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { supportedPayment } from '../entities/supported-payment';

@Injectable({
    providedIn: 'root',
})
export class supportedPaymentRepository extends BaseRepository {
    protected _model: supportedPayment = new supportedPayment();

    constructor(injector: Injector) {
        super((item: any) => (new supportedPayment()).fill(item), 'admin/supported-payment-methods', injector);
    }
}
