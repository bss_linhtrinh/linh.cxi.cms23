import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { orderHistoryReport } from '../entities/order-history-report';

@Injectable({
    providedIn: 'root',
})
export class orderHistoryReportRepository extends BaseRepository {

    constructor(injector: Injector) {
        super((item: any) => (new orderHistoryReport()).fill(item), 'admin/report/order-history', injector);
    }
}
