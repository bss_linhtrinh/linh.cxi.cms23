import { Observable } from "rxjs";

export interface BaseRepositoryInterface {
  /**
   * Find a resource by an id
   *
   * @param id
   * @param {Object} data
   * @returns {Promise<any>}
   */
  find(id, data?: any);

  /**
   * Return a array of all elements of the resource
   */
  all(reload?: boolean);

  /**
   * Save a resource
   *
   * @param {Object} data
   * @returns {Observable<any>}
   */
  save(data: any);

  /**
   * Create a resource
   *
   * @param {Object} data
   */
  create(data: any);

  /**
   * Update a resource
   *
   * @param model
   * @param {Object} data
   */
  update(model: any, data: any);

  /**
   * Destroy a resource
   *
   * @param model
   * @param {boolean} loading
   * @returns {Promise<any>}
   */
  destroy(model: any, loading?: boolean);


  /**
   * Active a resource
   *
   * @param model
   * @param {Object} params
   */
  activate(model: any, params?: Object);


  /**
   * Deactive a resource
   *
   * @param model
   * @param {Object} params
   */
  deactivate(model: any, params?: Object);
}
