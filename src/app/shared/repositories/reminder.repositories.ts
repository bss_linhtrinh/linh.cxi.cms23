import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {environment} from '../../../environments/environment';
import { Reminder } from '../entities/reminder';

@Injectable({
    providedIn: 'root',
})
export class ReminderRepository extends BaseRepository {
    isMultipleDelete = true;
    isBookingApi = true;

    constructor(injector: Injector) {
        super((item: any) => Reminder.create(item), `${environment.baseApiBookingUrl}/admin/reminder`, injector);
    }
}
