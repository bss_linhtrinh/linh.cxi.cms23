import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {environment} from '../../../environments/environment';
import {Checklist} from '../entities/checklist';
import {map, switchMap, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class CheckListRepository extends BaseRepository {

    isMultipleDelete = true;
    isBookingApi = true;

    constructor(injector: Injector) {
        super((item: any) => (new Checklist()).fill(item), `${environment.baseApiBookingUrl}/checkList`, injector);
    }

    estimateTime(params) {
        return this.apiService.post(`${environment.baseApiBookingUrl}/checkList/estimateTime`, params)
            .pipe(
                map(res => res.data)
            );
    }

    changeStatus(model: any, params?: any): Observable<any> {
        const action = (params && params.active) ? `activate` : `deactivate`;
        const confirm$ = this.messagesService.showConfirmationMessage$(this.CONFIRM_MSG, action, this.singular_resource);
        const activate$ = this.apiService.put(`${this._resource}/${model.id}/changeStatus`, null, params)
            .pipe(
                tap(() => model.is_activated = true),
                tap(() => this.cxiGridService.fireEventRowActivated(model))
            );

        return confirm$
            .pipe(
                switchMap(() => activate$)
            );
    }
}
