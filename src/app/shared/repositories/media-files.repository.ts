import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {MediaFile} from '../entities/media-file';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class MediaFilesRepository extends BaseRepository {
    protected _model: MediaFile = new MediaFile();

    constructor(injector: Injector) {
        super((item: any) => (new MediaFile()).fill(item), 'admin/file', injector);
    }

    uploadFile(file: File) {
        return super.uploadFile(file)
            .pipe(
                map((res) => this._createModel(res)),
            );
    }
}
