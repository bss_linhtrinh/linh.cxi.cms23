import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import {Doctor} from '../entities/doctor';

@Injectable({
    providedIn: 'root'
})

export class DoctorRepository extends BaseRepository {

    protected _model: Doctor = new Doctor();

    constructor(injector: Injector) {
        super((item: any) => Doctor.create(item), 'admin/doctor', injector);
    }

    uploadAvatar(doctor: Doctor, file: File) {
        return this.apiService.uploadFile(`${this._resource}/${doctor.id}/upload-avatar`, file);
    }

    uploadCreate(doctor: Doctor, file: File) {
        return this.apiService.uploadFile(`admin/file`, file);
    }

}
