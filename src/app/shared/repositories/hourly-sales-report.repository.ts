import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { hourlySalesReport } from '../entities/hourly-sales';

@Injectable({
    providedIn: 'root',
})
export class hourlySalesReportRepository extends BaseRepository {

    constructor(injector: Injector) {
        super((item: any) => (new hourlySalesReport()).fill(item), 'admin/report/hourly-sales', injector);
    }
}
