import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Voucher} from '../entities/voucher';

@Injectable({
    providedIn: 'root',
})
export class VouchersRepository extends BaseRepository {

    protected _model: Voucher = new Voucher();

    constructor(injector: Injector) {
        super((item: any) => (new Voucher()).fill(item), 'admin/vouchers', injector);
    }
}
