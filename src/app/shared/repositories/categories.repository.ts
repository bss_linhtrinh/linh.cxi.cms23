import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { Category } from '../entities/category';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Product } from '../entities/product';

@Injectable({
    providedIn: 'root'
})

export class CategoriesRepository extends BaseRepository {

    protected _model: Category = new Category();

    constructor(injector: Injector) {
        super((item: any) => (new Category()).fill(item), 'admin/catalog/categories', injector);
    }

    uploadAvatar(category: Category, file: File) {
        return this.apiService.uploadFile(`admin/file`, file);
    }

    allAsTree(params?: object): Observable<Category[]> {
        return this.all(params)
            .pipe(
                map(
                    (categories: Category[]) => Category.treelize(categories)
                )
            );
    }

    getProductsByCategory(category_id: number) {
        const url = `${this._resource}/${category_id}/products`;
        return this.apiService.get(url)
            .pipe(
                map(res => res.data),
                map(
                    (items: any[]): Product[] => items.map(item => Product.create(item))
                ),
            );
    }
}
