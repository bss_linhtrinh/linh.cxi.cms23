import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Permission} from '../entities/permission';

@Injectable({
    providedIn: 'root'
})
export class PermissionsRepository extends BaseRepository {
    protected _model: Permission = new Permission();

    constructor(injector: Injector) {
        super((item: any) => (new Permission()).fill(item), 'admin/permissions', injector);
    }
}
