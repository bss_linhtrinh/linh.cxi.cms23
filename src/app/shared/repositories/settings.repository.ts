import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {switchMap} from 'rxjs/operators';
import {UserScope} from '../entities/user-scope';
import {NEVER} from 'rxjs';
import {ScopeService} from '../services/scope/scope.service';
import {SessionStorage} from 'ngx-webstorage';
import {Option} from '../entities/option';

@Injectable({
    providedIn: 'root',
})
export class SettingsRepository extends BaseRepository {
    isCached = true;
    @SessionStorage('settings') settings: Option[];

    constructor(injector: Injector) {
        super((item: any) => (new Option()).fill(item), 'admin/settings', injector);
    }

    loadAll() {
        const scopeService = this.injector.get(ScopeService);
        const settingsSource = scopeService.getUserScope$()
            .pipe(
                switchMap(
                    (userScope: UserScope) => {
                        if (userScope.outlet && userScope.outlet.id) {
                            return this.all({scope: 'outlets', outlet_id: userScope.outlet.id});
                        } else if (userScope.brand && userScope.brand.id) {
                            return this.all({scope: 'brands', brand_id: userScope.brand.id});
                        } else if (userScope.organization && userScope.organization.id) {
                            return this.all({scope: 'organizations', organization_id: userScope.organization.id});
                        } else {
                            return NEVER;
                        }
                    }
                )
            );
        return settingsSource;
    }

    findValueByKey(key: string) {
        if (
            key &&
            this.settings &&
            this.settings.length > 0
        ) {
            const setting = this.settings.find((s: Option) => s.key === key);
            if (setting) {
                return setting.value;
            }
        }

        return null;
    }

    findByKey(key: string) {
        if (
            key &&
            this.settings &&
            this.settings.length > 0
        ) {
            const setting = this.settings.find((s: Option) => s.key === key);
            if (setting) {
                return setting;
            }
        }

        return null;
    }
}
