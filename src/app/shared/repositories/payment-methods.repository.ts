import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { PaymentMethod } from '../entities/payment-method';

@Injectable({
    providedIn: 'root',
})
export class PaymentMethodsRepository extends BaseRepository {
    protected _model: PaymentMethod = new PaymentMethod();

    constructor(injector: Injector) {
        super((item: any) => (new PaymentMethod()).fill(item), 'admin/payment-methods', injector);
    }
}
