import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { driverListDeliveryApp } from '../entities/driver-list-delivery-app';

@Injectable({
    providedIn: 'root',
})
export class driverListDeliveryAppRepository extends BaseRepository {

    constructor(injector: Injector) {
        super((item: any) => (new driverListDeliveryApp()).fill(item), 'admin/report/delivery-app/driver-list', injector);
    }
}
