import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Currency} from '../entities/currency';
import {filter, map, switchMap, tap, toArray} from 'rxjs/operators';
import {from} from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class SupportedCurrenciesRepository extends BaseRepository {
    isCached = true;

    constructor(injector: Injector) {
        super((item: any) => (new Currency()).fill(item), 'admin/supported-currencies', injector);
    }

    getAvailable(currencies: Currency[], editCurrency?: Currency) {
        const currencyCodes = currencies.map(currency => currency.code);
        let localSupportedCurrencies: Currency[];

        const source = this.all()
            .pipe(
                filter((supportedCurrencies: Currency[]) => supportedCurrencies && Array.isArray(supportedCurrencies)),
                // save supportedCurrencies
                tap((supportedCurrencies: Currency[]) => localSupportedCurrencies = supportedCurrencies),
                // filter (available)
                switchMap((supportedCurrencies: Currency[]) => from(supportedCurrencies)),
                filter((currency: Currency) => !currencyCodes.includes(currency.code)),
                toArray(),
            );

        if (editCurrency) {
            source
                .pipe(
                    map(
                        (availableSupportedCurrencies: Currency[]) => {
                            const supportedItem = localSupportedCurrencies.find(supportedCurrency => supportedCurrency.code === editCurrency.code);
                            const availableItem = availableSupportedCurrencies.find(supportedCurrency => supportedCurrency.code === editCurrency.code);
                            if (supportedItem && !availableItem) {
                                availableSupportedCurrencies.push(supportedItem);
                            }
                            return availableSupportedCurrencies;
                        }
                    )
                );
        }

        return source;
    }
}
