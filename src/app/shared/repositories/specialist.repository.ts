import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import {Specialist} from '../entities/specialist';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class SpecialistRepository extends BaseRepository {

    protected _model: Specialist = new Specialist();

    constructor(injector: Injector) {
        super((item: any) => Specialist.create(item), 'admin/specialist', injector);
    }

    uploadAvatar(specialist: Specialist, file: File) {
        return this.apiService.uploadFile(`admin/file`, file);
    }

    allAsTree(params?: object): Observable<Specialist[]> {
        return this.all(params)
            .pipe(
                map(
                    (specialists: Specialist[]) => Specialist.treelize(specialists)
                )
            );
    }

}
