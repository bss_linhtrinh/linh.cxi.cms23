import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { discountReport } from '../entities/discount-report';

@Injectable({
    providedIn: 'root',
})
export class discountReportRepository extends BaseRepository {

    constructor(injector: Injector) {
        super((item: any) => (new discountReport()).fill(item), 'admin/report/discounts', injector);
    }
}
