import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {RewardSetting} from '../entities/reward-setting';

@Injectable({
    providedIn: 'root'
})

export class RewardsSettingRepository extends BaseRepository {
    protected _model: RewardSetting = new RewardSetting();

    constructor(injector: Injector) {
        super((item: any) => (new RewardSetting()).fill(item), 'admin/loyalty/rewards', injector);
    }
}
