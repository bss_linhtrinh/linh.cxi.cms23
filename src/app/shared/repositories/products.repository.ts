import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Product} from '../entities/product';
import {Customer} from '../entities/customer';

@Injectable({
    providedIn: 'root',
})
export class ProductsRepository extends BaseRepository {
    protected _model: Customer = new Customer();

    constructor(injector: Injector) {
        super((item: any) => (new Product()).fill(item), 'admin/catalog/products', injector);
    }
}
