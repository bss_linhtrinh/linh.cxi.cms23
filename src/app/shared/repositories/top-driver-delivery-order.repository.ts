import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { topDriverDeliveryOrder } from '../entities/top-drivers-delivery-order';

@Injectable({
    providedIn: 'root',
})
export class topDriverDeliveryOrderRepository extends BaseRepository {

    constructor(injector: Injector) {
        super((item: any) => (new topDriverDeliveryOrder()).fill(item), 'admin/report/delivery-app/top-drivers', injector);
    }
}
