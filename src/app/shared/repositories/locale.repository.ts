import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Locale} from '../entities/locale';

@Injectable({
    providedIn: 'root',
})
export class LocalesRepository extends BaseRepository {
    protected _model: Locale = new Locale();

    constructor(injector: Injector) {
        super((item: any) => (new Locale()).fill(item), 'admin/locales', injector);
    }
}
