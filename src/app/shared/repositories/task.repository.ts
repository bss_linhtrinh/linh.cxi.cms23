import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {environment} from '../../../environments/environment';
import {Task} from '../entities/task';
import {Observable} from 'rxjs';
import {switchMap, tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class TaskRepository extends BaseRepository {

    isMultipleDelete = true;
    isBookingApi = true;

    constructor(injector: Injector) {
        super((item: any) => (new Task()).fill(item), `${environment.baseApiBookingUrl}/task`, injector);
    }


    changeStatus(model: any, params?: any): Observable<any> {
        const action = (params && params.active) ? `activate` : `deactivate`;
        const confirm$ = this.messagesService.showConfirmationMessage$(this.CONFIRM_MSG, action, this.singular_resource);
        const activate$ = this.apiService.put(`${this._resource}/${model.id}/changeStatus`, null, params)
            .pipe(
                tap(() => model.is_activated = true),
                tap(() => this.cxiGridService.fireEventRowActivated(model))
            );

        return confirm$
            .pipe(
                switchMap(() => activate$)
            );
    }
}
