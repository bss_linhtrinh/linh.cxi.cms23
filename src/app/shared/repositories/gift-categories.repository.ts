import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { GiftCategories } from '../entities/gift-categories';
import { map } from 'rxjs/operators';
import { Gift } from '../entities/gift';

@Injectable({
    providedIn: 'root',
})
export class GiftCategoriesRepository extends BaseRepository {

    protected _model: GiftCategories = new GiftCategories();

    constructor(injector: Injector) {
        super((item: any) => (new GiftCategories()).fill(item), 'admin/gift-categories', injector);
    }
    getGiftByCategory(category_gift_id: number) {
        const url = `${this._resource}/${category_gift_id}/gifts`;
        return this.apiService.get(url, { status: 1 }) 
            .pipe(
                map(res => res.data),
                map(
                    (items: any[]): Gift[] => items.map(item => Gift.create(item))
                ),
            );
    }
}
