import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Hobby} from '../entities/hobby';

@Injectable({
    providedIn: 'root',
})
export class HobbyRepository extends BaseRepository {
    protected _model: Hobby = new Hobby();

    constructor(injector: Injector) {
        super((item: any) => (new Hobby()).fill(item), 'admin/hobby', injector);
    }
}
