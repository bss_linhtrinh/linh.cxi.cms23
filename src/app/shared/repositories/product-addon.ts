import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {AddOn} from '../entities/add-on';

@Injectable({
    providedIn: 'root',
})
export class ProductAddonRepository extends BaseRepository {
    protected _model: AddOn = new AddOn();

    constructor(injector: Injector) {
        super((item: any) => (new AddOn()).fill(item), 'admin/catalog/addons', injector);
    }

    uploadAvatar(addon: AddOn, file: File) {
        return this.apiService.uploadFile(`admin/file`, file);
    }
}
