import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { platformOverviewReport } from '../entities/platform-overview-report';

@Injectable({
    providedIn: 'root',
})
export class platformOverviewReportRepository extends BaseRepository {

    constructor(injector: Injector) {
        super((item: any) => (new platformOverviewReport()).fill(item), 'admin/report/platform-overview', injector);
    }
}
