import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';
import {Outlet} from '../entities/outlets';

@Injectable({
    providedIn: 'root'
})
export class OutletsRepository extends BaseRepository {

    protected _model: Outlet = new Outlet();

    constructor(injector: Injector) {
        super((item: any) => (new Outlet()).fill(item), 'admin/outlets', injector);
    }

    outletBaseBrandSuperAdmin(organizationId: string | number, brandId: string | number): Observable<any[]> {
        return new Observable((observer) => {
            this.apiService.get(`admin/brands/${brandId}/outlets`, {'organization_id': organizationId})
                .pipe(
                    map(res => res.data),
                    map(items => items.map(item => this._createModel(item))),
                )
                .subscribe(
                    (response: any) => observer.next(response),
                    errors => observer.error(errors),
                    () => observer.complete()
                );
        });
    }

    getOutletsByBrand(brandId: string | number, param?: any): Observable<any[]> {
        if (!brandId) {
            return of(null);
        }

        if (!param) {
            param = {pagination: 0, is_activated: 1};
        } else {
            param.pagination = 0;
            param.is_activated = 1;
        }

        return this.apiService.get(`admin/brands/${brandId}/outlets`, param)
            .pipe(
                map(res => res.data),
                map(items => items.map(item => this._createModel(item))),
            );
    }
}
