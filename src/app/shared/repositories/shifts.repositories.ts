import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {environment} from '../../../environments/environment';
import {Shift} from '../../schedules/shift';
import {Observable} from 'rxjs';
import {map, switchMap, tap} from 'rxjs/operators';
import {ShiftItem} from '../../schedules/shift-item';
import {ShiftBlock} from '../../schedules/shift-block';
import {Salesman} from '../../schedules/salesman';

@Injectable({
    providedIn: 'root',
})
export class ShiftsRepository extends BaseRepository {
    isMultipleDelete = true;
    isBookingApi = true;

    constructor(injector: Injector) {
        super((item: any) => Shift.create(item), `${environment.baseApiBookingUrl}/admin/shift`, injector);
    }

    // activate & deactivate
    changeStatus(model: any, params?: any): Observable<any> {
        const action = (params && params.active) ? `activate` : `deactivate`;
        const confirm$ = this.messagesService.showConfirmationMessage$(this.CONFIRM_MSG, action, this.singular_resource);

        const activate$ = this.apiService.put(`${this._resource}/${model.id}/changeStatus`, null, params)
            .pipe(
                tap(() => model.is_activated = true),
                tap(() => this.cxiGridService.fireEventRowActivated(model))
            );

        return confirm$
            .pipe(
                switchMap(() => activate$)
            );
    }

    // assign staffs for this shiftBlock
    assignShiftForStaffs(shiftBlock: ShiftBlock, staffIds: string[]): Observable<ShiftBlock> {
        const data = {
            staffs: staffIds
        };
        return this.apiService.post(`${this._resource}/${shiftBlock.id}/assign`, data)
            .pipe(
                map(res => res.data),
                map((shiftBlockData: any) => ShiftBlock.create(shiftBlockData))
            );
    }

    // unassign this shiftItem (staff)
    unassignShiftForStaffs(shiftItem: ShiftItem): Observable<ShiftItem> {
        return this.apiService.post(`${this._resource}/${shiftItem.id}/unassign`)
            .pipe(
                map(res => res.data),
                map((shiftItemData: any) => ShiftItem.create(shiftItemData))
            );
    }

    // change status shift item
    changeStatusShiftItems(shiftItems: ShiftItem[], status: string) {
        const params = {
            arrId: shiftItems
                .filter(item => item && item.id)
                .map(item => item.id),
            status: status
        };
        return this.apiService.put(`${this._resource}/shiftItem`, null, params)
            .pipe(
                map(res => res.data),
                map(
                    (shiftItems: any[]) => shiftItems.map(item => ShiftItem.create(item))
                )
            );
    }

    // get free staffs for shiftBlock
    getFreeStaffsForShiftBlock(
        shiftBlock: ShiftBlock,
        params?: object
    ): Observable<Salesman[]> {
        const url = `${this._resource}/${shiftBlock.id}/freeSaleman`;
        return this.apiService.get(url, params)
            .pipe(
                map(res => res.data),
                map((salesmans: any[]) => salesmans.map(s => Salesman.create(s)))
            );
    }
}
