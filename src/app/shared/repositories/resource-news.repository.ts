import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {ResourceNews} from '../entities/resource-news';

@Injectable({
    providedIn: 'root',
})
export class ResourceNewsRepository extends BaseRepository {
    constructor(injector: Injector) {
        const _createModel = (item: any) => (new ResourceNews()).fill(item);
        super(_createModel, 'admin/resources', injector);
    }
}
