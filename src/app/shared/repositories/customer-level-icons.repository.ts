import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {CustomerLevelIcon} from '../entities/customer-level-icon';

@Injectable({
    providedIn: 'root',
})
export class CustomerLevelIconsRepository extends BaseRepository {
    constructor(injector: Injector) {
        super((item: any) => (new CustomerLevelIcon()).fill(item), 'admin/customer-level-icon', injector);
    }
}
