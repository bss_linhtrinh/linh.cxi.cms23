import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {environment} from '../../../environments/environment';
import { Reminder } from '../entities/reminder';

@Injectable({
    providedIn: 'root',
})
export class ReminderAdminRepository extends BaseRepository {
    isMultipleDelete = true;
    isBookingApi = true;
    constructor(injector: Injector) {
        super((item: any) => (new Reminder()).fill(item), `${environment.baseApiBookingUrl}/reminder`, injector);
    }
}
