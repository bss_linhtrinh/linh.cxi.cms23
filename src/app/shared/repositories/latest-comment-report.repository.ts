import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { latestCommentReport } from '../entities/latest-comment-report';

@Injectable({
    providedIn: 'root',
})
export class latestCommentRepository extends BaseRepository {

    constructor(injector: Injector) {
        super((item: any) => (new latestCommentReport()).fill(item), 'admin/report/latest-comments', injector);
    }
}
