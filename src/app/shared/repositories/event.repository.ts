import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Event} from '../entities/event';
import {environment} from '../../../environments/environment';
import {map, mergeMap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class EventRepository extends BaseRepository {
    constructor(injector: Injector) {
        const _createModel = (item: any) => (new Event()).fill(item);
        super(_createModel, 'admin/resources', injector);
    }

    findEvent(id) {
        const url = `${environment.baseApiUrl}/admin/resources/${id}`;
        return this.apiService.get(url)
            .pipe(
                map(res => res.data),
                map((data: any[] | any) => {
                    if (data instanceof Array) {
                        return data.map(item => this._createModel(item));
                    } else {
                        return this._createModel(data);
                    }
                }),
            );
    }

    getListExhibitor(eventId) {
        const url = `admin/resources/${eventId}/get-components`;
        return this.apiService.get(url, {type: 'booth'})
            .pipe(
                map(res => res.data)
            );
    }

    getListComponent() {
        const url = `${environment.baseApiUrl}/admin/resources/component`;
        return this.apiService.get(url)
            .pipe(
                map(res => res.data)
            );
    }

    getListComponentByEventId(params) {
        const url = `${environment.baseApiUrl}/admin/resources/component`;
        return this.apiService.get(url, params)
            .pipe(
                map(res => res.data)
            );
    }

    getTicketByEvent(id, params) {
        const url = `${environment.baseApiUrl}/admin/resources/${id}/get-tickets`;
        return this.apiService.get(url, params)
            .pipe(
                map(res => res.data)
            );
    }
}
