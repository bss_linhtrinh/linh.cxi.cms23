import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { salesReport } from '../entities/sales-report';

@Injectable({
    providedIn: 'root',
})
export class salesReportRepository extends BaseRepository {

    constructor(injector: Injector) {
        super((item: any) => (new salesReport()).fill(item), 'admin/report/stores', injector);
    }
}
