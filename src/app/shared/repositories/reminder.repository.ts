import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { environment } from '../../../environments/environment';
import { Reminder } from '../entities/reminder';
import { tap, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class ReminderRepository extends BaseRepository {
    isMultipleDelete = true;
    isBookingApi = true;
    constructor(injector: Injector) {
        super((item: any) => (new Reminder()).fill(item), `${environment.baseApiBookingUrl}/admin/reminder`, injector);
    }

    changeStatus(models: any, params?: any): Observable<any> {
        const arrId = (models && models.length > 0)
            ? models.map(model => model.id).join(',')
            : null;
        if (!params) {
            params = { arrId: arrId };
        } else {
            params.arrId = arrId + ',' + arrId;
        }
        const action = (params && params.active) ? `activate` : `deactivate`;
        const confirm$ = this.messagesService.showConfirmationMessage$(this.CONFIRM_MSG, action, this.singular_resource);

        const activate$ = this.apiService.put(`${this._resource}`, null, params)
            .pipe(
                tap(() => models.is_activated = true),
                tap(() => this.cxiGridService.fireEventRowActivated(models))
            );

        return confirm$
            .pipe(
                switchMap(() => activate$)
            );
    }
}
