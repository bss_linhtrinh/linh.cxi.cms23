import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { orderListDeliveryApp } from '../entities/order-list-delivery-app';

@Injectable({
    providedIn: 'root',
})
export class orderListDeliveryAppRepository extends BaseRepository {

    constructor(injector: Injector) {
        super((item: any) => (new orderListDeliveryApp()).fill(item), 'admin/report/delivery-app/order-list', injector);
    }
}
