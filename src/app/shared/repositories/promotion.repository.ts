import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Promotion} from '../entities/promotion';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class PromotionRepository extends BaseRepository {

    protected _model: Promotion = new Promotion();

    constructor(injector: Injector) {
        super((item: any) => (new Promotion()).fill(item), 'admin/promotions', injector);
    }

    getAvailable(): Observable<Promotion[]> {
        const params: any = {
            filters: JSON.stringify({
                match: 'and',
                rules: [
                    {
                        field: 'status',
                        operator: '=',
                        value: 1
                    }
                ]
            })
        };
        return this.all(params);
    }
}
