import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Deliver} from '../entities/deliver';

@Injectable({
    providedIn: 'root',
})
export class DeliversRepository extends BaseRepository {
    protected _model: Deliver = new Deliver();

    constructor(injector: Injector) {
        super((item: any) => (new Deliver()).fill(item), 'admin/delivers', injector);
    }
}
