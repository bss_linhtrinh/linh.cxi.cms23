import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {CustomerType} from '../entities/customer-type';

@Injectable({
    providedIn: 'root',
})
export class CustomerTypeRepository extends BaseRepository {
    protected _model: CustomerType = new CustomerType();

    constructor(injector: Injector) {
        super((item: any) => (new CustomerType()).fill(item), 'admin/customer-type', injector);
    }
}
