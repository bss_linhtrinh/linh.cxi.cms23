import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Contents} from '../entities/contents';

@Injectable({
    providedIn: 'root',
})
export class ContentManagementRepository extends BaseRepository {
    constructor(injector: Injector) {
        const _createModel = (item: any) => (new Contents()).fill(item);
        super(_createModel, 'admin/contents', injector);
    }
}
