import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { User } from '../entities/user';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class UsersRepository extends BaseRepository {

    protected _model: User = new User();

    constructor(injector: Injector) {
        super((item: any) => (new User()).fill(item), 'admin/users', injector);
    }

    private changeProfileSource = new Subject<User>();
    changeProfile$ = this.changeProfileSource.asObservable();

    fireEventChangeProfile(item: User) {
        this.changeProfileSource.next(this._createModel(item));
    }

    uploadAvatar(user: User, file: File) {
        return this.apiService.uploadFile(`admin/users/${user.id}/upload-avatar`, file);
    }

    // profile
    uploadAvatarCurrentUser(file: File) {
        return this.apiService.uploadFile(`admin/profiles/change-avatar`, file);
    }

    changePasswordCurrentUser(changePasswordData: any) {
        return this.apiService.post(`admin/profiles/change-password`, changePasswordData)
            .pipe(
                map(res => res.data)
            );
    }
    // Remove-profile
    removeAvatarCurrentUser() {
        return this.apiService.post(`admin/profiles/remove-avatar`);
    }
}
