import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {CustomerLevel} from '../entities/customer-level';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class CustomerLevelsRepository extends BaseRepository {

    protected _model: CustomerLevel = new CustomerLevel();

    constructor(injector: Injector) {
        super((item: any) => (new CustomerLevel()).fill(item), 'admin/customer-level', injector);
    }

    getCustomerLevelAvailable() {
        return this.apiService.get(`admin/customer-level-available`)
            .pipe(
                map(res => res.data),
                map(
                    (orders: any[]) => orders.map(ord => this._createModel(ord))
                )
            );
    }
}
