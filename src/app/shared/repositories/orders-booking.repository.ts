import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Order} from '../entities/order';
import {map, tap} from 'rxjs/operators';
import {BookingList} from '../entities/booking-list';

@Injectable({
    providedIn: 'root',
})
export class OrdersBookingRepository extends BaseRepository {

    protected _model: BookingList = new BookingList();

    constructor(injector: Injector) {
        super((item: any) => (new BookingList()).fill(item), 'admin/sales/orders', injector);
    }

    changeStatusOrders(order: Order, data: object) {
        return this.apiService.post(`admin/sales/orders/${order.id}/confirm`, data);
    }

    updateStatusOrders(order: Order, data: object) {
        return this.apiService.post(`admin/sales/orders/${order.id}/update-status`, data);
    }

    getTrendingOrders(params) {
        return this.apiService.get(`admin/report/trending-orders`, params)
            .pipe(
                map(res => res.data),
                map(
                    (orders: any[]) => orders.map(ord => this._createModel(ord))
                )
            );
    }
}
