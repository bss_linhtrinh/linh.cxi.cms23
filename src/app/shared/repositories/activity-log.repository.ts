import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import {ActivityLog} from '../entities/activity-log';

@Injectable({
    providedIn: 'root',
})
export class ActivityLogRepository extends BaseRepository {
    protected _model: ActivityLog = new ActivityLog();
    constructor(injector: Injector) {
        super((item: any) => (new ActivityLog()).fill(item), 'admin/logs', injector);
    }
}
