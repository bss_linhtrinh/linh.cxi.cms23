import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Attribute} from '../entities/attribute';

@Injectable({
    providedIn: 'root',
})
export class AttributesRepository extends BaseRepository {
    protected _model: Attribute = new Attribute();
    constructor(injector: Injector) {
        super((item: any) => (new Attribute()).fill(item), 'admin/catalog/attributes', injector);
    }
}
