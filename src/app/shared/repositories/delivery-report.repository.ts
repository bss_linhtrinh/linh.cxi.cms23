import { Injectable, Injector } from '@angular/core';
import { BaseRepository } from './base.repository';
import { deliveryReport } from '../entities/delivery-report';

@Injectable({
    providedIn: 'root',
})
export class deliveryReportRepository extends BaseRepository {

    constructor(injector: Injector) {
        super((item: any) => (new deliveryReport()).fill(item), 'admin/report/delivery-report', injector);
    }
}
