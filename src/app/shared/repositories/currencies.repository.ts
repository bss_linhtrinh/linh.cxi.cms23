import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {Currency} from '../entities/currency';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {SessionStorage} from 'ngx-webstorage';

@Injectable({
    providedIn: 'root',
})
export class CurrenciesRepository extends BaseRepository {
    @SessionStorage('currencies') currencies: Currency[];
    @SessionStorage('current-currency') currentCurrency: Currency;

    constructor(injector: Injector) {
        super((item: any) => (new Currency()).fill(item), 'admin/currencies', injector);
    }

    all(params?: any): Observable<Currency[]> {
        return super.all(params)
            .pipe(
                tap((currencies: Currency[]) => this.currencies = currencies),
                tap((currencies: Currency[]) => {
                    if (currencies && currencies.length > 0) {
                        let defaultCurrency = currencies.find(cur => cur.is_default);
                        if (!defaultCurrency && currencies[0]) {
                            defaultCurrency = currencies[0];
                        }
                        this.currentCurrency = defaultCurrency;
                    }
                })
            );
    }
}
