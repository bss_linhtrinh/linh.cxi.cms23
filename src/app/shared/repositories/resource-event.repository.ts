import {Injectable, Injector} from '@angular/core';
import {BaseRepository} from './base.repository';
import {ResourceEvent} from '../entities/resource-event';

@Injectable({
    providedIn: 'root',
})
export class ResourceEventRepository extends BaseRepository {
    constructor(injector: Injector) {
        const _createModel = (item: any) => (new ResourceEvent()).fill(item);
        super(_createModel, 'admin/resources', injector);
    }
}
