import {Injectable} from '@angular/core';
import {ApiService} from '../api/api.service';
import {NEVER, Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';
import {LocalStorageService} from 'ngx-webstorage';
import {SettingsService} from '../../../organization-management/settings/settings.service';
import {AuthService} from '../auth/auth.service';
import {RegionLanguage} from '../../entities/region-language';

@Injectable({
    providedIn: 'root'
})
export class LanguageService {

    DEFAULT_LANGUAGE = 'en';

    private changedLanguageSource = new Subject<string>();
    changedLanguage$ = this.changedLanguageSource.asObservable();

    constructor(
        private apiService: ApiService,
        private localStorageService: LocalStorageService,
        private settingsService: SettingsService,
        private authService: AuthService,
    ) {
    }

    getCurrentLang() {
        const lang = this.localStorageService.retrieve('settings.currentLanguage');
        if (lang) {
            return lang;
        } else {

            // set 'en' as default language
            this.setCurrentLang(this.DEFAULT_LANGUAGE);
            return this.DEFAULT_LANGUAGE;
        }
    }

    setCurrentLang(lang: string) {
        this.localStorageService.store('settings.currentLanguage', lang);
    }

    get(): Observable<string> {
        const currentUser = this.authService.currentUser();
        if (currentUser) {
            return this.settingsService.getSetting('language', 'users', currentUser.id)
                .pipe(
                    map((regionLanguage: any) => RegionLanguage.create(regionLanguage)),
                    map((regionLanguage: RegionLanguage) => regionLanguage.language)
                );
        } else {
            return NEVER;
        }
    }

    changeLanguage(language: string) {
        this.changedLanguageSource.next(language);
    }
}
