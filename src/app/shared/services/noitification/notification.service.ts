import {Injectable, Injector} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';
import {map, switchMap, take, tap, toArray} from 'rxjs/operators';
import {fromArray} from 'rxjs/internal/observable/fromArray';
import {Observable, of} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    toastrService: ToastrService;
    translateService: TranslateService;

    TOAST_SUCCESS = 'toast-success';
    TOAST_ERROR = 'toast-error';
    TOAST_INFO = 'toast-info';
    TOAST_WARNING = 'toast-warning';
    TYPES = [
        this.TOAST_SUCCESS,
        this.TOAST_ERROR,
        this.TOAST_INFO,
        this.TOAST_WARNING,
    ];

    constructor(
        private injector: Injector
    ) {
        this.toastrService = this.injector.get(ToastrService);
        this.translateService = this.injector.get(TranslateService);
    }

    notify(message: string | string[], title?: string, options?: any, type?: string) {
        if (this.TYPES.indexOf(type) === -1) {
            return;
        }

        // translate
        let translatedMessageSource: any;
        translatedMessageSource = this.handleTranslateMessage$(message, title, options);

        // show toast
        translatedMessageSource
            .pipe(
                take(1), // take one message for show Toast
                tap((translatedMessage: string) => this.handleShowToast(translatedMessage, title, options, type))
            )
            .subscribe();
    }

    handleTranslateMessage$(message: string | string[], title?: string, options?: any): Observable<string> {
        let translatedMessageSource: Observable<string>;
        if (typeof message === 'string') {
            if (options && !options.translate) {
                translatedMessageSource = of(message);
            } else {
                translatedMessageSource = this.translateMessage$(message);
            }
        } else {
            message = this.checkDuplicateMessages(message);
            if (options && !options.translate) {
                translatedMessageSource = fromArray(message)
                    .pipe(
                        map((str: string) => str.replace(`<p>`, ``).replace(`</p>`, ``)),
                        map((translatedMsg) => `<p>${translatedMsg}</p>`),
                        toArray(),
                        map((translatedMsgArray: string[]) => translatedMsgArray.join('')),
                    );
            } else {
                translatedMessageSource = this.translateMessages$(message);
            }
        }
        return translatedMessageSource;
    }


    translateMessages$(messages: string[] | string): Observable<string> {
        return fromArray(messages)
            .pipe(
                map((str) => str.replace(`<p>`, ``).replace(`</p>`, ``)),
                map((str) => str.includes(`Messages.`) ? str : `Messages.${str}`),
                switchMap(
                    (msg) => this.translateService.get(msg)
                ),
                map(
                    (translatedMsg) => `<p>${translatedMsg}</p>`
                ),
                toArray(),
                map(
                    (translatedMsgArray: string[]) => translatedMsgArray.join('')
                )
            );
    }

    translateMessage$(message: string): Observable<string> {
        return of(message)
            .pipe(
                map((str) => str.includes(`Messages.`) ? str : `Messages.${str}`),
                switchMap(
                    (msg) => this.translateService.get(msg)
                )
            );
    }

    handleShowToast(msg: string, title?: string, options?: any, type?: string) {
        const activeToast = this.toastrService.show(msg, title, options, type);
        if (
            options &&
            options.onTap &&
            typeof options.onTap === 'function'
        ) {
            activeToast
                .onTap
                .subscribe(() => options.onTap());
        }
        return activeToast;
    }

    error(msg: string | string[], title?: string, options?: any) {
        return this.notify(msg, title, options, this.TOAST_ERROR);
    }

    success(msg: string | string[], title?: string, options?: any) {
        return this.notify(msg, title, options, this.TOAST_SUCCESS);
    }

    show(msg: string | string[], title?: string, options?: any, type?: string) {
        if (!type) {
            type = this.TOAST_SUCCESS;
        }
        return this.notify(msg, title, options, type);
    }

    checkDuplicateMessages(errorMessages: string[]) {
        const messages = [];
        errorMessages.forEach(
            (errorMessage: string) => {
                if (!messages.includes(errorMessage)) {
                    messages.push(errorMessage);
                }
            }
        );
        return messages;
    }
}
