import {Injectable} from '@angular/core';
import {Observable, Subject, throwError} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {catchError, finalize} from 'rxjs/operators';
import {UtilitiesService} from '../utilities/utilities.service';

@Injectable()
export class ApiService {
    private countApiStack = 0;

    private apiLoadingSource = new Subject<number>();
    private clientErrorsSource = new Subject<any>();

    apiLoading$ = this.apiLoadingSource.asObservable();
    clientErrors$ = this.clientErrorsSource.asObservable();

    constructor(
        private httpClient: HttpClient,
        private utilities: UtilitiesService
    ) {

    }

    eventLoading(countApiStack = 0) {

    }

    // Method
    get(resource, params?: any): Observable<any> {

        const httpOptions = {
            params: new HttpParams({
                fromObject: params
            })
        };

        return new Observable((observer) => {
            this.apiLoadingSource.next(++this.countApiStack);
            this.httpClient.get(resource, Object.assign({}, httpOptions))
                .pipe(
                    finalize(() => this.apiLoadingSource.next(--this.countApiStack)),
                    catchError(
                        err => {
                            this.handleValidationError(err);
                            return throwError(err);
                        }
                    )
                )
                .subscribe(
                    (res) => observer.next(res),
                    (err) => observer.error(err),
                    () => observer.complete()
                );
        });
    }

    getPdf(resource, params?: any): Observable<any> {

        const httpOptions = {
            params: new HttpParams({
                fromObject: params
            }),
            headers: {
                responseType: 'blob',
                Accept: 'application/pdf, text/plain, */*',
                'Content-Type': 'application/pdf'

            }
        };

        return new Observable((observer) => {
            this.apiLoadingSource.next(++this.countApiStack);
            this.httpClient.get(resource, Object.assign({}, httpOptions))
                .pipe(
                    finalize(() => this.apiLoadingSource.next(--this.countApiStack)),
                    catchError(
                        err => {
                            this.handleValidationError(err);
                            return throwError(err);
                        }
                    )
                )
                .subscribe(
                    (res) => observer.next(res),
                    (err) => observer.error(err),
                    () => observer.complete()
                );
        });
    }

    postWithFormData(resource: string, data?: Object, params?: any): Observable<any> {
        const httpOptions = {
            params: new HttpParams({
                fromObject: params
            })
        };
        const formData = this.utilities.parseToFormData(data);

        return new Observable((observer) => {
            this.apiLoadingSource.next(++this.countApiStack);
            this.httpClient.post(resource, this.parseData(formData), httpOptions)
                .pipe(
                    finalize(() => this.apiLoadingSource.next(--this.countApiStack)),
                    catchError(
                        err => {
                            this.handleValidationError(err);
                            return throwError(err);
                        }
                    )
                )
                .subscribe(
                    (res) => observer.next(res),
                    (err) => observer.error(err),
                    () => observer.complete()
                );
        });
    }

    post(resource: string, data?: Object, params?: any): Observable<any> {
        const httpOptions = {
            params: new HttpParams({
                fromObject: params
            })
        };

        this.apiLoadingSource.next(++this.countApiStack);
        return this.httpClient.post(resource, this.parseData(data), httpOptions)
            .pipe(
                finalize(() => this.apiLoadingSource.next(--this.countApiStack)),
                catchError(
                    err => {
                        this.handleValidationError(err);
                        return throwError(err);
                    }
                )
            );
    }

    put(resource: string, data?: Object, queryParams?: any): Observable<any> {
        const httpOptions = {
            params: new HttpParams({
                fromObject: queryParams
            })
        };

        return new Observable((observer) => {
            this.apiLoadingSource.next(++this.countApiStack);
            this.httpClient.put(resource, this.parseData(data), httpOptions)
                .pipe(
                    finalize(() => this.apiLoadingSource.next(--this.countApiStack)),
                    catchError(
                        err => {
                            this.handleValidationError(err);
                            return throwError(err);
                        }
                    )
                )
                .subscribe(
                    (res) => observer.next(res),
                    (err) => observer.error(err),
                    () => observer.complete()
                );
        });
    }

    delete(resource: string, queryParams?: any): Observable<any> {
        const httpOptions = {
            params: new HttpParams({
                fromObject: queryParams
            })
        };

        return new Observable((observer) => {
            this.apiLoadingSource.next(++this.countApiStack);
            this.httpClient.delete(resource, httpOptions)
                .pipe(
                    finalize(() => this.apiLoadingSource.next(--this.countApiStack)),
                    catchError(
                        err => {
                            this.handleValidationError(err);
                            return throwError(err);
                        }
                    )
                )
                .subscribe(
                    (res) => observer.next(res),
                    (err) => observer.error(err),
                    () => observer.complete()
                );
        });
    }

    uploadFile(resource: string, data?: FormData | File): Observable<any> {
        let formData: FormData;
        if (data instanceof File) {
            formData = new FormData();
            formData.append('file', data, data.name);
        } else {
            formData = data;
        }

        return this.post(resource, formData);
    }

    parseData(params: any) {
        let output: any;
        if (params instanceof FormData) {
            output = params;
        } else {
            output = {};
            for (const index in params) {
                const value = params[index];
                if (typeof value === 'undefined') {
                    continue;
                }

                output[index] = value;
            }
        }
        return output;
    }

    // handle error
    handleValidationError(e: any) {
        this.clientErrorsSource.next(e);
    }
}
