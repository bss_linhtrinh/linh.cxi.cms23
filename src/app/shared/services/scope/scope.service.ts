import {Injectable} from '@angular/core';
import {Provider} from '../../types/providers';
import {AuthService} from '../auth/auth.service';
import {UserScope} from '../../entities/user-scope';
import {Observable, of, Subject} from 'rxjs';
import {OrganizationsRepository} from '../../repositories/organizations.repository';
import {BrandsRepository} from '../../repositories/brands.repository';
import {OutletsRepository} from '../../repositories/outlets.repository';
import {map, tap} from 'rxjs/operators';
import {Outlet} from '../../entities/outlets';
import {Brand} from '../../entities/brand';
import {Organization} from '../../entities/organization';
import {LocalStorage} from 'ngx-webstorage';
import {Scope} from '../../types/scopes';

@Injectable({
    providedIn: 'root'
})
export class ScopeService {

    private userScopeSource = new Subject<UserScope>();
    userScope$ = this.userScopeSource.asObservable();

    @LocalStorage('scope.organization_id') organizationId: number;
    @LocalStorage('scope.brand_id') brandId: number;
    @LocalStorage('scope.outlet_id') outletId: number;
    @LocalStorage('scope.organization') organization: Organization;
    @LocalStorage('scope.brand') brand: Brand;
    @LocalStorage('scope.outlet') outlet: Outlet;
    @LocalStorage('scope.current') currentScope: Scope;

    constructor(private authService: AuthService,
                private organizationsRepository: OrganizationsRepository,
                private brandsRepository: BrandsRepository,
                private outletsRepository: OutletsRepository) {
    }

    setUserScope(userScope: UserScope) {
        this.organizationId = userScope.organization ? userScope.organization.id : null;
        this.brandId = userScope.brand ? userScope.brand.id : null;
        this.outletId = userScope.outlet ? userScope.outlet.id : null;
        this.organization = userScope.organization;
        this.brand = userScope.brand;
        this.outlet = userScope.outlet;
        this.currentScope = !!this.outletId
            ? Scope.Outlet
            : (
                !!this.brandId
                    ? Scope.Brand
                    : (
                        !!this.organizationId
                            ? Scope.Organization
                            : null
                    )
            );

        this.userScopeSource.next(userScope);
    }

    getUserScope$(): Observable<UserScope> {
        const filter = this.authService.getCurrentFilter();
        const currentUser = this.authService.currentUser();
        if (!currentUser) {
            return of(null);
        }

        // filter by:
        const scope = new UserScope();

        if (currentUser.provider === Provider.SuperAdmin) {
            if (filter) {
                if (filter.outlet && filter.outlet.id) {
                    scope.organization = filter.organization;
                    scope.brand = filter.brand;
                    scope.outlet = filter.outlet;
                } else if (filter.brand && filter.brand.id) {
                    scope.organization = filter.organization;
                    scope.brand = filter.brand;
                    scope.outlet = null;
                } else if (filter.organization && filter.organization.id) {
                    scope.organization = filter.organization;
                    scope.brand = null;
                    scope.outlet = null;
                }
            } else {
                scope.organization = null;
                scope.brand = null;
                scope.outlet = null;
            }
        } else if (currentUser.provider === Provider.Admin) {
            if (currentUser) {
                if (currentUser.outlet && currentUser.outlet.id) {
                    scope.organization = currentUser.organization;
                    scope.brand = currentUser.brand;
                    scope.outlet = currentUser.outlet;
                } else if (currentUser.brand && currentUser.brand.id) {
                    scope.organization = currentUser.organization;
                    scope.brand = currentUser.brand;
                    scope.outlet = null;
                    if (filter) {
                        if (filter.outlet && filter.outlet.id) {
                            scope.outlet = filter.outlet;
                        }
                    }
                } else if (currentUser.organization && currentUser.organization.id) {
                    scope.organization = currentUser.organization;
                    scope.brand = null;
                    scope.outlet = null;
                    if (filter) {
                        if (filter.outlet && filter.outlet.id) {
                            scope.brand = filter.brand;
                            scope.outlet = filter.outlet;
                        } else if (filter.brand && filter.brand.id) {
                            scope.brand = filter.brand;
                        }
                    }
                }
            }
        }

        this.setUserScope(scope);

        return of(scope);
    }

    updateUserScope$(userScope: UserScope): Observable<UserScope | null> {
        if (userScope.outlet && userScope.outlet.id) {
            return this.outletsRepository.save(userScope.outlet)
                .pipe(
                    tap((outlet: Outlet) => userScope.outlet = outlet),
                    map(() => userScope)
                );
        } else if (userScope.brand && userScope.brand.id) {
            return this.brandsRepository.save(userScope.brand)
                .pipe(
                    tap((brand: Brand) => userScope.brand = brand),
                    map(() => userScope)
                );
        } else if (userScope.organization && userScope.organization.id) {
            return this.organizationsRepository.selfUpdate(userScope.organization)
                .pipe(
                    tap((organization: Organization) => userScope.organization = organization),
                    map(() => userScope)
                );
        } else {
            return of(null);
        }
    }
}
