import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {ScopeService} from '../scope/scope.service';
import {UserScope} from '../../entities/user-scope';
import {AuthService} from '../auth/auth.service';
import {FilterStorageService} from '../filterStorage/filterStorage.service';
import Pusher from 'pusher-js';

// pusher.service.ts
// declare const Pusher: any;

@Injectable({
    providedIn: 'root'
})
export class PusherService {
    pusher: Pusher;
    filterLocalStore: any;
    channel: any;

    constructor(
        private httpClient: HttpClient,
        private authService: AuthService,
        private filterStorage: FilterStorageService,
        private scopeService: ScopeService
    ) {
        const accessToken = this.authService.getAccessToken(this.authService.getCurrentProvider());
        if (accessToken) {
            this.filterLocalStore = this.filterStorage.retrieve();
            if (this.filterLocalStore) {
                this.pusher = new Pusher(environment.pusher.key, {
                    cluster: environment.pusher.cluster,
                    encrypted: true,
                    authEndpoint: `${environment.baseUrl}/api/v1/broadcasting/auth`,
                    auth: {
                        params: {
                            organization_id: this.filterLocalStore.organization.id,
                            brand_id: this.filterLocalStore.brand.id,
                            outlet_id: this.filterLocalStore.outlet.id
                        },
                        headers: {
                            Authorization: `Bearer ${accessToken}`
                        }
                    }
                });
                if (this.filterLocalStore.outlet && this.filterLocalStore.outlet.id) {
                    this.channel = this.pusher.subscribe('private-order.on.outlet.' + this.filterLocalStore.outlet.id);
                } else if (this.filterLocalStore.brand && this.filterLocalStore.brand.id) {
                    this.channel = this.pusher.subscribe('private-order.on.brand.' + this.filterLocalStore.brand.id);
                } else if (this.filterLocalStore.organization && this.filterLocalStore.organization.id) {
                    this.channel = this.pusher.subscribe('private-order.on.organization.' + this.filterLocalStore.organization.id);
                }
            }

            if (this.filterLocalStore) {
                if (this.filterLocalStore.outlet && this.filterLocalStore.outlet.id) {
                    this.channel = this.pusher.subscribe('private-order.on.outlet.' + this.filterLocalStore.outlet.id);
                } else if (this.filterLocalStore.brand && this.filterLocalStore.brand.id) {
                    this.channel = this.pusher.subscribe('private-order.on.brand.' + this.filterLocalStore.brand.id);
                } else if (this.filterLocalStore.organization && this.filterLocalStore.organization.id) {
                    this.channel = this.pusher.subscribe('private-order.on.organization.' + this.filterLocalStore.organization.id);
                }
            } else {
                this.scopeService.getUserScope$()
                    .subscribe(
                        (userScope: UserScope) => {
                            this.pusher = new Pusher(environment.pusher.key, {
                                cluster: environment.pusher.cluster,
                                encrypted: true,
                                authEndpoint: `${environment.baseUrl}/api/v1/broadcasting/auth`,
                                auth: {
                                    params: {
                                        organization_id: userScope.organization ? userScope.organization.id : null,
                                        brand_id: userScope.brand ? userScope.brand.id : null,
                                        outlet_id: userScope.outlet ? userScope.outlet.id : null
                                    },
                                    headers: {
                                        Authorization: `Bearer ${accessToken}`
                                    }
                                }
                            });
                            if (userScope.outlet && userScope.outlet.id) {
                                this.channel = this.pusher.subscribe('private-order.on.outlet.' + userScope.outlet.id);
                            } else if (userScope.brand && userScope.brand.id) {
                                this.channel = this.pusher.subscribe('private-order.on.brand.' + userScope.brand.id);
                            } else if (userScope.organization && userScope.organization.id) {
                                this.channel = this.pusher.subscribe('private-order.on.organization.' + userScope.organization.id);
                            }
                        }
                    );
            }
        }
    }

    subscribe(channel: string) {
        return this.pusher.subscribe(channel);
    }

    unsubscribe(channel: string) {
        return this.pusher.unsubscribe(channel);
    }
}
