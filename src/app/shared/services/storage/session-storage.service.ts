import {Injectable} from '@angular/core';
import {SessionStorageService} from 'ngx-webstorage';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CxiSessionStorageService {

        constructor(protected sessionStorage: SessionStorageService) {
    }

    retrieve(key: string): any {
        return this.sessionStorage.retrieve(key);
    }

    store(key: string, value: any): void {
        return this.sessionStorage.store(key, value);
    }

    clear(key: string): void {
        return this.sessionStorage.clear(key);
    }

    observe(key: string): Observable<any> {
        return this.sessionStorage.observe(key);
    }
}
