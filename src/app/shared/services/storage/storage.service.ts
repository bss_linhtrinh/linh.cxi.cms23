import {Injectable} from '@angular/core';
import {LocalStorageService} from 'ngx-webstorage';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    constructor(
        protected localStorageService: LocalStorageService
    ) {
    }

    retrieve(key: string): any {
        return this.localStorageService.retrieve(key);
    }

    store(key: string, value: any): void {
        return this.localStorageService.store(key, value);
    }

    clear(key: string): void {
        return this.localStorageService.clear(key);
    }

    observe(key: string): Observable<any> {
        return this.localStorageService.observe(key);
    }
}
