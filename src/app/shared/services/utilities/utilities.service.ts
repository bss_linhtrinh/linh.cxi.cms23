import {Injectable} from '@angular/core';
import * as pluralize from 'pluralize';
import * as _ from 'lodash';
import {environment} from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class UtilitiesService {

    constructor() {
    }

    // RANDOMIZE

    characters = `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%&*=`;

    upperCharacters = `ABCDEFGHIJKLMNOPQRSTUVWXYZ`;
    lowerCharacters = `abcdefghijklmnopqrstuvwxyz`;
    numberCharacters = `0123456789`;
    specialCharacters = `!@#$%&*=`;
    codeCharacters = `ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789`;

    public randomInt(min: number, max: number) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    public randomChar(characters?: string) {
        if (!characters) {
            characters = this.characters;
        }
        return characters.charAt(this.randomInt(1, characters.length));
    }

    public randomString(length: number = 16) {
        let result = '';

        for (let i = 0; i < length; i++) {
            result += this.randomChar();
        }

        return result;
    }

    public randomCode(length: number = 8) {
        let result = '';

        for (let i = result.length; i < length; i++) {
            result += this.randomChar(this.codeCharacters);
        }

        return result;
    }

    public randomPassword(length: number = 8) {
        let result = '';

        result += this.randomChar(this.upperCharacters);
        result += this.randomChar(this.lowerCharacters);
        result += this.randomChar(this.numberCharacters);
        result += this.randomChar(this.specialCharacters);

        for (let i = result.length; i < length; i++) {
            result += this.randomChar();
        }

        // shuffle
        result = this.shuffle(result);

        return result;
    }

    public randomCodeEventBooking(length: number = 7) {
        let result = 'EVT';
        let number = '';


        for (let i = number.length; i < length; i++) {
            number += this.randomChar(this.numberCharacters);
        }

        // shuffle
        result = result + number;

        return result;
    }

    public randomCodeTicketBooking(length: number = 7) {
        let result = 'TKT';
        let number = '';

        for (let i = number.length; i < length; i++) {
            number += this.randomChar(this.numberCharacters);
        }

        // shuffle
        result = result + number;

        return result;
    }

    public shuffle(input: string): string {
        let a = input.split(''),
            n = a.length;

        for (let i = n - 1; i > 0; i--) {
            const j = this.randomInt(0, i);

            const tmp = a[i];
            a[i] = a[j];
            a[j] = tmp;
        }

        return a.join('');
    }

    // STRING

    public ChangeToSlug(title: string) {
        let slug = '';
        slug = title.toLowerCase();
        slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
        slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
        slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
        slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
        slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
        slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
        slug = slug.replace(/đ/gi, 'd');
        slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
        slug = slug.replace(/ /gi, '-');
        slug = slug.replace(/\-\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-/gi, '-');
        slug = slug.replace(/\-\-/gi, '-');
        slug = '@' + slug + '@';
        slug = slug.replace(/\@\-|\-\@|\@/gi, '');
        return slug;
    }

    public makeTitle(text: string): string {
        if (typeof text === 'string' && text !== '') {
            return text
                .replace(/[-_]/g, ' ')
                .split(' ')
                .map((word) => this.capitalize(word))
                .join(' ');
        }
        return text;
    }

    public singularize(str: string = '') {
        str = pluralize.singular(str);

        return str;
    }

    /**
     * capitalize word
     *
     * @param word
     */
    public capitalize(word: string): string {
        if (typeof word !== 'string') {
            return '';
        }
        return word.charAt(0).toUpperCase() + word.slice(1);
    }


    // OBJECTS , DATA
    public cloneDeep(data: any): any {
        return _.cloneDeep(data);
    }

    public isEqual(a: any, b: any): boolean {
        return _.isEqual(a, b);
    }

    // PARSE DATA
    boolval(v: any): boolean {
        if (typeof v === 'number') {
            return v !== 0;
        } else if (typeof v === 'string') {
            return v !== '0';
        }
        return !!v;
    }

    intval(v: any) {
        return parseInt(v, 10);
    }


    // LISTS
    removeItem(list: any[], condition: (item: any) => boolean) {
        return _.remove(list, condition);
    }


    // DATE
    parseDate(date: string | Date) {
        const d = new Date(date);
        let month = '' + (d.getMonth() + 1);
        let day = '' + d.getDate();
        const year = d.getFullYear();

        if (month.length < 2) {
            month = '0' + month;
        }
        if (day.length < 2) {
            day = '0' + day;
        }

        return [year, month, day].join('-');
    }


    // IMAGE
    basePathImage(path: string): string {
        let outputPath: string;
        if (path == null) {
            outputPath = '/assets/img/avatar-default.jpg';
        } else {
            if (path.includes('data:image/jpeg;')) {
                outputPath = path;
            } else {
                outputPath = `${environment.baseUrl}${path}`;
            }
        }
        return outputPath;
    }


    // FORM DATA
    parseToFormData(data: any) {
        let formData: FormData = new FormData();
        if (data.items && data.items.length > 0) {
            data.items.forEach(item => {
                item.items.forEach((partner, j) => {
                    if (partner.file && partner.file.length > 0 && typeof partner.file !== 'string') {
                        partner.file.forEach(i => {
                            if (i instanceof File) {
                                formData.append('file-' + j, i, i.name);
                            }
                            partner.file = 'file-' + j;
                        });
                    }
                });

            });
        }
        if (data instanceof Object) {
            Object.keys(data).forEach((key: any) => {
                const value = data[key];
                console.log(typeof value);
                if (typeof value === 'string') {
                    formData.append(key, value);
                } else {
                    formData.append(key, JSON.stringify(value));
                }
            });
        }
        return formData;
    }

    // COLOR
    lightenDarkenColor(col, amt) {

        let usePound = false;

        if (col[0] === '#') {
            col = col.slice(1);
            usePound = true;
        }

        const num = parseInt(col, 16);

        let r = (num >> 16) + amt;

        if (r > 255) {
            r = 255;
        } else if (r < 0) {
            r = 0;
        }

        let b = ((num >> 8) & 0x00FF) + amt;

        if (b > 255) {
            b = 255;
        } else if (b < 0) {
            b = 0;
        }

        let g = (num & 0x0000FF) + amt;

        if (g > 255) {
            g = 255;
        } else if (g < 0) {
            g = 0;
        }

        return (usePound ? '#' : '') + (g | (b << 8) | (r << 16)).toString(16);

    }
}
