import {Injectable} from '@angular/core';
import {ApiService} from '../api/api.service';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {StorageService} from '../storage/storage.service';
import {Provider} from '../../types/providers';
import {AuthService} from '../auth/auth.service';
import {ModulesService} from '../modules/modules.service';

@Injectable({
    providedIn: 'root'
})
export class CheckDomainService {

    constructor(
        private apiService: ApiService,
        private authService: AuthService,
        private storageService: StorageService,
        private modulesService: ModulesService
    ) {
    }

    check(): Observable<boolean> {
        const domain = window.location.hostname;
        const params = {
            domain: domain
        };

        const checkDomain = this.storageService.retrieve(domain);
        if (checkDomain) {
            return of(true);
        } else {
            return this.apiService.get('check', params)
                .pipe(
                    map(res => res.data),
                    tap((result: any) => {
                        if (result) {
                            if (result.app_id) {
                                this.storageService.store('system.app_id', result.app_id);
                            }
                            this.storageService.store('system.is_domain_bss', !!result.is_domain_bss);
                            this.storageService.store(domain, true);
                            this.authService.setCurrentProvider(result.is_domain_bss ? Provider.SuperAdmin : Provider.Admin);
                        }

                        this.modulesService.loadMenuItems()
                            .subscribe();
                    }),
                    map(() => true),
                    catchError(() => {
                        return of(false);
                    }),
                );
        }
    }

    isCxiDomain$(): Observable<boolean> {
        const is_domain_bss = this.storageService.retrieve('system.is_domain_bss');
        if (typeof is_domain_bss !== 'undefined') {
            return of(!!is_domain_bss);
        }

        return this.check();
    }

    isCxiDomain(): boolean {
        const is_domain_bss = this.storageService.retrieve('system.is_domain_bss');
        if (typeof is_domain_bss !== 'undefined') {
            return !!is_domain_bss;
        }
        return false;
    }
}
