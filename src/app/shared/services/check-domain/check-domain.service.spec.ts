import { TestBed } from '@angular/core/testing';

import { CheckDomainService } from './check-domain.service';

describe('CheckDomainService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CheckDomainService = TestBed.get(CheckDomainService);
    expect(service).toBeTruthy();
  });
});
