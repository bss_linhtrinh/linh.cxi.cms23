import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ApiService} from '../api/api.service';
import {map, tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ReportService {

    constructor(
        private apiService: ApiService
    ) {
    }

    getOrdersSummarize(params: object): Observable<any> {
        return this.apiService.get(`admin/report/order-summarize`, params)
            .pipe(
                map(res => res.data),
            );
    }

    getDeliveryManagementStatistics(params: object): Observable<any> {
        return this.apiService.get(`admin/report/delivery-management-statistics`, params)
            .pipe(
                map(res => res.data),
            );
    }

    getDeliveryManagementStatisticsForCharts(params: object): Observable<any> {
        return this.apiService.get(`admin/report/delivery-management-statistics-for-chart`, params)
            .pipe(
                map(res => res.data),
            );
    }

    getLoyaltyProgramStatistics(params: object): Observable<any> {
        return this.apiService.get(`admin/report/loyalty-program-statistics`, params)
            .pipe(
                map(res => res.data),
            );
    }
}
