import {Injectable} from '@angular/core';
import {LocalStorageService} from 'ngx-webstorage';

@Injectable()
export class FilterStorageService {

    constructor(
        protected localStorageService: LocalStorageService
    ) {
    }

    key = 'filter';

    retrieve(): any {
        return this.localStorageService.retrieve(this.key);
    }

    store(value: any): void {
        if (value.organization && value.organization.id) {
            this.localStorageService.store(`${this.key}.organization_id`, value.organization.id);
        }
        if (value.brand && value.brand.id) {
            this.localStorageService.store(`${this.key}.brand_id`, value.brand.id);
        }
        if (value.outlet && value.outlet.id) {
            this.localStorageService.store(`${this.key}.outlet_id`, value.outlet.id);
        }
        return this.localStorageService.store(this.key, value);
    }
}
