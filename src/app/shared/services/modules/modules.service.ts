import {Injectable} from '@angular/core';
import {Levels} from '../../types/levels';
import {Provider} from '../../types/providers';
import {MenuItem} from '../../entities/menu-item';
import {StorageService} from '../storage/storage.service';
import {environment} from '../../../../environments/environment';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ModulesService {
    currentUser: any = null;
    level: any = null;
    filter: any = null;

    constructor(
        private storage: StorageService,
        private http: HttpClient
    ) {
    }

    loadMenuItems() {
        const auth_provider = this.storage.retrieve(`auth.provider`);
        let menuItems$: any;

        switch (auth_provider) {
            case Provider.SuperAdmin:
                menuItems$ = this.loadCxiConfig$();
                break;

            default:
            case Provider.Admin:
                menuItems$ = this.loadTenantConfig$();
                break;
        }

        return menuItems$;
    }

    loadCxiConfig$() {
        const protocol = window.location.protocol;
        const hostname = window.location.hostname;
        const port = window.location.port;

        const baseUrl = `${protocol}//${hostname}` + (port ? `:${port}` : ``);

        return this.http.get(`${baseUrl}/assets/json/cxi.json`)
            .pipe(
                tap((data) => this.storage.store(`system.config`, data)),
            );
    }

    loadTenantConfig$() {
        let app_id = localStorage.getItem(`cxi|system.app_id`);

        if (app_id) {
            app_id = app_id.replace(`"`, ``).replace(`"`, ``);
            const url = `${environment.baseUrl}/storage/${app_id}.json`;

            const remoteJsonSource = this.http.get(url, {responseType: 'json'})
                .pipe(
                    tap((data) => this.storage.store(`system.config`, data))
                );

            const protocol = window.location.protocol;
            const hostname = window.location.hostname;
            const port = window.location.port;
            const baseUrl = (`${protocol}//${hostname}`) + (port ? `:${port}` : ``);

            const localJsonSource = this.http.get(`${baseUrl}/assets/json/${app_id}.json`)
                .pipe(
                    tap((data) => this.storage.store(`system.config`, data)),
                );

            return localJsonSource
                .pipe(
                    catchError(
                        (e) => remoteJsonSource
                    )
                );
        } else {
            return of(false);
        }
    }

    // getJson(url: string): Observable<any> {
    //     return new Observable((observer) => {
    //         try {
    //             $.getJSON(
    //                 url,
    //                 function (data) {
    //                     console.log(data);
    //                     observer.next(data);
    //                 },
    //                 function () {
    //                     throw 1;
    //                 }
    //             );
    //         } catch (e) {
    //             throw e;
    //         }
    //     });
    // }

    getFromStorage(): any[] {
        const systemConfigParse = this.storage.retrieve(`system.config`);

        let modules: any[];
        if (systemConfigParse &&
            systemConfigParse.modules &&
            systemConfigParse.modules.length) {
            modules = systemConfigParse.modules;
        }

        return modules;
    }

    getMenuItems(currentUser?: any, filter?: any): MenuItem[] {
        let menuItems: MenuItem[];
        this.currentUser = currentUser;
        this.filter = filter;

        // if (!module) {
        //     module = Modules.OrganizationManagement;
        // }

        // levels
        let level: Levels;
        if (currentUser) {
            if (currentUser.provider === Provider.SuperAdmin) {
                if (!filter || !(filter.organization && filter.organization.id)) {
                    level = Levels.BSS;
                } else {
                    if (filter.outlet && filter.outlet.id) {
                        level = Levels.Outlet;
                    } else if (filter.brand && filter.brand.id) {
                        level = Levels.Brand;
                    } else if (filter.organization && filter.organization.id) {
                        level = Levels.Organization;
                    }
                }
            } else if (currentUser.provider === Provider.Admin) {
                if (currentUser.outlet && currentUser.outlet.id) {
                    level = Levels.Outlet;
                } else if (currentUser.brand && currentUser.brand.id) {
                    if (filter && filter.outlet && filter.outlet.id) {
                        level = Levels.Outlet;
                    } else {
                        level = Levels.Brand;
                    }
                } else if (currentUser.organization && currentUser.organization.id) {
                    if (filter && filter.outlet && filter.outlet.id) {
                        level = Levels.Outlet;
                    } else if (filter && filter.brand && filter.brand.id) {
                        level = Levels.Brand;
                    } else {
                        level = Levels.Organization;
                    }
                }
            }
        }
        this.level = level;

        const MENU_ITEMS = this.getFromStorage();

        menuItems = MENU_ITEMS
            .map(
                m => MenuItem.create(m)
            );

        if ((currentUser && currentUser.provider) || level) {
            menuItems = menuItems
                .filter(
                    (menuItem: MenuItem) => this.filterMenuItem(menuItem, currentUser, filter, level)
                );
        }

        return menuItems;
    }

    filterMenuItem(menuItem: MenuItem, currentUser?: any, filter?: any, level?: any): boolean {
        let isActivate = true;

        // parent: filter provider
        if (currentUser &&
            currentUser.provider) {
            isActivate = !!isActivate
                && menuItem.allow_providers
                && menuItem.allow_providers.includes(currentUser.provider);
        }

        // parent: filter level
        if (level) {
            isActivate = !!isActivate
                && menuItem.allow_levels
                && menuItem.allow_levels.includes(this.level);
        }

        if (filter) {
            isActivate = !!isActivate
                && menuItem.allow_types
                && menuItem.allow_types.includes(filter.brand.scope);
        }

        // children
        if (
            isActivate &&
            menuItem.children &&
            menuItem.children.length > 0
        ) {
            menuItem.children = menuItem.children.filter((c: MenuItem) => {
                let isActivateChild = true;

                // filter provider
                if (currentUser &&
                    currentUser.provider) {
                    isActivateChild = isActivateChild
                        && c.allow_providers
                        && c.allow_providers.includes(currentUser.provider);
                }

                // filter level
                if (level) {
                    isActivateChild = isActivateChild
                        && c.allow_levels
                        && c.allow_levels.includes(this.level);
                }

                if (filter) {
                    isActivateChild = isActivateChild
                        && c.allow_types
                        && c.allow_types.includes(filter.brand.scope);
                }

                return isActivateChild;
            });
        }

        return isActivate;
    }
}
