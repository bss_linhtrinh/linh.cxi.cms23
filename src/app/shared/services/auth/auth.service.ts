import {Injectable} from '@angular/core';
import {User} from '../../entities/user';
import {catchError, map, tap} from 'rxjs/operators';
import {ApiService} from '../api/api.service';
import {Observable, of, Subject} from 'rxjs';
import {StorageService} from '../storage/storage.service';
import {CookieService} from 'ngx-cookie-service';
import {Provider} from '../../types/providers';
import {Levels} from '../../types/levels';
import {FilterStorageService} from '../filterStorage/filterStorage.service';
import {LocalStorage, SessionStorageService} from 'ngx-webstorage';
import {Scope} from '../../types/scopes';
import {BrandScope} from '../../types/brand-scope';
import {NotificationService} from '../noitification/notification.service';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private isAuthorisedSource = new Subject<boolean>();
    isAuthorised$ = this.isAuthorisedSource.asObservable();

    @LocalStorage('auth.provider') provider: Provider;
    @LocalStorage('auth.brandScope') brandScope: BrandScope;

    constructor(
        private router: Router,
        private apiService: ApiService,
        private cookieService: CookieService,
        private storageService: StorageService,
        private sessionStorageService: SessionStorageService,
        private filterStorageService: FilterStorageService,
        private notificationService: NotificationService
    ) {
    }

    currentUser(): User {
        let user: User = null;
        const storageUser = this.storageService.retrieve('user');
        if (storageUser) {
            user = (new User()).fill(storageUser);
        }
        return user;
    }

    currentUser$(): Observable<User> | null {
        const user = this.currentUser();
        return of(user);
    }

    getCurrentFilter() {
        return this.filterStorageService.retrieve();
    }

    getCurrentBrandScope() {
        let brandScope: BrandScope = null;
        const filter = this.getCurrentFilter();
        if (filter &&
            filter.brand &&
            filter.brand.scope) {
            brandScope = filter.brand.scope === 'fb'
                ? BrandScope.FB
                : filter.brand.scope === 'booking'
                    ? BrandScope.Booking
                    : null;
        }
        this.brandScope = brandScope;
        return brandScope;
    }

    getCurrentProvider(): Provider {
        return this.provider;
    }

    setCurrentProvider(provider: any) {
        const parseProvider = (provider === 'super-admin')
            ? Provider.SuperAdmin
            : provider === 'admins'
                ? Provider.Admin
                : Provider.SuperAdmin;
        return this.provider = parseProvider;
    }

    getCurrentLevel(isFilter?: boolean): Levels {
        let level: Levels;

        const currentUser = this.currentUser();
        const filter = this.getCurrentFilter();
        const provider = this.getCurrentProvider();

        if (typeof isFilter === 'undefined') {
            isFilter = true;
        }

        if (!currentUser) {
            return null;
        }

        if (provider === Provider.SuperAdmin) {
            level = Levels.BSS;
            if (isFilter && filter) {
                if (filter.outlet && filter.outlet.id) {
                    level = Levels.Outlet;
                } else if (filter.brand && filter.brand.id) {
                    level = Levels.Brand;
                } else if (filter.organization && filter.organization.id) {
                    level = Levels.Organization;
                }
            }
        } else if (provider === Provider.Admin) {
            if (currentUser.outlet && currentUser.outlet.id) {
                level = Levels.Outlet;
            } else if (currentUser.brand && currentUser.brand.id) {
                level = Levels.Brand;
                if (isFilter && filter) {
                    if (filter.outlet && filter.outlet.id) {
                        level = Levels.Outlet;
                    }
                }
            } else if (currentUser.organization && currentUser.organization.id) {
                level = Levels.Organization;
                if (isFilter && filter) {
                    if (filter.outlet && filter.outlet.id) {
                        level = Levels.Outlet;
                    } else if (filter.brand && filter.brand.id) {
                        level = Levels.Brand;
                    }
                }
            }
        }
        return level;
    }

    getCurrentScope(isFilter?: boolean): Scope {
        let scope: Scope;

        const currentUser = this.currentUser();
        const provider = this.getCurrentProvider();
        const filter = this.getCurrentFilter();


        if (typeof isFilter === 'undefined') {
            isFilter = true;
        }

        if (!currentUser) {
            scope = null;
        } else {
            switch (provider) {
                case Provider.SuperAdmin:
                    scope = Scope.BSS;
                    if (isFilter && filter) {
                        if (filter.outlet && filter.outlet.id) {
                            scope = Scope.Outlet;
                        } else if (filter.brand && filter.brand.id) {
                            scope = Scope.Brand;
                        } else if (filter.organization && filter.organization.id) {
                            scope = Scope.Organization;
                        }
                    }
                    break;

                case Provider.Admin:
                    if (currentUser.outlet && currentUser.outlet.id) {
                        scope = Scope.Outlet;
                    } else if (currentUser.brand && currentUser.brand.id) {
                        scope = Scope.Brand;
                        if (isFilter && filter) {
                            if (filter.outlet && filter.outlet.id) {
                                scope = Scope.Outlet;
                            }
                        }
                    } else if (currentUser.organization && currentUser.organization.id) {
                        scope = Scope.Organization;
                        if (isFilter && filter) {
                            if (filter.outlet && filter.outlet.id) {
                                scope = Scope.Outlet;
                            } else if (filter.brand && filter.brand.id) {
                                scope = Scope.Brand;
                            }
                        }
                    }
                    break;

                default:
                    scope = null;
                    break;
            }
        }

        return scope;
    }

    setAccessToken(data: any, provider?: Provider) {
        const access_token = data.access_token;
        const refresh_token = data.refresh_token;
        const expires_in = data.expires_in;
        const token_type = data.token_type;

        const expires = 720;
        const path = '/';
        const protocol = window.location.protocol;
        const domain = window.location.hostname;
        const secure = (protocol === 'https:');
        const sameSite = 'Strict';

        const prefixProvider = (provider === Provider.SuperAdmin)
            ? 'SA'
            : (provider === Provider.Admin)
                ? 'A'
                : '';

        if (access_token) {
            this.cookieService.set(`${prefixProvider}_access_token`, access_token, expires, path, domain, secure, sameSite);
            this.cookieService.set(`${prefixProvider}_refresh_token`, refresh_token, expires, path, domain, secure, sameSite);
            this.cookieService.set(`${prefixProvider}_expires_in`, expires_in, expires, path, domain, secure, sameSite);
            this.cookieService.set(`${prefixProvider}_token_type`, token_type, expires, path, domain, secure, sameSite);
        }
    }

    getAccessToken(provider?: Provider) {
        if (!provider) {
            return null;
        }

        const prefixProvider = (provider === Provider.SuperAdmin)
            ? 'SA'
            : (provider === Provider.Admin)
                ? 'A'
                : '';

        return this.cookieService.get(`${prefixProvider}_access_token`);
    }

    login(data: { username: string, password: string }) {
        const provider = this.getCurrentProvider();
        const signInData = {
            username: data.username,
            password: data.password,
            provider: provider
        };

        return this.apiService.post('oauth/sign-in', signInData)
            .pipe(
                tap(
                    (response) => this.handleLoginSuccess(response, provider)
                ),
                tap(() => this.isAuthorisedSource.next(true))
            );
    }

    handleLoginSuccess(data, provider: Provider) {
        if (data && data.access_token) {
            this.setAccessToken(data, provider);
        }
    }

    // signOut
    signOut(): Observable<any> {
        // call api Sign out here...

        return new Observable((observer) => {

            this.handleAfterSignOutClearStorage();

            observer.next();
        });
    }

    handleAfterSignOutClearStorage() {
        const path = '/';
        const domain = window.location.hostname;

        this.cookieService.deleteAll();
        this.cookieService.deleteAll(path, domain);

        this.storageService.clear('filter');
        this.storageService.clear('role');
        this.storageService.clear('user');
        this.storageService.clear('auth.provider');
        this.storageService.clear('auth.brandScope');

        this.storageService.clear('fg.email');
        this.storageService.clear('fg.is_resend');
        this.storageService.clear('fg.provider');
        this.storageService.clear('fg.resend_countdown');

        this.storageService.clear('scope.organization_id');
        this.storageService.clear('scope.brand_id');
        this.storageService.clear('scope.outlet_id');
        this.storageService.clear('scope.organization');
        this.storageService.clear('scope.brand');
        this.storageService.clear('scope.outlet');
        this.storageService.clear('scope.current');

        this.storageService.clear('dashboard.timeSelected');
        this.storageService.clear('dashboard.dateStart');
        this.storageService.clear('dashboard.dateEnd');

        this.storageService.clear('pagination.current_page');
        this.storageService.clear('pagination.per_page');
        this.storageService.clear('pagination.total');
        this.storageService.clear('pagination.from');
        this.storageService.clear('pagination.to');

        this.storageService.clear('filter');
        this.storageService.clear('filter.organization_id');
        this.storageService.clear('filter.brand_id');
        this.storageService.clear('filter.outlet_id');

        this.storageService.clear('settings.order');


        // sessionStorage clear ALL
        sessionStorage.clear();
    }

    forgotPassword(data?: Object): Observable<any> {
        return this.apiService.get('password/reset', data);
    }

    forgotPasswordSuperAdmin(data?: Object): Observable<any> {
        return this.apiService.post(`forgot-password`, data);
    }

    resetPasswordSuperAdmin(data?: Object): Observable<any> {
        return this.apiService.post(`reset-password`, data);
    }

    authorized(): Observable<User> {
        return this.apiService.get('authorized')
            .pipe(
                map((res) => res.data),
                map((data) => new User().fill(data)),
                tap(
                    (user: User) => {
                        this.storageService.store('user', user);
                        // this.storageService.store('provider', user.provider);
                        this.setCurrentProvider(user.provider);
                        this.storageService.store('role', user.role);

                        this.getCurrentBrandScope();
                    }
                ),
                catchError(
                    (error) => {
                        this.notificationService.error(`Unauthorised`);
                        throw error;
                    }
                )
            );
    }
}
