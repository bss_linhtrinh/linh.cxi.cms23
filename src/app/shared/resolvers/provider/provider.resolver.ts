import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable, of} from 'rxjs';
import {Provider} from '../../types/providers';
import {AuthService} from '../../services/auth/auth.service';
import {CheckDomainService} from '../../services/check-domain/check-domain.service';
import {switchMap, tap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ProviderResolver implements Resolve<any> {

    constructor(
        private authService: AuthService,
        private checkDomainService: CheckDomainService
    ) {
    }

    resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
        return this.checkDomainService.isCxiDomain$()
            .pipe(
                tap(
                    (is_domain_bss: boolean) => {
                        const provider = is_domain_bss ? Provider.SuperAdmin : Provider.Admin;
                        this.authService.setCurrentProvider(provider);
                    }
                ),
                switchMap(
                    () => {
                        const authProvider = this.authService.getCurrentProvider();
                        return of(authProvider);
                    }
                )
            );
    }
}
