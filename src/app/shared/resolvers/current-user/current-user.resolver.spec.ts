import { TestBed, async, inject } from '@angular/core/testing';

import { CurrentUserResolver } from './current-user.resolver';

describe('CurrentUserResolver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CurrentUserResolver]
    });
  });

  it('should ...', inject([CurrentUserResolver], (guard: CurrentUserResolver) => {
    expect(guard).toBeTruthy();
  }));
});
