import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from 'rxjs';
import {StorageService} from '../../services/storage/storage.service';

@Injectable({
    providedIn: 'root'
})
export class CurrentUserResolver implements Resolve<any> {
    constructor(private storageService: StorageService) {
    }

    resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
        return new Observable((observer) => {
            const user = this.storageService.retrieve('user');
            observer.next(user);
            observer.complete();
        });
    }
}
