import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from 'rxjs';
import {SettingsRepository} from '../../repositories/settings.repository';

@Injectable({
    providedIn: 'root'
})
export class SettingResolver implements Resolve<any> {

    constructor(
        private settingsRepository: SettingsRepository
    ) {
    }

    resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
        return this.settingsRepository.all({scope: 'brands'});
    }
}
