import {BaseEntity} from './base.entity';
import {BookingTicket} from './booking-ticket';
import {CustomerBooking} from './customer-booking';
import {formatDate} from '@angular/common';
import * as _ from 'lodash';

export class Booking extends BaseEntity {
    booking_type: string;
    resource_id: string;
    customer_id: number;
    customer_first_name: string;
    customer_last_name: string;
    customer_phone: string;
    customer_email: string;
    register_type: string;
    ticket_type: string;
    sub_register_type: string;
    tickets: BookingTicket[];
    party: CustomerBooking[];
    company_name: string;
    company_address: string;
    company_phone: string;
    company_website: string;
    company_tax_code: string;
    company_type: string;
    company_product: string;
    representative_name: string;
    representative_email: string;
    representative_position: string;
    representative_mobile: string;
    ticket_apply: any;
    items: any;


    constructor() {
        super();
        this.booking_type = 'event';
        this.resource_id = null;
        this.customer_id = null;
        this.customer_first_name = null;
        this.customer_last_name = null;
        this.customer_phone = null;
        this.customer_email = null;
        this.register_type = 'visitor';
        this.sub_register_type = null;
        this.ticket_type = 'Individual';
        this.tickets = null;
        this.party = [];
        this.company_name = null;
        this.company_address = null;
        this.company_phone = null;
        this.company_website = null;
        this.company_tax_code = null;
        this.company_type = null;
        this.company_product = null;
        this.representative_name = null;
        this.representative_email = null;
        this.representative_position = null;
        this.representative_mobile = null;
        this.ticket_apply = [];
        this.items = [];
    }

    static createAll(data: any[]) {
        return data.map(c => this.create(c));
    }

    static create(data: any) {
        return (new Booking()).fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        if (clonedObj.ticket_type === 'Individual') {
            clonedObj.sub_register_type = 'personal';
        }
        if (clonedObj.ticket_type === 'Group') {
            clonedObj.sub_register_type = 'group';
        }
        if (clonedObj.party && clonedObj.party.length > 0) {
            clonedObj.party.forEach(item => {
                if (!item.customer_first_name) {
                    item.customer_first_name = clonedObj.customer_first_name;
                }
                if (!item.customer_last_name) {
                    item.customer_last_name = clonedObj.customer_last_name;
                }
                if (!item.customer_email) {
                    item.customer_email = clonedObj.customer_email;
                }
                if (!item.customer_phone) {
                    item.customer_phone = clonedObj.customer_phone;
                }
            });
        }
        if (clonedObj.ticket_apply && clonedObj.ticket_apply.length > 0) {
            let count = 0;
            clonedObj.ticket_apply.forEach(ticket => {
                count = ticket.quantity;
                if (clonedObj.ticket_type === 'Individual') {
                    clonedObj.items.push({
                        product_id: ticket.ticketId,
                        items: this.forLoop(ticket.quantity, clonedObj.party)
                    });
                }
                if (clonedObj.ticket_type === 'Group') {
                    clonedObj.items.push({
                        product_id: ticket.ticketId,
                        items: clonedObj.party
                    });
                }

            });
        }
        if (!clonedObj.customer_id) {
            delete clonedObj.customer_id;
        }
        delete clonedObj.booking_type;
        delete clonedObj.ticket_apply;
        delete clonedObj.party;
        delete clonedObj.ticket_type;
        delete clonedObj.tickets;

        return clonedObj;
    }

    forLoop(quantity, value) {
        var items = [];
        for (var i = 1; i <= quantity; i++) {
            items.push(value[0]);
        }
        return items;
    }

}
