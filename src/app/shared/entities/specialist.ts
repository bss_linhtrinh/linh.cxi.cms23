import { BaseEntity } from './base.entity';
import {Image} from './image';
import * as _ from 'lodash';

export class Specialist extends BaseEntity {
    id: number;
    name: string;
    slug: string;
    description: string;
    status: number;
    is_activated: boolean;
    image: any;
    brand_id: number;
    translations: any;
    en: object = {};
    vi: object = {};
    _images: Image[];

    get images(): Image[] {
        return this._images;
    }

    set images(images: Image[]) {
        this._images = images;
    }

    get base_image(): any {
        if (this.images && this.images.length > 0) {
            return this.images[0];
        }
        return null;
    }

    set base_image(image: any) {
        if (this.images) {
            this.images[0] = image;
        }
    }

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.slug = null;
        this.description = null;
        this.status = 1;
        this.is_activated = true;
        this.image = null;
        this.images = null;
        this.base_image = null;
        this.brand_id = null;
        this.en = {};
        this.vi = {};
        this.translations = [];
    }

    static create(data) {
        return new Specialist().fill(data);
    }

    static treelize(specialists: Specialist[]): Specialist[] {
        if (specialists) {
            specialists = specialists.sort(
                (a, b) => a.id - b.id
            );
        }
        return specialists;
    }

    fill(data): this {
        // images
        const images = [];
        if (data.image) {
            const image = Image.create(data.image);
            data.base_image = image;
            images.push(image);
        }
        this.images = images;

        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        // parse image
        if (clonedObj.images && clonedObj.images.length > 0) {
            if (clonedObj.images[0] && clonedObj.images[0].id) {
                clonedObj.image = clonedObj.images[0].id;
            }
        }

        return clonedObj;
    }

}
