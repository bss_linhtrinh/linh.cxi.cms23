import {BaseEntity} from './base.entity';

export class RolePermission extends BaseEntity {
    key: string;
    module: string;
    view: boolean;
    create: boolean;
    edit: boolean;
    delete: boolean;

    constructor() {
        super();
        this.key = null;
        this.module = null;
        this.view = null;
        this.create = null;
        this.edit = null;
        this.delete = null;
    }

    static create(data?: Object) {
        return (new RolePermission()).fill(data);
    }

    static init() {
        // return ROLE_PERMISSIONS.map(p => this.create(p));
        return [];
    }

    parse() {
        return {
            id: this.key,
            view: this.intval(this.view),
            create: this.intval(this.create),
            edit: this.intval(this.edit),
            delete: this.intval(this.delete),
        };
    }

    fill(data) {
        const self = this;

        self.key = data.id;
        self.module = data.module
            ? data.module
            : data.name
                ? data.name
                : '';
        self.view = this.boolval(data.view);
        self.create = this.boolval(data.create);
        self.edit = this.boolval(data.edit);
        self.delete = this.boolval(data.delete);

        return self;
    }

    boolval(v: any): boolean {
        if (typeof v === 'number') {
            return v === 0 ? false : true;
        } else if (typeof v === 'string') {
            return v === '0' ? false : true;
        }
        return !!v;
    }

    intval(v: any): number {
        return !!v ? 1 : 0;
    }
}
