import {BaseEntity} from './base.entity';
import {TicketBreaktime} from './ticket-breaktime';
import {ObjectCustom} from './object';
import * as _ from 'lodash';
import {formatDate} from '@angular/common';

export class TicketChildren extends BaseEntity {
    id: number;
    name: string;
    short_name: string;
    payment_type: string;
    type: string;
    limit: number;
    sold: number;
    price?: ObjectCustom | any;
    location: string;
    start_time: string;
    end_time: string;
    limit_registration: boolean;
    code: string;
    available: number;
    is_auto_approved: boolean;
    start_sold: string;
    haveChild: boolean;
    end_sold: string;
    is_unlimited: boolean;
    minimum_purchase: number;
    maximum_purchase: number;
    quantity: number;
    extend_data: any;
    // breakTimes: TicketBreaktime[];

    get money(): ObjectCustom {
        return this.price.value;
    }
    set money(price) {
        this.price = price;
    }

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.short_name = null;
        this.payment_type = 'costly';
        this.type = 'visitor';
        this.limit = 0;
        this.sold = 0;
        this.price = 0;
        this.location = null;
        this.start_time = null;
        this.end_time = null;
        this.limit_registration = true;
        this.code = '';
        this.available = 0;
        this.is_auto_approved = true;
        this.start_sold = null;
        this.haveChild = false;
        this.is_unlimited = true;
        this.minimum_purchase = 1;
        this.maximum_purchase = 10;
        this.quantity = 1;
        this.end_sold = null;
        this.extend_data = {
            break_times: [new TicketBreaktime()]
        };
    }

    static create(data) {
        return new TicketChildren().fill(data);
    }

    fill(data) {
        if (data.is_auto_approved === 0) {
            data.is_auto_approved = true;
        }
        if (data.is_auto_approved === 1) {
            data.is_auto_approved = false;
        }
        if (data.is_unlimited === 0) {
            data.is_unlimited = true;
        }
        if (data.is_unlimited === 1) {
            data.is_unlimited = false;
        }
        if (!data.minimum_purchase && !data.maximum_purchase) {
            data.limit_registration = false;
        }

        if (!data.quantity) {
            data.quantity = 1;
        }
        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        clonedObj.start_time = formatDate(clonedObj.start_time, 'yyyy-MM-dd', 'en-US').toString();
        clonedObj.end_time = formatDate(clonedObj.end_time, 'yyyy-MM-dd', 'en-US').toString();
        clonedObj.start_sold = formatDate(clonedObj.start_sold, 'yyyy-MM-dd', 'en-US').toString();
        clonedObj.end_sold = formatDate(clonedObj.end_sold, 'yyyy-MM-dd', 'en-US').toString();
        clonedObj.is_auto_approved = !clonedObj.is_auto_approved;
        clonedObj.is_unlimited = !clonedObj.is_unlimited;
        if (!clonedObj.limit_registration) {
            delete clonedObj.minimum_purchase;
            delete clonedObj.maximum_purchase;
        }
        if (!clonedObj.id) {
            delete clonedObj.id;
        }

        delete clonedObj.available;

        return clonedObj;
    }
}
