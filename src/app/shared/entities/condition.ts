import {BaseEntity} from './base.entity';

export class Condition extends BaseEntity {
    attribute: string;
    operator: string;
    value: any;

    constructor() {
        super();
        this.attribute = 'point';
        this.operator = '>';
        this.value = null;
    }

    parse() {
        return {
            attribute: this.attribute,
            operator: this.operator,
            value: this.value,
        };
    }
}
