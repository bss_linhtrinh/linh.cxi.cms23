import {BaseEntity} from './base.entity';
import {Organization} from './organization';
import {Brand} from './brand';
import {Outlet} from './outlets';

export class UserScope extends BaseEntity {
    organization: Organization;
    brand: Brand;
    outlet: Outlet;

    constructor() {
        super();
        this.organization = null;
        this.brand = null;
        this.outlet = null;
    }
}
