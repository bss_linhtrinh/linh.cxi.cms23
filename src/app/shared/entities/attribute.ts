import {BaseEntity} from './base.entity';
import {AttributeOption} from './attributeOption';
import {AttributeTranslation} from './attribute-translation';
import {Image} from './image';

export class Attribute extends BaseEntity {
    id: number;
    type: string;
    admin_name: string;
    description: string;
    code: string;
    is_required: 0;
    is_unique: 0;
    is_configurable: 0;
    options: AttributeOption[];
    translations: AttributeTranslation[];

    image: Image;
    image_id: number;

    get name(): string {
        return this.admin_name;
    }

    set name(name: string) {
        this.admin_name = name;
    }

    constructor() {
        super();
        this.id = null;
        this.type = null;
        this.admin_name = null;
        this.description = null;
        this.code = null;
        this.name = null;
        this.is_required = 0;
        this.is_unique = 0;
        this.is_configurable = 0;
        this.options = [];
        this.translations = [];

        this.image = null;
        this.image_id = null;
    }

    static create(data) {
        return new Attribute().fill(data);
    }

    fill(data) {
        if (data.admin_name) {
            this.name = data.admin_name;
        }

        if (data.image) {
            data.image = Image.create(data.image);
        }

        if (data.options && data.options.length > 0) {
            data.options = data.options
                .map(
                    opt => AttributeOption.create(opt)
                );
        }

        if (data.translations && data.translations.length > 0) {
            data.translations = data.translations
                .map(
                    translation => AttributeTranslation.create(translation)
                );
        }

        return super.fill(data);
    }

    parse() {
        return {
            id: this.id,
            type: this.type,
            admin_name: this.admin_name,
            description: this.description,
            code: this.code,
            is_required: this.is_required,
            is_unique: this.is_unique,
            is_configurable: this.is_configurable,
            options: this.options.map(option => option.parse()),
            translations: this.translations,

            image_id: (this.image && this.image.id)
                ? this.image.id
                : this.image_id
                    ? this.image_id
                    : null
        };
    }

}
