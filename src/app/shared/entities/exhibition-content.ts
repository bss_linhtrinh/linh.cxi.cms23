import {BaseEntity} from './base.entity';
import {Img} from './img';

export class ExhibitionContent extends BaseEntity {
    title: string;
    content: string;


    constructor() {
        super();
        this.title = null;
        this.content = null;
    }

    static create(data) {
        return new ExhibitionContent().fill(data);
    }
}
