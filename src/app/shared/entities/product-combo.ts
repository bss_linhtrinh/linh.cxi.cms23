import {BaseEntity} from './base.entity';
import {ProductComboItem} from './product-combo-item';

export class ProductCombo extends BaseEntity {
    id: number;
    name: string;
    price: number;
    percentage: number; // 0-100
    unit: string;
    items: ProductComboItem[];

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.price = 0;
        this.percentage = 0;
        this.unit = 'percentage';
        this.items = [new ProductComboItem()];
    }

    static create(data) {
        return new ProductCombo().fill(data);
    }

    fill(data): this {
        if (data.items) {
            data.items = data.items.map(item => ProductComboItem.create(item));
        }

        return super.fill(data);
    }

    parse() {
        return {
            id: this.id,
            name: this.name,
            price: this.price,
            percentage: this.percentage,
            unit: this.unit,
            items: this.items,
        };
    }
}
