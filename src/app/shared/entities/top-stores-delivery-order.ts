import { BaseEntity } from './base.entity';

export class topStoresDeliveryOrder extends BaseEntity {
    name: string;
    count: number;
    constructor() {
        super();
        this.name = null;
        this.count = null;
    }

    static create(data) {
        return new topStoresDeliveryOrder().fill(data);
    }
}
