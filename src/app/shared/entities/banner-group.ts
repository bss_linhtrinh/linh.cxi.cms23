import {BaseEntity} from './base.entity';
import * as _ from 'lodash';
import {BannerList} from './bannerList';

export class BannerGroup extends BaseEntity {
    bannerList: BannerList[];

    constructor() {
        super();
        this.bannerList = [new BannerList()];
    }

    static createAll(data: any[]) {
        return data.map(c => this.create(c));
    }

    static create(data) {
        return new BannerGroup().fill(data);
    }

    fill(data) {

        if (data.bannerList) {
            data.bannerList = data.bannerList.map(banner => BannerList.create(banner));
        }
        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        if (clonedObj.bannerList && clonedObj.bannerList.length > 0) {
            clonedObj.bannerList = clonedObj.bannerList.map(banner => banner.parse());
        }
        return clonedObj;
    }
}
