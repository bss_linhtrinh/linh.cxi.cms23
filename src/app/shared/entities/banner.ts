import {BaseEntity} from './base.entity';
import {BannerLink} from './banner-link';

export class Banner extends BaseEntity {
    id: number;
    brand_id: number;

    web_links: BannerLink[];
    mobile_links: BannerLink[];

    constructor() {
        super();
        this.id = null;
        this.brand_id = null;
        this.web_links = [new BannerLink()];
        this.mobile_links = [new BannerLink()];
    }

    static create(data): Banner {
        return (new Banner()).fill(data);
    }

    fill(data): this {
        if (data.web_links) {
            data.web_links = data.web_links.map(
                bannerLink => BannerLink.create(bannerLink)
            );
        }

        if (data.mobile_links) {
            data.mobile_links = data.mobile_links.map(
                bannerLink => BannerLink.create(bannerLink)
            );
        }

        return super.fill(data);
    }

    parse() {
        return {
            id: this.id,
            brand_id: this.brand_id ? this.brand_id : null,
            web_links: this.web_links ? this.web_links.map(link => link.parse()) : null,
            mobile_links: this.mobile_links ? this.mobile_links.map(link => link.parse()) : null
        };
    }
}
