import { BaseEntity } from './base.entity';

export class orderStaticForChartDeliveryApp extends BaseEntity {
    label: string;
    total: number;
    constructor() {
        super();
        this.label = null;
        this.total = null;
    }

    static create(data) {
        return new orderStaticForChartDeliveryApp().fill(data);
    }
}
