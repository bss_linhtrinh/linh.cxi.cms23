import { BaseEntity } from './base.entity';
import * as _ from 'lodash';
import { Image } from './image';

export class Tables extends BaseEntity {
    id: number;
    name: string;
    outlet_id: number;
    floor_id: number;
    image_id: number;
    table_category_id: number;
    brand_id: number;

    image: any;

    _images: Image[];

    get images(): Image[] {
        return this._images;
    }

    set images(images: Image[]) {
        this._images = images;
    }

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.outlet_id = null;
        this.floor_id = null;
        this.table_category_id = null;
        this.image_id = null;
        this.image = null;
        this.brand_id = null;
    }

    static create(data): Tables {
        return (new Tables()).fill(data);
    }
    fill(data): this {
        // images
        const images = [];
        if (data.image) {
            images.push(Image.create(data.image));
        }
        this.images = images;

        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        if (clonedObj.images && clonedObj.images.length > 0) {
            if (clonedObj.images[0] && clonedObj.images[0].id) {
                clonedObj.image = clonedObj.images[0];
                clonedObj.image_id = clonedObj.images[0].id;
            }
        }

        return clonedObj;
    }
}
