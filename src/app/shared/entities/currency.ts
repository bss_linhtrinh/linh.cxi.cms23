import {BaseEntity} from './base.entity';

export class Currency extends BaseEntity {
    id: number;
    supported_current_id: number;
    name: string;
    code: string;
    symbol: string;

    thousands_separator: string;
    decimal_places: number;
    decimal_separator: string;

    is_default: boolean;
    status: number;
    is_activated: boolean;
    brand_id: number;

    constructor() {
        super();
        this.id = null;
        this.supported_current_id = null;
        this.name = null;
        this.code = null;
        this.symbol = null;

        this.thousands_separator = ',';
        this.decimal_places = 0;
        this.decimal_separator = '.';

        this.is_default = null;
        this.status = 1;
        this.brand_id = null;
        this.is_activated = true;
    }

    static create(data) {
        return new Currency().fill(data);
    }

    fill(data?:any) {
        //return super.fill(data);
        // status => is_activated
        if (typeof data.status !== 'undefined') {
            data.is_activated = !!data.status;
        }
        return super.fill(data);
    }

    parse() {
        return {
            id: this.id,
            name: this.name,
            code: this.code,
            symbol: this.symbol,
            thousands_separator: this.thousands_separator,
            decimal_places: this.decimal_places,
            decimal_separator: this.decimal_separator,
            is_default: this.is_default,
            status: this.status,
            brand_id: this.brand_id,
        };
    }
}
