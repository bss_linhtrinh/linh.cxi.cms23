import { BaseEntity } from './base.entity';

export class CaseComment extends BaseEntity {
    id: number;
    content: string;
    updated_at: string;
    created_at: string;
    case_id: number;
    files: any;
    created_by: any;
    get name_comment(): string {
        if (!this.created_by.first_name) {
            return `${this.created_by.last_name}`;
        }

        if (!this.created_by.last_name) {
            return `${this.created_by.first_name}`;
        }
        if (!this.created_by) {
            return `--`;
        }
        return `${this.created_by.first_name} ${this.created_by.last_name}`;
    }
    constructor() {
        super();
        this.id = null;
        this.content = null;
        this.updated_at = null;
        this.created_at = null;
        this.files = [];
        this.created_by = null;
    }

    static create(data) {
        return new CaseComment().fill(data);
    }
    parse() {
        return {
            id: this.id,
            case_id: this.case_id,
            content: this.content,
            files: this.files,
        };
    }
}
