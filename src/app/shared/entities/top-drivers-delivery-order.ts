import { BaseEntity } from './base.entity';

export class topDriverDeliveryOrder extends BaseEntity {
    first_name: string;
    last_name: string;
    count: number;
    get name(): string {
        if (!this.first_name) {
            return `${this.last_name}`;
        }

        if (!this.last_name) {
            return `${this.first_name}`;
        }

        return `${this.first_name} ${this.last_name}`;
    }
    constructor() {
        super();
        this.first_name = null;
        this.last_name = null;
        this.count = null;
    }

    static create(data) {
        return new topDriverDeliveryOrder().fill(data);
    }
}
