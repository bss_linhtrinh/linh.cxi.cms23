import {BaseEntity} from './base.entity';
import {Img} from './img';
import {Image} from './image';
import * as _ from 'lodash';
import {SeatMapBenefit} from './seat-map-benefit';
import {SeatMapNote} from './seat-map-note';

export class SeatMapData extends BaseEntity {
    benefit: SeatMapBenefit[];
    note: SeatMapNote[];
    annottion: any;
    base64data: string;


    constructor() {
        super();
        this.benefit = [new SeatMapBenefit()];
        this.note = [new SeatMapNote()];
        this.annottion = null;
        this.base64data = null;
    }

    static create(data) {
        return new SeatMapData().fill(data);
    }

    fill(data) {

        if (data.base64data) {
            data.annottion.backgroundImage.src = data.base64data;
        }

        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);
        if (clonedObj.annottion) {
            const seatmap = clonedObj.annottion.toJSON(['hasControls',  'hasBorders' , 'name']);
            seatmap.backgroundImage.src = '';
            clonedObj.annottion = seatmap;
        }

        delete clonedObj.base64data;

        return clonedObj;
    }
}
