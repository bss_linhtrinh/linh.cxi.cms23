import {BaseEntity} from './base.entity';
import {Img} from './img';
import {ExhibitionContent} from './exhibition-content';
import {Image} from './image';
import * as _ from 'lodash';
import {formatDate} from '@angular/common';

export class Exhibition extends BaseEntity {
    id: number;
    data: ExhibitionContent[];
    note: ExhibitionContent[];
    image: Image[];
    image_ids: any;

    get images(): Image[] {
        return this.image;
    }

    set images(Images: Image[]) {
        this.image = Images;
    }


    constructor() {
        super();
        this.id = null;
        this.data = [new ExhibitionContent()];
        this.note = [new ExhibitionContent()];
        this.images = null;
        this.image_ids = null;
    }

    static create(data) {
        return new Exhibition().fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        if (clonedObj.images && clonedObj.images.length > 0) {
            if (clonedObj.images[0] && clonedObj.images[0].id) {
                clonedObj.image_ids.push(clonedObj.images[0].id);
            }
        }

        return clonedObj;
    }
}
