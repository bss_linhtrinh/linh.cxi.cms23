import {BaseEntity} from './base.entity';

export class MediaFile extends BaseEntity {
    id: number;
    filename: string;
    filesize: number;
    extension: string;
    folder_id: number;
    is_folder: boolean;
    media_type: string;
    mimetype: string;
    path: string;
    path_string: string;

    constructor() {
        super();
        this.id = null;
        this.filename = null;
        this.filesize = null;
        this.extension = null;
        this.folder_id = null;
        this.is_folder = null;
        this.media_type = null;
        this.mimetype = null;
        this.path = null;
        this.path_string = null;
    }
}
