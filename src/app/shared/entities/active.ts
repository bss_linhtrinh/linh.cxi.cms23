import {BaseEntity} from './base.entity';
import {active} from '../constants/active ';

export class Active extends BaseEntity {
    name: string;
    value: string;

    constructor() {
        super();
        this.name = null;
        this.value = null;
    }

    static create(data): Active {
        return (new Active()).fill(data);
    }

    static init(): Active[] {
        let ac: Active[];

        ac = active.map(g => this.create(g));

        return ac;
    }
}
