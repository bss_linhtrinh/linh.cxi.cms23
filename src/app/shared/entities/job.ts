import {BaseEntity} from './base.entity';
import {Image} from './image';

export class Job extends BaseEntity {
    id: number;
    jobId: number;
    billId: number;
    status?: object;
    images: [];
    isPaid: boolean;
    locations: [];
    activities: [];
    tickets: [];
    party: [];
    detail: object;
    resource: object;
    event: object;
    service: string;
    createdBy: object;
    quantity: number;
    estimation: object;
    earningAndPayment: object;
    createdAt: string;
    updatedAt: string;
    qrcode: string;
    method: string;


    constructor() {
        super();
        this.id = null;
        this.jobId = null;
        this.billId = null;
        this.status = null;
        this.images = null;
        this.isPaid = false;
        this.locations = null;
        this.activities = null;
        this.tickets = null;
        this.party = null;
        this.resource = null;
        this.detail = null;
        this.event = null;
        this.service = null;
        this.createdBy = null;
        this.quantity = null;
        this.estimation = null;
        this.earningAndPayment = null;
        this.createdAt = null;
        this.updatedAt = null;
        this.qrcode = null;
        this.method = null;
    }

    static createAll(data: any[]) {
        return data.map(c => this.create(c));
    }

    static create(data: any) {
        return (new Job()).fill(data);
    }

    fill(data): this {


        return super.fill(data);
    }

    parse() {
        return {
            id: this.id,
            jobId: this.jobId,
            billId: this.billId,
            status: this.status,
            images: this.images,
            isPaid: this.isPaid,
            locations: this.locations,
            activities: this.activities,
            tickets: this.tickets,
            party: this.party,
            event: this.event,
            service: this.service,
            createdBy: this.createdBy,
            quantity: this.quantity,
            estimation: this.estimation,
            createdAt: this.createdAt,
            updatedAt: this.updatedAt,
            qrcode: this.qrcode,
            method: this.method,
        };
    }
}
