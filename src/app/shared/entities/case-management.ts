import { BaseEntity } from './base.entity';

export class CasesManagement extends BaseEntity {
    id: number;
    name: string;
    status: string;
    ccs: string;
    priority: string;
    is_activated: number;
    following: string;
    expired_at: string;
    request: string;
    updated_at: string;
    created_at: string;
    responsible_parties: any;
    tags: string;
    customer_id: number;
    en: object = {};
    vi: object = {};
    translations: any;
    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.status = 'new';
        this.ccs = null;
        this.request = null;
        this.responsible_parties = [];
        this.priority = 'normal';
        this.following = null;
        this.tags = null;
        this.customer_id = null;
        this.expired_at = null;
        this.en = {};
        this.vi = {};
        this.translations = null;
        this.updated_at = null;
        this.created_at = null;
        this.is_activated = null;
    }

    static create(data) {
        return new CasesManagement().fill(data);
    }
}
