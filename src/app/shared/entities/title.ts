import {BaseEntity} from './base.entity';
import {formatDate} from '@angular/common';
import * as _ from 'lodash';

export class Title extends BaseEntity {
    en: string;
    vi: string;

    constructor() {
        super();
        this.en = '';
        this.vi = '';
    }

    static create(data) {
        return new Title().fill(data);
    }
}
