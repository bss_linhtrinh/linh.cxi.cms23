import { BaseEntity } from './base.entity';

export class ActivityLog extends BaseEntity {
    id: number;
    date: string;
    type: string;
    logger: number;
    value: string;
    constructor() {
        super();
        this.id = null;
        this.date = null;
        this.type = null;
        this.logger = null;
        this.value = null;
    }

    static create(data: any) {
        return new ActivityLog().fill(data);
    }

    fill(data): this {
        if (data.value) {
            data.value = JSON.stringify(data.value);
        }

        return super.fill(data);
    }
}
