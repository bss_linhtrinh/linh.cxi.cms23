import {BaseEntity} from './base.entity';
import {Condition} from './condition';

export class CustomerLevel extends BaseEntity {
    id: number;
    name: string;
    brand_id: number;
    description: string;
    conditions: Condition[][];
    previous_id: number;
    previous_level: Object;
    is_activated: boolean;

    get status() {
        if (this.is_activated) {
            return 'Apply';
        } else {
            return 'Deactivated';
        }
    }


    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.brand_id = null;
        this.description = null;
        this.conditions = [[new Condition()]];
        this.previous_id = null;
        this.previous_level = null;
        this.is_activated = false;
    }

    static createAll(data: any[]) {
        return data.map(c => this.create(c));
    }

    static create(data: any) {
        return (new CustomerLevel()).fill(data);
    }

    parse() {
        return {
            name: this.name,
            description: this.description,
            brand_id: this.brand_id,
            previous_id: this.previous_id,
            conditions: this.conditions.map(g => {
                return g.map(c => {
                    if (typeof c.parse === 'function') {
                        return c.parse();
                    }
                    return c;
                });
            })
        };
    }
}
