import {BaseEntity} from './base.entity';

export class OrderDriver extends BaseEntity {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    phone: string;
    latitude: number;
    longitude: number;
    outlet: string;

    get name(): string {
        if (!this.first_name) {
            return `${this.last_name}`;
        } else if (!this.last_name) {
            return `${this.first_name}`;
        } else {
            return `${this.first_name} ${this.last_name}`;
        }
    }

    set name(name: string) {
        const arrNames = name.split(' ');
        if (arrNames && arrNames.length > 0) {
            this.first_name = arrNames.shift();
            this.last_name = arrNames.join(' ');
        } else {
            this.first_name = name;
            this.last_name = null;
        }
    }

    constructor() {
        super();
        this.id = null;
        this.first_name = null;
        this.last_name = null;
        this.email = null;
        this.phone = null;
        this.latitude = null;
        this.longitude = null;
        this.outlet = null;
    }

    static create(data) {
        return new OrderDriver().fill(data);
    }

    fill(data) {
        return super.fill(data);
    }
}
