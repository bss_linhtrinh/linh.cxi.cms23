import {BaseEntity} from './base.entity';
import {Currency} from './currency';

export class CurrencyRate extends BaseEntity {
    id: number;
    currency_code: string;
    currency_id: number;
    amount: number;
    point: number;;

    constructor() {
        super();
        this.id = null;
        this.currency_code = 'VND';
        this.currency_id = null;
        this.amount = null;
        this.point = 1;
    }

    static create(data) {
        return new CurrencyRate().fill(data);
    }

    fill(data): this {
        if (!data.point) {
            data.point = 1;
        }
        return super.fill(data);
    }

    parse() {
        return {
            id: this.id,
            currency_code: this.currency_code,
            currency_id: this.currency_id,
            amount: this.amount,
        };
    }
}
