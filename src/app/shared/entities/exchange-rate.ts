import {BaseEntity} from './base.entity';

export class ExchangeRate extends BaseEntity {
    id: string;
    name: string;
    accumulated_rate: number;
    maximum_amount: number;

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.accumulated_rate = 0;
        this.maximum_amount = 0;
    }

    static create(data: any) {
        return (new ExchangeRate()).fill(data);
    }

    parse() {
        return {
            id: this.id,
            name: this.name,
            accumulated_rate: this.accumulated_rate,
            maximum_amount: this.maximum_amount,
        };
    }
}
