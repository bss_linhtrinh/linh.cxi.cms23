import {BaseEntity} from './base.entity';
import {Img} from './img';
import {Image} from './image';
import * as _ from 'lodash';

export class SponsorItem extends BaseEntity {
    product_id: string;
    qty_ordered: number;

    constructor() {
        super();
        this.product_id = null;
        this.qty_ordered = 1;
    }

    static create(data) {
        return new SponsorItem().fill(data);
    }

    fill(data) {
        return super.fill(data);
    }

    parse() {

    }
}
