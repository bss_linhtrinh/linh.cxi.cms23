import {Locale} from './locale';
import {BaseEntity} from './base.entity';

export class AttributeTranslation extends BaseEntity {
    locale: string;
    name: string;

    constructor() {
        super();
        this.locale = null;
        this.name = null;
    }

    static create(data) {
        return new AttributeTranslation().fill(data);
    }

    parseCodeLocale(data: Locale) {
        this.locale = data.code;
        return this;
    }

    parse() {
        return {
            locale: this.locale,
            name: this.locale
        };
    }
}
