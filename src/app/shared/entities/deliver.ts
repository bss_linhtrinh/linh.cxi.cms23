import {BaseEntity} from './base.entity';
import {Brand} from './brand';
import {Outlet} from './outlets';
import {Organization} from './organization';
import {Role} from './role';
import {LinkMeta} from './link-meta';

export class Deliver extends BaseEntity {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    phone: string;
    gender: string;
    dob: string;
    address: string;
    is_activated: boolean;

    brand: Brand;
    outlet: Outlet;
    organization: Organization;
    role: Role;
    scope: string;

    avatar: string;
    created_at: string;
    urls: LinkMeta;

    get name(): string {
        if (!this.first_name) {
            return `${this.last_name}`;
        }

        if (!this.last_name) {
            return `${this.first_name}`;
        }

        return `${this.first_name} ${this.last_name}`;
    }

    get outlet_address(): string {
        return this.outlet ? this.outlet.address : null;
    }

    constructor() {
        super();
        this.id = null;
        this.first_name = null;
        this.last_name = null;
        this.email = null;
        this.phone = null;
        this.gender = null;
        this.dob = null;
        this.address = null;
        this.is_activated = null;

        this.brand = null;
        this.outlet = null;
        this.organization = null;
        this.role = null;
        this.scope = null;

        this.avatar = null;

        this.created_at = null;

        this.urls = null;
    }

    fill(data): this {
        return super.fill(data);
    }
}
