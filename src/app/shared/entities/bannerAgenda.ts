import {BaseEntity} from './base.entity';
import {formatDate} from '@angular/common';
import * as _ from 'lodash';
import {Title} from './title';
import {Image} from './image';

export class BannerAgenda extends BaseEntity {
    date: string;
    title_en: string;
    title_vi: string;
    language: string;
    en: any;
    vi: any;


    constructor() {
        super();
        this.date = null;
        this.title_en = null;
        this.title_vi = null;
        this.language = 'Tiếng Việt';
        this.en = null;
        this.vi = null;
    }

    static create(data) {
        return new BannerAgenda().fill(data);
    }

    fill(data) {

        // images
        if (data.translations) {
            const en = _.find(data.translations, ['locale', 'en']);
            const vi = _.find(data.translations, ['locale', 'vi']);
            if (en) {
                data.title_en = en.title;
            }
            if (vi) {
                data.title_vi = vi.title;
            }
        }
        return super.fill(data);
    }

    parse(): object {
        const clonedObj = _.cloneDeep(this);
        clonedObj.date = formatDate(clonedObj.date, 'yyyy-MM-dd', 'en-US').toString();

        clonedObj.en = {
            title: clonedObj.title_en
        };
        clonedObj.vi = {
            title: clonedObj.title_vi
        };

        return clonedObj;
    }
}
