import {BaseEntity} from './base.entity';
import {Timeline} from './timeline';
import {Sponsor} from './sponsor';
import {Highlight} from './highlight';
import {Speaker} from './speaker';
import {Topic} from './topic';
import {BankInfo} from './bank-info';
import {Reporter} from './reporter';
import {Ticket} from './ticket';
import {BannerList} from './bannerList';
import {Img} from './img';
import {Exhibition} from './exhibition';
import {Company} from './company';

export class Detail extends BaseEntity {
    activities: object;
    content: string;
    createdAt: string;
    endTime: string;
    startTime: string;
    isDeleted: boolean;
    location: string;
    promotionDiscount: number;
    vipPromotionDiscount: number;
    sponsorPackages: object;
    target: string;
    unit: string;
    contentImageUrl: Img;
    boothMap: Img;
    locations: [];
    thumbnailUrl: Img | File;
    timeLine?: Timeline[];
    sponsors?: Sponsor[];
    highlightInfo?: Highlight[];
    speakers?: Speaker[];
    topics?: Topic[];
    bankInfo?: BankInfo;
    reporters?: Reporter[];
    tickets?: Ticket[];
    banner?: BannerList[];
    letters: any;
    exhibitions: Exhibition[];
    companies: Company[];

    constructor() {
        super();
        this.activities = null;
        this.content = null;
        this.createdAt = null;
        this.endTime = null;
        this.startTime = null;
        this.isDeleted = false;
        this.location = null;
        this.promotionDiscount = 0;
        this.vipPromotionDiscount = 0;
        this.sponsorPackages = {};
        this.target = '';
        this.unit = 'VND';
        this.contentImageUrl = new Img();
        this.boothMap = new Img();
        this.thumbnailUrl = new Img();
        this.locations = [];
        this.timeLine = [new Timeline()];
        this.sponsors = [new Sponsor()];
        this.highlightInfo = [new Highlight()];
        this.speakers = [new Speaker()];
        this.topics = [new Topic()];
        this.bankInfo = new BankInfo();
        this.reporters = [new Reporter()];
        this.tickets = [new Ticket()];
        this.banner = [new BannerList()];
        this.letters = null;
        // this.exhibitions = [new Exhibition()];
        // this.companies = [new Company()];
    }

    static create(data) {
        return new Detail().fill(data);
    }

    fill(data): this {

        if (data.reporters) {
            data.reporters = data.reporters.map(reporter => Reporter.create(reporter));
        }
        if (data.speakers) {
            data.speakers = data.speakers.map(speaker => Speaker.create(speaker));
        }
        if (data.tickets) {
            data.tickets = data.tickets.map(ticket => Ticket.create(ticket));
        }
        if (data.sponsors) {
            data.sponsors = data.sponsors.map(sponsor => Sponsor.create(sponsor));
        }
        if (data.highlightInfo) {
            data.highlightInfo = data.highlightInfo.map(highlight => Highlight.create(highlight));
        }
        if (data.bankInfo) {
            data.bankInfo = BankInfo.create(data.bankInfo);
        }
        if (data.topics) {
            data.topics = data.topics.map(topic => Topic.create(topic));
        }
        if (data.activities && data.activities.timeLine) {
            data.timeLine = data.activities.timeLine.map(timeLine => Timeline.create(timeLine));
        }
        if (data.banner instanceof Object) {
            const bannerArray = [];
            Object.keys(data.banner).forEach((key: any) => {
                const value = data.banner[key];
                bannerArray.push({_id: value._id, resolution: key, showTitle: value.showTitle, url: value.url});
            });
            data.banner = bannerArray.map(banner => BannerList.create(banner));
        }

        if (data.exhibitions) {
            data.exhibitions = data.exhibitions.map(exhibition => Exhibition.create(exhibition));
        }
        if (data.companies) {
            data.companies = data.companies.map(company => Company.create(company));
        }
        return super.fill(data);
    }
}
