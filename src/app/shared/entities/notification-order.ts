import {BaseEntity} from './base.entity';

export class NotificationOrder extends BaseEntity {
    message: string;
    order_id: number;
    read_at: boolean;
    status_from: string;
    status_to: string;

    constructor() {
        super();
        this.message = null;
        this.order_id = null;
        this.read_at = null;
        this.status_from = null;
        this.status_to = null;
    }
}
