import {BaseEntity} from './base.entity';
import {formatDate} from '@angular/common';
import * as _ from 'lodash';

export class List extends BaseEntity {
    id: number;
    desktopBackgroundFile: File;
    mobileBackgroundFile: File;
    thumbnailFile: File;
    start_time: string;
    end_time: string;
    title: string;

    constructor() {
        super();
        this.id = null;
        this.desktopBackgroundFile = null;
        this.mobileBackgroundFile = null;
        this.thumbnailFile = null;
        this.start_time = null;
        this.end_time = null;
        this.title = null;
    }

    static create(data) {
        return new List().fill(data);
    }

    parse(): object {
        const list = _.cloneDeep(this);
        list.start_time = formatDate(list.start_time, 'yyyy-MM-dd HH:mm', 'en-US').toString();
        list.end_time = formatDate(list.end_time, 'yyyy-MM-dd HH:mm', 'en-US').toString();

        return list;
    }
}
