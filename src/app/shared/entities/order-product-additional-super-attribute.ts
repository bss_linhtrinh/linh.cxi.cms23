import {BaseEntity} from './base.entity';

export class OrderProductAdditionalSuperAttribute extends BaseEntity {

    attribute_id: number;
    option_id: number;

    constructor() {
        super();
        this.attribute_id = null;
        this.option_id = null;
    }

    static create(data) {
        return new OrderProductAdditionalSuperAttribute().fill(data);
    }

    fill(data) {
        return super.fill(data);
    }
}
