import { BaseEntity } from './base.entity';

export class orderHistoryReport extends BaseEntity {

    id: number;
    date: string;
    order_method: string;
    status: string;
    sub_total: number;
    tax: string;
    total: number;
    payment_total: number;
    customer_first_name: string;
    customer_last_name: string;
    customer_id: number;
    customer_email: string;
    customer_level: string;
    promotion: string;
    promotion_discount_code: number;
    promotion_discount: number;
    promotion_max_discount: number;
    voucher_code: string;
    voucher_discount: string;
    voucher_max_discount: string;
    payment_method: string;
    payment_status: boolean;
    payment_detail: any;
    gift_point: string;
    application_scope: string;
    outlet: any;
    items: any;
    driver: string;
    total_items: number;
    platform: string;

    constructor() {
        super();
        this.id = null;
        this.date = null;
        this.order_method = null;
        this.status = null;
        this.sub_total = null;
        this.tax = null;
        this.total = null;
        this.payment_total = null;
        this.customer_first_name = null;
        this.customer_last_name = null;
        this.customer_id = null;
        this.customer_email = null;
        this.customer_level = null;
        this.promotion = null;
        this.promotion_discount_code = null;
        this.promotion_discount = null;
        this.promotion_max_discount = null;
        this.voucher_code = null;
        this.voucher_discount = null;
        this.voucher_max_discount = null;
        this.payment_method = null;
        this.payment_status = null;
        this.payment_detail = null;
        this.gift_point = null;
        this.application_scope = null;
        this.outlet = null;
        this.items = null;
        this.driver = null;
        this.total_items = null;
        this.platform = null;
    }

    static create(data) {
        return new orderHistoryReport().fill(data);
    }
}
