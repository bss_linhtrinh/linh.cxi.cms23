import {BaseEntity} from './base.entity';
import {Agenda} from './agenda';
import {BannerAgenda} from './bannerAgenda';
import * as _ from 'lodash';
import {TimelineChildren} from './timeline-children';

export class Timeline extends BaseEntity {
    id: number;
    banner?: BannerAgenda;
    agenda?: Agenda[];
    children?: TimelineChildren[];

    constructor() {
        super();
        this.id = null;
        this.banner = new BannerAgenda();
        this.agenda = [new Agenda()];
        this.children = [];
    }

    static create(data) {
        return new Timeline().fill(data);
    }

    fill(data): this {
        if (data.banner) {
            data.banner = BannerAgenda.create(data.banner);
        }
        if (data.agenda) {
            data.agenda = data.agenda.map(agenda => BannerAgenda.create(agenda));
        }
        if (data.children) {
            data.children = data.children.map(timeline => TimelineChildren.create(timeline));
        }
        return super.fill(data);
    }
}
