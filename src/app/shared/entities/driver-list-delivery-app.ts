import { BaseEntity } from './base.entity';

export class driverListDeliveryApp extends BaseEntity {

    id: number;
    first_name: string;
    last_name: string;
    phone: string;
    latitude: number;
    longitude: number;
    application_scope: string;
    total: number;
    get name(): string {
        if (!this.first_name) {
            return `${this.last_name}`;
        }
        if (!this.last_name) {
            return `${this.first_name}`;
        }
        return `${this.first_name} ${this.last_name}`;
    }
    constructor() {
        super();
        this.id = null;
        this.first_name = null;
        this.last_name = null;
        this.phone = null;
        this.latitude = null;
        this.longitude = null;
        this.application_scope = null;
        this.total = null;
    }

    static create(data) {
        return new driverListDeliveryApp().fill(data);
    }
}
