import {BaseEntity} from './base.entity';

export class OrderComment extends BaseEntity {
    id: number;
    content: string;

    constructor() {
        super();
        this.id = null;
        this.content = null;
    }

    static create(data) {
        return new OrderComment().fill(data);
    }

    fill(data): this {
        return super.fill(data);
    }

    parse() {
        return {
            id: this.id,
            content: this.content
        };
    }
}
