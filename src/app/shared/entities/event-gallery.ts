import {BaseEntity} from './base.entity';
import {Title} from './title';
import * as _ from 'lodash';
import {Image as CxiImage, Image} from './image';

export class EventGallery extends BaseEntity {
    title_vi: string;
    title_en: string;
    type: string;
    image_id: any;
    image_ids: any;
    en: any;
    vi: any;


    constructor() {
        super();
        this.title_vi = null;
        this.title_en = null;
        this.type = 'gallery';
        this.image_id = null;
        this.image_ids = null;
        this.en = null;
        this.vi = null;
    }

    static create(data) {
        return new EventGallery().fill(data);
    }

    fill(data) {

        // images
        if (data.image_id) {
            data.image_id = Image.create(data.image_id);
        }

        if (data.image_ids && data.image_ids.length > 0) {
            data.image_ids = data.image_ids.map(image => CxiImage.create(image));
        }

        if (data.translations) {
            const en = _.find(data.translations, ['locale', 'en']);
            const vi = _.find(data.translations, ['locale', 'vi']);
            if (en) {
                data.title_en = en.title;
            }
            if (vi) {
                data.title_vi = vi.title;
            }
        }
        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        if (clonedObj.image_id) {
            if (clonedObj.image_id && clonedObj.image_id.id) {
                clonedObj.image_id = clonedObj.image_id.id;
            }
        }

        clonedObj.en = {
            title: clonedObj.title_en
        };
        clonedObj.vi = {
            title: clonedObj.title_vi
        };

        return clonedObj;
    }
}
