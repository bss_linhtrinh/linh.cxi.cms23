import {BaseEntity} from './base.entity';
import {status} from '../constants/status';

export class Status extends BaseEntity {
    name: string;
    value: string;

    constructor() {
        super();
        this.name = null;
        this.value = null;
    }

    static create(data): Status {
        return (new Status()).fill(data);
    }

    static init(): Status[] {
        let stt: Status[];

        stt = status.map(g => this.create(g));

        return stt;
    }
}
