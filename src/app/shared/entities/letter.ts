import {BaseEntity} from './base.entity';
import {Img} from './img';
import {Image} from './image';
import * as _ from 'lodash';

export class Letter extends BaseEntity {
    image_ids: any;


    constructor() {
        super();
        this.image_ids = null;
    }

    static create(data) {
        return new Letter().fill(data);
    }

    fill(data) {

        // images
        if (data.files && data.files.length > 0) {
            data.image_ids = data.files.map(image => Image.create(image));
        }
        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        if (clonedObj.image_ids && clonedObj.image_ids.length > 0) {
            const images = [];
            clonedObj.image_ids.forEach(img => {
                if (img.id) {
                    images.push(img.id);
                }
            });
            clonedObj.image_ids = images;
        } else {
            clonedObj.image_ids = [];
        }

        return clonedObj;
    }
}
