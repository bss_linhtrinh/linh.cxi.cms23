import { BaseEntity } from './base.entity';

export class Country extends BaseEntity {
    id: number;
    name: string;
    code: string;

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.code = null;
    }

    static create(data) {
        return new Country().fill(data);
    }
}
