import { BaseEntity } from './base.entity';

export class salesReport extends BaseEntity {
    brand: any;
    id: number;
    name: string;
    quantity: number;
    sales_total: number;

    constructor() {
        super();
        this.brand = null;
        this.id = null;
        this.name = null;
        this.quantity = null;
        this.sales_total = null;
    }

    static create(data) {
        return new salesReport().fill(data);
    }
}
