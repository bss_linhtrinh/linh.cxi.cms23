import {BaseEntity} from './base.entity';
import * as _ from 'lodash';

export class Task extends BaseEntity {
    _id: string;
    name: string;
    description: string;
    allOutlets: boolean;
    brandId: number;
    name_vi: string;
    name_en: string;
    description_en: string;
    description_vi: string;
    outletId: number[];
    applicationScope: string;
    createdAt: string;
    updatedAt: string;
    active: boolean;
    translations: any;

    get id(): string {
        return this._id;
    }

    get is_activated(): boolean {
        return this.active;
    }

    set is_activated(is_activated: boolean) {
        this.active = is_activated;
    }

    constructor() {
        super();
        this._id = null;
        this.name = null;
        this.name_vi = null;
        this.name_en = null;
        this.brandId = null;
        this.description = null;
        this.description_vi = null;
        this.description_en = null;
        this.outletId = [];
        this.allOutlets = true;
        this.applicationScope = 'All';
        this.createdAt = null;
        this.updatedAt = null;
        this.active = false;
        this.translations = [
            {name: null, description: null, locale: 'en'},
            {name: null, description: null, locale: 'vi'},
        ];
    }

    static create(data) {
        return new Task().fill(data);
    }

    fill(data) {
        if (data.allOutlets) {
            data.applicationScope = 'All';
        } else {
            data.applicationScope = 'Specific outlet';
        }

        return super.fill(data);
    }

    parse(): object {
        return {
            name: this.name,
            description: this.description,
            brandId: this.brandId,
            outletId: this.outletId,
            allOutlets: !!(this.applicationScope === 'All'),
            translations: this.translations
        };
    }

    translation () {
        let arr = [];
        if(this.name_en) {
            arr.push({name: this.name_en, description: this.description_en, locale: 'en'});
        }
        if(!this.name_en) {
            arr.push({name: this.name_vi, description: this.description_vi, locale: 'en'});
        }
        if(this.name_vi) {
            arr.push({name: this.name_vi, description: this.description_vi, locale: 'vi'});
        }
        if(!this.name_vi) {
            arr.push({name: this.name_en, description: this.description_en, locale: 'vi'});
        }
        return arr;
    }
}
