import {BaseEntity} from './base.entity';

export class deliveryReport extends BaseEntity {
    items: any;
    customer_last_name: string;
    customer_first_name: string;
    created_at: string;
    customer_email: string;
    constructor() {
        super();
        this.items = null;
        this.customer_last_name = null;
        this.customer_first_name = null;
        this.created_at = null;
        this.customer_email = null;
    }

    static create(data) {
        return new deliveryReport().fill(data);
    }
}
