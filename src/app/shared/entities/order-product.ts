import {BaseEntity} from './base.entity';
import {OrderProductAdditional} from './order-product-additional';
import {OrderProductAddOn} from './order-product-add-on';
import * as _ from 'lodash';

export class OrderProduct extends BaseEntity {
    id: number;

    product_id?: number;
    name?: string;
    type?: string;
    sku?: string;

    price: number;
    qty_ordered: number;
    total: number;

    additional?: OrderProductAdditional;
    child?: OrderProduct;
    addons: OrderProductAddOn[];

    get addOns(): string {
        return this.addons
            .filter(addOn => addOn.name)
            .map(addOn => addOn.name)
            .join(', ');
    }

    set addOns(val: string) {

    }

    constructor() {
        super();
        this.id = null;

        this.product_id = null;
        this.type = null;
        this.sku = null;
        this.name = null;

        this.price = null;
        this.qty_ordered = 1;
        this.total = null;

        this.additional = null;
        this.child = null;
        this.addons = [];
        this.addOns = null;
    }

    static create(data) {
        return new OrderProduct().fill(data);
    }

    fill(data) {
        // price
        if (data.price) {
            data.price = parseInt(data.price, 10);
        }

        if (data.total &&
            typeof data.total === 'string') {
            data.total = parseInt(data.total, 10);
        }

        if (data.addons) {
            data.addons = data.addons.map(addon => OrderProductAddOn.create(addon));
        }

        if (data.child) {
            data.child = OrderProduct.create(data.child);
        }

        return super.fill(data);
    }

    parse() {
        return {
            id: this.id,
            product_id: this.product_id,
            type: this.type,
            sku: this.sku,
            name: this.name,
            price: this.price,
            qty_ordered: this.qty_ordered,
            total: this.total,
            additional: this.additional,
            child: this.child ? this.child.parse() : null,
            addons: this.addons ? this.addons.map((addOn: OrderProductAddOn) => addOn.parse()) : null,
        };
    }
}
