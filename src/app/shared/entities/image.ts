import {environment} from '../../../environments/environment';
import {MediaFile} from './media-file';

export class Image extends MediaFile {
    id: number;
    _medium_thumb: string;
    _original_image: string;
    _small_thumb: string;

    get medium_thumb(): string {
        return this._medium_thumb;
    }

    set medium_thumb(val: string) {
        const baseUrl = environment.baseUrl;
        if (val && !val.includes('data:image/png;')) {
            if (!val.includes(baseUrl)) {
                this._medium_thumb = `${baseUrl}${val}`;
            }
        } else {
            this._medium_thumb = val;
        }
    }

    get original_image(): string {
        return this._original_image;
    }

    set original_image(val: string) {
        const baseUrl = environment.baseUrl;
        if (val && !val.includes('data:image/png;')) {
            if (val && !val.includes(baseUrl)) {
                this._original_image = `${baseUrl}${val}`;
            }
        } else {
            this._original_image = val;
        }
    }

    get small_thumb(): string {
        return this._small_thumb;
    }

    set small_thumb(val: string) {
        const baseUrl = environment.baseUrl;
        if (val && !val.includes('data:image/png;')) {
            if (val && !val.includes(baseUrl)) {
                this._small_thumb = `${baseUrl}${val}`;
            }
        } else {
            this._small_thumb = val;
        }
    }

    get path_image(): string {
        const baseUrl = environment.baseUrl;
        return this.original_image.replace(`${baseUrl}`, ``);
    }

    constructor() {
        super();
        this.id = null;
        this.medium_thumb = null;
        this.original_image = null;
        this.small_thumb = null;
    }

    static create(data): Image {
        return new Image().fill(data);
    }

    fill(data): this {
        if (data) {
            if (typeof data === 'object') {
                // medium_thumb
                if (data.medium_thumb) {
                    this.medium_thumb = data.medium_thumb;
                }
                // original_image
                if (data.original_image) {
                    this.original_image = data.original_image;
                }
                // small_thumb
                if (data.small_thumb) {
                    this.small_thumb = data.small_thumb;
                }
            } else if (typeof data === 'string') {
                this.medium_thumb = data;
                this.original_image = data;
                this.small_thumb = data;
            }
        } else {
            this.medium_thumb = data;
            this.original_image = data;
            this.small_thumb = data;
        }

        return super.fill(data);
    }
}
