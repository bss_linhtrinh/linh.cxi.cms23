import {BaseEntity} from './base.entity';
import {ProductComboItemAdditional} from './product-combo-item-additional';
import {ProductType} from '../types/product-type';

export class ProductComboItem extends BaseEntity {
    id: number;
    product_id: number;
    type: ProductType;
    qty_ordered: number;
    price: number;
    total: number;

    additional?: ProductComboItemAdditional;
    child?: ProductComboItem;
    addon_types: any[];

    constructor() {
        super();
        this.id = null;
        this.product_id = null;
        this.type = null;
        this.qty_ordered = null;
        this.price = 0;
        this.total = 0;

        this.additional = null;
        this.child = null;
        this.addon_types = [];
    }

    static create(data) {
        return new ProductComboItem().fill(data);
    }

    fill(data): this {
        if (data.addon_types) {
            data.addon_types = data.addon_types.map(addOn => addOn.id);
        }

        return super.fill(data);
    }

    parse() {
        const clonedObj = Object.assign({}, this);

        if (clonedObj.addon_types && clonedObj.addon_types.length > 0) {
            const addOnTypes = [];
            for (const addOnTypeId of clonedObj.addon_types) {
                addOnTypes.push({
                    addon_id: addOnTypeId,
                    qty_ordered: 1,
                    price: 0,
                    total: 0,
                });
            }
            clonedObj.addon_types = addOnTypes;
        }

        return clonedObj;
    }
}
