import {BaseEntity} from './base.entity';
import * as _ from 'lodash';
import {Image} from './image';

export class Category extends BaseEntity {
    id: number;
    name: string;
    slug: string;
    status: number;
    image: string;
    position: number;
    display_mode: string;
    description: string;
    parent_id: number;
    parent: string;
    is_activated: boolean;
    children: any;
    brand_id: number;
    translations: any;
    en: object = {};
    vi: object = {};
    _images: Image[];

    get images(): Image[] {
        return this._images;
    }

    set images(images: Image[]) {
        this._images = images;
    }

    get base_image(): any {
        if (this.images && this.images.length > 0) {
            return this.images[0];
        }
        return null;
    }

    set base_image(image: any) {
        if (this.images) {
            this.images[0] = image;
        }
    }

    translateProps = ['name', 'description'];

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.slug = null;
        this.status = 1;
        this.image = '';
        this.images = null;
        this.base_image = null;
        this.position = 0;
        this.display_mode = 'products_and_description';
        this.description = null;
        this.parent_id = 0;
        this.parent = null;
        this.is_activated = false;
        this.children = [];
        this.brand_id = null;
        this.en = {};
        this.vi = {};
        this.translations = [];
    }

    static create(data) {
        return new Category().fill(data);
    }

    static treelize(categories: Category[]): Category[] {
        if (categories) {
            categories = categories.sort(
                (a, b) => a.id - b.id
            );

            for (const category of categories) {
                if (category.children &&
                    category.children.length) {

                    for (const i in category.children) {
                        const subItem = category.children[i];


                        // remove child-item from parent list
                        const index = categories.findIndex(x => x.id === subItem.id);
                        if (index !== -1) {
                            categories.splice(index, 1);
                        }


                        // create instance
                        category.children[i] = Category.create(subItem);
                    }
                }
            }
        }
        return categories;
    }

    fill(data): this {
        // images
        const images = [];
        if (data.image) {
            const image = Image.create(data.image);
            data.base_image = image;
            images.push(image);
        }
        this.images = images;

        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        // parse image
        if (clonedObj.images && clonedObj.images.length > 0) {
            if (clonedObj.images[0] && clonedObj.images[0].path_image) {
                clonedObj.image = clonedObj.images[0].path_image;
            }
        }

        return clonedObj;
    }
}
