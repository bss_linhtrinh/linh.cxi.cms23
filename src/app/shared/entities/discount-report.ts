import {BaseEntity} from './base.entity';

export class discountReport extends BaseEntity {
    id:number;
    orders: any;
    name: string;
    email: string;
    phone: string;
    address: string;
    level: string;
    reward_points: number;
    total_orders: number;
    total_paid: number;
    payment_total: number;
    latest_order: string;

    constructor() {
        super();
        this.orders = null;
        this.name = null;
        this.email = null;
        this.phone = null;
        this.address = null;
        this.level = null;
        this.reward_points = null;
        this.total_orders = null;
        this.total_paid = null;
        this.payment_total = null;
        this.latest_order = null;
    }

    static create(data) {
        return new discountReport().fill(data);
    }
}
