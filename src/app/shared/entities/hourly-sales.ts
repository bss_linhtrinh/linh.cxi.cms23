import { BaseEntity } from './base.entity';

export class hourlySalesReport extends BaseEntity {

    childs: any;
    avg_sale_with_order: string;
    period: string;
    sale_percent: string;
    sale_total: string;
    transaction_total: number;
    constructor() {
        super();
        this.childs = null;
        this.avg_sale_with_order = null;
        this.period = null;
        this.sale_percent = null;
        this.sale_total = null;
        this.transaction_total = null;
    }

    static create(data) {
        return new hourlySalesReport().fill(data);
    }
}
