import {BaseEntity} from './base.entity';
import {Organization} from './organization';
import {Outlet} from './outlets';
import {Brand} from './brand';
import {Provider} from '../types/providers';

export class User extends BaseEntity {
    id: number;
    first_name: string;
    last_name: string;
    dob: string;
    gender: string;
    email: string;
    phone: string;
    address: string;
    start_working_date: string;

    // password
    password: string;
    password_confirmation: string;


    // scope
    user_role: string;
    organization: Organization;
    brand: Brand;
    outlet: Outlet;
    role_id: number;
    organization_id: number;
    brand_id: number;
    outlet_id: number;

    role: any;
    provider: string;

    // setting
    is_notify: boolean;
    is_activated: boolean;

    get name(): string {
        const fn = this.capitalize(this.first_name);
        const ln = this.capitalize(this.last_name);

        if (!fn) {
            return `${ln}`;
        }

        if (!ln) {
            return `${fn}`;
        }

        return `${fn} ${ln}`;
    }

    set name(name: string) {
        const arr = name.split(' ');

        this.first_name = arr[0];
        arr.unshift();

        this.last_name = arr.join(' ');
    }

    // avatar: any;
    private _avatar: string | any;

    get avatar(): string | any {
        return this._avatar;
    }

    set avatar(val: string | any) {
        this._avatar = this.parseAvatar(val);
    }

    get role_name(): string {
        return this.role ? this.role.name : null;
    }

    get application_scope(): string {
        const currentUser = JSON.parse(localStorage.getItem('cxi|user'));
        const filter = JSON.parse(localStorage.getItem('cxi|filter'));

        if (!this || !this.id) {
            return '';
        }


        if (currentUser.provider === Provider.SuperAdmin) {

            // 1. SUPER ADMIN

            if (!filter || !(filter.organization && filter.organization.id)) { // without filter
                return `BeeSight Soft`;
            } else { // with filter

                if (filter.outlet && filter.outlet.id) {

                    // with filter outlet

                    if (!this.organization || !this.organization.name) {
                        return ``; // throw error
                    }

                    if (!this.brand || !this.brand.name) {
                        return ``; // throw error
                    }

                    if (!this.outlet || !this.outlet.name) {
                        return ``; // throw error
                    }

                    return `${this.outlet.name}`;

                } else if (filter.brand && filter.brand.id) {

                    // with filter brand
                    if (!this.organization || !this.organization.name) {
                        return ``; // throw error
                    }

                    if (!this.brand || !this.brand.name) {
                        return ``; // throw error
                    }

                    if (!this.outlet || !this.outlet.name) {
                        return `${this.brand.name}`;
                    }

                    return `${this.brand.name} / ${this.outlet.name}`;

                } else if (filter.organization && filter.organization.id) {

                    // with filter organization
                    if (!this.organization || !this.organization.name) {
                        return ``; // throw error
                    }

                    if (!this.brand || !this.brand.name) {
                        return `${this.organization.name}`;
                    }

                    if (!this.outlet || !this.outlet.name) {
                        return `${this.brand.name}`;
                    }

                    return `${this.brand.name} / ${this.outlet.name}`;

                } else {
                    return ``; // throw error , filter cannot be empty
                }
            }
        } else if (currentUser.provider === Provider.Admin) {

            // 2. ADMIN : (organization, brand, outlet)

            if (!filter || !filter.brand || !filter.brand.id) {

                // without filter
                if (!this.organization || !this.organization.name) {
                    return ``; // throw error
                }

                if (!this.brand || !this.brand.name) {
                    return `${this.organization.name}`;
                }

                if (!this.outlet || !this.outlet.name) {
                    return `${this.brand.name}`;
                }

                return `${this.brand.name} / ${this.outlet.name}`;

            } else {

                // with filter

                if (filter.outlet && filter.outlet.id) {

                    // with filter outlet

                    if (!this.organization || !this.organization.name) {
                        return ``; // throw error
                    }

                    if (!this.brand || !this.brand.name) {
                        return ``; // throw error
                    }

                    if (!this.outlet || !this.outlet.name) {
                        return ``; // throw error
                    }

                    return `${this.outlet.name}`;

                } else if (filter.brand && filter.brand.id) {

                    // with filter brand

                    if (!this.organization || !this.organization.name) {
                        return ``; // throw error
                    }

                    if (!this.brand || !this.brand.name) {
                        return ``; // throw error
                    }

                    if (!this.outlet || !this.outlet.name) {
                        return `${this.brand.name}`;
                    }

                    return `${this.brand.name} / ${this.outlet.name}`;

                } else {
                    return ``; // throw error , filter cannot be empty
                }
            }
        } else {
            return ``; // throw error , cannot found provider current user
        }
    }

    set application_scope(scope: string) {

    }

    constructor() {
        super();
        this.id = null;
        this.first_name = '';
        this.last_name = '';
        this.dob = null;
        this.gender = 'male';
        this.email = null;
        this.phone = null;
        this.address = null;
        this.password = null;
        this.password_confirmation = null;
        this.start_working_date = null;

        // avatar
        this.avatar = null;

        // scope
        this.user_role = null;
        this.organization = null;
        this.brand = null;
        this.outlet = null;
        this.role_id = null;
        this.organization_id = null;
        this.brand_id = null;
        this.outlet_id = null;
        this.application_scope = null;

        this.role = null;
        this.provider = null;

        // setting
        this.is_notify = null;
        this.is_activated = null;
    }

    static create(data): User {
        return (new User()).fill(data);
    }

    fill(data): this {
        if (data.organization && data.organization.id) {
            data.organization_id = data.organization.id;
        }
        if (data.brand && data.brand.id) {
            data.brand_id = data.brand.id;
        }
        if (data.outlet && data.outlet.id) {
            data.outlet_id = data.outlet.id;
        }
        if (data.role && data.role.id) {
            data.role_id = data.role.id;
        }

        // avatar
        if (data.avatar) {
            this.avatar = data.avatar;
        }

        return super.fill(data);
    }

    parse() {
        // auto select scopeOrganizationId , if Super Admin
        const scopeOrganizationId = localStorage.getItem('cxi|scope.organization_id');

        return {
            id: this.id,
            first_name: this.first_name,
            last_name: this.last_name,
            dob: this.dob,
            gender: this.gender,
            email: this.email,
            phone: this.phone,
            address: this.address,
            password: this.password,
            password_confirmation: this.password_confirmation,
            user_role: this.user_role,

            role_id: this.role_id,
            organization_id: this.organization_id
                ? this.organization_id
                : (
                    scopeOrganizationId
                        ? scopeOrganizationId
                        : null
                ),
            brand_id: this.brand_id,
            outlet_id: this.outlet_id,

            provider: this.provider,
            is_notify: this.is_notify,
            is_activated: this.is_activated,
            start_working_date: this.start_working_date
        };
    }
}
