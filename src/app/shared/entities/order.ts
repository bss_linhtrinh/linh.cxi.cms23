import {BaseEntity} from './base.entity';
import {OrderStatus} from '../types/order-status';
import {Brand} from './brand';
import {Customer} from './customer';
import {OrderProduct} from './order-product';
import {Outlet} from './outlets';
import {Address} from './address';
import * as _ from 'lodash';
import {Deliver} from './deliver';
import {Image} from './image';
import {OrderType} from '../types/order-type';
import {OrderPromotion} from './order-promotion';
import {OrderDriver} from './order-driver';
import {OrderComment} from './order-comment';
import {OrderDelivery} from './order-delivery';
import {PaymentMethod} from './payment-method';
import {User} from './user';

export class Order extends BaseEntity {
    id: number;
    name: string;
    status: OrderStatus;
    order_type: OrderType;
    note: string;

    payment_method: PaymentMethod;
    payment_method_id: number;
    payment_status: string; // paid|void

    // brand
    brand: Brand;
    brand_id: number;

    // outlet
    outlet: Outlet;
    outlet_id: number;

    // customer
    is_guest: boolean;
    customer_id: number;
    customer: Customer;
    customer_email: string;
    customer_first_name: string;
    customer_last_name: string;
    customer_phone: string;

    employee_id: number;
    employee: User;

    staff_id: number;
    staff: User;

    // deliver
    deliver: Deliver;
    deliver_id: number;
    delivery: OrderDelivery;

    // total
    discount_amount: number;
    discount_percent: number;
    grand_total: number;
    sub_total: number;
    base_grand_total: number;
    base_sub_total: number;
    total_item_count: number;
    total_qty_ordered: number;
    commission_amount: number;

    billing_address: Address;
    shipping_address: Address;

    items: OrderProduct[];


    // fields mới thêm tạm
    food_ready: string;
    discount_code: string;
    voucher_code: string;
    voucher_discount: string;
    voucher_maximum_discount: string;
    gift_point: number;

    count: number;
    images: Image[];
    promotions: OrderPromotion[];
    drivers: OrderDriver[];
    comments: OrderComment[];

    platform: string;
    created_at: string;

    get final_total() {
        return this.grand_total - (this.commission_amount ? this.commission_amount : 0);
    }

    get promotion(): OrderPromotion {
        return this.promotions && this.promotions.length ? this.promotions[0] : null;
    }

    set promotion(val: OrderPromotion) {
        if (this.promotions && this.promotions.length > 0) {
            this.promotions[0] = val;
        } else {
            this.promotions = [val];
        }
    }

    get customer_name(): string {
        if (!this.customer_first_name) {
            return `${this.customer_last_name}`;
        }
        if (!this.customer_last_name) {
            return `${this.customer_first_name}`;
        }
        return `${this.customer_first_name} ${this.customer_last_name}`;
    }

    set customer_name(name: string) {
        const arr = name.split(' ');
        this.customer_last_name = arr.pop();
        this.customer_first_name = arr.join(' ');
    }

    get price(): number {
        return this.sub_total;
    }

    set price(val: number) {
        this.sub_total = val;
    }

    get driver(): OrderDriver {
        return (this.drivers && this.drivers.length > 0)
            ? this.drivers[0]
            : new OrderDriver();
    }

    set driver(orderDriver: OrderDriver) {
        if (this.drivers && this.drivers.length > 0) {
            this.drivers[0] = orderDriver;
        } else {
            this.drivers = [orderDriver];
        }
    }

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.status = OrderStatus.Pending;
        this.order_type = OrderType.Delivery;
        this.note = null;

        this.payment_method_id = null;

        // brand
        this.brand = null;
        this.brand_id = null;

        this.outlet = null;
        this.outlet_id = null;

        // customer
        this.is_guest = null;
        this.customer_id = null;
        this.customer = null;
        this.customer_email = null;
        this.customer_first_name = null;
        this.customer_last_name = null;
        this.customer_phone = null;
        // this.customer_name = null;

        this.employee = null;
        this.employee_id = null;
        this.staff = null;
        this.staff_id = null;

        this.deliver = null;
        this.deliver_id = null;
        this.delivery = null;

        this.discount_amount = null;
        this.grand_total = null;
        this.sub_total = null;
        this.base_grand_total = null;
        this.base_sub_total = null;
        this.total_item_count = null;
        this.total_qty_ordered = null;

        this.billing_address = new Address();
        this.shipping_address = new Address();
        this.items = [];
        this.images = null;
        this.promotions = [new OrderPromotion()];
        this.count = null;
        this.platform = 'web_cms';
        this.created_at = null;
        this.drivers = [new OrderDriver()];
        this.comments = [new OrderComment()];
    }

    fill(data) {
        // brand & outlet
        if (data.brand && data.brand.id) {
            data.brand_id = data.brand.id;
        }
        if (data.outlet && data.outlet.id) {
            data.outlet_id = data.outlet.id;
        }

        // order_type
        if (data.order_type) {
            data.order_type = data.order_type === 'dine_in'
                ? OrderType.DineIn
                : data.order_type === 'pick_on_outlet'
                    ? OrderType.PickOnOutlet
                    : data.order_type === 'delivery'
                        ? OrderType.Delivery
                        : null;
        }

        // customer
        if (data.customer) {
            data.customer = Customer.create(data.customer);
        }
        // customer_name
        if (data.customer_name) {
            this.customer_name = data.customer_name;
        }


        // employee & staff
        if (data.employee && data.employee.id) {
            data.employee_id = data.employee.id;
        }
        if (data.staff && data.staff.id) {
            data.staff_id = data.staff.id;
        }

        // deliver
        if (data.drivers) {
            data.drivers = data.drivers.map(driver => OrderDriver.create(driver));
        } else {
            data.drivers = [new OrderDriver()];
        }
        if ((data.deliver && data.deliver.id) && !data.deliver_id) {
            data.deliver_id = data.deliver.id;
        }
        if (data.delivery) {
            data.delivery = OrderDelivery.create(data.delivery);
        }

        // images
        if (data.images) {
            data.images = data.images.map(image => Image.create(image));
        }

        // promotions
        if (data.promotions && data.promotions.length) {
            data.promotions = data.promotions.map(promotion => OrderPromotion.create(promotion));
        } else {
            data.promotions = [new OrderPromotion()]; // old
        }

        // address
        if (data.billing_address) {
            data.billing_address = Address.create(data.billing_address);
        }
        if (data.shipping_address) {
            data.shipping_address = Address.create(data.shipping_address);
        }

        // items
        if (data.items &&
            data.items.length > 0) {
            data.items = data.items.map(
                item => OrderProduct.create(item)
            );
        }

        // comments
        if (data.comments &&
            data.comments.length > 0
        ) {
            data.comments = data.comments.map(comment => OrderComment.create(comment));
        }

        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);
        if (clonedObj.is_guest && clonedObj.customer_id === 0) {
            delete clonedObj.customer_id;
        }

        // delete tạm
        delete clonedObj.food_ready;
        delete clonedObj.discount_code;
        delete clonedObj.voucher_code;
        delete clonedObj.voucher_discount;
        delete clonedObj.voucher_maximum_discount;
        delete clonedObj.gift_point;

        return {
            id: this.id,
            name: this.name,
            status: this.status,
            order_type: this.order_type,
            note: this.note,

            payment_method_id: this.payment_method_id,

            // brand & outlet
            brand_id: this.brand_id,
            outlet_id: this.outlet_id,

            // customer
            is_guest: this.is_guest,
            customer_id: this.customer_id,
            customer_email: this.customer_email,
            customer_first_name: this.customer_first_name,
            customer_last_name: this.customer_last_name,
            customer_phone: this.customer_phone,

            staff_id: this.staff_id ? this.staff_id : null,
            employee_id: this.employee_id ? this.employee_id : null,

            discount_amount: this.discount_amount,
            grand_total: this.grand_total,
            sub_total: this.sub_total,
            base_grand_total: this.base_grand_total,
            base_sub_total: this.base_sub_total,
            total_item_count: this.total_item_count,
            total_qty_ordered: this.total_qty_ordered,

            billing_address: this.billing_address ? this.billing_address.parse() : null,
            shipping_address: this.shipping_address ? this.shipping_address.parse() : null,
            items: this.items.map(item => item.parse()),
            deliver_id: (
                this.drivers &&
                this.drivers.length > 0 &&
                this.drivers[0] &&
                this.drivers[0].id
            )
                ? this.drivers[0].id
                : null,
            promotions: this.promotions.map(p => p.parse()),
            comments: this.comments
                .filter(c => c.content)
                .map(c => c.parse()),
            platform: this.platform
        };
    }
}
