import {BaseEntity} from './base.entity';

export class CommissionOutlet extends BaseEntity {
    outlet_id: number;
    percentage: number;
    amount: number;

    constructor() {
        super();
        this.outlet_id = null;
        this.percentage = 0;
        this.amount = 0;
    }

    static create(data) {
        return new CommissionOutlet().fill(data);
    }

    fill(data): this {
        return super.fill(data);
    }

    parse() {
        return {
            outlet_id: this.outlet_id,
            percentage: this.percentage ? this.percentage : 0,
            amount: this.amount ? this.amount : 0
        };
    }
}
