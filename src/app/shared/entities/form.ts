const classes = {

};

export class Form {
    constructor(className, data) {
        return (new classes[className]()).fill(data);
    }
}
