import {BaseEntity} from './base.entity';
import {GENDERS} from '../constants/genders';

export class Gender extends BaseEntity {
    name: string;
    value: string;

    constructor() {
        super();
        this.name = null;
        this.value = null;
    }

    static create(data): Gender {
        return (new Gender()).fill(data);
    }

    static init(): Gender[] {
        let genders: Gender[];
        genders = GENDERS.map(g => this.create(g));
        return genders;
    }
}
