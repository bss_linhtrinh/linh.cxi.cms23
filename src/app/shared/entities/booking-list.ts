import {BaseEntity} from './base.entity';
import {Customer} from './customer';
import {BookingTicket} from './booking-ticket';

export class BookingList extends BaseEntity {
    id: number;
    status: string;
    status_label: string;
    customer_id: any;
    customer_email: string;
    customer_first_name: string;
    customer_last_name: string;
    customer_phone: string;
    sub_total: number;
    base_sub_total: number;
    formated_sub_total: string;
    items: any;
    brand: any;
    resource: any;
    order_type: string;
    payment_method_id: number;
    payment_status: string;
    bill_id: string;
    created_at: string;

    get customer_name(): string {

        // if (!this.customer_first_name) {
        //     return `${this.customer_first_name}`;
        // }
        //
        // if (!this.customer_last_name) {
        //     return `${this.customer_last_name}`;
        // }

        return `${this.customer_first_name} ${this.customer_last_name}`;
    }

    set customer_name(name: string) {
        const arr = name.split(' ');

        this.customer_first_name = arr[0];
        arr.unshift();

        this.customer_last_name = arr.join(' ');
    }


    constructor() {
        super();
        this.id = null;
        this.status = null;
        this.customer_id = null;
        this.status_label = null;
        this.customer_first_name = null;
        this.customer_last_name = null;
        this.customer_phone = null;
        this.customer_email = null;
        this.sub_total = null;
        this.base_sub_total = null;
        this.formated_sub_total = null;
        this.items = null;
        this.brand = null;
        this.resource = null;
        this.order_type = null;
        this.payment_method_id = null;
        this.payment_status = null;
        this.bill_id = null;
        this.created_at = null;
    }

    static createAll(data: any[]) {
        return data.map(c => this.create(c));
    }

    static create(data: any) {
        return (new BookingList()).fill(data);
    }

    fill(data): this {

        if (!data.customer_last_name) {
            data.customer_last_name = '';
        }

        if (!data.customer_first_name) {
            data.customer_first_name = '';
        }

        return super.fill(data);
    }
}
