import {BaseEntity} from './base.entity';

export class Permission extends BaseEntity {
    id: number;
    name: string;

    constructor() {
        super();
        this.id = null;
        this.name = null;
    }
}
