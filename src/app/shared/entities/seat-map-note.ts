import {BaseEntity} from './base.entity';
import * as _ from 'lodash';

export class SeatMapNote extends BaseEntity {
    title: string;
    // content_en: string;
    // content_vi: string;
    color: string;
    description: string;
    // en: any;
    // vi: any;


    constructor() {
        super();
        this.title = null;
        // this.content_en = null;
        // this.content_vi = null;
        this.color = '#2889e9';
        this.description = null;
        // this.en = null;
        // this.vi = null;
    }

    static create(data) {
        return new SeatMapNote().fill(data);
    }

    // fill(data) {
    //
    //     images
    //     if (data.translations) {
    //         const en = _.find(data.translations, ['locale', 'en']);
    //         const vi = _.find(data.translations, ['locale', 'vi']);
    //         if (en) {
    //             data.title_en = en.title;
    //             data.content_en = en.content;
    //         }
    //         if (vi) {
    //             data.title_vi = vi.title;
    //             data.content_vi = vi.content;
    //         }
    //     }
    //     return super.fill(data);
    // }
    // parse() {
    //     const clonedObj = _.cloneDeep(this);
    //
    //
    //     clonedObj.en = {
    //         title: clonedObj.title_en,
    //         content: clonedObj.content_en
    //     };
    //     clonedObj.vi = {
    //         title: clonedObj.title_vi,
    //         content: clonedObj.content_vi
    //     };
    //
    //     return clonedObj;
    // }
}
