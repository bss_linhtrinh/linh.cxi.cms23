import {BaseEntity} from './base.entity';

export class OrderDelivery extends BaseEntity {
    assigned_at: string;
    food_ready_at: string;
    picked_up_at: string;
    delivered_at: string;
    driver_confirmed: boolean;

    constructor() {
        super();
        this.assigned_at = null;
        this.food_ready_at = null;
        this.picked_up_at = null;
        this.delivered_at = null;
        this.driver_confirmed = null;
    }

    static create(data) {
        return new OrderDelivery().fill(data);
    }

    fill(data) {
        return super.fill(data);
    }
}
