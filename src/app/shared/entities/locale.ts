import {BaseEntity} from './base.entity';

export class Locale extends BaseEntity {
    id: number;
    name: string;
    code: string;

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.code = null;
    }
}
