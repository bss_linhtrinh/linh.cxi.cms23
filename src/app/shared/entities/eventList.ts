import {BaseEntity} from './base.entity';

export class EventList extends BaseEntity {
    id: number;
    location: string;
    title: string;
    start_time: string;
    end_time: string;
    status: string;
    thumbnailUrl: object;
    bookingType: string;


    constructor() {
        super();
        this.id = null;
        this.location = null;
        this.title = null;
        this.start_time = null;
        this.end_time = null;
        this.status = null;
        this.thumbnailUrl = null;
        this.bookingType = null;
    }

    static createAll(data: any[]) {
        return data.map(c => this.create(c));
    }

    static create(data: any) {
        return (new EventList()).fill(data);
    }
}
