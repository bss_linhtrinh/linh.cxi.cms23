import { BaseEntity } from './base.entity';

export class actionLogReport extends BaseEntity {
    categories: any;
    brand_name: string;
    name: string;
    outlet_name: string;
    price: number;
    quantity: number;
    sales_total: number;

    constructor() {
        super();
        this.categories = null;
        this.brand_name = null;
        this.name = null;
        this.outlet_name = null;
        this.price = null;
        this.quantity = null;
        this.sales_total = null;
    }

    static create(data) {
        return new actionLogReport().fill(data);
    }
}
