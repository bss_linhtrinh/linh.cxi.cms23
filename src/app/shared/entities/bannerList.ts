import {BaseEntity} from './base.entity';
import * as _ from 'lodash';
import {Image} from './image';

export class BannerList extends BaseEntity {
    id: number;
    resolution: string;
    show_title: boolean;
    file: any;
    group: string;

    constructor() {
        super();
        this.id = null;
        this.resolution = null;
        this.file = null;
        this.show_title = true;
        this.group = null;
    }

    static createAll(data: any[]) {
        return data.map(c => this.create(c));
    }

    static create(data) {
        return new BannerList().fill(data);
    }

    fill(data) {

        // images
        if (data.file) {
            data.file = Image.create(data.file);
        }
        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        if (clonedObj.file) {
            if (clonedObj.file && clonedObj.file.id) {
                clonedObj.file = clonedObj.file.id;
            }
        }

        return clonedObj;
    }
}
