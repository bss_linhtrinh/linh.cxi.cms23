import { BaseEntity } from './base.entity';
import { Address } from './address';
import { Image } from './image';
import { environment } from '../../../environments/environment';

export class Customer extends BaseEntity {
    id: number;
    first_name: string;
    last_name: string;
    gender: string;
    address: string;
    email: string;
    phone: string;
    dob: string;
    brand_id: number;
    password: string;
    password_confirmation: string;
    is_activated: boolean;
    level: any;
    image: string;
    customer_type_id: string;
    contact_method: string;
    characteristic_notes: string;
    social_network: string;
    salesman_id: string;
    hobbies: any;
    _images: Image[];
    salesman: any;
    addresses: Address[];
    private _avatar: string;
    //avatar: string;
    path_string: string;
    // add tạm
    gift_point: number;
    customer_type: any;
    //feed back
    favorite_products: any;
    country: string;
    city: string;
    district: string;
    more_info: string;
    references: string;
    budget: number;
    customer_classification: string;
    potential: string;
    gifs: string;
    invited_events: string;
    column_date_1: string;
    column_date_2: string;
    column_date_3: string;
    column_date_4: string;
    column_date_5: string;
    anniversaries: any;
    contacts: any;

    get name(): string {
        if (!this.first_name) {
            return `${this.last_name}`;
        }

        if (!this.last_name) {
            return `${this.first_name}`;
        }

        return `${this.first_name} ${this.last_name}`;
    }
    get avatar(): string {
        if (this._avatar === null) {
            return `/assets/img/avatar-default.jpg`;
        } else if (
            this._avatar.includes('data:image/jpeg;') ||
            this._avatar.includes('data:image/png')
        ) {
            return this._avatar;
        } else {
            return `${environment.baseUrl}${this._avatar}`;
        }
    }
    set avatar(val: string) {
        this._avatar = val;
    }

    get customer_level(): string {
        if (!this.level) {
            return `--`;
        }

        return `${this.level.name}`;
    }
    get salesman_customer(): string {
        if (!this.salesman) {
            return `--`;
        }
        if (this.salesman && !this.salesman.last_name) {
            return `${this.salesman.first_name}`;
        }
        if (this.salesman && !this.salesman.first_name) {
            return `${this.salesman.last_name}`;
        }
        return `${this.salesman.first_name} ${this.salesman.last_name}`;
    }
    get customer_types(): string {
        if (!this.customer_type) {
            return `--`;
        }
        return `${this.customer_type.name}`;
    }
    constructor() {
        super();
        this.id = null;
        this.first_name = null;
        this.last_name = null;
        this.email = null;
        this.gender = 'male';
        this.address = null;
        this.phone = null;
        this.dob = null;
        this.brand_id = null;
        this.password = null;
        this.password_confirmation = null;
        this.avatar = null;
        this.path_string = null;
        this.customer_type_id = null;
        this.customer_type = null;
        this.is_activated = false;
        this.level = null;
        this.addresses = [];
        this.hobbies = [];
        this.contact_method = null;
        this.characteristic_notes = null;
        this.social_network = null;
        this.salesman_id = null;
        this.salesman = null;
        // add tạm
        this.gift_point = null;
        //feed
        this.favorite_products = [];
        this.country = null;
        this.city = null;
        this.district = null;
        this.more_info = null;
        this.references = null;
        this.budget = null;
        this.customer_classification = null;
        this.potential = null;
        this.gifs = null;
        this.invited_events = null;
        this.column_date_1 = null;
        this.column_date_2 = null;
        this.column_date_3 = null;
        this.column_date_4 = null;
        this.column_date_5 = null;
        this.anniversaries = [];
        this.contacts = [];
    }

    static create(data) {
        return new Customer().fill(data);
    }

    fill(data): this {
        if (data.avatar) {
            this.avatar = data.avatar;
        }

        return super.fill(data);
    }

    parse() {
        return {
            id: this.id,
            first_name: this.first_name,
            last_name: this.last_name,
            email: this.email,
            gender: this.gender,
            address: this.address,
            phone: this.phone,
            dob: this.dob,
            brand_id: this.brand_id,
            password: this.password,
            password_confirmation: this.password_confirmation,
            avatar: this._avatar,
            path_string: this.path_string,
            is_activated: this.is_activated,
            level: this.level,
            addresses: this.addresses,
            gift_point: this.gift_point,
            hobbies: this.hobbies,
            customer_type_id: this.customer_type_id,
            contact_method: this.contact_method,
            characteristic_notes: this.characteristic_notes,
            social_network: this.social_network,
            salesman_id: this.salesman_id,
            //feed
            country: this.country,
            city: this.city,
            district: this.district,
            more_info: this.more_info,
            references: this.references,
            budget: this.budget,
            customer_classification: this.customer_classification,
            potential: this.potential,
            gifs: this.gifs,
            invited_events: this.invited_events,
            column_date_1: this.column_date_1,
            column_date_2: this.column_date_2,
            column_date_3: this.column_date_3,
            column_date_4: this.column_date_4,
            column_date_5: this.column_date_5,
            //favorite_products: this.favorite_products,
            anniversaries: this.anniversaries,
            contacts: this.contacts,
        };
    }
}
