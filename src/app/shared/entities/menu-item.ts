import {BaseEntity} from './base.entity';
import {Levels} from '../types/levels';
import {Provider} from '../types/providers';

export class MenuItem extends BaseEntity {
    name: string;
    link: string;
    icon?: string;
    children?: MenuItem[];
    isExpand: boolean;

    // modules?: Modules[];
    allow_providers?: string[];
    allow_types?: string[];
    allow_levels?: Levels[];

    constructor() {
        super();
        this.name = '';
        this.link = null;
        this.icon = null;
        this.children = [];
        this.isExpand = false;

        // this.modules = [];
        this.allow_providers = [];
        this.allow_levels = [];
        this.allow_types = [];
    }

    static create(data) {
        return (new MenuItem()).fill(data);
    }

    toggle() {
        this.isExpand = !this.isExpand;
    }
}
