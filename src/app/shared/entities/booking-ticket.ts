import {BaseEntity} from './base.entity';
import {Customer} from './customer';

export class BookingTicket extends BaseEntity {
    ticketId: string;
    name: string;
    quantity: string;
    party: Customer[];

    constructor() {
        super();
        this.ticketId = null;
        this.name = null;
        this.quantity = null;
        this.party = [new Customer()];
    }

    static create(data) {
        return new BookingTicket().fill(data);
    }
}
