import {BaseEntity} from './base.entity';
import {Title} from './title';
import {Image} from './image';
import {ContentModuleImage} from './content-module-image';
import * as _ from 'lodash';
import {ContentContact} from './content-contact';

export class Content extends BaseEntity {
    page_name: string;
    title: string;
    slug: string;
    content: string;
    contact: ContentContact;
    files: any;


    constructor() {
        super();
        this.page_name = null;
        this.title = null;
        this.slug = null;
        this.content = null;
        this.contact = new ContentContact();
        this.files = null;
    }

    static create(data) {
        return new Content().fill(data);
    }

    fill(data) {

        if (data.files && data.files.length > 0) {
            data.files = data.files.map(image => Image.create(image));
        }
        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        if (clonedObj.files && clonedObj.files.length > 0) {
            const images = [];
            clonedObj.files.forEach(img => {
                if (img.id) {
                    images.push(img.id);
                }
            });
            clonedObj.files = images;
        } else {
            clonedObj.files = [];
        }

        return clonedObj;
    }
}
