import {BaseEntity} from './base.entity';
import * as _ from 'lodash';
import {Image} from './image';

export class Organization extends BaseEntity {
    id: number;
    name: string;
    email: string;
    tax_code: string;
    phone: string;
    max_brands: number;
    share_customer_data: string;
    is_activated: boolean;
    website: {
        app_id: string,
        id: number
    };

    image: any;
    image_id: number;

    _images: Image[];

    get images(): Image[] {
        return this._images;
    }

    set images(images: Image[]) {
        this._images = images;
    }

    constructor() {
        super();
        this.id = null;
        this.name = '';
        this.email = '';
        this.tax_code = '';
        this.phone = '';
        this.max_brands = 0;
        this.share_customer_data = '1';
        this.is_activated = null;
        this.website = {
            app_id: '',
            id: null
        };
        this.image_id = null;
        this.image = null;
        this.images = null;
    }

    static create(data): Organization {
        return (new Organization()).fill(data);
    }

    public fill(data) {
        const images = [];

        if (data.domain) {
            data.domain = data.domain.split('.')[0] || data.domain;
        }

        if (data.image) {
            images.push(Image.create(data.image));
        }
        this.images = images;

        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        // parse image
        if (clonedObj.images && clonedObj.images.length > 0) {
            if (clonedObj.images[0] && clonedObj.images[0].id) {
                clonedObj.image = clonedObj.images[0];
                clonedObj.image_id = clonedObj.images[0].id;
            }
        }

        return clonedObj;
    }
}
