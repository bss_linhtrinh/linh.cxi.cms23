import { BaseEntity } from './base.entity';
import { Image } from './image';
import * as _ from 'lodash';

export class Promotion extends BaseEntity {
    id: number;
    name: string;
    code: string;
    status: boolean;
    description: string;
    discount_type: string;
    discount_amount: number;
    discount_threshold: number;
    discount_quantity: number;
    minimum_amount: number;
    maximum_discount: number;
    free_gift_condition: string;
    starts_from: string;
    ends_till: string;
    weekly_schedules: any;
    dob_type: string;
    purchasing_time: number;
    products: any;
    product_blacklist: any;
    customer_levels: any;
    payment_methods: any;
    image: any;
    image_id: number;
    brand_id: number;
    gifts: any;
    platform: Array<string>;
    apply_for_all_outlets: boolean;
    outlets: any;
    price: number;
    allowed_gift_quantity_items: number;
    en: object = {};
    vi: object = {};
    translations: any;
    is_activated: number;

    _images: Image[];

    get images(): Image[] {
        return this._images;
    }

    set images(images: Image[]) {
        this._images = images;
    }

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.code = null;
        this.description = null;
        this.status = true;
        this.discount_type = 'discount';
        this.discount_amount = 0;
        this.discount_threshold = 0;
        this.discount_quantity = null;
        this.minimum_amount = 0;
        this.maximum_discount = null;
        this.free_gift_condition = 'product';
        this.starts_from = null;
        this.ends_till = null;
        this.weekly_schedules = null;
        this.dob_type = 'day';
        this.purchasing_time = 0;
        this.products = null;
        this.product_blacklist = null;
        this.customer_levels = null;
        this.payment_methods = null;
        this.image_id = null;
        this.image = null;
        this.images = null;
        this.brand_id = null;
        this.gifts = null;
        this.is_activated = null;
        this.platform = null;
        this.apply_for_all_outlets = true;
        this.outlets = null;
        this.price = 0;
        this.allowed_gift_quantity_items = 0;
        this.en = {};
        this.vi = {};
        this.translations = null;
    }
    static create(data): Promotion {
        return (new Promotion()).fill(data);
    }

    fill(data): this {
        // images
        const images = [];
        if (data.image) {
            images.push(Image.create(data.image));
        }
        this.images = images;

        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        if (clonedObj.images && clonedObj.images.length > 0) {
            if (clonedObj.images[0] && clonedObj.images[0].id) {
                clonedObj.image = clonedObj.images[0];
                clonedObj.image_id = clonedObj.images[0].id;
            }
        }

        return clonedObj;
    }
}
