import {BaseEntity} from './base.entity';
import {Img} from './img';
import {Image} from './image';
import * as _ from 'lodash';

export class Highlight extends BaseEntity {
    id: number;
    content_vi: string;
    content_en: string;
    image_id: any;
    en: any;
    vi: any;


    constructor() {
        super();
        this.id = null;
        this.content_vi = null;
        this.content_en = null;
        this.image_id = null;
        this.en = null;
        this.vi = null;
    }

    static create(data) {
        return new Highlight().fill(data);
    }

    fill(data) {

        // images
        if (data.image_id) {
            data.image_id = Image.create(data.image_id);
        }

        if (data.translations) {
            const en = _.find(data.translations, ['locale', 'en']);
            const vi = _.find(data.translations, ['locale', 'vi']);
            if (en) {
                data.content_en = en.content;
            }
            if (vi) {
                data.content_vi = vi.content;
            }
        }

        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        if (clonedObj.image_id) {
            if (clonedObj.image_id && clonedObj.image_id.id) {
                clonedObj.image_id = clonedObj.image_id.id;
            }
        }

        clonedObj.en = {
            content: clonedObj.content_en
        };
        clonedObj.vi = {
            content: clonedObj.content_vi
        };

        return clonedObj;
    }
}
