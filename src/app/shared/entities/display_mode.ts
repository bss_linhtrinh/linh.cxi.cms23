import {BaseEntity} from './base.entity';
import {Modes} from '../constants/display_mode';

export class Mode extends BaseEntity {
    name: string;
    value: string;

    constructor() {
        super();
        this.name = null;
        this.value = null;
    }

    static create(data): Mode {
        return (new Mode()).fill(data);
    }

    static init(): Mode[] {
        let modes: Mode[];

        modes = Modes.map(g => this.create(g));

        return modes;
    }
}
