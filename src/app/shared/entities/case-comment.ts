import { BaseEntity } from './base.entity';

export class CaseComment extends BaseEntity {
    id: number;
    title: string;
    content: string;
    is_activated: number;
    status: number;
    created_at: string;
    en: object = {};
    vi: object = {};
    translations: any;
    constructor() {
        super();
        this.id = null;
        this.title = null;
        this.content = null;
        this.created_at = null;
        this.is_activated = null;
        this.status = null;
        this.en = {};
        this.vi = {};
        this.translations = null;
    }

    static create(data) {
        return new CaseComment().fill(data);
    }
}
