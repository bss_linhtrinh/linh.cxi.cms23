import {BaseEntity} from './base.entity';
import {ROLE_SCOPES} from '../constants/role-scopes';
import {Scope} from '../types/scopes';

export class RoleScope extends BaseEntity {
    value: Scope | null;
    name: string;

    constructor() {
        super();
        this.value = null;
        this.name = null;
    }

    static create(): RoleScope[] {
        return ROLE_SCOPES.map(
            s => (new RoleScope()).fill(s)
        );
    }
}
