import { BaseEntity } from './base.entity';

export class PaymentMethod extends BaseEntity {
    id: number;
    name: string;
    code: string;
    value: string;
    status: boolean;
    brand_id: number;
    isShow: boolean;
    constructor() {
        super();
        this.id = null;
        this.value = null;
        this.brand_id = null;
        this.name = null;
        this.code = null;
        this.status = true;
    }
}
