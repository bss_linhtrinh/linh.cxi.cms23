import { BaseEntity } from './base.entity';


export class Backlist extends BaseEntity {
    action: string;
    list: any;

    constructor() {
        super();
        this.action = null;
        this.list = [];
    }

    static create(data): Backlist {
        return (new Backlist()).fill(data);
    }
}
