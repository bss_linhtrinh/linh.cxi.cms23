import {BaseEntity} from './base.entity';
import {Image} from './image';
import * as _ from 'lodash';

export class Topic extends BaseEntity {
    id: number;
    title_en: string;
    title_vi: string;
    image_id: any;
    en: any;
    vi: any;

    constructor() {
        super();
        this.id = null;
        this.title_en = null;
        this.title_vi = null;
        this.image_id = null;
        this.en = null;
        this.vi = null;
    }

    static create(data) {
        return new Topic().fill(data);
    }

    fill(data) {

        // images
        if (data.image_id) {
            data.image_id = Image.create(data.image_id);
        }

        if (data.translations) {
            const en = _.find(data.translations, ['locale', 'en']);
            const vi = _.find(data.translations, ['locale', 'vi']);
            if (en) {
                data.title_en = en.title;
            }
            if (vi) {
                data.title_vi = vi.title;
            }
        }
        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        if (clonedObj.image_id) {
            if (clonedObj.image_id && clonedObj.image_id.id) {
                clonedObj.image_id = clonedObj.image_id.id;
            }
        }

        clonedObj.en = {
            title: clonedObj.title_en
        };
        clonedObj.vi = {
            title: clonedObj.title_vi
        };

        return clonedObj;
    }
}
