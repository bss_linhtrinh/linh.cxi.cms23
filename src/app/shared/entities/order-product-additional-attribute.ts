import {BaseEntity} from './base.entity';

export class OrderProductAdditionalAttribute extends BaseEntity {

    id: string;
    option_id: number;
    option_label: string;
    attribute_name: string;

    constructor() {
        super();
        this.id = null;
        this.option_id = null;
        this.option_label = null;
        this.attribute_name = null;
    }

    static create(data) {
        return new OrderProductAdditionalAttribute().fill(data);
    }

    fill(data) {
        return super.fill(data);
    }
}
