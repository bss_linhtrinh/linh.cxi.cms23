import {BaseEntity} from './base.entity';
import {Agenda} from './agenda';
import {BannerAgenda} from './bannerAgenda';
import * as _ from 'lodash';
import {TimelineChild} from './timeline-child';

export class TimelineChildren extends BaseEntity {
    id: number;
    banner?: BannerAgenda;
    agenda?: Agenda[];
    children?: TimelineChild[];

    constructor() {
        super();
        this.id = null;
        this.banner = new BannerAgenda();
        this.agenda = [new Agenda()];
        this.children = [];
    }

    static create(data) {
        return new TimelineChildren().fill(data);
    }
    // parse() {
    //     const clonedObj = _.cloneDeep(this);
    //     if (clonedObj.agenda && clonedObj.agenda.length > 0) {
    //         clonedObj.agenda = clonedObj.agenda.map(reporter => reporter.parse());
    //     }
    // }

    fill(data): this {
        if (data.banner) {
            data.banner = BannerAgenda.create(data.banner);
        }
        if (data.agenda) {
            data.agenda = data.agenda.map(agenda => BannerAgenda.create(agenda));
        }
        if (data.children) {
            data.children = data.children.map(timeline => TimelineChild.create(timeline));
        }
        return super.fill(data);
    }
}
