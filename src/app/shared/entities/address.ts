import {BaseEntity} from './base.entity';

export class Address extends BaseEntity {
    id: number;
    name: string;
    phone: string;
    building_name: string;
    address_nickname: string;
    address_type: string;
    address1: string;
    address2: string;
    country: string;
    state: string;
    city: string;
    ward: any;
    district: any;
    postcode: string;
    latitude: number;
    longitude: number;
    notes: string;

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.phone = null;
        this.address_nickname = null;
        this.building_name = null;
        this.address_type = null;
        this.address1 = null;
        this.address2 = null;
        this.country = null;
        this.state = null;
        this.city = null;
        this.ward = null;
        this.district = null;
        this.postcode = null;
        this.latitude = null;
        this.longitude = null;
        this.notes = null;
    }

    static create(data) {
        return new Address().fill(data);
    }

    fill(data) {
        return super.fill(data);
    }

    parse() {
        let clonedObj = Object.assign({}, this);

        if (clonedObj.name && clonedObj.address1) {
            delete clonedObj.ward;
            delete clonedObj.district;

            if (!clonedObj.notes) {
                clonedObj.notes = 'Order notes';
            }
        } else {
            clonedObj = null;
        }

        return clonedObj;
    }
}
