import {BaseEntity} from './base.entity';
import {ExchangeRate} from './exchange-rate';
import {Brand} from './brand';
import {CurrencyRate} from './currency-rate';

export class RewardSetting extends BaseEntity {
    id: number;
    brand: Brand;
    brand_id: number;
    currency_rates: CurrencyRate[];
    customer_level_exchange_rate: ExchangeRate[];

    get currency_rate() {
        return this.currency_rates[0];
    }

    set currency_rate(val) {
        this.currency_rates[0] = val;
    }

    point_accumulated: string; // all_stores
    outlets: number[];
    reviewing_point: number;
    reviewing_earning_time: string; // always
    rating_point: number; // always
    rating_earning_time: string; // always

    constructor() {
        super();
        this.id = null;
        this.brand = null;
        this.brand_id = null;
        this.currency_rates = [new CurrencyRate()];
        this.customer_level_exchange_rate = [new ExchangeRate()];
        this.point_accumulated = 'all_stores';
        this.outlets = [];
        this.reviewing_point = null;
        this.reviewing_earning_time = 'always';
        this.rating_point = null;
        this.rating_earning_time = 'always';
    }

    fill(data): this {
        if (data.currency_rates &&
            data.currency_rates.length > 0) {
            data.currency_rates = data.currency_rates.map(
                currencyRate => CurrencyRate.create(currencyRate)
            );
        } else {
            data.currency_rates = [new CurrencyRate()];
        }
        if (data.brand && data.brand.id && !data.brand_id) {
            data.brand_id = data.brand.id;
        }
        if (data.customer_level_exchange_rate &&
            data.customer_level_exchange_rate.length > 0) {
            data.customer_level_exchange_rate = data.customer_level_exchange_rate.map(
                exchangeRate => ExchangeRate.create(exchangeRate)
            );
        }
        return super.fill(data);
    }

    parse() {
        return {
            id: this.id,
            brand_id: this.brand_id,
            currency_rates: this.currency_rates.map(cr => cr.parse()),
            point_accumulated: this.point_accumulated,
            outlets: this.outlets,
            reviewing_point: this.reviewing_point,
            reviewing_earning_time: this.reviewing_earning_time,
            rating_point: this.rating_point,
            rating_earning_time: this.rating_earning_time,
            customer_level_exchange_rate: this.customer_level_exchange_rate.map(er => er.parse())
        };
    }
}
