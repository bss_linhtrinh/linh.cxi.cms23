import {BaseEntity} from './base.entity';

export class Language extends BaseEntity {

    id: number;
    code: string;
    name: string;
    flag: string;

    constructor() {
        super();
        this.id = null;
        this.code = null;
        this.name = null;
        this.flag = null;
    }

    static create(data) {
        return new Language().fill(data);
    }

    fill(data): this {
        if (data.code) {
            data.flag = `assets/img/languages/flags/${data.code}.png`;
        }
        return super.fill(data);
    }
}
