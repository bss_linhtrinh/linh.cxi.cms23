import { BaseEntity } from './base.entity';

export class Notification extends BaseEntity {
    id: number;
    name: string;
    constructor() {
        super();
        this.id = null;
        this.name = null;
    }

    static create(data) {
        return new Notification().fill(data);
    }
}
