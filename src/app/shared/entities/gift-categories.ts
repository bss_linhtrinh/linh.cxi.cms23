import { BaseEntity } from './base.entity';
import * as _ from 'lodash';
import { Image } from './image';

export class GiftCategories extends BaseEntity {
    id: number;
    name: string;
    description: string;
    image_id: number;
    status: boolean;
    image: any;
    is_activated: number;
    brand_id: number;
    en: object = {};
    vi: object = {};
    translations: any;
    _images: Image[];

    get images(): Image[] {
        return this._images;
    }

    set images(images: Image[]) {
        this._images = images;
    }

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.description = null;
        this.image_id = null;
        this.image = null;
        this.status = true;
        this.is_activated = 0;
        this.brand_id = null;
        this.en = {};
        this.vi = {};
        this.translations = null;
    }

    static create(data): GiftCategories {
        return (new GiftCategories()).fill(data);
    }
    fill(data): this {
        // images
        const images = [];
        if (data.image) {
            images.push(Image.create(data.image));
        }
        this.images = images;

        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        if (clonedObj.images && clonedObj.images.length > 0) {
            if (clonedObj.images[0] && clonedObj.images[0].id) {
                clonedObj.image = clonedObj.images[0];
                clonedObj.image_id = clonedObj.images[0].id;
            }
        }

        return clonedObj;
    }
}
