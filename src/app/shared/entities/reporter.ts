import {BaseEntity} from './base.entity';
import {Img} from './img';
import {Image} from './image';
import * as _ from 'lodash';

export class Reporter extends BaseEntity {
    id: number;
    customer_name: string;
    customer_degree: string;
    customer_position: string[];
    customer_description: string;
    customer_report_title_vi: string;
    customer_report_title_en: string;
    customer_report_content_vi: string;
    customer_report_content_en: string;
    customer_images: any;
    customer_curriculum_vitae: any;
    en: any;
    vi: any;

    constructor() {
        super();
        this.id = null;
        this.customer_name = null;
        this.customer_degree = null;
        this.customer_position = [];
        this.customer_description = null;
        this.customer_report_title_vi = null;
        this.customer_report_title_en = null;
        this.customer_report_content_vi = null;
        this.customer_report_content_en = null;
        this.customer_images = null;
        this.customer_curriculum_vitae = null;
        this.en = null;
        this.vi = null;
    }

    static create(data) {
        return new Reporter().fill(data);
    }

    fill(data) {

        // images
        if (data.customer_images && data.customer_images.length > 0) {
            data.customer_images = data.customer_images.map(image => Image.create(image));
        }
        if (data.customer_curriculum_vitae && data.customer_curriculum_vitae.length > 0) {
            data.customer_curriculum_vitae = data.customer_curriculum_vitae.map(image => Image.create(image));
        }
        if (data.translations) {
            const en = _.find(data.translations, ['locale', 'en']);
            const vi = _.find(data.translations, ['locale', 'vi']);
            if (en) {
                data.customer_report_title_en = en.customer_report_title;
                data.customer_report_content_en = en.customer_report_content;
            }
            if (vi) {
                data.customer_report_title_vi = vi.customer_report_title;
                data.customer_report_content_vi = vi.customer_report_content;
            }
        }
        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        if (clonedObj.customer_images && clonedObj.customer_images.length > 0) {
            const images = [];
            clonedObj.customer_images.forEach(img => {
                if (img.id) {
                    images.push(img.id);
                }
            });
            clonedObj.customer_images = images;
        } else {
            clonedObj.customer_images = [];
        }

        if (clonedObj.customer_curriculum_vitae && clonedObj.customer_curriculum_vitae.length > 0) {
            const images_cv = [];
            clonedObj.customer_curriculum_vitae.forEach(img => {
                if (img.id) {
                    images_cv.push(img.id);
                }
            });
            clonedObj.customer_curriculum_vitae = images_cv;
        } else {
            clonedObj.customer_curriculum_vitae = [];
        }
        if (typeof clonedObj.customer_position === 'string') {
            const array = [];
            array.push(clonedObj.customer_position);
            clonedObj.customer_position = array;
        }

        clonedObj.en = {
            customer_report_title: clonedObj.customer_report_title_en,
            customer_report_content: clonedObj.customer_report_content_en

        };
        clonedObj.vi = {
            customer_report_title: clonedObj.customer_report_title_vi,
            customer_report_content: clonedObj.customer_report_content_vi
        };

        return clonedObj;
    }
}
