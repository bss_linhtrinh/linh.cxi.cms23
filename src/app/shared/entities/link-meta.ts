import {BaseEntity} from './base.entity';

export class LinkMeta extends BaseEntity {
    change_password_url: number;

    constructor() {
        super();
        this.change_password_url = null;
    }

    fill(data): this {
        return super.fill(data);
    }
}
