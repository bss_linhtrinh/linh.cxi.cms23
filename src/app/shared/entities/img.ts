import {BaseEntity} from './base.entity';
import {formatDate} from '@angular/common';
import * as _ from 'lodash';

export class Img extends BaseEntity {
    id: number;
    url: string;

    constructor() {
        super();
        this.id = null;
        this.url = '';
    }

    static create(data) {
        return new Img().fill(data);
    }
}
