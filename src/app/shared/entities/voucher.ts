import {BaseEntity} from './base.entity';
import * as _ from 'lodash';
import {Image} from './image';

export class Voucher extends BaseEntity {
    id: number;
    name: string;
    type: string;
    description: string;
    code: string;
    qr_code: string;
    quantity: number;
    used: number;
    in_stock: number;
    status: boolean;
    begin_date: string;
    expiry_date: string;
    minimum_order: number;
    maximum_discount: number;
    amount: number;
    allow_to_redeem: boolean;
    point_to_redeem: number;
    all_outlets: boolean;
    all_items: boolean;
    outlets: number[];
    image_id: number;
    image: Image;

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.type = 'cash_voucher_statistic';
        this.code = null;
        this.qr_code = null;
        this.quantity = null;
        this.used = null;
        this.in_stock = null;
        this.description = null;
        this.status = false;
        this.begin_date = null;
        this.expiry_date = null;
        this.minimum_order = 0;
        this.maximum_discount = 0;
        this.amount = 0;
        this.allow_to_redeem = false;
        this.point_to_redeem = 0;
        this.all_items = true;
        this.all_outlets = false;
        this.outlets = [];
        this.image_id = null;
        this.image = null;
    }

    static createAll(data: any[]) {
        return data.map(c => this.create(c));
    }

    static create(data: any) {
        return (new Voucher()).fill(data);
    }

    fill(data): this {

        // outlets
        if (typeof data.all_outlets !== 'undefined') {
            data.all_outlets = !!data.all_outlets;
        }
        if (data.outlets) {
            data.outlets = data.outlets.map(outlet => outlet.id);
        }

        // products
        if (typeof data.all_items !== 'undefined') {
            data.all_items = !!data.all_items;
        }
        if (data.products) {
            data.products = data.products.map(product => product.id);
        }

        if (data.image) {
            data.image = Image.create(data.image);
        }

        return super.fill(data);

    }

    parse() {
        return {
            id: this.id,
            name: this.name,
            type: this.type,
            description: this.description,
            code: this.code,
            qr_code: this.qr_code,
            allow_to_redeem: this.allow_to_redeem,
            point_to_redeem: this.point_to_redeem,
            quantity: this.quantity,
            used: this.used,
            in_stock: this.in_stock,
            status: this.status,
            begin_date: this.begin_date,
            expiry_date: this.expiry_date,
            minimum_order: this.minimum_order,
            maximum_discount: this.maximum_discount,
            amount: this.amount,
            all_outlets: this.all_outlets,
            all_items: this.all_items,
            outlets: this.outlets,
            image_id: this.image_id
                ? this.image_id
                : (
                    (this.image && this.image.id)
                        ? this.image.id
                        : null
                )
        };
    }
}
