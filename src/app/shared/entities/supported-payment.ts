import { BaseEntity } from './base.entity';

export class supportedPayment extends BaseEntity {
    id: number;
    name: string;
    code: string;
    status: boolean;
    value: any;
    isShow: boolean;
    constructor() {
        super();
        this.id = null;
        this.value = null;
        this.name = null;
        this.status = true;
        this.code = null;
    }
}
