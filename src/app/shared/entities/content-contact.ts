import {BaseEntity} from './base.entity';

export class ContentContact extends BaseEntity {
    address: string;
    phone: string;
    fax: string;


    constructor() {
        super();
        this.address = null;
        this.phone = null;
        this.fax = null;
    }

    static create(data) {
        return new ContentContact().fill(data);
    }
}
