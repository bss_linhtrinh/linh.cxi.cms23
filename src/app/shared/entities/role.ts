import {BaseEntity} from './base.entity';
import {RolePermission} from './role-permission';
import {Scope} from '../types/scopes';

export class Role extends BaseEntity {
    id: number;
    name: string;
    description: string;
    slug: string;
    parent_id: number;
    scope: Scope;
    permissions: RolePermission[];
    color: string;

    en: any;
    vi: any;
    translations: any;
    translateProps = ['name', 'description'];

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.description = null;
        this.slug = null;
        this.parent_id = null;
        this.scope = Scope.Organization;
        this.permissions = RolePermission.init();
        this.color = '#ffffff';

        this.en = {name: null};
        this.vi = {name: null};
        this.translations = null;
    }

    fill(data): this {
        if (data.parent && data.parent.id) {
            data.parent_id = data.parent.id;
        }

        if (data.permissions) {
            data.permissions = data.permissions.map(p => RolePermission.create(p));
        }

        return super.fill(data);
    }

    parse() {
        this.parseTranslate();
        return {
            id: this.id,
            name: this.name,
            description: this.description,
            slug: this.slug,
            parent_id: this.parent_id,
            scope: this.scope,
            permissions: this.permissions
                .filter(p => p instanceof RolePermission)
                .map(
                    (p: RolePermission) => p.parse()
                ),
            color: this.color,

            en: this.en,
            vi: this.vi
        };
    }
}
