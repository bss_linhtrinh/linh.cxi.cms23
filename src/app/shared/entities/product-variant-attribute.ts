import {BaseEntity} from './base.entity';

export class ProductVariantAttribute extends BaseEntity {
    id: string;
    name: string;
    options: string;
    value: string;

    constructor() {
        super();
        this.id = null; // id attribute
        this.name = null; // name attribute
        this.options = null; // options attribute
        this.value = null; // value of option
    }

    static create(data) {
        return new ProductVariantAttribute().fill(data);
    }

    parse(): Object {
        const objEmpty = {};
        objEmpty[this.id] = this.value;
        return objEmpty;
    }


}
