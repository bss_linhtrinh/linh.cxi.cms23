import {BaseEntity} from './base.entity';
import {Image} from './image';

export class BannerLink extends BaseEntity {
    id: number;

    image: Image;
    image_id: number;
    link_to: number;

    constructor() {
        super();
        this.id = null;
        this.image = null;
        this.image_id = null;

        this.link_to = null;
    }

    static create(data): BannerLink {
        return (new BannerLink()).fill(data);
    }

    fill(data): this {
        if (data.image) {
            data.image = Image.create(data.image);
        }

        return super.fill(data);
    }

    parse() {
        return {
            image_id: this.image_id ? this.image_id : null,
            image: this.image ? this.image : null,
            link_to: this.link_to ? this.link_to : null,
        };
    }
}
