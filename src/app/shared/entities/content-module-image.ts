import {BaseEntity} from './base.entity';
import {Content} from './content';
import * as _ from 'lodash';
import {Image} from './image';

export class ContentModuleImage extends BaseEntity {
    type: string;
    title_en: string;
    title_vi: string;
    content_vi: string;
    content_en: string;
    description_vi: string;
    description_en: string;
    translations: any;
    images: any;
    vi: any;
    en: any;


    constructor() {
        super();
        this.type = 'image';
        this.title_en = null;
        this.title_vi = null;
        this.content_vi = null;
        this.content_en = null;
        this.description_vi = null;
        this.description_en = null;
        this.images = null;
        this.translations = null;
        this.vi = null;
        this.en = null;
    }

    static create(data) {
        return new ContentModuleImage().fill(data);
    }

    fill(data) {
        if (data.translations) {
            const en = _.find(data.translations, ['locale', 'en']);
            const vi = _.find(data.translations, ['locale', 'vi']);
            if (en) {
                data.title_en = en.title;
                data.content_en = en.content;
                data.description_en = en.description;
            }
            if (vi) {
                data.title_vi = vi.title;
                data.content_vi = vi.content;
                data.description_vi = vi.description;
            }
        }
        if (data.images && data.images.length > 0) {
            data.images = data.images.map(image => Image.create(image));
        }

        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);
        clonedObj.en = {
            title: clonedObj.title_en,
            content: clonedObj.content_en,
            description: clonedObj.description_en
        };
        clonedObj.vi = {
            title: clonedObj.title_vi,
            content: clonedObj.content_vi,
            description: clonedObj.description_vi
        };

        if (clonedObj.images && clonedObj.images.length > 0) {
            const images = [];
            clonedObj.images.forEach(img => {
                if (img.id) {
                    images.push(img.id);
                }
            });
            clonedObj.images = images;
        } else {
            clonedObj.images = [];
        }

        return clonedObj;
    }
}
