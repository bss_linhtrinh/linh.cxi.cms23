import {BaseEntity} from './base.entity';
import {Brand} from './brand';
import * as _ from 'lodash';

export class Outlet extends BaseEntity {
    id: number;
    code: string;
    name: string;
    description: string;
    contact_name: string;
    contact_email: string;
    contact_number: string;
    contact_fax: string;
    is_activated: boolean;

    country: string;
    state: string;
    city: string;
    street: string;
    postcode: string;
    latitude: number;
    longitude: number;
    address?: string;

    brand: Brand;
    parent_id: number;

    // Front end
    disabled?: boolean;
    radiusCircle?: any;

    get brand_name(): string {
        return this.brand ? this.brand.name : '';
    }

    set brand_name(scope: string) {

    }

    en: any;
    vi: any;
    translateProps = ['name'];

    constructor() {
        super();
        this.id = null;
        this.code = null;
        this.name = null;
        this.contact_name = null;
        this.contact_email = null;
        this.contact_number = null;
        this.contact_fax = null;
        this.is_activated = null;


        this.country = '';
        this.state = '';
        this.city = '';
        this.street = '';
        this.postcode = '';
        this.latitude = null;
        this.longitude = null;
        this.address = null;

        this.parent_id = null;
        this.brand = null;


        // Front-end
        this.disabled = null;
        this.radiusCircle = null;

        this.en = {name: null};
        this.vi = {name: null};
    }

    static create(data): Outlet {
        return (new Outlet()).fill(data);
    }

    fill(data): this {
        if (data.brand && data.brand.id) {
            data.brand = Brand.create(data.brand);

            if (!data.parent_id) {
                data.parent_id = data.brand.id;
            }
        }

        return super.fill(data);
    }

    parse() {
        this.parseTranslate();
        return {
            id: this.id,
            code: this.code,
            name: this.name,
            description: this.description,
            contact_name: this.contact_name,
            contact_email: this.contact_email,
            contact_number: this.contact_number,
            contact_fax: this.contact_fax,
            is_activated: this.is_activated,

            country: this.country,
            state: this.state,
            city: this.city,
            street: this.street,
            postcode: this.postcode,
            latitude: this.latitude,
            longitude: this.longitude,
            address: this.address,

            parent_id: this.parent_id,

            en: this.en,
            vi: this.vi
        };
    }
}
