import {BaseEntity} from './base.entity';
import {Condition} from './condition';

export class Option extends BaseEntity {

    id: number;
    key: string;
    value: string;
    scope: string;
    brand_id: number;

    is_json: boolean;

    constructor() {
        super();
        this.key = null;
        this.value = null;
        this.scope = 'brands';
        this.brand_id = null;
        this.is_json = false;
    }

    static createAll(data: any[]) {
        return data.map(c => this.create(c));
    }

    static create(data: any) {
        return (new Option()).fill(data);
    }

    parse() {
        return {
            key: this.key,
            value: this.value,
            brand_id: this.brand_id,
            scope: this.scope,
            is_json: this.is_json,
        };
    }
}
