import {BaseEntity} from './base.entity';
import {OrderProductAdditionalAttribute} from './order-product-additional-attribute';
import {OrderProductAdditionalSuperAttribute} from './order-product-additional-super-attribute';

export class OrderProductAdditional extends BaseEntity {
    quantity: number;
    attributes: OrderProductAdditionalAttribute[];
    is_configurable: boolean;
    super_attribute: OrderProductAdditionalSuperAttribute[];
    selected_configurable_option: number;

    constructor() {
        super();
        this.quantity = null;
        this.attributes = null;
        this.is_configurable = false;
        this.super_attribute = null;
        this.selected_configurable_option = null;
    }

    fill(data) {
        // attributes
        if (data.attributes) {
            const attributes: OrderProductAdditionalAttribute[] = [];
            for (const key in data.attributes) {
                const item = data.attributes[key];
                const newItem = Object.assign({}, item);
                newItem.id = key;
                attributes.push(OrderProductAdditionalAttribute.create(newItem));
            }
            data.attributes = attributes;
        }

        // super_attribute
        if (data.super_attribute) {
            const super_attribute: OrderProductAdditionalSuperAttribute[] = [];
            for (const key in data.super_attribute) {
                const value = data.super_attribute[key];
                const newItem = {
                    attribute_id: key,
                    option_id: value
                };
                super_attribute.push(OrderProductAdditionalSuperAttribute.create(newItem));
            }
            data.super_attribute = super_attribute;
        }

        return super.fill(data);
    }

    parse() {

    }
}


//
// 1. Transform attributes
// {
//     "size": {
//         "option_id": 1,
//         "option_label": "S",
//         "attribute_name": "Size"
//     }
// }
//
// to
//
// [
//     {
//         "id": "size",
//         "option_id": 1,
//         "option_label": "S",
//         "attribute_name": "Size"
//     }
// ]
//
// 2. Transform super_attribute
// {
//     "19": 1
// }
//
// to
//
// [
//     {
//         "attribute_id": "19",
//         "option_id": 1,
//     }
// ]
