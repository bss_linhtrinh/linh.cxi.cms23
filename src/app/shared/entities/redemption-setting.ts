import {BaseEntity} from './base.entity';

export class RedemptionSetting extends BaseEntity {
    tier_process: boolean;
    redemption_list: any[];

    constructor() {
        super();
        this.tier_process = null;
        this.redemption_list = [
            {name: 'A', point_to_redempt: 10},
            {name: 'B', point_to_redempt: 3},
            {name: 'C', point_to_redempt: ''},
        ];
    }

    static create(data): RedemptionSetting {
        return (new RedemptionSetting()).fill(data);
    }
}
