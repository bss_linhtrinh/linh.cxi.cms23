import {Locale} from './locale';
import {BaseEntity} from './base.entity';

export class AttributeOptionTranslation extends BaseEntity {
    locale: string;
    label: string;

    constructor() {
        super();
        this.locale = null;
        this.label = null;
    }

    static create(data) {
        return new AttributeOptionTranslation().fill(data);
    }

    parseCodeLocale(data: Locale) {
        this.locale = data.code;
        this.label = '';
        return this;
    }

    parse() {
        return {
            locale: this.locale,
            label: this.label
        };
    }
}
