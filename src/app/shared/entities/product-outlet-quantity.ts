import {BaseEntity} from './base.entity';

export class ProductOutletQuantity extends BaseEntity {
    outlet_id: number;
    quantity: number;

    constructor() {
        super();
        this.outlet_id = null;
        this.quantity = null;
    }

    static create(data) {
        return new ProductOutletQuantity().fill(data);
    }

    fill(data) {
        return super.fill(data);
    }
}
