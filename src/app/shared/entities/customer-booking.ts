import { BaseEntity } from './base.entity';
import { Address } from './address';
import { Image } from './image';
import * as _ from 'lodash';
import { environment } from '../../../environments/environment';

export class CustomerBooking extends BaseEntity {
    qty_ordered: number;
    customer_gender: string;
    customer_date_of_certification: string;
    customer_position: string;
    customer_degree: string;
    customer_address: string;
    customer_dob: string;
    customer_phone: string;
    certification: string;
    customer_first_name: string;
    customer_last_name: string;
    customer_email: string;
    customer_place_of_certification: string;
    customer_certification: string;
    customer_company: string;
    file: any;


    constructor() {
        super();
        this.qty_ordered = 1;
        this.customer_gender = null;
        this.customer_date_of_certification = null;
        this.customer_position = null;
        this.customer_degree = null;
        this.customer_address = null;
        this.customer_dob = null;
        this.customer_phone = null;
        this.certification = null;
        this.customer_first_name = null;
        this.customer_last_name = null;
        this.customer_email = null;
        this.customer_place_of_certification = null;
        this.customer_certification = null;
        this.customer_company = null;
        this.file = null;
    }

    static create(data) {
        return new CustomerBooking().fill(data);
    }

    fill(data): this {

        if (data.file && data.file.length > 0) {
            data.file = data.file.map(image => Image.create(image));
        }
        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        if (clonedObj.file && clonedObj.file.length > 0) {
            const images = [];
            clonedObj.file.forEach(img => {
                if (img.id) {
                    images.push(img.id);
                }
            });
            clonedObj.file = images;
        } else {
            clonedObj.file = [];
        }

        return clonedObj;
    }
}
