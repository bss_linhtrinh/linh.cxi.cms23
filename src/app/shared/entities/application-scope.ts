import {BaseEntity} from './base.entity';

export class ApplicationScope extends BaseEntity {
    id: number;
    organization_id?: number;
    brand_id: number;
    outlet_id: number;

    constructor() {
        super();
        this.id = null;
        this.organization_id = null;
        this.brand_id = null;
        this.outlet_id = null;
    }
}
