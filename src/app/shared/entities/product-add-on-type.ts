import {BaseEntity} from './base.entity';
import {ProductAddOnTypeAddOn} from './product-add-on-type-add-on';

export class ProductAddOnType extends BaseEntity {
    id: number;
    name: string;
    price: number;
    addons: ProductAddOnTypeAddOn[];
    display_type: string;
    enable_promotion: boolean;
    apply_same_price: boolean;

    // single
    single_options: any[];
    single_add_on_id: number;

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.price = null;
        this.addons = [
            new ProductAddOnTypeAddOn()
        ];
        this.display_type = 'single';
        this.enable_promotion = false;
        this.apply_same_price = false;

        //
        this.single_options = [];
        this.single_add_on_id = null;
    }

    static create(data) {
        return new ProductAddOnType().fill(data);
    }

    fill(data) {
        // add-ons
        if (data.addons) {
            if (data.addons.length > 0) {
                data.addons = data.addons.map(
                    (addOn) => ProductAddOnTypeAddOn.create(addOn)
                );
            } else {
                // if data.addons is empty list
                data.addons.push(new ProductAddOnTypeAddOn());
            }
        }

        return super.fill(data);
    }

    parse() {
        if (this.id && this.addons) {
            const addOns = this.addons
                .filter(
                    addOn => {
                        return addOn.id;
                    }
                )
                .map(
                    addOn => {
                        if (this.apply_same_price) {
                            return {
                                id: addOn.id,
                                price: this.price
                            };
                        } else {
                            return {
                                id: addOn.id,
                                price: addOn.price
                            };
                        }
                    }
                );

            return {
                id: this.id ? this.id : null,
                price: this.price ? this.price : null,
                display_type: this.display_type,
                enable_promotion: this.enable_promotion,
                apply_same_price: this.apply_same_price,
                addons: addOns ? addOns : null
            };
        }
    }
}
