import * as _ from 'lodash';
import * as moment from 'moment';
import {environment} from '../../../environments/environment';

export class BaseEntity {

    translateProps = [];

    constructor() {
    }

    fill(data) {
        const self = this;

        _.each(data, (val, key) => {
            if (self.hasOwnProperty(key)) {
                self[key] = val;
            }
        });

        this.fillTranslate(data.translations, this.translateProps);

        return self;
    }


    // parse & format
    parseDate(date: any) {
        let dateString: string;

        date = moment.utc(date);
        dateString = date.format('YYYY-MM-DDTHH:mm:ss.SSSZ');

        return dateString;
    }

    parseAvatar(avatar: string): string {
        let urlSrc: string;

        if (avatar == null) {
            urlSrc = '/assets/img/avatar-default.jpg';
        } else if (typeof avatar === 'string') {
            if (avatar.includes('data:image/jpeg;') ||
                avatar.includes('data:image/png')) {
                urlSrc = avatar;
            } else {
                urlSrc = `${environment.baseUrl}${avatar}`;
            }
        } else {
            urlSrc = avatar;
        }

        return urlSrc;
    }

    formatHour(hour: string, fromFormat?: string, toFormat?: string): string {
        let momentTime: any;

        if (typeof hour === 'string') {
            if (fromFormat) {
                momentTime = moment(hour, fromFormat);
            } else {
                momentTime = moment(hour);
            }
        }

        if (toFormat) {
            return momentTime.format(toFormat);
        } else {
            return momentTime.format('hh:mm A');
        }
    }

    // string
    capitalize(str: string) {
        return str ? _.startCase(_.toLower(str)) : str;
    }

    // fill translate
    fillTranslate(translations, props?: string[]) {
        const languages = ['en', 'vi'];

        if (translations) {
            for (const i in languages) {
                const code = languages[i];
                const objectTranslator = _.find(translations, ['locale', code]);
                if (objectTranslator) {
                    for (const j in props) {
                        const prop = props[j];
                        if (objectTranslator[prop]) {
                            this[code][prop] = objectTranslator[prop];
                        }
                    }
                }
            }
        } else {
            for (const i in languages) {
                const code = languages[i];
                if (typeof this[code] === 'undefined' && !this[code]) {
                    continue;
                }

                for (const j in props) {
                    const prop = props[j];
                    if (this[prop]) {
                        this[code][prop] = this[prop];
                    }
                }
            }
        }

        return this;
    }

    parseTranslate() {
        const currentLanguage = JSON.parse(localStorage.getItem('cxi|settings.currentlanguage'));

        // set value
        if (currentLanguage &&
            this[currentLanguage]) {
            if (this.translateProps && this.translateProps) {
                for (const index in this.translateProps) {
                    const translateProp = this.translateProps[index];
                    if (translateProp &&
                        this[currentLanguage][translateProp] &&
                        (this[translateProp] === null || this[translateProp] === '')) {
                        this[translateProp] = this[currentLanguage][translateProp];
                    }
                }
            }
        }
    }
}
