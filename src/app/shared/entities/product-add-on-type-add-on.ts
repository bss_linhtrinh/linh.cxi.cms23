import {BaseEntity} from './base.entity';

export class ProductAddOnTypeAddOn extends BaseEntity {
    id: number;
    price: number;

    //
    quantity: number;
    selected: boolean;

    constructor() {
        super();
        this.id = null;
        this.price = null;

        //
        this.quantity = 0;
        this.selected = false;
    }

    static create(data) {
        return new ProductAddOnTypeAddOn().fill(data);
    }
}
