import {BaseEntity} from './base.entity';
import {Image} from './image';
import {BrandScope} from '../types/brand-scope';

export class Brand extends BaseEntity {
    code: string;
    id: number;
    name: string;
    description: string;
    contact_name: string;
    contact_email: string;
    contact_number: string;
    website: string;
    base_currency_id: string;
    status: string;
    is_activated: boolean;
    scope: string;

    image: any;
    image_id: number;

    _images: Image[];

    get images(): Image[] {
        return this._images;
    }

    set images(images: Image[]) {
        this._images = images;
    }

    en: any;
    vi: any;
    translateProps = ['name'];
    translations: any;

    constructor() {
        super();
        this.code = '';
        this.id = null;
        this.name = null;
        this.description = '';
        this.contact_name = '';
        this.contact_email = '';
        this.contact_number = '';
        this.website = '';
        this.base_currency_id = '';
        this.status = '1';
        this.is_activated = null;
        this.image_id = null;
        this.image = null;
        this.scope = 'fb';
        this.images = null;

        this.en = {name: null};
        this.vi = {name: null};
        this.translations = null;
    }

    static create(data): Brand {
        return (new Brand()).fill(data);
    }

    fill(data): this {
        // images
        const images = [];
        if (data.image) {
            images.push(Image.create(data.image));
        }
        if (data.scope) {
            data.scope = data.scope === 'fb'
                ? BrandScope.FB
                : data.scope === 'booking'
                    ? BrandScope.Booking
                    : null;
        }
        this.images = images;

        return super.fill(data);
    }

    parse() {
        this.parseTranslate();
        return {
            id: this.id,
            code: this.code,
            name: this.name,
            description: this.description,
            contact_name: this.contact_name,
            contact_email: this.contact_email,
            contact_number: this.contact_number,
            website: this.website,
            base_currency_id: this.base_currency_id,
            status: this.status,
            is_activated: this.is_activated,

            image_id: this.image_id
                ? this.image_id
                : (
                    this.image && this.image.id
                        ? this.image.id
                        : (
                            this.images && this.images.length > 0 && this.images[0] && this.images[0].id
                                ? this.images[0].id
                                : null
                        )
                ),

            scope: this.scope,

            en: this.en,
            vi: this.vi
        };
    }
}
