import {BaseEntity} from './base.entity';

export class ProductComboItemAddOn extends BaseEntity {
    id: number;
    addon_id: number;
    qty_ordered: number;
    price: number;
    total: number;

    constructor() {
        super();
        this.addon_id = null;
        this.qty_ordered = 1;
        this.price = 0;
        this.total = 0;
    }
}
