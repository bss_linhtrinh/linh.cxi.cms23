import {BaseEntity} from './base.entity';
import {Img} from './img';
import {Image} from './image';
import * as _ from 'lodash';
import {SponsorItem} from './sponsor-item';

export class Sponsor extends BaseEntity {
    id: number;
    name: string;
    domain: string;
    package: string;
    file: any;
    items: [SponsorItem];

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.domain = null;
        this.package = null;
        this.file = null;
        this.items = [new SponsorItem()];
    }

    static create(data) {
        return new Sponsor().fill(data);
    }

    fill(data) {

        // images
        if (data.file) {
            data.file = Image.create(data.file);
        }
        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        if (clonedObj.file) {
            if (clonedObj.file && clonedObj.file.id) {
                clonedObj.file = clonedObj.file.id;
            }
        }
        if (clonedObj.package) {
            clonedObj.items[0].product_id = clonedObj.package;
        }
        if (!clonedObj.id) {
            delete clonedObj.id;
        }

        return clonedObj;
    }
}
