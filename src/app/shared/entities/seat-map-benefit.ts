import {BaseEntity} from './base.entity';
import {Img} from './img';
import {Image} from './image';
import * as _ from 'lodash';

export class SeatMapBenefit extends BaseEntity {
    title: string;
    content: string;


    constructor() {
        super();
        this.title = null;
        this.content = null;
    }

    static create(data) {
        return new SeatMapBenefit().fill(data);
    }

    fill(data) {

        return super.fill(data);
    }

    parse() {

    }
}
