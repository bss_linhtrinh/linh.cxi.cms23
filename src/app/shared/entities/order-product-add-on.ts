import {BaseEntity} from './base.entity';
import * as _ from 'lodash';

export class OrderProductAddOn extends BaseEntity {

    addon_id: number;
    name?: string;
    qty_ordered: number;
    price: number;
    total: number;

    constructor() {
        super();
        this.addon_id = null;
        this.name = null;
        this.qty_ordered = null;
        this.price = null;
        this.total = null;
    }

    static create(data) {
        return new OrderProductAddOn().fill(data);
    }

    fill(data) {
        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);
        delete clonedObj.name;
        return clonedObj;
    }
}
