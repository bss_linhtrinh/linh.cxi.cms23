import {BaseEntity} from './base.entity';
import {Content} from './content';
import * as _ from 'lodash';
import {Image} from './image';
import {ContentModuleImage} from './content-module-image';
import {LocalStorage} from 'ngx-webstorage';

export class Contents extends BaseEntity {

    @LocalStorage('scope.brand_id') brandId: number;

    id: string;
    name: string;
    title: string;
    brand_id: number;
    type: string;
    content: string;
    page_name: string;
    translations: any;
    images: any;
    items: ContentModuleImage[];
    vi: Content;
    en: Content;


    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.title = null;
        this.content = null;
        this.brand_id = null;
        this.type = null;
        this.page_name = null;
        this.images = null;
        this.translations = null;
        this.items = [new ContentModuleImage()];
        this.vi = new Content();
        this.en = new Content();
    }

    static create(data) {
        return new Contents().fill(data);
    }

    fill(data) {
        if (!data.page_name && !data.title && !data.content) {
            data.translations.forEach(item => {
                if (item.page_name) {
                    data.page_name = item.page_name;
                }
                if (item.title) {
                    data.title = item.title;
                }
                if (item.content) {
                    data.content = item.content;
                }
            });
        }
        if (data.translations && data.translations.length > 0) {
            data.translations.forEach(item => {
                if (item.locale === 'vi') {
                    data.vi = Content.create(item);
                }
                if (item.locale === 'en') {
                    data.en = Content.create(item);
                }
            });
        }
        if (data.images && data.images.length > 0) {
            data.images = data.images.map(image => Image.create(image));
        }

        if (data.items) {
            data.items = data.items.map(item => ContentModuleImage.create(item));
        }

        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);
        if (!clonedObj.vi) {
            delete clonedObj.vi;
        }

        if (clonedObj.vi) {
            clonedObj.vi = clonedObj.vi.parse();
        }
        if (!clonedObj.en) {
            delete clonedObj.en;
        }
        if (clonedObj.en) {
            clonedObj.en = clonedObj.en.parse();
        }

        if (clonedObj.images && clonedObj.images.length > 0) {
            const images = [];
            clonedObj.images.forEach(img => {
                if (img.id) {
                    images.push(img.id);
                }
            });
            clonedObj.images = images;
        } else {
            clonedObj.images = [];
        }

        if (clonedObj.type === 'page') {
            delete clonedObj.name;
        }
        if (clonedObj.type === 'module') {
            delete clonedObj.images;
        }
        if (clonedObj.type === 'mail') {
            delete clonedObj.items;
        }
        delete clonedObj.id;
        delete clonedObj.translations;
        delete clonedObj.title;
        delete clonedObj.content;

        if (clonedObj.items && clonedObj.items.length > 0) {
            clonedObj.items = clonedObj.items.map(item => item.parse());
        }

        if (this.brandId) {
            clonedObj.brand_id = this.brandId;
        }

        return clonedObj;
    }
}
