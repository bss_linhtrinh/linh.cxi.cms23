import {BaseEntity} from './base.entity';
import {AttributeOptionTranslation} from './attribute-option-translation';
import {Locale} from './locale';
import {Image} from './image';
import {AttributeTranslation} from './attribute-translation';

export class AttributeOption extends BaseEntity {
    id: number;
    admin_name: string;
    description: string;
    sort_order: number;
    translations: AttributeOptionTranslation[];

    image: Image;
    image_id: number;

    get name(): string {
        return this.admin_name;
    }

    set name(val: string) {
        this.admin_name = val;
    }

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.description = null;
        this.sort_order = null;
        this.translations = [];

        this.image = null;
        this.image_id = null;
    }

    static create(data) {
        return new AttributeOption().fill(data);
    }

    public initOptionTranslations(data: Locale[]) {
        this.translations = data.map(item => {
            return new AttributeOptionTranslation().parseCodeLocale(item);
        });
    }

    fill(data): this {
        if (data.image) {
            data.image = Image.create(data.image);
        }
        if (data.translations &&
            data.translations.length > 0) {
            data.translations = data.translations
                .map(
                    translation => AttributeOptionTranslation.create(translation)
                );
        }
        return super.fill(data);
    }

    parse() {
        return {
            id: this.id,
            admin_name: this.name,
            description: this.description,
            sort_order: this.sort_order,

            translations: this.translations.map(translate => translate.parse()),

            image_id: (this.image && this.image.id)
                ? this.image.id
                : this.image_id
                    ? this.image_id
                    : null
        };
    }
}
