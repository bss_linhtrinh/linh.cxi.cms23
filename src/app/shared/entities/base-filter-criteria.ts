import {BaseEntity} from './base.entity';
import {ColumnFormat} from '../types/column-format';
import {ColumnFilterType} from '../types/column-filter-type';

export class BaseFilterCriteria extends BaseEntity {
    name: string;
    title?: string;
    filterType?: ColumnFilterType;
    filterData?: any[];
    format?: ColumnFormat;
    value: any;
    selected: boolean;

    constructor() {
        super();
        this.name = null;
        this.title = null;
        this.filterType = ColumnFilterType.Text;
        this.filterData = [];
        this.format = ColumnFormat.None;
        this.value = null;
        this.selected = false;
    }

    static from(data: any[]) {
        return data.map(item => this.create(item));
    }

    static create(data) {
        return new BaseFilterCriteria().fill(data);
    }

    static parserList(array: any[]) {
        array = array.map(arr => {
            return this.create(arr);
        });
        return array;
    }
}
