import {BaseEntity} from './base.entity';
import {Image} from './image';
import {formatDate} from '@angular/common';
import * as _ from 'lodash';

export class ResourceNews extends BaseEntity {
    id: number;
    name: string;
    type: string;
    title: string;
    content: string;
    content_image: any;
    en: any;
    vi: any;
    is_activated: boolean;
    translations: any;

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.type = 'news';
        this.title = null;
        this.content = null;
        this.content_image = null;
        this.is_activated = true;
        this.translations = [];
        this.en = {title: null, content: null};
        this.vi = {title: null, content: null};
    }

    get base_image(): any {
        if (this.content_image) {
            return this.content_image._original_image;
        }
        return null;
    }

    set base_image(image: any) {
        if (this.content_image) {
            this.content_image = image;
        }
    }

    static create(data) {
        return new ResourceNews().fill(data);
    }

    fill(data) {
        if (data.content_image) {
            data.content_image = Image.create(data.content_image);
        }
        if (data.translations) {
            this.en = _.find(data.translations, ['locale', 'en']);
            this.vi = _.find(data.translations, ['locale', 'vi']);
        }
        return super.fill(data);
    }

    parse(): object {
        return {
            id: this.id ? this.id : null,
            name: this.name,
            type: this.type,
            booking_type: 'online',
            allow_booking: true,
            start_time: formatDate(new Date().toString(), 'yyyy-MM-dd HH:mm:ss', 'en-US').toString(),
            end_time: formatDate(new Date().toString(), 'yyyy-MM-dd HH:mm:ss', 'en-US').toString(),
            content_image: (this.content_image && this.content_image.id) ? this.content_image.id : '',
            en: this.en,
            vi: this.vi,
        };
    }
}
