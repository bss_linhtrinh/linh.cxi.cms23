import { BaseEntity } from './base.entity';

export class latestCommentReport extends BaseEntity {
   
    id: number;
    constructor() {
        super();
        this.id = null;
    }

    static create(data) {
        return new latestCommentReport().fill(data);
    }
}
