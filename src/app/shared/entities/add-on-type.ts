import { BaseEntity } from './base.entity';

export class AddOnType extends BaseEntity {
    id: number;
    name: string;
    status: boolean;
    description: string;
    display_type: string;
    is_activated: boolean;
    brand_id: number;
    en: object = {};
    vi: object = {};
    translations: any;
    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.description = null;
        this.display_type = 'single';
        this.status = true;
        this.is_activated = false;
        this.brand_id = null;
        this.en = {};
        this.vi = {};
        this.translations = null;
    }

    static create(data) {
        return new AddOnType().fill(data);
    }
}
