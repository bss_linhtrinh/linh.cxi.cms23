import {BaseEntity} from './base.entity';
import * as _ from 'lodash';
import {SeatMapData} from './seat-map-data';
import {Observable, Observer} from 'rxjs';
import {Image as CxiImage} from './image';


export class SeatMap extends BaseEntity {
    title_en: string;
    title_vi: string;
    data: SeatMapData;
    image_ids: any;
    files: any;
    en: any;
    vi: any;


    constructor() {
        super();
        this.title_en = null;
        this.title_vi = null;
        this.data = new SeatMapData();
        this.image_ids = null;
        this.files = null;
        this.en = null;
        this.vi = null;
    }

    static create(data) {
        return new SeatMap().fill(data);
    }

    fill(data) {
        var dataSeat = {};
        dataSeat = data.data;

        // images
        if (data.translations) {
            const en = _.find(data.translations, ['locale', 'en']);
            const vi = _.find(data.translations, ['locale', 'vi']);
            if (en) {
                data.title_en = en.title;
            }
            if (vi) {
                data.title_vi = vi.title;
            }
        }
        if (data.files && data.files.length > 0) {
            data.image_ids = data.files.map(image => CxiImage.create(image));
        }

        if (data.image_ids && data.image_ids.length > 0) {
            data.image_ids.forEach(item => {
                this.getBase64ImageFromURL(item._original_image).subscribe(base64data => {
                    if (base64data) {
                        dataSeat['base64data'] = base64data;
                        data.data = SeatMapData.create(dataSeat);
                    }

                });
            });
        }

        if (dataSeat) {
            data.data = SeatMapData.create(dataSeat);
        }
        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);


        if (clonedObj.image_ids && clonedObj.image_ids.length > 0) {
            const images = [];
            clonedObj.image_ids.forEach(img => {
                if (img.id) {
                    images.push(img.id);
                }
            });
            clonedObj.image_ids = images;
        } else {
            clonedObj.image_ids = [];
        }

        if (clonedObj.data) {
            clonedObj.data = clonedObj.data.parse();
        }

        clonedObj.en = {
            title: clonedObj.title_en
        };
        clonedObj.vi = {
            title: clonedObj.title_vi
        };

        return clonedObj;
    }


    getBase64ImageFromURL(url: string) {
        return Observable.create((observer: Observer<string>) => {
            const img = new Image();
            img.crossOrigin = 'Anonymous';
            img.src = url;
            img.src = url;
            if (!img.complete) {
                img.onload = () => {
                    observer.next(this.getBase64Image(img));
                    observer.complete();
                };
                img.onerror = (err) => {
                    observer.error(err);
                };
            } else {
                observer.next(this.getBase64Image(img));
                observer.complete();
            }
        });
    }

    getBase64Image(img: HTMLImageElement) {
        var canvas = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0);
        var dataURL = canvas.toDataURL('image/png');
        return dataURL;
    }
}
