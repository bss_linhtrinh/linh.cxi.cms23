import { BaseEntity } from './base.entity';
import { Organization } from './organization';
import { Brand } from './brand';
import { Outlet } from './outlets';

export class Filter extends BaseEntity {
    organization: Organization;
    brand: Brand;
    outlet: Outlet;

    constructor() {
        super();
        this.organization = new Organization();
        this.brand = new Brand();
        this.outlet = new Outlet();
    }
}
