import {BaseEntity} from './base.entity';
import * as _ from 'lodash';
import {formatDate} from '@angular/common';

export class TicketBreaktime extends BaseEntity {
    name: string;
    type: string;
    from: string;
    to: string;
    day: string;

    constructor() {
        super();
        this.name = null;
        this.type = null;
        this.from = null;
        this.to = null;
        this.day = null;
    }

    static create(data) {
        return new TicketBreaktime().fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        clonedObj.from = formatDate(clonedObj.from, 'HH:mm', 'en-US').toString();
        clonedObj.to = formatDate(clonedObj.to, 'HH:mm', 'en-US').toString();
        clonedObj.day = formatDate(clonedObj.day, 'yyyy-MM-dd', 'en-US').toString();

        return clonedObj;
    }

}
