import {BaseEntity} from './base.entity';
import {Title} from './title';
import * as _ from 'lodash';
import {formatDate} from '@angular/common';

export class Agenda extends BaseEntity {
    from: string;
    to: string;
    title_en: string;
    title_vi: string;
    speaker: string;
    en: any;
    vi: any;


    constructor() {
        super();
        this.from = null;
        this.to = null;
        this.title_en = null;
        this.title_vi = null;
        this.speaker = null;
        this.en = null;
        this.vi = null;
    }

    static create(data) {
        return new Agenda().fill(data);
    }
    fill(data) {

        // images
        if (data.translations) {
            const en = _.find(data.translations, ['locale', 'en']);
            const vi = _.find(data.translations, ['locale', 'vi']);
            if (en) {
                data.title_en = en.title;
            }
            if (vi) {
                data.title_vi = vi.title;
            }
        }
        return super.fill(data);
    }

    parse(): object {
        const clonedObj = _.cloneDeep(this);

        clonedObj.en = {
            title: clonedObj.title_en
        };
        clonedObj.vi = {
            title: clonedObj.title_vi
        };

        return clonedObj;
    }
}
