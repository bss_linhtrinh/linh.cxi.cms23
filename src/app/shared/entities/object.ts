import {BaseEntity} from './base.entity';
export class ObjectCustom extends BaseEntity {
    value: number;
    unit: string;

    constructor() {
        super();
        this.value = 0;
        this.unit = 'VND';
    }

    static create(data) {
        return new ObjectCustom().fill(data);
    }
}
