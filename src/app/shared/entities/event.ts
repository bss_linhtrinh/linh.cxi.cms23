import {BaseEntity} from './base.entity';
import {Timeline} from './timeline';
import {Sponsor} from './sponsor';
import {Highlight} from './highlight';
import {Speaker} from './speaker';
import {Topic} from './topic';
import {BankInfo} from './bank-info';
import {Reporter} from './reporter';
import {Ticket} from './ticket';
import {List} from './list';
import {BannerList} from './bannerList';
import {Exhibition} from './exhibition';
import {Company} from './company';
import {Image} from './image';
import {formatDate} from '@angular/common';
import {SeatMap} from './seat-map';
import {Letter} from './letter';
import * as _ from 'lodash';
import {del} from 'selenium-webdriver/http';
import {EventGallery} from './event-gallery';
import {BannerGroup} from './banner-group';

export class Event extends BaseEntity {
    id: number;
    title: string;
    title_en: string;
    title_vi: string;
    start_time: string;
    end_time: string;
    location_en: string;
    location_vi: string;
    content_en: string;
    content_vi: string;
    code: string;
    en: any;
    vi: any;
    // detail: Detail;
    promotionDiscount: number;
    vipPromotionDiscount: number;
    currency_code: string;
    target: string;
    priority: number;
    is_feature: boolean;
    payment_type: string;
    is_auto_approved: boolean;
    media: any;
    advertises: any;
    activities: any;
    forums: any;
    booths: any;
    banner_group?: BannerGroup[];
    time_line?: Timeline[];
    sponsors?: Sponsor[];
    highlight_info?: Highlight[];
    speakers?: Speaker[];
    topics?: Topic[];
    bank_info?: BankInfo;
    reporters?: Reporter[];
    tickets?: Ticket[];
    list?: List[];
    banners?: any;
    sponsorPackages: object;
    allow_booking: boolean;
    booking_type: string;
    status: string;
    thumbnail: any;
    content_image: any;
    letters: Letter[];
    exhibitions: Exhibition[];
    companies: Company[];
    type: string;
    translations: any;
    seatmaps: SeatMap[];
    galleries: EventGallery[];


    constructor() {
        super();
        this.id = null;
        this.title = null;
        this.title_en = null;
        this.title_vi = null;
        this.start_time = null;
        this.end_time = null;
        this.location_en = null;
        this.location_vi = null;
        this.content_en = '';
        this.content_vi = '';
        this.code = '';
        this.en = null;
        this.vi = null;
        this.promotionDiscount = 0;
        this.vipPromotionDiscount = 0;
        this.currency_code = 'VND';
        this.target = null;
        this.payment_type = 'bank';
        this.priority = 1;
        this.is_feature = false;
        this.is_auto_approved = false;
        this.media = null;
        this.advertises = null;
        this.activities = null;
        this.forums = null;
        this.booths = null;
        this.banner_group = [new BannerGroup()];
        this.time_line = [new Timeline()];
        this.sponsors = [new Sponsor()];
        this.highlight_info = [new Highlight()];
        this.speakers = [new Speaker()];
        this.topics = [new Topic()];
        this.bank_info = new BankInfo();
        this.reporters = [new Reporter()];
        this.tickets = [new Ticket()];
        this.list = [];
        this.banners = [];
        this.sponsorPackages = {};
        this.allow_booking = true;
        this.booking_type = 'online';
        this.status = null;
        this.thumbnail = null;
        this.content_image = null;
        this.letters = [new Letter()];
        this.exhibitions = [new Exhibition()];
        this.companies = [new Company()];
        this.type = 'event';
        this.translations = null;
        this.seatmaps = [new SeatMap()];
        this.galleries = [new EventGallery()];
    }


    static createAll(data: any[]) {
        return data.map(c => this.create(c));
    }


    static create(data: any) {
        return (new Event()).fill(data);
    }

    fill(data) {

        // if(data.start_time) {
        //     data.start_time = formatDate(data.start_time)
        // }

        // images

        if (data.thumbnail) {
            data.thumbnail = Image.create(data.thumbnail);
        }

        if (data.translations) {
            const en = _.find(data.translations, ['locale', 'en']);
            const vi = _.find(data.translations, ['locale', 'vi']);
            if (en) {
                data.title_en = en.title;
                data.location_en = en.location;
                data.content_en = en.content;
            }
            if (vi) {
                data.title_vi = vi.title;
                data.location_vi = vi.location;
                data.content_vi = vi.content;
            }

        }

        if (data.content_image) {
            data.content_image = Image.create(data.content_image);
        }

        if (data.reporters && data.reporters.length > 0) {
            data.reporters = data.reporters.map(reporter => Reporter.create(reporter));
        }
        if (data.speakers && data.speakers.length > 0) {
            data.speakers = data.speakers.map(speaker => Speaker.create(speaker));
        }
        if (data.tickets && data.tickets.length > 0) {
            data.tickets = data.tickets.map(ticket => Ticket.create(ticket));
        }
        if (data.sponsors && data.sponsors.length > 0) {
            data.sponsors = data.sponsors.map(sponsor => Sponsor.create(sponsor));
        }
        if (data.highlight_info && data.highlight_info.length > 0) {
            data.highlight_info = data.highlight_info.map(highlight => Highlight.create(highlight));
        }
        if (data.bank_info) {
            data.bank_info = BankInfo.create(data.bank_info);
        }
        if (data.topics) {
            data.topics = data.topics.map(topic => Topic.create(topic));
        }
        if (data.exhibitions) {
            data.exhibitions = data.exhibitions.map(exhibition => Exhibition.create(exhibition));
        }
        if (data.companies) {
            data.companies = data.companies.map(company => Company.create(company));
        }
        if (data.activities && data.activities.time_line) {
            data.time_line = data.activities.time_line.map(timeLine => Timeline.create(timeLine));
        }
        if (data.banners) {
            var arr: any;
            var customArr = [];
            arr = _.groupBy(data.banners, (item) => item.group ? item.group : 'group-1');
            Object.values(arr).forEach(item => {
                customArr.push({bannerList: item});
            });
            data.banner_group = customArr.map(bannerGroup => BannerGroup.create(bannerGroup));
        }
        if (data.letters) {
            data.letters = data.letters.map(letter => Letter.create(letter));
        }

        if (data.seatmaps) {
            data.seatmaps = data.seatmaps.map(seatmap => SeatMap.create(seatmap));
        }

        if (data.galleries) {
            data.galleries = data.galleries.map(gallery => EventGallery.create(gallery));
        }
        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        if (clonedObj.thumbnail) {
            if (clonedObj.thumbnail && clonedObj.thumbnail.id) {
                clonedObj.thumbnail = clonedObj.thumbnail.id;
            }
        }

        if (clonedObj.content_image) {
            if (clonedObj.content_image && clonedObj.content_image.id) {
                clonedObj.content_image = clonedObj.content_image.id;
            }
        }


        if (clonedObj.reporters && clonedObj.reporters.length > 0) {
            clonedObj.reporters = clonedObj.reporters.map(reporter => reporter.parse());
        }

        if (clonedObj.speakers && clonedObj.speakers.length > 0) {
            clonedObj.speakers = clonedObj.speakers.map(speaker => speaker.parse());
        }

        if (clonedObj.tickets && clonedObj.tickets.length > 0) {
            clonedObj.tickets = clonedObj.tickets.map(ticket => ticket.parse());
        }

        if (clonedObj.sponsors && clonedObj.sponsors.length > 0) {
            clonedObj.sponsors = clonedObj.sponsors.map(sponsor => sponsor.parse());
        }

        if (clonedObj.highlight_info && clonedObj.highlight_info.length > 0) {
            clonedObj.highlight_info = clonedObj.highlight_info.map(highlight => highlight.parse());
        }

        if (clonedObj.topics && clonedObj.topics.length > 0) {
            clonedObj.topics = clonedObj.topics.map(topic => topic.parse());
        }

        // if (clonedObj.time_line && clonedObj.time_line.length > 0) {
        //     clonedObj.time_line = clonedObj.time_line.map(timeLine => timeLine.parse());
        // }

        if (clonedObj.exhibitions && clonedObj.exhibitions.length > 0) {
            clonedObj.exhibitions = clonedObj.exhibitions.map(exhibition => exhibition.parse());
        }

        if (clonedObj.companies && clonedObj.companies.length > 0) {
            clonedObj.companies = clonedObj.companies.map(company => company.parse());
        }

        if (clonedObj.seatmaps && clonedObj.seatmaps.length > 0) {
            clonedObj.seatmaps = clonedObj.seatmaps.map(seatMap => seatMap.parse());
        }

        if (clonedObj.banner_group && clonedObj.banner_group.length > 0) {
            clonedObj.banners = [];
            clonedObj.banner_group = clonedObj.banner_group.map(banner => banner.parse());
            clonedObj.banner_group.forEach((item, index) => {
                item.bannerList.forEach(i => {
                    i.group = 'group-' + (index + 1);
                    clonedObj.banners.push(i);
                });
            });
        }
        if (clonedObj.letters && clonedObj.letters.length > 0) {
            clonedObj.letters = clonedObj.letters.map(letter => letter.parse());
        }

        if (clonedObj.galleries && clonedObj.galleries.length > 0) {
            clonedObj.galleries = clonedObj.galleries.map(gallery => gallery.parse());
        }

        clonedObj.en = {
            title: clonedObj.title_en,
            location: clonedObj.location_en,
            content: clonedObj.content_en
        };
        clonedObj.vi = {
            title: clonedObj.title_vi,
            location: clonedObj.location_vi,
            content: clonedObj.content_vi
        };

        if (clonedObj.activities) {
            delete clonedObj.activities;
        }

        clonedObj.start_time = formatDate(clonedObj.start_time, 'yyyy-MM-dd HH:mm:ss', 'en-US').toString();
        clonedObj.end_time = formatDate(clonedObj.end_time, 'yyyy-MM-dd HH:mm:ss', 'en-US').toString();


        return clonedObj;
    }
}
