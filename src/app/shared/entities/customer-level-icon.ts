import {BaseEntity} from './base.entity';
import {Image} from './image';
import {CustomerLevel} from './customer-level';
import {Brand} from './brand';

export class CustomerLevelIcon extends BaseEntity {
    id: number;

    customer_level_id: number;
    customer_level: CustomerLevel;

    brand_id: number;
    brand: Brand;

    image: Image;
    image_id: number;

    constructor() {
        super();
        this.id = null;
        this.customer_level_id = null;
        this.customer_level = null;
        this.brand_id = null;
        this.brand = null;
        this.image = null;
        this.image_id = null;
    }

    static create(data: any) {
        return (new CustomerLevelIcon()).fill(data);
    }

    fill(data): this {
        if (data.image) {
            data.image = Image.create(data.image);
        }
        return super.fill(data);
    }

    parse() {
        return {
            customer_level_id: this.customer_level_id,
            brand_id: this.brand_id,
            image_id: (this.image && this.image.id) ? this.image.id : null
        };
    }
}
