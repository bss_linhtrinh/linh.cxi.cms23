import {BaseEntity} from './base.entity';
import * as _ from 'lodash';

export class OrderPromotion extends BaseEntity {
    id: number;
    order_id: number;
    promotion_id: number;
    name: string;
    code: string;
    discount_amount?: number;
    maximum_discount?: number;

    constructor() {
        super();
        this.id = null;
        this.order_id = null;
        this.promotion_id = null;
        this.name = null;
        this.code = null;
        this.discount_amount = null;
        this.maximum_discount = null;
    }

    static create(data) {
        return new OrderPromotion().fill(data);
    }

    fill(data) {

        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        return clonedObj;
    }
}
