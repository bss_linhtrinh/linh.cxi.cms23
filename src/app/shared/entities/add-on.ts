import {BaseEntity} from './base.entity';
import {Image} from './image';
import * as _ from 'lodash';

export class AddOn extends BaseEntity {
    id: number;
    name: string;
    status: boolean;
    image: string;
    description: string;
    is_activated: boolean;
    type: object;
    brand_id: number;
    en: object = {};
    vi: object = {};
    translations: any;
    _images: Image[];

    get images(): Image[] {
        return this._images;
    }

    set images(images: Image[]) {
        this._images = images;
    }

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.status = true;
        this.image = '';
        this.images = null;
        this.description = null;
        this.is_activated = false;
        this.type = null;
        this.brand_id = null;
        this.en = {};
        this.vi = {};
        this.translations = null;
    }

    fill(data): this {
        // images
        const images = [];
        if (data.image) {
            images.push(Image.create(data.image));
        }
        this.images = images;

        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        // parse image
        if (clonedObj.images && clonedObj.images.length > 0) {
            if (clonedObj.images[0] && clonedObj.images[0].path_image) {
                clonedObj.image = clonedObj.images[0].path_image;
            }
        }

        return clonedObj;
    }
}
