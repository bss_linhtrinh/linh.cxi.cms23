import {BaseEntity} from './base.entity';
import {Image} from './image';
import {CommissionOutlet} from './commission-outlet';
import {Brand} from './brand';

export class Commission extends BaseEntity {
    id: number;
    name: string;
    brand_id: number;
    brand: Brand;
    outlets: CommissionOutlet[];

    all_outlets: number;
    all_outlets_percentage: number;
    all_outlets_amount: number;

    image: Image;
    image_id: number;

    get application_scope(): string {
        return this.brand ? this.brand.name : null;
    }

    get commission(): number {
        return this.all_outlets
            ? (
                this.all_outlets_percentage
                    ? this.all_outlets_percentage
                    : null
            )
            : (
                this.outlets &&
                this.outlets.length > 0 &&
                this.outlets[0] &&
                this.outlets[0].percentage
                    ? this.outlets[0].percentage
                    : null
            );
    }

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.brand_id = null;
        this.brand = null;
        this.outlets = [new CommissionOutlet()];
        this.all_outlets = 1;
        this.all_outlets_percentage = 0;
        this.all_outlets_amount = 0;
        this.image = null;
        this.image_id = null;
    }

    static create(data) {
        return new Commission().fill(data);
    }

    fill(data): this {
        // images
        if (data.image) {
            data.image = Image.create(data.image);
        }

        if (data.brand && data.brand.id) {
            data.brand_id = data.brand.id;
        }

        if (data.outlets) {
            data.outlets = data.outlets.map(outlet => CommissionOutlet.create(outlet));
        }

        return super.fill(data);
    }

    parse() {
        return {
            id: this.id,
            name: this.name,
            brand_id: this.brand_id,
            image_id: (this.image && this.image.id) ? this.image.id : null,

            all_outlets: this.all_outlets,
            all_outlets_percentage: this.all_outlets && this.all_outlets_percentage
                ? this.all_outlets_percentage
                : 0,
            all_outlets_amount: this.all_outlets && this.all_outlets_amount
                ? this.all_outlets_amount
                : 0,

            outlets: this.outlets
                .filter(outlet => outlet.outlet_id)
                .map(outlet => outlet.parse()),
        };
    }
}
