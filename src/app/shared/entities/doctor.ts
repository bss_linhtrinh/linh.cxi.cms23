import { BaseEntity } from './base.entity';
import {Image} from './image';
import * as _ from 'lodash';
import {environment} from '../../../environments/environment';

export class Doctor extends BaseEntity {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    dob: string;
    gender: string;
    cell_phone: string;
    home_telephone: string;
    nationality: string;
    foreign_language: any;
    level: any;
    home_address: string;
    clinic_address: string;
    health_center: any;
    password: string;
    password_confirmation: string;
    language: string;
    member_since: string;
    clinical_skills: any;
    status: number; // un-active, active, deactivate
    approved: boolean;
    start_date: string;
    is_activated: boolean;
    private _avatar: string | any;
    brand_id: number;
    translations: any;
    en: object = {};
    vi: object = {};
    _images: Image[];

    get name(): string {
        if (!this.first_name) {
            return `${this.last_name}`;
        }

        if (!this.last_name) {
            return `${this.first_name}`;
        }

        return `${this.first_name} ${this.last_name}`;
    }

    get doctor_level(): string {
        if (!this.level) {
            return `--`;
        }

        return `${this.level.name}`;
    }

    get avatar(): string {
        if (this._avatar === null) {
            return `/assets/img/avatar-default.jpg`;
        } else if (
            this._avatar.includes('data:image/jpeg;') ||
            this._avatar.includes('data:image/png')
        ) {
            return this._avatar;
        } else {
            return `${environment.baseUrl}${this._avatar}`;
        }
    }
    set avatar(val: string) {
        this._avatar = val;
    }

    constructor() {
        super();
        this.id = null;
        this.first_name = null;
        this.last_name = null;
        this.email = null;
        this.dob = null;
        this.gender = 'male';
        this.cell_phone = null;
        this.home_telephone = null;
        this.nationality = null;
        this.foreign_language = null;
        this.level = null;
        this.home_address = null;
        this.clinic_address = null;
        this.health_center = null;
        this.password = null;
        this.password_confirmation = null;
        this.language = null;
        this.member_since = null;
        this.clinical_skills = null;
        this.status = 1;
        this.is_activated = true;
        this.avatar = null;
        this.brand_id = null;
        this.en = {};
        this.vi = {};
        this.translations = [];
    }

    static create(data) {
        return new Doctor().fill(data);
    }

    fill(data): this {
        if (data.avatar) {
            this.avatar = data.avatar;
        }

        return super.fill(data);
    }

    parse() {
        return {
            id: this.id,
            first_name: this.first_name,
            last_name: this.last_name,
            email: this.email,
            gender: this.gender,
            dob: this.dob,
            cell_phone: this.cell_phone,
            home_telephone: this.home_telephone,
            nationality: this.nationality,
            foreign_language: this.foreign_language,
            level: this.level,
            home_address: this.home_address,
            clinic_address: this.clinic_address,
            health_center: this.health_center,
            password: this.password,
            password_confirmation: this.password_confirmation,
            language: this.language,
            member_since: this.member_since,
            clinical_skills: this.clinical_skills,
            brand_id: this.brand_id,
            avatar: this._avatar,
            is_activated: this.is_activated,
            start_date: this.start_date,
            status: this.status,
            approved: this.approved,
        };
    }

}
