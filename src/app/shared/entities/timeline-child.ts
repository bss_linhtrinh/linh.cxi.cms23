import {BaseEntity} from './base.entity';
import {Agenda} from './agenda';
import {BannerAgenda} from './bannerAgenda';
import * as _ from 'lodash';

export class TimelineChild extends BaseEntity {
    id: number;
    banner?: BannerAgenda;
    agenda?: Agenda[];

    constructor() {
        super();
        this.id = null;
        this.banner = new BannerAgenda();
        this.agenda = [new Agenda()];
    }

    static create(data) {
        return new TimelineChild().fill(data);
    }

    fill(data): this {
        if (data.banner) {
            data.banner = BannerAgenda.create(data.banner);
        }
        if (data.agenda) {
            data.agenda = data.agenda.map(agenda => BannerAgenda.create(agenda));
        }
        return super.fill(data);
    }
}
