import { BaseEntity } from './base.entity';

export class platformOverviewReport extends BaseEntity {

    id: number;
    total: number;
    platform: string;

    constructor() {
        super();
        this.total = null;
        this.platform = null;
    }

    static create(data) {
        return new platformOverviewReport().fill(data);
    }
}
