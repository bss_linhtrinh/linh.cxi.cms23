import {BaseEntity} from './base.entity';

export class BankInfo extends BaseEntity {
    owner: string;
    bank: string;
    account_number: number;
    account_name: string;

    constructor() {
        super();
        this.owner = null;
        this.bank = null;
        this.account_number = null;
        this.account_name = null;
    }

    static create(data) {
        return new BankInfo().fill(data);
    }
}
