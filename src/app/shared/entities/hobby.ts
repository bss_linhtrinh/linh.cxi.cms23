import { BaseEntity } from './base.entity';

export class Hobby extends BaseEntity {
    id: number;
    name: string;
    description: string;
    brand_id: number;
    en: object = {};
    vi: object = {};
    translations: any;
    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.description = null;
        this.brand_id = null;
        this.en = {};
        this.vi = {};
        this.translations = null;
    }

    static create(data) {
        return new Hobby().fill(data);
    }
}
