import {BaseEntity} from './base.entity';
import {ProductVariant} from './product-variant';
import {Image} from './image';
import {Attribute} from './attribute';
import {ProductAddOnType} from './product-add-on-type';
import {ProductCombo} from './product-combo';
import {ProductType} from '../types/product-type';
import {ProductOutletQuantity} from './product-outlet-quantity';

export class Product extends BaseEntity {
    id: number;
    name: string;
    type: ProductType;
    price_type: string;
    price: number;
    description: string;
    sku: string;
    sold_out: boolean;
    status: boolean;
    brand_id: number;
    categories: number[];
    addon_types?: ProductAddOnType[];
    super_attributes: number[];
    variants: ProductVariant[];
    combos: ProductCombo[];
    size: any;
    all_outlets: boolean;
    application_scope: string;

    images: Image[];

    get base_image(): Image {
        if (this.images && this.images.length > 0) {
            return this.images[0];
        }
        return null;
    }

    set base_image(image: Image) {
        if (this.images) {
            this.images[0] = image;
        }
    }

    cover_image: Image;
    display_image: Image;

    get is_activated(): boolean {
        return this.status;
    }

    set is_activated(is_activated: boolean) {
        this.status = is_activated;
    }

    is_stand_out: boolean;

    outlets: ProductOutletQuantity[];

    en: any;
    vi: any;
    translateProps = ['name', 'description'];

    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.type = ProductType.Simple;
        this.price_type = 'fixed';
        this.price = 0;
        this.description = '';
        this.sku = null;
        this.sold_out = false;
        this.status = true;
        this.brand_id = null;

        this.categories = [];
        this.addon_types = [];
        this.super_attributes = [];
        this.variants = [];
        this.combos = [];

        this.outlets = [];

        this.images = null;
        this.base_image = null;
        this.cover_image = null;
        this.display_image = null;

        this.all_outlets = true;
        this.application_scope = 'All'; // 'All', 'Specific outlet'

        this.is_stand_out = false;

        this.en = {name: null, description: null};
        this.vi = {name: null, description: null};
    }

    static create(data) {
        return new Product().fill(data);
    }

    fill(data) {
        // type
        if (data.type) {
            data.type = (data.type === 'simple')
                ? ProductType.Simple
                : (
                    data.type === 'configurable'
                        ? ProductType.Configurable
                        : (
                            data.type === 'combo'
                                ? ProductType.Combo
                                : ProductType.Simple
                        )
                );
        }

        // categories
        if (data.categories) {
            data.categories = data.categories.map(
                category => (typeof category === 'object' && category.id) ? category.id : category
            );
        }

        // brand & brand_id
        if (data.brand && data.brand) {
            data.brand_id = data.brand.id;
        }

        // outlets
        if (data.outlets && data.outlets.length > 0) {
            const outlets = [];
            for (const index in data.outlets) {
                const outlet = data.outlets[index];
                let item: any;
                if (typeof outlet === 'number') {
                    item = ProductOutletQuantity.create({outlet_id: outlet, quantity: null});
                } else {
                    item = ProductOutletQuantity.create({outlet_id: outlet.id, quantity: outlet.quantity});
                }
                outlets.push(item);
            }
            data.outlets = outlets;
        }

        // images
        if (data.images) {
            data.images = data.images.map(image => Image.create(image));
        }
        if (data.cover_image) {
            data.cover_image = Image.create(data.cover_image);
        }
        if (data.display_image) {
            data.display_image = Image.create(data.display_image);
        }

        if (data.type === 'simple') {

            // add-on types
            if (data.addon_types) {
                data.addon_types = data.addon_types.map(a => ProductAddOnType.create(a));
            }

        } else if (data.type === 'configurable') {

            // super-attributes
            if (data.super_attributes) {
                data.super_attributes = data.super_attributes.map(a => Attribute.create(a));
            }

            // variants
            if (data.variants) {
                data.variants = data.variants.map(
                    (variant: ProductVariant) => ProductVariant.create(variant, data.super_attributes)
                );
            }

            // super-attributes
            if (data.super_attributes) {
                data.super_attributes = data.super_attributes.map(
                    (attr: Attribute) => attr.id
                );
            }
        } else if (data.type === 'combo') {
            // combo
            if (data.combos) {
                data.combos = data.combos.map(a => ProductCombo.create(a));
            }
        }

        // all_outlets
        if (data.all_outlets) {
            data.application_scope = 'All';
        } else {
            data.application_scope = 'Specific outlet';
        }

        return super.fill(data);
    }

    parse(): object {
        this.parseTranslate();

        return {
            id: this.id,
            name: this.name,
            type: this.type,
            price_type: this.price_type,
            price: this.price,
            description: this.description,
            sku: this.sku,
            sold_out: this.sold_out,
            is_stand_out: this.is_stand_out,
            status: this.status,
            brand_id: this.brand_id,
            categories: this.categories && this.categories.length > 0
                ? this.categories
                : [],

            // images
            images: this.images && this.images.length > 0
                ? this.images.map(img => img.id)
                : [],
            cover_image: this.cover_image && this.cover_image.id ? this.cover_image.id : null,
            display_image: this.display_image && this.display_image.id ? this.display_image.id : null,

            all_outlets: !!(this.application_scope === 'All'),
            application_scope: this.application_scope,

            // configurable
            variants: this.variants && this.variants.length > 0
                ? this.variants.map(variant => variant.parse())
                : null,
            super_attributes: this.super_attributes,

            // combo
            combos: this.combos && this.combos.length > 0
                ? this.combos.map(c => c.parse())
                : null,

            outlets: this.parseProductOutletQuantity(),

            en: this.en,
            vi: this.vi
        };
    }

    parseProductOutletQuantity() {
        const outletsQuantity: any = {};
        this.outlets.forEach(
            (outlet: ProductOutletQuantity) => {
                outletsQuantity[outlet.outlet_id] = {quantity: outlet.quantity};
            }
        );
        return outletsQuantity;
    }
}
