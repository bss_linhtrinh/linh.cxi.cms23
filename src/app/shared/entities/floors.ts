import { BaseEntity } from './base.entity';
import * as _ from 'lodash';
import { Image } from './image';

export class Floors extends BaseEntity {
    id: number;
    name: string;
    outlet_id: number;
    brand_id: number;
    
    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.outlet_id = null;
        this.brand_id = null;
    }

    static create(data): Floors {
        return (new Floors()).fill(data);
    }
    
}
