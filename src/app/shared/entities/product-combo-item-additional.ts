import {BaseEntity} from './base.entity';

export class ProductComboItemAdditional extends BaseEntity {
    id: number;
    quantity: number;
    attributes: any;
    is_configurable: boolean;
    super_attribute: any;
    selected_configurable_option: number;

    constructor() {
        super();
        this.quantity = null;
        this.attributes = null;
        this.is_configurable = false;
        this.super_attribute = null;
        this.selected_configurable_option = null;
    }
}
