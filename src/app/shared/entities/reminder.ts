import { BaseEntity } from './base.entity';

export class Reminder extends BaseEntity {
    _id: string;
    brandId: number;
    outletId: any;
    title: string;
    startDate: string;
    sendTo: any;
    hour: number;
    timeHour: number;
    minute: number;
    timeMinute: number;
    meridiem: string;
    timeMeridiem: string;
    repeatType: string;
    days: any;
    endAfter: number;
    updatedAt: string;
    createdAt: string;
    status: boolean;
    active: boolean;
    translations: any;
    get id(): string {
        return this._id;
    }
    get is_activated(): boolean {
        return this.active;
    }

    set is_activated(is_activated: boolean) {
        this.active = is_activated;
    }
    constructor() {
        super();
        this._id = null;
        this.brandId = null;
        this.outletId = [];
        this.title = null;
        this.startDate = null;
        this.sendTo = [];
        this.minute = null;
        this.timeMinute = null;
        this.meridiem = null;
        this.timeMeridiem = null;
        this.hour = null;
        this.timeHour = null;
        this.repeatType = 'daily';
        this.days = [];
        this.endAfter = 1;
        this.updatedAt = null;
        this.createdAt = null;
        this.status = false;
        this.active = false;
        this.translations = [
            { name: null, locale: 'en' },
            { name: null, locale: 'vi' },
        ];
        this.brandId = null;
        this.outletId = [];
    }

    static create(data) {
        return new Reminder().fill(data);
    }
    parse() {
        return {
            id: this._id,
            brandId: this.brandId,
            outletId: this.outletId,
            title: this.title,
            startDate: this.startDate,
            sendTo: this.sendTo,
            minute: this.minute,
            meridiem: this.meridiem,
            hour: this.hour,
            repeatType: this.repeatType,
            days: this.days,
            updatedAt: this.updatedAt,
            endAfter: this.endAfter,
            translations: this.translations
        };
    }
}
