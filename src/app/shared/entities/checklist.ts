import {BaseEntity} from './base.entity';
import * as _ from 'lodash';

export class Checklist extends BaseEntity {
    _id: string;
    name: string;
    tasks: string[];
    allOutlets: boolean;
    brandId: number;
    outletId: number[];
    startDate: string;
    shift: string;
    repeatType: string;
    endAfter: number;
    days: number[];
    applicationScope: string;
    createdAt: string;
    updatedAt: string;
    taskTotal: number;
    isShiftAssigned: number;
    estimatedEndTime: string;
    active: boolean;
    translations: any;

    get id(): string {
        return this._id;
    }

    get is_activated(): boolean {
        return this.active;
    }

    set is_activated(is_activated: boolean) {
        this.active = is_activated;
    }

    constructor() {
        super();
        this._id = null;
        this.name = null;
        this.tasks = [];
        this.brandId = null;
        this.outletId = [];
        this.allOutlets = true;
        this.startDate = new Date().toString();
        this.shift = null;
        this.repeatType = 'none';
        this.endAfter = 0;
        this.days = [];
        this.applicationScope = 'All';
        this.createdAt = null;
        this.updatedAt = null;
        this.taskTotal = 0;
        this.isShiftAssigned = 0;
        this.estimatedEndTime = null;
        this.active = false;
        this.translations = [
            {name: null, locale: 'en'},
            {name: null, locale: 'vi'},
        ];
    }

    static create(data) {
        return new Checklist().fill(data);
    }

    fill(data) {
        if (data.allOutlets) {
            data.applicationScope = 'All';
        } else {
            data.applicationScope = 'Specific outlet';
        }

        return super.fill(data);
    }

    parse(): object {
        if (this.isShiftAssigned === 1) {
            return {
                tasks: this.tasks,
                translations: this.translations
            };
        }
        if (this.isShiftAssigned === 0) {
            return {
                tasks: this.tasks,
                brandId: this.brandId,
                outletId: this.outletId,
                startDate: this.startDate,
                shift: this.shift,
                repeatType: this.repeatType,
                endAfter: this.endAfter,
                days: this.days,
                allOutlets: !!(this.applicationScope === 'All'),
                translations: this.translations
            };
        }
    }
}
