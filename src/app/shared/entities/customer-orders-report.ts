import { BaseEntity } from './base.entity';

export class customerOrdersReport extends BaseEntity {

    id: number;
    name:string;
    email:string;
    phone: string;
    address: string;
    level: string;
    reward_points: number;
    total_orders: number;
    latest_order: string;
    payment_total: number;
    total_paid: number;
    orders: any;

    constructor() {
        super();
        this.id = null;
        this.name=null;
        this.email=null;
        this.phone=null;
        this.address=null;
        this.level=null;
        this.reward_points= null;
        this.total_orders= null;
        this.latest_order=null;
        this.payment_total= null;
        this.orders= null;
        this.total_paid= null;
    }

    static create(data) {
        return new customerOrdersReport().fill(data);
    }
}
