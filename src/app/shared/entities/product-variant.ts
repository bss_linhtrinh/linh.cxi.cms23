import {BaseEntity} from './base.entity';
import {ProductVariantAttribute} from './product-variant-attribute';
import {Attribute} from './attribute';
import {ProductAddOnType} from './product-add-on-type';

export class ProductVariant extends BaseEntity {
    id: number;

    name: string;
    sku: string;
    price: number;
    status: boolean;

    attributes: ProductVariantAttribute[] | any;
    addon_types: ProductAddOnType[] | any;

    constructor() {
        super();
        this.id = null;

        this.name = null;
        this.sku = (new Date().getTime()).toString();
        this.price = null;
        this.status = true;

        this.attributes = [
            // new ProductVariantAttribute()
        ];
        this.addon_types = [
            new ProductAddOnType()
        ];
    }

    static create(data: any, super_attributes?: Attribute[]) {
        return new ProductVariant().fill(data, super_attributes);
    }

    fill(data: any, super_attributes?: Attribute[]) {
        // attributes
        if (data.attributes) {
            const attributes = [];
            for (const index in data.attributes) {
                const attribute = super_attributes.find(
                    (att: Attribute) => att.id === parseInt(index, 10)
                );
                if (attribute) {
                    const newAttr = ProductVariantAttribute.create({
                        id: attribute.id,
                        name: attribute.name,
                        options: attribute.options,
                        value: parseInt(data.attributes[index], 10)
                    });
                    attributes.push(newAttr);
                }
            }
            data.attributes = attributes;
        }

        // addon_types
        if (data.addon_types) {
            data.addon_types = data.addon_types
                .map(
                    (addOnType) => ProductAddOnType.create(addOnType)
                );
        }

        return super.fill(data);
    }

    parse() {
        const objAttributes = {};
        this.attributes.forEach(
            (attribute: ProductVariantAttribute) => {
                if (attribute.id && attribute.value) {
                    objAttributes[attribute.id] = attribute.value;
                }
            }
        );

        return {
            id: this.id,
            name: this.name,
            sku: this.sku,
            price: this.price,
            status: this.status,
            attributes: objAttributes,
            addon_types: this.addon_types
                ? this.addon_types.map(a => a.parse())
                : []
        };
    }
}
