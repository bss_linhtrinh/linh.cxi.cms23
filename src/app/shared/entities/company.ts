import {BaseEntity} from './base.entity';
import {Img} from './img';
import {Image} from './image';
import * as _ from 'lodash';

export class Company extends BaseEntity {
    id: number;
    company_name: string;
    company_website: string;
    company_logo: any;


    constructor() {
        super();
        this.id = null;
        this.company_name = null;
        this.company_website = null;
        this.company_logo = null;
    }

    static create(data) {
        return new Company().fill(data);
    }

    fill(data) {

        // images
        if (data.company_logo) {
            data.company_logo = Image.create(data.company_logo);
        }
        return super.fill(data);
    }

    parse() {
        const clonedObj = _.cloneDeep(this);

        if (clonedObj.company_logo) {
            if (clonedObj.company_logo && clonedObj.company_logo.id) {
                clonedObj.company_logo = clonedObj.company_logo.id;
            }
        }

        return clonedObj;
    }
}
