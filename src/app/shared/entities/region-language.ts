import {BaseEntity} from './base.entity';

export class RegionLanguage extends BaseEntity {
    language: string;

    constructor() {
        super();
        this.language = 'en';
    }

    static create(data): RegionLanguage {
        return (new RegionLanguage()).fill(data);
    }

    fill(data): this {
        return super.fill(data);
    }
}
