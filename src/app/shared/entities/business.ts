import {BaseEntity} from './base.entity';
import {DEFAULT_COUNTRY_CODE_UK} from '../constants/constants';

export class Business extends BaseEntity {
    id: number;
    name: string;
    description: string;
    address: string;
    address_line1: string;
    address_line2: string;
    postcode: number;
    date_started: string;
    currency_id: number;
    country_id: number;
    cis_registered: boolean;
    vat_registered: boolean;
    vat_number: string;
    cash_basis_accounting: boolean;
    number_of_transactions: number;
    number_of_invoices: number;


    constructor() {
        super();
        this.id = null;
        this.name = null;
        this.description = null;
        this.address = null;
        this.address_line1 = null;
        this.address_line2 = null;
        this.postcode = null;
        this.date_started = (new Date()).toDateString();
        this.currency_id = 1;
        this.country_id = DEFAULT_COUNTRY_CODE_UK;
        this.cis_registered = false;
        this.vat_registered = false;
        this.vat_number = null;
        this.cash_basis_accounting = false;
        this.number_of_transactions = null;
        this.number_of_invoices = null;
    }
}
