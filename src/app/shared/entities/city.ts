import { BaseEntity } from './base.entity';

export class City extends BaseEntity {
    id: number;
    name: string;

    constructor() {
        super();
        this.id = null;
        this.name = null;
    }

    static create(data) {
        return new City().fill(data);
    }
}
