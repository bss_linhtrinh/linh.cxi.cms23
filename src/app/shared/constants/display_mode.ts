export const Modes = [
    { name: 'products and description', value: 'products_and_description' },
    { name: 'products', value: 'products_only' },
    { name: 'description', value: 'description_only' }
]