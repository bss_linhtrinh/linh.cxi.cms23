export const ROLE_PERMISSIONS = [
    {
        id: 'user-admin',
        key: 'user-admin',
        module: 'User',
        view: 0,
        create: 0,
        edit: 0,
        delete: 0
    },
    {
        id: 'attribute',
        key: 'attribute',
        module: 'Attribute',
        view: 0,
        create: 0,
        edit: 0,
        delete: 0
    },
    {
        id: 'attribute-family',
        key: 'attribute-family',
        module: 'Attribute Family',
        view: 0,
        create: 0,
        edit: 0,
        delete: 0
    },
    {
        id: 'brand',
        key: 'brand',
        module: 'Brand',
        view: 0,
        create: 0,
        edit: 0,
        delete: 0
    },
    {
        id: 'outlet',
        key: 'outlet',
        module: 'Outlet',
        view: 0,
        create: 0,
        edit: 0,
        delete: 0
    },
    {
        id: 'category',
        key: 'category',
        module: 'Category',
        view: 0,
        create: 0,
        edit: 0,
        delete: 0
    },
    {
        id: 'role',
        key: 'role',
        module: 'Role',
        view: 0,
        create: 0,
        edit: 0,
        delete: 0
    },
    {
        id: 'media',
        key: 'media',
        module: 'Media',
        view: 0,
        create: 0,
        edit: 0,
        delete: 0
    },
    {
        id: 'order',
        key: 'order',
        module: 'Order',
        view: 0,
        create: 0,
        edit: 0,
        delete: 0
    },
    {
        id: 'organization',
        key: 'organization',
        module: 'Organization',
        view: 0,
        create: 0,
        edit: 0,
        delete: 0
    },
    {
        id: 'product',
        key: 'product',
        module: 'Product',
        view: 0,
        create: 0,
        edit: 0,
        delete: 0
    },
    {
        id: 'setting',
        key: 'setting',
        module: 'Setting',
        view: 0,
        create: 0,
        edit: 0,
        delete: 0
    },
    {
        id: 'customer',
        key: 'customer',
        module: 'Customer',
        view: 0,
        create: 0,
        edit: 0,
        delete: 0
    }
];
