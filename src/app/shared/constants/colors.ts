export const COLORS = [
    {borderColor: '#3b48a5', backgroundColor: '#3b48a5'}, // primary
    {borderColor: '#15a5de', backgroundColor: '#15a5de'}, // accent
    {borderColor: '#0097ff', backgroundColor: '#0097ff'}, // blue
    {borderColor: '#00cc00', backgroundColor: '#00cc00'}, // green
    {borderColor: '#ff6d00', backgroundColor: '#ff6d00'}, // orange
    {borderColor: '#f1295c', backgroundColor: '#f1295c'}, // red
    {borderColor: '#6737e1', backgroundColor: '#6737e1'}, // purple
    {borderColor: '#FFC70F', backgroundColor: '#FFC70F'}, // yellow
];
