export const CHART_COLOR = [
    { // income
        borderColor: '#00b748',
        backgroundColor: '#ffffff',
    },
    { // expense
        borderColor: '#f2665e',
        backgroundColor: '#ffffff',
    }
];
export const CHART_TYPE = 'bar';
export const CHART_OPTIONS = {
    responsive: true,
    responsiveAnimationDuration: 500,
    maintainAspectRatio: false,
    layout: {
        padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
        }
    },
    animation: {
        easing: 'linear'
    },
    legend: {
        display: true,
        position: 'top',
        labels: {
            padding: 30,
            fontColor: '#3f4041',
            fontSize: 12,
            boxWidth: 10
        }
    },
    tooltips: {
        enabled: false,
        mode: 'x',
        intersect: false,
        position: 'nearest',
        backgroundColor: 'var(--dark-05)',
        titleMarginBottom: 5,
        padding: 10,
        callbacks: {
            label: function (tooltipItem, data) {
                let label = data.datasets[tooltipItem.datasetIndex].label || '';
                let value = formatCurrency(tooltipItem.yLabel);
                return {label: label, value: value};
            },
            labelColor: function (tooltipItem) {
                let colors = CHART_COLOR[tooltipItem.datasetIndex];
                return {
                    borderColor: colors.borderColor,
                    backgroundColor: colors.backgroundColor
                };
            },
        },
        custom: function (tooltipModel) {

            // Tooltip Element
            let tooltipEl = document.getElementById('chartjs-tooltip');

            // Create element on first render
            if (!tooltipEl) {
                tooltipEl = document.createElement('div');
                tooltipEl.id = 'chartjs-tooltip';
                tooltipEl.innerHTML = '<table></table>';
                document.body.appendChild(tooltipEl);
            }

            // Hide if no tooltip
            if (tooltipModel.opacity == 0) {
                tooltipEl.style.opacity = '0';
                return;
            }

            // Set caret Position
            tooltipEl.classList.remove('above', 'below', 'no-transform');
            if (tooltipModel.yAlign) {
                tooltipEl.classList.add(tooltipModel.yAlign);
            } else {
                tooltipEl.classList.add('no-transform');
            }

            function getBody(bodyItem) {
                return bodyItem.lines;
            }

            // Set Text
            if (tooltipModel.body) {
                let titleLines = tooltipModel.title || [];
                let bodyLines = tooltipModel.body.map(getBody);

                let thStyle = 'text-transform: uppercase';
                let innerHtml = '<thead style="' + thStyle + '">';

                titleLines.forEach(function (title) {
                    innerHtml += '<tr><th style="padding-bottom: 10px" colspan="2">' + title + '</th></tr>';
                });
                innerHtml += '</thead>';

                // tbody
                let bdStyle = 'color: var(--dark-03)';
                innerHtml += '<tbody style="' + bdStyle + '">';

                bodyLines.forEach(function (body, i) {
                    let colors = tooltipModel.labelColors[i];

                    let style = 'background:' + colors.backgroundColor;
                    style += '; border-color:' + colors.borderColor;
                    style += '; border-width: 3px';
                    style += '; border-style: solid';
                    style += '; height: 6px';
                    style += '; width: 6px';
                    style += '; display: inline-block';
                    style += '; border-radius: 50%';
                    style += '; margin: 6px';
                    style += '; float: left';

                    let span = '<span style="' + style + '"></span>';
                    innerHtml += '<tr><td style=" width: 90px;">' + span + body[0].label + '</td>';
                    innerHtml += '<td style="text-align: right; font-weight: 600; width: 60px;">' + body[0].value + '</td></tr>';
                });
                innerHtml += '</tbody>';

                let tableRoot = tooltipEl.querySelector('table');
                tableRoot.innerHTML = innerHtml;
            }

            // `this` will be the overall tooltip
            let position = this._chart.canvas.getBoundingClientRect();

            // Display, position, and set styles for font
            tooltipEl.style.opacity = '1';
            tooltipEl.style.position = 'absolute';
            tooltipEl.style.left = position.left + tooltipModel.caretX + 'px';
            tooltipEl.style.top = position.top + tooltipModel.caretY + 'px';
            tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
            tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
            tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
            tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
            tooltipEl.style.borderRadius = '8px';
        }
    },
    elements: {
        point: {
            radius: 0,
            pointStyle: 'circle',
            borderWidth: 3,
            borderColor: '#00b748',
            backgroundColor: '#ffffff',

            hitRadius: 6,
            hoverRadius: 6,
            hoverBorderWidth: 3,
        },
        line: {
            tension: 0.4,
            borderWidth: 3,
            fill: false
        }
    },
    scales: {
        xAxes: [{
            type: 'category',
            offset: true,
            ticks: {
                fontColor: '#a9aaad', // dark-03
                lineHeight: 1.5,
                fontSize: 12,
                padding: 30,
                callback: function (value, index, values) {
                    return value.substring(0, 3).toUpperCase();
                }
            },
            gridLines: {
                display: false,
                lineWidth: 0,
                drawBorder: false,
                drawOnChartArea: false,
                drawTicks: false,
            }
        }],
        yAxes: [{
            type: 'linear',
            position: 'right',
            gridLines: {
                display: true,
                color: '#e8eaef', //dark-04,
                lineWidth: 1,
                drawBorder: false,
                drawOnChartArea: true,
                drawTicks: true,

                zeroLineWidth: 1,
                zeroLineColor: '#e8eaef',
            },
            ticks: {
                autoSkip: true,
                beginAtZero: true,
                maxTicksLimit: 6,
                fontColor: '#a9aaad', // dark-03
                lineHeight: 1.5,
                fontSize: 12,
                callback: function (value, index, values) {
                    return formatCurrency(value);
                }
            }
        }]
    }
};


function formatCurrency(value) {
    if (value < 0) {
        value *= -1;
        value = '(£' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + ')';
    } else {
        value = '£' + value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');  // 12,345.67;
    }
    return value;
}
