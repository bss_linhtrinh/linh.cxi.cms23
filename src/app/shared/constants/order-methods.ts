import {OrderType} from '../types/order-type';

export const ORDER_METHODS = [
    {id: OrderType.DineIn, name: 'Dine in'},
    {id: OrderType.PickOnOutlet, name: 'Take away/Pickup'},
    {id: OrderType.Delivery, name: 'Delivery'},
];

