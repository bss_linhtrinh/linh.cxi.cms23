export const PRODUCT_TYPES = [
    {id: 'simple', name: 'Simple'},
    {id: 'configurable', name: 'Configurable'},
    {id: 'combo', name: 'Combo'}
];
