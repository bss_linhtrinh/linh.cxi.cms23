export const PLANS = [
    {
        id: 0,
        name: 'Free',
        price: 0.00,
        description: 'Tax Return + Software (restriction)',
        class: 'btn-primary'
    },
    {
        id: 1,
        name: 'Club',
        price: 7.50,
        description: 'Tax Return + Software',
        class: 'btn-primary'
    },
    {
        id: 2,
        name: 'Professional',
        price: 15.00,
        description: 'Accountant + Software for Sole Trader',
        class: 'btn-info'
    },
    {
        id: 3,
        name: 'Business',
        price: 50.00,
        description: 'Accountant + Software for Limited Company',
        class: 'btn-danger'
    }
];