export const GENDERS = [
    {
        name: 'Male', value: 'male', translators: [{name: 'Male', locale: 'en'},
            {name: 'Nam', locale: 'vi'}]
    },
    {
        name: 'Female', value: 'female', translators: [{name: 'Female', locale: 'en'},
            {name: 'Nữ', locale: 'vi'}]
    },
];
