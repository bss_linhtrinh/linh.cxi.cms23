import {OrderStatus} from '../types/order-status';

export const ORDER_STATUSES = [
    {id: OrderStatus.Pending, name: 'Pending'},
    {id: OrderStatus.Confirmed, name: 'Confirmed'},
    {id: OrderStatus.Ordered, name: 'Ordered'},
    {id: OrderStatus.FoodReady, name: 'Food Ready'},
    {id: OrderStatus.OnTheWay, name: 'On The Way'},
    // {id: OrderStatus.Delivering, name: 'Delivering'},
    // {id: OrderStatus.Arrived, name: 'Arrived'},
    {id: OrderStatus.Delivered, name: 'Delivered'},
    {id: OrderStatus.Canceled, name: 'Canceled'},
    {id: OrderStatus.Completed, name: 'Completed'},
];


export const ORDER_GROUP_STATUSES = [
    { name: 'Pending', statuses: [OrderStatus.Pending] },
    { name: 'Confirmed', statuses: [OrderStatus.Confirmed] },
    { name: 'Delivering', statuses: [OrderStatus.FoodReady, OrderStatus.OnTheWay] },
    { name: 'Delivered', statuses: [OrderStatus.Ordered, OrderStatus.Delivered, OrderStatus.Completed] },
    { name: 'Canceled', statuses: [OrderStatus.Canceled] },
];
