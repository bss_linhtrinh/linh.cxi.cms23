import {Levels} from '../types/levels';
import {Provider} from '../types/providers';
import {Types} from '../types/types';

export const MENU_ITEMS = [
    {
        name: 'Brands',
        link: '/brands',
        icon: 'icon-brand',
        allow_providers: [
            Provider.SuperAdmin,
            Provider.Admin
        ],
        allow_levels: [
            Levels.Organization
        ],
        allow_types: [
            Types.FB,
            Types.Booking
        ]
    },
    {
        name: 'Users',
        link: null,
        icon: 'icon-customer',
        allow_providers: [
            Provider.SuperAdmin,
            Provider.Admin
        ],
        allow_levels: [
            Levels.BSS,
            Levels.Organization,
            Levels.Brand,
            Levels.Outlet
        ],
        allow_types: [
            Types.FB,
            Types.Booking
        ],
        children: [
            {
                name: 'Users List',
                link: '/users',
                icon: 'icon-dashboard',
                allow_providers: [
                    Provider.SuperAdmin,
                    Provider.Admin
                ],
                allow_levels: [
                    Levels.BSS,
                    Levels.Organization,
                    Levels.Brand,
                    Levels.Outlet,
                ],
                allow_types: [
                    Types.FB,
                    Types.Booking
                ],
            },
            {
                name: 'Roles',
                link: '/roles',
                icon: 'icon-dashboard',
                allow_providers: [
                    Provider.SuperAdmin,
                    Provider.Admin
                ],
                allow_levels: [
                    Levels.Organization,
                    Levels.Brand
                ],
                allow_types: [
                    Types.FB,
                    Types.Booking
                ],
            },
            /* Hide by Linh, temporary 26/12
            {
                name: 'Permissions',
                link: '/permissions',
                icon: 'icon-dashboard',
                allow_providers: [
                    Provider.SuperAdmin,
                    Provider.Admin
                ],
                allow_levels: [
                    Levels.Organization,
                    Levels.Brand,
                    Levels.Outlet,
                ],
            },
            */
        ]
    },
    {
        name: 'Products',
        icon: 'icon-product',
        link: null,
        allow_providers: [
            Provider.SuperAdmin,
            Provider.Admin
        ],
        allow_levels: [
            Levels.Organization,
            Levels.Brand,
            Levels.Outlet,
        ],
        allow_types: [
            Types.FB,
        ],
        children: [
            {
                name: 'Categories',
                link: '/categories',
                icon: 'icon-dashboard',
                allow_providers: [
                    Provider.SuperAdmin,
                    Provider.Admin
                ],
                allow_levels: [
                    Levels.Organization,
                    Levels.Brand,
                    Levels.Outlet,
                ],
                allow_types: [
                    Types.FB,
                    Types.Booking
                ]
            },
            {
                name: 'Products',
                link: '/products',
                icon: 'icon-dashboard',
                allow_providers: [
                    Provider.SuperAdmin,
                    Provider.Admin
                ],
                allow_levels: [
                    Levels.Organization,
                    Levels.Brand,
                    Levels.Outlet,
                ],
                allow_types: [
                    Types.FB,
                    Types.Booking
                ]
            }
        ]
    },
    {
        name: 'Orders',
        icon: 'icon-orders',
        link: '/orders',
        allow_providers: [
            Provider.SuperAdmin,
            Provider.Admin
        ],
        allow_levels: [
            Levels.Organization,
            Levels.Brand,
            Levels.Outlet,
        ],
        allow_types: [
            Types.FB,
            Types.Booking
        ],
        children: [
            {
                name: 'Board',
                icon: 'icon-orders',
                link: '/orders/board',
                allow_providers: [
                    Provider.SuperAdmin,
                    Provider.Admin
                ],
                allow_levels: [
                    Levels.Organization,
                    Levels.Brand,
                    Levels.Outlet,
                ],
                allow_types: [
                    Types.FB,
                ]
            },
            {
                name: 'List',
                icon: 'icon-orders',
                link: '/orders/list',
                allow_providers: [
                    Provider.SuperAdmin,
                    Provider.Admin
                ],
                allow_levels: [
                    Levels.Organization,
                    Levels.Brand,
                    Levels.Outlet,
                ],
                allow_types: [
                    Types.FB,
                    Types.Booking
                ]
            },
        ]
    },
    {
        name: 'Customers',
        link: 'null',
        icon: 'icon-customer',
        allow_providers: [
            Provider.SuperAdmin,
            Provider.Admin
        ],
        allow_levels: [
            Levels.Organization,
            Levels.Brand,
            Levels.Outlet,
        ],
        allow_types: [
            Types.FB,
            Types.Booking
        ],
        children: [
            {
                name: 'Customers',
                link: '/customer',
                icon: 'icon-dashboard',
                allow_providers: [
                    Provider.SuperAdmin,
                    Provider.Admin
                ],
                allow_levels: [
                    Levels.Organization,
                    Levels.Brand,
                    Levels.Outlet,
                ],
                allow_types: [
                    Types.FB,
                    Types.Booking
                ]
            },
            {
                name: 'Customer-type',
                link: '/customer-type',
                icon: 'icon-dashboard',
                allow_providers: [
                    Provider.SuperAdmin,
                    Provider.Admin
                ],
                allow_levels: [
                    Levels.Organization,
                    Levels.Brand,
                    Levels.Outlet,
                ],
                allow_types: [
                    Types.FB,
                    Types.Booking
                ]
            },
            {
                name: 'Hobby',
                link: '/hobby',
                icon: 'icon-dashboard',
                allow_providers: [
                    Provider.SuperAdmin,
                    Provider.Admin
                ],
                allow_levels: [
                    Levels.Organization,
                    Levels.Brand,
                    Levels.Outlet,
                ],
                allow_types: [
                    Types.FB,
                    Types.Booking
                ]
            },
        ]
    },
    {
        name: 'Case management',
        link: '/case-management',
        icon: 'icon-customer',
        allow_providers: [
            Provider.SuperAdmin,
            Provider.Admin
        ],
        allow_levels: [
            Levels.Organization,
            Levels.Brand,
            Levels.Outlet,
        ],
        allow_types: [
            Types.FB,
            Types.Booking
        ],
    }
];

