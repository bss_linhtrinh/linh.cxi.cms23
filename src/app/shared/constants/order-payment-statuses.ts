export const ORDER_PAYMENT_STATUSES = [
    {id: 'pending', name: 'Pending'},
    {id: 'paid', name: 'Paid'},
    {id: 'void', name: 'Void'},
];
