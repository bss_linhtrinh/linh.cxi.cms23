export const MOCK_DATA = {
    ORDERS: {
        day: {
            LABELS: ['6:00 - 7:00', '7:00 - 8:00', '8:00 - 9:00', '9:00 - 10:00', '10:00 - 11:00', '11:00 - 12:00'],
            DATA: [
                {data: [1, 3, 7, 10, 3, 2], label: 'Dining'},
                {data: [0, 26, 2, 20, 1, 0], label: 'Pick-up'},
                {data: [0, 2, 11, 15, 17, 29], label: 'Delivery'}
            ]
        },
        week: {
            LABELS: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            DATA: [
                {data: [59, 80, 81, 56, 55, 65, 59], label: 'Dining'},
                {data: [28, 48, 40, 80, 81, 19, 86], label: 'Pick-up'},
                {data: [28, 19, 86, 27, 90, 48, 40], label: 'Delivery'}
            ]
        },
        month: {
            LABELS: ['26 Aug - 1 Sep', '2 Sep - 8 Sep', '9 Sep - 15 Sep', '16 Sep - 22 Sep',],
            DATA: [
                {data: [65, 59, 80, 81], label: 'Dining'},
                {data: [28, 48, 40, 19], label: 'Pick-up'},
                {data: [28, 48, 40, 19], label: 'Delivery'}
            ]
        },
        year: {
            LABELS: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            DATA: [
                {data: [80, 81, 56, 55, 99, 87, 77, 89, 102, 123, 99, 59], label: 'Dining'},
                {data: [28, 48, 40, 19, 123, 99, 59, 99, 87, 77, 89, 102], label: 'Pick-up'},
                {data: [28, 48, 40, 19, 86, 27, 90, 40, 123, 99, 59, 99, 87], label: 'Delivery'}
            ]
        },
    },
    DELIVERY_ORDERS: {
        day: {
            DATA: [32, 28, 27, 31, 36, 35, 36]
        },
        week: {
            DATA: [99, 128, 127, 131, 333, 135, 222]
        },
        month: {
            DATA: [189, 34, 227, 231, 236, 235, 444]
        },
        year: {
            DATA: [250, 278, 327, 331, 229, 335, 466]
        },
    },
    ON_TIME_ORDERS: {
        day: {
            DATA: [26, 28, 33, 31, 29, 44, 47],
        },
        week: {
            DATA: [88, 128, 222, 131, 88, 144, 120],
        },
        month: {
            DATA: [199, 228, 233, 345, 229, 244, 555],
        },
        year: {
            DATA: [277, 111, 333, 331, 266, 444, 378],
        },
    },
    CANCELED_ORDERS: {
        day: {
            DATA: [18, 28, 27, 12, 7, 8, 11],
        },
        week: {
            DATA: [200, 128, 127, 31, 36, 44, 52],
        },
        month: {
            DATA: [240, 228, 227, 177, 166, 144, 152],
        },
        year: {
            DATA: [400, 328, 327, 131, 236, 244, 152],
        },
    }
};

