export const LANGUAGES = [
    {id: 'en', code: 'en', name: 'English'},
    {id: 'vi', code: 'vi', name: 'Tiếng Việt'},
];

export const DEFAULT_LANGUAGE = 'en';
