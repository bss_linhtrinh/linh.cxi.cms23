export const DUE_ON_OPTIONS = [
    'Due upon receipt',
    'Due within 7 days',
    'Due within 14 days',
    'Due within 21 days',
    'Due within 30 days',
    'Paid'
];