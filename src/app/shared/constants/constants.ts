

export const CXI_SIZE = 14;

// country code
export const DEFAULT_COUNTRY_CODE_UK = 77;


export const SHIPPING_ADDRESS_MINIMUM_RADIUS = 0;
export const SHIPPING_ADDRESS_MAXIMUM_RADIUS = 100;
