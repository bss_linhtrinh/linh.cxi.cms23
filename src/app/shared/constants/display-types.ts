export const DISPLAY_TYPES = [
    {name: 'Single', value: 'single'},
    {name: 'Multiple', value: 'multiple'},
    {name: 'True / False', value: 'true-false'},
];
