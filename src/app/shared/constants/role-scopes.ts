import {Scope} from '../types/scopes';
export const ROLE_SCOPES = [
    {name: 'Organizations', value: Scope.Organization},
    {name: 'Brands', value: Scope.Brand},
    {name: 'Outlets', value: Scope.Outlet},
];
