import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListBacklistComponent } from './list-backlist/list-backlist.component';


const routes: Routes = [
  { path: 'list', component: ListBacklistComponent, data: { breadcrumb: 'List backlist customer' } },
  { path: '', redirectTo: 'list', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerBacklistRoutingModule { }
