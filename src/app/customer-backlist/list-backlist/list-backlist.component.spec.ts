import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListBacklistComponent } from './list-backlist.component';

describe('ListBacklistComponent', () => {
  let component: ListBacklistComponent;
  let fixture: ComponentFixture<ListBacklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListBacklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListBacklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
