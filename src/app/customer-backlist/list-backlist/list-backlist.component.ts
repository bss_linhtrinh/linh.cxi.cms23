import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/shared/entities/customer';
import { LocalStorage } from 'ngx-webstorage';
import { CustomerRepository } from 'src/app/shared/repositories/customers.repository';
import { Router, ActivatedRoute } from '@angular/router';
import { CxiGridConfig } from '../../shared/components/cxi-grid/cxi-grid-config';
import { CxiGridColumn } from '../../shared/components/cxi-grid/cxi-grid-column';
import { ColumnFilterType } from '../../shared/types/column-filter-type';
import { ColumnFormat } from '../../shared/types/column-format';
import { Backlist } from 'src/app/shared/entities/backlist-customer';
declare const helper: any;
@Component({
  selector: 'app-list-backlist',
  templateUrl: './list-backlist.component.html',
  styleUrls: ['./list-backlist.component.scss']
})
export class ListBacklistComponent implements OnInit {
  cxiGridConfig = CxiGridConfig.from({
    paging: true,
    sorting: true,
    filtering: true,
    className: false,
    selecting: true,
    pagingServer: true,
    backlist: true,
    repository: this.customerRepository,
    actions: {
      onEdit: (customers: Customer) => {
        this.onEdit(customers);
      },
      onDelete: (customers: Customer) => {
        this.onDelete(customers);
      },
      onActivate: (customers: Customer) => {
        this.onActivate(customers);
      },
      onDeactivate: (customers: Customer) => {
        this.onDeactivate(customers);
      },
    }
  });
  cxiGridColumns = CxiGridColumn.from([
    { title: 'name', name: 'name', avatar: 'avatar' },
    { title: 'dob', name: 'dob', filterType: ColumnFilterType.Datepicker, sort: true, format: 'dateTime' },
    {
      title: 'gender', name: 'gender', filterType: ColumnFilterType.Select,
      filterData: [
        { name: 'Male', id: 'male' },
        { name: 'Female', id: 'female' },
      ]
    },
    { title: 'phone', name: 'phone', className: 'phone-number', format: ColumnFormat.PhoneNumber },
    { title: 'email', name: 'email' },
  ]);
  public backlist = new Backlist();
  @LocalStorage('scope.organization_id') organizationId: number;
  @LocalStorage('scope.brand_id') brandId: number;
  @LocalStorage('scope.outlet_id') outletId: number;
  constructor(
    private customerRepository: CustomerRepository,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
  }
  onEdit(customers: Customer) {
    this.router.navigate(['../../customer/', customers.id, 'edit'], { relativeTo: this.activatedRoute });
  }
  onDelete(customers: Customer) {
    this.customerRepository.destroy(customers).subscribe();
  }
  onActivate(customers: Customer) {
    this.customerRepository.activate(customers).subscribe();
  }
  onDeactivate(customer: Customer) {
    this.customerRepository.deactivate(customer).subscribe();
  }
  checkCustomer(item) {
    let index = this.backlist.list.indexOf(item.id);
    if (index == -1) {
      this.backlist.list.push(item.id);
    } else {
      this.backlist.list.splice(index, 1);
    }
  }
  switch() {
    this.backlist.action = 'remove';
    this.customerRepository.backlistCustomer(this.backlist).subscribe();
    this.router.navigateByUrl('/customer/list');
    setTimeout(() => {
      helper.showNotification(`Updated successfully !!!`, 'done', 'success', 1600);
    }, 600);
  }
  checkCustomerAll(item?: any) {
    console.log(item)
    if (item.check_all_item == true) {
      item.data_item.forEach(_o => {
        this.backlist.list.push(_o.id);
      });
    } else {
      this.backlist.list = [];
    }
  }
}
