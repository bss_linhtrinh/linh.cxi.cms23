import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerBacklistRoutingModule } from './customer-backlist-routing.module';
import { ListBacklistComponent } from './list-backlist/list-backlist.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ListBacklistComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    CustomerBacklistRoutingModule
  ]
})
export class CustomerBacklistModule { }
