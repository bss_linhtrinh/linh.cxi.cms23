import {Injectable} from '@angular/core';
import {Resolve, ActivatedRouteSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthService} from './shared/services/auth/auth.service';


@Injectable()
export class AuthResolver implements Resolve<any> {

    constructor(
        private router: Router,
        private auth: AuthService,
    ) {
    }

    resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
        return this.auth.authorized()
            .pipe(
                catchError(
                    (error) => {
                        this.router.navigate(
                            ['auth/login']
                            // {queryParams: {redirect_uri: route._routerState.url}}
                        );
                        throw error;
                    }
                )
            );
    }
}
