import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EventRepository} from '../../../shared/repositories/event.repository';
import {Event} from '../../../shared/entities/event';
import {Observable, Observer} from 'rxjs';
import {Topic} from '../../../shared/entities/topic';
import {Ticket} from '../../../shared/entities/ticket';
import * as _ from 'lodash';
import {Reporter} from '../../../shared/entities/reporter';
import {Speaker} from '../../../shared/entities/speaker';
import {Sponsor} from '../../../shared/entities/sponsor';
import {Highlight} from '../../../shared/entities/highlight';
import {BankInfo} from '../../../shared/entities/bank-info';
import {Timeline} from '../../../shared/entities/timeline';
import {BannerList} from '../../../shared/entities/bannerList';
import {TicketChildren} from '../../../shared/entities/ticket-children';
import {ObjectCustom} from '../../../shared/entities/object';
import {Exhibition} from '../../../shared/entities/exhibition';
import {Company} from '../../../shared/entities/company';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import {Letter} from '../../../shared/entities/letter';
import {SeatMap} from '../../../shared/entities/seat-map';
import {MessagesService} from '../../../messages/messages.service';
import {switchMap} from 'rxjs/operators';

declare const helper: any;

@Component({
    selector: 'app-edit-event',
    templateUrl: './edit-event.component.html',
    styleUrls: ['./edit-event.component.scss']
})
export class EditEventComponent implements OnInit {

    event: Event = new Event();
    level = [];
    sponsorTicket = [];
    selectedFileLetter: File = null;
    currency = [{name: 'VND', value: 'VND'},
        {name: 'USD', value: 'USD'}];
    method = [{name: 'Bank transfer', value: 'bank'},
        {name: 'Cash', value: 'cash'}];
    minDateTicket: any;
    bookingType = [{name: 'Offline', value: 'offline'},
        {name: 'Online', value: 'online'}];
    allowBooking = [{name: 'Có', value: true},
        {name: 'Không', value: false}];
    isFeature = [{name: 'Có', value: true},
        {name: 'Không', value: false}];
    paymentType = [{name: 'Free', value: 'free'},
        {name: 'Charge', value: 'costly'}];
    priority = [{name: 1, value: 1},
        {name: 2, value: 2},
        {name: 3, value: 3},
        {name: 4, value: 4},
        {name: 5, value: 5},
        {name: 6, value: 6},
        {name: 7, value: 7},
        {name: 8, value: 8},
        {name: 9, value: 9},
        {name: 10, value: 10}];
    listComponent = [];
    component: string;
    backupEvent: Event = new Event();
    isLoading = false;
    componentAdding = {
        topics: false,
        reporters: false,
        speakers: false,
        tickets: false,
        sponsors: false,
        highlight_info: false,
        bank_info: false,
        time_line: false,
        letters: false,
        exhibitions: false,
        companies: false,
        seatmaps: false
    };
    imagesLetter = [];
    oldImagesLetter = [];
    oldImageLetterLength: any;
    imagesSeatMap = [];
    oldImagesSeatMap = [];
    oldImageSeatMapLength: any;

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private eventRepository: EventRepository,
                public utilitiesService: UtilitiesService,
                private messagesService: MessagesService
    ) {
    }

    ngOnInit() {
        this.loadEvent();
    }


    loadEvent() {
        this.activatedRoute.params.subscribe((data) => {
            const id = data.id;
            this.eventRepository.findEvent(id).subscribe((res) => {
                this.event = res;
                this.getListComponent(id);
                this.backupEvent = _.cloneDeep(this.event);
            });
        });
    }


    getListComponent(id) {
        const params = {id: id};
        this.getTicketSponsor(id);
        this.eventRepository.getListComponentByEventId(params).subscribe(res => {
            this.listComponent = res.filter(item => item.is_used !== 1);
            this.setSponsorTicket();
            this.displayComponent(res);
            this.isLoading = true;
        });
    }

    setSponsorTicket() {
        const sponsor = {name: 'Sponsor', key: 'sponsors', disabled: true};
        if (this.sponsorTicket.length === 0) {
            this.listComponent.map((todo, i) => {
                if (todo.key === sponsor.key) {
                    this.listComponent[i] = sponsor;
                }
            });
        }
        if (this.sponsorTicket.length > 0) {
            this.sponsorTicket.forEach(item => {
                this.level.push({name: item.name, value: item.id});
            });
        }
    }

    getTicketSponsor(eventId) {
        this.eventRepository.getTicketByEvent(eventId, {type: 'sponsor'})
            .subscribe(
                (tickets) => {
                    this.sponsorTicket = tickets;
                });
    }

    autoGenerate() {
        const randomString = this.utilitiesService.randomCodeEventBooking();
        this.event.code = randomString;
    }

    displayComponent(res) {
        const parse = res.filter(item => item.is_used === 1);
        parse.forEach((item) => {
            if (item.key === 'topics') {
                this.componentAdding.topics = true;
            }
            if (item.key === 'reporters') {
                this.componentAdding.reporters = true;
            }
            if (item.key === 'speakers') {
                this.componentAdding.speakers = true;
            }
            if (item.key === 'tickets') {
                this.componentAdding.tickets = true;
            }
            if (item.key === 'sponsors') {
                this.componentAdding.sponsors = true;
            }
            if (item.key === 'highlight_info') {
                this.componentAdding.highlight_info = true;
            }
            if (item.key === 'bank_info') {
                this.componentAdding.bank_info = true;
            }
            if (item.key === 'time_line') {
                this.componentAdding.time_line = true;
            }
            if (item.key === 'letters') {
                this.componentAdding.letters = true;
            }
            if (item.key === 'exhibitions') {
                this.componentAdding.exhibitions = true;
            }
            if (item.key === 'companies') {
                this.componentAdding.companies = true;
            }
            if (item.key === 'seatmaps') {
                this.componentAdding.seatmaps = true;
            }
        });
    }

    onSubmit() {
        const event = this.parseData(this.event);
        this.eventRepository.save(event).subscribe(res => {
                this.router.navigateByUrl('/events');
                helper.showNotification(`${this.event.title} updated successfully!!`, 'done', 'success');
            },
            err => {
                this.event = _.cloneDeep(this.backupEvent);
            });
    }

    onFileChanged(selectedFile: File) {
        // this.event.thumbnailUrl = selectedFile;
    }

    onFileContent(selectedFile: File) {
        // this.event.contentImageUrl = selectedFile;
    }

    cancel() {
        const message = `Are you sure you want to cancel ?`;
        this.messagesService.statusConfirmation(message)
            .pipe(
                switchMap(() => this.router.navigate(['..', 'list'], {relativeTo: this.activatedRoute}))
            )
            .subscribe();
    }

    addComponent() {
        const data = this.componentAdding;
        Object.keys(data).forEach((key: any) => {
            if (key === this.component) {
                data[key] = true;
                this.prepareComponent(key);
            }
        });
        this.listComponent = this.listComponent.filter(item => item.key !== this.component);
        this.component = this.listComponent[0].key;
    }

    prepareComponent(key) {
        switch (key) {
            case 'tickets':
                this.event.tickets.push(new Ticket());
                break;
            case 'reporters':
                this.event.reporters.push(new Reporter());
                break;
            case 'speakers':
                this.event.speakers.push(new Speaker());
                break;
            case 'sponsors':
                this.event.sponsors.push(new Sponsor());
                break;
            case 'topics':
                this.event.topics.push(new Topic());
                break;
            case 'highlight_info':
                this.event.highlight_info.push(new Highlight());
                break;
            case 'bank_info':
                this.event.bank_info = new BankInfo();
                break;
            case 'time_line':
                this.event.time_line.push(new Timeline());
                break;
            case 'exhibitions':
                this.event.exhibitions.push(new Exhibition());
                break;
            case 'companies':
                this.event.companies.push(new Company());
                break;
            case 'letters':
                this.event.letters.push(new Letter());
                break;
            case 'seatmaps':
                this.event.seatmaps.push(new SeatMap());
                break;
        }
        this.backupEvent = _.cloneDeep(this.event);
    }

    parseData(event) {
        if (!this.componentAdding.topics) {
            event.topics = [];
            delete event.topics;
        }
        if (!this.componentAdding.reporters) {
            event.reporters = [];
            delete event.reporters;
        }
        if (!this.componentAdding.speakers) {
            event.speakers = [];
            delete event.speakers;
        }
        if (!this.componentAdding.tickets) {
            event.tickets = [];
            delete event.tickets;
        }
        if (!this.componentAdding.sponsors) {
            event.sponsors = [];
            delete event.sponsors;
        }
        if (!this.componentAdding.highlight_info) {
            event.highlight_info = [];
            delete event.highlight_info;
        }
        if (!this.componentAdding.time_line) {
            event.time_line = [];
            delete event.time_line;
        }
        if (!this.componentAdding.letters) {
            event.letters = [];
            delete event.letters;
        }
        if (!this.componentAdding.exhibitions) {
            event.exhibitions = [];
            delete event.exhibitions;
        }
        if (!this.componentAdding.companies) {
            event.companies = [];
            delete event.companies;
        }
        if (!this.componentAdding.seatmaps) {
            event.seatmaps = [];
            delete event.seatmaps;
        }
        return event;
    }

    deleteComponent(component) {
        switch (component) {
            case 'ticket':
                this.componentAdding.tickets = false;
                this.listComponent.push({name: 'Tickets', key: 'tickets'});
                break;
            case 'reporter':
                this.componentAdding.reporters = false;
                this.listComponent.push({name: 'Representative', key: 'reporters'});
                break;
            case 'speaker':
                this.componentAdding.speakers = false;
                this.listComponent.push({name: 'Speakers', key: 'speakers'});
                break;
            case 'sponsor':
                this.componentAdding.sponsors = false;
                this.listComponent.push({name: 'Sponsors', key: 'sponsors'});
                break;
            case 'topic':
                this.componentAdding.topics = false;
                this.listComponent.push({name: 'Topics', key: 'topics'});
                break;
            case 'highlight':
                this.componentAdding.highlight_info = false;
                this.listComponent.push({name: 'Highlight Info', key: 'highlight_info'});
                break;
            case 'billing':
                this.componentAdding.bank_info = false;
                this.listComponent.push({name: 'Payment Info', key: 'bank_info'});
                break;
            case 'timeline':
                this.componentAdding.time_line = false;
                this.listComponent.push({name: 'Agenda', key: 'time_line'});
                break;
            case 'letter':
                this.componentAdding.letters = false;
                this.listComponent.push({name: 'Letter', key: 'letters'});
                break;
            case 'exhibition':
                this.componentAdding.exhibitions = false;
                this.listComponent.push({name: 'Exhibitions', key: 'exhibitions'});
                break;
            case 'company':
                this.componentAdding.companies = false;
                this.listComponent.push({name: 'Companies', key: 'companies'});
                break;
            case 'seatmap':
                this.componentAdding.seatmaps = false;
                this.listComponent.push({name: 'Seat Map', key: 'seatmaps'});
                break;
        }
    }

    onFileContentMap(selectedFile: any) {
        this.oldImagesSeatMap = selectedFile.oldVal;
    }


    onChangePaymentType(event) {
        if (event === 'costly') {
            this.event.is_auto_approved = false;
        }
    }

    onFileLetter(selectedFile: File) {
        this.selectedFileLetter = selectedFile;
    }

    handleChangeMethod(method) {
        if (method === 'bank') {
            this.event.tickets.forEach(ticket => {
                ticket.is_auto_approved = true;
                if (ticket.children && ticket.children.length > 0) {
                    ticket.children.forEach(child => {
                        child.is_auto_approved = true;
                    });
                }
            });
        }
    }
}
