import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectSpeakersComponent } from './select-speakers.component';

describe('SelectSpeakersComponent', () => {
  let component: SelectSpeakersComponent;
  let fixture: ComponentFixture<SelectSpeakersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSpeakersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSpeakersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
