import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../../shared/custom-form/element-base/element-base';
import {Speaker} from '../../../../shared/entities/speaker';

@Component({
    selector: 'app-select-speaker',
    templateUrl: './select-speaker.component.html',
    styleUrls: ['./select-speaker.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectSpeakerComponent, multi: true}
    ]
})
export class SelectSpeakerComponent extends ElementBase<Speaker> implements OnInit {

    positions = [{value: 'Bác sĩ', name: 'Bác sĩ'},
        {value: 'Đóc tơ', name: 'Đóc tơ'},
        {value: 'Y tá', name: 'Y tá'}];
    selectedFile: File = null;
    selectedFileCV: File = null;

    private _name: string;
    private _label: string;

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        this._label = value.replace('_', ' ');
    }

    @Input() public size = 'sm';
    @Input() public width = '';

    @Output() add = new EventEmitter();
    @Output() remove = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;


    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onChange() {
        //
    }

    removeItem() {
        this.remove.emit();
    }

    onFileContent(selectedFile: File) {
        this.selectedFile = selectedFile;
    }

    onFileCV(selectedFile: File) {
        this.selectedFileCV = selectedFile;
    }

}
