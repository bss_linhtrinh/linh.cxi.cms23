import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectSpeakerComponent } from './select-speaker.component';

describe('SelectSpeakerComponent', () => {
  let component: SelectSpeakerComponent;
  let fixture: ComponentFixture<SelectSpeakerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSpeakerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSpeakerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
