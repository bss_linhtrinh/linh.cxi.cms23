import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectReportersComponent } from './select-reporters.component';

describe('SelectReportersComponent', () => {
  let component: SelectReportersComponent;
  let fixture: ComponentFixture<SelectReportersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectReportersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectReportersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
