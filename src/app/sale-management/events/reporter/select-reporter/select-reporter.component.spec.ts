import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectReporterComponent } from './select-reporter.component';

describe('SelectReporterComponent', () => {
  let component: SelectReporterComponent;
  let fixture: ComponentFixture<SelectReporterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectReporterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectReporterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
