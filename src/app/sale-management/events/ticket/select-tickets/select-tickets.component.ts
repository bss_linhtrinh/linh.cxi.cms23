import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../../shared/custom-form/element-base/element-base';
import {Ticket} from '../../../../shared/entities/ticket';
import {TicketChildren} from '../../../../shared/entities/ticket-children';
import {tick} from '@angular/core/testing';

@Component({
    selector: 'app-select-tickets',
    templateUrl: './select-tickets.component.html',
    styleUrls: ['./select-tickets.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectTicketsComponent, multi: true}
    ]
})
export class SelectTicketsComponent extends ElementBase<Ticket[]> implements OnInit {

    private _name: string;
    @Input() payment_method: string;
    @Input() sponsors: any[];
    private _label: string;
    ticket = 'single_ticket';
    ticketType = [{name: 'Bundle ticket', value: 'bundle_ticket'},
        {name: 'Single ticket', value: 'single_ticket'}];

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        this._label = value.replace('_', ' ');
    }

    @Input() public size = 'sm';
    @Input() public width = '';
    @Input() public minDate;
    @Input() public maxDate;
    @Input() public isChildren;
    @Input() public allowEmpty;

    @Output() addGroup = new EventEmitter();
    @Output() removeGroup = new EventEmitter();
    @Output() change = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }


    // add-on types

    addAddOnType() {
        if (!this.value) {
            this.value = [];
            this.value.map(ticket => TicketChildren.create(ticket));
        }
        this.value.push(new Ticket());

        this.change.emit();
    }

    onRemove(index: number) {
        this.value.splice(index, 1);

        // check empty
        if (this.allowEmpty) {
            this.checkEmpty();
        }
        // check disabled
        // this.checkDisabled();
        this.change.emit();
    }

    onChangeVariantAddOnType() {
        // check disabled
        // this.checkDisabled();
    }

    checkEmpty() {
        if (!this.value || this.value.length === 0) {
            this.addAddOnType();
        }
    }

    // checkDisabled() {
    //     this.sponsors.forEach(addOnType => {
    //         addOnType.disabled = this.value.findIndex(v => v.id === addOnType.id) !== -1;
    //     });
    // }

    isDisabledAddNew() {
        return this.sponsors && this.value && this.sponsors.length === this.value.length;
    }

    onChangeTicketType(ticket) {
        if (ticket === 'single_ticket') {
            this.isChildren = false;
        } else if (ticket === 'bundle_ticket') {
            this.isChildren = true;
        }
    }


}
