import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../../shared/custom-form/element-base/element-base';
import {Ticket} from '../../../../shared/entities/ticket';
import {UtilitiesService} from '../../../../shared/services/utilities/utilities.service';

@Component({
    selector: 'app-select-ticket',
    templateUrl: './select-ticket.component.html',
    styleUrls: ['./select-ticket.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectTicketComponent, multi: true}
    ]
})
export class SelectTicketComponent extends ElementBase<Ticket> implements OnInit {

    paymentType = [{name: 'Free', value: 'free'},
        {name: 'Charge', value: 'costly'}];
    isUnlimited = [{name: 'Có', value: false},
        {name: 'Không', value: true}];
    types = [
        {name: 'Sponsor', value: 'sponsor'},
        {name: 'Advertise', value: 'advertise'},
        {name: 'Booth', value: 'booth'},
        {name: 'Visitor', value: 'visitor'},
        {name: 'Forum', value: 'forum'},
        {name: 'Speaker', value: 'speaker'},
        {name: 'Booth Startup', value: 'boothStartup'},
    ];
    haveChildren = false;

    private _name: string;
    private _label: string;

    @Input() payment_method: string;

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        this._label = value.replace('_', ' ');
    }

    @Input() public size = 'sm';
    @Input() public width = '';
    @Input() public minDate;
    @Input() public maxDate;
    @Input() public isChildren;
    @Input() public index;

    @Output() add = new EventEmitter();
    @Output() remove = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;


    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        public utilitiesService: UtilitiesService
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onChange() {
        //
    }

    removeItem() {
        this.remove.emit();
    }

    handleTicketChange(ticket) {
        if (ticket === 'free') {
            this.value.price = 0;
        }
    }

    handleChangeChildren() {
        if (this.value.children[0]) {
            this.value.haveChild = true;
        } else {
            this.value.haveChild = false;
        }
    }

    // test(time) {
    //     if (time !== null) {
    //         console.log(time);
    //     }
    // }

    autoGenerate() {
        const randomString = this.utilitiesService.randomCodeTicketBooking();
        this.value.code = randomString;
    }
}
