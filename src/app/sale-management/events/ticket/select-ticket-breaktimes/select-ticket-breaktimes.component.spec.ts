import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectTicketBreaktimesComponent } from './select-ticket-breaktimes.component';

describe('SelectTicketBreaktimesComponent', () => {
  let component: SelectTicketBreaktimesComponent;
  let fixture: ComponentFixture<SelectTicketBreaktimesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectTicketBreaktimesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectTicketBreaktimesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
