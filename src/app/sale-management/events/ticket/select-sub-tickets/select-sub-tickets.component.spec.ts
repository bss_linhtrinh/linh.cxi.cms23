import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectSubTicketsComponent } from './select-sub-tickets.component';

describe('SelectSubTicketsComponent', () => {
  let component: SelectSubTicketsComponent;
  let fixture: ComponentFixture<SelectSubTicketsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSubTicketsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSubTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
