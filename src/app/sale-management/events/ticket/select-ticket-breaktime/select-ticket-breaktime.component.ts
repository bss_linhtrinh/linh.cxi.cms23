import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {ElementBase} from '../../../../shared/custom-form/element-base/element-base';
import {TicketBreaktime} from '../../../../shared/entities/ticket-breaktime';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';

@Component({
  selector: 'app-select-ticket-breaktime',
  templateUrl: './select-ticket-breaktime.component.html',
  styleUrls: ['./select-ticket-breaktime.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectTicketBreaktimeComponent, multi: true}
    ]
})
export class SelectTicketBreaktimeComponent extends ElementBase<TicketBreaktime> implements OnInit {

    private _name: string;
    private _label: string;

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        this._label = value.replace('_', ' ');
    }

    @Input() public size = 'sm';
    @Input() public width = '';
    @Input() public minDate;
    @Input() public maxDate;

    @Output() add = new EventEmitter();
    @Output() remove = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;


    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onChange() {
        //
    }

    removeItem() {
        this.remove.emit();
    }

}
