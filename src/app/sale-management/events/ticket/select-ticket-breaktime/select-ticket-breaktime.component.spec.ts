import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectTicketBreaktimeComponent } from './select-ticket-breaktime.component';

describe('SelectTicketBreaktimeComponent', () => {
  let component: SelectTicketBreaktimeComponent;
  let fixture: ComponentFixture<SelectTicketBreaktimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectTicketBreaktimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectTicketBreaktimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
