import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectHighlightInfosComponent } from './select-highlight-infos.component';

describe('SelectHighlightInfosComponent', () => {
  let component: SelectHighlightInfosComponent;
  let fixture: ComponentFixture<SelectHighlightInfosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectHighlightInfosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectHighlightInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
