import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectHighlightInfoComponent } from './select-highlight-info.component';

describe('SelectHighlightInfoComponent', () => {
  let component: SelectHighlightInfoComponent;
  let fixture: ComponentFixture<SelectHighlightInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectHighlightInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectHighlightInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
