import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectLetterComponent } from './select-letter.component';

describe('SelectLetterComponent', () => {
  let component: SelectLetterComponent;
  let fixture: ComponentFixture<SelectLetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectLetterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectLetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
