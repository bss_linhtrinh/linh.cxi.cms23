import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectLettersComponent } from './select-letters.component';

describe('SelectLettersComponent', () => {
  let component: SelectLettersComponent;
  let fixture: ComponentFixture<SelectLettersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectLettersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectLettersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
