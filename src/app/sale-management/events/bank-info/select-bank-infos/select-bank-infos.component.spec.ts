import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectBankInfosComponent } from './select-bank-infos.component';

describe('SelectBankInfosComponent', () => {
  let component: SelectBankInfosComponent;
  let fixture: ComponentFixture<SelectBankInfosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectBankInfosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectBankInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
