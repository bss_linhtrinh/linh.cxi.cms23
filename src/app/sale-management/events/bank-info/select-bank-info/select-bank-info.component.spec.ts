import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectBankInfoComponent } from './select-bank-info.component';

describe('SelectBankInfoComponent', () => {
  let component: SelectBankInfoComponent;
  let fixture: ComponentFixture<SelectBankInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectBankInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectBankInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
