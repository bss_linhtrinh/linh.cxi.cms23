import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectExhibitionNoteComponent } from './select-exhibition-note.component';

describe('SelectExhibitionNoteComponent', () => {
  let component: SelectExhibitionNoteComponent;
  let fixture: ComponentFixture<SelectExhibitionNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectExhibitionNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectExhibitionNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
