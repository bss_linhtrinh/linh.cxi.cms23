import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectExhibitionBenefitComponent } from './select-exhibition-benefit.component';

describe('SelectExhibitionBenefitComponent', () => {
  let component: SelectExhibitionBenefitComponent;
  let fixture: ComponentFixture<SelectExhibitionBenefitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectExhibitionBenefitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectExhibitionBenefitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
