import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectExhibitionBenefitsComponent } from './select-exhibition-benefits.component';

describe('SelectExhibitionBenefitsComponent', () => {
  let component: SelectExhibitionBenefitsComponent;
  let fixture: ComponentFixture<SelectExhibitionBenefitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectExhibitionBenefitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectExhibitionBenefitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
