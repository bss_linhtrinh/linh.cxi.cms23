import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectExhibitionNotesComponent } from './select-exhibition-notes.component';

describe('SelectExhibitionNotesComponent', () => {
  let component: SelectExhibitionNotesComponent;
  let fixture: ComponentFixture<SelectExhibitionNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectExhibitionNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectExhibitionNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
