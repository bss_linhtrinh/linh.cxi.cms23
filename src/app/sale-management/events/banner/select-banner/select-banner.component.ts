import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../../shared/custom-form/element-base/element-base';
import * as _ from 'lodash';
import {BannerList} from '../../../../shared/entities/bannerList';

@Component({
    selector: 'app-select-banner',
    templateUrl: './select-banner.component.html',
    styleUrls: ['./select-banner.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectBannerComponent, multi: true}
    ]
})
export class SelectBannerComponent extends ElementBase<BannerList> implements OnInit {

    selectedFile: File = null;
    private _name: string;
    private _label: string;

    @Input() resolutions;

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        this._label = value.replace('_', ' ');
    }

    @Input() public size = 'sm';
    @Input() public width = '';

    @Output() add = new EventEmitter();
    @Output() remove = new EventEmitter();
    @Output() changeRes = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;


    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onChange(res) {
        const resolutions = _.cloneDeep(this.resolutions);
        const index = _.findIndex(resolutions, {value: res});
        switch (res) {
            case 'default':
                resolutions.splice(index, 1, {name: 'Default', value: 'default', disabled: true});
                break;
            case '375x500':
                resolutions.splice(index, 1, {name: 'Mobile', value: '375x500', disabled: true});
                break;
            case '1280x720':
                resolutions.splice(index, 1, {name: '1280x720', value: '1280x720', disabled: true});
                break;
            case '1440x810':
                resolutions.splice(index, 1, {name: '1440x810', value: '1440x810', disabled: true});
                break;
            case '1920x1080':
                resolutions.splice(index, 1, {name: '1920x1080', value: '1920x1080', disabled: true});
                break;
        }
        this.changeRes.emit(resolutions);
    }

    removeItem() {
        this.remove.emit(this.value.resolution);
    }

    onFileDesktop(file: File) {
        this.selectedFile = file;
    }


}
