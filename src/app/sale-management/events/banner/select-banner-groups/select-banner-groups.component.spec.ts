import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectBannerGroupsComponent } from './select-banner-groups.component';

describe('SelectBannerGroupsComponent', () => {
  let component: SelectBannerGroupsComponent;
  let fixture: ComponentFixture<SelectBannerGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectBannerGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectBannerGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
