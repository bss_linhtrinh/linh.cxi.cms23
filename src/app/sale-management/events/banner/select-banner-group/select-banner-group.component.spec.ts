import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectBannerGroupComponent } from './select-banner-group.component';

describe('SelectBannerGroupComponent', () => {
  let component: SelectBannerGroupComponent;
  let fixture: ComponentFixture<SelectBannerGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectBannerGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectBannerGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
