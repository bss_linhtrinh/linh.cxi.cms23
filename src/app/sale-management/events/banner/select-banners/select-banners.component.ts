import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../../shared/custom-form/element-base/element-base';
import {List} from '../../../../shared/entities/list';
import {BannerList} from '../../../../shared/entities/bannerList';
import * as _ from 'lodash';

@Component({
    selector: 'app-select-banners',
    templateUrl: './select-banners.component.html',
    styleUrls: ['./select-banners.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectBannersComponent, multi: true}
    ]
})
export class SelectBannersComponent extends ElementBase<BannerList[]> implements OnInit {

    private _name: string;
    @Input() highlight: any[];
    private _label: string;

    resolutions = [
        {name: 'Default', value: 'default', disabled: false},
        {name: 'Mobile', value: '375x500', disabled: false},
        {name: '1280x720', value: '1280x720', disabled: false},
        {name: '1440x810', value: '1440x810', disabled: false},
        {name: '1920x1080', value: '1920x1080', disabled: false},
    ];

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        this._label = value.replace('_', ' ');
    }

    @Input() public size = 'sm';
    @Input() public width = '';

    @Output() addGroup = new EventEmitter();
    @Output() removeGroup = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }


    // add-on types

    addAddOnType() {
        this.value.push(new BannerList());
    }

    onRemove(index: number, res) {
        const resolutions = _.cloneDeep(this.resolutions);
        this.value.splice(index, 1);

        // check empty
        this.checkEmpty();

        // check disabled
        // this.checkDisabled();

        const i = _.findIndex(resolutions, {value: res});
        switch (res) {
            case 'default':
                resolutions.splice(i, 1, {name: 'Default', value: 'default', disabled: false});
                break;
            case '375x500':
                resolutions.splice(i, 1, {name: 'Mobile', value: '375x500', disabled: false});
                break;
            case '1280x720':
                resolutions.splice(i, 1, {name: '1280x720', value: '1280x720', disabled: false});
                break;
            case '1440x810':
                resolutions.splice(i, 1, {name: '1440x810', value: '1440x810', disabled: false});
                break;
            case '1920x1080':
                resolutions.splice(i, 1, {name: '1920x1080', value: '1920x1080', disabled: false});
                break;
        }
        this.resolutions = _.cloneDeep(resolutions);
    }

    onChangeVariantAddOnType() {
        // check disabled
        // this.checkDisabled();
    }

    checkEmpty() {
        if (!this.value || this.value.length === 0) {
            this.addAddOnType();
        }
    }

    // checkDisabled() {
    //     this.highlight.forEach(addOnType => {
    //         addOnType.disabled = this.value.findIndex(v => v.id === addOnType.id) !== -1;
    //     });
    // }

    isDisabledAddNew() {
        return this.highlight && this.value && this.highlight.length === this.value.length;
    }

    onChangeRes(resolution) {
        this.resolutions = resolution;
    }

}
