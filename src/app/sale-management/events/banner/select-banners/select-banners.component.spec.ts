import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectBannersComponent } from './select-banners.component';

describe('SelectBannersComponent', () => {
  let component: SelectBannersComponent;
  let fixture: ComponentFixture<SelectBannersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectBannersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectBannersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
