import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectSubEventTimelinesComponent } from './select-sub-event-timelines.component';

describe('SelectSubEventTimelinesComponent', () => {
  let component: SelectSubEventTimelinesComponent;
  let fixture: ComponentFixture<SelectSubEventTimelinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSubEventTimelinesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSubEventTimelinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
