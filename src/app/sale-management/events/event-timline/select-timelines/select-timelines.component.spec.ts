import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectTimelinesComponent } from './select-timelines.component';

describe('SelectTimelinesComponent', () => {
  let component: SelectTimelinesComponent;
  let fixture: ComponentFixture<SelectTimelinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectTimelinesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectTimelinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
