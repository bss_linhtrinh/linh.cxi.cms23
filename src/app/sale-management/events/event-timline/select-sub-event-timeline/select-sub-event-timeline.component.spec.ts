import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectSubEventTimelineComponent } from './select-sub-event-timeline.component';

describe('SelectSubEventTimelineComponent', () => {
  let component: SelectSubEventTimelineComponent;
  let fixture: ComponentFixture<SelectSubEventTimelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSubEventTimelineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSubEventTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
