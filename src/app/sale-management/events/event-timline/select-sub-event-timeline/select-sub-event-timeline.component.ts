import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../../shared/custom-form/element-base/element-base';
import {TimelineChildren} from '../../../../shared/entities/timeline-children';

@Component({
  selector: 'app-select-sub-event-timeline',
  templateUrl: './select-sub-event-timeline.component.html',
  styleUrls: ['./select-sub-event-timeline.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectSubEventTimelineComponent, multi: true}
    ]
})
export class SelectSubEventTimelineComponent extends ElementBase<TimelineChildren> implements OnInit {

    private _name: string;
    private _label: string;

    language = [{value: 'Tiếng Việt', name: 'Vietnamese'}, {value: 'Tiếng Anh', name: 'English'}];

    @Input() haveChild = true;

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        this._label = value.replace('_', ' ');
    }

    @Input() public size = 'sm';
    @Input() public width = '';

    @Output() add = new EventEmitter();
    @Output() remove = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;


    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onChange() {
        //
    }

    removeItem() {
        this.remove.emit();
    }

}
