import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectEventTimelinesComponent } from './select-event-timelines.component';

describe('SelectEventTimelinesComponent', () => {
  let component: SelectEventTimelinesComponent;
  let fixture: ComponentFixture<SelectEventTimelinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectEventTimelinesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectEventTimelinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
