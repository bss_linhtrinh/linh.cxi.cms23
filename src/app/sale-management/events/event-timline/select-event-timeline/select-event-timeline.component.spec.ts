import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectEventTimelineComponent } from './select-event-timeline.component';

describe('SelectEventTimelineComponent', () => {
  let component: SelectEventTimelineComponent;
  let fixture: ComponentFixture<SelectEventTimelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectEventTimelineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectEventTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
