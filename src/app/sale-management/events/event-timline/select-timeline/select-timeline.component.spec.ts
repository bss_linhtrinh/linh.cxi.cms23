import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectTimelineComponent } from './select-timeline.component';

describe('SelectTimelineComponent', () => {
  let component: SelectTimelineComponent;
  let fixture: ComponentFixture<SelectTimelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectTimelineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
