import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Event} from '../../../shared/entities/event';
import {EventRepository} from '../../../shared/repositories/event.repository';
import {ActivatedRoute, Router} from '@angular/router';
import * as _ from 'lodash';
import {NotificationService} from '../../../shared/services/noitification/notification.service';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import {MessagesService} from '../../../messages/messages.service';
import {switchMap} from 'rxjs/operators';

@Component({
    selector: 'app-create-event',
    templateUrl: './create-event.component.html',
    styleUrls: ['./create-event.component.scss']
})
export class CreateEventComponent implements OnInit {

    event: Event = new Event();
    selectedFileThumbnail: File = null;
    selectedFileContent: File = null;
    selectedFileLetter: File = null;
    selectedFileSeat: File = null;
    currency = [{name: 'VND', value: 'VND'},
        {name: 'USD', value: 'USD'}];
    method = [{name: 'Bank transfer', value: 'bank'},
        {name: 'Point', value: 'point'}];
    minDateTicket: any;
    bookingType = [{name: 'Offline', value: 'offline'},
        {name: 'Online', value: 'online'}];
    allowBooking = [{name: 'Có', value: true},
        {name: 'Không', value: false}];
    isFeature = [{name: 'Có', value: true},
        {name: 'Không', value: false}];
    paymentType = [{name: 'Free', value: 'free'},
        {name: 'Charge', value: 'costly'}];
    priority = [{name: 1, value: 1},
        {name: 2, value: 2},
        {name: 3, value: 3},
        {name: 4, value: 4},
        {name: 5, value: 5},
        {name: 6, value: 6},
        {name: 7, value: 7},
        {name: 8, value: 8},
        {name: 9, value: 9},
        {name: 10, value: 10}];
    level = [];

    listComponent = [];
    component: string;
    minDate = new Date();
    backupEvent: Event = new Event();

    componentAdding = {
        topics: false,
        reporters: false,
        speakers: false,
        tickets: false,
        sponsors: false,
        highlight_info: false,
        bank_info: false,
        time_line: false,
        letters: false,
        exhibitions: false,
        companies: false,
        seatmaps: false
    };

    // @ViewChild('eventComponent') eventComponent: ElementRef;

    constructor(private eventRepository: EventRepository,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                private notificationService: NotificationService,
                public utilitiesService: UtilitiesService,
                private messagesService: MessagesService) {
    }

    ngOnInit() {
        this.getListComponent();
    }

    cancel() {
        const message = `Are you sure you want to cancel ?`;
        this.messagesService.statusConfirmation(message)
            .pipe(
                switchMap(() => this.router.navigate(['..', 'list'], {relativeTo: this.activatedRoute}))
            )
            .subscribe();
    }


    onSubmit() {
        const event = this.parseData(this.event);
        const oldStartTime = event.start_time;
        const oldEndTime = event.end_time;
        this.backupEvent = _.cloneDeep(this.event);
        if (this.isEmptyOrSpaces(event.location_vi) && this.isEmptyOrSpaces(event.location_en)) {
            this.notificationService.error('Location is not allow empty string !!');
        } else {
            this.eventRepository.save(event).subscribe(res => {
                    this.router.navigateByUrl('/events');
                },
                (err) => {
                    this.event.start_time = oldStartTime;
                    this.event.end_time = oldEndTime;
                    this.event = _.cloneDeep(this.backupEvent);
                });
        }
    }

    onFileChanged(selectedFile: File) {
        this.selectedFileThumbnail = selectedFile;
    }

    onFileContent(selectedFile: File) {
        this.selectedFileContent = selectedFile;
    }

    onFileLetter(selectedFile: File) {
        this.selectedFileLetter = selectedFile;
    }

    onFileContentMap(selectedFile: any) {
        this.selectedFileSeat = selectedFile;
    }

    getListComponent() {
        const sponsor = {name: 'Sponsor', key: 'sponsors', disabled: true};
        this.eventRepository.getListComponent().subscribe(res => {
            res.map((todo, i) => {
                if (todo.key === sponsor.key) {
                    res[i] = sponsor;
                }
            });
            this.listComponent = res;
        });
    }

    isEmptyOrSpaces(str) {
        return str === null || str.match(/^ *$/) !== null;
    }

    addComponent() {
        const data = this.componentAdding;
        Object.keys(data).forEach((key: any) => {
            if (key === this.component) {
                data[key] = true;
            }
        });
        this.listComponent = this.listComponent.filter(item => item.key !== this.component);
        this.component = '';
    }


    parseData(event) {
        if (!this.componentAdding.topics) {
            event.topics = [];
            delete event.topics;
        }
        if (!this.componentAdding.reporters) {
            event.reporters = [];
            delete event.reporters;
        }
        if (!this.componentAdding.speakers) {
            event.speakers = [];
            delete event.speakers;
        }
        if (!this.componentAdding.tickets) {
            event.tickets = [];
            delete event.tickets;
        }
        //
        // if (this.componentAdding.tickets) {
        //     event.tickets.forEach(ticket => {
        //         if (ticket.limit) {
        //             ticket.available = ticket.limit;
        //         }
        //         if (ticket.children && ticket.children.length > 0) {
        //             ticket.children.forEach(child => {
        //                 if (child.limit) {
        //                     child.available = child.limit;
        //                 }
        //             });
        //         }
        //     });
        // }
        if (!this.componentAdding.sponsors) {
            event.sponsors = [];
            delete event.sponsors;
        }
        if (!this.componentAdding.highlight_info) {
            event.highlight_info = [];
            delete event.highlight_info;
        }
        // if (!this.componentAdding.bank_info) {
        //     event.bank_info = {};
        //     delete event.bank_info;
        // }
        if (!this.componentAdding.time_line) {
            event.time_line = [];
            delete event.time_line;
        }
        if (!this.componentAdding.letters) {
            event.letters = [];
            delete event.letters;
        }
        if (!this.componentAdding.exhibitions) {
            event.exhibitions = [];
            delete event.exhibitions;
        }
        if (!this.componentAdding.companies) {
            event.companies = [];
            delete event.companies;
        }
        if (!this.componentAdding.seatmaps) {
            event.seatmaps = [];
            delete event.seatmaps;
        }

        return event;
    }

    deleteComponent(component) {
        switch (component) {
            case 'ticket':
                this.componentAdding.tickets = false;
                this.listComponent.push({name: 'Tickets', key: 'tickets'});
                break;
            case 'reporter':
                this.componentAdding.reporters = false;
                this.listComponent.push({name: 'Representative', key: 'reporters'});
                break;
            case 'speaker':
                this.componentAdding.speakers = false;
                this.listComponent.push({name: 'Speakers', key: 'speakers'});
                break;
            case 'sponsor':
                this.componentAdding.sponsors = false;
                this.listComponent.push({name: 'Sponsors', key: 'sponsors'});
                break;
            case 'topic':
                this.componentAdding.topics = false;
                this.listComponent.push({name: 'Topics', key: 'topics'});
                break;
            case 'highlight':
                this.componentAdding.highlight_info = false;
                this.listComponent.push({name: 'Highlight Info', key: 'highlight_info'});
                break;
            case 'billing':
                this.componentAdding.bank_info = false;
                this.listComponent.push({name: 'Payment Info', key: 'bank_info'});
                break;
            case 'timeline':
                this.componentAdding.time_line = false;
                this.listComponent.push({name: 'Agenda', key: 'time_line'});
                break;
            case 'letter':
                this.componentAdding.letters = false;
                this.listComponent.push({name: 'Letter', key: 'letters'});
                break;
            case 'exhibition':
                this.componentAdding.exhibitions = false;
                this.listComponent.push({name: 'Exhibitions', key: 'exhibitions'});
                break;
            case 'company':
                this.componentAdding.companies = false;
                this.listComponent.push({name: 'Companies', key: 'companies'});
                break;
            case 'seatmap':
                this.componentAdding.seatmaps = false;
                this.listComponent.push({name: 'Seat Map', key: 'seatmaps'});
                break;
        }
    }
    changeTime(time) {
        const minTime = _.cloneDeep(time);
        this.minDateTicket = minTime.toISOString(minTime.setDate(minTime.getDate() - 1));
    }

    autoGenerate() {
        const randomString = this.utilitiesService.randomCodeEventBooking();
        this.event.code = randomString;
    }

    handleChangeMethod(method) {
        if (method === 'bank') {
                this.event.tickets.forEach(ticket => {
                        ticket.is_auto_approved = true;
                    if (ticket.children && ticket.children.length > 0) {
                        ticket.children.forEach(child => {
                                child.is_auto_approved = true;
                        });
                    }
                });
        }
    }
}
