import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectGalleriesComponent } from './select-galleries.component';

describe('SelectGalleriesComponent', () => {
  let component: SelectGalleriesComponent;
  let fixture: ComponentFixture<SelectGalleriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectGalleriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectGalleriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
