import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectGalleryComponent } from './select-gallery.component';

describe('SelectGalleryComponent', () => {
  let component: SelectGalleryComponent;
  let fixture: ComponentFixture<SelectGalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectGalleryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
