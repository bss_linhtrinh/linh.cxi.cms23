import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EventRoutingModule} from './event-routing.module';
import {ListEventComponent} from './list-event/list-event.component';
import {CreateEventComponent} from './create-event/create-event.component';
import {EditEventComponent} from './edit-event/edit-event.component';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule} from '@angular/forms';
import { SelectSponsorsComponent } from './sponsor/select-sponsors/select-sponsors.component';
import { SelectSponsorComponent } from './sponsor/select-sponsor/select-sponsor.component';
import { SelectHighlightInfosComponent } from './highlight-info/select-highlight-infos/select-highlight-infos.component';
import { SelectHighlightInfoComponent } from './highlight-info/select-highlight-info/select-highlight-info.component';
import { SelectSpeakersComponent } from './speaker/select-speakers/select-speakers.component';
import { SelectSpeakerComponent } from './speaker/select-speaker/select-speaker.component';
import { SelectTopicsComponent } from './topic/select-topics/select-topics.component';
import { SelectTopicComponent } from './topic/select-topic/select-topic.component';
import { SelectBankInfosComponent } from './bank-info/select-bank-infos/select-bank-infos.component';
import { SelectBankInfoComponent } from './bank-info/select-bank-info/select-bank-info.component';
import { SelectReportersComponent } from './reporter/select-reporters/select-reporters.component';
import { SelectReporterComponent } from './reporter/select-reporter/select-reporter.component';
import { SelectTicketsComponent } from './ticket/select-tickets/select-tickets.component';
import { SelectTicketComponent } from './ticket/select-ticket/select-ticket.component';
import { SelectTicketBreaktimesComponent } from './ticket/select-ticket-breaktimes/select-ticket-breaktimes.component';
import { SelectTicketBreaktimeComponent } from './ticket/select-ticket-breaktime/select-ticket-breaktime.component';
import { SelectEventTimelinesComponent } from './event-timline/select-event-timelines/select-event-timelines.component';
import { SelectEventTimelineComponent } from './event-timline/select-event-timeline/select-event-timeline.component';
import { SelectTimelinesComponent } from './event-timline/select-timelines/select-timelines.component';
import { SelectTimelineComponent } from './event-timline/select-timeline/select-timeline.component';
import { SelectBannersComponent } from './banner/select-banners/select-banners.component';
import { SelectBannerComponent } from './banner/select-banner/select-banner.component';
import {DeleteEventComponent} from './delete-booking/delete-event.component';
import { SelectSubTicketsComponent } from './ticket/select-sub-tickets/select-sub-tickets.component';
import { SelectExhibitionBenefitsComponent } from './exhibition/select-exhibition-benefits/select-exhibition-benefits.component';
import { SelectExhibitionBenefitComponent } from './exhibition/select-exhibition-benefit/select-exhibition-benefit.component';
import { SelectExhibitionNotesComponent } from './exhibition/select-exhibition-notes/select-exhibition-notes.component';
import { SelectExhibitionNoteComponent } from './exhibition/select-exhibition-note/select-exhibition-note.component';
import { SelectCompaniesComponent } from './company/select-companies/select-companies.component';
import { SelectCompanyComponent } from './company/select-company/select-company.component';
import { SelectSeatMapsComponent } from './seat-map/select-seat-maps/select-seat-maps.component';
import { SelectSeatMapComponent } from './seat-map/select-seat-map/select-seat-map.component';
import { SelectLettersComponent } from './letter/select-letters/select-letters.component';
import { SelectLetterComponent } from './letter/select-letter/select-letter.component';
import { SelectSeatMapNotesComponent } from './seat-map/select-seat-map-notes/select-seat-map-notes.component';
import { SelectSeatMapNoteComponent } from './seat-map/select-seat-map-note/select-seat-map-note.component';
import { ColorPickerModule } from 'ngx-color-picker';
import { SelectExhibitorPopupComponent } from './seat-map/select-exhibitor-popup/select-exhibitor-popup.component';
import { ExhibitorInformationPopupComponent } from './seat-map/exhibitor-information-popup/exhibitor-information-popup.component';
import { SelectGalleriesComponent } from './gallery/select-galleries/select-galleries.component';
import { SelectGalleryComponent } from './gallery/select-gallery/select-gallery.component';
import { SelectBannerGroupComponent } from './banner/select-banner-group/select-banner-group.component';
import { SelectBannerGroupsComponent } from './banner/select-banner-groups/select-banner-groups.component';
import { SelectSubEventTimelinesComponent } from './event-timline/select-sub-event-timelines/select-sub-event-timelines.component';
import { SelectSubEventTimelineComponent } from './event-timline/select-sub-event-timeline/select-sub-event-timeline.component';


@NgModule({
    declarations: [ListEventComponent, CreateEventComponent, EditEventComponent, SelectSponsorsComponent, SelectSponsorComponent, SelectHighlightInfosComponent, SelectHighlightInfoComponent, SelectSpeakersComponent, SelectSpeakerComponent, SelectTopicsComponent, SelectTopicComponent, SelectBankInfosComponent, SelectBankInfoComponent, SelectReportersComponent, SelectReporterComponent, SelectTicketsComponent, SelectTicketComponent, SelectTicketBreaktimesComponent, SelectTicketBreaktimeComponent, SelectEventTimelinesComponent, SelectEventTimelineComponent, SelectTimelinesComponent, SelectTimelineComponent, SelectBannersComponent, SelectBannerComponent, DeleteEventComponent, SelectSubTicketsComponent, SelectExhibitionBenefitsComponent, SelectExhibitionBenefitComponent, SelectExhibitionNotesComponent, SelectExhibitionNoteComponent, SelectCompaniesComponent, SelectCompanyComponent, SelectSeatMapsComponent, SelectSeatMapComponent, SelectLettersComponent, SelectLetterComponent, SelectSeatMapNotesComponent, SelectSeatMapNoteComponent, SelectExhibitorPopupComponent, ExhibitorInformationPopupComponent, SelectGalleriesComponent, SelectGalleryComponent, SelectBannerGroupComponent, SelectBannerGroupsComponent, SelectSubEventTimelinesComponent, SelectSubEventTimelineComponent],
    imports: [
        CommonModule,
        ColorPickerModule,
        SharedModule,
        FormsModule,
        EventRoutingModule
    ],
    entryComponents: [
        DeleteEventComponent,
        SelectExhibitorPopupComponent,
        ExhibitorInformationPopupComponent
    ]
})
export class EventModule {
}
