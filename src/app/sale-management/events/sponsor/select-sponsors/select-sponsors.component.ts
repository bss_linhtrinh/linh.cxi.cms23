import {Component, EventEmitter, Inject, Input, OnChanges, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {ElementBase} from '../../../../shared/custom-form/element-base/element-base';
import {Sponsor} from '../../../../shared/entities/sponsor';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';

@Component({
  selector: 'app-select-sponsors',
  templateUrl: './select-sponsors.component.html',
  styleUrls: ['./select-sponsors.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectSponsorsComponent, multi: true}
    ]
})
export class SelectSponsorsComponent extends ElementBase<Sponsor[]> implements OnInit {

    private _name: string;
    @Input() sponsors: any[];
    private _label: string;

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        this._label = value.replace('_', ' ');
    }

    @Input() public level;

    @Input() public size = 'sm';
    @Input() public width = '';

    @Output() addGroup = new EventEmitter();
    @Output() removeGroup = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }


    // add-on types

    addAddOnType() {
        this.value.push(new Sponsor());
    }

    onRemove(index: number) {
        this.value.splice(index, 1);

        // check empty
        this.checkEmpty();

        // check disabled
        // this.checkDisabled();
    }

    onChangeVariantAddOnType() {
        // check disabled
        // this.checkDisabled();
    }

    checkEmpty() {
        if (!this.value || this.value.length === 0) {
            this.addAddOnType();
        }
    }

    // checkDisabled() {
    //     this.sponsors.forEach(addOnType => {
    //         addOnType.disabled = this.value.findIndex(v => v.id === addOnType.id) !== -1;
    //     });
    // }

    isDisabledAddNew() {
        return this.sponsors && this.value && this.sponsors.length === this.value.length;
    }

}
