import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectSponsorsComponent } from './select-sponsors.component';

describe('SelectSponsorsComponent', () => {
  let component: SelectSponsorsComponent;
  let fixture: ComponentFixture<SelectSponsorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSponsorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSponsorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
