import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {ElementBase} from '../../../../shared/custom-form/element-base/element-base';
import {Sponsor} from '../../../../shared/entities/sponsor';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';

@Component({
    selector: 'app-select-sponsor',
    templateUrl: './select-sponsor.component.html',
    styleUrls: ['./select-sponsor.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectSponsorComponent, multi: true}
    ]
})
export class SelectSponsorComponent extends ElementBase<Sponsor> implements OnInit {

    private _name: string;
    private _label: string;


    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        this._label = value.replace('_', ' ');
    }

    @Input() public level;

    @Input() public size = 'sm';
    @Input() public width = '';

    @Output() add = new EventEmitter();
    @Output() remove = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;


    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onChange() {
        //
    }

    removeItem() {
        this.remove.emit();
    }

    onFileContent(file: File) {
        this.value.file = file;
    }

}
