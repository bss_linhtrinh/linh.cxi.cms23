import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-delete-booking',
  templateUrl: './delete-event.component.html',
  styleUrls: ['./delete-event.component.scss']
})
export class DeleteEventComponent implements OnInit {

    @Input() title: string;
    @Input() message = 'Bạn có chắc muốn xóa Hội nghị/ Hội thảo này?';
    @Input() btnYes = 'Có';
    @Input() btnNo = 'Hủy';

    constructor(private ngbActiveModal: NgbActiveModal) {
    }

    ngOnInit() {
    }

    no() {
        this.ngbActiveModal.dismiss('cancel');
    }

    yes() {
        this.ngbActiveModal.close(true);
    }

    cancel() {
        this.ngbActiveModal.dismiss('cancel');
    }

}
