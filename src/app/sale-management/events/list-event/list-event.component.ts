import {Component, OnInit, ViewChild} from '@angular/core';
import {CxiGridColumn} from '../../../shared/components/cxi-grid/cxi-grid-column';
import {EventRepository} from '../../../shared/repositories/event.repository';
import {ActivatedRoute, Router} from '@angular/router';
import {Event} from '../../../shared/entities/event';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CxiGridComponent} from '../../../shared/components/cxi-grid/cxi-grid.component';
import {DeleteEventComponent} from '../delete-booking/delete-event.component';
import {ColumnFormat} from '../../../shared/types/column-format';

@Component({
    selector: 'app-list-event',
    templateUrl: './list-event.component.html',
    styleUrls: ['./list-event.component.scss']
})
export class ListEventComponent implements OnInit {
    private cxiGrid: CxiGridComponent;

    cxiTableConfig = {
        paging: true,
        sorting: true,
        filtering: true,
        pagingServer: true,
        isSort: true,
        repository: this.eventRepository,
        resource: 'resource/event',
        isResource: true,
        actions: {
            onEdit: (row: any) => {
                this.onEdit(row);
            },
            onDelete: (row: any) => {
                this.onDelete(row);
            },
        },
    };
    cxiTableColumns = CxiGridColumn.from([
        {title: 'Title', name: 'title', sort: true, className: 'eventName'},
        {title: 'Location', name: 'location', sort: true, className: 'location'},
        {title: 'Start Time', name: 'start_time', sort: true, filterType: 'datepicker', className: 'startTime', format: ColumnFormat.Date},
        {title: 'End Time', name: 'end_time', sort: true, filterType: 'datepicker', className: 'endTime', format: ColumnFormat.Date},
        {title: 'Booking Type', name: 'booking_type', sort: true, className: 'bookingType'},
    ]);

    constructor(private eventRepository: EventRepository,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
    }

    onDelete(event: Event) {
        this.eventRepository.destroy(event)
            .subscribe();
    }

    onEdit(event: Event) {
        this.router.navigate(['..', event.id], {relativeTo: this.activatedRoute});
    }

}
