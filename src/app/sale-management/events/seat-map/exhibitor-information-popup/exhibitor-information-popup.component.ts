import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-exhibitor-information-popup',
    templateUrl: './exhibitor-information-popup.component.html',
    styleUrls: ['./exhibitor-information-popup.component.scss']
})
export class ExhibitorInformationPopupComponent implements OnInit {

    @Input() exhibitor;

    constructor() {
    }

    ngOnInit() {
    }

}
