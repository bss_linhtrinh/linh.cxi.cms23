import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExhibitorInformationPopupComponent } from './exhibitor-information-popup.component';

describe('ExhibitorInformationPopupComponent', () => {
  let component: ExhibitorInformationPopupComponent;
  let fixture: ComponentFixture<ExhibitorInformationPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExhibitorInformationPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExhibitorInformationPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
