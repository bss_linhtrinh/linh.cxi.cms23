import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectSeatMapComponent } from './select-seat-map.component';

describe('SelectSeatMapComponent', () => {
  let component: SelectSeatMapComponent;
  let fixture: ComponentFixture<SelectSeatMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSeatMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSeatMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
