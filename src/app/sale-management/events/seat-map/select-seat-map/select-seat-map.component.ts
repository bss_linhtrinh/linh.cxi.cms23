import {
    AfterViewChecked,
    AfterViewInit,
    Component,
    EventEmitter,
    HostListener,
    Inject,
    Input, OnChanges,
    OnInit,
    Optional,
    Output, SimpleChanges,
    ViewChild
} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../../shared/custom-form/element-base/element-base';
import {SeatMap} from '../../../../shared/entities/seat-map';
import {fabric} from 'fabric';
import {Image} from '../../../../shared/entities/image';
import {MediaFilesRepository} from '../../../../shared/repositories/media-files.repository';
import {SelectExhibitorPopupComponent} from '../select-exhibitor-popup/select-exhibitor-popup.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {EventRepository} from '../../../../shared/repositories/event.repository';
import {ExhibitorInformationPopupComponent} from '../exhibitor-information-popup/exhibitor-information-popup.component';
import {timer} from 'rxjs';

@Component({
    selector: 'app-select-seat-map',
    templateUrl: './select-seat-map.component.html',
    styleUrls: ['./select-seat-map.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectSeatMapComponent, multi: true}
    ]
})
export class SelectSeatMapComponent extends ElementBase<SeatMap> implements OnInit, OnChanges {

    private _label: string;
    widthCanvas: number;
    height: number;
    listExhibitor = [];
    file: File = null;

    canvas: any;

    @HostListener('window:resize', ['$event'])
    resizeSeatMap() {
        this.getSize();
        this.GetCanvasAtResoution(this.widthCanvas);
    }


    @Input() name: string;
    @Input() event: any;

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        this._label = value.replace('_', ' ');
    }

    @Input() public size = 'sm';
    @Input() public width = '';
    @Input() public seat: any;

    @Output() add = new EventEmitter();
    @Output() remove = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;


    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private mediaFilesRepository: MediaFilesRepository,
        private _ngbModal: NgbModal,
        private eventRepository: EventRepository
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onChange() {
        //
    }

    removeItem() {
        this.remove.emit();
    }

    onFileContent(file: File) {
        this.file = file;
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.seat) {
           this.getCanvas();
        }
    }

    // ngAfterViewInit() {
    //     this.getCanvas();
    // }

    getCanvas() {
        const self = this;
        const canvasName = this.name + '-myCanvas';
        this.canvas = new fabric.Canvas(canvasName);
        if (this.seat.data.annottion) {
            self.canvas.loadFromJSON(this.seat.data.annottion, function () {
                // self.canvas.setDimensions({width: 800, height: 400});
                self.canvas.renderAll();
            }, function(o, object) {
                object.on('mousedblclick', function () {
                });
            });
        }
    }


    GetCanvasAtResoution(newWidth) {
        if (this.value.data.annottion.width !== newWidth) {
            var scaleMultiplier = newWidth / this.value.data.annottion.width;
            var objects = this.value.data.annottion.getObjects();
            for (var i in objects) {
                objects[i].scaleX = objects[i].scaleX * scaleMultiplier;
                objects[i].scaleY = objects[i].scaleY * scaleMultiplier;
                objects[i].left = objects[i].left * scaleMultiplier;
                objects[i].top = objects[i].top * scaleMultiplier;
                objects[i].setCoords();
            }
            var obj = this.value.data.annottion.backgroundImage;
            if (obj) {
                obj.scaleX = obj.scaleX * scaleMultiplier;
                obj.scaleY = obj.scaleY * scaleMultiplier;
            }

            this.value.data.annottion.discardActiveObject();
            this.value.data.annottion.setWidth(this.value.data.annottion.getWidth() * scaleMultiplier);
            this.value.data.annottion.setHeight(this.value.data.annottion.getHeight() * scaleMultiplier);
            this.value.data.annottion.renderAll();
            this.value.data.annottion.calcOffset();
        }
    }

    getSize() {
        const canvasName = this.name + '-seat-map';
        const navBottomHeight = document.getElementById(canvasName).offsetHeight;
        const navBottomWidth = document.getElementById(canvasName).offsetWidth;
        this.widthCanvas = navBottomWidth - 50;
        this.height = navBottomHeight - 30;
    }

    upload(event, el?: any) {
        const canvasName = this.name + '-myCanvas';
        this.value.data.annottion = new fabric.Canvas(canvasName);
        this.getSize();
        const self = this;
        if (event.target.files && event.target.files[0]) {
            this.uploadFile(event.target.files[0]);

            const reader = new FileReader();
            reader.onload = function (f: any) {
                const data = f.target.result;
                fabric.Image.fromURL(data, function (img) {
                    // self.height = img.height;
                    // add background image
                    self.value.data.annottion.setDimensions({width: self.widthCanvas, height: self.height});
                    self.value.data.annottion.setBackgroundImage(img, self.value.data.annottion.renderAll.bind(self.value.data.annottion), {
                        scaleX: self.value.data.annottion.width / img.width,
                        scaleY: self.value.data.annottion.height / img.height
                    });
                });
            };
            reader.readAsDataURL(event.target.files[0]); // read file as data url
        }
    }

    uploadFile(file: File, el?: any) {
        this.mediaFilesRepository.uploadFile(file)
            .subscribe(
                (image: Image) => {

                    // add media-file
                    if (!this.value.image_ids || this.value.image_ids.length === 0) {
                        this.value.image_ids = [];
                    }

                    this.value.image_ids.push(image);
                },
                () => {
                }
            );
    }

    addNote() {
        if (this.event) {
            const self = this;
            this.value.data.annottion.on({
                'mouse:down': function (e) {
                    const dimensions = self.value.data.annottion.getPointer(e);
                    self.getListExhibitor(dimensions);
                    self.value.data.annottion.off('mouse:down');
                }
            });
        }
    }

    getListExhibitor(dimensions) {
        this.eventRepository.getListExhibitor(this.event).subscribe(res => {
            this.listExhibitor = res;
            this.openModalSelectExhibitor(dimensions);
        });
    }

    openModalSelectExhibitor(dimensions) {
        const self = this;
        const modalRef = this._ngbModal.open(SelectExhibitorPopupComponent, {windowClass: 'select-exhibitor-popup'});
        modalRef.componentInstance.listExhibitors = this.listExhibitor;
        modalRef.result
            .then(
                (res) => {
                    const circle = new fabric.Circle({
                        left: dimensions.x - 20,
                        top: dimensions.y - 20,
                        radius: 30,
                        name: res,
                        hasControls: false,
                        hasBorders: false,
                        fill: 'rgba(255,0,0,0.5)',
                    });
                    this.value.data.annottion.add(circle);
                    circle.on('mousedblclick', function () {
                        self.openModalExhibitorInfo(circle.name);
                    });
                },
                () => {
                },
            );
    }

    openModalExhibitorInfo(exhibitor) {
        const modalRef = this._ngbModal.open(ExhibitorInformationPopupComponent, {windowClass: 'exhibitor-information-popup'});
        modalRef.componentInstance.exhibitor = exhibitor;
        modalRef.result
            .then(
                (res) => {
                },
                () => {
                },
            );
    }
}
