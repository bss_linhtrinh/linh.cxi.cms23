import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectSeatMapNotesComponent } from './select-seat-map-notes.component';

describe('SelectSeatMapNotesComponent', () => {
  let component: SelectSeatMapNotesComponent;
  let fixture: ComponentFixture<SelectSeatMapNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSeatMapNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSeatMapNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
