import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {SeatMapNote} from '../../../../shared/entities/seat-map-note';
import {ElementBase} from '../../../../shared/custom-form/element-base/element-base';

@Component({
  selector: 'app-select-seat-map-notes',
  templateUrl: './select-seat-map-notes.component.html',
  styleUrls: ['./select-seat-map-notes.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectSeatMapNotesComponent, multi: true}
    ]
})
export class SelectSeatMapNotesComponent extends ElementBase<SeatMapNote[]> implements OnInit {

    private _name: string;
    @Input() highlight: any[];
    private _label: string;

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        this._label = value.replace('_', ' ');
    }

    @Input() public size = 'sm';
    @Input() public width = '';

    @Output() addGroup = new EventEmitter();
    @Output() removeGroup = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }


    // add-on types

    addAddOnType() {
        this.value.push(new SeatMapNote());
    }

    onRemove(index: number) {
        this.value.splice(index, 1);

        // check empty
        this.checkEmpty();

        // check disabled
        // this.checkDisabled();
    }

    onChangeVariantAddOnType() {
        // check disabled
        // this.checkDisabled();
    }

    checkEmpty() {
        if (!this.value || this.value.length === 0) {
            this.addAddOnType();
        }
    }

    // checkDisabled() {
    //     this.highlight.forEach(addOnType => {
    //         addOnType.disabled = this.value.findIndex(v => v.id === addOnType.id) !== -1;
    //     });
    // }

    isDisabledAddNew() {
        return this.highlight && this.value && this.highlight.length === this.value.length;
    }

}
