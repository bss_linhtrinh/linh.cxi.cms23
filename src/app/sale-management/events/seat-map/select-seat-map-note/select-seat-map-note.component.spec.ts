import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectSeatMapNoteComponent } from './select-seat-map-note.component';

describe('SelectSeatMapNoteComponent', () => {
  let component: SelectSeatMapNoteComponent;
  let fixture: ComponentFixture<SelectSeatMapNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSeatMapNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSeatMapNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
