import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../../shared/custom-form/element-base/element-base';
import {SeatMap} from '../../../../shared/entities/seat-map';

@Component({
  selector: 'app-select-seat-maps',
  templateUrl: './select-seat-maps.component.html',
  styleUrls: ['./select-seat-maps.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectSeatMapsComponent, multi: true}
    ]
})
export class SelectSeatMapsComponent extends ElementBase<SeatMap[]> implements OnInit {

    private _name: string;
    @Input() sponsors: any[];
    private _label: string;

    @Input() event: any;

    @Input('name')
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value.replace('_', ' ');
    }

    @Input()
    get label(): string {
        return this._label;
    }

    set label(value: string) {
        this._label = value.replace('_', ' ');
    }

    @Input() public size = 'sm';
    @Input() public width = '';

    @Output() addGroup = new EventEmitter();
    @Output() removeGroup = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }


    // add-on types

    addAddOnType() {
        this.value.push(new SeatMap());
    }

    onRemove(index: number) {
        this.value.splice(index, 1);

        // check empty
        this.checkEmpty();

        // check disabled
        // this.checkDisabled();
    }

    onChangeVariantAddOnType() {
        // check disabled
        // this.checkDisabled();
    }

    checkEmpty() {
        if (!this.value || this.value.length === 0) {
            this.addAddOnType();
        }
    }

    // checkDisabled() {
    //     this.sponsors.forEach(addOnType => {
    //         addOnType.disabled = this.value.findIndex(v => v.id === addOnType.id) !== -1;
    //     });
    // }

    isDisabledAddNew() {
        return this.sponsors && this.value && this.sponsors.length === this.value.length;
    }

}
