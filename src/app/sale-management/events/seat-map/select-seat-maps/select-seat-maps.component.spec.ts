import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectSeatMapsComponent } from './select-seat-maps.component';

describe('SelectSeatMapsComponent', () => {
  let component: SelectSeatMapsComponent;
  let fixture: ComponentFixture<SelectSeatMapsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSeatMapsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSeatMapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
