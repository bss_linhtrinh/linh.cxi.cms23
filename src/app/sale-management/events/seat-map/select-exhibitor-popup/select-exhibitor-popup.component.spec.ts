import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectExhibitorPopupComponent } from './select-exhibitor-popup.component';

describe('SelectExhibitorPopupComponent', () => {
  let component: SelectExhibitorPopupComponent;
  let fixture: ComponentFixture<SelectExhibitorPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectExhibitorPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectExhibitorPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
