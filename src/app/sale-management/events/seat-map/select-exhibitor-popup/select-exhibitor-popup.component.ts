import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-select-exhibitor-popup',
    templateUrl: './select-exhibitor-popup.component.html',
    styleUrls: ['./select-exhibitor-popup.component.scss']
})
export class SelectExhibitorPopupComponent implements OnInit {
    type = 'booth';

    @Input() listExhibitors = [];
    types = [{value: 'booth', name: 'Booth'}];

    constructor(private ngbActiveModal: NgbActiveModal) {
    }

    ngOnInit() {
    }

    add(item) {
        this.ngbActiveModal.close(item);
    }
}
