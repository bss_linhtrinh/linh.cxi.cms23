import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    // dashboard
    {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(mod => mod.DashboardModule),
        data: { breadcrumb: 'Dashboard' }
    },



    {
        path: 'events',
        loadChildren: () => import('./events/event.module').then(mod => mod.EventModule),
        data: { breadcrumb: 'Events' }
    },

    {
        path: 'pages',
        loadChildren: () => import('./content-management/page-management/page-management.module').then(mod => mod.PageManagementModule),
        data: { breadcrumb: 'Page Management' }
    },

    {
        path: 'modules',
        loadChildren: () => import('./content-management/module-management/module-management.module').then(mod => mod.ModuleManagementModule),
        data: { breadcrumb: 'Modules Management' }
    },

    {
        path: 'mail',
        loadChildren: () => import('./content-management/mail-management/mail-management.module').then(mod => mod.MailManagementModule),
        data: { breadcrumb: 'Mail Management' }
    },


    // {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SaleManagementRoutingModule {
}
