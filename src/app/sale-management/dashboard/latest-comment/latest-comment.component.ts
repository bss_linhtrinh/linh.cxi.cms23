import { Component, OnInit, Input } from '@angular/core';
import { latestCommentRepository } from 'src/app/shared/repositories/latest-comment-report.repository';

@Component({
  selector: 'app-latest-comment',
  templateUrl: './latest-comment.component.html',
  styleUrls: ['./latest-comment.component.scss']
})
export class LatestCommentComponent implements OnInit {

  @Input() timeSelected: string;
  @Input() dateStart: string;
  @Input() dateEnd: string;

  public latest_comments = [];

  constructor(
    private latestComment: latestCommentRepository,
  ) { }

  ngOnInit() {
    this.getListLatestComment();
  }
  getListLatestComment() {
    this.latestComment.all({ date_start: this.dateStart, date_end: this.dateEnd })
      .subscribe(
        (res: any) => {
          this.latest_comments = res;
        },
        (error) => {
          console.log(error)
        }
      );
  }
}
