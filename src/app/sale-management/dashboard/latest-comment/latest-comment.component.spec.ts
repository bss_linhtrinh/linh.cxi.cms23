import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LatestCommentComponent } from './latest-comment.component';

describe('LatestCommentComponent', () => {
  let component: LatestCommentComponent;
  let fixture: ComponentFixture<LatestCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LatestCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LatestCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
