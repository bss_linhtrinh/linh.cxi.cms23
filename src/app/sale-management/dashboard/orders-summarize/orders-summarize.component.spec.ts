import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersSummarizeComponent } from './orders-summarize.component';

describe('OrdersSummarizeComponent', () => {
  let component: OrdersSummarizeComponent;
  let fixture: ComponentFixture<OrdersSummarizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersSummarizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersSummarizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
