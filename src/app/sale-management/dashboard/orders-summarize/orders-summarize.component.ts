import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {Label} from 'ng2-charts';
import {CHART_TYPE} from '../../../shared/constants/charts';
import {ReportService} from '../../../shared/services/report/report.service';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import {debounceTime, distinctUntilChanged, map, tap} from 'rxjs/operators';

@Component({
    selector: 'app-orders-summarize',
    templateUrl: './orders-summarize.component.html',
    styleUrls: ['./orders-summarize.component.scss']
})
export class OrdersSummarizeComponent implements OnInit, OnChanges {

    chartOptions: ChartOptions = {
        responsive: true,
        responsiveAnimationDuration: 500,
        maintainAspectRatio: false,
        legend: {
            display: true,
            position: 'top',
            // align: 'start', waiting for chart.js 2.9.0
            fullWidth: false,

            labels: {
                boxWidth: 4,
                fontColor: '#16171a',
                fontSize: 14,
                fontFamily: 'sfprotextregular',

                padding: 30,

                usePointStyle: true
            }
        },
        elements: {
            point: {
                radius: 3,
                pointStyle: 'circle',
                borderWidth: 3
            }
        }
    };
    chartLabels: Label[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    chartData: ChartDataSets[] = [
        // {data: [65, 59, 80, 81, 56, 55, 40, 59, 80, 81, 56, 55], label: 'Dining'},
        // {data: [28, 48, 40, 19, 86, 27, 90, 65, 59, 80, 81, 56], label: 'Pick-up'},
        // {data: [28, 48, 40, 19, 86, 27, 90, 40, 19, 86, 27, 90], label: 'Delivery'}
        {data: [], label: 'Dining'},
        {data: [], label: 'Pick-up'},
        {data: [], label: 'Delivery'},
    ];
    chartColors = [
        { // blue-500,
            borderColor: '#0097ff',
            backgroundColor: '#0097ff',
        },
        { // purple-500,
            borderColor: '#6737e1',
            backgroundColor: '#6737e1',
        },
        { // green-500,
            borderColor: '#00cc00',
            backgroundColor: '#00cc00',
        }
    ];
    chartLegend = true;
    chartType: ChartType = CHART_TYPE;

    @Input() timeSelected: string;
    @Input() dateStart: string;
    @Input() dateEnd: string;

    constructor(
        private reportService: ReportService,
        private utilitiesService: UtilitiesService
    ) {
    }

    ngOnInit() {
        this.refreshChart();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.timeSelected ||
            changes.dateStart ||
            changes.dateEnd) {
            this.refreshChart();
        }
    }

    refreshChart() {
        this.getReportData();
    }

    getReportData() {
        const params = {
            page: 1,
            per_page: 4,
            order_by: 'created_at',
            order: 'desc',

            // date
            date_start: this.utilitiesService.parseDate(this.dateStart),
            date_end: this.utilitiesService.parseDate(this.dateEnd),
            group_by: this.timeSelected
        };
        this.reportService.getOrdersSummarize(params)
            .pipe(
                debounceTime(500),
                distinctUntilChanged(),
                map(
                    (chartData: any[]) => this.mapChartData(chartData)
                ),
            )
            .subscribe(
                (chartData: any) => {
                    this.chartData = chartData.data;
                    this.chartLabels = chartData.labels;
                }
            );
    }


    mapChartData(chartData: any[]) {
        const labels = [];
        const delivery = {
            data: [],
            label: 'Delivery'
        };
        const dine_in = {
            data: [],
            label: 'Dining'
        };
        const pick_up = {
            data: [],
            label: 'Pick-up'
        };

        chartData.forEach(
            item => {
                if (item.label) {
                    if (!labels.includes(item.label)) {
                        labels.push(item.label);
                    }
                    const currentIndex = labels.findIndex(label => label === item.label);
                    if (currentIndex !== -1) {
                        if (item.delivery) {
                            delivery.data.push(item.delivery);
                        } else {
                            delivery.data.push(0);
                        }
                        if (item.dine_in) {
                            dine_in.data.push(item.dine_in);
                        } else {
                            dine_in.data.push(0);
                        }
                        if (item.pick_up) {
                            pick_up.data.push(item.pick_up);
                        } else {
                            pick_up.data.push(0);
                        }
                    }
                }
            }
        );

        const resultChartData = {
            labels: labels,
            data: [dine_in, pick_up, delivery]
        };

        return resultChartData;
    }
}
