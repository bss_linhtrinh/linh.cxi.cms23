import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {map, tap} from 'rxjs/operators';
import {ReportService} from '../../../shared/services/report/report.service';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';

@Component({
    selector: 'app-delivery-management',
    templateUrl: './delivery-management.component.html',
    styleUrls: ['./delivery-management.component.scss']
})
export class DeliveryManagementComponent implements OnInit, OnChanges {

    @Input() timeSelected: string;
    @Input() dateStart: string;
    @Input() dateEnd: string;

    lineChartType: ChartType = 'line';
    lineChartOptions: ChartOptions = {
        responsive: true,
        responsiveAnimationDuration: 500,
        maintainAspectRatio: false,
        layout: {
            padding: {
                left: -10,
                right: 0,
                top: 0,
                bottom: 0
            }
        },
        legend: {
            display: false,
        },
        scales: {
            xAxes: [{
                type: 'category',
                gridLines: {
                    display: false,
                    drawBorder: false,
                },
                ticks: {
                    display: false,
                },
            }],
            yAxes: [{
                gridLines: {
                    display: false,
                    drawBorder: false,
                },
                ticks: {
                    display: false,
                }
            }]
        }
    };
    lineChartLabels = [];
    lineChartData1: ChartDataSets[] = [
        {
            data: [32, 28, 27, 31, 36, 35, 36],
            backgroundColor: 'white',
            fill: 'top',
            borderColor: '#00cc00', // color 1
            borderWidth: 2,
            pointRadius: 0,
        },
    ];

    // lineChart2
    lineChartData2: ChartDataSets[] = [
        {
            data: [26, 28, 33, 31, 29, 44, 47],
            backgroundColor: 'white',
            fill: 'top',
            borderColor: '#2ac2d0', // $accent-500
            borderWidth: 2,
            pointRadius: 0,
        },
    ];

    // lineChart3
    lineChartData3: ChartDataSets[] = [
        {
            data: [28, 28, 27, 31, 36, 44, 52],
            backgroundColor: 'white',
            fill: 'top',
            borderColor: '#f4341b', // green
            borderWidth: 2,
            pointRadius: 0,
        },
    ];

    canceledOrders: number;
    deliveredOrders: number;
    totalDeliveryOrders: number;

    constructor(
        private reportService: ReportService,
        private utilitiesService: UtilitiesService
    ) {
    }

    ngOnInit() {
        this.refreshChart();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.timeSelected ||
            changes.dateStart ||
            changes.dateEnd) {
            this.refreshChart();
        }
    }

    refreshChart() {
        this.getDeliveryStatistics();
        this.getDeliveryChart();
    }

    getDeliveryStatistics() {
        const params = {
            page: 1,
            per_page: 4,
            order_by: 'created_at',
            order: 'desc',

            // date
            date_start: this.utilitiesService.parseDate(this.dateStart),
            date_end: this.utilitiesService.parseDate(this.dateEnd),
            group_by: this.timeSelected
        };
        this.reportService.getDeliveryManagementStatistics(params)
            .pipe(
                map(
                    (chartData: any[]) => chartData[0]
                ),
            )
            .subscribe(
                (chartData: any) => {
                    this.canceledOrders = chartData.canceled_orders;
                    this.deliveredOrders = chartData.delivered_orders;
                    this.totalDeliveryOrders = chartData.total_delivery_orders;
                }
            );
    }

    getDeliveryChart() {
        const params = {
            page: 1,
            per_page: 4,
            order_by: 'created_at',
            order: 'desc',

            // date
            date_start: this.utilitiesService.parseDate(this.dateStart),
            date_end: this.utilitiesService.parseDate(this.dateEnd),
            group_by: this.timeSelected
        };
        this.reportService.getDeliveryManagementStatisticsForCharts(params)
            .pipe(
                map(
                    (chartData: any[]) => this.mapChartData(chartData)
                ),
                tap(
                    data => console.log(data)
                )
            )
            .subscribe(
                (chartData: any) => {
                    this.lineChartData1[0].data = chartData.totalDeliveryOrders;
                    this.lineChartData2[0].data = chartData.onTimeOrders;
                    this.lineChartData3[0].data = chartData.canceledOrders;
                }
            );
    }

    mapChartData(chartData: any[]) {
        const totalDeliveryOrders = [];
        const onTimeOrders = [];
        const canceledOrders = [];

        chartData.forEach(
            item => {
                if (item.label) {
                    if (item.total_delivery_orders) {
                        totalDeliveryOrders.push(item.total_delivery_orders);
                    } else {
                        totalDeliveryOrders.push(0);
                    }
                    if (item.delivered_orders) {
                        onTimeOrders.push(item.delivered_orders);
                    } else {
                        onTimeOrders.push(0);
                    }
                    if (item.canceled_orders) {
                        canceledOrders.push(item.canceled_orders);
                    } else {
                        canceledOrders.push(0);
                    }
                }
            }
        );

        // minimum 4 items
        if (!totalDeliveryOrders || totalDeliveryOrders.length < 2) {
            for (let i = 0; i <= 2 - totalDeliveryOrders.length; i++) {
                totalDeliveryOrders.unshift(0);
            }
        }
        if (!onTimeOrders || onTimeOrders.length <= 1) {
            for (let i = 0; i <= 2 - onTimeOrders.length; i++) {
                onTimeOrders.unshift(0);
            }
        }
        if (!canceledOrders || canceledOrders.length <= 1) {
            for (let i = 0; i <= 2 - canceledOrders.length; i++) {
                canceledOrders.unshift(0);
            }
        }

        const resultChartData = {
            totalDeliveryOrders: totalDeliveryOrders,
            onTimeOrders: onTimeOrders,
            canceledOrders: canceledOrders,
        };

        return resultChartData;
    }
}
