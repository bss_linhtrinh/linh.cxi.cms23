import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DashboardRoutingModule} from './dashboard-routing.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import {FormsModule} from '@angular/forms';
import {ChartsModule} from 'ng2-charts';
import {SharedModule} from '../../shared/shared.module';
import {TrendingOrdersComponent} from './trending-orders/trending-orders.component';
import {LatestCustomersComponent} from './latest-customers/latest-customers.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {OrdersSummarizeComponent} from './orders-summarize/orders-summarize.component';
import {DeliveryManagementComponent} from './delivery-management/delivery-management.component';
import {LoyaltyProgramComponent} from './loyalty-program/loyalty-program.component';
import { LatestCommentComponent } from './latest-comment/latest-comment.component';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import {CustomerLevelComponent} from './loyalty-program/customer-level/customer-level.component';

export const components = [
    DashboardComponent,
    TrendingOrdersComponent,
    LatestCustomersComponent,
    OrdersSummarizeComponent,
    DeliveryManagementComponent,
    LoyaltyProgramComponent
];

@NgModule({
    declarations: [
        ...components,
        LatestCommentComponent,
        CustomerLevelComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ChartsModule,
        NgbModule,
        SharedModule,
        DashboardRoutingModule,
        SlickCarouselModule
    ]
})
export class DashboardModule {
}
