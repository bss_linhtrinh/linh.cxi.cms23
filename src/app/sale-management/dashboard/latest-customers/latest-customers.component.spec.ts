import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LatestCustomersComponent } from './latest-customers.component';

describe('LatestCustomersComponent', () => {
  let component: LatestCustomersComponent;
  let fixture: ComponentFixture<LatestCustomersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LatestCustomersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LatestCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
