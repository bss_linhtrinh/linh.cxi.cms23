import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Customer} from '../../../shared/entities/customer';
import {CustomerRepository} from '../../../shared/repositories/customers.repository';
import {AuthService} from '../../../shared/services/auth/auth.service';
import {Levels} from '../../../shared/types/levels';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import {CxiGridColumn} from '../../../shared/components/cxi-grid/cxi-grid-column';
import {CxiGridConfig} from '../../../shared/components/cxi-grid/cxi-grid-config';

@Component({
    selector: 'app-latest-customers',
    templateUrl: './latest-customers.component.html',
    styleUrls: ['./latest-customers.component.scss']
})
export class LatestCustomersComponent implements OnInit, OnChanges {
    @Input() timeSelected: string;
    @Input() dateStart: string;
    @Input() dateEnd: string;

    customers: Customer[] = [];

    // table
    cxiTableConfig = CxiGridConfig.from({
        scrolling: true,
    });
    cxiTableColumns = CxiGridColumn.from([
        {title: 'name', name: 'name', avatar: 'avatar'},
        {title: 'phone', name: 'phone'},
        {title: 'address', name: 'address'},
        {title: 'email', name: 'email'},
        // {title: 'level', name: 'level'},
    ]);

    constructor(
        private customersRepository: CustomerRepository,
        private authService: AuthService,
        private utilitiesService: UtilitiesService
    ) {
    }

    ngOnInit() {
        this.getCustomers();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.dateStart || changes.dateEnd) {
            this.getCustomers();
        }
    }

    getCustomers() {
        const level = this.authService.getCurrentLevel();
        if (level === Levels.BSS) {
            return;
        }

        if (!this.dateStart || !this.dateEnd) {
            return;
        }

        const params = {
            page: 1,
            per_page: 10,
            order_by: 'created_at',
            order: 'desc',

            // date
            date_start: this.utilitiesService.parseDate(this.dateStart),
            date_end: this.utilitiesService.parseDate(this.dateEnd),
        };
        this.customersRepository.getLatestCustomers(params)
            .subscribe(
                (customers: Customer[]) => {
                    this.customers = customers;
                }
            );
    }
}
