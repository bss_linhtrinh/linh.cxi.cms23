import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendingOrdersComponent } from './trending-orders.component';

describe('TrendingOrdersComponent', () => {
  let component: TrendingOrdersComponent;
  let fixture: ComponentFixture<TrendingOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrendingOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendingOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
