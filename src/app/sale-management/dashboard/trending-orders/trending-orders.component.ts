import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Levels} from '../../../shared/types/levels';
import {AuthService} from '../../../shared/services/auth/auth.service';
import {OrdersRepository} from '../../../shared/repositories/orders.repository';
import {Order} from '../../../shared/entities/order';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';

@Component({
    selector: 'app-trending-orders',
    templateUrl: './trending-orders.component.html',
    styleUrls: ['./trending-orders.component.scss']
})
export class TrendingOrdersComponent implements OnInit, OnChanges {

    @Input() timeSelected: string;
    @Input() dateStart: string;
    @Input() dateEnd: string;

    orders: Order[];

    constructor(
        private ordersRepository: OrdersRepository,
        private authService: AuthService,
        private utilitiesService: UtilitiesService
    ) {
    }

    ngOnInit() {
        this.getTrendingOrders();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.dateStart || changes.dateEnd) {
            this.getTrendingOrders();
        }
    }

    getTrendingOrders() {
        const level = this.authService.getCurrentLevel();
        if (level === Levels.BSS) {
            return;
        }

        const params = {
            page: 1,
            per_page: 10,
            order_by: 'created_at',
            order: 'desc',

            // date
            date_start: this.utilitiesService.parseDate(this.dateStart),
            date_end: this.utilitiesService.parseDate(this.dateEnd),
        };
        this.ordersRepository.getTrendingOrders(params)
            .subscribe(
                (orders: Order[]) => {
                    this.orders = orders;
                }
            );
    }
}
