import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import {ReportService} from '../../../shared/services/report/report.service';
import {LocalStorage} from 'ngx-webstorage';
import {Scope} from '../../../shared/types/scopes';

@Component({
    selector: 'app-loyalty-program-statistics',
    templateUrl: './loyalty-program.component.html',
    styleUrls: ['./loyalty-program.component.scss']
})
export class LoyaltyProgramComponent implements OnInit, OnChanges {

    // time
    @Input() timeSelected: string;
    @Input() dateStart: string;
    @Input() dateEnd: string;

    @LocalStorage('scope.brand_id') brandId: Scope;
    @LocalStorage('scope.current') currentScope: Scope;

    loyaltyStatistic: any;

    slideConfig = {
        slidesToShow: 4,
        slidesToScroll: 4,
        infinite: false
    };

    constructor(
        private utilitiesService: UtilitiesService,
        private reportService: ReportService
    ) {
    }

    ngOnInit() {
        this.getData();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.timeSelected ||
            changes.dateStart ||
            changes.dateEnd) {
            this.getData();
        }
    }

    getData() {
        if (this.currentScope === Scope.Brand) {
            this.getLoyaltyProgramStatistics();
        }
    }

    getLoyaltyProgramStatistics() {
        const params = {
            date_start: this.utilitiesService.parseDate(this.dateStart),
            date_end: this.utilitiesService.parseDate(this.dateEnd),
            group_by: this.timeSelected,
            brand_id: this.brandId
        };
        this.reportService.getLoyaltyProgramStatistics(params)
            .pipe(
                debounceTime(500),
                distinctUntilChanged(),

                tap((loyaltyStatistic: any) => this.loyaltyStatistic = loyaltyStatistic),
            )
            .subscribe(
            );
    }

    getColor(i: number) {
        let c = null;
        switch (i) {
            case 0:
                c = 'accent';
                break;
            case 1:
                c = 'green';
                break;
            case 2:
                c = 'grey';
                break;
            case 3:
                c = 'yellow';
                break;
            case 4:
                c = 'blue';
                break;
        }

        return c;
    }
}
