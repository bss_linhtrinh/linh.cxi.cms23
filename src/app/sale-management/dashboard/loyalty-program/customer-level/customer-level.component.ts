import {Component, Input, OnInit} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {TranslateService} from '@ngx-translate/core';
import {map, tap} from 'rxjs/operators';

@Component({
    selector: 'app-customer-level',
    templateUrl: './customer-level.component.html',
    styleUrls: ['./customer-level.component.scss']
})
export class CustomerLevelComponent implements OnInit {
    @Input() customerLevel: any;
    @Input() color: string;

    constructor(
        private translateService: TranslateService
    ) {
    }

    ngOnInit() {
    }

    getImageUrl(path: string) {
        return `${environment.baseUrl}${path}`;
    }

    getTranslatedName(name: string) {
        return this.translateService.get(`Loyalty Program Statistic.${name}`)
            .pipe(
                map((translatedName: string) => {
                    if (translatedName === `Loyalty Program Statistic.${name}`) {
                        return name;
                    }
                    return translatedName;
                })
            );
    }
}
