import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../shared/services/auth/auth.service';
import {Levels} from '../../../shared/types/levels';
import {LocalStorage} from 'ngx-webstorage';
import {timer} from 'rxjs';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import { formatDate } from '@angular/common';

@Component({
    selector: 'cxi-sale-management-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    userLevel: Levels;

    // show
    isShowLatestCustomer = false;
    isShowTrendingOrder = false;
    public today = new Date();
    // public dateEnd = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');
    // public dateStart = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');

    // time
    @LocalStorage('dashboard.timeSelected') timeSelected: string;
    @LocalStorage('dashboard.dateStart') dateStart = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');
    @LocalStorage('dashboard.dateEnd') dateEnd = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');

    constructor(private authService: AuthService,
                private utilitiesService: UtilitiesService) {
    }

    ngOnInit() {
        if (!this.timeSelected) {
            this.timeSelected = 'year';
        }
        if (!this.dateEnd) {
            this.dateEnd = this.utilitiesService.parseDate(new Date());
        }
        if (!this.dateStart) {
            const dateStart = this.dateEnd ? new Date(this.dateEnd) : new Date();
            dateStart.setMonth(dateStart.getMonth() - 3);
            this.dateStart = this.utilitiesService.parseDate(dateStart);
        }

        this.getCustomers();
    }


    selectTime(timeSelected: string) {
        this.timeSelected = timeSelected;
    }

    getCustomers() {
        // get Current user's level (not filter)
        this.userLevel = this.authService.getCurrentLevel();
        if (this.userLevel !== Levels.BSS) {
            timer(100).subscribe(() => this.isShowLatestCustomer = true);
            timer(100).subscribe(() => this.isShowTrendingOrder = true);
            return;
        } else {
            this.isShowLatestCustomer = false;
            this.isShowTrendingOrder = false;
        }
    }

    openDate() {
    }
}
