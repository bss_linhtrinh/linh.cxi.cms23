import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SaleManagementRoutingModule} from './sale-management-routing.module';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        SaleManagementRoutingModule,
    ]
})
export class SaleManagementModule {
}
