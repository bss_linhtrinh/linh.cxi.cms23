import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectModuleImagesComponent } from './select-module-images.component';

describe('SelectModuleImagesComponent', () => {
  let component: SelectModuleImagesComponent;
  let fixture: ComponentFixture<SelectModuleImagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectModuleImagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectModuleImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
