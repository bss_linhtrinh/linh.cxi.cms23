import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectModuleImageComponent } from './select-module-image.component';

describe('SelectModuleImageComponent', () => {
  let component: SelectModuleImageComponent;
  let fixture: ComponentFixture<SelectModuleImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectModuleImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectModuleImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
