import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ModuleManagementRoutingModule} from './module-management-routing.module';
import {CreateModuleComponent} from './create-module/create-module.component';
import {ListModuleComponent} from './list-module/list-module.component';
import {EditModuleComponent} from './edit-module/edit-module.component';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule} from '@angular/forms';
import { SelectModuleImageComponent } from './select-module-image/select-module-image.component';
import { SelectModuleImagesComponent } from './select-module-images/select-module-images.component';


@NgModule({
    declarations: [CreateModuleComponent, ListModuleComponent, EditModuleComponent, SelectModuleImageComponent, SelectModuleImagesComponent],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        ModuleManagementRoutingModule
    ]
})
export class ModuleManagementModule {
}
