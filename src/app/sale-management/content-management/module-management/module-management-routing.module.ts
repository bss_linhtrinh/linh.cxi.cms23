import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListModuleComponent} from './list-module/list-module.component';
import {CreateModuleComponent} from './create-module/create-module.component';
import {EditModuleComponent} from './edit-module/edit-module.component';


const routes: Routes = [
    {
        path: '',
        canActivate: [],
        children: [
            {
                path: 'list',
                component: ListModuleComponent,
                data: {breadcrumb: 'List module'}
            },
            {
                path: 'create',
                component: CreateModuleComponent,
                data: {breadcrumb: 'Create module'}
            },
            {
                path: ':id',
                component: EditModuleComponent,
                data: {breadcrumb: 'Edit module'}
            },
            {path: '', redirectTo: 'list', pathMatch: 'full'},
        ]
    }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModuleManagementRoutingModule { }
