import { Component, OnInit } from '@angular/core';
import {Contents} from '../../../../shared/entities/contents';
import {ActivatedRoute, Router} from '@angular/router';
import {EventRepository} from '../../../../shared/repositories/event.repository';
import {UtilitiesService} from '../../../../shared/services/utilities/utilities.service';
import {ContentManagementRepository} from '../../../../shared/repositories/content-management.repository';

declare const helper: any;

@Component({
  selector: 'app-edit-module',
  templateUrl: './edit-module.component.html',
  styleUrls: ['./edit-module.component.scss']
})
export class EditModuleComponent implements OnInit {

    content: Contents = new Contents();
    file: File = null;
    componentAdding = {
        vi: false,
        en: false,
    };
    component: string;

    listComponent = [
        {name: 'Vietnamese', key: 'vi'},
        {name: 'English', key: 'en'},
    ];

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private eventRepository: EventRepository,
                public utilitiesService: UtilitiesService,
                private contentManagementRepository: ContentManagementRepository) {
    }

    ngOnInit() {
        this.loadPage();
    }

    onSubmit() {
        const content = this.parseData(this.content);
        this.contentManagementRepository.save(content).subscribe(res => {
                if (res) {
                    this.router.navigateByUrl('/modules');
                    helper.showNotification(`Updated successfully!!`, 'done', 'success');
                }
            },
            err => {
                this.router.navigateByUrl('/modules');
                helper.showNotification(`Updated successfully!!`, 'done', 'success');
            });
    }

    loadPage() {
        this.activatedRoute.params.subscribe((data) => {
            const id = data.id;
            this.contentManagementRepository.find(id).subscribe((res) => {
                this.prepareComponent(res);
                this.content = res;
            });
        });
    }

    prepareComponent(content) {
        if (content.vi && content.vi.title) {
            this.componentAdding.vi = true;
            this.listComponent = this.listComponent.filter(item => item.key !== 'vi');
        }
        if (content.en && content.en.title) {
            this.componentAdding.en = true;
            this.listComponent = this.listComponent.filter(item => item.key !== 'en');
        }
    }

    addComponent() {
        const data = this.componentAdding;
        Object.keys(data).forEach((key: any) => {
            if (key === this.component) {
                data[key] = true;
            }
        });
        this.listComponent = this.listComponent.filter(item => item.key !== this.component);
        this.component = '';
    }

    deleteComponent(component) {
        switch (component) {
            case 'vietnamese':
                this.componentAdding.vi = false;
                this.listComponent.push({name: 'Vietnamese', key: 'vi'});
                break;
            case 'english':
                this.componentAdding.en = false;
                this.listComponent.push({name: 'English', key: 'en'});
                break;
        }
    }

    parseData(content) {
        if (!this.componentAdding.vi) {
            delete content.vi ;
        }
        if (!this.componentAdding.en) {
            delete content.en;
        }
        return content;
    }

    onFileContent(file: any) {
        this.file = file;
    }

}
