import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PageManagementRoutingModule} from './page-management-routing.module';
import {CreatePageComponent} from './create-page/create-page.component';
import {ListPageComponent} from './list-page/list-page.component';
import {EditPageComponent} from './edit-page/edit-page.component';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule} from '@angular/forms';


@NgModule({
    declarations: [CreatePageComponent, ListPageComponent, EditPageComponent],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        PageManagementRoutingModule
    ]
})
export class PageManagementModule {
}
