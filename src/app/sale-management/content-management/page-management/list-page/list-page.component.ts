import { Component, OnInit } from '@angular/core';
import {CxiGridColumn} from '../../../../shared/components/cxi-grid/cxi-grid-column';
import {ContentManagementRepository} from '../../../../shared/repositories/content-management.repository';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Contents} from '../../../../shared/entities/contents';

@Component({
  selector: 'app-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.scss']
})
export class ListPageComponent implements OnInit {

    cxiTableConfig = {
        paging: true,
        sorting: true,
        filtering: true,
        pagingServer: true,
        contentType: 'page',
        repository: this.contentManagementRepository,
        actions: {
            onEdit: (row: any) => {
                this.onEdit(row);
            },
            onDelete: (row: any) => {
                this.onDelete(row);
            },
        },
    };
    cxiTableColumns = CxiGridColumn.from([
        {title: 'Page Name', name: 'page_name', sort: true, className: 'pageName'},
        {title: 'Title', name: 'title', sort: true, className: 'title'},
        {title: 'Content', name: 'content', sort: true, className: 'content'},
    ]);

    constructor(private contentManagementRepository: ContentManagementRepository,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
    }


    ngOnInit() {
    }

    onEdit(content: Contents) {
        this.router.navigate(['..', content.id], {relativeTo: this.activatedRoute});
    }

    onDelete(content: Contents) {
        this.contentManagementRepository.destroy(content)
            .subscribe();
    }

}
