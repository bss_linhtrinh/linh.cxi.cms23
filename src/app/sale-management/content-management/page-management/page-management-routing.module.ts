import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListPageComponent} from './list-page/list-page.component';
import {CreatePageComponent} from './create-page/create-page.component';
import {EditPageComponent} from './edit-page/edit-page.component';


const routes: Routes = [
    {
        path: '',
        canActivate: [],
        children: [
            {
                path: 'list',
                component: ListPageComponent,
                data: {breadcrumb: 'List page'}
            },
            {
                path: 'create',
                component: CreatePageComponent,
                data: {breadcrumb: 'Create page'}
            },
            {
                path: ':id',
                component: EditPageComponent,
                data: {breadcrumb: 'Edit page'}
            },
            {path: '', redirectTo: 'list', pathMatch: 'full'},
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageManagementRoutingModule { }
