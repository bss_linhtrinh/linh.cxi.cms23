import { Component, OnInit } from '@angular/core';
import {Contents} from '../../../../shared/entities/contents';
import {ContentManagementRepository} from '../../../../shared/repositories/content-management.repository';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.scss']
})
export class CreatePageComponent implements OnInit {

    content: Contents = new Contents();
    file: File = null;
    componentAdding = {
        vi: false,
        en: false,
    };
    component: string;

    listComponent = [
        {name: 'Vietnamese', key: 'vi'},
        {name: 'English', key: 'en'},
    ];

    constructor(private contentManagementRepository: ContentManagementRepository,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
    }

    onSubmit() {
        const content = this.parseData(this.content);
        content.type = 'page';
        this.contentManagementRepository.save(content).subscribe(res => {
                this.router.navigateByUrl('/pages');
            },
            (err) => {
                this.router.navigateByUrl('/pages');
            });
    }

    addComponent() {
        const data = this.componentAdding;
        Object.keys(data).forEach((key: any) => {
            if (key === this.component) {
                data[key] = true;
            }
        });
        this.listComponent = this.listComponent.filter(item => item.key !== this.component);
        this.component = '';
    }

    deleteComponent(component) {
        switch (component) {
            case 'vietnamese':
                this.componentAdding.vi = false;
                this.listComponent.push({name: 'Vietnamese', key: 'vi'});
                break;
            case 'english':
                this.componentAdding.en = false;
                this.listComponent.push({name: 'English', key: 'en'});
                break;
        }
    }

    parseData(content) {
        if (!this.componentAdding.vi) {
            delete content.vi;
        }
        if (!this.componentAdding.en) {
            delete content.en;
        }
        return content;
    }

    onFileContent(file: any) {
        this.file = file;
    }

}
