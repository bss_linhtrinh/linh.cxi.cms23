import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MailManagementRoutingModule} from './mail-management-routing.module';
import {CreateMailComponent} from './create-mail/create-mail.component';
import {ListMailComponent} from './list-mail/list-mail.component';
import {EditMailComponent} from './edit-mail/edit-mail.component';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule} from '@angular/forms';


@NgModule({
    declarations: [CreateMailComponent, ListMailComponent, EditMailComponent],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        MailManagementRoutingModule
    ]
})
export class MailManagementModule {
}
