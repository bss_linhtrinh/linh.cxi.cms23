import { Component, OnInit } from '@angular/core';
import {Contents} from '../../../../shared/entities/contents';
import {ContentManagementRepository} from '../../../../shared/repositories/content-management.repository';
import {ActivatedRoute, Router} from '@angular/router';

declare const helper: any;


@Component({
  selector: 'app-edit-mail',
  templateUrl: './edit-mail.component.html',
  styleUrls: ['./edit-mail.component.scss']
})
export class EditMailComponent implements OnInit {

    content: Contents = new Contents();
    file: File = null;
    componentAdding = {
        vi: false,
        en: false,
    };
    component: string;

    listComponent = [
        {name: 'Vietnamese', key: 'vi'},
        {name: 'English', key: 'en'},
    ];

    constructor(private contentManagementRepository: ContentManagementRepository,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
        this.loadPage();
    }

    loadPage() {
        this.activatedRoute.params.subscribe((data) => {
            const id = data.id;
            this.contentManagementRepository.find(id).subscribe((res) => {
                this.prepareComponent(res);
                this.content = res;
            });
        });
    }

    prepareComponent(content) {
        if (content.vi && content.vi.title) {
            this.componentAdding.vi = true;
            this.listComponent = this.listComponent.filter(item => item.key !== 'vi');
        }
        if (content.en && content.en.title) {
            this.componentAdding.en = true;
            this.listComponent = this.listComponent.filter(item => item.key !== 'en');
        }
    }

    onSubmit() {
        const content = this.parseData(this.content);
        this.contentManagementRepository.save(content).subscribe(res => {
                if (res) {
                    this.router.navigateByUrl('/modules');
                    helper.showNotification(`Updated successfully!!`, 'done', 'success');
                }
            },
            err => {
                this.router.navigateByUrl('/modules');
                helper.showNotification(`Updated successfully!!`, 'done', 'success');
            });
    }

    addComponent() {
        const data = this.componentAdding;
        Object.keys(data).forEach((key: any) => {
            if (key === this.component) {
                data[key] = true;
            }
        });
        this.listComponent = this.listComponent.filter(item => item.key !== this.component);
        this.component = '';
    }

    deleteComponent(component) {
        switch (component) {
            case 'vietnamese':
                this.componentAdding.vi = false;
                this.listComponent.push({name: 'Vietnamese', key: 'vi'});
                break;
            case 'english':
                this.componentAdding.en = false;
                this.listComponent.push({name: 'English', key: 'en'});
                break;
        }
    }

    onFileContent(file: any) {
        this.file = file;
    }

    parseData(content) {
        if (!this.componentAdding.vi) {
            delete content.vi;
        }
        if (!this.componentAdding.en) {
            delete content.en;
        }
        return content;
    }
}
