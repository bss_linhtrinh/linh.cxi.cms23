import { Component, OnInit } from '@angular/core';
import {CxiGridColumn} from '../../../../shared/components/cxi-grid/cxi-grid-column';
import {ContentManagementRepository} from '../../../../shared/repositories/content-management.repository';
import {ActivatedRoute, Router} from '@angular/router';
import {Contents} from '../../../../shared/entities/contents';

@Component({
  selector: 'app-list-mail',
  templateUrl: './list-mail.component.html',
  styleUrls: ['./list-mail.component.scss']
})
export class ListMailComponent implements OnInit {

    cxiTableConfig = {
        paging: true,
        sorting: true,
        filtering: true,
        pagingServer: true,
        contentType: 'email',
        repository: this.contentManagementRepository,
        actions: {
            onEdit: (row: any) => {
                this.onEdit(row);
            },
            onDelete: (row: any) => {
                this.onDelete(row);
            },
        },
    };
    cxiTableColumns = CxiGridColumn.from([
        {title: 'Page Name', name: 'page_name', sort: true, className: 'pageName'},
        {title: 'Title', name: 'title', sort: true, className: 'title'},
        {title: 'Content', name: 'content', sort: true, className: 'content'},
    ]);

    constructor(private contentManagementRepository: ContentManagementRepository,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
    }


    ngOnInit() {
    }

    onEdit(content: Contents) {
        this.router.navigate(['..', content.id], {relativeTo: this.activatedRoute});
    }

    onDelete(content: Contents) {
        this.contentManagementRepository.destroy(content)
            .subscribe();
    }

}
