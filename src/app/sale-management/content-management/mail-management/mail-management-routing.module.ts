import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListMailComponent} from './list-mail/list-mail.component';
import {CreateMailComponent} from './create-mail/create-mail.component';
import {EditMailComponent} from './edit-mail/edit-mail.component';


const routes: Routes = [
    {
        path: '',
        canActivate: [],
        children: [
            {
                path: 'list',
                component: ListMailComponent,
                data: {breadcrumb: 'List mail'}
            },
            {
                path: 'create',
                component: CreateMailComponent,
                data: {breadcrumb: 'Create mail'}
            },
            {
                path: ':id',
                component: EditMailComponent,
                data: {breadcrumb: 'Edit mail'}
            },
            {path: '', redirectTo: 'list', pathMatch: 'full'},
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MailManagementRoutingModule { }
