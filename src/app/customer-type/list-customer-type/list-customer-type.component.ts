import { Component, OnInit } from '@angular/core';
import { CxiGridConfig } from '../../shared/components/cxi-grid/cxi-grid-config';
import { CxiGridColumn } from '../../shared/components/cxi-grid/cxi-grid-column';
import { LocalStorage } from 'ngx-webstorage';
import { CustomerTypeRepository } from 'src/app/shared/repositories/customers-type';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerType } from 'src/app/shared/entities/customer-type';

@Component({
  selector: 'app-list-customer-type',
  templateUrl: './list-customer-type.component.html',
  styleUrls: ['./list-customer-type.component.scss']
})
export class ListCustomerTypeComponent implements OnInit {
  // table
  cxiGridConfig = CxiGridConfig.from({
    paging: true,
    sorting: true,
    filtering: true,
    selecting: true,
    pagingServer: true,
    repository: this.customerTypeRepository,
    translatable: true,
    actions: {
      onEdit: (customerType: CustomerType) => {
        return this.onEdit(customerType);
      },
      onDelete: (customerType: CustomerType) => {
        return this.onDelete(customerType);
      },
      // onActivate: (addonType: AddOnType) => {
      //     this.onActivate(addonType);
      // },
      // onDeactivate: (addonType: AddOnType) => {
      //     this.onDeactivate(addonType);
      // },
    }
  });
  cxiGridColumns = CxiGridColumn.from([
    { title: 'name', name: 'name' },
    { title: 'description', name: 'description' },
  ]);

  @LocalStorage('scope.organization_id') organizationId: number;
  @LocalStorage('scope.brand_id') brandId: number;
  @LocalStorage('scope.outlet_id') outletId: number;
  constructor(
    private customerTypeRepository: CustomerTypeRepository,
    private router: Router,
    public activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
  }
  onEdit(customerType: CustomerType) {
    this.router.navigate(['..', customerType.id, 'edit'], { relativeTo: this.activatedRoute });
  }

  onDelete(customerType: CustomerType) {
    this.customerTypeRepository.destroy(customerType).subscribe();
  }
}
