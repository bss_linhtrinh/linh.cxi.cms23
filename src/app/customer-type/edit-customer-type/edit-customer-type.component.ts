import { Component, OnInit } from '@angular/core';
import { CustomerTypeRepository } from 'src/app/shared/repositories/customers-type';
import { ActivatedRoute, Router } from '@angular/router';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { CustomerType } from 'src/app/shared/entities/customer-type';
import { map, filter, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-edit-customer-type',
  templateUrl: './edit-customer-type.component.html',
  styleUrls: ['./edit-customer-type.component.scss']
})
export class EditCustomerTypeComponent implements OnInit {
  public customerType: CustomerType = new CustomerType();
  userScope: UserScope;
  public brand = [];
  tab: string = 'en';
  constructor(
    private customerTypeRepository: CustomerTypeRepository,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private scopeService: ScopeService,
    private brandRepository: BrandsRepository,
  ) { }

  ngOnInit() {
    this.loadCustomerType();
    this.loadListBrand();
  }
  loadCustomerType() {
    this.activatedRoute.params
      .pipe(
        filter(data => data.id),
        map(data => data.id),
        switchMap(
          (id) => this.customerTypeRepository.find(id)
        )
      )
      .subscribe((customerType: CustomerType) => {
        this.customerType = customerType;
        this.customerType.translations.forEach(item => {
          if (item.locale == 'en') {
            this.customerType.en = item;
          } else if (item.locale == 'vi') {
            this.customerType.vi = item;
          }
        });
      });
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.customerType.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  loadListBrand() {
    this.brandRepository.all()
      .subscribe(
        (res) => {
          this.brand = res;
          this.getListUserScope();
        }
      );
  }
  onSubmit() {
    this.customerType.translations = null;
    this.customerTypeRepository.save(this.customerType)
      .subscribe((res) => {
        this.cancel();
      });
  }
  cancel() {
    this.router.navigate(['../..', 'list'], { relativeTo: this.activatedRoute });
  }
}
