import { Component, OnInit } from '@angular/core';
import { CustomerTypeRepository } from 'src/app/shared/repositories/customers-type';
import { ActivatedRoute, Router } from '@angular/router';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { CustomerType } from 'src/app/shared/entities/customer-type';

@Component({
  selector: 'app-create-customer-type',
  templateUrl: './create-customer-type.component.html',
  styleUrls: ['./create-customer-type.component.scss']
})
export class CreateCustomerTypeComponent implements OnInit {
  public customerType: CustomerType = new CustomerType();
  userScope: UserScope;
  public brand = [];
  tab: string = 'en';
  constructor(
    private customerTypeRepository: CustomerTypeRepository,
    private router: Router,
    private route: ActivatedRoute,
    private scopeService: ScopeService,
    private brandRepository: BrandsRepository,
  ) { }

  ngOnInit() {
    this.loadListBrand();
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.customerType.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  loadListBrand() {
    this.brandRepository.all()
      .subscribe(
        (res) => {
          this.brand = res;
          this.customerType.brand_id = this.brand[0].id;
          this.getListUserScope();
        }
      );
  }
  onSubmit() {
    this.customerType.translations = null;
    this.customerTypeRepository.save(this.customerType)
      .subscribe(
        (res) => {
          this.router.navigate(['../list'], { relativeTo: this.route });
        }
      );
  }
}
