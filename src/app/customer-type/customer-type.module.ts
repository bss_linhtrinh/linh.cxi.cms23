import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerTypeRoutingModule } from './customer-type-routing.module';
import { ListCustomerTypeComponent } from './list-customer-type/list-customer-type.component';
import { CreateCustomerTypeComponent } from './create-customer-type/create-customer-type.component';
import { EditCustomerTypeComponent } from './edit-customer-type/edit-customer-type.component';
import {SharedModule} from 'src/app/shared/shared.module';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [ListCustomerTypeComponent, CreateCustomerTypeComponent, EditCustomerTypeComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    CustomerTypeRoutingModule
  ]
})
export class CustomerTypeModule { }
