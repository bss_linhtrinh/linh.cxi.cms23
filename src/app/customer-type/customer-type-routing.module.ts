import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListCustomerTypeComponent } from './list-customer-type/list-customer-type.component';
import { CreateCustomerTypeComponent } from './create-customer-type/create-customer-type.component';
import { EditCustomerTypeComponent } from './edit-customer-type/edit-customer-type.component';
import {CheckPermissionsGuard} from '../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [CheckPermissionsGuard],
    children: [
      { path: 'list', component: ListCustomerTypeComponent, data: { breadcrumb: 'List customer type' } },
      { path: 'create', component: CreateCustomerTypeComponent, data: { breadcrumb: 'Create new customer type' } },
      { path: ':id/edit', component: EditCustomerTypeComponent, data: { breadcrumb: 'Edit customer type' } },
      { path: '', redirectTo: 'list', pathMatch: 'full' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerTypeRoutingModule { }
