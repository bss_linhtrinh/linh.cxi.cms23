import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

export enum DeactivateUserSelection {
    Temporary = 'temporary',
    Private = 'private',
    Other = 'other',
}


@Component({
    selector: 'app-deactivate-user',
    templateUrl: './deactivate-user.component.html',
    styleUrls: ['./deactivate-user.component.scss']
})
export class DeactivateUserComponent implements OnInit {

    deactivateUser: {
        selection: DeactivateUserSelection;
        period: number;
        reason: any;
    };


    constructor(private ngbActiveModal: NgbActiveModal) {
    }

    ngOnInit() {
        this.deactivateUser = {
            selection: DeactivateUserSelection.Temporary,
            period: 1,
            reason: null,
        };
    }

    no() {
        this.ngbActiveModal.close(false);
    }

    yes() {
        this.ngbActiveModal.close(this.deactivateUser);
    }

    cancel() {
        this.ngbActiveModal.dismiss('cancel');
    }
}
