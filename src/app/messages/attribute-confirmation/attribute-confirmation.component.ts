import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Attribute} from '../../shared/entities/attribute';

@Component({
    selector: 'app-attribute-confirmation',
    templateUrl: './attribute-confirmation.component.html',
    styleUrls: ['./attribute-confirmation.component.scss']
})
export class AttributeConfirmationComponent implements OnInit {
    data: any;

    @Input() btnYes = 'Save';
    @Input() btnNo = 'No';
    @Input() items: Attribute[] = [];

    constructor(private ngbActiveModal: NgbActiveModal) {
    }

    ngOnInit() {
    }

    no() {
        this.ngbActiveModal.close(false);
    }

    yes() {
        this.ngbActiveModal.close(this.data);
    }

    cancel() {
        this.ngbActiveModal.dismiss('cancel');
    }

}
