import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttributeConfirmationComponent } from './attribute-confirmation.component';

describe('AttributeConfirmationComponent', () => {
  let component: AttributeConfirmationComponent;
  let fixture: ComponentFixture<AttributeConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttributeConfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttributeConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
