import {Injectable} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {from, Observable, of, throwError} from 'rxjs';
import {DiscardChangeComponent} from './discard-change/discard-change.component';
import {catchError, filter, map, switchMap} from 'rxjs/operators';
import {UnsavedChangesComponent} from './unsaved-changes/unsaved-changes.component';
import {StatusConfirmationComponent} from './status-confirmation/status-confirmation.component';
import {DeactivateUserComponent, DeactivateUserSelection} from './deactivate-user/deactivate-user.component';
import {AttributeConfirmationComponent} from './attribute-confirmation/attribute-confirmation.component';
import {Attribute} from '../shared/entities/attribute';
import {Address} from '../shared/entities/address';
import {CxiSelectAddressComponent} from './cxi-select-address/cxi-select-address.component';
import {CxiSelectItemsComponent} from './cxi-select-items/cxi-select-items.component';
import * as _ from 'lodash';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
    providedIn: 'root'
})
export class MessagesService {

    constructor(
        private ngbModal: NgbModal,
        private translateService: TranslateService
    ) {
    }

    // Thái
    statusConfirmation(message?: string | { message: string, params: any }, options?: any): Observable<any> {
        const modalRef = this.ngbModal.open(StatusConfirmationComponent, {centered: true, windowClass: 'status-confirmation'});
        if (message) {
            modalRef.componentInstance.message = message;
        }
        if (options) {
            modalRef.componentInstance.options = options;
        }
        return from(modalRef.result)
            .pipe(
                catchError((e) => throwError(e)),
                filter((isConfirmed: boolean) => isConfirmed)
            );
    }

    showConfirmationMessage$(message: string, action: string, object: string): Observable<string> {
        object = _.capitalize(object);
        const msg = {
            message: message,
            params: {
                action: action,
                object: object
            }
        };
        const confirm = of(msg)
            .pipe(
                switchMap((messages: any) => {
                    return this.translatedResource$(object)
                        .pipe(
                            map(
                                (translatedResource: string) => {
                                    messages.params.object = translatedResource;
                                    return messages;
                                }
                            )
                        );
                }),
                switchMap((messages: any) => {
                    return this.translatedAction$(action)
                        .pipe(
                            map(
                                (translatedAction: string) => {
                                    messages.params.action = translatedAction;
                                    return messages;
                                }
                            )
                        );
                }),
                switchMap(
                    (messages: any) => this.statusConfirmation(messages)
                )
            );
        return confirm;
    }

    translatedResource$(name: string): Observable<string> {
        return this.translateService.get(`${name}.${name}`)
            .pipe(
                map((translatedResource: string) => translatedResource.charAt(0).toLocaleLowerCase() + translatedResource.slice(1)),
            );
    }

    translatedAction$(name: string): Observable<string> {
        return this.translateService.get(`actions.${name}`);
    }

    unsavedChanges(): Observable<any> {
        const modalRef = this.ngbModal.open(UnsavedChangesComponent, {centered: true, windowClass: 'unsaved-changes'});
        return from(modalRef.result)
            .pipe(catchError(() => of(false)));
    }

    discardChanges(): Observable<any> {
        const modalRef = this.ngbModal.open(DiscardChangeComponent, {centered: true});
        return from(modalRef.result)
            .pipe(catchError(() => of(false)));
    }

    deactivateUser(): Observable<any> {
        const modalRef = this.ngbModal.open(DeactivateUserComponent, {centered: true, windowClass: 'deactivate-user'});
        return from(modalRef.result)
            .pipe(
                switchMap(
                    (deactivateUser: any) => {
                        if (!deactivateUser) {
                            return of(false);
                        }

                        if (deactivateUser.selection === DeactivateUserSelection.Temporary) {
                            return this.statusConfirmation(`User will be deactivated based on selected day`);
                        } else if (deactivateUser.selection === DeactivateUserSelection.Private ||
                            deactivateUser.selection === DeactivateUserSelection.Other) {
                            return this.statusConfirmation(`User will be active till the Admin reactivate the account`);
                        }

                        return of(false);
                    }
                ),
                filter((isConfirmed: boolean) => isConfirmed)
            );
    }


    attributeConfirmation(items: Attribute[] = [], data?: any): Observable<any> {
        const modalRef = this.ngbModal.open(AttributeConfirmationComponent, {centered: true, windowClass: 'cxi-select-item-from-list'});
        if (items) {
            modalRef.componentInstance.items = items;
        }
        if (data) {
            modalRef.componentInstance.data = data;
        }

        return from(modalRef.result);
    }

    selectItemFromList(options?: any): Observable<any> {
        const modalRef = this.ngbModal.open(CxiSelectItemsComponent, {windowClass: 'cxi-select-item-from-list'});
        if (options) {
            if (options.repository) {
                modalRef.componentInstance.repository = options.repository;
            }
            if (options.columns) {
                modalRef.componentInstance.columns = options.columns;
            }
        }
        return from(modalRef.result)
            .pipe(
                catchError(() => of(null)),
            );
    }

    selectAddresses(addresses: Address[]): Observable<any> {
        const modalRef = this.ngbModal.open(CxiSelectAddressComponent, {windowClass: 'cxi-select-addresses'});
        modalRef.componentInstance.addresses = addresses;
        return from(modalRef.result)
            .pipe(
                catchError(() => of(null)),
            );
    }


}
