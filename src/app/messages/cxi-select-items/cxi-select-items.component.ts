import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {CxiGridColumn} from '../../shared/components/cxi-grid/cxi-grid-column';
import {CxiGridConfig} from '../../shared/components/cxi-grid/cxi-grid-config';

@Component({
    selector: 'app-cxi-select-items',
    templateUrl: './cxi-select-items.component.html',
    styleUrls: ['./cxi-select-items.component.scss']
})
export class CxiSelectItemsComponent implements OnInit, OnChanges {
    @Input() name: string;
    @Input() repository: any;
    @Input() columns: any[];
    searchStr: any = '';

    cxiGridConfig: CxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: false,
        filtering: false,
        selecting: false,
        pagingServer: true,
        repository: this.repository
    });
    cxiColumns: CxiGridColumn[];

    className: string;

    constructor(
        private ngbActiveModal: NgbActiveModal
    ) {
    }

    ngOnInit() {
        this.initConfigCxiGrid();
        // this.getClass();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.columns &&
            (
                changes.columns.previousValue !== changes.columns.currentValue ||
                changes.repository.previousValue !== changes.repository.currentValue
            )
        ) {
            this.initConfigCxiGrid();
            // this.getClass();
        }
    }

    initConfigCxiGrid() {
        const columns = [
            {title: 'name', name: 'name', avatar: 'avatar'},
            {title: 'email', name: 'email'},
            {title: 'address', name: 'address'},
            {
                title: '',
                name: '',
                action: {
                    label: 'Select',
                }
            },
        ];


        if (this.columns) {
            this.cxiColumns = CxiGridColumn.from(this.columns);
        } else {
            this.cxiColumns = CxiGridColumn.from(columns);
        }


        if (this.repository) {
            this.cxiGridConfig.repository = this.repository;
        }
        this.cxiGridConfig = CxiGridConfig.from(this.cxiGridConfig);
    }

    no() {
        this.ngbActiveModal.close(false);
    }

    yes(row: any) {
        this.ngbActiveModal.close(row);
    }

    cancel() {
        this.ngbActiveModal.dismiss('cancel');
    }

    isEmptyClass() {
        return this.searchStr.lenght === 0;
    }

    onChangeSearch() {

    }

    // getClass() {
    //     if (this.items && this.items.length > 0) {
    //         const instance = this.items[0];
    //         if (instance) {
    //             this.className = instance.constructor.name;
    //         }
    //     }
    // }
}
