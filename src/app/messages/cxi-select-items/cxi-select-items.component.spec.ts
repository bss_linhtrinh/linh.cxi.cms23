import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {CxiSelectItemsComponent} from './cxi-select-items.component';

describe('CxiSelectItemsComponent', () => {
  let component: CxiSelectItemsComponent;
  let fixture: ComponentFixture<CxiSelectItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiSelectItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiSelectItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
