import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-unsaved-changes',
    templateUrl: './unsaved-changes.component.html',
    styleUrls: ['./unsaved-changes.component.scss']
})
export class UnsavedChangesComponent implements OnInit {

    @Input() btnYes = 'Yes';
    @Input() btnNo = 'No';
    
    constructor(private ngbActiveModal: NgbActiveModal) {
    }

    ngOnInit() {
    }

    yes() {
        this.ngbActiveModal.close(true);
    }

    no() {
        this.ngbActiveModal.close(false);
    }

    cancel() {
        this.ngbActiveModal.close(false);
    }
}
