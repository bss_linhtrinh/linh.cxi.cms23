import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscardChangeComponent } from './discard-change.component';

describe('DiscardChangeComponent', () => {
  let component: DiscardChangeComponent;
  let fixture: ComponentFixture<DiscardChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscardChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscardChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
