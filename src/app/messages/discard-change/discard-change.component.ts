import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-discard-change',
    templateUrl: './discard-change.component.html',
    styleUrls: ['./discard-change.component.scss']
})
export class DiscardChangeComponent implements OnInit {

    constructor(private ngbActiveModal: NgbActiveModal) {
    }

    ngOnInit() {
    }

    ok() {
        this.ngbActiveModal.close(true);
    }

    cancel() {
        this.ngbActiveModal.close(false);
    }
}
