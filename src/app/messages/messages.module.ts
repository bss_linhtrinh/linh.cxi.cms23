import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StatusConfirmationComponent} from './status-confirmation/status-confirmation.component';
import {DiscardChangeComponent} from './discard-change/discard-change.component';
import {UnsavedChangesComponent} from './unsaved-changes/unsaved-changes.component';
import {DeactivateUserComponent} from './deactivate-user/deactivate-user.component';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {AttributeConfirmationComponent} from './attribute-confirmation/attribute-confirmation.component';
import { CxiSelectAddressComponent } from './cxi-select-address/cxi-select-address.component';
import {CxiSelectItemsComponent} from './cxi-select-items/cxi-select-items.component';

const messageComponents = [
    StatusConfirmationComponent,
    DiscardChangeComponent,
    UnsavedChangesComponent,
    DeactivateUserComponent,
    AttributeConfirmationComponent,
    CxiSelectItemsComponent,
    CxiSelectAddressComponent
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
    ],
    declarations: [
        ...messageComponents,
    ],
    entryComponents: [
        ...messageComponents
    ],
    exports: [
        ...messageComponents
    ]
})
export class MessagesModule {
}
