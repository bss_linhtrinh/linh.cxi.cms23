import {Component, OnInit, Input} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-status-confirmation',
    templateUrl: './status-confirmation.component.html',
    styleUrls: ['./status-confirmation.component.scss']
})
export class StatusConfirmationComponent implements OnInit {
    @Input() title: string;
    @Input() message: string | { message: string, params: any } = 'Are you sure you want to do something?';

    @Input() options: {
        buttons: { label: string, action?: () => void }[]
    } = null;
    @Input() btnYes = 'Yes';
    @Input() btnNo = 'No';

    constructor(
        private ngbActiveModal: NgbActiveModal,
        private translateService: TranslateService
    ) {
    }

    ngOnInit() {
    }

    no() {
        this.ngbActiveModal.close(false);
    }

    yes() {
        this.ngbActiveModal.close(true);
    }

    cancel() {
        this.dismiss();
    }

    buttonHasCallback(button: { label: string, callback: () => void }) {
        return button.callback && typeof button.callback === 'function';
    }

    dismiss() {
        this.ngbActiveModal.dismiss('cancel');
    }

    translateMessage(): Observable<string> {
        let key: string;
        let params: any;

        switch (typeof this.message) {
            case 'string':
            default:
                key = ('Messages.' + this.message);
                break;

            case 'object':
                key = ('Messages.' + this.message.message);
                params = this.message.params;
                break;
        }

        return this.translateService.get(key, params);
    }
}
