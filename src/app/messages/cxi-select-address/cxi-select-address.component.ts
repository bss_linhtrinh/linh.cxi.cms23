import {Component, Input, OnInit} from '@angular/core';
import {CxiGridConfig} from '../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../shared/components/cxi-grid/cxi-grid-column';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Address} from '../../shared/entities/address';

@Component({
  selector: 'app-cxi-select-address',
  templateUrl: './cxi-select-address.component.html',
  styleUrls: ['./cxi-select-address.component.scss']
})
export class CxiSelectAddressComponent implements OnInit {

    @Input() name: string;
    @Input() addresses: Address[];
    searchStr: any = '';

    cxiTableConfig = CxiGridConfig.from({
        paging: true,
        sorting: false,
        filtering: false,
        selecting: false,
    });
    cxiColumns = CxiGridColumn.from([
        {title: 'address', name: 'address1'},
        {
            title: '',
            name: '',
            action: {
                label: 'Select',
            }
        },
    ]);

  constructor(private ngbActiveModal: NgbActiveModal) { }

  ngOnInit() {
  }
    no() {
        this.ngbActiveModal.close(false);
    }

    yes(row: any) {
        this.ngbActiveModal.close(row);
    }

    cancel() {
        this.ngbActiveModal.dismiss('cancel');
    }

    isEmptyClass() {
        return this.searchStr.lenght === 0;
    }

    onChangeSearch() {

    }
}
