import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiSelectAddressComponent } from './cxi-select-address.component';

describe('CxiSelectAddressComponent', () => {
  let component: CxiSelectAddressComponent;
  let fixture: ComponentFixture<CxiSelectAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiSelectAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiSelectAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
