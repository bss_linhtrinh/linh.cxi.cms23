import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import { FormsModule } from '@angular/forms';
import {SpecialistRoutingModule} from './specialist-routing.module';
import {SharedModule} from '../shared/shared.module';
import {ListSpecialistComponent} from './list-specialist/list-specialist.component';
import {CreateSpecialistComponent} from './create-specialist/create-specialist.component';
import {EditSpecialistComponent} from './edit-specialist/edit-specialist.component';

@NgModule({
    declarations: [
        ListSpecialistComponent,
        CreateSpecialistComponent,
        EditSpecialistComponent
    ],
    imports: [
        SpecialistRoutingModule,
        CommonModule,
        SharedModule,
        FormsModule,
    ]
})
export class SpecialistModule {
}
