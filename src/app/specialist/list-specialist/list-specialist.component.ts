import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorage } from 'ngx-webstorage';
import {CxiGridConfig} from '../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../shared/components/cxi-grid/cxi-grid-column';
import {ColumnFormat} from '../../shared/types/column-format';
import {Specialist} from '../../shared/entities/specialist';
import {SpecialistRepository} from '../../shared/repositories/specialist.repository';

@Component({
    selector: 'app-list-specialist',
    templateUrl: './list-specialist.component.html',
    styleUrls: ['./list-specialist.component.scss']
})
export class ListSpecialistComponent implements OnInit {

    public specialists: Specialist[] = [];
    tab: string = 'en';
    // table
    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        pagingServer: true,
        repository: this.specialistRepository,
        translatable: true,
        actions: {
            onEdit: (specialist: Specialist) => {
                return this.onEdit(specialist);
            },
            onDelete: (specialist: Specialist) => {
                return this.onDelete(specialist);
            },
            onActivate: (specialist: Specialist) => {
                this.onActivate(specialist);
            },
            onDeactivate: (specialist: Specialist) => {
                this.onDeactivate(specialist);
            },
        }
    });

    cxiGridColumns = CxiGridColumn.from([
        { title: 'name', name: 'name', avatar: 'images' },
        { title: 'description', name: 'description' },
    ]);

    @LocalStorage('scope.organization_id') organizationId: number;
    @LocalStorage('scope.brand_id') brandId: number;
    @LocalStorage('scope.outlet_id') outletId: number;

    constructor(
        private specialistRepository: SpecialistRepository,
        private router: Router,
        public activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit() {
    }

    onEdit(specialist: Specialist) {
        this.router.navigate(['..', specialist.id, 'edit'], { relativeTo: this.activatedRoute });
    }

    onDelete(specialist: Specialist) {
        this.specialistRepository.destroy(specialist).subscribe();
    }

    onActivate(specialist: Specialist) {
        this.specialistRepository.activate(specialist).subscribe();
    }

    onDeactivate(specialist: Specialist) {
        this.specialistRepository.deactivate(specialist).subscribe();
    }
}
