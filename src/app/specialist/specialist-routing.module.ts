import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CheckPermissionsGuard} from '../shared/guards/check-permissions/check-permissions.guard';
import {ListSpecialistComponent} from './list-specialist/list-specialist.component';
import {CreateSpecialistComponent} from './create-specialist/create-specialist.component';
import {EditSpecialistComponent} from './edit-specialist/edit-specialist.component';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: '', redirectTo: 'list', pathMatch: 'full'},
            {path: 'list', component: ListSpecialistComponent, data: {breadcrumb: 'List specialist'}},
            {path: 'create', component: CreateSpecialistComponent, data: {breadcrumb: 'Create specialist'}},
            {path: ':id/edit', component: EditSpecialistComponent, data: {breadcrumb: 'Edit specialist'}},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SpecialistRoutingModule {
}
