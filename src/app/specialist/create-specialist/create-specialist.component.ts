import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilitiesService } from 'src/app/shared/services/utilities/utilities.service';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { Status } from 'src/app/shared/entities/status';
import { environment } from 'src/environments/environment';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { UserScope } from 'src/app/shared/entities/user-scope';
import {SpecialistRepository} from '../../shared/repositories/specialist.repository';
import {Specialist} from '../../shared/entities/specialist';

declare const helper: any;

@Component({
    selector: 'app-create-specialist',
    templateUrl: './create-specialist.component.html',
    styleUrls: ['./create-specialist.component.scss']
})

export class CreateSpecialistComponent implements OnInit {
    public specialist: Specialist;
    public specialists: Specialist[] = [];
    public status: Status[] = Status.init();
    public isShow: boolean = false;
    public brand = [];

    tab: string = 'en';

    userScope: UserScope;
    currentUser: any;

    selectedFile: File = null;

    constructor(
        public specialistRepository: SpecialistRepository,
        public router: Router,
        public activatedRoute: ActivatedRoute,
        public utilitiesService: UtilitiesService,
        private authService: AuthService,
        private brandRepository: BrandsRepository,
        private scopeService: ScopeService,
    ) {
        this.currentUser = this.authService.currentUser();
    }

    ngOnInit() {
        this.specialist = new Specialist();
        this.getSpecialist();
        this.loadListBrand();
    }
    getListUserScope() {
        this.scopeService.getUserScope$()
            .subscribe(
                (res: any) => {
                    this.userScope = res;
                    if (this.userScope.brand) {
                        this.specialist.brand_id = this.userScope.brand.id;
                    }
                }
            );
    }
    loadListBrand() {
        this.brandRepository.all()
            .subscribe(
                (res) => {
                    this.brand = res;
                    this.specialist.brand_id = this.brand[0].id;
                    this.getListUserScope();
                }
            );
    }
    showSpecialist() {
        this.isShow = !this.isShow;
        if (this.isShow === false) {
            // this.specialist.parent_id = 0;
        } else {
            // this.specialist.parent_id = this.specialists[0].id;
        }
    }
    getSpecialist() {
        this.specialistRepository.all({ pagination: 0 })
            .subscribe(
                (specialists: Specialist[]) => {
                    this.specialists = specialists;
                }
            );
    }
    onFileChanged(selectedFile: File) {
        this.selectedFile = selectedFile;
    }
    onSubmit() {
        this.specialist.translations = null;
        if (this.specialist.name != null) {
            this.specialist.slug = this.utilitiesService.ChangeToSlug(this.specialist.name);
        }

        this.specialistRepository.save(this.specialist).subscribe(
            (res) => {
                console.log(res);
                this.router.navigateByUrl('/specialist');
                helper.showNotification(`${this.specialist.name} has been added successfully!!`, 'done', 'success');
            },
            (error) => {
                console.log(error);
            });
    }
}
