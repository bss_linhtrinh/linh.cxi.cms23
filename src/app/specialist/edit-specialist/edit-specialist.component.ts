import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Mode } from 'src/app/shared/entities/display_mode';
import { UtilitiesService } from 'src/app/shared/services/utilities/utilities.service';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { FilterStorageService } from 'src/app/shared/services/filterStorage/filterStorage.service';
import { Status } from 'src/app/shared/entities/status';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';

import {SpecialistRepository} from '../../shared/repositories/specialist.repository';
import {Specialist} from '../../shared/entities/specialist';

declare const helper: any;

@Component({
  selector: 'app-edit-specialist',
  templateUrl: './edit-specialist.component.html',
  styleUrls: ['./edit-specialist.component.scss']
})

export class EditSpecialistComponent implements OnInit {
  public specialist: Specialist;
  public specialists: Specialist[] = [];
  public mode: Mode[] = Mode.init();
  public status: Status[] = Status.init();
  public isShow: boolean = false;
  public brand = [];
  userScope: UserScope;
  currentUser: any;
  selectedFile: File = null;
  tab: string = 'en';
  public translations = [
    {
      name: null,
      description: null,
      locale: 'en'
    },
    {
      name: null,
      description: null,
      locale: 'vi'
    }
  ];

  constructor(
    public specialistRepository: SpecialistRepository,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public utilitiesService: UtilitiesService,
    private authService: AuthService,
    private filterStorage: FilterStorageService,
    private brandRepository: BrandsRepository,
    private scopeService: ScopeService,
  ) {
    this.currentUser = this.authService.currentUser();
  }

  ngOnInit() {
    this.specialist = new Specialist();
    this.loadEditSpecialists();
    this.getSpecialist();
    this.loadListBrand();
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.specialist.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  loadListBrand() {
    this.brandRepository.all()
      .subscribe(
        (res) => {
          this.brand = res;
          this.getListUserScope();
        }
      );
  }
  onFileChanged(selectedFile: File) {
    this.selectedFile = selectedFile;
  }
  getSpecialist() {
    this.specialistRepository.all({ pagination: 0 })
      .subscribe(
        (specialists: Specialist[]) => {
          this.specialists = specialists;
        }
      );
  }
  loadEditSpecialists() {
    this.activatedRoute.params.subscribe((data) => {
      const id = data.id;
      this.specialistRepository.find(id).subscribe((res) => {
        this.specialist = res;
        this.specialist.translations.forEach(item => {
          if (item.locale) {
            if (item.locale.toLowerCase() === 'en') {
              this.specialist.en = item;
            } else if (item.locale.toLowerCase() === 'vi') {
              this.specialist.vi = item;
            }
          }
        });
      });
    });
  }
  onSubmit() {
    if (this.specialist.name != null) {
      this.specialist.slug = this.utilitiesService.ChangeToSlug(this.specialist.name);
    }
    this.specialist.translations = null;
    this.specialistRepository.save(this.specialist).subscribe((res) => {
      this.router.navigateByUrl('/specialist');
      helper.showNotification(`${this.specialist.name} updated successfully!!`, 'done', 'success');
    });
  }
  onCancel() {
    this.router.navigate(['../..', 'list'], { relativeTo: this.activatedRoute });
  }
}
