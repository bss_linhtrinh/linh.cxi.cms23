import {Component, Injectable, OnDestroy, OnInit} from '@angular/core';
import {NavigationCancel, NavigationEnd, NavigationStart, Router} from '@angular/router';
import {NgbDateAdapter, NgbDateParserFormatter, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgxSpinnerService} from 'ngx-spinner';
import {ApiService} from './shared/services/api/api.service';
import {NEVER, of, Subscription, timer} from 'rxjs';
import {SidebarService} from './cxi/sidebar.service';
import {PusherService} from './shared/services/pusher/pusher.service';
import {NotificationOrder} from './shared/entities/notification-order';
import {debounceTime, delay, distinctUntilChanged, filter, map, switchMap, tap} from 'rxjs/operators';
import {fromArray} from 'rxjs/internal/observable/fromArray';
import {NotificationService} from './shared/services/noitification/notification.service';
import {UserScope} from './shared/entities/user-scope';
import {ScopeService} from './shared/services/scope/scope.service';
import {TranslateService} from '@ngx-translate/core';
import {LanguageService} from './shared/services/language/language.service';
import {AuthService} from './shared/services/auth/auth.service';
import {LocalStorage} from 'ngx-webstorage';
import {Provider} from './shared/types/providers';

/**
 * Example of a Native Date adapter
 */
@Injectable()
export class BaseDateNativeAdapter extends NgbDateAdapter<string> {

    fromModel(value: string): NgbDateStruct {
        const objDate = new Date(value);
        const model = value
            ? (objDate && objDate.getFullYear)
                ? {
                    year: objDate.getFullYear(),
                    month: objDate.getMonth() + 1,
                    day: objDate.getDate()
                }
                : null
            : null;

        return model;
    }

    toModel(date: NgbDateStruct): string {
        const objDate = date
            ? new Date(date.year, date.month - 1, date.day)
            : null;
        const formattedDate = objDate
            ? (objDate.getFullYear() + '-' +
                (
                    objDate.getMonth() + 1 < 10
                        ? '0' + (objDate.getMonth() + 1)
                        : objDate.getMonth() + 1
                )
                + '-'
                + (
                    objDate.getDate() < 10
                        ? '0' + objDate.getDate()
                        : objDate.getDate()
                )
            )
            : null;
        return formattedDate;
    }
}

@Injectable()
export class BaseParserFormatter extends NgbDateParserFormatter {
    parse(value: string): NgbDateStruct {
        const objDate = new Date(value);
        const model = value
            ? (objDate && objDate.getFullYear)
                ? {
                    year: objDate.getFullYear(),
                    month: objDate.getMonth() + 1,
                    day: objDate.getDate()
                }
                : null
            : null;
        return model;
    }

    format(date: NgbDateStruct): string {
        const options = {year: 'numeric', month: 'short', day: '2-digit'};
        const objDate = date
            ? new Date(date.year, date.month - 1, date.day)
            : null;
        const formatDate = objDate
            ? objDate.toLocaleDateString('en-GB', options)
            : null;
        return formatDate;
    }
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, OnDestroy {
    title = 'bss.cxi.cms';
    subscriptions: Subscription[] = [];
    currentLanguage: string;

    @LocalStorage('auth.provider') provider: Provider;
    @LocalStorage('settings.locale') locale: string;

    constructor(
        private router: Router,
        private spinner: NgxSpinnerService,
        private apiService: ApiService,
        private notificationService: NotificationService,
        private sidebarService: SidebarService,
        private pusherService: PusherService,
        private scopeService: ScopeService,
        private translate: TranslateService,
        private languageService: LanguageService,
        private authService: AuthService
    ) {

        // this language will be used as a fallback when a translation isn't found in the current language
        translate.setDefaultLang('en');

    }

    ngOnInit() {
        this.subscribes();

        // language
        this.getCurrentLanguage();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());

        // pusher unsubscribe
        this.unsubscribePusher();
    }

    subscribes() {
        this.subscribeLoadingAPi();
        this.subscribeClientErrors();
        this.subscribePusher();
        this.subscribeEventsNavigation();
        this.subscribeChangedLanguage();
    }

    subscribeChangedLanguage() {
        this.languageService.changedLanguage$
            .pipe(
                distinctUntilChanged(),
                debounceTime(100),
                tap((lang) => this.translate.use(lang))
            )
            .subscribe();

        this.authService.isAuthorised$
            .pipe(
                filter((isAuthorised: boolean) => isAuthorised),
                tap(() => this.getCurrentLanguage())
            )
            .subscribe();
    }

    getCurrentLanguage() {
        // the lang to use, if the lang isn't available, it will use the current loader to get them
        this.translate.use(this.languageService.getCurrentLang());

        if (this.provider === Provider.Admin) {
            this.languageService.get()
                .pipe(
                    tap(
                        (language: string) => {
                            if (language !== this.currentLanguage) {
                                this.currentLanguage = language;
                                this.languageService.setCurrentLang(language);
                                this.languageService.changeLanguage(language);
                            }

                            // locale
                            this.locale = this.currentLanguage;
                        }
                    )
                )
                .subscribe(
                );
        }
    }

    subscribeEventsNavigation() {
        const sub1 = this.router.events
            .subscribe((event) => {
                // Loading
                if (event instanceof NavigationStart) {
                    // this.loading = true;
                } else if (
                    event instanceof NavigationEnd
                ) {

                    // show/hide sidebar
                    if (event.url.includes('/filter') ||
                        event.urlAfterRedirects.includes('/filter')) {
                        timer(1).subscribe(() => this.sidebarService.hide());
                    } else {
                        timer(1).subscribe(() => this.sidebarService.show());
                    }
                }

                // Handle Navigation Cancel
                if (event instanceof NavigationCancel) {
                    switch (event.reason) {
                        case 'not_permission':
                            timer(1000)
                                .subscribe(
                                    () => this.router.navigate(['/'])
                                );
                            break;
                        case 'domain_invalid':
                            timer(1000)
                                .subscribe(
                                    () => {
                                        this.router.navigate(['/errors/domain-invalid']);
                                    }
                                );

                            break;
                        case 'unauthorized':
                        default:
                            break;
                    }
                }
            });
        this.subscriptions.push(sub1);
    }

    subscribeLoadingAPi() {
        const spinnerHandle = this.apiService.apiLoading$
            .pipe(
                delay(10),
                tap(
                    (isLoading: number) => {
                        if (isLoading > 0) {
                            this.spinner.show();
                        } else {
                            this.spinner.hide();
                        }
                    }
                ),
            );
        const sub = spinnerHandle.subscribe();
        this.subscriptions.push(sub);
    }

    subscribeClientErrors() {
        const sub = this.apiService.clientErrors$
            .pipe(
                // error http
                filter(e => e.status && e.error),
                switchMap(
                    e => {
                        if ([400, 402, 403, 404, 422, 429].indexOf(e.status) !== -1) {
                            return this.clientErrorsBadRequest$(e);
                        } else if ([401].indexOf(e.status) !== -1) {
                            return this.clientErrorsUnauthorized$(e);
                        } else {
                            return NEVER;
                        }
                    }
                ),
            )
            .subscribe();

        this.subscriptions.push(sub);
    }

    clientErrorsUnauthorized$(errorData: any) {
        return of(errorData)
            .pipe(
                // error body
                map(errData => errData.error),
                map((errBody: any) => errBody.errors),
                tap((errors: any[]) => {
                    let errorMessage = '';
                    if (errors && errors.length > 0) {
                        errors.forEach(e => {
                            const errMsg = this.getErrorMessage(e);
                            if (typeof e === 'string') {
                                errorMessage += `The error is malformed: ${errMsg}`;
                            } else if (errMsg) {
                                errorMessage += `<p>${errMsg}</p>`;
                            }
                        });
                    }
                    if (errorMessage) {
                        this.notificationService.error(`${errorMessage}`, `Unauthorised`, {translate: false});
                        this.router.navigateByUrl(`/auth/login`);
                    }
                }),
                switchMap((errors: any[]) => fromArray(errors)),
            );
    }

    clientErrorsBadRequest$(errorData: any) {
        return of(errorData)
            .pipe(
                // error body
                map(errData => errData.error),
                map((errBody: any) => errBody.errors),
                tap((errors: any[]) => {
                    let errorMessage = '';
                    if (errors && errors.length > 0) {
                        errors.forEach(e => {
                            const errMsg = this.getErrorMessage(e);
                            if (typeof e === 'string') {
                                errorMessage += `The error is malformed: ${errMsg}`;
                            } else if (errMsg) {
                                errorMessage += `<p>${errMsg}</p>`;
                            }
                        });
                    }
                    this.notificationService.error(`${errorMessage}`, null, {translate: false});
                }),
                switchMap((errors: any[]) => fromArray(errors)),
            );
    }

    getErrorMessage(e) {
        return e
            ? (
                e.message
                    ? e.message
                    : (
                        e.errorMessage
                            ? e.errorMessage
                            : null
                    )
            )
            : null;
    }

    subscribePusher() {
        this.scopeService.getUserScope$()
            .subscribe(
                (userScope: UserScope) => {
                    if (!userScope) {
                        return;
                    }

                    let userLevel: string;
                    let userLevelId: number;


                    if (userScope.outlet && userScope.outlet.id) {
                        userLevel = 'outlet';
                        userLevelId = userScope.outlet.id;
                    } else if (userScope.brand && userScope.brand.id) {
                        userLevel = 'brand';
                        userLevelId = userScope.brand.id;
                    } else if (userScope.organization && userScope.organization.id) {
                        userLevel = 'organization';
                        userLevelId = userScope.organization.id;
                    }


                    const channelName = `order.on.${userLevel}.${userLevelId}`;
                    const eventsName = [
                        `orders.created`,
                        `orders.updated`,
                    ];

                    const channel = this.pusherService.subscribe(channelName);

                    eventsName.forEach(
                        eventName => {
                            channel
                                .bind(eventName, (data: NotificationOrder) => {
                                    this.showToast(data);
                                });
                        }
                    );
                }
            );
    }

    unsubscribePusher() {
        this.scopeService.getUserScope$()
            .subscribe(
                (userScope: UserScope) => {

                    let userLevel: string;
                    let userLevelId: number;

                    if (userScope.outlet && userScope.outlet.id) {
                        userLevel = 'outlet';
                        userLevelId = userScope.outlet.id;
                    } else if (userScope.brand && userScope.brand.id) {
                        userLevel = 'brand';
                        userLevelId = userScope.brand.id;
                    } else if (userScope.organization && userScope.organization.id) {
                        userLevel = 'organization';
                        userLevelId = userScope.organization.id;
                    }

                    const channelName = `order.on.${userLevel}.${userLevelId}`;
                    this.pusherService.unsubscribe(channelName);
                }
            );
    }

    showToast(data: NotificationOrder) {
        const options = {
            opTap: () => {
                this.router.navigate(['/orders']);
                this.toasterClickedHandler();
            }
        };
        this.notificationService.success([data.message], null, options);
    }

    toasterClickedHandler() {
        // console.log('Toastr clicked');
    }
}
