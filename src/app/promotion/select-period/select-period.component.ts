import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { AmazingTimePickerService } from 'amazing-time-picker';

@Component({
  selector: 'app-select-period',
  templateUrl: './select-period.component.html',
  styleUrls: ['./select-period.component.scss']
})
export class SelectPeriodComponent implements OnInit, OnChanges {

  weekly_schedules_clients = [
    { id: null, day_name: 'Monday', start: '', end: '', isShow: false },
    { id: null, day_name: 'Tuesday', start: '', end: '', isShow: false },
    { id: null, day_name: 'Wednesday', start: '', end: '', isShow: false },
    { id: null, day_name: 'Thursday', start: '', end: '', isShow: false },
    { id: null, day_name: 'Friday', start: '', end: '', isShow: false },
    { id: null, day_name: 'Saturday', start: '', end: '', isShow: false },
    { id: null, day_name: 'Sunday', start: '', end: '', isShow: false },
  ];
  weekly_schedules_ar = [];
  @Input() dataPeriod: any = [];
  @Output() messageTimePeriod = new EventEmitter();

  constructor(
    private atp: AmazingTimePickerService,
  ) { }

  ngOnInit() {
  }
  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.dataPeriod &&
      simpleChanges.dataPeriod.currentValue &&
      simpleChanges.dataPeriod.currentValue !== simpleChanges.dataPeriod.previousValue) {
      this.dataPeriod.forEach(_time => {
        _time.isShow = true;
        this.weekly_schedules_clients.forEach(_time1 => {
          if (_time1.day_name == _time.day_name) {
            _time1.start = _time.start;
            _time1.end = _time.end;
            _time1.isShow = _time.isShow;
            _time1.id = _time.id;
            this.weekly_schedules_ar.push(_time1);
          }
        });
      });
    }
    this.messageTimePeriod.emit(this.weekly_schedules_ar);
  }
  checkDate(item?: any, _index?: number) {
    for (let i = 0; i < this.weekly_schedules_clients.length; i++) {
      if (this.weekly_schedules_clients[_index] === this.weekly_schedules_clients[i]) {
        this.weekly_schedules_clients[_index].isShow = !this.weekly_schedules_clients[_index].isShow;
      }
    }
    this.checkTime(item);
  }
  open(item?: any) {
    const amazingTimePicker = this.atp.open({
      theme: 'material-blue',
    });
    amazingTimePicker.afterClose().subscribe(time => {
      item.start = time;
    });
  }
  open1(item?: any) {
    const amazingTimePicker = this.atp.open({
      theme: 'material-blue',
    });
    amazingTimePicker.afterClose().subscribe(time => {
      item.end = time;
    });
  }
  checkTime(item?: any) {
    let index = this.weekly_schedules_ar.indexOf(item);
    if (index == -1) {
      this.weekly_schedules_ar.push(item);
    } else {
      this.weekly_schedules_ar.splice(index, 1);
    }
    this.messageTimePeriod.emit(this.weekly_schedules_ar);
  }

}
