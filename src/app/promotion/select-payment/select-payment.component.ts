import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-select-payment',
  templateUrl: './select-payment.component.html',
  styleUrls: ['./select-payment.component.scss']
})
export class SelectPaymentComponent implements OnInit, OnChanges {

  @Input() dataPayment: any = [];
  @Input() checkPayment: any = [];

  is_check_all1: boolean = false;
  paymentMethods = [];
  paymentMethods_req = [];

  @Output() messagePaymentMethod = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.checkPayment || simpleChanges.dataPayment) {
      this.paymentMethods_req = this.checkPayment;
      if (this.paymentMethods_req.length > 0 && this.dataPayment.length > 0) {
        (this.paymentMethods_req.length == this.dataPayment.length) ? this.is_check_all1 = true : '';
      }
      this.messagePaymentMethod.emit(this.paymentMethods_req);
    }
  }
  //chekcall_payment
  checkPaymentMethodsAll() {
    let vm = this;
    if (vm.is_check_all1 == true) {
      vm.paymentMethods_req = [];
    }
    if (!vm.is_check_all1) {
      vm.dataPayment.forEach(_item => {
        let index = this.paymentMethods_req.indexOf(_item);
        if (index == -1) {
          this.paymentMethods_req.push(_item);
        }
      });
    } else {
      for (var i = 0; i < vm.paymentMethods_req.length; i++) {
        for (var j = 0; j < vm.dataPayment.length; j++) {
          vm.paymentMethods_req = [];
        }
      }
    }
    this.messagePaymentMethod.emit(this.paymentMethods_req);
  }
  //checkpaymentmethod
  checkPaymentMethods(item?: string) {
    var vm = this;
    let index = vm.paymentMethods_req.indexOf(item);
    if (index == -1) {
      vm.paymentMethods_req.push(item);
    } else {
      vm.is_check_all1 = false;
      vm.paymentMethods_req.splice(index, 1);
    }
    (vm.paymentMethods_req.length == vm.dataPayment.length) ? vm.is_check_all1 = true : '';
    vm.messagePaymentMethod.emit(vm.paymentMethods_req);
  }

}
