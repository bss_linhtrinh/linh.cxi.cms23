import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CreatePromotionComponent} from './create-promotion/create-promotion.component';
import {ListPromotionComponent} from './list-promotion/list-promotion.component';
import {EditPromotionComponent} from './edit-promotion/edit-promotion.component';
import {CheckPermissionsGuard} from '../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: '', redirectTo: 'list', pathMatch: 'full'},
            {path: 'create', component: CreatePromotionComponent, data: {breadcrumb: 'Create new promotion'}},
            {path: 'list', component: ListPromotionComponent, data: {breadcrumb: 'List promotions'}},
            {path: ':id/edit', component: EditPromotionComponent, data: {breadcrumb: 'Edit promotion'}},
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PromotionRoutingModule {
}
