import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AmazingTimePickerModule} from 'amazing-time-picker';
import {PromotionRoutingModule} from './promotion-routing.module';
import {CreatePromotionComponent} from './create-promotion/create-promotion.component';
import {ListPromotionComponent} from './list-promotion/list-promotion.component';
import {EditPromotionComponent} from './edit-promotion/edit-promotion.component';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {SelectPlatformComponent} from './select-platform/select-platform.component';
import {SelectUserComponent} from './select-user/select-user.component';
import {SelectPaymentComponent} from './select-payment/select-payment.component';
import {SelectPeriodComponent} from './select-period/select-period.component';
import {ApplicationScopeComponent} from './application-scope/application-scope.component';
import {SelectProductComponent} from './select-product/select-product.component';
import {SelectGiftComponent} from './select-gift/select-gift.component';

@NgModule({
    declarations: [
        CreatePromotionComponent,
        ListPromotionComponent,
        EditPromotionComponent,
        SelectPlatformComponent,
        SelectUserComponent,
        SelectPaymentComponent,
        SelectPeriodComponent,
        ApplicationScopeComponent,
        SelectProductComponent,
        SelectGiftComponent
    ],
    imports: [
        CommonModule,
        AmazingTimePickerModule,
        SharedModule,
        FormsModule,
        PromotionRoutingModule
    ]
})
export class PromotionModule {
}
