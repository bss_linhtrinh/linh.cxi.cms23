import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PromotionRepository } from 'src/app/shared/repositories/promotion.repository';
import { filter, map, switchMap } from 'rxjs/operators';
import { Promotion } from 'src/app/shared/entities/promotion';
import { Category } from 'src/app/shared/entities/category';
import { Product } from 'src/app/shared/entities/product';
import { Active } from 'src/app/shared/entities/active';
import { PaymentMethodsRepository } from 'src/app/shared/repositories/payment-methods.repository';
import { CustomerLevelsRepository } from 'src/app/shared/repositories/customer-levels.repository';

declare const helper: any;
@Component({
    selector: 'app-edit-promotion',
    templateUrl: './edit-promotion.component.html',
    styleUrls: ['./edit-promotion.component.scss']
})
export class EditPromotionComponent implements OnInit {
    tab: string = 'en';
    public promotion: Promotion = new Promotion();
    public categories: Category[] = [];
    public products: Product[] = [];
    public status: Active[] = Active.init();
    selectedFile: File = null;

    minDate = new Date('1970-01-01');
    maxDate = new Date();
    free_gift_conditions = [
        { name: 'Product', value: 'product' },
        { name: 'Minimum price', value: 'minimum_price' },
        { name: 'None', value: 'none' },
    ];
    discount_types = [
        { name: 'Discount', value: 'discount' },
        { name: 'Discount item', value: 'discount_item' },
        { name: 'Free gift', value: 'free_gift' },
        { name: 'Single price', value: 'single_price' },
    ];
    userScope: any;
    brand: any;
    outlet: any;
    weekly_schedules_ar = [];
    productClient = [];
    promotion_gift_check = 'All';

    loadproduct: boolean = false;
    product_gift = [];
    product_gift_id = [];
    customerLevel = [];
    customerlevel_req = [];

    paymentMethods = [];
    paymentMethods_req = [];

    gifts_client = {
        gift_type: '',
        gift_ids: [],
    }
    gifts_req = {
        gift_type: '',
        gift_ids: [],
    }
    gift = {
        gift_id: null,
        allowed_gift_quantity_items: 0,
        promotion_gift_type: '',
        product_ids: [],
        quantity: 1
    };
    listGift: any;
    product_select = [];

    is_check_all1: boolean = false;

    checked_customer_level = [];
    checked_payment_methods = [];
    payment_promotion = [];
    customerLevel_promotion = [];
    product_res = [];
    weekly_schedules_push = [];
    //
    minimum_amount_pr: string;
    maximum_discount_pr: string;
    price_pr: string;
    //outlet
    check_outlet: string = 'all';
    outlet_item: any;
    selected_outlet = [];
    show_outlet: boolean = false;
    res_outlet = [];
    platform_req = [];
    platform_res = [];
    application_scope: any;
    changePayment: any;
    drop_brand_id: string;
    applied_product: string = 'applied';
    isShow: boolean = false;
    discount_threshold_pr: string;

    constructor(
        private activatedRoute: ActivatedRoute,
        private promotionRepository: PromotionRepository,
        private paymentMethodRepository: PaymentMethodsRepository,
        private customerlevelRepository: CustomerLevelsRepository,
        private router: Router,
    ) {
    }

    ngOnInit() {
        this.filterMoney();
        this.loadPromotion();
    }

    loadPromotion() {
        this.activatedRoute.params
            .pipe(
                filter(data => data.id),
                map(data => data.id),
                switchMap(
                    (id) => this.promotionRepository.find(id)
                )
            )
            .subscribe((res) => {
                this.promotion = res;
                this.promotion.translations.forEach(item => {
                    if (item.locale == 'en') {
                        this.promotion.en = item;
                    } else if (item.locale == 'vi') {
                        this.promotion.vi = item;
                    }
                });
                this.checked_customer_level = this.promotion.customer_levels;
                this.checked_payment_methods = this.promotion.payment_methods;
                if (this.promotion.gifts !== '') {
                    this.gifts_client = this.promotion.gifts;
                }
                if (this.promotion.platform != null) {
                    this.platform_res = this.promotion.platform;
                }
                if (this.promotion.free_gift_condition == 'minimum_price' || this.promotion.free_gift_condition == 'none') {
                    helper.filterMoneys();
                    this.isShow = true;
                    this.loadproduct = true;
                }
                this.product_res = this.promotion.products;
                if (this.promotion.product_blacklist.length > 0) {
                    this.applied_product = 'not_applied';
                    this.productClient = this.promotion.product_blacklist;
                } else if (this.promotion.products.length > 0) {
                    this.productClient = this.promotion.products;
                }
                (this.promotion && this.promotion.brand_id) ? this.drop_brand_id = this.promotion.brand_id.toString() : null;
                (this.promotion.minimum_amount) ? this.minimum_amount_pr = this.promotion.minimum_amount.toString() : null;
                (this.promotion.minimum_amount) ? this.minimum_amount_pr = helper.formatNumber(this.promotion.minimum_amount, 'VND') : null;
                (this.promotion.maximum_discount) ? this.maximum_discount_pr = this.promotion.maximum_discount.toString() : null;
                (this.promotion.maximum_discount) ? this.maximum_discount_pr = helper.formatNumber(this.promotion.maximum_discount, 'VND') : null;
                //(this.promotion.discount_threshold) ? this.discount_threshold_pr = this.promotion.discount_threshold.toString() : null;
                //(this.promotion.discount_threshold) ? this.discount_threshold_pr = helper.formatNumber(this.promotion.discount_threshold, 'VND') : null;
                (this.promotion.price) ? this.price_pr = this.promotion.price.toString() : null;
                this.weekly_schedules_push = this.promotion.weekly_schedules;
                (this.promotion.discount_type == 'single_price') ? helper.filterMoneys() : '';
                this.loadApi();
            });
    }

    loadApi() {
        this.loadPaymentMethods();
        this.loadCustomerLevel();
    }
    onFileChanged(selectedFile: File) {
        this.selectedFile = selectedFile;
    }
    //paymentmethods
    loadPaymentMethods() {
        this.paymentMethodRepository.all({ status: 1, brand_id: this.promotion.brand_id })
            .subscribe(res => {
                this.paymentMethods = res;
                //load Edit payment
                this.paymentMethods.forEach(_item => {
                    this.checked_payment_methods.forEach(_item1 => {
                        if (_item1 == _item.id) {
                            this.payment_promotion.push(_item);
                            this.paymentMethods_req = this.payment_promotion;
                        }
                    });
                });
                //load Edit payment
            });
    }
    //customerlevel
    loadCustomerLevel() {
        this.customerlevelRepository.all({ status: 1, brand_id: this.promotion.brand_id })
            .subscribe(res => {
                this.customerLevel = res;
                this.customerLevel.forEach(_item => {
                    this.checked_customer_level.forEach(_item1 => {
                        if (_item1 == _item.id) {
                            this.customerLevel_promotion.push(_item);
                            this.customerlevel_req = this.customerLevel_promotion;
                        }
                    });
                });
            });
    }
    addProduct() {
        if (this.promotion.discount_type == 'discount_item' || this.promotion.discount_type == 'free_gift') {
            this.price_pr = '';
            this.promotion.discount_amount = 0;
            this.minimum_amount_pr = '';
            this.maximum_discount_pr = '';
            this.discount_threshold_pr = '';
            this.isShow = false;
            this.promotion.free_gift_condition = this.free_gift_conditions[0].value;
            this.loadproduct = false;
            helper.filterMoneys();
        }
        if (this.promotion.discount_type == 'discount_item' || this.promotion.discount_type == 'discount') {
            this.price_pr = '';
            this.promotion.discount_amount = 0;
            this.minimum_amount_pr = '';
            this.maximum_discount_pr = '';
            this.discount_threshold_pr = '';
            this.filterMoney();
        }
        if (this.promotion.discount_type == 'single_price') {
            this.promotion.discount_amount = 0;
            this.minimum_amount_pr = '';
            this.maximum_discount_pr = '';
            this.discount_threshold_pr = '';
            this.isShow = false;
            helper.filterMoneys();
        }
        if (this.promotion.discount_amount > 100) {
            this.promotion.discount_amount = 0;
        }
    }
    showTabProduct(_value) {
        if (_value.value == 'none' || _value.value == 'minimum_price') {
            this.loadproduct = true;
            this.isShow = true;
            helper.filterMoneys();
        } else {
            this.loadproduct = false;
            this.isShow = false;
        }
        this.minimum_amount_pr = '';
    }
    filterMoney() {
        helper.filterMoneys();
        helper.percentInput();
    }
    //new-promotion
    platformMessage($event: any) {
        this.platform_req = $event;
    }
    customerlevelMessage($event: any) {
        this.customerlevel_req = $event;
    }
    paymentMethodMessage($event: any) {
        this.paymentMethods_req = $event;
    }
    periodMessage($event: any) {
        this.weekly_schedules_ar = $event;
    }
    scopeMessage($event: any) {
        this.selected_outlet = $event;
    }
    brandMess($event) {
        this.application_scope = $event;
        this.promotion.brand_id = this.application_scope.brand_id;
        this.check_outlet = this.application_scope.check_outlet;
        this.loadPaymentMethods();
    }
    productMessage($event) {
        this.productClient = $event;
    }
    productGiftMessage($event) {
        this.gifts_client.gift_ids = $event;
    }
    promotionGiftTypeMessage($event) {
        this.gifts_client.gift_type = $event
    }
    giftIdMessage($event) {
        //this.gifts_client[0].gift_id = $event;
    }
    quantityItemMessage($event) {
        this.promotion.allowed_gift_quantity_items = $event;
    }
    checkQuantityMessage($event) {
        this.promotion_gift_check = $event;
        if (this.promotion_gift_check == 'Limit') {
            this.promotion.allowed_gift_quantity_items = 1;
            this.gifts_client.gift_ids = [];
        } else {
            this.gifts_client.gift_ids = [];
            this.promotion.allowed_gift_quantity_items = 0;
        }
    }
    showBrandMess($event) {
        this.changePayment = $event;
        if (this.changePayment.loadpayment == true && this.changePayment.brand_id) {
            this.promotion.brand_id = this.changePayment.brand_id;
        }
    }
    showOuletChange($event) {
        this.check_outlet = $event;
    }
    formatData() {
        this.promotion.translations = null;
        this.gifts_req.gift_type = this.gifts_client.gift_type;
        this.gifts_req.gift_ids = this.gifts_client.gift_ids;
        this.promotion.gifts = this.gifts_req;
        //Platforms
        this.promotion.platform = this.platform_req.map(_p => {
            return this.promotion.platform = _p.value;
        });
        //outlet
        if (this.check_outlet == 'all') {
            this.promotion.apply_for_all_outlets = true;
            this.promotion.outlets = [];
        } else {
            this.promotion.apply_for_all_outlets = false;
            this.promotion.outlets = this.selected_outlet.map(_item => {
                return this.promotion.outlets = _item.id;
            });
        }
        //outlet
        //quantity-number
        if (this.minimum_amount_pr) {
            let p = '';
            this.minimum_amount_pr.split(',').forEach(_e => {
                p += _e;
            });
            this.promotion.minimum_amount = Number(p);
        }
        if (this.minimum_amount_pr == '') {
            this.promotion.minimum_amount = 0;
        }
        if (this.maximum_discount_pr) {
            let p1 = '';
            this.maximum_discount_pr.split(',').forEach(_e1 => {
                p1 += _e1;
            });
            this.promotion.maximum_discount = Number(p1);
        }
        if (this.discount_threshold_pr) {
            let p2 = '';
            this.discount_threshold_pr.split(',').forEach(_e1 => {
                p2 += _e1;
            });
            //this.promotion.discount_threshold = Number(p2);
        }
        if (this.maximum_discount_pr == '') {
            this.promotion.maximum_discount = 0;
        }
        //quantity-number
        this.promotion.customer_levels = this.customerlevel_req.map(_o => {
            return this.promotion.customer_levels = _o.id;
        });
        //paymentMethod
        this.promotion.payment_methods = this.paymentMethods_req.map(_payment => {
            return this.promotion.payment_methods = _payment.id;
        });
        this.promotion.weekly_schedules = this.weekly_schedules_ar;
        if (typeof this.productClient != null && this.productClient.length > 0) {
            this.promotion.products = this.productClient.map(_e => {
                return this.promotion.products = _e.id;
            });
        }
        if (this.applied_product == 'not_applied') {
            this.promotion.product_blacklist = this.promotion.products;
            this.promotion.products = [];
        } else {
            this.promotion.product_blacklist = [];
        }
        if (this.promotion.discount_type == "free_gift") {
            this.promotion.discount_amount = null;
            //this.promotion.minimum_amount = null;
            this.promotion.maximum_discount = null;
        }
        //
        if (this.promotion.discount_amount > 100) {
            this.promotion.discount_amount = 0;
        }
        //
        this.promotion.discount_amount = Number(this.promotion.discount_amount);
        //
        if (this.price_pr) {
            let _price = '';
            this.price_pr.split(',').forEach(_pri => {
                _price += _pri;
            });
            this.promotion.price = Number(_price);
        }
    }
    //submit
    onSubmit() {
        this.formatData();
        this.promotionRepository.save(this.promotion).subscribe(res => {
            this.cancel();
        });
    }
    cancel() {
        this.router.navigate(['../..', 'list'], { relativeTo: this.activatedRoute });
    }
}
