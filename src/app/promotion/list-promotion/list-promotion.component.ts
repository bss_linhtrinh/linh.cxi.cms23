import {Component, OnInit} from '@angular/core';
import {Promotion} from 'src/app/shared/entities/promotion';
import {PromotionRepository} from 'src/app/shared/repositories/promotion.repository';
import {Router, ActivatedRoute} from '@angular/router';
import {LocalStorage, SessionStorage} from 'ngx-webstorage';
import {CxiGridConfig} from '../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../shared/components/cxi-grid/cxi-grid-column';

@Component({
    selector: 'app-list-promotion',
    templateUrl: './list-promotion.component.html',
    styleUrls: ['./list-promotion.component.scss']
})
export class ListPromotionComponent implements OnInit {

    public promotions: Promotion[] = [];

    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        className: false,
        pagingServer: true,
        repository: this.promotionRepository,
        translatable: true,
        actions: {
            onEdit: (promotion: Promotion) => {
                return this.onEdit(promotion);
            },
            onDelete: (promotion: Promotion) => {
                return this.onDelete(promotion);
            },
            onActivate: (promotion: Promotion) => {
                this.onActivate(promotion);
            },
            onDeactivate: (promotion: Promotion) => {
                this.onDeactivate(promotion);
            },
        }
    });
    cxiGridColumns = CxiGridColumn.from([
        {title: 'name', name: 'name', avatar: 'images'},
        {title: 'description', name: 'description'},
        {title: 'start date', name: 'starts_from', format: 'date'},
        {title: 'end date', name: 'ends_till', format: 'date'},
    ]);

    @LocalStorage('scope.organization_id') organizationId: number;
    @LocalStorage('scope.brand_id') brandId: number;
    @LocalStorage('scope.outlet_id') outletId: number;

    constructor(
        private promotionRepository: PromotionRepository,
        private router: Router,
        public activatedRoute: ActivatedRoute,
    ) {
    }

    ngOnInit() {
    }

    onEdit(promotion: Promotion) {
        this.router.navigate(['..', promotion.id, 'edit'], {relativeTo: this.activatedRoute});
    }

    onDelete(promotion: Promotion) {
        this.promotionRepository.destroy(promotion).subscribe();
    }

    onActivate(promotion: Promotion) {
        this.promotionRepository.activate(promotion).subscribe();
    }

    onDeactivate(promotion: Promotion) {
        this.promotionRepository.deactivate(promotion).subscribe();
    }

}
