import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-select-platform',
  templateUrl: './select-platform.component.html',
  styleUrls: ['./select-platform.component.scss']
})
export class SelectPlatformComponent implements OnInit, OnChanges {

  @Input() dataPlatform = [];

  public platform_req: any = [];
  public platforms = [
    { name: 'Web cms', value: 'web_cms' },
    { name: 'App user', value: 'app_user' },
    { name: 'App pos', value: 'app_pos' },
    { name: 'Grab', value: 'grab' },
    { name: 'Now', value: 'now' },
    { name: 'Menu Digital', value: 'app_menu_digital' },
  ];

  @Output() messageEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.dataPlatform &&
      simpleChanges.dataPlatform.currentValue &&
      simpleChanges.dataPlatform.currentValue !== simpleChanges.dataPlatform.previousValue) {
      this.platforms.forEach(_p1 => {
        this.dataPlatform.forEach(_p2 => {
          if (_p2 == _p1.value) {
            this.platform_req.push(_p1);
          }
        });
      });
      this.messageEvent.emit(this.platform_req);
    }
  }

  checkPlatforms(_item) {
    let index = this.platform_req.indexOf(_item);
    if (index == -1) {
      this.platform_req.push(_item);
    } else {
      this.platform_req.splice(index, 1);
    }
    this.messageEvent.emit(this.platform_req);
  }

}
