import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectGiftComponent } from './select-gift.component';

describe('SelectGiftComponent', () => {
  let component: SelectGiftComponent;
  let fixture: ComponentFixture<SelectGiftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectGiftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectGiftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
