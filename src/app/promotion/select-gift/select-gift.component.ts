import { Component, OnInit, Output, EventEmitter, OnChanges, SimpleChanges, Input } from '@angular/core';
import { CategoriesRepository } from 'src/app/shared/repositories/categories.repository';
import { Category } from 'src/app/shared/entities/category';
import { Product } from 'src/app/shared/entities/product';
import { GiftCategoriesRepository } from 'src/app/shared/repositories/gift-categories.repository';

declare const helper: any;

@Component({
  selector: 'app-select-gift',
  templateUrl: './select-gift.component.html',
  styleUrls: ['./select-gift.component.scss']
})
export class SelectGiftComponent implements OnInit, OnChanges {

  @Output() messageProductGift = new EventEmitter();
  @Output() messagePromotionGiftType = new EventEmitter();
  @Output() messagePromotionGiftId = new EventEmitter();
  @Output() messageQuantityItem = new EventEmitter();
  @Output() checkQuantityItem = new EventEmitter();

  @Input() dataGift;
  @Input() quantityGift;
  @Input() brand_id;

  public categories: Category[] = [];
  public products: Product[] = [];
  public categoryGift: any;
  promotion_gift_types = [
    { name: 'products', value: 'products' },
    { name: 'free gift', value: 'free_gift' }
  ];
  gifts_client = {
    gift_type: 'customer_pr',
    gift_ids: [],
  }
  promotion_gift_qty = ['All', 'Limit'];
  promotion_gift_check = 'All';
  gift = {
    gift_id: null,
    allowed_gift_quantity_items: 0,
    promotion_gift_type: '',
    product_ids: [],
    quantity: 1
  };
  cate_child_pr: any = 'customer_pr';
  sub_cate_pr: any;
  subcate_child_pr: any = 'customer_pr';
  isShowcate1: boolean = false;
  product_select = [];
  product_gift = [];
  disable_quantity: boolean = false;
  disableProduct: boolean = false;
  isShow: boolean = false;
  product_gift_id = [];
  listGift: any;
  gift_id: string;
  gift_detail = [];
  brand_id_req: number;

  constructor(
    private categoriesRepository: CategoriesRepository,
    private giftCategoryRepository: GiftCategoriesRepository
  ) { }

  ngOnInit() {
  }
  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.dataGift &&
      simpleChanges.dataGift.currentValue &&
      simpleChanges.dataGift.currentValue !== simpleChanges.dataGift.previousValue) {
      this.dataGift.forEach(_i => {
        _i.name = _i.gift_detail.name;
        _i.id = _i.gift_detail.id;
      });
      this.product_gift_id = this.dataGift.map(_gift => {
        return this.product_gift_id = _gift.id;
      })
      this.product_gift = this.dataGift;
      (this.dataGift.length > 0) ? this.gifts_client.gift_type = this.dataGift[0].gift_type : null;
      this.gift.quantity = this.quantityGift;
      (this.gift.quantity > 0) ? this.isShow = true : this.isShow = false;
      if (this.product_gift.length >= this.gift.quantity) {
        this.disableProduct = true;
        this.disable_quantity = true;
      } else {
        this.disableProduct = false;
      }
      if (this.gift.quantity > 0) {
        this.promotion_gift_check = 'Limit';
        this.isShow = true;
      } else {
        this.promotion_gift_check = 'All';
        this.disableProduct = false;
      }
      if (this.dataGift.length > 0 && this.dataGift[0].gift_type == 'free_gift') {
        this.getCategoryGift();
        this.gift_id = 'customer_pr';
      } else {
        this.getCategories();
      }
      this.messageQuantityItem.emit(this.gift.quantity);
      this.messagePromotionGiftType.emit(this.gifts_client.gift_type);
      this.messageProductGift.emit(this.product_gift_id);
    }
    if (this.brand_id != undefined) {
      this.brand_id_req = this.brand_id.brand_id;
      this.gifts_client.gift_type = 'customer_pr';
    }
  }
  getCategories() {
    this.categoriesRepository
      .allAsTree({ status: 1, brand_id: this.brand_id_req })
      .subscribe(
        (categories: Category[]) => {
          this.categories = categories;
        }
      );
  }
  getCategoryGift() {
    this.giftCategoryRepository
      .all({ status: 1, brand_id: this.brand_id_req })
      .subscribe(
        (res: any) => {
          this.categoryGift = res;
        }
      );
  }
  checkQty(e: string) {
    if (e === 'All') {
      this.isShow = false;
      this.disableProduct = false;
      this.product_select = [];
      this.product_gift = [];
      this.product_gift_id = [];
      this.cate_child_pr = 'customer_pr';
    } else if (e === 'Limit') {
      this.gift.quantity = 1;
      this.isShow = true;
      this.product_select = [];
      this.product_gift = [];
      this.product_gift_id = [];
      this.cate_child_pr = 'customer_pr';
    }
    this.checkQuantityItem.emit(e);
  }
  incrementPlus() {
    helper.incrementQuantity(this.gift);
    (this.product_gift.length == this.gift.quantity) ? this.disable_quantity = true : this.disable_quantity = false;
    if (this.product_gift.length >= this.gift.quantity) {
      this.disableProduct = true;
    } else {
      this.disableProduct = false;
    }
    this.messageQuantityItem.emit(this.gift.quantity);
  }
  changeCategoryGift() {
    this.getGiftBycategory(this.gift_id)
  }
  incrementMinus() {
    helper.incrementQuantity(this.gift, false);
    (this.product_gift.length == this.gift.quantity) ? this.disable_quantity = true : this.disable_quantity = false;
    (this.product_gift.length >= this.gift.quantity) ? this.disableProduct = true : this.disableProduct = false;
    this.messageQuantityItem.emit(this.gift.quantity);
  }
  changePromotion() {
    if (this.gifts_client.gift_type == 'free_gift') {
      this.product_gift = [];
      this.product_gift_id = [];
      this.product_select = [];
      this.isShowcate1 = false;
      this.cate_child_pr = 'customer_pr';
      this.getCategoryGift();
      this.gift_id = 'customer_pr';
    } else if (this.gifts_client.gift_type == 'products') {
      this.product_gift = [];
      this.product_gift_id = [];
      this.product_select = [];
      this.getCategories();
    }
    this.messagePromotionGiftType.emit(this.gifts_client.gift_type);
  }
  onChangePromotionCt(categories: number) {
    let o;
    this.subcate_child_pr = 'customer_pr';
    this.categories.forEach(item => {
      if (item.id == this.cate_child_pr) {
        // this.isShowcate = true;
        o = item;
      }
    });
    if (o.children.length !== 0) {
      this.product_select = [];
      this.sub_cate_pr = o.children;
      this.isShowcate1 = true;
    } else {
      this.sub_cate_pr = [];
      this.isShowcate1 = false;
      this.getProductBycategoryPr(o.id);
    }
  }
  onChangeProductSubCt() {
    this.getProductBycategoryPr(this.subcate_child_pr);
  }
  addProductGift(item, _id, _index) {
    let index = this.product_gift.indexOf(item);
    if (this.promotion_gift_check == 'All') {
      if (index == -1) {
        this.product_gift.push(item);
        this.product_gift_id.push(_id);
        this.product_select.splice(_index, 1);
      } else {
        this.product_gift.splice(index, 1);
        this.product_gift_id.splice(index, 1);
        this.product_select.push(item);
      }
    } else {
      if (index == -1) {
        this.product_gift.push(item);
        this.product_gift_id.push(_id);
        this.product_select.splice(_index, 1);
        if (this.product_gift.length >= this.gift.quantity) {
          this.disableProduct = true;
        } else {
          this.disableProduct = false;
        }
      } else {
        this.disableProduct = false;
        this.product_gift.splice(index, 1);
        this.product_gift_id.splice(index, 1);
        this.product_select.push(item);
      }
    }
    (this.product_gift.length == this.gift.quantity) ? this.disable_quantity = true : this.disable_quantity = false;
    this.messageProductGift.emit(this.product_gift_id);
  }
  getProductBycategoryPr(categories?: number) {
    this.categoriesRepository.getProductsByCategory(categories)
      .subscribe(
        (product: Product[]) => {
          this.product_select = product;
          if (this.product_gift.length > 0) {
            this.product_gift.forEach(_p => {
              this.product_select.forEach(_p1 => {
                if (_p.id == _p1.id) {
                  let index = this.product_select.indexOf(_p1);
                  if (index !== -1) {
                    this.product_select.splice(index, 1);
                  }
                }
              });
            });
          }
        }
      );
  }
  getGiftBycategory(categories) {
    this.giftCategoryRepository.getGiftByCategory(categories)
      .subscribe(
        (res: any) => {
          (res && res.length > 0) ? this.product_select = res : null;
          if (this.product_gift.length > 0) {
            this.product_gift.forEach(_p => {
              this.product_select.forEach(_p1 => {
                if (_p.id == _p1.id) {
                  let index = this.product_select.indexOf(_p1);
                  if (index !== -1) {
                    this.product_select.splice(index, 1);
                  }
                }
              });
            });
          }
        }
      );
  }
}
