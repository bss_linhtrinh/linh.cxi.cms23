import { Component, OnInit } from '@angular/core';
import { Promotion } from 'src/app/shared/entities/promotion';
import { PromotionRepository } from 'src/app/shared/repositories/promotion.repository';
import { Active } from 'src/app/shared/entities/active';
import { Router } from '@angular/router';
import { PaymentMethodsRepository } from 'src/app/shared/repositories/payment-methods.repository';

declare const helper: any;

@Component({
    selector: 'app-create-promotion',
    templateUrl: './create-promotion.component.html',
    styleUrls: ['./create-promotion.component.scss']
})
export class CreatePromotionComponent implements OnInit {
    tab: string = 'en';
    public promotion: Promotion = new Promotion();
    public status: Active[] = Active.init();
    selectedFile: File = null;
    minDate = new Date('1970-01-01');
    maxDate = new Date();
    discount_types = [
        { name: 'Discount', value: 'discount' },
        { name: 'Discount item', value: 'discount_item' },
        { name: 'Free gift', value: 'free_gift' },
        { name: 'Single price', value: 'single_price' },
    ];
    free_gift_conditions = [
        { name: 'Product', value: 'product' },
        { name: 'Minimum price', value: 'minimum_price' },
        { name: 'None', value: 'none' },
    ];
    weekly_schedules_ar = [];
    productClient = [];
    gifts_client = {
        gift_type: '',
        gift_ids: [],
    }
    loadproduct: boolean = false;
    customerlevel_req = [];
    customerLevel = [];
    paymentMethods = [];
    paymentMethods_req = [];
    minimum_amount_pr: string;
    maximum_discount_pr: string;
    price_pr: string;
    check_outlet: string = 'all';
    outlet_item: any;
    selected_outlet = [];
    platform_req = [];
    percent_pr: string;
    application_scope: any;
    promotion_gift_check: string = 'All';
    changePayment: any;
    applied_product: string = 'applied';
    isShow: boolean = false;
    discount_threshold_pr: string;

    constructor(
        private promotionRepository: PromotionRepository,
        private paymentMethodRepository: PaymentMethodsRepository,
        private router: Router,
    ) {
    }

    ngOnInit() {
        this.filterMoney();
    }
    onFileChanged(selectedFile: File) {
        this.selectedFile = selectedFile;
    }
    //paymentmethods
    loadPaymentMethods() {
        this.paymentMethodRepository.all({ status: 1, brand_id: this.promotion.brand_id })
            .subscribe(res => {
                this.paymentMethods = res;
            });
    }
    addProduct() {
        if (this.promotion.discount_type == 'discount_item' || this.promotion.discount_type == 'free_gift') {
            this.price_pr = '';
            this.promotion.discount_amount = 0;
            this.minimum_amount_pr = '';
            this.maximum_discount_pr = '';
            this.discount_threshold_pr = '';
            this.isShow = false;
            this.promotion.free_gift_condition = this.free_gift_conditions[0].value;
            this.loadproduct = false;
            helper.filterMoneys();
        }
        if (this.promotion.discount_type == 'discount_item' || this.promotion.discount_type == 'discount') {
            this.price_pr = '';
            this.promotion.discount_amount = 0;
            this.minimum_amount_pr = '';
            this.maximum_discount_pr = '';
            this.discount_threshold_pr = '';
            this.filterMoney();
        }
        if (this.promotion.discount_type == 'single_price') {
            this.promotion.discount_amount = 0;
            this.minimum_amount_pr = '';
            this.maximum_discount_pr = '';
            this.discount_threshold_pr = '';
            this.isShow = false;
            helper.filterMoneys();
        }
        if (this.promotion.discount_amount > 100) {
            this.promotion.discount_amount = 0;
        }
    }
    showTabProduct(_value) {
        if (_value.value == 'none' || _value.value == 'minimum_price') {
            this.loadproduct = true;
            this.isShow = true;
            helper.filterMoneys();
        } else {
            this.loadproduct = false;
            this.isShow = false;
        }
        this.minimum_amount_pr = '';
    }
    filterMoney() {
        helper.filterMoneys();
        helper.percentInput();
    }
    platformMessage($event: any) {
        this.platform_req = $event;
    }
    customerlevelMessage($event: any) {
        this.customerlevel_req = $event;
    }
    paymentMethodMessage($event: any) {
        this.paymentMethods_req = $event;
    }
    periodMessage($event: any) {
        this.weekly_schedules_ar = $event;
    }
    scopeMessage($event: any) {
        this.selected_outlet = $event;
    }
    brandMess($event) {
        this.application_scope = $event;
        this.promotion.brand_id = this.application_scope.brand_id;
        this.check_outlet = this.application_scope.check_outlet;
        this.loadPaymentMethods();
    }
    productMessage($event) {
        this.productClient = $event;
    }
    productGiftMessage($event) {
        this.gifts_client.gift_ids = $event;
    }
    promotionGiftTypeMessage($event) {
        this.gifts_client.gift_type = $event;
    }
    giftIdMessage($event) {
        //this.gifts_client[0].gift_id = $event;
    }
    quantityItemMessage($event) {
        this.promotion.allowed_gift_quantity_items = $event;
    }
    checkQuantityMessage($event) {
        this.promotion_gift_check = $event;
        if (this.promotion_gift_check == 'Limit') {
            this.promotion.allowed_gift_quantity_items = 1;
            this.gifts_client.gift_ids = [];
        } else {
            this.gifts_client.gift_ids = [];
            this.promotion.allowed_gift_quantity_items = 0;
        }
    }
    showBrandMess($event) {
        this.changePayment = $event;
        if (this.changePayment.loadpayment == true && this.changePayment.brand_id) {
            this.promotion.brand_id = this.changePayment.brand_id;
            this.loadPaymentMethods();
        }
    }
    showOuletChange($event) {
        this.check_outlet = $event;
    }
    //format_data
    formatData() {
        //Platforms
        this.promotion.platform = this.platform_req.map(_p => {
            return this.promotion.platform = _p.value;
        });
        //outlet
        if (this.check_outlet == 'all') {
            this.promotion.apply_for_all_outlets = true;
            this.promotion.outlets = []
        } else {
            this.promotion.apply_for_all_outlets = false;
            this.promotion.outlets = this.selected_outlet.map(_item => {
                return this.promotion.outlets = _item.id;
            });
        }
        //outlet
        if (this.minimum_amount_pr) {
            let p = '';
            this.minimum_amount_pr.split(',').forEach(_e => {
                p += _e;
            });
            this.promotion.minimum_amount = Number(p);
        }
        if (this.maximum_discount_pr) {
            let p1 = '';
            this.maximum_discount_pr.split(',').forEach(_e1 => {
                p1 += _e1;
            });
            this.promotion.maximum_discount = Number(p1);
        }
        if (this.discount_threshold_pr) {
            let p2 = '';
            this.discount_threshold_pr.split(',').forEach(_e1 => {
                p2 += _e1;
            });
            //this.promotion.discount_threshold = Number(p2);
        }
        this.promotion.customer_levels = this.customerlevel_req.map(_o => {
            return this.promotion.customer_levels = _o.id;
        });
        //paymentMethod
        this.promotion.payment_methods = this.paymentMethods_req.map(_payment => {
            return this.promotion.payment_methods = _payment.id;
        });
        this.promotion.gifts = this.gifts_client;
        this.promotion.weekly_schedules = this.weekly_schedules_ar;
        if (typeof this.productClient != null && this.productClient.length > 0) {
            this.promotion.products = this.productClient.map(_e => {
                return this.promotion.products = _e.id;
            });
        } else if (this.productClient.length <= 0) {
            this.promotion.products = [];
        }
        if (this.applied_product == 'not_applied') {
            this.promotion.product_blacklist = this.promotion.products;
            this.promotion.products = [];
        } else {
            this.promotion.product_blacklist = [];
        }
        //
        if (this.promotion.discount_type == "free_gift") {
            this.promotion.discount_amount = null;
            //this.promotion.minimum_amount = null;
            this.promotion.maximum_discount = null;
        }
        //
        if (this.promotion.discount_amount > 100) {
            this.promotion.discount_amount = 0;
        }
        this.promotion.discount_amount = Number(this.promotion.discount_amount);
        //
        if (this.price_pr) {
            let _price = '';
            this.price_pr.split(',').forEach(_pri => {
                _price += _pri;
            });
            this.promotion.price = Number(_price);
        }
    }
    //submit
    onSubmit() {
        this.formatData();
        this.promotionRepository.save(new Promotion().fill(new Promotion().fill(this.promotion))).subscribe(res => {
            this.router.navigateByUrl('/promotion');
        });
    }
}
