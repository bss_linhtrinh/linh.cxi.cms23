import { Component, OnInit, Output, EventEmitter, OnChanges, SimpleChanges, Input } from '@angular/core';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { Promotion } from 'src/app/shared/entities/promotion';
import { OutletsRepository } from 'src/app/shared/repositories/outlets.repository';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';

@Component({
  selector: 'app-application-scope',
  templateUrl: './application-scope.component.html',
  styleUrls: ['./application-scope.component.scss']
})
export class ApplicationScopeComponent implements OnInit, OnChanges {
  public promotion: Promotion = new Promotion();
  userScope: any;
  brand: any;
  outlet: any;
  check_outlet: string = 'all';
  outlet_item: any;
  selected_outlet = [];
  brand_id: string;
  @Input() dataBrand: string;
  @Input() dataOutlet: any;
  @Output() messageScope = new EventEmitter();
  @Output() messageBrand = new EventEmitter();
  @Output() showBrand = new EventEmitter();
  @Output() showOuletChange = new EventEmitter();

  constructor(
    private scopeService: ScopeService,
    private outletRepository: OutletsRepository,
    private brandRepository: BrandsRepository,
  ) { }

  ngOnInit() {
    this.getListUserScope();
    this.getListBrand();
  }
  ngOnChanges(simpleChanges: SimpleChanges) {
    this.brand_id = this.dataBrand;
    if (simpleChanges.dataOutlet &&
      simpleChanges.dataOutlet.currentValue &&
      simpleChanges.dataOutlet.currentValue !== simpleChanges.dataOutlet.previousValue) {
      if (this.dataOutlet == null) {
        this.check_outlet = 'all';
      } else if (this.dataOutlet && this.dataOutlet.length > 0) {
        this.check_outlet = 'custom';
        this.selected_outlet = this.dataOutlet;
        this.onChangeBrand();
      }
    }
    this.messageScope.emit(this.selected_outlet);
  }
  getListBrand() {
    if (this.userScope.organization && this.userScope.brand == null) {
      this.brandRepository.all({ status: 1 }).subscribe(res => {
        this.brand = res;
        (this.dataBrand == null) ? this.brand_id = 'customer_pr' : '';
      });
    }
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res) => {
          this.userScope = res;
          if (res.brand != null) {
            this.promotion.brand_id = res.brand.id;
            let sen_data = {
              brand_id: this.promotion.brand_id,
              loadpayment: true
            }
            this.showBrand.emit(sen_data);
          }
        }
      );
  }
  //onchange brand
  onChangeBrand() {
    let sen_data = {
      brand_id: this.brand_id,
      check_outlet: this.check_outlet
    }
    if (this.check_outlet == 'custom') {
      this.outletRepository.getOutletsByBrand(this.brand_id)
        .subscribe(res => {
          this.outlet = res;
          this.outlet_item = 'customer_pr';
          this.outlet.forEach(_e => {
            // _e.disabled = true;
            this.selected_outlet.forEach(_t => {
              if (_e.id == _t.id) {
                _e.disabled = true;
              }
            });
          });
        });
    }
    this.messageBrand.emit(sen_data);
  }
  //checkCustomScope
  checkCustomScope() {
    this.check_outlet = 'custom';
    this.brand_id = 'customer_pr'
    this.outlet_item = 'customer_pr';
    this.outletRepository.getOutletsByBrand(this.promotion.brand_id)
      .subscribe(res => {
        this.outlet = res;
        this.outlet_item = 'customer_pr';
        this.outlet.forEach(_e => {
          // _e.disabled = true;
          this.selected_outlet.forEach(_t => {
            if (_e.id == _t.id) {
              _e.disabled = true;
            }
          });
        });
      });
    this.showOuletChange.emit(this.check_outlet);
  }
  checkAllScope() {
    this.brand_id = 'customer_pr';
    this.selected_outlet = [];
    this.check_outlet = 'all';
    this.messageScope.emit(this.selected_outlet);
    this.showOuletChange.emit(this.check_outlet);
  }
  //add brand
  addOutletByBrand() {
    this.outlet.forEach(_o => {
      if (_o.id == this.outlet_item) {
        this.selected_outlet.push(_o);
        this.outlet_item = 'customer_pr';
      }
    });
    this.messageScope.emit(this.selected_outlet);
  }
  //remove outlet
  removeSelectOutlet(_item) {
    let index = this.selected_outlet.indexOf(_item);
    if (index !== -1) {
      this.selected_outlet.splice(index, 1);
      this.outlet.forEach(_e => {
        if (_e.id == _item.id) {
          _e.disabled = false;
        }
      });
    }
    this.messageScope.emit(this.selected_outlet);
  }

}
