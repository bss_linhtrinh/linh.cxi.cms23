import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Category } from 'src/app/shared/entities/category';
import { CategoriesRepository } from 'src/app/shared/repositories/categories.repository';
import { Product } from 'src/app/shared/entities/product';

@Component({
  selector: 'app-select-product',
  templateUrl: './select-product.component.html',
  styleUrls: ['./select-product.component.scss']
})
export class SelectProductComponent implements OnInit, OnChanges {

  @Input() promotion_type: string;
  @Input() dataProduct: any;
  @Input() brand_id: any;
  @Output() messageProduct = new EventEmitter();

  public categories: Category[] = [];
  public products: Product[] = [];

  sub_cate: any = [];
  sub_cate_pr: any;
  subcate_child: any = 'customer_pr';
  subcate_child_pr: any = 'customer_pr';
  productClient = [];
  cate_child: any = 'customer_pr';
  isShowcate: boolean = false;
  brandShow: number;

  constructor(
    private categoriesRepository: CategoriesRepository,
  ) { }

  ngOnInit() {
    if (this.promotion_type == 'discount_item' || this.promotion_type == 'free_gift' || this.promotion_type == 'single_price') {
      this.getCategories();
    }
  }
  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.dataProduct &&
      simpleChanges.dataProduct.currentValue &&
      simpleChanges.dataProduct.currentValue !== simpleChanges.dataProduct.previousValue) {
      this.productClient = this.dataProduct;
    }
    if (simpleChanges.brand_id && this.brand_id != undefined) {
      this.brandShow = this.brand_id.brand_id;
      this.getCategories();
      this.productClient = [];
      this.products = [];
      this.cate_child = 'customer_pr';
    }
  }
  getCategories() {
    this.categoriesRepository
      .allAsTree({ status: 1, brand_id: this.brandShow })
      .subscribe(
        (categories: Category[]) => {
          this.categories = categories;
        }
      );
  }
  getProductBycategory(categories?: number) {
    this.categoriesRepository.getProductsByCategory(categories)
      .subscribe(
        (product: Product[]) => {
          this.products = [];
          product.forEach(_p => {
            if (_p.status == true) {
              this.products.push(_p);
            }
          });
          if (this.productClient.length > 0) {
            this.productClient.forEach(_p => {
              this.products.forEach(_p1 => {
                if (_p.id == _p1.id) {
                  let index = this.products.indexOf(_p1);
                  if (index !== -1) {
                    this.products.splice(index, 1);
                  }
                }
              });
            });
          }
        }
      );
  }
  onChangeProduct(categories: number) {
    this.sub_cate = [];
    let o;
    this.subcate_child = 'customer_pr';
    this.categories.forEach(item => {
      if (item.id == this.cate_child) {
        o = item;
      }
    });
    if (o.children.length !== 0) {
      this.products = [];
      o.children.forEach(_i => {
        if (_i.is_activated == 1) {
          this.sub_cate.push(_i);
        }
      });
      this.isShowcate = true;
    } else {
      this.sub_cate = [];
      this.isShowcate = false;
      this.getProductBycategory(o.id);
    }
  }
  checkProduct(item, _index) {
    let index = this.productClient.indexOf(item);
    if (index == -1) {
      this.productClient.push(item);
      this.products.splice(_index, 1);
    } else {
      this.productClient.splice(index, 1);
      this.products.push(item);
    }
    this.messageProduct.emit(this.productClient);
  }
  onChangeProductSub(_item) {
    this.getProductBycategory(this.subcate_child);
  }

}
