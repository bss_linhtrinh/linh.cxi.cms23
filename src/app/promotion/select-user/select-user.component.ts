import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CustomerLevelsRepository } from 'src/app/shared/repositories/customer-levels.repository';

@Component({
  selector: 'app-select-user',
  templateUrl: './select-user.component.html',
  styleUrls: ['./select-user.component.scss']
})
export class SelectUserComponent implements OnInit, OnChanges {
  @Input() dataCustomerLevel: any = [];
  @Input() checkedCustomer: any = [];
  @Input() brand_id: any;
  @Input() changeCustomer;
  is_check_all: boolean = false;
  customerLevel = [];
  customerlevel_req = [];
  brandShow: number;

  @Output() messageCustomerlevel = new EventEmitter();

  constructor(
    private customerlevelRepository: CustomerLevelsRepository,
  ) { }
  ngOnInit() {
    if (this.changeCustomer && this.changeCustomer.loadpayment == true) {
      this.loadCustomerLevel();
    }
  }
  ngOnChanges(simpleChanges: SimpleChanges) {
    this.customerlevel_req = this.checkedCustomer;
    if (simpleChanges.checkedCustomer &&
      simpleChanges.checkedCustomer.currentValue &&
      simpleChanges.checkedCustomer.currentValue !== simpleChanges.checkedCustomer.previousValue) {
      this.customerlevel_req = this.checkedCustomer;
      if (this.customerlevel_req.length > 0 && this.dataCustomerLevel.length > 0) {
        (this.customerlevel_req.length == this.dataCustomerLevel.length) ? this.is_check_all = true : '';
      }
      this.messageCustomerlevel.emit(this.customerlevel_req);
    }
    if (simpleChanges.brand_id && this.brand_id != undefined) {
      this.brandShow = this.brand_id.brand_id;
      this.loadCustomerLevel();
      this.is_check_all = false;
    }
  }
  //load list-customer
  loadCustomerLevel() {
    this.customerlevelRepository.all({ status: 1, brand_id: this.brandShow })
      .subscribe(res => {
        this.dataCustomerLevel = res;
      });
  }
  //checkall customerlevel
  checkCustomerLevelAll() {
    let vm = this;
    if (!vm.is_check_all) {
      vm.dataCustomerLevel.forEach(_item => {
        let index = this.customerlevel_req.indexOf(_item);
        if (index == -1) {
          this.customerlevel_req.push(_item);
        }
      });
    } else {
      for (var i = 0; i < vm.customerlevel_req.length; i++) {
        for (var j = 0; j < vm.dataCustomerLevel.length; j++) {
          vm.customerlevel_req = [];
        }
      }
    }
    this.messageCustomerlevel.emit(this.customerlevel_req);
  }
  //checkcustomer
  checkCustomerLevel(item?: string) {
    let index = this.customerlevel_req.indexOf(item);
    if (index == -1) {
      this.customerlevel_req.push(item);
    } else {
      this.is_check_all = false;
      this.customerlevel_req.splice(index, 1);
    }
    (this.customerlevel_req.length == this.dataCustomerLevel.length) ? this.is_check_all = true : '';
    this.messageCustomerlevel.emit(this.customerlevel_req);
  }

}
