import {Component, OnInit} from '@angular/core';
import {CxiGridColumn} from '../../shared/components/cxi-grid/cxi-grid-column';
import {TaskRepository} from '../../shared/repositories/task.repository';
import {ActivatedRoute, Router} from '@angular/router';
import {Task} from '../../shared/entities/task';
import {ColumnFilterType} from '../../shared/types/column-filter-type';
import {ColumnFormat} from '../../shared/types/column-format';

@Component({
    selector: 'app-tasks',
    templateUrl: './tasks.component.html',
    styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

    cxiTableConfig = {
        paging: true,
        sorting: true,
        filtering: true,
        pagingServer: true,
        translatable: true,
        selecting: true,
        repository: this.taskRepository,
        actions: {
            onEdit: (row: any) => {
                this.onEdit(row);
            },
            onDelete: (row: any) => {
                this.onDelete(row);
            },
            onActivate: (row: any) => {
                this.onActivate(row);
            },
            onDeactivate: (row: any) => {
                this.onDeactivate(row);
            },
        },
    };
    cxiTableColumns = CxiGridColumn.from([
        {title: 'Name', name: 'name', sort: true, className: 'pageName'},
        {title: 'Description', name: 'description', sort: true, className: 'title'},
        {
            title: 'Created date',
            name: 'updatedAt',
            sort: true,
            className: 'content',
            filterType: ColumnFilterType.Datepicker,
            format: ColumnFormat.Date
        },
    ]);

    constructor(private taskRepository: TaskRepository,
                private router: Router,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
    }


    onResize(event) {
        const divWidth = document.getElementById('list-body').offsetWidth;
        console.log(divWidth);
    }


    onEdit(row) {
        this.router.navigate(['..', row.id, 'edit'], {relativeTo: this.route});
    }

    onDelete(task: Task) {
        const data = [task];
        this.taskRepository.destroyMultiple(data)
            .subscribe();
    }

    onActivate(task: Task) {
        this.taskRepository.changeStatus(task, {active: true})
            .subscribe();
    }

    onDeactivate(task: Task) {
        this.taskRepository.changeStatus(task, {active: false})
            .subscribe();
    }

}
