import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TasksRoutingModule} from './tasks-routing.module';
import {TasksComponent} from './tasks.component';
import {EditTaskComponent} from './edit-task/edit-task.component';
import {CreateTaskComponent} from './create-task/create-task.component';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule} from '@angular/forms';


@NgModule({
    declarations: [
        TasksComponent,
        EditTaskComponent,
        CreateTaskComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        TasksRoutingModule
    ]
})
export class TasksModule {
}
