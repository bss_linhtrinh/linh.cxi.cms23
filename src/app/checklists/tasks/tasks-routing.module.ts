import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CheckPermissionsGuard} from '../../shared/guards/check-permissions/check-permissions.guard';
import {TasksComponent} from './tasks.component';
import {CreateTaskComponent} from './create-task/create-task.component';
import {EditTaskComponent} from './edit-task/edit-task.component';


const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: 'list', component: TasksComponent, data: {breadcrumb: 'List tasks'}},
            {path: 'create', component: CreateTaskComponent, data: {breadcrumb: 'Create task'}},
            {path: ':id/edit', component: EditTaskComponent, data: {breadcrumb: 'Edit task'}},
            {path: '', redirectTo: 'list', pathMatch: 'full'},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TasksRoutingModule {
}
