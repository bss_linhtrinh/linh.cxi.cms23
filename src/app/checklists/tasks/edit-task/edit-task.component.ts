import {Component, OnInit} from '@angular/core';
import {Task} from '../../../shared/entities/task';
import {TaskRepository} from '../../../shared/repositories/task.repository';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-edit-task',
    templateUrl: './edit-task.component.html',
    styleUrls: ['./edit-task.component.scss']
})
export class EditTaskComponent implements OnInit {

    task: Task;
    tab: any;


    constructor(private taskRepository: TaskRepository,
                private router: Router,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.loadTask();
    }

    loadTask() {
        this.activatedRoute.params.subscribe((data) => {
            const id = data.id;
            this.taskRepository.find(id).subscribe((res) => {
                this.task = res;
            });
        });
    }

    onSubmit() {
        this.taskRepository.save(this.task)
            .subscribe(
                () => this.back()
            );
    }

    back() {
        this.router.navigate(['../..'], {relativeTo: this.activatedRoute});
    }

}
