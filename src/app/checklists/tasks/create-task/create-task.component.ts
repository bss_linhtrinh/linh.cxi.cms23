import {AfterViewInit, Component, ElementRef, HostListener, OnDestroy, OnInit} from '@angular/core';
import {Task} from '../../../shared/entities/task';
import {TaskRepository} from '../../../shared/repositories/task.repository';
import {ActivatedRoute, Router} from '@angular/router';
import {LocalStorage} from 'ngx-webstorage';

@Component({
    selector: 'app-create-task',
    templateUrl: './create-task.component.html',
    styleUrls: ['./create-task.component.scss']
})
export class CreateTaskComponent implements OnInit {

    @LocalStorage('scope.brand_id') brandId: any;
    @LocalStorage('scope.outlet_id') outletId: any;

    task: Task;
    tab: any;



    constructor(private taskRepository: TaskRepository,
                private router: Router,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.initTask();
    }

    initTask() {
        this.task = new Task();
    }

    onSubmit() {
        if (this.brandId && this.outletId) {
            this.task.brandId = this.brandId;
            this.task.outletId = [this.outletId];
        }
        this.taskRepository.save(this.task)
            .subscribe(
                () => this.back()
            );
    }
    back() {
        this.router.navigate(['..'], {relativeTo: this.activatedRoute});
    }

}
