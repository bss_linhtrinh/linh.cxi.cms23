import {Component, OnInit} from '@angular/core';
import {Checklist} from '../../../shared/entities/checklist';
import {Task} from '../../../shared/entities/task';
import {ShiftsRepository} from '../../../shared/repositories/shifts.repositories';
import {TaskRepository} from '../../../shared/repositories/task.repository';
import {CheckListRepository} from '../../../shared/repositories/check-list.repository';
import {ActivatedRoute, Router} from '@angular/router';
import * as _ from 'lodash';
import {LocalStorage} from 'ngx-webstorage';

@Component({
    selector: 'app-edit-checklist',
    templateUrl: './edit-checklist.component.html',
    styleUrls: ['./edit-checklist.component.scss']
})
export class EditChecklistComponent implements OnInit {

    check_list: Checklist = new Checklist();
    task: Task = new Task();
    estimate = '';
    minDate = new Date();
    shift = [];
    repeatTypes = [
        {value: 'none', name: 'None'},
        {value: 'daily', name: 'Daily'},
        {value: 'weekly', name: 'Weekly'},
        {value: 'monthly', name: 'Monthly'},
    ];

    taskList = [];
    tab: string = 'en';

    constructor(private shiftsRepository: ShiftsRepository,
                private taskRepository: TaskRepository,
                private checklistRepository: CheckListRepository,
                private router: Router,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.getChecklist();
        this.getTaskList();
        this.getShiftList();
    }

    handleTab() {
        this.getTaskList();
        this.getShiftList();
    }

    getChecklist() {
        this.activatedRoute.params.subscribe((data) => {
            const id = data.id;
            this.checklistRepository.find(id).subscribe((res) => {
                this.check_list = res;
                this.estimate = res.estimatedEndTime;
            });
        });
    }

    getTaskList() {
        let arr = [];
        this.taskRepository.all()
            .subscribe(
                (res) => {
                    res.map(item => {
                        if (item.active === true) {
                            arr.push({id: item._id, name: item.name});
                        }
                    });
                    this.taskList = _.cloneDeep(arr);
                }
            );
    }

    getShiftList() {
        let arr = [];
        this.shiftsRepository.all()
            .subscribe(
                (res) => {
                    res.map(item => {
                        if (item.active === true) {
                            arr.push({id: item.id, name: item.name});
                        }
                    });
                    this.checkExistShift(arr);
                }
            );
    }

    checkExistShift(arr) {
        if (!_.find(arr, {id: this.check_list.shift})) {
            this.check_list.shift = arr[0].id;
        }
        this.shift = _.cloneDeep(arr);
    }


    onSubmit() {
        this.checklistRepository.save(this.check_list)
            .subscribe(
                () => this.back()
            );
    }

    back() {
        this.router.navigate(['../..'], {relativeTo: this.activatedRoute});
    }

    handleCheckboxChange(task) {
        if (this.tab === 'en') {
            this.task.name_en = task;
        }
        if (this.tab === 'vi') {
            this.task.name_vi = task;
        }
        this.task.outletId = this.check_list.outletId;
        this.task.brandId = this.check_list.brandId;
        this.createTask(this.task);
    }

    createTask(task) {
        let arr = [];
        arr = _.cloneDeep(this.check_list.tasks);
        this.taskRepository.save(task)
            .subscribe(
                (res) => {
                    arr.push(res._id);
                    this.check_list.tasks = _.cloneDeep(arr);
                    this.getTaskList();
                }
            );
    }

    onEndAfterChange() {
        this.estimateTime();
    }

    estimateTime() {
        let params = {
            startDate: this.check_list.startDate,
            repeatType: this.check_list.repeatType,
            endAfter: this.check_list.endAfter,
            days: this.check_list.days
        };
        if (this.check_list.endAfter) {
            this.checklistRepository.estimateTime(params)
                .subscribe(
                    (res) => {
                        this.estimate = res.estimatedEndTime;
                    }
                );
        }
    }

    onHandleChange() {
        if (this.check_list.repeatType === 'none') {
            this.check_list.endAfter = 0;
            this.estimate = '';
        }
        if (this.check_list.repeatType !== 'weekly') {
            this.check_list.days = [];
            this.estimateTime();
        } else {
            this.estimateTime();
        }
    }

    isDisableEndAfter() {
        return !this.check_list.startDate || this.check_list.repeatType === 'none';
    }


}
