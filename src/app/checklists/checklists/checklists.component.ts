import {Component, OnInit} from '@angular/core';
import {CxiGridColumn} from '../../shared/components/cxi-grid/cxi-grid-column';
import {Checklist} from '../../shared/entities/checklist';
import {CheckListRepository} from '../../shared/repositories/check-list.repository';
import {Task} from '../../shared/entities/task';
import {ActivatedRoute, Router} from '@angular/router';
import {Shift} from '../../schedules/shift';
import {ColumnFilterType} from '../../shared/types/column-filter-type';
import {ColumnFormat} from '../../shared/types/column-format';

@Component({
    selector: 'app-checklists',
    templateUrl: './checklists.component.html',
    styleUrls: ['./checklists.component.scss']
})
export class ChecklistsComponent implements OnInit {

    cxiTableConfig = {
        paging: true,
        sorting: true,
        filtering: true,
        pagingServer: true,
        selecting: true,
        translatable: true,
        repository: this.checklistRepository,
        actions: {
            onEdit: (row: any) => {
                this.onEdit(row);
            },
            onDelete: (row: any) => {
                this.onDelete(row);
            },
            onActivate: (row: any) => {
                this.onActivate(row);
            },
            onDeactivate: (row: any) => {
                this.onDeactivate(row);
            },
        },
    };
    cxiTableColumns = CxiGridColumn.from([
        {title: 'Name', name: 'name', sort: true, className: 'pageName'},
        {title: 'Task total', name: 'taskTotal', sort: true, className: 'title'},
        {
            title: 'Created date',
            name: 'updatedAt',
            sort: true,
            className: 'content',
            filterType: ColumnFilterType.Datepicker,
            format: ColumnFormat.Date
        },
    ]);

    constructor(private checklistRepository: CheckListRepository,
                private router: Router,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
    }

    onEdit(row) {
        this.router.navigate(['..', row.id, 'edit'], {relativeTo: this.route});
    }

    onDelete(checkList: Checklist) {
        const data = [checkList];
        this.checklistRepository.destroyMultiple(data)
            .subscribe();
    }

    onActivate(checkList: Checklist) {
        this.checklistRepository.changeStatus(checkList, {active: true})
            .subscribe();
    }

    onDeactivate(checkList: Checklist) {
        this.checklistRepository.changeStatus(checkList, {active: false})
            .subscribe();
    }


}
