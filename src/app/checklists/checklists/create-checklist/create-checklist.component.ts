import {Component, OnInit} from '@angular/core';
import {Checklist} from '../../../shared/entities/checklist';
import {ShiftsRepository} from '../../../shared/repositories/shifts.repositories';
import {TaskRepository} from '../../../shared/repositories/task.repository';
import {Task} from '../../../shared/entities/task';
import {CheckListRepository} from '../../../shared/repositories/check-list.repository';
import {ActivatedRoute, Router} from '@angular/router';
import {LocalStorage} from 'ngx-webstorage';
import * as _ from 'lodash';

@Component({
    selector: 'app-create-checklist',
    templateUrl: './create-checklist.component.html',
    styleUrls: ['./create-checklist.component.scss']
})
export class CreateChecklistComponent implements OnInit {

    @LocalStorage('scope.brand_id') brandId: any;
    @LocalStorage('scope.outlet_id') outletId: any;

    check_list: Checklist = new Checklist();
    task: Task = new Task();
    estimate = new Date().toString();
    taskName = '';
    minDate = new Date();
    shift = [];
    repeatTypes = [
        {value: 'none', name: 'None'},
        {value: 'daily', name: 'Daily'},
        {value: 'weekly', name: 'Weekly'},
        {value: 'monthly', name: 'Monthly'},
    ];

    taskList = [];
    tab: any;


    constructor(private shiftsRepository: ShiftsRepository,
                private taskRepository: TaskRepository,
                private checklistRepository: CheckListRepository,
                private router: Router,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.getTaskList();
        this.getShiftList();
    }

    handleTab() {
        this.getTaskList();
        this.getShiftList();
    }

    getTaskList() {
        let arr = [];
        this.taskRepository.all()
            .subscribe(
                (res) => {
                    res.map(item => {
                        if (item.active === true) {
                            arr.push({id: item._id, name: item.name});
                        }
                    });
                    this.taskList = _.cloneDeep(arr);
                }
            );
    }

    getShiftList() {
        let arr = [];
        this.shiftsRepository.all()
            .subscribe(
                (res) => {
                    res.map(item => {
                        if (item.active === true) {
                            arr.push({id: item.id, name: item.name});
                        }
                    });
                    this.shift = _.cloneDeep(arr);
                    this.check_list.shift = this.shift[0].id;
                }
            );
    }

    onSubmit() {
        if (this.brandId && this.outletId) {
            this.check_list.brandId = this.brandId;
            this.check_list.outletId = [this.outletId];
        }
        this.checklistRepository.save(this.check_list)
            .subscribe(
                () => this.back()
            );
    }

    back() {
        this.router.navigate(['..'], {relativeTo: this.activatedRoute});
    }

    handleCheckboxChange(task) {
        let translations = _.cloneDeep(this.task.translations);
        translations.forEach(item => {
            item.name = task;
        });
        this.task.translations = _.cloneDeep(translations);
        this.task.outletId = [this.outletId];
        this.task.brandId = this.brandId;
        this.createTask(this.task);
    }

    createTask(task) {
        let arr = [];
        arr = _.cloneDeep(this.check_list.tasks);
        this.taskRepository.save(task)
            .subscribe(
                (res) => {
                    arr.push(res._id);
                    this.getTaskList();
                    this.check_list.tasks = _.cloneDeep(arr);
                }
            );
    }

    onEndAfterChange() {
        this.estimateTime();
    }

    estimateTime() {
        let params = {
            startDate: this.check_list.startDate,
            repeatType: this.check_list.repeatType,
            endAfter: this.check_list.endAfter,
            days: this.check_list.days
        };
        if (this.check_list.repeatType !== 'none') {
            this.checklistRepository.estimateTime(params)
                .subscribe(
                    (res) => {
                        this.estimate = res.estimatedEndTime;
                    },
                    (err) => {
                        this.estimate = this.check_list.startDate;
                    }
                );
        }
    }

    onHandleChange() {
        if (this.check_list.repeatType === 'none') {
            this.check_list.endAfter = 0;
            this.estimate = this.check_list.startDate;
        }
        if (this.check_list.repeatType !== 'weekly') {
            this.check_list.days = [];
            this.estimateTime();
        } else {
            this.estimateTime();
        }
    }

    isDisableEndAfter() {
        return !this.check_list.startDate || this.check_list.repeatType === 'none';
    }
}
