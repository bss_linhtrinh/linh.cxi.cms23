import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CheckPermissionsGuard} from '../shared/guards/check-permissions/check-permissions.guard';
import {ChecklistsComponent} from './checklists/checklists.component';
import {CreateChecklistComponent} from './checklists/create-checklist/create-checklist.component';
import {EditChecklistComponent} from './checklists/edit-checklist/edit-checklist.component';


const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: 'list', component: ChecklistsComponent, data: {breadcrumb: 'List checklists'}},
            {path: 'create', component: CreateChecklistComponent, data: {breadcrumb: 'Create checklist'}},
            {path: ':id/edit', component: EditChecklistComponent, data: {breadcrumb: 'Edit checklist'}},
            {path: '', redirectTo: 'list', pathMatch: 'full'},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ChecklistsRoutingModule {
}
