import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChecklistsRoutingModule} from './checklists-routing.module';
import {ChecklistsComponent} from './checklists/checklists.component';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {CreateChecklistComponent} from './checklists/create-checklist/create-checklist.component';
import {EditChecklistComponent} from './checklists/edit-checklist/edit-checklist.component';
import {SchedulesModule} from '../schedules/schedules.module';

@NgModule({
    declarations: [
        ChecklistsComponent,
        CreateChecklistComponent,
        EditChecklistComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        SchedulesModule,
        ChecklistsRoutingModule
    ]
})
export class ChecklistsModule {
}
