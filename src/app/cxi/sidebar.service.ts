import {Injectable} from '@angular/core';
import { Subject} from 'rxjs';
import {StorageService} from '../shared/services/storage/storage.service';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class SidebarService {
    private isShow = true;

    // …other properties…
    private changeStatusSource = new Subject<boolean>();
    // private changeModuleSource = new Subject<Modules>();
    public changeStatus$ = this.changeStatusSource.asObservable();

    // public changeModule$ = this.changeModuleSource.asObservable();

    constructor(
    ) {
    }

    show(): void {
        this.isShow = true;
        this.changeStatusSource.next(true);
    }

    hide(): void {
        this.isShow = false;
        this.changeStatusSource.next(false);
    }

    toggle(): void {
        this.isShow = !this.isShow;
        this.changeStatusSource.next(this.isShow);
    }
}
