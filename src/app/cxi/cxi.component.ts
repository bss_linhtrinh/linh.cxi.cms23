import { Component, OnDestroy, OnInit } from '@angular/core';
import { SidebarService } from './sidebar.service';
import { Subscription } from 'rxjs';
import { delay, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { CurrenciesRepository } from '../shared/repositories/currencies.repository';
import { Currency } from '../shared/entities/currency';
import { SessionStorage } from 'ngx-webstorage';
import { AuthService } from '../shared/services/auth/auth.service';
import { Scope } from '../shared/types/scopes';
import { Language } from '../shared/entities/language';
import { LANGUAGES } from '../shared/constants/languages';

@Component({
    selector: 'app-cxi',
    templateUrl: './cxi.component.html',
    styleUrls: ['./cxi.component.scss']
})

export class CxiComponent implements OnInit, OnDestroy {
    isShowSidebar = true;
    subscriptions: Subscription[] = [];

    @SessionStorage('currencies') currencies: Currency[];
    @SessionStorage('languages') languages: Language[];

    constructor(
        private router: Router,
        private sidebarService: SidebarService,
        private authService: AuthService,
        private currenciesRepository: CurrenciesRepository
    ) {
        const sidebarSubscription = this.sidebarService.changeStatus$
            .pipe(
                delay(100)
            )
            .subscribe(
                (isShowSidebar: boolean) => {
                    this.isShowSidebar = isShowSidebar;
                }
            );
        this.subscriptions.push(sidebarSubscription);
    }

    ngOnInit() {
        this.loadData();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    loadData() {
        this.getCurrencies();
        this.getLanguages();
    }

    getCurrencies() {
        const userScope = this.authService.getCurrentScope();
        switch (userScope) {
            case Scope.Organization:
            case Scope.Brand:
            case Scope.Outlet:
                this.currenciesRepository.all()
                    .pipe(tap((currencies: Currency[]) => this.currencies = currencies))
                    .subscribe();
                break;

            default:
                break;
        }
    }

    getLanguages() {
        this.languages = LANGUAGES.map(l => Language.create(l));
    }
}
