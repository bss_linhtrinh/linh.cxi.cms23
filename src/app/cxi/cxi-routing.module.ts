import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CxiComponent } from './cxi.component';
import { AuthResolver } from '../auth.resolver';
import { HomeComponent } from './home/home.component';
import { ProviderResolver } from '../shared/resolvers/provider/provider.resolver';
import { CurrentUserResolver } from '../shared/resolvers/current-user/current-user.resolver';
import { HasModulesGuard } from '../shared/guards/has-modules/has-modules.guard';

const routes: Routes = [
    { path: 'home', component: HomeComponent, resolve: { provider: ProviderResolver } },
    {
        path: '',
        component: CxiComponent,
        resolve: { auth: AuthResolver },
        canActivate: [HasModulesGuard],
        children: [
            { path: '', redirectTo: '/home', pathMatch: 'full' },
            {
                path: '',
                loadChildren: () => import('../organization-management/organization-management.module').then(mod => mod.OrganizationManagementModule),
                data: { breadcrumb: 'CXI' }
            },
            {
                path: '',
                loadChildren: () => import('../sale-management/sale-management.module').then(mod => mod.SaleManagementModule),
                data: { breadcrumb: 'CXI' }
            },
            {
                path: 'profile',
                loadChildren: () => import('../profile/profile.module').then(mod => mod.ProfileModule)
            },
            {
                path: 'filter',
                loadChildren: () => import('../filter/filter.module').then(mod => mod.FilterModule),
            },

            // org - brand - outlet
            {
                path: 'organizations',
                loadChildren: () => import('../organization/organization.module').then(mod => mod.OrganizationModule),
                data: { breadcrumb: 'Organizations' }
            },
            {
                path: 'brands',
                loadChildren: () => import('../brand/brand.module').then(mod => mod.BrandModule),
                data: { breadcrumb: 'Brands' }
            },
            {
                path: 'outlets',
                loadChildren: () => import('../outlet/outlet.module').then(mod => mod.OutletModule),
                data: { breadcrumb: 'Outlets' }
            },

            // users - roles - permissions
            {
                path: 'users',
                loadChildren: () => import('../user/users/user.module').then(mod => mod.UserModule),
                resolve: { currentUser: CurrentUserResolver },
                data: { breadcrumb: 'Users' }
            },
            {
                path: 'roles',
                loadChildren: () => import('../user/roles/roles.module').then(mod => mod.RolesModule),
                data: { breadcrumb: 'Users' }
            },
            {
                path: 'permissions',
                loadChildren: () => import('../user/permissions/permissions.module').then(mod => mod.PermissionsModule),
                data: { breadcrumb: 'Users' }
            },

            // products
            {
                path: 'categories',
                loadChildren: () => import('../product/categories/categories.module').then(mod => mod.CategoriesModule),
                data: { breadcrumb: 'Products' }
            },
            {
                path: 'products',
                loadChildren: () => import('../product/products/products.module').then(mod => mod.ProductsModule),
                data: { breadcrumb: 'Products' }
            },
            {
                path: 'attributes',
                loadChildren: () => import('../product/attributes/attributes.module').then(mod => mod.AttributesModule),
                data: { breadcrumb: 'Products' }
            },
            {
                path: 'product-add-ons',
                loadChildren: () => import('../product/product-addons/product-addons.module').then(mod => mod.ProductAddonsModule),
                data: { breadcrumb: 'Products' }
            },
            {
                path: 'product-add-on-type',
                loadChildren: () => import('../product/product-addon-type/product-addon-type.module').then(mod => mod.ProductAddonTypeModule),
                data: { breadcrumb: 'Products' }
            },

            // customer
            {
                path: 'customer',
                loadChildren: () => import('../customers/customers.module').then(mod => mod.CustomersModule),
                data: { breadcrumb: 'Customers' }
            },
            // customer-type
            {
                path: 'customer-type',
                loadChildren: () => import('../customer-type/customer-type.module').then(mod => mod.CustomerTypeModule),
                data: { breadcrumb: 'Customer type' }
            },
            // hobby
            {
                path: 'hobby',
                loadChildren: () => import('../hobby/hobby.module').then(mod => mod.HobbyModule),
                data: { breadcrumb: 'Hobby' }
            },
            // case-comment-template
            {
                path: 'case-comment-template',
                loadChildren: () => import('../case-comments/case-comments.module').then(mod => mod.CaseCommentsModule),
                data: { breadcrumb: 'Comment case template' }
            },

            // orders
            {
                path: 'orders',
                loadChildren: () => import('../orders/orders.module').then(mod => mod.OrdersModule),
                data: { breadcrumb: 'Orders' }
            },

            // promotion, gifts, gift-categories
            {
                path: 'promotion',
                loadChildren: () => import('../promotion/promotion.module').then(mod => mod.PromotionModule),
                data: { breadcrumb: 'Promotion' }
            },
            {
                path: 'gift',
                loadChildren: () => import('../gifts/gifts.module').then(mod => mod.GiftsModule),
                data: { breadcrumb: 'Gift' }
            },
            {
                path: 'gift-categories',
                loadChildren: () => import('../gift-categories/gift-categories.module').then(mod => mod.GiftCategoriesModule),
                data: { breadcrumb: 'Gift' }
            },

            // vouchers
            {
                path: 'vouchers',
                loadChildren: () => import('../vouchers/vouchers.module').then(mod => mod.VouchersModule),
                data: {breadcrumb: 'Vouchers'}
            },

            // loyalty
            {
                path: 'loyalty-program',
                loadChildren: () => import('../loyalty/loyalty.module').then(mod => mod.LoyaltyModule),
                data: { breadcrumb: 'Loyalty Programs' }
            },

            // user-app
            {
                path: 'user-app',
                loadChildren: () => import('../user-app/user-app.module').then(mod => mod.UserAppModule),
                data: { breadcrumb: '' }
            },


            // tables, table-category, floors, seat-map
            {
                path: 'tables',
                loadChildren: () => import('../seat/tables/tables.module').then(mod => mod.TablesModule),
                data: { breadcrumb: 'Tables' }
            },
            {
                path: 'table-categories',
                loadChildren: () => import('../seat/table-categories/table-categories.module').then(mod => mod.TableCategoriesModule),
                data: { breadcrumb: 'Table-categories' }
            },
            {
                path: 'floors',
                loadChildren: () => import('../seat/floors/floors.module').then(mod => mod.FloorsModule),
                data: { breadcrumb: 'Floors' }
            },
            {
                path: 'seat-map',
                loadChildren: () => import('../seat/seat-map/seat-map.module').then(mod => mod.SeatMapModule),
                data: { breadcrumb: '' }
            },

            // case management
            {
                path: 'checklists',
                loadChildren: () => import('../checklists/checklists.module').then(mod => mod.ChecklistsModule),
                data: { breadcrumb: 'Checklists' }
            },

            {
                path: 'tasks',
                loadChildren: () => import('../checklists/tasks/tasks.module').then(mod => mod.TasksModule),
                data: { breadcrumb: 'Tasks' }
            },

            {
                path: 'case-management',
                loadChildren: () => import('../cases/cases.module').then(mod => mod.CasesModule),
                data: { breadcrumb: 'Case' }
            },
            {
                path: 'reminders',
                loadChildren: () => import('../reminders/reminders.module').then(mod => mod.RemindersModule),
                data: { breadcrumb: 'Reminder' }
            },
            //backlist
            {
                path: 'backlist-customer',
                loadChildren: () => import('../customer-backlist/customer-backlist.module').then(mod => mod.CustomerBacklistModule),
                data: { breadcrumb: 'Backlist Customer' }
            },
            {
                path: 'schedules',
                loadChildren: () => import('../schedules/schedules.module').then(mod => mod.SchedulesModule),
                data: { breadcrumb: 'Schedules' }
            },
            {
                path: 'resource',
                loadChildren: () => import('../resource/resource.module').then(mod => mod.ResourceModule),
                data: {breadcrumb: 'Content Management'}
            },

            // HealthHub
            {
                path: 'specialist',
                loadChildren: () => import('../specialist/specialist.module').then(mod => mod.SpecialistModule),
                data: {breadcrumb: 'Specialist'}
            },
            {
                path: 'doctor',
                loadChildren: () => import('../doctor/doctor.module').then(mod => mod.DoctorModule),
                data: {breadcrumb: 'Doctor'}
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CxiRoutingModule {
}
