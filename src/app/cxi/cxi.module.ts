import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CxiRoutingModule} from './cxi-routing.module';
import {CxiComponent} from './cxi.component';
import {HeaderComponent} from './header/header.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {AsideComponent} from './aside/aside.component';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {NgbCollapseModule, NgbDropdownModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BreadcrumbModule} from 'angular-crumbs';
import { CxiBreadcrumbsComponent } from './cxi-breadcrumbs/cxi-breadcrumbs.component';
import { HeaderScopeComponent } from './header/header-scope/header-scope.component';
import { HomeComponent } from './home/home.component';

@NgModule({
    declarations: [
        CxiComponent,
        HeaderComponent,
        SidebarComponent,
        AsideComponent,
        CxiBreadcrumbsComponent,
        HeaderScopeComponent,
        HomeComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        NgbCollapseModule,
        NgbDropdownModule,
        NgbModule,
        BreadcrumbModule,
        SharedModule,
        CxiRoutingModule
    ]
})
export class CxiModule {
}
