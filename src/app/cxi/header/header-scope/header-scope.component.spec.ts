import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderScopeComponent } from './header-scope.component';

describe('HeaderScopeComponent', () => {
  let component: HeaderScopeComponent;
  let fixture: ComponentFixture<HeaderScopeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderScopeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderScopeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
