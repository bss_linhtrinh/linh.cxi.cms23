import {Component, OnInit} from '@angular/core';
import {ScopeService} from '../../../shared/services/scope/scope.service';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {LocalStorage} from 'ngx-webstorage';
import {Scope} from '../../../shared/types/scopes';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import {Organization} from '../../../shared/entities/organization';
import {Brand} from '../../../shared/entities/brand';
import {Outlet} from '../../../shared/entities/outlets';
import {Provider} from '../../../shared/types/providers';

@Component({
    selector: 'app-header-scope',
    templateUrl: './header-scope.component.html',
    styleUrls: ['./header-scope.component.scss']
})
export class HeaderScopeComponent implements OnInit {
    scopeLevel: string;
    scopeName: string;

    @LocalStorage('auth.provider') authProvider: Provider;
    @LocalStorage('scope.organization') scopeOrganization: Organization;
    @LocalStorage('scope.brand') scopeBrand: Brand;
    @LocalStorage('scope.outlet') scopeOutlet: Outlet;
    @LocalStorage('scope.current') scopeCurrent: Scope;

    constructor(
        private router: Router,
        private scopeService: ScopeService,
        private translateService: TranslateService,
        private utilitiesService: UtilitiesService,
    ) {
    }

    ngOnInit() {
    }

    filter() {
        this.router.navigate(['/filter']);
    }

    getScopeLevel() {
        const currentScope = this.utilitiesService.singularize(this.scopeCurrent ? this.scopeCurrent : '');
        const labelScope = this.utilitiesService.capitalize(currentScope);

        this.scopeLevel =
            (
                this.authProvider === Provider.SuperAdmin &&
                (
                    this.scopeCurrent !== Scope.Organization &&
                    this.scopeCurrent !== Scope.Brand &&
                    this.scopeCurrent !== Scope.Outlet
                )
            )
                ? `scope.Scope`
                : `scope.${labelScope}`;

        return this.scopeLevel;
    }

    getScopeName() {

        const currentScope = this.utilitiesService.singularize(this.scopeCurrent ? this.scopeCurrent : '');
        const labelScope = this.utilitiesService.capitalize(currentScope);

        this.scopeName =
            (
                this.authProvider === Provider.SuperAdmin &&
                (
                    this.scopeCurrent !== Scope.Organization &&
                    this.scopeCurrent !== Scope.Brand &&
                    this.scopeCurrent !== Scope.Outlet
                )
            )
                ? `Super Admin`
                : (
                    this['scope' + labelScope]
                        ? this['scope' + labelScope].name
                        : null
                );

        return this.scopeName;
    }
}
