import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UsersRepository} from 'src/app/shared/repositories/users.repository';
import {User} from 'src/app/shared/entities/user';
import {AuthService} from '../../shared/services/auth/auth.service';
import {tap} from 'rxjs/operators';
import {NotificationsRepository} from 'src/app/shared/repositories/notifications.repository';
import {Notification} from 'src/app/shared/entities/notification';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    isUserManager = true;
    searchStr = '';
    user: User;
    notification: Notification[] = [];

    constructor(
        private authService: AuthService,
        private router: Router,
        private userRepository: UsersRepository,
        private notificationRepository: NotificationsRepository,
    ) {
        this.userRepository.changeProfile$
            .subscribe((user: User) => this.user = user);
    }

    ngOnInit() {
        this.user = this.authService.currentUser();
        //this.loadNotification();
    }

    errorHandler(event) {
        event.target.src = 'assets/img/avatar-default.jpg';
    }

    signOut() {
        this.authService.signOut()
            .pipe(
                tap(
                    () => this.router.navigateByUrl(`/auth/login`)
                )
            )
            .subscribe();
    }

    loadNotification() {
        this.notificationRepository.all()
            .subscribe((res?: any) => {
                this.notification = res;
            });
    }
}
