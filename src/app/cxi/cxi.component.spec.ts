import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiComponent } from './cxi.component';

describe('CxiComponent', () => {
  let component: CxiComponent;
  let fixture: ComponentFixture<CxiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
