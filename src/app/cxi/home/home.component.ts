import {Component, OnInit} from '@angular/core';
import {Provider} from '../../shared/types/providers';
import {Router} from '@angular/router';
import {Scope} from '../../shared/types/scopes';
import {AuthService} from '../../shared/services/auth/auth.service';
import {SettingsRepository} from '../../shared/repositories/settings.repository';
import {UserScope} from '../../shared/entities/user-scope';
import {ScopeService} from '../../shared/services/scope/scope.service';
import {NEVER} from 'rxjs';
import {switchMap} from 'rxjs/operators';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    constructor(
        private router: Router,
        private authService: AuthService,
        private scopeService: ScopeService,
        private settingsRepository: SettingsRepository
    ) {
    }

    ngOnInit() {
        this.redirect();
        this.getData();
    }

    redirect() {
        const currentScope = this.authService.getCurrentScope();
        const provider = this.authService.getCurrentProvider();

        switch (provider) {
            case Provider.SuperAdmin:
                // Switch by current Scope
                switch (currentScope) {
                    case Scope.Organization:
                        this.router.navigate([`/users`]);
                        break;

                    case Scope.Brand:
                    case Scope.Outlet:
                        this.router.navigate([`/users`]);
                        break;

                    case Scope.BSS:
                        this.router.navigate([`/users`]);
                        break;

                    default:
                        this.router.navigate([`/auth/login`]);
                        break;
                }
                break;

            case Provider.Admin:
                // Switch by current Scope
                switch (currentScope) {
                    case Scope.Organization:
                        this.router.navigate([`/users`]);
                        break;

                    case Scope.Brand:
                    case Scope.Outlet:
                        this.router.navigate([`/users`]);
                        break;

                    default:
                        this.router.navigate([`/auth/login`]);
                        break;
                }
                break;

            default:
                this.router.navigate([`/home`]);
                break;
        }

    }

    getData() {
        this.getBrandScope();
        this.getSettings();
    }

    getBrandScope() {
        this.authService.getCurrentBrandScope();
    }

    getSettings() {
        this.scopeService.getUserScope$()
            .pipe(
                switchMap(
                    (userScope: UserScope) => {
                        if (userScope) {
                            if (userScope.outlet && userScope.outlet.id) {
                                return this.settingsRepository.all({scope: 'outlets', outlet_id: userScope.outlet.id});
                            } else if (userScope.brand && userScope.brand.id) {
                                return this.settingsRepository.all({scope: 'brands', brand_id: userScope.brand.id});
                            } else if (userScope.organization && userScope.organization.id) {
                                return this.settingsRepository.all({scope: 'organizations', organization_id: userScope.organization.id});
                            } else {
                                return NEVER;
                            }
                        }
                    }
                )
            )
            .subscribe();
    }
}
