import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CxiBreadcrumbsComponent } from './cxi-breadcrumbs.component';

describe('CxiBreadcrumbsComponent', () => {
  let component: CxiBreadcrumbsComponent;
  let fixture: ComponentFixture<CxiBreadcrumbsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CxiBreadcrumbsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CxiBreadcrumbsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
