import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {filter, map, tap} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'cxi-breadcrumbs',
    templateUrl: './cxi-breadcrumbs.component.html',
    styleUrls: ['./cxi-breadcrumbs.component.scss']
})
export class CxiBreadcrumbsComponent implements OnInit {
    exceptRoutes: any[] = [
        '/filter',
        '/dashboard'
    ];
    isActive: boolean;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private translateService: TranslateService
    ) {
        this.router.events
            .pipe(
                filter(event => event instanceof NavigationEnd),
                tap(() => this.translate())
            )
            .subscribe(
            );
    }

    ngOnInit() {
        this.subscribeEventsNavigation();

        //
        // this.translate();
    }

    subscribeEventsNavigation() {
        this.isActive = !this.exceptRoutes.includes(this.router.url);

        this.router.events
            .pipe(
                filter(
                    event => event instanceof NavigationEnd
                ),
                map((event: NavigationEnd) => {
                    return event.urlAfterRedirects;
                }),
                map((url: string) => {
                    return !this.exceptRoutes.includes(url);
                }),

            )
            .subscribe(
                (isActive: boolean) => this.isActive = isActive
            );
    }

    translate() {
        const breadcrumbs = [];
        let currentRoute = this.activatedRoute.root,
            url = '';
        do {
            const childrenRoutes = currentRoute.children;
            currentRoute = null;

            childrenRoutes.forEach(route => {
                if (route.outlet === 'primary') {

                    if (
                        route.snapshot &&
                        route.snapshot.data &&
                        route.snapshot.data.breadcrumb
                    ) {
                        this.translateService.get(`breadcrumb.${route.snapshot.data.breadcrumb}`)
                            .subscribe(
                                (translatedBreadcrumb: string) => route.snapshot.data.breadcrumb = translatedBreadcrumb
                            );
                    }

                    currentRoute = route;
                }
            });

        } while (currentRoute);
    }
}
