import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {AuthService} from '../../shared/services/auth/auth.service';
import {MenuItem} from '../../shared/entities/menu-item';
import {FilterStorageService} from '../../shared/services/filterStorage/filterStorage.service';
import {ScopeService} from '../../shared/services/scope/scope.service';
import {UserScope} from '../../shared/entities/user-scope';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, of, Subscription} from 'rxjs';
import {map, switchMap, tap, toArray} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {fromArray} from 'rxjs/internal/observable/fromArray';
import {UtilitiesService} from '../../shared/services/utilities/utilities.service';
import {ModulesService} from '../../shared/services/modules/modules.service';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {
    isFolded: boolean;
    menuItems: MenuItem[];
    userScope: UserScope;
    scroll: number;

    currentUser: any;

    @Input() show: boolean;
    @Output() toggled = new EventEmitter();

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private authService: AuthService,
        private scopeService: ScopeService,
        private filterService: FilterStorageService,
        private elementRef: ElementRef,
        private translate: TranslateService,
        private utils: UtilitiesService,
        private modulesService: ModulesService
    ) {
        this.currentUser = this.authService.currentUser();

        translate.onLangChange
            .pipe(
                tap(
                    () => this.initMenuItems()
                )
            )
            .subscribe();
    }


    ngOnInit() {
        // this.checkModule();
        this.initMenuItems();
        this.getUserScope();
        this.checkActivateMenuItems();
    }

    ngOnDestroy() {
    }

    initMenuItems() {
        const menuItems = this.modulesService.getMenuItems(this.currentUser, this.filterService.retrieve());

        const clonedMenuItems = this.utils.cloneDeep(menuItems);

        // translate menu item
        this.translateMenuItems$(clonedMenuItems)
            .subscribe(
                (translatedMenuItems: MenuItem[]) => this.menuItems = translatedMenuItems
            )
        ;
    }

    translateMenuItems$(menuItems: MenuItem[]) {
        return fromArray(menuItems)
            .pipe(
                switchMap(
                    (menuItem: MenuItem) => this.translateMenuItem$(menuItem)
                ),
                toArray()
            )
            ;
    }

    translateMenuItem$(menuItem: MenuItem): Observable<MenuItem> {
        menuItem.name = menuItem.name.replace('sidebar.', '');
        const translateKey = `sidebar.${menuItem.name}`;

        return this.translate.get(translateKey)
            .pipe(
                tap((translatedName) => menuItem.name = translatedName),
                switchMap(() => {
                    if (menuItem.children && menuItem.children.length > 0) {
                        return this.translateMenuItems$(menuItem.children)
                            .pipe(
                                map(
                                    (translatedChildrenMenuItems: MenuItem[]) => {
                                        menuItem.children = translatedChildrenMenuItems;
                                        return menuItem;
                                    }
                                )
                            );
                    } else {
                        return of(menuItem);
                    }
                })
            );
    }

    getUserScope() {
        this.scopeService.getUserScope$()
            .subscribe(
                (userScope: UserScope) => {
                    this.userScope = userScope;
                }
            );
    }


    toggle() {
        this.isFolded = !this.isFolded;
        this.toggled.emit(this.isFolded);
    }


    checkActivateMenuItems() {
        this.menuItems.forEach(
            (menuItem: MenuItem) => {
                menuItem.isExpand = this.isActivateChild(menuItem);
            }
        );
    }

    toggleMenuItem(menuItem: MenuItem) {
        menuItem.toggle();
    }

    isCollapse(menuItem: MenuItem) {
        return !menuItem.isExpand;
    }

    isActivateChild(menuItem: MenuItem) {
        let isActivateChild = false;
        if (menuItem.children) {
            menuItem.children.forEach(
                child => {
                    isActivateChild = isActivateChild ||
                        this.router.isActive(child.link, false) ||
                        this.router.url.includes(child.link);
                }
            );
        }

        return isActivateChild;
    }
}
