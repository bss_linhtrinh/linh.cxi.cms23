import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FilterRoutingModule} from './filter-routing.module';
import {FilterComponent} from './filter/filter.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {AuthCurrent} from './auth.current';

@NgModule({
    declarations: [
        FilterComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        FilterRoutingModule
    ],
    providers: [
        AuthCurrent
    ]
})
export class FilterModule {
}
