import { Component, OnInit } from '@angular/core';
import { User } from '../../shared/entities/user';
import { Organization } from '../../shared/entities/organization';
import { Brand } from '../../shared/entities/brand';
import { Outlet } from '../../shared/entities/outlets';
import { Filter } from '../../shared/entities/filter';
import { BaseFilterCriteria } from '../../shared/entities/base-filter-criteria';
import { Provider } from '../../shared/types/providers';
import { Scope } from '../../shared/types/scopes';
import * as _ from 'lodash';
import { Role } from '../../shared/types/roles';
import { ActivatedRoute, Router } from '@angular/router';
import { OrganizationsRepository } from '../../shared/repositories/organizations.repository';
import { BrandsRepository } from '../../shared/repositories/brands.repository';
import { OutletsRepository } from '../../shared/repositories/outlets.repository';
import { FilterStorageService } from '../../shared/services/filterStorage/filterStorage.service';
import { AuthService } from '../../shared/services/auth/auth.service';
import { Levels } from '../../shared/types/levels';
import { environment } from 'src/environments/environment';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { switchMap } from 'rxjs/operators';
import { NEVER } from 'rxjs';
import { SettingsRepository } from '../../shared/repositories/settings.repository';
import { ModulesService } from '../../shared/services/modules/modules.service';

@Component({
    selector: 'cxi-filter',
    templateUrl: './filter.component.html',
    styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
    activeIndexOrganization: number;
    activeIndexBrand: number;
    activeIndexOutlet: number;
    orgName: string;
    brandName: string;
    baseUrl: string = environment.baseUrl;
    userScope: UserScope;

    currentUser: User;
    organizations: Organization[] = [];
    brands: Brand[] = [];
    outlets: Outlet[] = [];

    objectFilterStorage: Filter = new Filter();

    filterAttrs: BaseFilterCriteria[];

    isShowOrganization: boolean;
    isShowBrand: boolean;
    isShowOutlet: boolean;
    isShowsuperadmin: boolean;

    userLevel: Levels;

    constructor(
        private router: Router,
        private organizationsRepository: OrganizationsRepository,
        private brandsRepository: BrandsRepository,
        private outletsRepository: OutletsRepository,
        private routerActive: ActivatedRoute,
        private filterStore: FilterStorageService,
        private authService: AuthService,
        private scopeService: ScopeService,
        private settingsRepository: SettingsRepository,
        private modulesService: ModulesService
    ) {
    }


    ngOnInit() {
        this.getListUserScope();
        this.getUserScope();
        this.filterAttributes();
        this.getData();
    }

    canDeactivate() {
        this.authService.getCurrentBrandScope();
        this.settingsRepository.loadAll()
            .subscribe();

        // modules
        this.modulesService.loadMenuItems()
            .subscribe();

        return true;
    }

    getUserScope() {
        this.userLevel = this.authService.getCurrentLevel(false);
        switch (this.userLevel) {
            case Levels.BSS:
                this.isShowOrganization = false;
                this.isShowBrand = false;
                this.isShowOutlet = false;
                break;
            case Levels.Organization:
                this.isShowOrganization = true;
                this.isShowBrand = false;
                this.isShowOutlet = false;
                break;
            case Levels.Brand:
                this.isShowOrganization = false;
                this.isShowBrand = true;
                this.isShowOutlet = false;
                this.orgName = this.userScope.organization.name;
                this.getAllBrands();
                break;
            case Levels.Outlet: // AUTO SKIP FILTER
                this.isShowOrganization = false;
                this.isShowBrand = false;
                this.isShowOutlet = true;
                break;
        }
        console.log(this.isShowBrand)
        console.log(this.isShowOrganization)
        console.log(this.isShowOutlet)
    }

    filterAttributes() {
        let filterCriteriants: any;
        switch (this.userLevel) {
            case Levels.BSS:
                filterCriteriants = [
                    { name: 'organization', title: 'organization', filterType: 'select', filterData: this.organizations },
                    { name: 'brand', title: 'brand', filterType: 'select', filterData: this.brands },
                    { name: 'outlet', title: 'outlet', filterType: 'select', filterData: this.outlets },
                ];
                break;
            case Levels.Organization:
                filterCriteriants = [
                    { name: 'brand', title: 'brand', filterType: 'select', filterData: this.brands },
                    { name: 'outlet', title: 'outlet', filterType: 'select', filterData: this.outlets },
                ];
                break;
            case Levels.Brand:
                filterCriteriants = [
                    { name: 'outlet', title: 'outlet', filterType: 'select', filterData: this.outlets },
                ];
                break;
            case Levels.Outlet: // AUTO SKIP FILTERc
                break;
        }

        this.filterAttrs = BaseFilterCriteria.from(filterCriteriants);
    }

    getData() {
        this.objectFilterStorage = this.filterStore.retrieve() || this.objectFilterStorage;

        this.currentUser = this.routerActive.snapshot.data.currentUser;

        switch (this.currentUser.provider) {
            case 'super-admin':
                // bss admin will get all organizations
                this.getAllOrganizations();
                break;

            case 'admins': // org-admin, brand-admin, outlet-admin
                this.activeIndexOrganization = 0;
                this.objectFilterStorage.organization = this.currentUser.organization;
                this.organizations.push(this.objectFilterStorage.organization);
                this.filterStore.store(this.objectFilterStorage);

                switch (this.currentUser.role.scope) {

                    case Scope.Organization:
                        // org-admin saved brand
                        if (this.objectFilterStorage.brand.id) {
                            this.getAllBrands();
                        } else { // org-admin not saved brand
                            this.brands = [];
                            this.activeIndexBrand = null;
                            this.outlets = [];
                            this.activeIndexOutlet = null;
                            //this.activeOrganization(this.activeIndexOrganization);
                        }
                        break;

                    case Scope.Brand:
                        this.brands = [this.currentUser.brand];
                        this.activeIndexBrand = 0;
                        this.objectFilterStorage.brand = this.currentUser.brand;
                        this.filterStore.store(this.objectFilterStorage);

                        // brand-admin saved outlet
                        if (this.objectFilterStorage.outlet.id) {
                            this.outlets = [];
                            this.outletsRepository.getOutletsByBrand(this.currentUser.brand.id)
                                .subscribe(
                                    (outlets: Outlet[]) => {
                                        this.outlets = outlets;
                                        this.activeIndexOutlet = _.findIndex(this.outlets, this.objectFilterStorage.outlet);

                                    }
                                );
                        } else { // brand-admin not saved outlet
                            this.outlets = [];
                            this.activeIndexOutlet = null;
                            // this.activeBrand(this.activeIndexBrand);
                        }
                        break;

                    case Scope.Outlet:
                        this.brands = [this.currentUser.brand];
                        this.activeIndexBrand = 0;
                        this.objectFilterStorage.brand = this.currentUser.brand;
                        this.filterStore.store(this.objectFilterStorage);
                        break;
                    default:
                        break;
                }

                break;

            default:
                break;
        }
    }

    getAllOrganizations() {
        this.organizationsRepository.all()
            .subscribe(
                (organizations: Organization[]) => {
                    this.organizations = organizations;
                    this.activeIndexOrganization = _.findIndex(this.organizations, this.objectFilterStorage.organization);

                    // if (this.activeIndexOrganization > -1) {
                    //     this.activeOrganization(this.activeIndexOrganization);
                    // }
                }
            );
    }

    getAllBrands() {
        this.brandsRepository.all()
            .subscribe(
                (brands: Brand[]) => {
                    this.brands = brands;
                    this.activeIndexBrand = _.findIndex(this.brands, this.objectFilterStorage.brand);

                    // if (this.activeIndexBrand >= 0) {
                    //     this.activeBrand(this.activeIndexBrand);
                    // }
                }
            );
    }

    isInvalid() {
        // return this.activeIndexOrganization < 0 || this.objectFilterStorage.organization.id === null;
        return false;
    }

    saveFilter(type: string, object: any) {
        this.objectFilterStorage[type] = object;

        switch (type) {
            case 'organization':
                this.objectFilterStorage.brand = new Brand();
                this.objectFilterStorage.outlet = new Outlet();
                break;
            case 'brand':
                this.objectFilterStorage.organization = this.organizations[this.activeIndexOrganization];
                this.objectFilterStorage.outlet = new Outlet();
                break;
            case 'outlet':
                this.objectFilterStorage.organization = this.organizations[this.activeIndexOrganization];
                this.objectFilterStorage.brand = this.brands[this.activeIndexBrand];
                break;
            default:
                break;
        }

        this.filterStore.store(this.objectFilterStorage);
        this.router.navigateByUrl('/');
    }

    activeOrganization(item: any, index: any) {
        this.orgName = item.name;
        this.getListUserScope();
        if (this.currentUser.provider === Provider.SuperAdmin || (this.currentUser.provider === Provider.Admin && this.currentUser.role.scope === Scope.Organization)) {
            this.activeIndexOrganization = index;
            this.activeIndexBrand = null;
            this.activeIndexOutlet = null;

            this.brands = [];
            this.brandsRepository.all({ 'organization_id': this.organizations[this.activeIndexOrganization].id, active: 1 })
                .subscribe(
                    (brands: Brand[]) => {
                        this.isShowOrganization = false;
                        this.isShowBrand = true;
                        this.brands = brands;

                        this.activeIndexBrand = _.findIndex(this.brands, this.objectFilterStorage.brand);

                        // if (this.activeIndexBrand >= 0) {
                        //     this.activeBrand(this.activeIndexBrand);
                        // }
                    }
                );
        }
    }

    activeBrand(item: any, index: any) {
        this.brandName = item.name;
        this.getListUserScope();
        if (this.currentUser.provider === Provider.SuperAdmin || this.currentUser.role.scope === Scope.Organization || this.currentUser.role.scope === Scope.Brand) {
            this.activeIndexBrand = index;
            this.activeIndexOutlet = null;
            this.outlets = [];

            switch (this.currentUser.provider) {
                case 'super-admin':
                    this.outletsRepository.outletBaseBrandSuperAdmin(this.organizations[this.activeIndexOrganization].id, this.brands[this.activeIndexBrand].id)
                        .subscribe(
                            (outlets: Outlet[]) => {
                                this.isShowOutlet = true;
                                this.isShowBrand = false;
                                this.isShowOrganization = false;
                                this.outlets = outlets;
                            }
                        );
                    break;

                case 'admins':
                    this.outletsRepository.getOutletsByBrand(this.brands[this.activeIndexBrand].id, { 'organization_id': this.organizations[this.activeIndexOrganization].id })
                        .subscribe(
                            (outlets: Outlet[]) => {
                                this.isShowOutlet = true;
                                this.isShowBrand = false;
                                this.isShowOrganization = false;
                                this.outlets = outlets;
                                this.outlets = outlets;
                            }
                        );
                    break;

                default:
                    break;
            }

        }
    }

    activeOutlet(index) {
        this.getListUserScope();
    }

    handleSkip() {
        if (this.currentUser.provider === Role.SuperAdmin) {
            this.objectFilterStorage = new Filter();
        }

        this.filterStore.store(this.objectFilterStorage);
        this.router.navigateByUrl('/');
    }

    backBrand() {
        this.isShowOrganization = false;
        this.isShowBrand = true;
        this.isShowOutlet = false;
    }

    backOrganization() {
        this.isShowOrganization = true;
        this.isShowBrand = false;
        this.isShowOutlet = false;
    }

    backManagement() {
        this.isShowOrganization = false;
        this.isShowBrand = false;
        this.isShowOutlet = false;
        this.isShowsuperadmin = false;
    }

    getListUserScope() {
        this.scopeService.getUserScope$()
            .subscribe(
                (userScope: UserScope) => {
                    this.userScope = userScope;
                }
            );
    }

    loadSuperadmin() {
        this.isShowOrganization = true;
        this.isShowsuperadmin = true;
    }
}
