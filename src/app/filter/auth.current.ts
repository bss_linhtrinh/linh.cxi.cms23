import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { User } from 'src/app/shared/entities/user';


@Injectable()
export class AuthCurrent implements Resolve<User> {

    constructor(
        private router: Router,
        private auth: AuthService
        ) {
    }

    resolve(route: ActivatedRouteSnapshot): Observable<User> | Promise<User> | User {
        return this.auth.currentUser$();
    }
}
