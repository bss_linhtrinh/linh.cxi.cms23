import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthCurrent} from './auth.current';
import {IsSkipFilter} from '../shared/guards/is-skip-filter/is-skip-filter.guard';
import {FilterComponent} from './filter/filter.component';
import {CanDeactivateGuard} from '../shared/guards/can-deactivate/can-deactivate.guard';

const routes: Routes = [
    {
        path: '',
        resolve: {
            currentUser: AuthCurrent
        },
        canActivate: [
            IsSkipFilter
        ],
        canDeactivate: [CanDeactivateGuard],
        component: FilterComponent,
        pathMatch: 'full',
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FilterRoutingModule {
}
