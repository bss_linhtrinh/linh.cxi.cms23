import {Component, OnInit} from '@angular/core';
import {BrandsRepository} from 'src/app/shared/repositories/brands.repository';
import {Router, ActivatedRoute} from '@angular/router';
import {Brand} from 'src/app/shared/entities/brand';
import {SessionStorage} from 'ngx-webstorage';
import {Language} from '../../shared/entities/language';

@Component({
    selector: 'app-create-brand',
    templateUrl: './create-brand.component.html',
    styleUrls: ['./create-brand.component.scss']
})
export class CreateBrandComponent implements OnInit {
    brand: Brand = new Brand();
    selectedFile: File = null;
    scope = [{name: 'FB', value: 'fb'},
        {name: 'Booking', value: 'booking'},
    ];
    tab: any;

    @SessionStorage('languages') languages: Language[];

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private brandsRepository: BrandsRepository,
    ) {
    }

    ngOnInit() {
    }

    cancel() {
        this.router.navigate(['..', 'list'], {relativeTo: this.route});
    }

    onFileChanged(selectedFile: File) {
        this.selectedFile = selectedFile;
    }

    onSubmit() {
        this.brandsRepository.save(this.brand)
            .subscribe(
                (brand: Brand) => {
                    this.brand = brand;
                    this.router.navigate(['..', 'list'], {relativeTo: this.route});
                }
            );
    }
}
