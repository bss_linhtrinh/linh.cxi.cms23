import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrandRoutingModule } from './brand-routing.module';
import { CreateBrandComponent } from './create-brand/create-brand.component';
import { EditBrandComponent } from './edit-brand/edit-brand.component';
import { ListBrandComponent } from './list-brand/list-brand.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ListBrandComponent, CreateBrandComponent, EditBrandComponent],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    BrandRoutingModule,
  ]
})
export class BrandModule { }
