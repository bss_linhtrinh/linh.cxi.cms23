import {Component, OnInit} from '@angular/core';
import {BrandsRepository} from 'src/app/shared/repositories/brands.repository';
import {Router, ActivatedRoute} from '@angular/router';
import {Brand} from 'src/app/shared/entities/brand';
import {CxiGridColumn} from '../../shared/components/cxi-grid/cxi-grid-column';

@Component({
    selector: 'app-list-brand',
    templateUrl: './list-brand.component.html',
    styleUrls: ['./list-brand.component.scss']
})
export class ListBrandComponent implements OnInit {
    // table
    cxiGridConfig = {
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        pagingServer: true,
        translatable: true,
        repository: this.brandsRepository,
        actions: {
            onEdit: (brand: Brand) => {
                this.onEdit(brand);
            },
            onView: (brand: Brand) => {
                this.onView(brand);
            },
            onActivate: (brand: Brand) => {
                this.onActivate(brand);
            },
            onDeactivate: (brand: Brand) => {
                this.onDeactivate(brand);
            },
            onDelete: (brand: Brand) => {
                this.onDelete(brand);
            },
        }
    };
    cxiGridColumns = CxiGridColumn.from([
        {title: 'Name', name: 'name', avatar: 'images'},
        {title: 'Email', name: 'contact_email'},
        {title: 'Hotline', name: 'contact_number'},
        {title: 'Website', name: 'website'},
    ]);

    brands: Brand[] = [];

    constructor(
        private brandsRepository: BrandsRepository,
        private router: Router,
        private routerActive: ActivatedRoute,
    ) {
    }

    ngOnInit() {
    }

    onEdit(brand: Brand) {
        this.router.navigate(['..', brand.id, 'edit'], {relativeTo: this.routerActive});
    }

    onView(brand: Brand) {
        this.router.navigate(['..', brand.id, 'edit'], {relativeTo: this.routerActive});
    }

    onActivate(brand: Brand) {
        this.brandsRepository.activate(brand)
            .subscribe();
    }

    onDeactivate(brand: Brand) {
        this.brandsRepository.deactivate(brand)
            .subscribe();
    }

    onDelete(brand: Brand) {
        this.brandsRepository.destroy(brand)
            .subscribe();
    }

}
