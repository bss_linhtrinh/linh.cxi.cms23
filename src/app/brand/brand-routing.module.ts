import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListBrandComponent} from './list-brand/list-brand.component';
import {CreateBrandComponent} from './create-brand/create-brand.component';
import {EditBrandComponent} from './edit-brand/edit-brand.component';
import {CheckPermissionsGuard} from '../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: 'list', component: ListBrandComponent, data: {breadcrumb: 'List brands'}},
            {path: 'create', component: CreateBrandComponent, data: {breadcrumb: 'Create new brand'}},
            {path: ':id/edit', component: EditBrandComponent, data: {breadcrumb: 'Edit brand'}},
            {path: '', redirectTo: 'list', pathMatch: 'full'},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BrandRoutingModule {
}
