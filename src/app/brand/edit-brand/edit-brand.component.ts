import {Component, OnInit} from '@angular/core';
import {Brand} from 'src/app/shared/entities/brand';
import {Router, ActivatedRoute} from '@angular/router';
import {BrandsRepository} from 'src/app/shared/repositories/brands.repository';
import {filter, map, switchMap} from 'rxjs/operators';
import {SessionStorage} from 'ngx-webstorage';
import {Language} from '../../shared/entities/language';

@Component({
    selector: 'app-edit-brand',
    templateUrl: './edit-brand.component.html',
    styleUrls: ['./edit-brand.component.scss']
})
export class EditBrandComponent implements OnInit {
    brand: Brand = new Brand();
    selectedFile: File = null;
    scope = [{name: 'FB', value: 'fb'},
        {name: 'Booking', value: 'booking'},
    ];
    tab: any;

    @SessionStorage('languages') languages: Language[];

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private brandsRepository: BrandsRepository,
    ) {
    }

    ngOnInit() {
        this.getBrandDetail();
    }

    getBrandDetail() {
        this.route.params
            .pipe(
                filter(data => data.id),
                map(data => data.id),
                switchMap(
                    (id: number) => this.brandsRepository.find(id)
                )
            )
            .subscribe(
                (brand: Brand) => {
                    this.brand = brand;
                }
            );
    }

    cancel() {
        this.router.navigate(['../..', 'list'], {relativeTo: this.route});
    }

    onFileChanged(selectedFile: File) {
        this.selectedFile = selectedFile;
    }

    onSubmit() {
        this.brandsRepository.save(this.brand)
            .subscribe(
                (brand: Brand) => {
                    this.brand = brand;
                    this.router.navigate(['../..', 'list'], {relativeTo: this.route});
                },
            );
    }
}
