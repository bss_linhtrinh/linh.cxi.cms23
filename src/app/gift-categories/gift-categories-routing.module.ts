import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CreateGiftCategoryComponent} from './create-gift-category/create-gift-category.component';
import {ListGiftCategoriesComponent} from './list-gift-categories/list-gift-categories.component';
import {EditGiftCategoryComponent} from './edit-gift-category/edit-gift-category.component';
import {CheckPermissionsGuard} from '../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: 'create', component: CreateGiftCategoryComponent, data: {breadcrumb: 'Create new gift-categories'}},
            {path: 'list', component: ListGiftCategoriesComponent, data: {breadcrumb: 'List gift-categories'}},
            {path: ':id/edit', component: EditGiftCategoryComponent, data: {breadcrumb: 'Edit gift-categories'}},
            {path: '', redirectTo: 'list', pathMatch: 'full'},
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GiftCategoriesRoutingModule {
}
