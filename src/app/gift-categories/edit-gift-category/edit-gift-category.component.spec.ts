import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGiftCategoryComponent } from './edit-gift-category.component';

describe('EditGiftCategoryComponent', () => {
  let component: EditGiftCategoryComponent;
  let fixture: ComponentFixture<EditGiftCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditGiftCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGiftCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
