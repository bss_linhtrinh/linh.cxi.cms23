import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GiftCategoriesRepository } from 'src/app/shared/repositories/gift-categories.repository';
import { GiftCategories } from 'src/app/shared/entities/gift-categories';
import { filter, switchMap, map } from 'rxjs/operators';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
import { UserScope } from 'src/app/shared/entities/user-scope';

@Component({
  selector: 'app-edit-gift-category',
  templateUrl: './edit-gift-category.component.html',
  styleUrls: ['./edit-gift-category.component.scss']
})
export class EditGiftCategoryComponent implements OnInit {

  public gift_categories: GiftCategories = new GiftCategories();
  selectedFile: File = null;
  userScope: UserScope;
  public brand = [];
  tab: string = 'en';
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private giftCategoriesRepository: GiftCategoriesRepository,
    private scopeService: ScopeService,
    private brandRepository: BrandsRepository,
  ) { }

  ngOnInit() {
    this.loadGiftCategories();
    this.loadListBrand();
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.gift_categories.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  loadListBrand() {
    this.brandRepository.all()
      .subscribe(
        (res) => {
          this.brand = res;
          this.getListUserScope();
        }
      );
  }
  onFileChanged(selectedFile: File) {
    this.selectedFile = selectedFile;
  }
  loadGiftCategories() {
    this.activatedRoute.params
      .pipe(
        filter(data => data.id),
        map(data => data.id),
        switchMap(
          (id) => this.giftCategoriesRepository.find(id)
        )
      )
      .subscribe((res?: any) => {
        let vm =  this;
        vm.gift_categories = res;
        this.gift_categories.translations.forEach(item => {
          if (item.locale == 'en') {
            this.gift_categories.en = item;
          } else if (item.locale == 'vi') {
            this.gift_categories.vi = item;
          }
        });
      });
  }
  onSubmit() {
    this.gift_categories.translations = null;
    this.giftCategoriesRepository.save(this.gift_categories)
      .subscribe((res) => {
        this.cancel();
      });
  }
  cancel() {
    this.router.navigate(['../..', 'list'], { relativeTo: this.activatedRoute });
  }

}
