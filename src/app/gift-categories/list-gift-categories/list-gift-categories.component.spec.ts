import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGiftCategoriesComponent } from './list-gift-categories.component';

describe('ListGiftCategoriesComponent', () => {
  let component: ListGiftCategoriesComponent;
  let fixture: ComponentFixture<ListGiftCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListGiftCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGiftCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
