import { Component, OnInit } from '@angular/core';
import { GiftCategories } from 'src/app/shared/entities/gift-categories';
import { GiftCategoriesRepository } from 'src/app/shared/repositories/gift-categories.repository';
import { Router, ActivatedRoute } from '@angular/router';
import {LocalStorage} from 'ngx-webstorage';
import {CxiGridConfig} from '../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../shared/components/cxi-grid/cxi-grid-column';

@Component({
    selector: 'app-list-gift-categories',
    templateUrl: './list-gift-categories.component.html',
    styleUrls: ['./list-gift-categories.component.scss']
})
export class ListGiftCategoriesComponent implements OnInit {
    public gift_categories: GiftCategories[] = [];
    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        pagingServer: true,
        repository: this.giftCategoriesRepository,
        translatable: true,
        actions: {
            onEdit: (gift_categories: GiftCategories) => {
                return this.onEdit(gift_categories);
            },
            onDelete: (gift_categories: GiftCategories) => {
                return this.onDelete(gift_categories);
            },
            onActivate: (gift_categories: GiftCategories) => {
                this.onActivate(gift_categories);
            },
            onDeactivate: (gift_categories: GiftCategories) => {
                this.onDeactivate(gift_categories);
            },
        }
    });
    cxiGridColumns = CxiGridColumn.from([
        { title: 'name', name: 'name', avatar: 'images' },
        { title: 'description', name: 'description' },
    ]);

    @LocalStorage('scope.organization_id') organizationId: number;
    @LocalStorage('scope.brand_id') brandId: number;
    @LocalStorage('scope.outlet_id') outletId: number;

    constructor(
        private giftCategoriesRepository: GiftCategoriesRepository,
        private router: Router,
        public activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit() {
    }

    onEdit(gift_categories: GiftCategories) {
        this.router.navigate(['..', gift_categories.id, 'edit'], { relativeTo: this.activatedRoute });
    }

    onDelete(gift_categories: GiftCategories) {
        this.giftCategoriesRepository.destroy(gift_categories).subscribe();
    }
    onActivate(gift_categories: GiftCategories) {
        this.giftCategoriesRepository.activate(gift_categories).subscribe();
    }

    onDeactivate(gift_categories: GiftCategories) {
        this.giftCategoriesRepository.deactivate(gift_categories).subscribe();
    }
}
