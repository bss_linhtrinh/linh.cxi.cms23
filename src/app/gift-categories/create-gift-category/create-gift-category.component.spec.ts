import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateGiftCategoryComponent } from './create-gift-category.component';

describe('CreateGiftCategoryComponent', () => {
  let component: CreateGiftCategoryComponent;
  let fixture: ComponentFixture<CreateGiftCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateGiftCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateGiftCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
