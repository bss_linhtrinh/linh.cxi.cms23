import { Component, OnInit } from '@angular/core';
import { GiftCategories } from 'src/app/shared/entities/gift-categories';
import { GiftCategoriesRepository } from 'src/app/shared/repositories/gift-categories.repository';
import { Router, ActivatedRoute } from '@angular/router';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';

@Component({
  selector: 'app-create-gift-category',
  templateUrl: './create-gift-category.component.html',
  styleUrls: ['./create-gift-category.component.scss']
})
export class CreateGiftCategoryComponent implements OnInit {

  public gift_categories: GiftCategories = new GiftCategories();
  selectedFile: File = null;
  userScope: UserScope;
  public brand = [];
  tab: string = 'en';
  constructor(
    private giftCategoriesRepository: GiftCategoriesRepository,
    private router: Router,
    public activatedRoute: ActivatedRoute,
    private scopeService: ScopeService,
    private brandRepository: BrandsRepository,
  ) { }

  ngOnInit() {
    this.loadListBrand();
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.gift_categories.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  loadListBrand() {
    this.brandRepository.all()
      .subscribe(
        (res) => {
          this.brand = res;
          this.gift_categories.brand_id = this.brand[0].id;
          this.getListUserScope();
        }
      );
  }
  onFileChanged(selectedFile: File) {
    this.selectedFile = selectedFile;
  }
  onSubmit() {
    this.giftCategoriesRepository.save(this.gift_categories)
      .subscribe(res => {
        this.router.navigate(['../list'], { relativeTo: this.activatedRoute });
      });
  }

}
