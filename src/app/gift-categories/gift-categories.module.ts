import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GiftCategoriesRoutingModule } from './gift-categories-routing.module';
import { EditGiftCategoryComponent } from './edit-gift-category/edit-gift-category.component';
import { CreateGiftCategoryComponent } from './create-gift-category/create-gift-category.component';
import { ListGiftCategoriesComponent } from './list-gift-categories/list-gift-categories.component';
import {SharedModule} from '../shared/shared.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [CreateGiftCategoryComponent, EditGiftCategoryComponent, ListGiftCategoriesComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    GiftCategoriesRoutingModule
  ]
})
export class GiftCategoriesModule { }
