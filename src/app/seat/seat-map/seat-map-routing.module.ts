import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListSeatMapComponent} from './list-seat-map/list-seat-map.component';
import {CheckPermissionsGuard} from '../../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
    {path: '', pathMatch: 'full', canActivate: [CheckPermissionsGuard], component: ListSeatMapComponent, data: {breadcrumb: 'Seat Map'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SeatMapRoutingModule {
}
