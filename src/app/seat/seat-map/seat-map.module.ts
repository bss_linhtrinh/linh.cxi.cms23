import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SeatMapRoutingModule} from './seat-map-routing.module';
import {ListSeatMapComponent} from './list-seat-map/list-seat-map.component';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule} from '@angular/forms';

@NgModule({
    declarations: [ListSeatMapComponent],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        SeatMapRoutingModule
    ]
})
export class SeatMapModule {
}
