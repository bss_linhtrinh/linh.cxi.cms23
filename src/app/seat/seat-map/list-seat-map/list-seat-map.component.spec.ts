import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSeatMapComponent } from './list-seat-map.component';

describe('ListSeatMapComponent', () => {
  let component: ListSeatMapComponent;
  let fixture: ComponentFixture<ListSeatMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSeatMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSeatMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
