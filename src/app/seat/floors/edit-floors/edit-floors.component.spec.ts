import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFloorsComponent } from './edit-floors.component';

describe('EditFloorsComponent', () => {
  let component: EditFloorsComponent;
  let fixture: ComponentFixture<EditFloorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditFloorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFloorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
