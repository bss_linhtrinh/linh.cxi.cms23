import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Floors } from 'src/app/shared/entities/floors';
import { Router, ActivatedRoute } from '@angular/router';
import { FloorsRepository } from 'src/app/shared/repositories/floors.repository';
import { OutletsRepository } from 'src/app/shared/repositories/outlets.repository';
import { filter, map, switchMap } from 'rxjs/operators';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
declare var jQuery: any;

@Component({
  selector: 'app-edit-floors',
  templateUrl: './edit-floors.component.html',
  styleUrls: ['./edit-floors.component.scss']
})
export class EditFloorsComponent implements OnInit, OnChanges {
  @Input() dataFloor;
  public floors: Floors = new Floors();
  public outlets = [];
  userScope: UserScope;
  public brand = [];
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private floorsRepository: FloorsRepository,
    private outletRepository: OutletsRepository,
    private scopeService: ScopeService,
    private brandRepository: BrandsRepository,
  ) { }

  ngOnInit() {
    this.loadFoors();
    this.loadListBrand();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dataFloor) {
      this.floors = this.dataFloor;
    }
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.floors.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  loadListBrand() {
    this.brandRepository.all()
      .subscribe(
        (res) => {
          this.brand = res;
          this.getListUserScope();
          this.loadOutlet();
        }
      );
  }
  changeOulet() {
    this.loadOutlet();
  }
  loadOutlet() {
    this.outletRepository.all({ brand_id: this.floors.brand_id })
      .subscribe(res => {
        this.outlets = res;
        (this.outlets.length) ? this.floors.outlet_id = this.outlets[0].id : null;
      })
  }
  loadFoors() {
    this.activatedRoute.params
      .pipe(
        filter(data => data.id),
        map(data => data.id),
        switchMap(
          (id) => this.floorsRepository.find(id)
        )
      )
      .subscribe((floors: Floors) => {
        this.floors = floors;
      });
  }
  onSubmit() {
    this.floorsRepository.save(this.floors)
      .subscribe((res) => {
        this.closeModal();
      });
  }
  closeModal() {
    (function ($) {
      $(document).ready(function () {
        $('#editFloor').modal('hide');
      });
    })(jQuery);
  }
}
