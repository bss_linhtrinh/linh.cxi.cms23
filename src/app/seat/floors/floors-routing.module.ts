import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CreateFloorsComponent} from './create-floors/create-floors.component';
import {ListFloorsComponent} from './list-floors/list-floors.component';
import {EditFloorsComponent} from './edit-floors/edit-floors.component';

const routes: Routes = [
    {path: '', redirectTo: 'list', pathMatch: 'full'},
    {path: 'create', component: CreateFloorsComponent, data: {breadcrumb: 'Create new floor'}},
    {path: 'list', component: ListFloorsComponent, data: {breadcrumb: 'List floors'}},
    {path: ':id/edit', component: EditFloorsComponent, data: {breadcrumb: 'Edit floor'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FloorsRoutingModule {
}
