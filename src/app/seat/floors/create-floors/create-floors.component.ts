import { Component, OnInit } from '@angular/core';
import { Floors } from 'src/app/shared/entities/floors';
import { FloorsRepository } from 'src/app/shared/repositories/floors.repository';
import { Router, ActivatedRoute } from '@angular/router';
import { OutletsRepository } from 'src/app/shared/repositories/outlets.repository';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
declare var jQuery: any;

@Component({
  selector: 'app-create-floors',
  templateUrl: './create-floors.component.html',
  styleUrls: ['./create-floors.component.scss']
})
export class CreateFloorsComponent implements OnInit {

  public floors: Floors = new Floors();
  public outlets = [];
  selectedFile: File = null;
  userScope: UserScope;
  public brand = [];
  constructor(
    private floorsRepository: FloorsRepository,
    private outletRepository: OutletsRepository,
    private router: Router,
    public activatedRoute: ActivatedRoute,
    private scopeService: ScopeService,
    private brandRepository: BrandsRepository,
  ) { }

  ngOnInit() {
    this.loadListBrand();
  }
  onFileChanged(selectedFile: File) {
    this.selectedFile = selectedFile;
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.floors.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  loadListBrand() {
    this.brandRepository.all()
      .subscribe(
        (res) => {
          this.brand = res;
          this.floors.brand_id = this.brand[0].id;
          this.getListUserScope();
          this.loadOutlet();
        }
      );
  }
  loadOutlet() {
    this.outletRepository.all({ brand_id: this.floors.brand_id })
      .subscribe(res => {
        this.outlets = res;
        (this.outlets.length) ? this.floors.outlet_id = this.outlets[0].id : null;
      })
  }
  changeOulet() {
    this.loadOutlet();
  }
  onSubmit() {
    this.floorsRepository.save(this.floors)
      .subscribe(res => {
        this.router.navigate(['../list'], { relativeTo: this.activatedRoute });
        this.closeModal();
        this.floors = new Floors();
      });
  }
  closeModal() {
    (function ($) {
      $(document).ready(function () {
        $('#exampleModal').modal('hide');
      });
    })(jQuery);
  }
}
