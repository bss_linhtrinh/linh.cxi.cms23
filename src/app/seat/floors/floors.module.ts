import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FloorsRoutingModule } from './floors-routing.module';
import { CreateFloorsComponent } from './create-floors/create-floors.component';
import { EditFloorsComponent } from './edit-floors/edit-floors.component';
import { ListFloorsComponent } from './list-floors/list-floors.component';
import {SharedModule} from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [CreateFloorsComponent, EditFloorsComponent, ListFloorsComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    FloorsRoutingModule
  ]
})
export class FloorsModule { }
