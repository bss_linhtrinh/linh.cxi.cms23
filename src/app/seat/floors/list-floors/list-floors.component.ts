import { Component, OnInit } from '@angular/core';
import { Floors } from 'src/app/shared/entities/floors';
import { FloorsRepository } from 'src/app/shared/repositories/floors.repository';
import { Router, ActivatedRoute } from '@angular/router';
import { CxiGridConfig } from '../../../shared/components/cxi-grid/cxi-grid-config';
import { CxiGridColumn } from '../../../shared/components/cxi-grid/cxi-grid-column';
declare var jQuery: any;

@Component({
    selector: 'app-list-floors',
    templateUrl: './list-floors.component.html',
    styleUrls: ['./list-floors.component.scss']
})
export class ListFloorsComponent implements OnInit {

    public isShow: boolean = false;
    public isShowCreat: boolean = false;
    public floor: Floors = new Floors();
    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        className: false,
        selecting: true,
        pagingServer: true,
        repository: this.floorsRepository,
        actions: {
            onEdit: (floors: Floors) => {
                return this.onEdit(floors);
            },
            onDelete: (floors: Floors) => {
                return this.onDelete(floors);
            },
        }
    });

    cxiGridColumns = CxiGridColumn.from([
        { title: 'name', name: 'name', className: 'name' },
    ]);

    constructor(
        private floorsRepository: FloorsRepository,
        private router: Router,
        public activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit() {
    }
    addNewFloor() {
        this.isShowCreat = true;
    }
    onEdit(floor: Floors) {
        this.isShow = true;
        this.floorsRepository.find(floor.id)
            .subscribe((res?: any) => {
                this.floor = res;
            })
        this.showModal();
    }
    onDelete(floors: Floors) {
        this.floorsRepository.destroy(floors).subscribe();
    }
    showModal() {
        (function ($) {
            $(document).ready(function () {
                $('#editFloor').modal('show');
            });
        })(jQuery);
    }
}
