import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CreateTableCategoriesComponent} from './create-table-categories/create-table-categories.component';
import {ListTableCategoriesComponent} from './list-table-categories/list-table-categories.component';
import {EditTableCategoriesComponent} from './edit-table-categories/edit-table-categories.component';

const routes: Routes = [
    {path: '', redirectTo: 'list', pathMatch: 'full'},
    {path: 'create', component: CreateTableCategoriesComponent, data: {breadcrumb: 'Create new table-categories'}},
    {path: 'list', component: ListTableCategoriesComponent, data: {breadcrumb: 'List table-categories'}},
    {path: ':id/edit', component: EditTableCategoriesComponent, data: {breadcrumb: 'Edit table-categories'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TableCategoriesRoutingModule {
}
