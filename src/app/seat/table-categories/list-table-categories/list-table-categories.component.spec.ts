import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTableCategoriesComponent } from './list-table-categories.component';

describe('ListTableCategoriesComponent', () => {
  let component: ListTableCategoriesComponent;
  let fixture: ComponentFixture<ListTableCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTableCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTableCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
