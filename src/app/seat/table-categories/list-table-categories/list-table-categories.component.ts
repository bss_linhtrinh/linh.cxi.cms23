import { Component, OnInit } from '@angular/core';
import { TableCategories } from 'src/app/shared/entities/table-categories';
import { TableCategoriesRepository } from 'src/app/shared/repositories/table-categories.repository';
import { Router, ActivatedRoute } from '@angular/router';
import { CxiGridConfig } from '../../../shared/components/cxi-grid/cxi-grid-config';
import { CxiGridColumn } from '../../../shared/components/cxi-grid/cxi-grid-column';
declare var jQuery: any;

@Component({
    selector: 'app-list-table-categories',
    templateUrl: './list-table-categories.component.html',
    styleUrls: ['./list-table-categories.component.scss']
})
export class ListTableCategoriesComponent implements OnInit {

    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        pagingServer: true,
        repository: this.tableCategoriesRepository,
        actions: {
            onEdit: (table_categories: TableCategories) => {
                return this.onEdit(table_categories);
            },
            onDelete: (table_categories: TableCategories) => {
                return this.onDelete(table_categories);
            },
        }
    });

    cxiGridColumns = CxiGridColumn.from([
        { title: 'name', name: 'name', avatar: 'images' },
        // { title: 'image', name: 'image', className: 'image' },
    ]);
    public table_category: TableCategories = new TableCategories();
    public isShow: boolean = false;
    constructor(
        private tableCategoriesRepository: TableCategoriesRepository,
        private router: Router,
        public activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit() {
    }

    onEdit(table_categories: TableCategories) {
        //this.router.navigate(['..', table_categories.id, 'edit'], { relativeTo: this.activatedRoute });
        this.isShow = true;
        this.tableCategoriesRepository.find(table_categories.id)
            .subscribe((res?: any) => {
                this.table_category = res;
            })
        this.showModal();
    }

    onDelete(table_categories: TableCategories) {
        this.tableCategoriesRepository.destroy(table_categories).subscribe();
    }
    showModal() {
        (function ($) {
            $(document).ready(function () {
                $('#editCategoryTable').modal('show');
            });
        })(jQuery);
    }
}
