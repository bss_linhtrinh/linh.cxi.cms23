import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TableCategoriesRepository } from 'src/app/shared/repositories/table-categories.repository';
import { TableCategories } from 'src/app/shared/entities/table-categories';
import { filter, map, switchMap } from 'rxjs/operators';
declare var jQuery: any;

@Component({
  selector: 'app-edit-table-categories',
  templateUrl: './edit-table-categories.component.html',
  styleUrls: ['./edit-table-categories.component.scss']
})
export class EditTableCategoriesComponent implements OnInit, OnChanges {
  @Input() dataTableCate;
  public table_categories: TableCategories = new TableCategories();
  selectedFile: File = null;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private tableCategoriesRepository: TableCategoriesRepository,
  ) { }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dataTableCate) {
      this.table_categories = this.dataTableCate;
    }
  }
  onFileChanged(selectedFile: File) {
    this.selectedFile = selectedFile;
  }
  loadTableCategories() {
    this.activatedRoute.params
      .pipe(
        filter(data => data.id),
        map(data => data.id),
        switchMap(
          (id) => this.tableCategoriesRepository.find(id)
        )
      )
      .subscribe((table_categories: TableCategories) => {
        this.table_categories = table_categories;
      });
  }
  onSubmit() {
    this.tableCategoriesRepository.save(this.table_categories)
      .subscribe((res) => {
        this.closeModal();
        this.router.navigate(['../list'], { relativeTo: this.activatedRoute });
      });
  }
  closeModal() {
    (function ($) {
      $(document).ready(function () {
        $('#editCategoryTable').modal('hide');
      });
    })(jQuery);
  }
}
