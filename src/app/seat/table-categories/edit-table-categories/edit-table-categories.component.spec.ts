import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTableCategoriesComponent } from './edit-table-categories.component';

describe('EditTableCategoriesComponent', () => {
  let component: EditTableCategoriesComponent;
  let fixture: ComponentFixture<EditTableCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTableCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTableCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
