import { Component, OnInit } from '@angular/core';
import { TableCategories } from 'src/app/shared/entities/table-categories';
import { TableCategoriesRepository } from 'src/app/shared/repositories/table-categories.repository';
import { Router, ActivatedRoute } from '@angular/router';
declare var jQuery: any;

@Component({
  selector: 'app-create-table-categories',
  templateUrl: './create-table-categories.component.html',
  styleUrls: ['./create-table-categories.component.scss']
})
export class CreateTableCategoriesComponent implements OnInit {

  public table_categories: TableCategories = new TableCategories();

  selectedFile: File = null;

  constructor(
    private tableCategoriesRepository: TableCategoriesRepository,
    private router: Router,
    public activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
  }
  onFileChanged(selectedFile: File) {
    this.selectedFile = selectedFile;
  }
  onSubmit() {
    this.tableCategoriesRepository.save(this.table_categories)
      .subscribe(res => {
        //this.router.navigate(['../list'], { relativeTo: this.activatedRoute });
        this.closeModal();
        this.table_categories = new TableCategories();
      });
  }
  closeModal() {
    (function ($) {
      $(document).ready(function () {
        $('#exampleModal').modal('hide');
      });
    })(jQuery);
  }
}
