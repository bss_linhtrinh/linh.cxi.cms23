import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTableCategoriesComponent } from './create-table-categories.component';

describe('CreateTableCategoriesComponent', () => {
  let component: CreateTableCategoriesComponent;
  let fixture: ComponentFixture<CreateTableCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTableCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTableCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
