import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TableCategoriesRoutingModule } from './table-categories-routing.module';
import { ListTableCategoriesComponent } from './list-table-categories/list-table-categories.component';
import { CreateTableCategoriesComponent } from './create-table-categories/create-table-categories.component';
import { EditTableCategoriesComponent } from './edit-table-categories/edit-table-categories.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ListTableCategoriesComponent, CreateTableCategoriesComponent, EditTableCategoriesComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    TableCategoriesRoutingModule
  ],
})
export class TableCategoriesModule { }
