import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Tables } from 'src/app/shared/entities/tables';
import { TablesRepository } from 'src/app/shared/repositories/tables.repository';
import { Router, ActivatedRoute } from '@angular/router';
import { OutletsRepository } from 'src/app/shared/repositories/outlets.repository';
import { TableCategoriesRepository } from 'src/app/shared/repositories/table-categories.repository';
import { FloorsRepository } from 'src/app/shared/repositories/floors.repository';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
import { UserScope } from 'src/app/shared/entities/user-scope';
declare var jQuery: any;

@Component({
  selector: 'app-create-tables',
  templateUrl: './create-tables.component.html',
  styleUrls: ['./create-tables.component.scss']
})
export class CreateTablesComponent implements OnInit, OnChanges {

  public table: Tables = new Tables();
  public outlet = [];
  public table_categories = [];
  public floors = [];
  selectedFile: File = null;
  userScope: UserScope;
  public brand = [];
  constructor(
    private tablesRepository: TablesRepository,
    private outletRepository: OutletsRepository,
    private floorsRepository: FloorsRepository,
    private tableCategoriesRepository: TableCategoriesRepository,
    private scopeService: ScopeService,
    private brandRepository: BrandsRepository,
    public activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.loadOutlet();
    this.loadTableCate();
    this.loadFloors();
    this.loadListBrand();
  }
  ngOnChanges(simpleChanges: SimpleChanges) {
  }
  onFileChanged(selectedFile: File) {
    this.selectedFile = selectedFile;
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.table.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  loadListBrand() {
    this.brandRepository.all()
      .subscribe(
        (res) => {
          this.brand = res;
          this.table.brand_id = this.brand[0].id;
          this.getListUserScope();
        }
      );
  }
  loadOutlet() {
    this.outletRepository.all({ brand_id: this.table.brand_id })
      .subscribe(res => {
        this.outlet = res;
        (this.outlet.length) ? this.table.outlet_id = this.outlet[0].id : null;
      });
  }
  changeOulet() {
    this.loadOutlet();
  }
  loadTableCate() {
    this.tableCategoriesRepository.all()
      .subscribe(res => {
        this.table_categories = res;
        this.table.table_category_id = this.table_categories[0].id;
      });
  }
  loadFloors() {
    this.floorsRepository.all()
      .subscribe(res => {
        this.floors = res;
        this.table.floor_id = this.floors[0].id;
      });
  }
  onSubmit() {
    this.tablesRepository.save(this.table)
      .subscribe(res => {
        this.closeModal();
        this.table = new Tables();
      });
  }
  closeModal() {
    (function ($) {
      $(document).ready(function () {
        $('#exampleModal').modal('hide');
      });
    })(jQuery);
  }
}
