import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TablesRoutingModule} from './tables-routing.module';
import {ListTablesComponent} from './list-tables/list-tables.component';
import {EditTablesComponent} from './edit-tables/edit-tables.component';
import {CreateTablesComponent} from './create-tables/create-tables.component';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule} from '@angular/forms';

@NgModule({
    declarations: [
        ListTablesComponent,
        EditTablesComponent,
        CreateTablesComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        TablesRoutingModule
    ]
})
export class TablesModule {
}
