import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CreateTablesComponent} from './create-tables/create-tables.component';
import {ListTablesComponent} from './list-tables/list-tables.component';
import {EditTablesComponent} from './edit-tables/edit-tables.component';

const routes: Routes = [
    {path: '', redirectTo: 'list', pathMatch: 'full'},
    {path: 'create', component: CreateTablesComponent, data: {breadcrumb: 'Create new table'}},
    {path: 'list', component: ListTablesComponent, data: {breadcrumb: 'List tables'}},
    {path: ':id/edit', component: EditTablesComponent, data: {breadcrumb: 'Edit table'}},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TablesRoutingModule {
}
