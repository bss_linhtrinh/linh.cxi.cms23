import { Component, OnInit } from '@angular/core';
import { Tables } from 'src/app/shared/entities/tables';
import { TablesRepository } from 'src/app/shared/repositories/tables.repository';
import { Router, ActivatedRoute } from '@angular/router';
import { CxiGridConfig } from '../../../shared/components/cxi-grid/cxi-grid-config';
import { CxiGridColumn } from '../../../shared/components/cxi-grid/cxi-grid-column';
declare var jQuery: any;

@Component({
    selector: 'app-list-tables',
    templateUrl: './list-tables.component.html',
    styleUrls: ['./list-tables.component.scss']
})
export class ListTablesComponent implements OnInit {
    public isShow: boolean = false;
    public tables: Tables = new Tables();
    public dataTable: boolean;
    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        className: false,
        selecting: true,
        pagingServer: true,
        repository: this.tablesRepository,
        actions: {
            onEdit: (tables: Tables) => {
                return this.onEdit(tables);
            },
            onDelete: (tables: Tables) => {
                return this.onDelete(tables);
            },
        }
    });
    cxiGridColumns = CxiGridColumn.from([
        { title: 'name', name: 'name', avatar: 'images' },
        // { title: 'image', name: 'image', className: 'image' },
    ]);

    constructor(
        private tablesRepository: TablesRepository,
        private router: Router,
        public activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit() {
    }
    addNewTable() {
        this.dataTable = true;
    }
    onEdit(tables: Tables) {
        this.isShow = true;
        this.tablesRepository.find(tables.id)
            .subscribe((res?: any) => {
                this.tables = res;
            })
        this.showModal();
    }

    onDelete(tables: Tables) {
        this.tablesRepository.destroy(tables).subscribe();
    }
    showModal() {
        (function ($) {
            $(document).ready(function () {
                $('#editTable').modal('show');
            });
        })(jQuery);
    }
}
