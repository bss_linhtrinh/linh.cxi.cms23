import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { TablesRepository } from 'src/app/shared/repositories/tables.repository';
import { OutletsRepository } from 'src/app/shared/repositories/outlets.repository';
import { TableCategoriesRepository } from 'src/app/shared/repositories/table-categories.repository';
import { Router, ActivatedRoute } from '@angular/router';
import { Tables } from 'src/app/shared/entities/tables';
import { filter, map, switchMap } from 'rxjs/operators';
import { FloorsRepository } from 'src/app/shared/repositories/floors.repository';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
declare var jQuery: any;

@Component({
  selector: 'app-edit-tables',
  templateUrl: './edit-tables.component.html',
  styleUrls: ['./edit-tables.component.scss']
})
export class EditTablesComponent implements OnInit, OnChanges {
  @Input() dataTable;
  public table: Tables = new Tables();
  public outlet = [];
  public table_categories = [];
  public floors = [];
  selectedFile: File = null;
  userScope: UserScope;
  public brand = [];
  constructor(
    private tablesRepository: TablesRepository,
    private outletRepository: OutletsRepository,
    private floorsRepository: FloorsRepository,
    private tableCategoriesRepository: TableCategoriesRepository,
    private scopeService: ScopeService,
    private brandRepository: BrandsRepository,
    public activatedRoute: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.loadTables();
    this.loadOutlet();
    this.loadTableCate();
    this.loadFloors();
    this.loadListBrand();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dataTable) {
      this.table = this.dataTable;
    }
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.table.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  loadListBrand() {
    this.brandRepository.all()
      .subscribe(
        (res) => {
          this.brand = res;
          this.getListUserScope();
        }
      );
  }
  onFileChanged(selectedFile: File) {
    this.selectedFile = selectedFile;
  }
  loadOutlet() {
    this.outletRepository.all()
      .subscribe(res => {
        this.outlet = res;
      });
  }
  loadTableCate() {
    this.tableCategoriesRepository.all()
      .subscribe(res => {
        this.table_categories = res;
      });
  }
  loadFloors() {
    this.floorsRepository.all()
      .subscribe(res => {
        this.floors = res;
      });
  }
  loadTables() {
    this.activatedRoute.params
      .pipe(
        filter(data => data.id),
        map(data => data.id),
        switchMap(
          (id) => this.tablesRepository.find(id)
        )
      )
      .subscribe((table: Tables) => {
        this.table = table;
      });
  }
  onSubmit() {
    this.tablesRepository.save(this.table)
      .subscribe((res) => {
        this.closeModal();
      });
  }
  closeModal() {
    (function ($) {
      $(document).ready(function () {
        $('#editTable').modal('hide');
      });
    })(jQuery);
  }

}
