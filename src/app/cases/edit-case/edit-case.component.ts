import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CasesManagement } from 'src/app/shared/entities/case-management';
import { CaseRepository } from 'src/app/shared/repositories/case.repository';
import { CustomerRepository } from 'src/app/shared/repositories/customers.repository';
import { Customer } from 'src/app/shared/entities/customer';
import { map, filter, switchMap } from 'rxjs/operators';
import { UsersRepository } from 'src/app/shared/repositories/users.repository';
import { User } from 'src/app/shared/entities/user';
declare var jQuery: any;

@Component({
  selector: 'app-edit-case',
  templateUrl: './edit-case.component.html',
  styleUrls: ['./edit-case.component.scss']
})
export class EditCaseComponent implements OnInit {
  public searchText: any = [];
  public searchRespon: any = [];
  public users: User[];
  public case_management: CasesManagement = new CasesManagement();
  public customers: Customer[];
  public status: any = [
    { name: 'New', value: 'new' },
    { name: 'Pending', value: 'pending' },
    { name: 'Expired ', value: 'expired ' },
    { name: 'Closed  ', value: 'closed  ' },
  ];
  public priority: any = [
    { name: 'Low', value: 'low' },
    { name: 'Normal', value: 'normal' },
    { name: 'High ', value: 'high' },
    { name: 'Urgent  ', value: 'urgent' },
  ];
  tab: string = 'en';
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private caseManagementRepository: CaseRepository,
    private userRepository: UsersRepository,
    private customerRepository: CustomerRepository,
  ) { }

  ngOnInit() {
    this.filterReport();
    this.loadCaseManagement();
    this.loadListCustomer();
    this.loadUser();
  }
  filterReport() {
    (function ($) {
      $(document).ready(function () {
        $('body, html').on('click', function (event) {
          var target = $(event.target);
          if (!target.is('.search-req , .search-req *, .search, .search *, .mg-0, .mg-0 * ')) {
            $('.show-search-r').removeClass('active');
          }
        });
        $('#search .mg-0').click(function () {
          $(this).parent().addClass('active');
        })
      });
    })(jQuery);
  }
  loadListCustomer() {
    this.customerRepository.all({ status: 1 })
      .subscribe((res?: any) => {
        this.customers = res;
      })
  }
  loadCaseManagement() {
    this.activatedRoute.params
      .pipe(
        filter(data => data.id),
        map(data => data.id),
        switchMap(
          (id) => this.caseManagementRepository.find(id)
        )
      )
      .subscribe((case_management: CasesManagement) => {
        this.case_management = case_management;
        this.case_management.responsible_parties = this.case_management.responsible_parties.map(_i => { return this.case_management.responsible_parties = _i.id });
      });
  }
  loadUser() {
    this.userRepository.all({ status: 1, pagination: 0 })
      .subscribe((res?: any) => {
        this.users = res;
      })
  }
  searchCase($event) {
    let vm = this;
    vm.searchText = vm.customers.filter(function (item) {
      return item.name.toLowerCase().includes(vm.case_management.request.toLowerCase());
    })
  }
  checkReq(item?: any) {
    this.case_management.request = item.name;
    this.searchText = [];
  }
  searchrespon($event) {
    let vm = this;
    vm.searchRespon = vm.users.filter(function (item) {
      return item.name.toLowerCase().includes(vm.case_management.responsible_parties.toLowerCase());
    })
  }
  checkRes(item?: any) {
    this.case_management.responsible_parties = item.name;
    this.searchRespon = [];
  }
  onSubmit() {
    this.caseManagementRepository.save(this.case_management)
      .subscribe((res) => {
        this.cancel();
      });
  }
  cancel() {
    this.router.navigate(['../..', 'list'], { relativeTo: this.activatedRoute });
  }
}
