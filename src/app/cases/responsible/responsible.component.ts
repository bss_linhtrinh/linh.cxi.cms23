import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel } from '@angular/forms';
import { ElementBase } from 'src/app/shared/custom-form/element-base/element-base';
import { of, Subscription } from 'rxjs';
import { delay, map, retryWhen } from 'rxjs/operators';
import { BaseFilterCriteria } from 'src/app/shared/entities/base-filter-criteria';

@Component({
  selector: 'app-responsible',
  templateUrl: './responsible.component.html',
  styleUrls: ['./responsible.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: ResponsibleComponent, multi: true }
  ]
})
export class ResponsibleComponent extends ElementBase<number[]> implements OnInit, OnDestroy, OnChanges {


  @Input() array: any;

  @Input() name: string;
  @Input() label: string;
  @Input() public type: string;
  @Input() public placeholder: string;
  @Input() bindLabel = null;
  @Input() bindValue = null;
  @Input() public items: any[] = [];
  @Output() arrayChange = new EventEmitter();
  @Output() inputCheckboxChange = new EventEmitter();
  subscriptions: Subscription[] = [];

  @ViewChild(NgModel, { static: true }) model: NgModel;
  internalValues = [];

  filterAttrs: BaseFilterCriteria = BaseFilterCriteria.create({ name: 'name', type: 'text', value: '' });

  constructor(
    @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
    @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
  ) {
    super(validators, asyncValidators);
  }

  ngOnInit() {
    this.initTime();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  ngOnChanges(changes: SimpleChanges) {
    // if (changes && changes.array.previousValue !== changes.array.currentValue) {
    //   this.parseValue();
    // }

    this.parseValue();

  }

  initTime() {
    let count = 0;
    const source = of(null);
    const example = source.pipe(
      map(() => {
        if (!this.array || this.array.length === 0) {
          throw 1;
        }
        return 1;
      }),
      retryWhen(errors =>
        errors.pipe(
          // restart in 1 seconds
          delay(500)
        )
      ),
      delay(100),
    );
    const doValueSub = example.subscribe(
      () => this.parseValue()
    );
    this.subscriptions.push(doValueSub);
  }

  parseValue() {
    if (this.array && this.array.length > 0) {
      const internalValues = [];
      // init false
      if (this.items && this.items.length > 0) {
        this.items.forEach(
          (selection: any, index: number) => {
            internalValues[index] = this.array.findIndex(v => v === selection.id) !== -1;
          }
        );
      }
      this.internalValues = internalValues;
    }
  }


  onChange(check: any, item) {
    if (check === true) {
      this.array.push(item.id);
    }
    if (check === false) {
      let index = this.array.findIndex(x => x === item.id);
      if (index !== -1) {
        this.array.splice(index, 1);
      }
    }
    this.arrayChange.emit(this.array);
  }

  onInputCheckboxChange(event) {
    this.inputCheckboxChange.emit(event);
  }

}

