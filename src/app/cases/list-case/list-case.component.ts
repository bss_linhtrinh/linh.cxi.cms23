import { Component, OnInit } from '@angular/core';
import { CxiGridConfig } from '../../shared/components/cxi-grid/cxi-grid-config';
import { CxiGridColumn } from '../../shared/components/cxi-grid/cxi-grid-column';
import { LocalStorage } from 'ngx-webstorage';
import { Router, ActivatedRoute } from '@angular/router';
import { CaseRepository } from 'src/app/shared/repositories/case.repository';
import { CasesManagement } from 'src/app/shared/entities/case-management';
import { ColumnFormat } from '../../shared/types/column-format';
import { ColumnFilterType } from '../../shared/types/column-filter-type';

@Component({
  selector: 'app-list-case',
  templateUrl: './list-case.component.html',
  styleUrls: ['./list-case.component.scss']
})
export class ListCaseComponent implements OnInit {
  // table
  cxiGridConfig = CxiGridConfig.from({
    paging: true,
    sorting: true,
    filtering: true,
    selecting: true,
    pagingServer: true,
    repository: this.caseManagementRepository,
    translatable: true,
    actions: {
      onEdit: (caseManagement: CasesManagement) => {
        return this.onEdit(caseManagement);
      },
      onDelete: (caseManagement: CasesManagement) => {
        return this.onDelete(caseManagement);
      },
      onComment: (caseManagement: CasesManagement) => {
        return this.onComment(caseManagement);
      },
      onActivate: (caseManagement: CasesManagement) => {
        this.onActivate(caseManagement);
      },
      onDeactivate: (caseManagement: CasesManagement) => {
        this.onDeactivate(caseManagement);
      },
    }
  });
  cxiGridColumns = CxiGridColumn.from([
    { title: 'title', name: 'name' },
    { title: 'expiration date', name: 'expired_at', format: 'dateTime', sort: true, filterType: ColumnFilterType.Datepicker },
    { title: 'responsible parties', name: 'responsible_parties', fieldOption: 'first_name', fieldOption1: 'last_name', format: ColumnFormat.Fullname, sortable: false },
    {
      title: 'priority',
      name: 'priority',
      filterType: ColumnFilterType.Select,
      filterData: [
        { name: 'Low', id: 'low' },
        { name: 'Normal', id: 'normal' },
        { name: 'High ', id: 'high' },
        { name: 'Urgent  ', id: 'urgent' },
      ]
    },
    {
      title: 'status',
      name: 'status',
      filterType: ColumnFilterType.Select,
      filterData: [
        { name: 'New', id: 'new' },
        { name: 'Pending', id: 'pending' },
        { name: 'Expired ', id: 'expired ' },
        { name: 'Closed  ', id: 'closed  ' },
      ]
    },
    { title: 'created date', name: 'created_at', format: 'dateTime', sort: true, filterType: ColumnFilterType.Datepicker },
  ]);

  @LocalStorage('scope.organization_id') organizationId: number;
  @LocalStorage('scope.brand_id') brandId: number;
  @LocalStorage('scope.outlet_id') outletId: number;
  constructor(
    private caseManagementRepository: CaseRepository,
    private router: Router,
    public activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
  }
  onEdit(caseManagement: CasesManagement) {
    this.router.navigate(['..', caseManagement.id, 'edit'], { relativeTo: this.activatedRoute });
  }
  onComment(caseManagement: CasesManagement) {
    this.router.navigate(['..', caseManagement.id, 'comment-case'], { relativeTo: this.activatedRoute });
  }
  onDelete(caseManagement: CasesManagement) {
    this.caseManagementRepository.destroy(caseManagement).subscribe();
  }
  onActivate(caseManagement: CasesManagement) {
    this.caseManagementRepository.activate(caseManagement).subscribe();
  }

  onDeactivate(caseManagement: CasesManagement) {
    this.caseManagementRepository.deactivate(caseManagement).subscribe();
  }
}
