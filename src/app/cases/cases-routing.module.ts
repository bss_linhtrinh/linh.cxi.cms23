import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CheckPermissionsGuard } from '../shared/guards/check-permissions/check-permissions.guard';
import { ListCaseComponent } from './list-case/list-case.component';
import { CreateCaseComponent } from './create-case/create-case.component';
import { EditCaseComponent } from './edit-case/edit-case.component';
import { CommentCaseComponent } from './comment-case/comment-case.component';


const routes: Routes = [
  {
    path: '',
    canActivate: [CheckPermissionsGuard],
    children: [
      { path: 'list', component: ListCaseComponent, data: { breadcrumb: 'List Case' } },
      { path: 'create', component: CreateCaseComponent, data: { breadcrumb: 'Create new Case' } },
      { path: ':id/edit', component: EditCaseComponent, data: { breadcrumb: 'Edit Case' } },
      { path: ':id/comment-case', component: CommentCaseComponent, data: { breadcrumb: 'Comment Case' } },
      { path: '', redirectTo: 'list', pathMatch: 'full' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CasesRoutingModule { }
