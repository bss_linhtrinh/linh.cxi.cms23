import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CasesRoutingModule } from './cases-routing.module';
import { CreateCaseComponent } from './create-case/create-case.component';
import { ListCaseComponent } from './list-case/list-case.component';
import { EditCaseComponent } from './edit-case/edit-case.component';
import {SharedModule} from 'src/app/shared/shared.module';
import {FormsModule} from '@angular/forms';
import { CommentCaseComponent } from './comment-case/comment-case.component';
import { ResponsibleComponent } from './responsible/responsible.component';


@NgModule({
  declarations: [CreateCaseComponent, ListCaseComponent, EditCaseComponent, CommentCaseComponent, ResponsibleComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    CasesRoutingModule
  ]
})
export class CasesModule { }
