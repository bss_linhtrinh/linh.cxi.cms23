import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CasesManagement } from 'src/app/shared/entities/case-management';
import { CaseComment } from 'src/app/shared/entities/comment-case';
import { CaseRepository } from 'src/app/shared/repositories/case.repository';
import { map, filter, switchMap } from 'rxjs/operators';
import { CaseCommentRepository } from 'src/app/shared/repositories/case-comment.repository';
import { LocalStorage } from 'ngx-webstorage';
import { CaseCommentTemplateRepository } from 'src/app/shared/repositories/case-comment-repository';


@Component({
  selector: 'app-comment-case',
  templateUrl: './comment-case.component.html',
  styleUrls: ['./comment-case.component.scss']
})
export class CommentCaseComponent implements OnInit {
  public case_management: CasesManagement = new CasesManagement();
  public case_managements: CaseComment[];
  public case_comment: CaseComment = new CaseComment();
  public case_comments: CaseComment[] = [];
  public status: any = [
    { name: 'New', value: 'new' },
    { name: 'Pending', value: 'pending' },
    { name: 'Expired', value: 'expired' },
    { name: 'Closed', value: 'closed' },
  ];
  public priority: any = [
    { name: 'Low', value: 'low' },
    { name: 'Normal', value: 'normal' },
    { name: 'High', value: 'high' },
    { name: 'Urgent', value: 'urgent' },
  ];
  public _status: string = 'pending';
  public _priority: string = 'normal';
  public _expired_at: string = null;
  @LocalStorage('pagination.current_page') current_page: number = 1;
  @LocalStorage('pagination.per_page') per_page: number = 20;
  @LocalStorage('pagination.total') total: number;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private caseManagementRepository: CaseRepository,
    private caseCommentTemplateRepository: CaseCommentTemplateRepository,
    private caseCommenttRepository: CaseCommentRepository,
  ) { }

  ngOnInit() {
    this.loadCaseManagement();
    //this.getlistCaseCommentTemplate();
  }
  loadCaseManagement() {
    this.activatedRoute.params
      .pipe(
        filter(data => data.id),
        map(data => data.id),
        switchMap(
          (id) => this.caseManagementRepository.find(id)
        )
      )
      .subscribe((case_management: CasesManagement) => {
        this.case_management = case_management;
        this.getlistCaseComment();
      });
  }
  getlistCaseCommentTemplate() {
    this.caseCommentTemplateRepository.all()
      .subscribe((res?: any) => {
        this.case_managements = res;
      })
  }
  createComment() {
    this.case_comment.case_id = this.case_management.id;
    this.caseCommenttRepository.save(this.case_comment)
      .subscribe((res?: any) => {
        //this.getlistCaseComment();
        this.case_comment.content = '';
        this.UpdateCase();
      })
  }
  getlistCaseComment() {
    let sen_data = {
      order_by: "created_at",
      page: this.current_page,
      per_page: this.per_page,
      order: "desc",
      case_id: this.case_management.id
    }
    this.caseCommenttRepository.all(sen_data)
      .subscribe((res?: any) => {
        this.case_comments = res;
      })
  }
  onChangePagination() {
    this.getlistCaseComment();
  }
  cancel() {
    this.router.navigate(['../..', 'list'], { relativeTo: this.activatedRoute });
  }
  editCase() {
    this.router.navigate(['../..', this.case_management.id, 'edit'], { relativeTo: this.activatedRoute });
  }
  UpdateCase() {
    this.case_management.responsible_parties = this.case_management.responsible_parties.map(item => {
      return this.case_management.responsible_parties = item.id;
    })
    this.case_management.status = this._status;
    this.case_management.priority = this._priority;
    this.case_management.expired_at = this._expired_at;
    this.caseManagementRepository.save(this.case_management)
      .subscribe((res) => {
        this.loadCaseManagement();
      });
  }
}
