import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentCaseComponent } from './comment-case.component';

describe('CommentCaseComponent', () => {
  let component: CommentCaseComponent;
  let fixture: ComponentFixture<CommentCaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentCaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
