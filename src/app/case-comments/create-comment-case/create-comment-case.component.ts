import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CaseComment } from 'src/app/shared/entities/case-comment';
import { CaseCommentTemplateRepository } from 'src/app/shared/repositories/case-comment-repository';
@Component({
  selector: 'app-create-comment-case',
  templateUrl: './create-comment-case.component.html',
  styleUrls: ['./create-comment-case.component.scss']
})
export class CreateCommentCaseComponent implements OnInit {
  public case_comment: CaseComment = new CaseComment();
  tab: string = 'en';
  constructor(
    private caseCommentTyepeRepository: CaseCommentTemplateRepository,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
  }
  onSubmit() {
    this.case_comment.translations = null;
    this.caseCommentTyepeRepository.save(this.case_comment)
      .subscribe(
        (res) => {
          this.router.navigate(['../list'], { relativeTo: this.route });
        }
      );
  }
}
