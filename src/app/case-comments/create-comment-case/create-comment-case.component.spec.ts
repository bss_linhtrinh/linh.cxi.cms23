import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCommentCaseComponent } from './create-comment-case.component';

describe('CreateCommentCaseComponent', () => {
  let component: CreateCommentCaseComponent;
  let fixture: ComponentFixture<CreateCommentCaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCommentCaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCommentCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
