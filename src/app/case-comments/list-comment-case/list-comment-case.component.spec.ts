import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCommentCaseComponent } from './list-comment-case.component';

describe('ListCommentCaseComponent', () => {
  let component: ListCommentCaseComponent;
  let fixture: ComponentFixture<ListCommentCaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCommentCaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCommentCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
