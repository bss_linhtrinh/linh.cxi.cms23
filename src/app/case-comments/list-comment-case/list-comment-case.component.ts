import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalStorage } from 'ngx-webstorage';
import { CxiGridConfig } from '../../shared/components/cxi-grid/cxi-grid-config';
import { CxiGridColumn } from '../../shared/components/cxi-grid/cxi-grid-column';
import { CaseComment } from 'src/app/shared/entities/case-comment';
import { CaseCommentTemplateRepository } from 'src/app/shared/repositories/case-comment-repository';
import { ColumnFilterType } from '../../shared/types/column-filter-type';
@Component({
  selector: 'app-list-comment-case',
  templateUrl: './list-comment-case.component.html',
  styleUrls: ['./list-comment-case.component.scss']
})
export class ListCommentCaseComponent implements OnInit {
  public case_comment_templates: CaseComment[] = [];
  cxiGridConfig = CxiGridConfig.from({
    paging: true,
    sorting: true,
    filtering: true,
    selecting: true,
    pagingServer: true,
    repository: this.caseCommentTemplateRepository,
    translatable: true,
    actions: {
      onEdit: (case_comment_templates: CaseComment) => {
        return this.onEdit(case_comment_templates);
      },
      onDelete: (case_comment_templates: CaseComment) => {
        return this.onDelete(case_comment_templates);
      },
      onActivate: (case_comment_templates: CaseComment) => {
        this.onActivate(case_comment_templates);
      },
      onDeactivate: (case_comment_templates: CaseComment) => {
        this.onDeactivate(case_comment_templates);
      },
    }
  });
  cxiGridColumns = CxiGridColumn.from([
    { title: 'title', name: 'title' },
    { title: 'content', name: 'content' },
    { title: 'created date', name: 'created_at', format: 'dateTime', filterType: ColumnFilterType.Datepicker, sort: true,  },
  ]);

  @LocalStorage('scope.organization_id') organizationId: number;
  @LocalStorage('scope.brand_id') brandId: number;
  @LocalStorage('scope.outlet_id') outletId: number;

  constructor(
    private caseCommentTemplateRepository: CaseCommentTemplateRepository,
    private router: Router,
    public activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
  }
  onEdit(case_comment_templates: CaseComment) {
    this.router.navigate(['..', case_comment_templates.id, 'edit'], { relativeTo: this.activatedRoute });
  }

  onDelete(case_comment_templates: CaseComment) {
    this.caseCommentTemplateRepository.destroy(case_comment_templates).subscribe();
  }
  onActivate(case_comment_templates: CaseComment) {
    this.caseCommentTemplateRepository.activate(case_comment_templates).subscribe();
  }

  onDeactivate(case_comment_templates: CaseComment) {
    this.caseCommentTemplateRepository.deactivate(case_comment_templates).subscribe();
  }
}
