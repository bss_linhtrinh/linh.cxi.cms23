import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCommentCaseComponent } from './edit-comment-case.component';

describe('EditCommentCaseComponent', () => {
  let component: EditCommentCaseComponent;
  let fixture: ComponentFixture<EditCommentCaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCommentCaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCommentCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
