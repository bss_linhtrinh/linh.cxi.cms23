import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CaseComment } from 'src/app/shared/entities/case-comment';
import { CaseCommentTemplateRepository } from 'src/app/shared/repositories/case-comment-repository';
import { filter, switchMap, map } from 'rxjs/operators';

@Component({
  selector: 'app-edit-comment-case',
  templateUrl: './edit-comment-case.component.html',
  styleUrls: ['./edit-comment-case.component.scss']
})
export class EditCommentCaseComponent implements OnInit {

  public case_comment: CaseComment = new CaseComment();
  tab: string = 'en';
  constructor(
    private caseCommentTyepeRepository: CaseCommentTemplateRepository,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.loadCommentTemplate();
  }
  loadCommentTemplate() {
    this.activatedRoute.params
      .pipe(
        filter(data => data.id),
        map(data => data.id),
        switchMap(
          (id) => this.caseCommentTyepeRepository.find(id)
        )
      )
      .subscribe((res?: any) => {
        let vm = this;
        vm.case_comment = res;
        this.case_comment.translations.forEach(item => {
          if (item.locale == 'en') {
            this.case_comment.en = item;
          } else if (item.locale == 'vi') {
            this.case_comment.vi = item;
          }
        });
      });
  }
  onSubmit() {
    this.case_comment.translations = null;
    this.caseCommentTyepeRepository.save(this.case_comment)
      .subscribe((res) => {
        this.cancel();
      });
  }
  cancel() {
    this.router.navigate(['../..', 'list'], { relativeTo: this.activatedRoute });
  }
}
