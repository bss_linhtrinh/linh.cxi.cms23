import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CaseCommentsRoutingModule } from './case-comments-routing.module';
import { ListCommentCaseComponent } from './list-comment-case/list-comment-case.component';
import { CreateCommentCaseComponent } from './create-comment-case/create-comment-case.component';
import { EditCommentCaseComponent } from './edit-comment-case/edit-comment-case.component';
import {SharedModule} from '../shared/shared.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [ListCommentCaseComponent, CreateCommentCaseComponent, EditCommentCaseComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    CaseCommentsRoutingModule
  ]
})
export class CaseCommentsModule { }
