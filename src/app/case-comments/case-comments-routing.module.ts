import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CheckPermissionsGuard} from '../shared/guards/check-permissions/check-permissions.guard';
import { CreateCommentCaseComponent } from './create-comment-case/create-comment-case.component';
import { ListCommentCaseComponent } from './list-comment-case/list-comment-case.component';
import { EditCommentCaseComponent } from './edit-comment-case/edit-comment-case.component';


const routes: Routes = [
  {
    path: '',
    canActivate: [CheckPermissionsGuard],
    children: [
      { path: 'create', component: CreateCommentCaseComponent, data: { breadcrumb: 'Create new comment case template' } },
      { path: 'list', component: ListCommentCaseComponent, data: { breadcrumb: 'List comment case template' } },
      { path: ':id/edit', component: EditCommentCaseComponent, data: { breadcrumb: 'Edit comment case template' } },
      { path: '', redirectTo: 'list', pathMatch: 'full' },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CaseCommentsRoutingModule { }
