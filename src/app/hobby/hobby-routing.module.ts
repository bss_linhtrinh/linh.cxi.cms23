import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CheckPermissionsGuard} from '../shared/guards/check-permissions/check-permissions.guard';
import { ListHobbyComponent } from './list-hobby/list-hobby.component';
import { CreateHobbyComponent } from './create-hobby/create-hobby.component';
import { EditHobbyComponent } from './edit-hobby/edit-hobby.component';


const routes: Routes = [
  {
    path: '',
    canActivate: [CheckPermissionsGuard],
    children: [
      { path: 'list', component: ListHobbyComponent, data: { breadcrumb: 'List hobby' } },
      { path: 'create', component: CreateHobbyComponent, data: { breadcrumb: 'Create new hobby' } },
      { path: ':id/edit', component: EditHobbyComponent, data: { breadcrumb: 'Edit hobby' } },
      { path: '', redirectTo: 'list', pathMatch: 'full' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HobbyRoutingModule { }
