import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { HobbyRepository } from 'src/app/shared/repositories/hobby.repository';
import { Hobby } from 'src/app/shared/entities/hobby';
@Component({
  selector: 'app-create-hobby',
  templateUrl: './create-hobby.component.html',
  styleUrls: ['./create-hobby.component.scss']
})
export class CreateHobbyComponent implements OnInit {
  public hobby: Hobby = new Hobby();
  userScope: UserScope;
  public brand = [];
  tab: string = 'en';
  constructor(
    private hobbyRepository: HobbyRepository,
    private router: Router,
    private route: ActivatedRoute,
    private scopeService: ScopeService,
    private brandRepository: BrandsRepository,
  ) { }

  ngOnInit() {
    this.loadListBrand();
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.hobby.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  loadListBrand() {
    this.brandRepository.all()
      .subscribe(
        (res) => {
          this.brand = res;
          this.hobby.brand_id = this.brand[0].id;
          this.getListUserScope();
        }
      );
  }
  onSubmit() {
    this.hobby.translations = null;
    this.hobbyRepository.save(this.hobby)
      .subscribe(
        (res) => {
          this.router.navigate(['../list'], { relativeTo: this.route });
        }
      );
  }
}
