import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateHobbyComponent } from './create-hobby.component';

describe('CreateHobbyComponent', () => {
  let component: CreateHobbyComponent;
  let fixture: ComponentFixture<CreateHobbyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateHobbyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateHobbyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
