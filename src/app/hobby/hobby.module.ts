import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HobbyRoutingModule } from './hobby-routing.module';
import { ListHobbyComponent } from './list-hobby/list-hobby.component';
import { CreateHobbyComponent } from './create-hobby/create-hobby.component';
import { EditHobbyComponent } from './edit-hobby/edit-hobby.component';
import {SharedModule} from 'src/app/shared/shared.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [ListHobbyComponent, CreateHobbyComponent, EditHobbyComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    HobbyRoutingModule
  ]
})
export class HobbyModule { }
