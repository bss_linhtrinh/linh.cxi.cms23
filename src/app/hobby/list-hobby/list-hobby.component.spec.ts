import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListHobbyComponent } from './list-hobby.component';

describe('ListHobbyComponent', () => {
  let component: ListHobbyComponent;
  let fixture: ComponentFixture<ListHobbyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListHobbyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListHobbyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
