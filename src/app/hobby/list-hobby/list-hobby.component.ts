import { Component, OnInit } from '@angular/core';
import { CxiGridConfig } from '../../shared/components/cxi-grid/cxi-grid-config';
import { CxiGridColumn } from '../../shared/components/cxi-grid/cxi-grid-column';
import { LocalStorage } from 'ngx-webstorage';
import { Router, ActivatedRoute } from '@angular/router';
import { Hobby } from 'src/app/shared/entities/hobby';
import { HobbyRepository } from 'src/app/shared/repositories/hobby.repository';
@Component({
  selector: 'app-list-hobby',
  templateUrl: './list-hobby.component.html',
  styleUrls: ['./list-hobby.component.scss']
})
export class ListHobbyComponent implements OnInit {
  // table
  cxiGridConfig = CxiGridConfig.from({
    paging: true,
    sorting: true,
    filtering: true,
    selecting: true,
    pagingServer: true,
    repository: this.hobbyRepository,
    translatable: true,
    actions: {
      onEdit: (hobby: Hobby) => {
        return this.onEdit(hobby);
      },
      onDelete: (hobby: Hobby) => {
        return this.onDelete(hobby);
      },
      // onActivate: (addonType: AddOnType) => {
      //     this.onActivate(addonType);
      // },
      // onDeactivate: (addonType: AddOnType) => {
      //     this.onDeactivate(addonType);
      // },
    }
  });
  cxiGridColumns = CxiGridColumn.from([
    { title: 'name', name: 'name' },
    { title: 'description', name: 'description' },
  ]);
  @LocalStorage('scope.organization_id') organizationId: number;
  @LocalStorage('scope.brand_id') brandId: number;
  @LocalStorage('scope.outlet_id') outletId: number;
  constructor(
    private hobbyRepository: HobbyRepository,
    private router: Router,
    public activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
  }
  onEdit(hobby: Hobby) {
    this.router.navigate(['..', hobby.id, 'edit'], { relativeTo: this.activatedRoute });
  }

  onDelete(hobby: Hobby) {
    this.hobbyRepository.destroy(hobby).subscribe();
  }
}
