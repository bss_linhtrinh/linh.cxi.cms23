import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { map, filter, switchMap } from 'rxjs/operators';
import { HobbyRepository } from 'src/app/shared/repositories/hobby.repository';
import { Hobby } from 'src/app/shared/entities/hobby';

@Component({
  selector: 'app-edit-hobby',
  templateUrl: './edit-hobby.component.html',
  styleUrls: ['./edit-hobby.component.scss']
})
export class EditHobbyComponent implements OnInit {
  public hobby: Hobby = new Hobby();
  userScope: UserScope;
  public brand = [];
  tab: string = 'en';
  constructor(
    private hobbyRepository: HobbyRepository,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private scopeService: ScopeService,
    private brandRepository: BrandsRepository,
  ) { }

  ngOnInit() {
    this.loadHobby();
    this.loadListBrand();
  }
  loadHobby() {
    this.activatedRoute.params
      .pipe(
        filter(data => data.id),
        map(data => data.id),
        switchMap(
          (id) => this.hobbyRepository.find(id)
        )
      )
      .subscribe((hobby: Hobby) => {
        this.hobby = hobby;
        this.hobby.translations.forEach(item => {
          if (item.locale == 'en') {
            this.hobby.en = item;
          } else if (item.locale == 'vi') {
            this.hobby.vi = item;
          }
        });
      });
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.hobby.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  loadListBrand() {
    this.brandRepository.all()
      .subscribe(
        (res) => {
          this.brand = res;
          this.getListUserScope();
        }
      );
  }
  onSubmit() {
    this.hobby.translations = null;
    this.hobbyRepository.save(this.hobby)
      .subscribe((res) => {
        this.cancel();
      });
  }
  cancel() {
    this.router.navigate(['../..', 'list'], { relativeTo: this.activatedRoute });
  }
}
