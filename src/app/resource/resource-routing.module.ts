import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
    // dashboard
    {
        path: 'events',
        loadChildren: () => import('./resource-events/resource-events.module').then(mod => mod.ResourceEventsModule),
        data: { breadcrumb: 'Events' }
    },
    {
        path: 'news',
        loadChildren: () => import('./resource-news/resource-news.module').then(mod => mod.ResourceNewsModule),
        data: { breadcrumb: 'News' }
    },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResourceRoutingModule { }
