import {Component, OnInit} from '@angular/core';
import {ResourceNews} from '../../../shared/entities/resource-news';
import {ActivatedRoute, Router} from '@angular/router';
import {ResourceNewsRepository} from '../../../shared/repositories/resource-news.repository';
import {SessionStorage} from 'ngx-webstorage';
import {Language} from '../../../shared/entities/language';

@Component({
    selector: 'app-edit-news',
    templateUrl: './edit-news.component.html',
    styleUrls: ['./edit-news.component.scss']
})
export class EditNewsComponent implements OnInit {

    news: ResourceNews = new ResourceNews();
    tab: any;

    @SessionStorage('languages') languages: Language[];

    constructor(private resourceNewsRepository: ResourceNewsRepository,
                private router: Router,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.getNewsDetail();
    }

    getNewsDetail() {
        this.activatedRoute.params.subscribe((data) => {
            const id = data.id;
            this.resourceNewsRepository.find(id).subscribe((res) => {
                this.news = res;
            });
        });
    }

    onSubmit() {
        this.resourceNewsRepository.save(this.news)
            .subscribe(
                () => this.back()
            );
    }

    back() {
        this.router.navigate(['..'], {relativeTo: this.activatedRoute});
    }
}
