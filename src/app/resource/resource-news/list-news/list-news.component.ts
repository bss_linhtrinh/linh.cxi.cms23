import {Component, OnInit} from '@angular/core';
import {CxiGridColumn} from '../../../shared/components/cxi-grid/cxi-grid-column';
import {ActivatedRoute, Router} from '@angular/router';
import {ResourceNews} from '../../../shared/entities/resource-news';
import {ResourceNewsRepository} from '../../../shared/repositories/resource-news.repository';

@Component({
    selector: 'app-list-news',
    templateUrl: './list-news.component.html',
    styleUrls: ['./list-news.component.scss']
})
export class ListNewsComponent implements OnInit {

    cxiTableConfig = {
        paging: true,
        sorting: true,
        filtering: true,
        pagingServer: true,
        translatable: true,
        selecting: true,
        contentType: 'news',
        repository: this.resourceNewsRepository,
        actions: {
            onEdit: (row: any) => {
                this.onEdit(row);
            },
            onDelete: (row: any) => {
                this.onDelete(row);
            },
            onActivate: (row: any) => {
                this.onActivate(row);
            },
            onDeactivate: (row: any) => {
                this.onDeactivate(row);
            }
        },
    };
    cxiTableColumns = CxiGridColumn.from([
        {title: 'Title', name: 'title', avatar: 'base_image', sort: true, className: 'title'},
        {title: 'Content', name: 'content', sort: true, className: 'content', format: 'rich-text'},
    ]);

    constructor(private resourceNewsRepository: ResourceNewsRepository,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
    }

    onEdit(news: ResourceNews) {
        this.router.navigate(['..', news.id], {relativeTo: this.activatedRoute});
    }

    onDelete(news: ResourceNews) {
        this.resourceNewsRepository.destroy(news)
            .subscribe();
    }

    onActivate(news: ResourceNews) {
        this.resourceNewsRepository.activate(news).subscribe();
    }

    onDeactivate(news: ResourceNews) {
        this.resourceNewsRepository.deactivate(news).subscribe();
    }
}
