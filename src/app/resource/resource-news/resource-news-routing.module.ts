import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListNewsComponent} from './list-news/list-news.component';
import {CreateNewsComponent} from './create-news/create-news.component';
import {EditNewsComponent} from './edit-news/edit-news.component';


const routes: Routes = [
    {
        path: '',
        canActivate: [],
        children: [
            {
                path: 'list',
                component: ListNewsComponent,
                data: {breadcrumb: 'List news'}
            },
            {
                path: 'create',
                component: CreateNewsComponent,
                data: {breadcrumb: 'Create news'}
            },
            {
                path: ':id',
                component: EditNewsComponent,
                data: {breadcrumb: 'Edit news'}
            },
            {path: '', redirectTo: 'list', pathMatch: 'full'},
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResourceNewsRoutingModule { }
