import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ResourceNewsRoutingModule} from './resource-news-routing.module';
import {CreateNewsComponent} from './create-news/create-news.component';
import {ListNewsComponent} from './list-news/list-news.component';
import {EditNewsComponent} from './edit-news/edit-news.component';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule} from '@angular/forms';


@NgModule({
    declarations: [CreateNewsComponent, ListNewsComponent, EditNewsComponent],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        ResourceNewsRoutingModule
    ]
})
export class ResourceNewsModule {
}
