import {Component, OnInit} from '@angular/core';
import {ResourceNews} from '../../../shared/entities/resource-news';
import {ActivatedRoute, Router} from '@angular/router';
import {ResourceNewsRepository} from '../../../shared/repositories/resource-news.repository';
import {SessionStorage} from 'ngx-webstorage';
import {Language} from '../../../shared/entities/language';

@Component({
    selector: 'app-create-news',
    templateUrl: './create-news.component.html',
    styleUrls: ['./create-news.component.scss']
})
export class CreateNewsComponent implements OnInit {

    news: ResourceNews = new ResourceNews();
    tab: any;

    @SessionStorage('languages') languages: Language[];

    constructor(private resourceNewsRepository: ResourceNewsRepository,
                private router: Router,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
    }

    onSubmit() {
        this.resourceNewsRepository.save(this.news)
            .subscribe(
                () => this.back()
            );
    }

    back() {
        this.router.navigate(['..'], {relativeTo: this.activatedRoute});
    }
}
