import {Component, OnInit} from '@angular/core';
import {ResourceEvent} from '../../../shared/entities/resource-event';
import {ResourceEventRepository} from '../../../shared/repositories/resource-event.repository';
import {ActivatedRoute, Router} from '@angular/router';
import {SessionStorage} from 'ngx-webstorage';
import {Language} from '../../../shared/entities/language';

@Component({
    selector: 'app-edit-event',
    templateUrl: './edit-event.component.html',
    styleUrls: ['./edit-event.component.scss']
})
export class EditEventComponent implements OnInit {

    minDate = new Date();
    tab: any;

    @SessionStorage('languages') languages: Language[];

    event: ResourceEvent = new ResourceEvent();

    constructor(private resourceEventRepository: ResourceEventRepository,
                private router: Router,
                private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {
        this.getEventDetail();
    }

    getEventDetail() {
        this.activatedRoute.params.subscribe((data) => {
            const id = data.id;
            this.resourceEventRepository.find(id).subscribe((res) => {
                this.event = res;
            });
        });
    }

    onSubmit() {
        this.resourceEventRepository.save(this.event)
            .subscribe(
                () => this.back()
            );
    }

    back() {
        this.router.navigate(['..'], {relativeTo: this.activatedRoute});
    }

}
