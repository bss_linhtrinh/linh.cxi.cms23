import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ResourceEventsRoutingModule} from './resource-events-routing.module';
import {CreateEventComponent} from './create-event/create-event.component';
import {ListEventComponent} from './list-event/list-event.component';
import {EditEventComponent} from './edit-event/edit-event.component';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule} from '@angular/forms';


@NgModule({
    declarations: [CreateEventComponent, ListEventComponent, EditEventComponent],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        ResourceEventsRoutingModule
    ]
})
export class ResourceEventsModule {
}
