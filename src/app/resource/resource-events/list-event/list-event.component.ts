import { Component, OnInit } from '@angular/core';
import {CxiGridColumn} from '../../../shared/components/cxi-grid/cxi-grid-column';
import {ActivatedRoute, Router} from '@angular/router';
import {ResourceEventRepository} from '../../../shared/repositories/resource-event.repository';
import {ResourceEvent} from '../../../shared/entities/resource-event';
import {ColumnFilterType} from '../../../shared/types/column-filter-type';

@Component({
  selector: 'app-list-event',
  templateUrl: './list-event.component.html',
  styleUrls: ['./list-event.component.scss']
})
export class ListEventComponent implements OnInit {

    cxiTableConfig = {
        paging: true,
        sorting: true,
        filtering: true,
        translatable: true,
        pagingServer: true,
        selecting: true,
        contentType: 'events',
        repository: this.resourceEventRepository,
        actions: {
            onEdit: (row: any) => {
                this.onEdit(row);
            },
            onDelete: (row: any) => {
                this.onDelete(row);
            },
            onActivate: (row: any) => {
                this.onActivate(row);
            },
            onDeactivate: (row: any) => {
                this.onDeactivate(row);
            }
        },
    };
    cxiTableColumns = CxiGridColumn.from([
        {title: 'Title', name: 'title', avatar: 'base_image', sort: true, className: 'title'},
        {title: 'Start Time', name: 'start_time', sort: true, format: 'datetime' , filterType: ColumnFilterType.Datepicker},
        {title: 'End Time', name: 'end_time', sort: true,  format: 'datetime', filterType: ColumnFilterType.Datepicker},
        {title: 'Content', name: 'content', sort: true, className: 'content', format: 'rich-text'},
    ]);

    constructor(private resourceEventRepository: ResourceEventRepository,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
    }


    ngOnInit() {
    }

    onEdit(event: ResourceEvent) {
        this.router.navigate(['..', event.id], {relativeTo: this.activatedRoute});
    }

    onDelete(event: ResourceEvent) {
        this.resourceEventRepository.destroy(event)
            .subscribe();
    }

    onActivate(event: ResourceEvent) {
        this.resourceEventRepository.activate(event).subscribe();
    }
    onDeactivate(event: ResourceEvent) {
        this.resourceEventRepository.deactivate(event).subscribe();
    }
}
