import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListEventComponent} from './list-event/list-event.component';
import {CreateEventComponent} from './create-event/create-event.component';
import {EditEventComponent} from './edit-event/edit-event.component';


const routes: Routes = [
    {
        path: '',
        canActivate: [],
        children: [
            {
                path: 'list',
                component: ListEventComponent,
                data: {breadcrumb: 'List events'}
            },
            {
                path: 'create',
                component: CreateEventComponent,
                data: {breadcrumb: 'Create event'}
            },
            {
                path: ':id',
                component: EditEventComponent,
                data: {breadcrumb: 'Edit event'}
            },
            {path: '', redirectTo: 'list', pathMatch: 'full'},
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResourceEventsRoutingModule { }
