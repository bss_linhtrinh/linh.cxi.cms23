import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../../shared/services/auth/auth.service';
import {Router} from '@angular/router';
import {NotificationService} from '../../../shared/services/noitification/notification.service';
import {TranslateService} from '@ngx-translate/core';
import {Provider} from '../../../shared/types/providers';
import {filter, finalize, map, startWith, switchMap, takeWhile, tap} from 'rxjs/operators';
import {LocalStorage} from 'ngx-webstorage';
import {BehaviorSubject, NEVER, Observable, Subscription, timer} from 'rxjs';
import {CheckDomainService} from '../../../shared/services/check-domain/check-domain.service';

@Component({
    selector: 'app-resend',
    templateUrl: './resend.component.html',
    styleUrls: ['./resend.component.scss']
})
export class ResendComponent implements OnInit, OnDestroy {
    @LocalStorage('fg.email') email: string;
    @LocalStorage('fg.is_resend') is_resend: boolean;
    @LocalStorage('fg.provider') provider: string;
    @LocalStorage('fg.resend_countdown') resend_countdown: number;

    loading: boolean;
    subscriptions: Subscription[] = [];

    // countdown
    toggle$: Observable<boolean>;
    ms$: Observable<number>;
    minutes$: Observable<string>;
    seconds$: Observable<string>;
    isDisabledResend: boolean;

    constructor(
        private auth: AuthService,
        private router: Router,
        private checkDomainService: CheckDomainService,
        private notificationService: NotificationService,
        private translateService: TranslateService
    ) {
    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

    checkIsSuperAdmin() {
        return this.checkDomainService.isCxiDomain();
    }

    submit() {
        let forgotPasswordSource: any;

        if (this.checkIsSuperAdmin()) {
            const data = {
                email: this.email,
                provider: Provider.SuperAdmin
            };
            forgotPasswordSource = this.auth.forgotPasswordSuperAdmin(data);
        } else {
            const data = {
                username: this.email,
                is_resend: !!this.is_resend,
                provider: Provider.Admin
            };
            forgotPasswordSource = this.auth.forgotPassword(data);
        }

        this.loading = true;
        forgotPasswordSource
            .pipe(
                finalize(() => {
                    this.loading = false;
                    this.startCountDown();
                }),
                tap(() => this.handleSuccess())
            )
            .subscribe(
                () => {
                },
                (error) => this.handleErrors(error)
            );
    }

    handleSuccess() {
        const message = `ForgotPassword.Reset link has been sent`;

        this.translateService.get(message)
            .pipe(
                tap((msg: any) => this.notificationService.success([msg])),
            )
            .subscribe(
            );
    }

    handleErrors(error) {
        let message = '';
        const errorStatus = error.status;
        const errorData = error.error;

        if (typeof error === 'string') {
            message += error;
        } else {
            switch (errorStatus) {
                case 400:
                case 401:
                case 402:
                case 403:
                case 404:
                case 422:
                    if (errorData) {
                        if (errorData.errors) {
                            const errors = errorData.errors;
                            for (const e of errors) {
                                message += '<p>' + e.message + '</p>';
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        this.notificationService.error(message);
    }

    // start count down
    startCountDown() {
        const K = 1000;
        const INTERVAL = K;
        const MINUTES = 5;
        const TIME = MINUTES * K * 60;

        let current: number;
        let time = TIME;

        const toMinutesDisplay = (ms: number) => Math.floor(ms / K / 60);
        const toSecondsDisplay = (ms: number) => Math.floor(ms / K) % 60;
        const toSecondsDisplayString = (ms: number) => {
            const seconds = toSecondsDisplay(ms);
            return seconds < 10 ? `0${seconds}` : seconds.toString();
        };

        const currentSeconds = () => time / INTERVAL;
        const toMs = (t: number) => t * INTERVAL;
        const toRemainingSeconds = (t: number) => currentSeconds() - t;

        this.toggle$ = new BehaviorSubject(true);
        const remainingSeconds$ = this.toggle$.pipe(
            switchMap((running: boolean) => (running ? timer(0, INTERVAL) : NEVER)),
            map(toRemainingSeconds),
            takeWhile(t => t >= 0),
        );

        this.ms$ = remainingSeconds$.pipe(
            map(toMs),
            tap((t: number) => current = t)
        );
        this.minutes$ = this.ms$.pipe(
            map(toMinutesDisplay),
            map(s => s.toString()),
            startWith(toMinutesDisplay(time).toString())
        );
        this.seconds$ = this.ms$.pipe(
            map(toSecondsDisplayString),
            startWith(toSecondsDisplayString(time).toString())
        );

        // update current time on clicks
        const toggleSub = this.toggle$.pipe(
            tap((toggled: boolean) => this.isDisabledResend = toggled),
            filter((toggled: boolean) => !toggled)
        ).subscribe(() => {
            time = current;
        });

        const remainingSub = remainingSeconds$.subscribe({
            complete: () => {
                this.isDisabledResend = false;
            }
        });

        this.subscriptions.push(toggleSub);
        this.subscriptions.push(remainingSub);
    }
}
