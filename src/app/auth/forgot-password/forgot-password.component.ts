import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {finalize, tap} from 'rxjs/operators';
import {AuthService} from '../../shared/services/auth/auth.service';
import {NotificationService} from '../../shared/services/noitification/notification.service';
import {TranslateService} from '@ngx-translate/core';
import {Provider} from '../../shared/types/providers';
import {LocalStorage} from 'ngx-webstorage';
import {CheckDomainService} from '../../shared/services/check-domain/check-domain.service';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

    @LocalStorage('fg.email') email: string;
    @LocalStorage('fg.is_resend') is_resend: boolean;
    @LocalStorage('fg.provider') provider: string;

    loading: boolean;

    constructor(
        private auth: AuthService,
        private router: Router,
        private checkDomain: CheckDomainService,
        private notificationService: NotificationService,
        private translateService: TranslateService
    ) {
    }

    ngOnInit() {
        this.checkIsSuperAdmin();
    }

    checkIsSuperAdmin() {
        return this.checkDomain.isCxiDomain();
    }

    submit() {
        let forgotPasswordSource: any;

        if (this.checkIsSuperAdmin()) {
            const data = {
                email: this.email,
                provider: Provider.SuperAdmin
            };
            forgotPasswordSource = this.auth.forgotPasswordSuperAdmin(data);
        } else {
            const data = {
                username: this.email,
                is_resend: !!this.is_resend,
                provider: Provider.Admin
            };
            forgotPasswordSource = this.auth.forgotPassword(data);
        }

        this.loading = true;
        forgotPasswordSource
            .pipe(
                finalize(() => this.loading = false),
                tap(() => this.handleSuccess())
            )
            .subscribe(
                () => {
                },
                (error) => this.handleErrors(error)
            );
    }

    handleSuccess() {
        const message = `ForgotPassword.Reset link has been sent`;
        const resendUrl = `/auth/forgot-password/resend`;

        this.translateService.get(message)
            .pipe(
                tap((msg: any) => this.notificationService.success([msg])),
                tap(() => this.router.navigateByUrl(resendUrl))
            )
            .subscribe();
    }

    handleErrors(error) {
        let message = '';
        const errorStatus = error.status;
        const errorData = error.error;

        if (typeof error === 'string') {
            message += error;
        } else {
            switch (errorStatus) {
                case 400:
                case 401:
                case 402:
                case 403:
                case 404:
                case 422:
                    if (errorData) {
                        if (errorData.errors) {
                            const errors = errorData.errors;
                            for (const e of errors) {
                                message += '<p>' + e.message + '</p>';
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        this.notificationService.error(message);
    }
}
