import {AuthComponent} from './auth.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ResendComponent} from './forgot-password/resend/resend.component';
import {HasProviderGuard} from '../shared/guards/has-provider/has-provider.guard';

const AuthRoutes: Routes = [
    {
        path: '',
        component: AuthComponent,
        children: [
            {
                path: 'login',
                canActivate: [
                    HasProviderGuard
                ],
                component: LoginComponent
            },
            {path: 'sign-up', redirectTo: 'register'},
            {path: 'signup', redirectTo: 'register'},
            {path: 'register', component: RegisterComponent},
            {path: 'forgot-password', component: ForgotPasswordComponent},
            {path: 'forgot-password/resend', component: ResendComponent},
            {path: 'reset-password', component: ResetPasswordComponent},
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AuthRoutes)
    ],
    declarations: [],
    exports: [
        RouterModule
    ]
})
export class AuthRoutingModule {
}
