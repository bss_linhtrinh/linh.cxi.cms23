import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {finalize, tap} from 'rxjs/operators';
import {NotificationService} from '../../shared/services/noitification/notification.service';
import {AuthService} from '../../shared/services/auth/auth.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

    email: string;
    code: string;
    password: string;
    password_confirmation: string;
    loading: boolean;

    constructor(
        private _activeRouter: ActivatedRoute,
        private router: Router,
        private authService: AuthService,
        private notificationService: NotificationService,
        private translateService: TranslateService
    ) {
        this._activeRouter.queryParams.subscribe(params => {
            this.email = params['email'];
            this.code = params['code'];
        });
    }

    ngOnInit() {

    }

    submit() {
        if (!this.code || !this.email || !this.password || !this.password_confirmation) {
            return;
        }
        const params = {
            email: this.email,
            code: this.code,
            password: this.password,
            password_confirmation: this.password_confirmation
        };
        this.loading = true;
        this.authService.resetPasswordSuperAdmin(params)
            .pipe(
                finalize(() => this.loading = false)
            )
            .subscribe(
                () => this.handleSuccess(),
                (error) => this.handleErrors(error),
            );
    }

    handleSuccess() {
        this.translateService.get('ResetPassword.Your password has been updated')
            .pipe(
                tap(
                    (msg: string) => {
                        this.notificationService.success([msg]);
                        this.router.navigateByUrl('/auth/login');
                    }
                )
            )
            .subscribe();
    }

    handleErrors(error) {
        let message = '';
        const errorStatus = error.status;
        const errorData = error.error;

        if (typeof error === 'string') {
            message += error;
        } else {
            switch (errorStatus) {
                case 400:
                case 401:
                case 402:
                case 403:
                case 404:
                case 422:
                    if (errorData) {
                        if (errorData.errors) {
                            const errors = errorData.errors;
                            for (const e of errors) {
                                message += '<p>' + e.message + '</p>';
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        this.notificationService.error(message);
    }
}
