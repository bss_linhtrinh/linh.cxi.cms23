import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthComponent} from './auth.component';
import {AuthRoutingModule} from './auth-routing.module';
import {FormsModule} from '@angular/forms';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgSelectModule} from '@ng-select/ng-select';
import {PrivacyPolicyComponent} from './register/privacy-policy/privacy-policy.component';
import {SharedModule} from '../shared/shared.module';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {TermOfServicesComponent} from './register/term-of-services/term-of-services.component';
import { LogoComponent } from './logo/logo.component';
import { ResendComponent } from './forgot-password/resend/resend.component';

@NgModule({
    imports: [
        FormsModule,
        NgSelectModule,
        NgbModule,
        CommonModule,
        SharedModule,
        AuthRoutingModule
    ],
    declarations: [
        AuthComponent,
        LoginComponent,
        RegisterComponent,
        PrivacyPolicyComponent,
        ForgotPasswordComponent,
        ResetPasswordComponent,
        TermOfServicesComponent,
        LogoComponent,
        ResendComponent
    ],
    entryComponents: [
        PrivacyPolicyComponent,
        TermOfServicesComponent
    ]
})
export class AuthModule {
}
