import {Component, OnInit} from '@angular/core';
import {finalize} from 'rxjs/operators';
import {CookieService} from 'ngx-cookie-service';
import {StorageService} from '../../shared/services/storage/storage.service';
import {ApiService} from '../../shared/services/api/api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../shared/services/auth/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    user = {
        email: '',
        password: '',
        is_remember: false,
    };
    isError = false;

    errors: any[];
    loading: boolean;

    constructor(private apiService: ApiService,
                private cookieService: CookieService,
                private storage: StorageService,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                private auth: AuthService) {
    }

    ngOnInit() {

    }

    login(): void {
        this.loading = true;

        const data = {
            username: this.user.email,
            password: this.user.password
        };

        this.auth.login(data)
            .pipe(
                finalize(() => this.loading = false),
            )
            .subscribe(
                (res: any) => this.handleLoginSuccess(res),
                (error) => this.handleErrors(error),
            );
    }

    /**
     * Handle login success
     * @param response
     */
    handleLoginSuccess(response) {
        const redirectUrl = this.getRedirectUrl()
        if (redirectUrl.includes('http')) {
            window.location.href = redirectUrl;
        } else {
            this.router.navigateByUrl(redirectUrl);
        }
    }

    handleErrors(error: any) {
        // let message = '';
        // const errorStatus = error.status;
        // const errorData = error.errors;
        //
        // if (typeof error === 'string') {
        //     message += error;
        // } else {
        //     switch (errorStatus) {
        //         case 400:
        //         case 401:
        //         case 402:
        //         case 403:
        //         case 404:
        //         case 422:
        //             if (errorData) {
        //                 if (errorData.errors) {
        //                     const errors = errorData.errors;
        //                     for (const e of errors) {
        //                         message += '<p>' + e.message + '</p>';
        //                     }
        //                 }
        //             }
        //             break;
        //         default:
        //             break;
        //     }
        // }
        if (error.status === 500) {
            this.isError = true;
        } else {
            this.isError = false;
        }

        throw error;
    }

    getRedirectUrl() {
        const redirectUri = this.activatedRoute.snapshot.queryParams.redirect_uri
            ? this.activatedRoute.snapshot.queryParams.redirect_uri
            : '/filter';

        return redirectUri;
    }
}
