import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-term-of-services',
  templateUrl: './term-of-services.component.html',
  styleUrls: ['./term-of-services.component.scss']
})
export class TermOfServicesComponent implements OnInit {

  constructor(private _ngbActiveModal: NgbActiveModal) { }

  ngOnInit() {
  }
  cancel() {
    this._ngbActiveModal.dismiss('cancel');
  }

}
