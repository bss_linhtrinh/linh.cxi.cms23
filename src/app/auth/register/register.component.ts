import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {finalize} from 'rxjs/operators';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PrivacyPolicyComponent} from './privacy-policy/privacy-policy.component';
import {TermOfServicesComponent} from './term-of-services/term-of-services.component';
import {REGEX} from '../../shared/constants/regex';
import {ApiService} from '../../shared/services/api/api.service';
import {NotificationService} from '../../shared/services/noitification/notification.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    user = {
        name: '',
        email: '',
        password: '',
        confirm_password: '',
        user_type_id: 1,
    };
    userTypes: any[];
    errors: any;
    agree: any = false;
    emailPattern = REGEX.EMAIL;
    loading: boolean;
    
    constructor(private apiService: ApiService,
                private _router: Router,
                private _ngbModal: NgbModal,
                private notificationService: NotificationService) {
    }
    
    ngOnInit() {
        this.getUserTypes();
    }
    
    getUserTypes() {
        this.loading = true;
        this.apiService.get('user-types')
            .pipe(
                finalize(() => this.loading = false)
            )
            .subscribe((userTypes: any[]) => this.userTypes = userTypes);
    }
    
    register() {
        this.loading = true;
        this.apiService.post('register', this.user)
            .pipe(
                finalize(() => this.loading = false)
            )
            .subscribe(
                (response: any) => this.handleSuccess(response),
                (errors) => this.handleErrors(errors),
            );
    }
    
    handleSuccess(response) {
        if (response.data) {
            window.location.replace(response.data.login_success_redirect_uri);
        }
    }
    
    handleErrors(error) {
        let message = '';
        const errorStatus = error.status;
        const errorData = error.error;
        
        if (typeof error === 'string') {
            message += error;
        } else {
            switch (errorStatus) {
                case 400:
                case 401:
                case 402:
                case 403:
                case 404:
                case 422:
                    if (errorData) {
                        if (errorData.errors) {
                            const errors = errorData.errors;
                            for (const e of errors) {
                                message += '<p>' + e.message + '</p>';
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        if (message) {
            this.notificationService.error(message, null, {
                timeOut: 1000,
                easing: 'ease-in',
                easeTime: 300
            });
        }
    }
    
    privacyAndPolicy() {
        const modalRef = this._ngbModal.open(PrivacyPolicyComponent, {windowClass: 'privacy-policy'});
        modalRef.result
            .then(
                () => {
                },
                () => {
                },
            );
    }
    
    termOfServices() {
        const modalRef = this._ngbModal.open(TermOfServicesComponent, {windowClass: 'term-of-services'});
        modalRef.result
            .then(
                () => {
                },
                () => {
                },
            );
    }
}
