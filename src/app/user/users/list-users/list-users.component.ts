import {Component, OnInit} from '@angular/core';
import {User} from '../../../shared/entities/user';
import {UsersRepository} from '../../../shared/repositories/users.repository';
import {ActivatedRoute, Router} from '@angular/router';
import {CxiGridConfig} from '../../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../../shared/components/cxi-grid/cxi-grid-column';
import {LocalStorage} from 'ngx-webstorage';
import {Scope} from '../../../shared/types/scopes';
import {ColumnFilterType} from '../../../shared/types/column-filter-type';
import {ColumnFormat} from '../../../shared/types/column-format';

@Component({
    selector: 'app-list-users',
    templateUrl: './list-users.component.html',
    styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {

    users: User[] = [];

    // table
    cxiTableConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        className: false,
        pagingServer: true,
        repository: this.usersRepository,
        actions: {
            onEdit: (user: User) => {
                this.onEdit(user);
            },
            onActivate: (user: User) => {
                this.onActivate(user);
            },
            onDeactivate: (user: User) => {
                this.onDeactivate(user);
            },
            onDelete: (user: User) => {
                this.onDelete(user);
            },
        }
    });
    cxiColumns = CxiGridColumn.from([
        {title: 'name', name: 'name', avatar: 'avatar'},
        {title: 'email', name: 'email'},
        {title: 'phone', name: 'phone', format: ColumnFormat.PhoneNumber},
        {title: 'start working date', name: 'start_working_date', format: 'date', filterType: ColumnFilterType.Datepicker},
        {title: 'role', name: 'role_name', sortable: false},
        {title: 'application scope', name: 'application_scope', sortable: false},
    ]);

    @LocalStorage('scope.current') currentScope: Scope;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private usersRepository: UsersRepository
    ) {
    }

    ngOnInit() {
    }

    onEdit(user: User) {
        this.router.navigate(['..', user.id, 'edit'], {relativeTo: this.route});
    }

    onActivate(user: User) {
        this.usersRepository.activate(user)
            .subscribe();
    }

    onDeactivate(user: User) {
        this.usersRepository.deactivate(user)
            .subscribe();
    }

    onDelete(user: User) {
        this.usersRepository.destroy(user)
            .subscribe();
    }
}
