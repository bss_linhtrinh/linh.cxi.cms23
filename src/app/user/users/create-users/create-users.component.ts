import {Component, OnInit} from '@angular/core';
import {User} from '../../../shared/entities/user';
import {Gender} from '../../../shared/entities/gender';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UsersRepository} from '../../../shared/repositories/users.repository';
import {mergeMap, tap} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
    selector: 'app-create-users',
    templateUrl: './create-users.component.html',
    styleUrls: ['./create-users.component.scss']
})
export class CreateUsersComponent implements OnInit {

    user: User = new User();
    genders: Gender[] = Gender.init();

    minDate = new Date('1970-01-01');
    maxDate = new Date();

    constructor(
        private utilitiesService: UtilitiesService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private usersRepository: UsersRepository
    ) {
    }

    ngOnInit() {
    }

    back() {
        this.router.navigate(['../list'], {relativeTo: this.activatedRoute});
    }

    cancel() {
        this.back();
    }

    createUser() {
        this.usersRepository.create(this.user)
            .pipe(
                mergeMap(
                    (user: User) => { // call activate user
                        if (!user.is_activated) {
                            const params = {do_not_confirm: true};
                            return this.usersRepository.activate(user, params);
                        }
                        return of(user);
                    }
                ),
                tap(
                    () => this.back()
                )
            )
            .subscribe();
    }

    autoGenerate() {
        const randomPassword = this.utilitiesService.randomPassword();

        this.user.password = randomPassword;
        this.user.password_confirmation = randomPassword;
    }
}
