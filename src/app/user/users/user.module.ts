import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserRoutingModule} from './user-routing.module';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';
import { ListUsersComponent } from './list-users/list-users.component';
import { CreateUsersComponent } from './create-users/create-users.component';
import { EditUsersComponent } from './edit-users/edit-users.component';
import { ApplicationScopeComponent } from './application-scope/application-scope.component';

@NgModule({
    declarations: [ListUsersComponent, CreateUsersComponent, EditUsersComponent, ApplicationScopeComponent],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        UserRoutingModule,
    ]
})
export class UserModule {
}
