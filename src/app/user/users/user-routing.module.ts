import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListUsersComponent} from './list-users/list-users.component';
import {CreateUsersComponent} from './create-users/create-users.component';
import {EditUsersComponent} from './edit-users/edit-users.component';
import {CheckPermissionsGuard} from '../../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: 'create', component: CreateUsersComponent, data: {breadcrumb: 'Create New User'}},
            {path: 'list', component: ListUsersComponent, data: {breadcrumb: 'List Users'}},
            {path: ':id/edit', component: EditUsersComponent, data: {breadcrumb: 'Edit User'}},
            {path: '', redirectTo: 'list', pathMatch: 'full'}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserRoutingModule {
}
