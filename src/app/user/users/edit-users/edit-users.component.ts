import {Component, OnInit} from '@angular/core';
import {User} from '../../../shared/entities/user';
import {Gender} from '../../../shared/entities/gender';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UsersRepository} from '../../../shared/repositories/users.repository';
import {tap} from 'rxjs/operators';

@Component({
    selector: 'app-edit-users',
    templateUrl: './edit-users.component.html',
    styleUrls: ['./edit-users.component.scss']
})
export class EditUsersComponent implements OnInit {

    user: User = new User();
    genders: Gender[] = Gender.init();

    minDate = new Date('1970-01-01');
    maxDate = new Date();

    isLoaded: boolean;

    constructor(private utilitiesService: UtilitiesService,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private usersRepository: UsersRepository) {
    }

    ngOnInit() {
        // get user detail
        this.getUserDetail();
    }

    getUserDetail() {
        this.activatedRoute.params
            .subscribe(
                (data) => {
                    const id = data.id;
                    this.usersRepository.find(id)
                        .pipe(
                            tap(() => this.isLoaded = true)
                        )
                        .subscribe(
                            (user: User) => {
                                this.user = user;
                            }
                        );
                }
            );
    }

    back() {
        this.router.navigate(['../../list'], {relativeTo: this.activatedRoute});
    }

    cancel() {
        this.back();
    }

    editUser() {
        this.usersRepository.save(this.user)
            .pipe(
                tap(() => this.back())
            )
            .subscribe();
    }

    autoGenerate() {
        const randomPassword = this.utilitiesService.randomPassword();
        this.user.password = randomPassword;
        this.user.password_confirmation = randomPassword;
    }
}
