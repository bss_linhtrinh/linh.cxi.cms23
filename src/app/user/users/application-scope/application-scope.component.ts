import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Brand} from '../../../shared/entities/brand';
import {Outlet} from '../../../shared/entities/outlets';
import {Role} from '../../../shared/entities/role';
import {BrandsRepository} from '../../../shared/repositories/brands.repository';
import {OutletsRepository} from '../../../shared/repositories/outlets.repository';
import {RolesRepository} from '../../../shared/repositories/roles.repository';
import {Scope} from '../../../shared/types/scopes';
import {finalize, tap} from 'rxjs/operators';
import {LocalStorage} from 'ngx-webstorage';

@Component({
    selector: 'app-application-scope',
    templateUrl: './application-scope.component.html',
    styleUrls: ['./application-scope.component.scss']
})
export class ApplicationScopeComponent implements OnInit, OnChanges {

    brands: Brand[] = [];
    outlets: Outlet[] = [];
    roles: Role[] = [];

    // application scope
    isHiddenBrand: boolean;
    isHiddenOutlet: boolean;
    isHiddenApplicationScope: boolean;

    @LocalStorage('scope.organization_id') scopeOrganizationId: number;
    @LocalStorage('scope.brand_id') scopeBrandId: number;
    @LocalStorage('scope.outlet_id') scopeOutletId: number;
    @LocalStorage('scope.current') scopeCurrent: Scope;

    @Input() name: string;

    isLoadingApi: boolean; // is loading api brand or outlet

    // brand
    _brandId: number;
    get brandId(): number {
        return this._brandId;
    }

    @Input('brandId')
    set brandId(value: number) {
        this._brandId = value;
        this.brandIdChange.emit(this.brandId);
    }

    @Output() brandIdChange = new EventEmitter<number>();

    // outlet
    _outletId: number;
    get outletId(): number {
        return this._outletId;
    }

    @Input('outletId')
    set outletId(value: number) {
        this._outletId = value;
        this.outletIdChange.emit(this.outletId);
    }

    @Output() outletIdChange = new EventEmitter<number>();

    // role
    _roleId: number;
    get roleId(): number {
        return this._roleId;
    }

    @Input('roleId')
    set roleId(value: number) {
        this._roleId = value;
        this.roleIdChange.emit(this.roleId);
    }

    @Output() roleIdChange = new EventEmitter<number>();


    constructor(
        private brandsRepository: BrandsRepository,
        private outletsRepository: OutletsRepository,
        private rolesRepository: RolesRepository
    ) {
    }

    ngOnInit() {
        this.getApplicationScope();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (
            (
                (
                    typeof changes.brandId !== 'undefined' &&
                    changes.brandId.currentValue !== changes.brandId.previousValue
                ) ||
                (
                    typeof changes.outletId !== 'undefined' &&
                    changes.outletId.currentValue !== changes.outletId.previousValue
                ) ||
                (
                    typeof changes.roleId !== 'undefined' &&
                    changes.roleId &&
                    changes.roleId.firstChange &&
                    changes.roleId.currentValue !== changes.roleId.previousValue
                )
            )
        ) {
            this.getAvailableRoles();
        }
    }

    getApplicationScope() {
        switch (this.scopeCurrent) {
            case Scope.Organization:
                this.isHiddenBrand = false;
                this.isHiddenOutlet = false;
                this.getBrands();
                break;

            case Scope.Brand:
                this.isHiddenBrand = true;
                this.isHiddenOutlet = false;
                if (this.scopeBrandId) {
                    this.brandId = this.scopeBrandId;
                }
                this.getOutletsByBrand(this.brandId);
                break;

            case Scope.Outlet:
                this.isHiddenBrand = true;
                this.isHiddenOutlet = true;

                if (this.scopeBrandId) {
                    this.brandId = this.scopeBrandId;
                }
                if (this.scopeOutletId) {
                    this.outletId = this.scopeOutletId;
                }
                break;
        }
    }

    getBrands() {
        this.isLoadingApi = true;
        this.brandsRepository.all()
            .pipe(
                finalize(() => this.isLoadingApi = false),
                tap(
                    (brands: Brand[]) => this.brands = brands
                ),
                tap(
                    () => {
                        if (this.brandId) { // edit
                            this.getOutletsByBrand(this.brandId);
                        }
                    }
                )
            )
            .subscribe(
            );
    }

    getOutletsByBrand(brandId: number) {
        this.isLoadingApi = true;
        this.outletsRepository.getOutletsByBrand(brandId)
            .pipe(
                finalize(() => this.isLoadingApi = false),
                tap(
                    (outlets: Outlet[]) => {
                        // refresh & remove
                        const outletIndex = outlets.findIndex(x => x.id === this.outletId);
                        if (outletIndex === -1) {
                            this.outletId = null;
                        }
                    }
                )
            )
            .subscribe(
                (outlets: Outlet[]) => {
                    this.outlets = outlets;
                }
            );
    }

    getAvailableRoles() {
        const scope = (this.scopeCurrent === Scope.Outlet)
            ? Scope.Outlet
            : this.scopeCurrent === Scope.Brand
                ? (
                    this.outletId
                        ? Scope.Outlet
                        : Scope.Brand
                )
                : (
                    this.scopeCurrent === Scope.Organization
                        ? (
                            this.outletId
                                ? Scope.Outlet
                                : (
                                    this.brandId
                                        ? Scope.Brand
                                        : Scope.Organization
                                )
                        )
                        : null
                );
        const params = {
            for: 'create-user',
            scope: scope
        };

        this.rolesRepository.allActivated(params)
            .pipe(
                // tap(
                //     (roles: Role[]) => {
                //         // refresh & remove
                //         const roleIndex = roles.findIndex(x => x.id === this.roleId);
                //         if (roleIndex === -1) {
                //             this.roleId = null;
                //         }
                //     }
                // ),
                tap((roles: Role[]) => this.roles = roles)
            )
            .subscribe()
        ;
    }

    onSelectBrand(brandId: number) {
        this.getOutletsByBrand(brandId);
    }
}
