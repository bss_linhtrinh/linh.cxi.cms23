import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CreateRoleComponent} from './create-role/create-role.component';
import {ListRolesComponent} from './list-roles/list-roles.component';
import {EditRoleComponent} from './edit-role/edit-role.component';
import {CheckPermissionsGuard} from '../../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: 'create', component: CreateRoleComponent, data: {breadcrumb: 'Create New Role'}},
            {path: 'list', component: ListRolesComponent, data: {breadcrumb: 'List Roles'}},
            {path: ':id', component: EditRoleComponent, data: {breadcrumb: 'Edit Role'}},
            {path: '', redirectTo: 'list', pathMatch: 'full'}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RolesRoutingModule {
}
