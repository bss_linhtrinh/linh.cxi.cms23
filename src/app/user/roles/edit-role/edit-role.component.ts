import {Component, OnInit} from '@angular/core';
import {Role} from '../../../shared/entities/role';
import {RoleScope} from '../../../shared/entities/role-scope';
import {ActivatedRoute, Router} from '@angular/router';
import {RolesRepository} from '../../../shared/repositories/roles.repository';
import {map, switchMap, tap, toArray} from 'rxjs/operators';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import {from} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {SessionStorage} from 'ngx-webstorage';
import {Language} from '../../../shared/entities/language';

@Component({
    selector: 'app-edit-role',
    templateUrl: './edit-role.component.html',
    styleUrls: ['./edit-role.component.scss']
})
export class EditRoleComponent implements OnInit {

    role: Role = new Role();
    scopes: RoleScope[] = [];
    roles: Role[];
    tab: any;
    @SessionStorage('languages') languages: Language[];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private rolesRepository: RolesRepository,
        private utilitiesService: UtilitiesService,
        private translateService: TranslateService
    ) {
    }

    ngOnInit() {
        this.getRoleDetail();
        this.getRoleScopes();
    }

    getRoleScopes() {
        from(RoleScope.create())
            .pipe(
                switchMap((roleScope: RoleScope) => {
                    return this.translateService.get(roleScope.name)
                        .pipe(
                            map(
                                (translatedName: string) => {
                                    roleScope.name = translatedName;
                                    return roleScope;
                                }
                            )
                        );
                }),
                toArray(),
                tap(
                    (scopes: RoleScope[]) => this.scopes = scopes
                )
            )
            .subscribe();
    }

    getRoleDetail() {
        this.activatedRoute.params
            .subscribe(
                (data) => this.rolesRepository.find(data.id)
                    .pipe(
                        tap(() => this.getRoles())
                    )
                    .subscribe(
                        (role: Role) => {
                            this.role = role;
                        }
                    )
            );
    }

    getRoles() {
        this.rolesRepository.all()
            .pipe(
                map(roles => roles.filter((r: Role) => r.id !== this.role.id))
            )
            .subscribe(
                (roles: Role[]) => this.roles = roles
            );
    }

    reloadRolesByLanguage(lang: string) {
        this.rolesRepository.allByLanguage(lang)
            .subscribe(
                (roles: Role[]) => this.roles = roles
            );
    }

    cancel() {
        this.back();
    }

    back() {
        this.router.navigate(['..', 'list'], {relativeTo: this.activatedRoute});
    }

    editRole() {
        const role = this.role.parse();
        this.rolesRepository.save(role)
            .pipe(
                tap(() => this.back())
            )
            .subscribe();
    }

    onChangeRoleName() {
        this.role.slug = this.utilitiesService.ChangeToSlug(this.role.name);
    }

    onTabChange(lang: string) {
        this.reloadRolesByLanguage(lang);
    }
}
