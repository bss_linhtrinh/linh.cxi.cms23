import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RolesRoutingModule} from './roles-routing.module';
import {CreateRoleComponent} from './create-role/create-role.component';
import {ListRolesComponent} from './list-roles/list-roles.component';
import {EditRoleComponent} from './edit-role/edit-role.component';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule} from '@angular/forms';

@NgModule({
    declarations: [CreateRoleComponent, ListRolesComponent, EditRoleComponent],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        RolesRoutingModule
    ]
})
export class RolesModule {
}
