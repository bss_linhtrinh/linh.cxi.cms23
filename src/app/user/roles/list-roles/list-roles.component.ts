import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RolesRepository} from '../../../shared/repositories/roles.repository';
import {Role} from '../../../shared/entities/role';
import {CxiGridColumn} from '../../../shared/components/cxi-grid/cxi-grid-column';
import {CxiGridConfig} from '../../../shared/components/cxi-grid/cxi-grid-config';

@Component({
    selector: 'app-list-roles',
    templateUrl: './list-roles.component.html',
    styleUrls: ['./list-roles.component.scss']
})
export class ListRolesComponent implements OnInit {
    roles: Role[] = [];

    // table
    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        className: false,
        pagingServer: true,
        translatable: true,
        repository: this.rolesRepository,
        actions: {
            onEdit: (role: Role) => {
                this.onEdit(role);
            },
            onDelete: (role: Role) => {
                this.onDelete(role);
            },
        }
    });
    cxiGridColumns = CxiGridColumn.from([
        {title: 'name', name: 'name'},
        {title: 'Description', name: 'description', format: 'rich-text'},
    ]);

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private rolesRepository: RolesRepository
    ) {
    }

    ngOnInit() {
    }

    onEdit(role: Role) {
        this.router.navigate(['..', role.id], {relativeTo: this.route});
    }

    onDelete(role: Role) {
        this.rolesRepository.destroy(role)
            .subscribe();
    }
}
