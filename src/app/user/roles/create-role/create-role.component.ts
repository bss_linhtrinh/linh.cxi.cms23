import {Component, OnInit} from '@angular/core';
import {Role} from '../../../shared/entities/role';
import {ActivatedRoute, Router} from '@angular/router';
import {RolesRepository} from '../../../shared/repositories/roles.repository';
import {map, switchMap, tap, toArray} from 'rxjs/operators';
import {RoleScope} from '../../../shared/entities/role-scope';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import {TranslateService} from '@ngx-translate/core';
import {from} from 'rxjs';
import {SessionStorage} from 'ngx-webstorage';
import {Language} from '../../../shared/entities/language';

@Component({
    selector: 'app-create-role',
    templateUrl: './create-role.component.html',
    styleUrls: ['./create-role.component.scss']
})
export class CreateRoleComponent implements OnInit {

    role: Role = new Role();
    scopes: RoleScope[] = [];
    roles: Role[];
    tab: any;
    @SessionStorage('languages') languages: Language[];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private rolesRepository: RolesRepository,
        private utilitiesService: UtilitiesService,
        private translateService: TranslateService
    ) {
    }

    ngOnInit() {
        this.getRoles();
        this.getRoleScopes();
    }

    getRoleScopes() {
        from(RoleScope.create())
            .pipe(
                switchMap((roleScope: RoleScope) => {
                    return this.translateService.get(roleScope.name)
                        .pipe(
                            map(
                                (translatedName: string) => {
                                    roleScope.name = translatedName;
                                    return roleScope;
                                }
                            )
                        );
                }),
                toArray(),
                tap(
                    (scopes: RoleScope[]) => this.scopes = scopes
                )
            )
            .subscribe();
    }

    getRoles() {
        const params = {for: 'create-role'};
        this.rolesRepository.all(params)
            .subscribe(
                (roles: Role[]) => {
                    this.roles = roles;
                    if (!this.role.parent_id && this.roles[0] && this.roles[0].id) {
                        this.role.parent_id = this.roles[0].id;
                    }
                }
            );
    }

    reloadRolesByLanguage(lang: string) {
        this.rolesRepository.allByLanguage(lang)
            .subscribe(
                (roles: Role[]) => {
                    this.roles = roles;
                    if (!this.role.parent_id && this.roles[0] && this.roles[0].id) {
                        this.role.parent_id = this.roles[0].id;
                    }
                }
            );
    }

    cancel() {
        this.back();
    }

    back() {
        this.router.navigate(['..', 'list'], {relativeTo: this.activatedRoute});
    }

    createRole() {
        const role = this.role.parse();
        this.rolesRepository.save(role)
            .pipe(
                tap(() => this.back())
            )
            .subscribe();
    }

    onChangeRoleName() {
        this.role.slug = this.utilitiesService.ChangeToSlug(this.role.name);
    }

    onTabChange(lang: string) {
        this.reloadRolesByLanguage(lang);
    }
}
