import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CreatePermissionComponent} from './create-permission/create-permission.component';
import {ListPermissionsComponent} from './list-permissions/list-permissions.component';
import {EditPermissionComponent} from './edit-permission/edit-permission.component';
import {CheckPermissionsGuard} from '../../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: '', redirectTo: 'list', pathMatch: 'full'},
            {path: 'create', component: CreatePermissionComponent, data: {breadcrumb: 'Create New Permission'}},
            {path: 'list', component: ListPermissionsComponent, data: {breadcrumb: 'List permissions'}},
            {path: ':id', component: EditPermissionComponent, data: {breadcrumb: 'Edit permissions'}},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PermissionsRoutingModule {
}
