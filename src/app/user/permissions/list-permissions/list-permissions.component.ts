import {Component, OnInit} from '@angular/core';
import {Role} from '../../../shared/entities/role';
import {ActivatedRoute, Router} from '@angular/router';
import {PermissionsRepository} from '../../../shared/repositories/permissions.repository';
import {Permission} from '../../../shared/entities/permission';

@Component({
    selector: 'app-list-permissions',
    templateUrl: './list-permissions.component.html',
    styleUrls: ['./list-permissions.component.scss']
})
export class ListPermissionsComponent implements OnInit {
    permissions: Permission[] = [];

    // table
    baseTableConfig = {
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        className: false,
        pagingServer: true,
        repository: this.permissionsRepository,
        actions: {
            onEdit: (role: Role) => {
                this.onEdit(role);
            },
            onDelete: (role: Role) => {
                this.onDelete(role);
            },
        }
    };
    columns = [
        {title: 'name', name: 'name', className: 'name'},
    ];
    currentUser: any;

    constructor(private router: Router,
                private route: ActivatedRoute,
                private permissionsRepository: PermissionsRepository) {
        this.currentUser = route.snapshot.data.currentUser;
    }

    ngOnInit() {
        // this.getPermissions();
    }

    // getPermissions() {
    //     this.permissionsRepository.all()
    //         .subscribe(
    //             (permissions: Permission[]) => this.permissions = permissions
    //         );
    // }

    onEdit(role: Role) {
        this.router.navigate(['..', role.id], {relativeTo: this.route});
    }

    onDelete(role: Role) {
        this.permissionsRepository.destroy(role)
            .subscribe();
    }
}
