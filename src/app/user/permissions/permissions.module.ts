import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PermissionsRoutingModule} from './permissions-routing.module';
import {EditPermissionComponent} from './edit-permission/edit-permission.component';
import {CreatePermissionComponent} from './create-permission/create-permission.component';
import {ListPermissionsComponent} from './list-permissions/list-permissions.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
    declarations: [EditPermissionComponent, CreatePermissionComponent, ListPermissionsComponent],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        PermissionsRoutingModule
    ]
})
export class PermissionsModule {
}
