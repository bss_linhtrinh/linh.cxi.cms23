import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import {LocalStorage} from 'ngx-webstorage';
import { CustomerRepository } from 'src/app/shared/repositories/customers.repository';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent implements OnInit, OnChanges {

  @LocalStorage('pagination.current_page') current_page: number = 1;
  @LocalStorage('pagination.per_page') per_page: number = 5;
  @LocalStorage('pagination.total') total: number;
  @Input() orderHistory: any;
  public customer: any;
  public listOrderHistory: any = [];
  constructor(
    public customerRepository: CustomerRepository,
  ) { }

  ngOnInit() {
    console.log(this.orderHistory)
  }
  ngOnChanges(simpleChanges: SimpleChanges) {
    if (this.orderHistory && this.orderHistory) {
      this.customer = this.orderHistory;
      (this.customer.id !== null ? this.getListOrderHistory() : null);
    }
  }
  getListOrderHistory() {
    let vm = this;
    let sen_data = {
      page: this.current_page,
      per_page: this.per_page,
    }
    vm.customerRepository.getOrderHistory(this.customer, sen_data)
      .subscribe((res?: any) => {
        console.log(res)
        this.listOrderHistory = res.data;
      })
  }
  onChangePagination() {
    this.getListOrderHistory();
  }
}
