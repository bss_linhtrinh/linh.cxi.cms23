import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UsersRepository } from 'src/app/shared/repositories/users.repository';
import { User } from 'src/app/shared/entities/user';
import { LocalStorage } from 'ngx-webstorage';
import { CustomerRepository } from 'src/app/shared/repositories/customers.repository';
import { Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
declare const helper: any;
@Component({
  selector: 'app-add-saleman',
  templateUrl: './add-saleman.component.html',
  styleUrls: ['./add-saleman.component.scss']
})
export class AddSalemanComponent implements OnInit {
  users: User[] = [];
  public key_search: string;
  public filter: any = null;
  public filterTable = [
    { name: 'NAME', isShow: true, isShowAsc: true, isShowDesc: false, order_by: 'name' },
    { name: 'EMAIL', isShow: true, isShowAsc: true, isShowDesc: false, order_by: 'email' },
    { name: 'PHONE NUMBER', isShow: true, isShowAsc: true, isShowDesc: false, order_by: 'phone' },
    { name: 'APPLICATION SCOPE', isShow: true, isShowAsc: true, isShowDesc: false, _disable: true },
  ];
  public filter_order: string = 'asc';
  public order_by_filter: string;
  @Input() customers;
  @LocalStorage('pagination.current_page') current_page: number;
  @LocalStorage('pagination.per_page') per_page: number;
  @LocalStorage('pagination.total') total: number;

  constructor(
    private ngbActiveModal: NgbActiveModal,
    private userRepository: UsersRepository,
    public customerRepository: CustomerRepository,
    public router: Router,
  ) { }

  ngOnInit() {
    this.getListUser();
  }
  getListUser() {
    let _sen_data = {
      order: this.filter_order,
      filters: this.filter,
      page: this.current_page,
      per_page: this.per_page,
      order_by: this.order_by_filter,
      status: 1
    };
    this.userRepository.all(_sen_data)
      .subscribe((res?: any) => {
        this.users = res;
      })
  }
  onChangePagination() {
    console.log(this.current_page)
    this.getListUser();
  }

  onKey(item) {
    this.filter = JSON.stringify({
      'match': 'and',
      'rules': [
        {
          'field': 'name',
          'operator': 'LIKE',
          'value': `%${this.key_search}%`
        },
      ]
    });
    this.getListUser();
  }
  assignSalemans(item) {
    this.formatData();
    this.customers.salesman_id = item.id;
    this.customerRepository.save(this.customers)
      .subscribe((res) => {
        this.ngbActiveModal.close('success');
        this.router.navigateByUrl('/customer/list');
        setTimeout(() => {
          helper.showNotification(`Updated successfully !!!`, 'done', 'success', 1200);
        }, 200);
      });
  }
  formatData() {
    this.customers.hobbies = this.customers.hobbies.map(item => {
      return this.customers.hobbies = item.id;
    });
  }
  cancel() {
    this.ngbActiveModal.dismiss('cancel');
  }
  showListOrder(item?: any, index?: number) {
    this.order_by_filter = item.order_by;
    this.filterTable.forEach((o) => {
      if (o.name.toUpperCase() == item.name) {
        item.isShowDesc = true;
        item.isShowAsc = false;
        item.isShow = !item.isShow;
      } else {
        o.isShowAsc = true;
        o.isShowDesc = false;
        o.isShow = true;
      }
    });
    if (item.isShow == true) {
      this.filter_order = 'desc';
      this.getListUser();
    } else {
      this.filter_order = 'asc';
      this.getListUser();
    }
  }
}
