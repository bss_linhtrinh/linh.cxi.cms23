import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSalemanComponent } from './add-saleman.component';

describe('AddSalemanComponent', () => {
  let component: AddSalemanComponent;
  let fixture: ComponentFixture<AddSalemanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSalemanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSalemanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
