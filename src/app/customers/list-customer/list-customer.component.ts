import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/shared/entities/customer';
import { CustomerRepository } from 'src/app/shared/repositories/customers.repository';
import { ActivatedRoute, Router } from '@angular/router';
import { ColumnFilterType } from '../../shared/types/column-filter-type';
import { ColumnFormat } from '../../shared/types/column-format';
import { CxiGridConfig } from '../../shared/components/cxi-grid/cxi-grid-config';
import { CxiGridColumn } from '../../shared/components/cxi-grid/cxi-grid-column';
import { LocalStorage } from 'ngx-webstorage';
import { Hobby } from 'src/app/shared/entities/hobby';
import { HobbyRepository } from 'src/app/shared/repositories/hobby.repository';
import { CustomerTypeRepository } from 'src/app/shared/repositories/customers-type';
import { Backlist } from 'src/app/shared/entities/backlist-customer';
import { UsersRepository } from 'src/app/shared/repositories/users.repository';
import { switchMap, tap } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from 'src/app/shared/entities/user';
import { AddSalemanComponent } from '../add-saleman/add-saleman.component';
import { from } from 'rxjs';

declare var jQuery: any;
declare const helper: any;

@Component({
    selector: 'app-list-customer',
    templateUrl: './list-customer.component.html',
    styleUrls: ['./list-customer.component.scss']
})
export class ListCustomerComponent implements OnInit {
    public hobbies: Hobby[] = [];
    public listItem: any = [];
    public hobby_req: any = [];
    public isShow: boolean = false;
    public customerTypes: any = [];
    public backlist = new Backlist();
    // table
    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        className: false,
        selecting: true,
        pagingServer: true,
        repository: this.customerRepository,
        actions: {
            onEdit: (customers: Customer) => {
                this.onEdit(customers);
            },
            onDelete: (customers: Customer) => {
                this.onDelete(customers);
            },
            onActivate: (customers: Customer) => {
                this.onActivate(customers);
            },
            onDeactivate: (customers: Customer) => {
                this.onDeactivate(customers);
            },
            onAddSalesman: (customers: Customer) => {
                this.onAddSalesman(customers);
            },
        }
    });
    cxiGridColumns = CxiGridColumn.from([
        { title: 'name', name: 'name', avatar: 'avatar' },
        { title: 'dob', name: 'dob', filterType: ColumnFilterType.Datepicker, sort: true, format: 'dateTime' },
        {
            title: 'gender', name: 'gender', filterType: ColumnFilterType.Select,
            filterData: [
                { name: 'Male', id: 'male' },
                { name: 'Female', id: 'female' },
            ]
        },
        { title: 'phone', name: 'phone', className: 'phone-number', format: ColumnFormat.PhoneNumber },
        { title: 'email', name: 'email' },
        // {
        //     title: 'customer type', name: 'customer_types', filterType: ColumnFilterType.Select, filterData: this.customerTypeRepository
        // },
        // { title: 'hobby', name: 'hobbies', fieldOption: 'name', format: ColumnFormat.Tag },
        { title: 'sales man', name: 'salesman_customer' },
        { title: 'without salesman', name: 'without_salesman', className: 'without-salesman' },
    ]);

    @LocalStorage('scope.organization_id') organizationId: number;
    @LocalStorage('scope.brand_id') brandId: number;
    @LocalStorage('scope.outlet_id') outletId: number;

    constructor(
        private customerRepository: CustomerRepository,
        private customerTypeRepository: CustomerTypeRepository,
        private hobbyRepository: HobbyRepository,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private userRepository: UsersRepository,
        private ngbModal: NgbModal,
    ) {
    }

    ngOnInit() {
        //this.getListCustomerType();
    }
    getListCustomerType() {
        this.customerTypeRepository.all({ pagination: 0 })
            .subscribe((res?: any) => {
                this.customerTypes = res;
            })
    }
    onDelete(customers: Customer) {
        this.customerRepository.destroy(customers).subscribe();
    }

    onEdit(customers: Customer) {
        this.router.navigate(['..', customers.id, 'edit'], { relativeTo: this.activatedRoute });
    }

    onActivate(customers: Customer) {
        this.customerRepository.activate(customers).subscribe();
    }

    onDeactivate(customer: Customer) {
        this.customerRepository.deactivate(customer).subscribe();
    }
    FilterCustomer(event?: any) {
        if (event.name == 'hobbies') {
            this.getListHobby();
            this.showModal();
        }
    }
    getListHobby() {
        this.hobbyRepository.all()
            .subscribe((res?: any) => {
                this.hobbies = res;
                console.log(this.hobbies)
            })
    }
    checkHobby(item: any) {
        let index = this.listItem.indexOf(item);
        if (index == -1) {
            this.listItem.push(item);
            this.hobby_req.push(item.id);
        } else {
            this.listItem.splice(index, 1);
            this.hobby_req.splice(index, 1);
        }
    }
    searchHobby() {
        this.isShow = !this.isShow;
        this.toTop();
        //this.isShow = false;
    }
    showModal() {
        (function ($) {
            $('#modal-search').modal('show');
        })(jQuery);
    }
    toTop() {
        (function ($) {
            $('#modal-search').modal('hide');
            return false;
        })(jQuery);
    }
    clearHobby(event) {
        if (event && event.length > 0) {
            this.listItem = [];
            this.hobby_req = [];
        }
    }
    checkCustomer(item) {
        let index = this.backlist.list.indexOf(item.id);
        if (index == -1) {
            this.backlist.list.push(item.id);
        } else {
            this.backlist.list.splice(index, 1);
        }
    }
    checkCustomerAll(item?: any) {
        console.log(item)
        if (item.check_all_item == true) {
            item.data_item.forEach(_o => {
                this.backlist.list.push(_o.id);
            });
        } else {
            this.backlist.list = [];
        }
    }
    switch() {
        this.backlist.action = 'add';
        this.customerRepository.backlistCustomer(this.backlist).subscribe();
        this.router.navigateByUrl('/backlist-customer/list');
        setTimeout(() => {
            helper.showNotification(`Updated successfully !!!`, 'done', 'success', 1200);
        }, 600);
    }

    onAddSalesman(customers: Customer) {
        const modalRef = this.ngbModal.open(AddSalemanComponent, { centered: true, windowClass: 'orders-add-product' });
        modalRef.componentInstance.customers = customers;
        modalRef.result.then((result) => {
            if ( result === 'success' ) {
                console.log(123)
            }
        }, (reason) => {});
    }
}
