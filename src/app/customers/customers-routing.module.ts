import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListCustomerComponent} from './list-customer/list-customer.component';
import {CreateCustomersComponent} from './create-customers/create-customers.component';
import {EditCustomersComponent} from './edit-customers/edit-customers.component';

const routes: Routes = [
    {path: 'list', component: ListCustomerComponent, data: {breadcrumb: 'List customers'}},
    {path: 'create', component: CreateCustomersComponent, data: {breadcrumb: 'Create new customer'}},
    {path: ':id/edit', component: EditCustomersComponent, data: {breadcrumb: 'Edit customer'}},
    {path: '', redirectTo: 'list', pathMatch: 'full'},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CustomersRoutingModule {
}
