import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/shared/entities/customer';
import { CustomerRepository } from 'src/app/shared/repositories/customers.repository';
import { Gender } from '../../shared/entities/gender';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { UtilitiesService } from 'src/app/shared/services/utilities/utilities.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
import { environment } from 'src/environments/environment';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { CustomerTypeRepository } from 'src/app/shared/repositories/customers-type';
import { HobbyRepository } from 'src/app/shared/repositories/hobby.repository';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { UsersRepository } from 'src/app/shared/repositories/users.repository';
import { User } from 'src/app/shared/entities/user';
import { CountryRepository } from 'src/app/shared/repositories/country.repository';
import { Country } from 'src/app/shared/entities/country';

declare const helper: any;

@Component({
  selector: 'app-create-customers',
  templateUrl: './create-customers.component.html',
  styleUrls: ['./create-customers.component.scss']
})
export class CreateCustomersComponent implements OnInit {

  public customer: Customer;
  public countries: Country[] = [];
  public genders: Gender[] = Gender.init();
  public baseUrl: string = environment.baseUrl;
  public brand = [];
  public customer_type: any = [];
  public hobbies: any = [];
  public hobbies_req: any = [];
  minDate = new Date('1970-01-01');
  maxDate = new Date();
  selectedFile: File = null;
  userScope: UserScope;
  public salesman: User;
  public _references: any = [
    { name: 'Walk in', value: 'walk_in' },
    { name: 'Fanpage', value: 'fanpage' },
    { name: 'Call in', value: 'call_in' },
  ];
  public _customer_classification: any = [
    { name: 'Purchased', value: 'purchased' },
    { name: 'Not purchase', value: 'not_purchased' },
  ];
  public _potential: any = [];
  public _dobs: any = [
    {
      date: "",
      content: ""
    }
  ];
  _dob = {
    date: "",
    content: ""
  }
  public _contacts: any = [
    {
      type: "phone",
      value: ""
    }
  ]
  _contact = {
    type: "phone",
    value: ""
  }
  select_contacts: any = [{ name: 'Phone', value: "phone" }, { name: 'Email', value: "email" }];
  _primary_contact: any = [
    { name: 'Viber', value: 'viber' },
    { name: 'Zalo', value: 'zalo' },
    { name: 'FaceBook', value: 'fb' },
    { name: 'Email', value: 'email' },
    { name: 'Whatsapp', value: 'whatsapp' },
    { name: 'Wechat', value: 'wechat' },
    { name: 'Sms', value: 'sms' },
    { name: 'Call', value: 'call' },
    { name: 'Directly meeting', value: 'directly_meeting' },
  ];
  _budget: string;
  _disable: boolean = false;
  public _favorite_products: any = [{ code: '' }];
  public _favorite_product = { code: '' };
  public _cities: any = [];
  public _districts: any = [];
  public quatity_dob: number = 1;

  constructor(
    public customerReponsitory: CustomerRepository,
    public countryReponsitory: CountryRepository,
    public userReponsitory: UsersRepository,
    public customerTypeReponsitory: CustomerTypeRepository,
    public hobbyReponsitory: HobbyRepository,
    public brandReponsitory: BrandsRepository,
    public router: Router,
    public http: HttpClient,
    public utilitiesService: UtilitiesService,
    private scopeService: ScopeService,
  ) { }

  ngOnInit() {
    this.filterMoney();
    this.customer = new Customer();
    this.loadBrand();
    //this.loadCustomerType();
    this.loadHobby();
    this.getSaleman();
    this.getListCountries();
  }
  filterMoney() {
    helper.filterMoneys();
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.customer.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  getSaleman() {
    this.userReponsitory.all({ status: 1 })
      .subscribe(res => {
        this.salesman = res;
        //this.customer.salesman_id = this.salesman[0].id;
      })
  }
  onFileChanged(event) {
    this.selectedFile = <File>event.target.files[0];
    this.onUpload();
  }
  loadBrand() {
    this.brandReponsitory.all({ status: 1 })
      .subscribe(res => {
        this.brand = res;
        this.customer.brand_id = this.brand[0].id;
        this.getListUserScope();
      })
  }
  loadCustomerType() {
    this.customerTypeReponsitory.all()
      .subscribe((res?: any) => {
        this.customer_type = res;
        this.customer.customer_type_id = this.customer_type[0].id;
      })
  }
  loadHobby() {
    this.hobbyReponsitory.all()
      .subscribe((res?: any) => {
        this.hobbies = res;
      })
  }
  checkHobby(item?: any) {
    let index = this.hobbies_req.indexOf(item);
    if (index == -1) {
      this.hobbies_req.push(item);
      this.customer.hobbies.push(item.id);
    } else {
      this.hobbies_req.splice(index, 1);
      this.customer.hobbies.splice(index, 1);
    }
  }
  onUpload() {
    this.customerReponsitory.uploadCreate(this.customer, this.selectedFile)
      .subscribe((res) => {
        if (res.data && res.data.path_string) {
          this.customer.avatar = res.data.path_string;
        }
      });
  }
  removeUpload() {
    this.customer.avatar = null;
  }
  autoGenerate() {
    const randomPassword = this.utilitiesService.randomPassword();
    this.customer.password = randomPassword;
    this.customer.password_confirmation = randomPassword;
  }
  handleAddressChange(address: Address) {
    this.customer.address = address.formatted_address;
  }
  onSubmit() {
    this.customer.level = null;
    this.formatData();
    this.customerReponsitory.save(this.customer).subscribe((res) => {
      this.router.navigateByUrl('/customer');
      helper.showNotification(`${this.customer.name} has been added successfully!!`, 'done', 'success');
    },
      (errors) => { console.log(errors) }
    )
  }
  //feed back
  addNewDob() {
    var _this = this;
    _this._dobs.push(JSON.parse(JSON.stringify(_this._dob)));
  }
  removeDob(index) {
    this._dobs.splice(index, 1);
  }
  getListCountries() {
    let sen_data = {
      order_by: "created_at",
      order: "desc",
      pagination: 0,
      authorization: 'none'
    }
    this.countryReponsitory.all(sen_data)
      .subscribe((res?: any) => {
        this.countries = res;
      })
  }
  changeCountry(item) {
    let _countryId;
    this.customer.city = '',
      this.customer.district = '',
      this._districts = [];
    this.countries.forEach(_item => {
      if (_item.name == item) {
        _countryId = _item;
      }
    });
    this.countryReponsitory.getCityByCountry(_countryId)
      .subscribe((res?: any) => {
        this._cities = res.data;
        console.log(this._cities)
      })
  }
  changeCity(item) {
    let _cityId;
    this.customer.district = '',
      this._cities.forEach(_item => {
        if (_item.name == item) {
          _cityId = _item;
        }
      });
    this.countryReponsitory.getDistrictsByCountry(_cityId)
      .subscribe((res?: any) => {
        this._districts = res.data;
        console.log(this._cities)
      })
  }
  formatData() {
    if (this._budget) {
      let __budget = '';
      this._budget.split(',').forEach(_b => {
        __budget += _b;
      });
      this.customer.budget = Number(__budget);
    }
    this._dobs = this._dobs.filter(_o => {
      return Boolean(_o.date) == true && Boolean(_o.content) == true;
    });
    this.customer.anniversaries = this._dobs;

    this._contacts = this._contacts.filter(_o => {
      return Boolean(_o.value) == true;
    });
    this.customer.contacts = this._contacts;
  }
  changePotential() {
    let potential1 = [
      { name: '1 time', value: '1time' },
      { name: '2 times', value: '2times' },
      { name: '3 times', value: '3times' },
    ];
    let potential2 = [
      { name: 'Very potential', value: 'very_potential' },
      { name: 'Potential', value: 'potential' },
      { name: 'Less potential', value: 'less_potential' },
      { name: 'Not potential', value: 'not_potential' },
      { name: 'Black list', value: 'Black list' },
    ];
    if (this.customer.customer_classification == "purchased") {
      this._potential = potential1;
    } else {
      this._potential = potential2;
    }
  }
  addNewContact() {
    var _this = this;
    _this._contacts.push(JSON.parse(JSON.stringify(_this._contact)));
  }
  removeContact(index) {
    this._contacts.splice(index, 1);
  }
  changeContact(item){
    item.value = '';
  }
}
