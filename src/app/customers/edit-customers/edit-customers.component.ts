import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/shared/entities/customer';
import { Gender } from '../../shared/entities/gender';
import { CustomerRepository } from 'src/app/shared/repositories/customers.repository';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
import { environment } from 'src/environments/environment';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { CustomerTypeRepository } from 'src/app/shared/repositories/customers-type';
import { HobbyRepository } from 'src/app/shared/repositories/hobby.repository';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { UsersRepository } from 'src/app/shared/repositories/users.repository';
import { User } from 'src/app/shared/entities/user';
import { CountryRepository } from 'src/app/shared/repositories/country.repository';
import { Country } from 'src/app/shared/entities/country';
declare const helper: any;

@Component({
  selector: 'app-edit-customers',
  templateUrl: './edit-customers.component.html',
  styleUrls: ['./edit-customers.component.scss']
})
export class EditCustomersComponent implements OnInit {

  public customer: Customer;
  public countries: Country[] = [];
  public genders: Gender[] = Gender.init();
  public baseUrl: string = environment.baseUrl;
  selectedFile: File = null;
  public customer_type: any = [];
  public hobbies: any = [];
  public hobbies_req: any = [];
  userScope: UserScope;
  public salesman: User;
  // table
  baseTableConfig = {
    paging: false,
    sorting: true,
    filtering: false,
    className: false,
    actions: {
      onEdit: (payment: any) => {
        //
      },
      onDelete: (payment: any) => {
        //
      },
    }
  };
  baseTableColumns = [
    { title: 'product', name: 'product', secondary_name: 'order_number', avatar: 'img', className: 'product' },
    { title: 'date', name: 'date', className: 'date' },
    { title: 'status', name: 'status', className: 'status' },
    { title: 'address', name: 'address', className: 'address' },
    { title: 'price', name: 'price', className: 'price' },
  ];
  public brand = [];
  public _references: any = [
    { name: 'Walk in', value: 'walk_in' },
    { name: 'Fanpage', value: 'fanpage' },
    { name: 'Call in', value: 'call_in' },
  ];
  public _customer_classification: any = [
    { name: 'Purchased', value: 'purchased' },
    { name: 'Not purchase', value: 'not_purchased' },
  ];
  public _potential: any = [];
  _disable: boolean = false;
  public _favorite_products: any = [];
  public _favorite_product = { code: '' };
  public _dobs: any = [];
  _dob = {
    date: "",
    content: ""
  };
  public _contacts: any = [];
  _contact = {
    type: "phone",
    value: ""
  };
  select_contacts: any = [{ name: 'Phone', value: "phone" }, { name: 'Email', value: "email" }];
  _primary_contact: any = [
    { name: 'Viber', value: 'viber' },
    { name: 'Zalo', value: 'zalo' },
    { name: 'FaceBook', value: 'fb' },
    { name: 'Email', value: 'email' },
    { name: 'Whatsapp', value: 'whatsapp' },
    { name: 'Wechat', value: 'wechat' },
    { name: 'Sms', value: 'sms' },
    { name: 'Call', value: 'call' },
    { name: 'Directly meeting', value: 'directly_meeting' },
  ];
  _budget: string;
  public _cities: any = [];
  public _districts: any = [];
  public quatity_dob: number = 1;

  constructor(
    public customerRepository: CustomerRepository,
    public countryReponsitory: CountryRepository,
    public userReponsitory: UsersRepository,
    public customerTypeReponsitory: CustomerTypeRepository,
    public hobbyReponsitory: HobbyRepository,
    public brandReponsitory: BrandsRepository,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    public http: HttpClient,
    private scopeService: ScopeService,
  ) {
  }

  ngOnInit() {
    this.filterMoney();
    this.customer = new Customer();
    this.loadHobby();
    this.loadBrand();
    //this.loadCustomerType();
    this.getSaleman();
    this.getListCountries();
  }
  filterMoney() {
    helper.filterMoneys();
  }
  loadEditCustomer() {
    this.activatedRoute.params.subscribe((data) => {
      let id = data.id;
      this.customerRepository.find(id).subscribe((res) => {
        this.customer = res;
        (this.customer.salesman && this.customer.salesman.id) ? this.customer.salesman_id = this.customer.salesman.id : null;
        this.customer.hobbies = this.customer.hobbies.map(e => {
          return e.id;
        })
        this.customer.hobbies.forEach(item => {
          this.hobbies.forEach(item1 => {
            if (item == item1.id) {
              this.hobbies_req.push(item1);
            }
          });
        });
        if (this.customer && this.customer.city) {
          this.getListCity(this.customer.country);
        }
        (this.customer.budget) ? this._budget = this.customer.budget = helper.formatNumber(this.customer.budget, '') : null;
        (this.customer.customer_classification) ? this.changePotential() : '';
        (this.customer.anniversaries.length > 0) ? this._dobs = this.customer.anniversaries : this._dobs.push({ date: "", content: "" });
        (this.customer.contacts.length > 0) ? this._contacts = this.customer.contacts : this._contacts.push({ type: 'phone', value: '' });
      });
    });
  }
  getSaleman() {
    this.userReponsitory.all({ status: 1 })
      .subscribe(res => {
        this.salesman = res;
      })
  }
  loadCustomerType() {
    this.customerTypeReponsitory.all()
      .subscribe((res?: any) => {
        this.customer_type = res;
        this.customer.customer_type_id = this.customer_type[0].id;
      })
  }
  loadHobby() {
    this.hobbyReponsitory.all()
      .subscribe((res?: any) => {
        this.hobbies = res;
        this.loadEditCustomer();
      })
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.customer.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  checkHobby(item?: any) {
    let index = this.hobbies_req.indexOf(item);
    if (index == -1) {
      this.hobbies_req.push(item);
      this.customer.hobbies.push(item.id);
    } else {
      this.hobbies_req.splice(index, 1);
      this.customer.hobbies.splice(index, 1);
    }
  }
  loadBrand() {
    this.brandReponsitory.all({ status: 1 })
      .subscribe(res => {
        this.brand = res;
        this.getListUserScope();
      })
  }
  onFileChanged(event) {
    this.selectedFile = <File>event.target.files[0];
    if (typeof this.selectedFile != 'undefined') {
      this.onUpload();
    }
  }
  onUpload() {
    this.customerRepository.uploadAvatar(this.customer, this.selectedFile)
      .subscribe((res: any) => {
        if (res.data && res.data.path_string) {
          this.customer.avatar = res.data.path_string;
        }
      });
  }
  removeUpload() {
    this.customer.avatar = null;
  }
  handleAddressChange(address: Address) {
    this.customer.address = address.formatted_address;
  }
  onSubmit() {
    this.customer.level = null;
    this.formatData();
    this.customerRepository.save(this.customer)
      .subscribe((res) => {
        this.router.navigateByUrl('/customer');
        helper.showNotification(`${this.customer.name} updated successfully!!`, 'done', 'success');
      });
  }
  //feed back
  addNewDob() {
    var _this = this;
    _this._dobs.push(JSON.parse(JSON.stringify(_this._dob)));
  }
  removeDob(index) {
    this._dobs.splice(index, 1);
  }
  getListCountries() {
    let sen_data = {
      order_by: "created_at",
      order: "desc",
      pagination: 0,
      authorization: 'none'
    }
    this.countryReponsitory.all(sen_data)
      .subscribe((res?: any) => {
        this.countries = res;
      })
  }
  changeCountry(item) {
    let _countryId;
    this.customer.city = '',
      this.customer.district = '',
      this._districts = [];
    this.countries.forEach(_item => {
      if (_item.name == item) {
        _countryId = _item;
      }
    });
    this.countryReponsitory.getCityByCountry(_countryId)
      .subscribe((res?: any) => {
        this._cities = res.data;
      })
  }
  getListCity(item) {
    let _countryId;
    this.countries.forEach(_item => {
      if (_item.name == item) {
        _countryId = _item;
      }
    });
    this.countryReponsitory.getCityByCountry(_countryId)
      .subscribe((res?: any) => {
        this._cities = res.data;
        if (this._cities.length > 0 && this.customer.district) {
          this.changeCity(this.customer.city);
        }
        console.log(this._cities)
      })
  }
  changeCity(item) {
    let _cityId;
    this._cities.forEach(_item => {
      if (_item.name == item) {
        _cityId = _item;
      }
    });
    this.countryReponsitory.getDistrictsByCountry(_cityId)
      .subscribe((res?: any) => {
        this._districts = res.data;
        console.log(this._cities)
      })
  }
  formatData() {
    if (this._budget) {
      let __budget = '';
      this._budget.split(',').forEach(_b => {
        __budget += _b;
      });
      this.customer.budget = Number(__budget);
    }
    this._dobs = this._dobs.filter(_o => {
      return Boolean(_o.date) == true && Boolean(_o.content) == true;
    });
    this.customer.anniversaries = this._dobs;

    this._contacts = this._contacts.filter(_o => {
      return Boolean(_o.value) == true;
    });
    this.customer.contacts = this._contacts;
  }
  changePotential() {
    let potential1 = [
      { name: '1 time', value: '1time' },
      { name: '2 times', value: '2times' },
      { name: '3 times', value: '3times' },
    ];
    let potential2 = [
      { name: 'Very potential', value: 'very_potential' },
      { name: 'Potential', value: 'potential' },
      { name: 'Less potential', value: 'less_potential' },
      { name: 'Not potential', value: 'not_potential' },
      { name: 'Black list', value: 'Black list' },
    ];
    if (this.customer.customer_classification == "purchased") {
      this._potential = potential1;
    } else {
      this._potential = potential2;
    }
  }
  addNewContact() {
    this._contact.type = 'phone';
    this._contact.value = '';
    var _this = this;
    _this._contacts.push(JSON.parse(JSON.stringify(_this._contact)));
  }
  removeContact(index) {
    this._contacts.splice(index, 1);
  }
  changeContact(item) {
    item.value = '';
  }
}
