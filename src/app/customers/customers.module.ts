import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomersRoutingModule } from './customers-routing.module';
import { ListCustomerComponent } from './list-customer/list-customer.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CreateCustomersComponent } from './create-customers/create-customers.component';
import { FormsModule } from '@angular/forms';
import { EditCustomersComponent } from './edit-customers/edit-customers.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { AddSalemanComponent } from './add-saleman/add-saleman.component';

@NgModule({
    declarations: [ListCustomerComponent, CreateCustomersComponent, EditCustomersComponent, OrderHistoryComponent, AddSalemanComponent],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        CustomersRoutingModule,
    ],
    entryComponents: [
        AddSalemanComponent
    ]
})
export class CustomersModule {
}
