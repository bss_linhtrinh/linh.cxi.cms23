import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {environment} from '../environments/environment';
import {Observable} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';
import {Provider} from './shared/types/providers';
import {AuthService} from './shared/services/auth/auth.service';
import {LocalStorage} from 'ngx-webstorage';
import {Scope} from './shared/types/scopes';
import {StorageService} from './shared/services/storage/storage.service';

@Injectable()
export class APIInterceptor implements HttpInterceptor {

    @LocalStorage('settings.locale') locale: string;
    @LocalStorage('language-left-sidebar.locale') llsLocale: string;

    provider: any;
    scope: any;
    filter: any;
    user: any;
    defaultAppId: string;

    constructor(
        private cookiesService: CookieService,
        private authService: AuthService,
        private storageService: StorageService
    ) {
    }


    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let apiReq: HttpRequest<any>;

        // Base URL
        if (!req.url.includes(`http`)) {
            apiReq = req.clone({url: `${environment.baseApiUrl}/${req.url}`});
        } else {
            apiReq = req.clone();
        }


        this.provider = this.authService.getCurrentProvider();
        this.scope = this.authService.getCurrentScope();
        this.filter = this.authService.getCurrentFilter();
        this.user = this.authService.currentUser();
        this.getDefaultAppId();


        apiReq = this.makeDefaultApiHeaders(apiReq);
        apiReq = this.makeCxiHeaders(apiReq);


        // access_token
        const access_token = this.authService.getAccessToken(this.provider);
        const authorization = apiReq.params.get('authorization');
        if (access_token && authorization !== 'none') {
            apiReq = apiReq.clone({
                setHeaders: {
                    'Authorization': `Bearer ${access_token}`
                }
            });
        }


        // filter by:
        const filterParams: any = {
            organization_id: apiReq.params.get('organization_id'),
            brand_id: apiReq.params.get('brand_id'),
            outlet_id: apiReq.params.get('outlet_id'),
        };
        if (this.provider === Provider.SuperAdmin) {
            if (this.filter) {
                if (this.filter.outlet && this.filter.outlet.id) {
                    filterParams.organization_id = (typeof filterParams.organization_id !== 'undefined' && filterParams.organization_id)
                        ? filterParams.organization_id
                        : this.filter.organization.id;
                    filterParams.brand_id = (typeof filterParams.brand_id !== 'undefined' && filterParams.brand_id)
                        ? filterParams.brand_id
                        : this.filter.brand.id;
                    filterParams.outlet_id = (typeof filterParams.outlet_id !== 'undefined' && filterParams.outlet_id)
                        ? filterParams.outlet_id
                        : this.filter.outlet.id;
                } else if (this.filter.brand && this.filter.brand.id) {
                    filterParams.organization_id = (typeof filterParams.organization_id !== 'undefined' && filterParams.organization_id)
                        ? filterParams.organization_id
                        : this.filter.organization.id;
                    filterParams.brand_id = (typeof filterParams.brand_id !== 'undefined' && filterParams.brand_id)
                        ? filterParams.brand_id
                        : this.filter.brand.id;
                } else if (this.filter.organization && this.filter.organization.id) {
                    filterParams.organization_id = (typeof filterParams.organization_id !== 'undefined' && filterParams.organization_id)
                        ? filterParams.organization_id
                        : this.filter.organization.id;
                }
            }
        } else if (this.provider === Provider.Admin) {
            if (this.user) {
                if (this.user.outlet && this.user.outlet.id) {
                    filterParams.organization_id = (typeof filterParams.organization_id !== 'undefined' && filterParams.organization_id)
                        ? filterParams.organization_id
                        : this.user.organization.id;
                    filterParams.brand_id = (typeof filterParams.brand_id !== 'undefined' && filterParams.brand_id)
                        ? filterParams.brand_id
                        : this.user.brand.id;
                    filterParams.outlet_id = (typeof filterParams.outlet_id !== 'undefined' && filterParams.outlet_id)
                        ? filterParams.outlet_id
                        : this.user.outlet.id;
                } else if (this.user.brand && this.user.brand.id) {
                    filterParams.organization_id = (typeof filterParams.organization_id !== 'undefined' && filterParams.organization_id)
                        ? filterParams.organization_id
                        : this.user.organization.id;
                    filterParams.brand_id = (typeof filterParams.brand_id !== 'undefined' && filterParams.brand_id)
                        ? filterParams.brand_id
                        : this.user.brand.id;

                    if (this.filter && this.filter.outlet && this.filter.outlet.id) {
                        filterParams.outlet_id = (typeof filterParams.outlet_id !== 'undefined' && filterParams.outlet_id)
                            ? filterParams.outlet_id
                            : this.filter.outlet.id;
                    }
                } else if (this.user.organization && this.user.organization.id) {
                    filterParams.organization_id = (typeof filterParams.organization_id !== 'undefined' && filterParams.organization_id)
                        ? filterParams.organization_id
                        : this.user.organization.id;

                    if (this.filter && this.filter.outlet && this.filter.outlet.id) {
                        filterParams.brand_id = (typeof filterParams.brand_id !== 'undefined' && filterParams.brand_id)
                            ? filterParams.brand_id
                            : this.filter.brand.id;
                        filterParams.outlet_id = (typeof filterParams.outlet_id !== 'undefined' && filterParams.outlet_id)
                            ? filterParams.outlet_id
                            : this.filter.outlet.id;
                    } else if (this.filter && this.filter.brand && this.filter.brand.id) {
                        filterParams.brand_id = (typeof filterParams.brand_id !== 'undefined' && filterParams.brand_id)
                            ? filterParams.brand_id
                            : this.filter.brand.id;
                    }
                }
            }
        }

        if (filterParams && filterParams.organization_id && parseInt(filterParams.organization_id, 10) !== -1) {
            apiReq = apiReq.clone({
                setParams: {
                    organization_id: filterParams.organization_id
                }
            });
        } else {
            apiReq = apiReq.clone({
                params: apiReq.params.delete('organization_id')
            });
        }

        if (filterParams && filterParams.brand_id && parseInt(filterParams.brand_id, 10) !== -1) {
            apiReq = apiReq.clone({
                setParams: {
                    brand_id: filterParams.brand_id
                }
            });
        } else {
            apiReq = apiReq.clone({
                params: apiReq.params.delete('brand_id')
            });
        }

        if (filterParams && filterParams.outlet_id && parseInt(filterParams.outlet_id, 10) !== -1) {
            apiReq = apiReq.clone({
                setParams: {
                    outlet_id: filterParams.outlet_id
                }
            });
        } else {
            apiReq = apiReq.clone({
                params: apiReq.params.delete('outlet_id')
            });
        }


        // booking
        if (filterParams && filterParams.organization_id && parseInt(filterParams.organization_id, 10) !== -1) {
            apiReq = apiReq.clone({
                setHeaders: {
                    'X-ORGANIZATION-ID': `${filterParams.organization_id}`
                }
            });
        }

        if (filterParams && filterParams.brand_id && parseInt(filterParams.brand_id, 10) !== -1) {
            apiReq = apiReq.clone({
                setHeaders: {
                    'X-BRAND-ID': `${filterParams.brand_id}`
                }
            });
        }

        if (this.provider !== null) {
            apiReq = apiReq.clone({
                setHeaders: {
                    'X-PROVIDER': `${this.provider}`
                }
            });
        }

        return next.handle(apiReq);
    }

    getDefaultAppId() {
        return this.defaultAppId = this.storageService.retrieve('system.app_id');
    }

    makeDefaultApiHeaders(apiReq: any) {
        apiReq = this.makeDefaultContentTypeHeader(apiReq);
        return apiReq;
    }

    makeDefaultContentTypeHeader(apiReq: any) {
        if (apiReq.body instanceof FormData) {
            // not
        } else {
            apiReq = apiReq.clone({
                setHeaders: {
                    'Content-Type': 'application/json',
                }
            });
        }

        return apiReq;
    }

    makeCxiHeaders(apiReq: any) {
        apiReq = this.makeCxiXAppHeaders(apiReq);
        return apiReq;
    }

    makeCxiXAppHeaders(apiReq: any) {

        // X-APP-PLATFORM
        let headers: any;
        headers = {
            'X-APP-PLATFORM': 'web_cms',
            // 'X-ORGANIZATION-ID': apiReq.params.get('organization_id'),
            // 'X-BRAND-ID': apiReq.params.get('brand_id')
        };


        // X-APP-ID
        const exceptionUrl = [
            // `${environment.baseApiUrl}/oauth/sign-in`
        ];
        if (!exceptionUrl.includes(apiReq.url)) {
            switch (this.provider) {
                case Provider.SuperAdmin:
                    break;

                case Provider.Admin:
                default:
                    switch (this.scope) {
                        case Scope.BSS:
                            delete headers['X-APP-ID'];
                            break;

                        case Scope.Organization:
                        case Scope.Brand:
                        case Scope.Outlet:
                        default:
                            const appId = (
                                this.filter &&
                                this.filter.organization &&
                                this.filter.organization.id &&
                                this.filter.organization.website &&
                                this.filter.organization.website.app_id
                            )
                                ? this.filter.organization.website.app_id
                                : (
                                    this.defaultAppId
                                        ? this.defaultAppId
                                        : null
                                );
                            if (appId) {
                                headers['X-APP-ID'] = appId;
                            }
                            break;
                    }
                    break;
            }
        }

        // X-APP-LOCALE
        headers['X-APP-LOCALE'] = apiReq.method === 'GET'
            ? (
                this.llsLocale
                    ? this.llsLocale
                    : (
                        this.locale
                            ? this.locale
                            : 'en'
                    )
            )
            : (
                this.locale
                    ? this.locale
                    : 'en'
            );

        apiReq = apiReq.clone({
            setHeaders: headers
        });

        return apiReq;
    }
}
