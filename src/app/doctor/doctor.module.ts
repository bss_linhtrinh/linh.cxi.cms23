import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import { FormsModule } from '@angular/forms';
import {DoctorRoutingModule} from './doctor-routing.module';
import {SharedModule} from '../shared/shared.module';
import {ListDoctorComponent} from './list-doctor/list-doctor.component';
import {CreateDoctorComponent} from './create-doctor/create-doctor.component';
// import {EditDoctorComponent} from './edit-doctor/edit-doctor.component';

@NgModule({
    declarations: [
        ListDoctorComponent,
        CreateDoctorComponent,
        // EditDoctorComponent
    ],
    imports: [
        DoctorRoutingModule,
        CommonModule,
        SharedModule,
        FormsModule,
    ]
})
export class DoctorModule {
}
