import { Component, OnInit } from '@angular/core';
import { Gender } from '../../shared/entities/gender';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { UtilitiesService } from 'src/app/shared/services/utilities/utilities.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
import { environment } from 'src/environments/environment';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { UserScope } from 'src/app/shared/entities/user-scope';
import {Doctor} from '../../shared/entities/doctor';
import {DoctorRepository} from '../../shared/repositories/doctor.repository';
declare const helper: any;

@Component({
    selector: 'app-create-doctor',
    templateUrl: './create-doctor.component.html',
    styleUrls: ['./create-doctor.component.scss']
})
export class CreateDoctorComponent implements OnInit {

    public doctor: Doctor;
    public genders: Gender[] = Gender.init();
    public baseUrl: string = environment.baseUrl;
    public brand = [];
    minDate = new Date('1970-01-01');
    maxDate = new Date();
    userScope: UserScope;
    // upload avatar
    selectedFile: File = null;

    constructor(
        public doctorRepository: DoctorRepository,
        public brandsRepository: BrandsRepository,
        public router: Router,
        public http: HttpClient,
        public utilitiesService: UtilitiesService,
        private scopeService: ScopeService,
    ) { }

    ngOnInit() {
        this.doctor = new Doctor();
        this.loadBrand();
    }
    getListUserScope() {
        this.scopeService.getUserScope$()
            .subscribe(
                (res: any) => {
                    this.userScope = res;
                    if (this.userScope.brand) {
                        this.doctor.brand_id = this.userScope.brand.id;
                    }
                }
            );
    }
    loadBrand() {
        this.brandsRepository.all({ status: 1 })
            .subscribe(res => {
                this.brand = res;
                this.doctor.brand_id = this.brand[0].id;
                this.getListUserScope();
            });
    }
    // region Avatar
    onFileChanged(event) {
        this.selectedFile = <File>event.target.files[0];
        this.onUpload();
    }
    onUpload() {
        this.doctorRepository.uploadCreate(this.doctor, this.selectedFile)
            .subscribe((res) => {
                if (res.data && res.data.path_string) {
                    this.doctor.avatar = res.data.path_string;
                }
            });
    }
    removeUpload() {
        this.doctor.avatar = null;
    }
    // endregion
    autoGenerate() {
        const randomPassword = this.utilitiesService.randomPassword();
        this.doctor.password = randomPassword;
        this.doctor.password_confirmation = randomPassword;
    }
    handleAddressChange(address: Address, field: string) {
        this.doctor[field] = address.formatted_address;
    }
    // Submit
    onSubmit() {
        this.doctor.level = null;
        this.doctorRepository.save(this.doctor)
            .subscribe(
                (res) => {
                    this.router.navigateByUrl('/doctor');
                    helper.showNotification(`${this.doctor.name} has been added successfully!!`, 'done', 'success');
                },
                errors => console.log(errors)
            );
    }
}
