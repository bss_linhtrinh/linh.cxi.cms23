import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CheckPermissionsGuard} from '../shared/guards/check-permissions/check-permissions.guard';
import {ListDoctorComponent} from './list-doctor/list-doctor.component';
import {CreateDoctorComponent} from './create-doctor/create-doctor.component';
// import {EditDoctorComponent} from './edit-doctor/edit-doctor.component';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: '', redirectTo: 'list', pathMatch: 'full'},
            {path: 'list', component: ListDoctorComponent, data: {breadcrumb: 'List doctor'}},
            {path: 'create', component: CreateDoctorComponent, data: {breadcrumb: 'Create doctor'}},
            // {path: ':id/edit', component: EditDoctorComponent, data: {breadcrumb: 'Edit doctor'}},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DoctorRoutingModule {
}
