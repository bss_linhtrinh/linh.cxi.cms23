import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorage } from 'ngx-webstorage';
import {CxiGridConfig} from '../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../shared/components/cxi-grid/cxi-grid-column';
import {Doctor} from '../../shared/entities/doctor';
import {DoctorRepository} from '../../shared/repositories/doctor.repository';
import {ColumnFilterType} from '../../shared/types/column-filter-type';
import {ColumnFormat} from '../../shared/types/column-format';

@Component({
    selector: 'app-list-doctor',
    templateUrl: './list-doctor.component.html',
    styleUrls: ['./list-doctor.component.scss']
})
export class ListDoctorComponent implements OnInit {

    public doctors: Doctor[] = [];
    tab: string = 'en';
    // table
    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        pagingServer: true,
        repository: this.doctorRepository,
        translatable: false,
        actions: {
            onEdit: (doctor: Doctor) => {
                return this.onEdit(doctor);
            },
            onDelete: (doctor: Doctor) => {
                return this.onDelete(doctor);
            },
            onActivate: (doctor: Doctor) => {
                this.onActivate(doctor);
            },
            onDeactivate: (doctor: Doctor) => {
                this.onDeactivate(doctor);
            },
        }
    });

    cxiGridColumns = CxiGridColumn.from([
        { title: 'Name', name: 'name', avatar: 'images' },
        { title: 'Email', name: 'email' },
        { title: 'Phone', name: 'phone', className: 'phone-number', format: ColumnFormat.PhoneNumber },
        { title: 'Start date', name: 'start_date', filterType: ColumnFilterType.Datepicker, format: ColumnFormat.Date },
        { title: 'Health center', name: 'health_center', filterType: ColumnFilterType.MultiSelect }
    ]);

    @LocalStorage('scope.organization_id') organizationId: number;
    @LocalStorage('scope.brand_id') brandId: number;
    @LocalStorage('scope.outlet_id') outletId: number;

    constructor(
        private doctorRepository: DoctorRepository,
        private router: Router,
        public activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit() {
    }

    onEdit(doctor: Doctor) {
        this.router.navigate(['..', doctor.id, 'edit'], { relativeTo: this.activatedRoute });
    }

    onDelete(doctor: Doctor) {
        this.doctorRepository.destroy(doctor).subscribe();
    }

    onActivate(doctor: Doctor) {
        this.doctorRepository.activate(doctor).subscribe();
    }

    onDeactivate(doctor: Doctor) {
        this.doctorRepository.deactivate(doctor).subscribe();
    }
}
