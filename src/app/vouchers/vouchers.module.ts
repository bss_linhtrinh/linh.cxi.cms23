import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {VouchersRoutingModule} from './vouchers-routing.module';
import {ListVouchersComponent} from './list-vouchers/list-vouchers.component';
import {CreateVoucherComponent} from './create-voucher/create-voucher.component';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';
import { GiftVoucherComponent } from './gift-voucher/gift-voucher.component';
import { CashVoucherComponent } from './cash-voucher/cash-voucher.component';
import { EditVoucherComponent } from './edit-voucher/edit-voucher.component';


@NgModule({
    declarations: [
        ListVouchersComponent,
        CreateVoucherComponent,
        GiftVoucherComponent,
        CashVoucherComponent,
        EditVoucherComponent
    ],
    imports: [
        SharedModule,
        FormsModule,
        CommonModule,
        VouchersRoutingModule
    ]
})
export class VouchersModule {
}
