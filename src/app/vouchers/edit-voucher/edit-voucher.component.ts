import {Component, OnInit} from '@angular/core';
import {Outlet} from '../../shared/entities/outlets';
import {Voucher} from '../../shared/entities/voucher';
import {UtilitiesService} from '../../shared/services/utilities/utilities.service';
import {ActivatedRoute, Router} from '@angular/router';
import {OutletsRepository} from '../../shared/repositories/outlets.repository';
import {VouchersRepository} from '../../shared/repositories/vouchers.repository';
import {filter, map, switchMap, tap} from 'rxjs/operators';

@Component({
    selector: 'app-edit-voucher',
    templateUrl: './edit-voucher.component.html',
    styleUrls: ['./edit-voucher.component.scss']
})
export class EditVoucherComponent implements OnInit {

    public voucher: Voucher;
    minDate = new Date();
    // maxDate = new Date();
    outlets: Outlet[];
    types = [
        {id: 1, value: 'gift_voucher', name: 'Gift Voucher'},
        {id: 2, value: 'cash_voucher_statistic', name: 'Cash Voucher - Statistic'},
        {id: 3, value: 'cash_voucher_discount', name: 'Cash Voucher - Discount'},
    ];
    allItemsOptions = [
        {name: 'all', value: true},
        {name: 'specific_items', value: false}
    ];
    allOutletsOptions = [
        {name: 'all', value: true},
        {name: 'specific_items', value: false}
    ];

    constructor(
        private utilitiesService: UtilitiesService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private outletsRepository: OutletsRepository,
        private vouchersRepository: VouchersRepository
    ) {
    }

    ngOnInit() {
        this.getVoucherDetail();
        this.getOutlets();
    }

    getOutlets() {
        this.outletsRepository.all()
            .pipe(
                tap((outlets: Outlet[]) => this.outlets = outlets)
            )
            .subscribe(
            );
    }

    getVoucherDetail() {
        this.activatedRoute.params
            .pipe(
                filter((data) => data.id),
                map((data) => data.id),
                switchMap(
                    id => this.vouchersRepository.find(id)
                ),
                tap((voucher: Voucher) => this.voucher = voucher)
            )
            .subscribe(
            );
    }

    editVoucher() {
        this.vouchersRepository.save(this.voucher)
            .pipe(
                tap(() => this.cancel())
            )
            .subscribe(
            );
    }

    cancel() {
        this.back();
    }

    back() {
        this.router.navigate(['../..', 'list'], {relativeTo: this.activatedRoute});
    }

    autoGenerateCode() {
        this.voucher.code = this.utilitiesService.randomCode(8);
    }
}
