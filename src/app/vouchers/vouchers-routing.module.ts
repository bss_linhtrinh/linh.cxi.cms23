import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CheckPermissionsGuard} from '../shared/guards/check-permissions/check-permissions.guard';
import {CreateVoucherComponent} from './create-voucher/create-voucher.component';
import {ListVouchersComponent} from './list-vouchers/list-vouchers.component';
import {GiftVoucherComponent} from './gift-voucher/gift-voucher.component';
import {CashVoucherComponent} from './cash-voucher/cash-voucher.component';
import {EditVoucherComponent} from './edit-voucher/edit-voucher.component';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            { path: 'list', component: ListVouchersComponent, data: { breadcrumb: 'List vouchers' } },
            { path: 'gift-voucher', component: GiftVoucherComponent, data: { breadcrumb: 'Gift vouchers' } },
            { path: 'cash-voucher', component: CashVoucherComponent, data: { breadcrumb: 'Cash vouchers' } },
            { path: 'create', component: CreateVoucherComponent, data: { breadcrumb: 'Create new voucher' } },
            { path: ':id/edit', component: EditVoucherComponent, data: { breadcrumb: 'Edit voucher' } },
            { path: '', redirectTo: 'list' },
        ]
    }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VouchersRoutingModule { }
