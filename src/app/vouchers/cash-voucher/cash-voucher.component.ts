import {Component, OnInit} from '@angular/core';
import {Promotion} from '../../shared/entities/promotion';

@Component({
    selector: 'app-cash-voucher',
    templateUrl: './cash-voucher.component.html',
    styleUrls: ['./cash-voucher.component.scss']
})
export class CashVoucherComponent implements OnInit {

    baseTableConfig = {
        paging: true,
        sorting: true,
        filtering: false,
        selecting: true,
        className: false,
        // pagingServer: true,
        // repository: this.promotionRepository,
        actions: {
            onEdit: (promotion: Promotion) => {
                return this.onEdit(promotion);
            },
            onDelete: (promotion: Promotion) => {
                return this.onDelete(promotion);
            },
        }
    };
    baseTableColumns = [
        {title: 'name', name: 'name', avatar: 'images'},
        {title: 'quantity', name: 'quantity', className: 'quantity'},
        {title: 'condition', name: 'condition', className: 'condition'},
        {title: 'type', name: 'type', className: 'type'},
        {title: 'value', name: 'value', className: 'value'},
        {title: 'begin date', name: 'starts_from', className: 'start-date', format: 'date'},
        {title: 'expiry date', name: 'end_till', className: 'end-date', format: 'date'},
        {title: 'status', name: 'status', className: 'status'},
        {title: 'amount', name: 'amount', className: 'amount'},
    ];

    constructor() {
    }

    ngOnInit() {
    }

    onEdit(promotion: Promotion) {
        // this.router.navigate(['..', promotion.id, 'edit'], {relativeTo: this.activatedRoute});
    }

    onDelete(promotion: Promotion) {
        // this.promotionRepository.destroy(promotion).subscribe();
    }

}
