import {Component, OnInit} from '@angular/core';
import {VouchersRepository} from '../../shared/repositories/vouchers.repository';
import {Voucher} from '../../shared/entities/voucher';
import {ActivatedRoute, Router} from '@angular/router';
import {CxiGridConfig} from '../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../shared/components/cxi-grid/cxi-grid-column';

@Component({
    selector: 'app-list-vouchers',
    templateUrl: './list-vouchers.component.html',
    styleUrls: ['./list-vouchers.component.scss']
})
export class ListVouchersComponent implements OnInit {
    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        className: false,
        pagingServer: true,
        repository: this.vouchersRepository,
        actions: {
            onEdit: (voucher: Voucher) => {
                return this.onEdit(voucher);
            },
            onDelete: (voucher: Voucher) => {
                return this.onDelete(voucher);
            },
        }
    });
    cxiGridColumns = CxiGridColumn.from([
        {title: 'name', name: 'name', className: 'code'},
        {title: 'type', name: 'type', className: 'type'},
        {title: 'code', name: 'code', className: 'name'},
        {title: 'used', name: 'used', className: 'used'},
        {title: 'in stock', name: 'in_stock', className: 'in_stock'},
        {title: 'quantity', name: 'quantity', className: 'quantity'},
        {title: 'begin date', name: 'begin_date', className: 'start-date', format: 'date'},
        {title: 'expiry date', name: 'expiry_date', className: 'end-date', format: 'date'},
        {title: 'status', name: 'status', className: 'status'},
    ]);

    constructor(
        private router: Router,
        private vouchersRepository: VouchersRepository,
        public activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit() {
    }

    onEdit(voucher: Voucher) {
        this.router.navigate(['..', voucher.id, 'edit'], {relativeTo: this.activatedRoute});
    }

    onDelete(voucher: Voucher) {
        this.vouchersRepository.destroy(voucher).subscribe();
    }

}
