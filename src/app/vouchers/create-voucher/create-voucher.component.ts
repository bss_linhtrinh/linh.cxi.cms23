import {Component, OnInit} from '@angular/core';
import {Voucher} from '../../shared/entities/voucher';
import {UtilitiesService} from '../../shared/services/utilities/utilities.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Outlet} from '../../shared/entities/outlets';
import {OutletsRepository} from '../../shared/repositories/outlets.repository';
import {VouchersRepository} from '../../shared/repositories/vouchers.repository';
import {tap} from 'rxjs/operators';

@Component({
    selector: 'app-create-voucher',
    templateUrl: './create-voucher.component.html',
    styleUrls: ['./create-voucher.component.scss']
})
export class CreateVoucherComponent implements OnInit {

    public voucher: Voucher = new Voucher();
    minDate = new Date();
    // maxDate = new Date();o
    outlets: Outlet[];
    types = [
        {id: 1, value: 'gift_voucher', name: 'Gift Voucher'},
        {id: 2, value: 'cash_voucher_statistic', name: 'Cash Voucher - Statistic'},
        {id: 3, value: 'cash_voucher_discount', name: 'Cash Voucher - Discount'},
    ];
    allItemsOptions = [
        {name: 'all', value: true},
        {name: 'specific_items', value: false}
    ];
    allOutletsOptions = [
        {name: 'all', value: true},
        {name: 'specific_items', value: false}
    ];

    constructor(
        private router: Router,
        private utilitiesService: UtilitiesService,
        private activatedRoute: ActivatedRoute,
        private outletsRepository: OutletsRepository,
        private vouchersRepository: VouchersRepository
    ) {
    }

    ngOnInit() {
        this.getOutlets();
    }

    getOutlets() {
        this.outletsRepository.all()
            .pipe(
                tap((outlets: Outlet[]) => this.outlets = outlets)
            )
            .subscribe(
            );
    }

    back() {
        this.router.navigate(['../list'], {relativeTo: this.activatedRoute});
    }

    cancel() {
        this.back();
    }

    createVoucher() {
        this.vouchersRepository.save(this.voucher)
            .pipe(
                tap(() => this.back())
            )
            .subscribe(
            );
    }

    autoGenerateCode() {
        this.voucher.code = this.utilitiesService.randomCode(8);
    }
}
