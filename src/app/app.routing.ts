import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CheckDomainGuard} from './shared/guards/check-domain/check-domain.guard';

const appRoutes: Routes = [
    {
        path: 'auth',
        loadChildren: () => import('./auth/auth.module').then(mod => mod.AuthModule),
        canLoad: [CheckDomainGuard]
    },
    {
        path: 'demo',
        loadChildren: () => import('./demo/demo.module').then(mod => mod.DemoModule),
        canLoad: [CheckDomainGuard]
    },
    {
        path: '',
        loadChildren: () => import('./cxi/cxi.module').then(mod => mod.CxiModule),
        canLoad: [CheckDomainGuard]
    },
    {path: '**', redirectTo: '/errors', data: {error: 404}}
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            {
                enableTracing: true, // <-- debugging purposes only
                onSameUrlNavigation: 'reload'
            }
        )
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
