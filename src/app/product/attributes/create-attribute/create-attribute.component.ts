import { Component, OnInit } from '@angular/core';
import { Attribute } from 'src/app/shared/entities/attribute';
import { ActivatedRoute, Router } from '@angular/router';
import { AttributesRepository } from 'src/app/shared/repositories/attributes.repository';
import { AttributeTypes } from '../../../shared/constants/attribute';
import { yesOrNo } from '../../../shared/constants/yesorno';
import { LocalesRepository } from 'src/app/shared/repositories/locale.repository';
import { Locale } from 'src/app/shared/entities/locale';
import { AttributeTranslation } from 'src/app/shared/entities/attribute-translation';
import { map, tap } from 'rxjs/operators';

@Component({
    selector: 'app-create-attribute',
    templateUrl: './create-attribute.component.html',
    styleUrls: ['./create-attribute.component.scss']
})
export class CreateAttributeComponent implements OnInit {

    attribute: Attribute = new Attribute();
    // options: AttributeOption[] = [];
    locales: Locale[] = [];
    baseAttributeTypes = AttributeTypes;
    yesOrNo = yesOrNo;
    tab: string = 'en';

    columns = [];

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private attributesRepository: AttributesRepository,
        private localeRepository: LocalesRepository
    ) {
    }

    ngOnInit() {
        this.getLocales();
    }

    getLocales() {
        this.localeRepository.all()
            .pipe(
                tap(
                    (locales: Locale[]) => this.locales = locales
                ),
                tap(
                    (locales: Locale[]) => {
                        locales.forEach(
                            (locale: Locale) => {
                                this.columns.push(
                                    {
                                        title: `${locale.name}(${locale.code})`,
                                        name: `translations`
                                    }
                                );
                            }
                        );
                    }
                ),
                map(
                    (locales: Locale[]): AttributeTranslation[] => {
                        return this.attribute.translations = locales.map(
                            (locale: Locale): AttributeTranslation => new AttributeTranslation().parseCodeLocale(locale)
                        );
                    }
                ),
            )
            .subscribe(
            );
    }

    cancel() {
        this.router.navigate(['..', 'list'], { relativeTo: this.route });
    }

    onSubmit() {

        // this.attribute.options = this.options;

        console.log(this.attribute);
        this.attributesRepository.save(this.attribute)
            .subscribe(
                (attributes: Attribute) => {
                    this.attribute = attributes;
                    this.router.navigate(['..', 'list'], { relativeTo: this.route });
                }
            );
    }

    getLabelLocale(locale: Locale) {
        return `${locale.name} (${locale.code})`;
    }
}
