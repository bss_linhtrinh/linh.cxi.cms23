import { Component, OnInit } from '@angular/core';
import { AttributesRepository } from 'src/app/shared/repositories/attributes.repository';
import { ActivatedRoute, Router } from '@angular/router';
import { Attribute } from 'src/app/shared/entities/attribute';
import { CxiGridColumn } from '../../../shared/components/cxi-grid/cxi-grid-column';
import { CxiGridConfig } from '../../../shared/components/cxi-grid/cxi-grid-config';
import { LocalStorage } from 'ngx-webstorage';
import { ColumnFormat } from '../../../shared/types/column-format';

@Component({
    selector: 'app-list-attribute',
    templateUrl: './list-attribute.component.html',
    styleUrls: ['./list-attribute.component.scss']
})
export class ListAttributeComponent implements OnInit {

    // table
    tab: string = 'en';
    cxiTableConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        className: false,
        pagingServer: true,
        repository: this.attributesRepository,
        translatable: true,
        actions: {
            onEdit: (attribute: Attribute) => {
                this.onEdit(attribute);
            },
            onDelete: (attribute: Attribute) => {
                this.onDelete(attribute);
            },
        }
    });
    cxiColumns = CxiGridColumn.from([
        { title: 'name', name: 'admin_name' },
        { title: 'type', name: 'type' },
        { title: 'options', name: 'options', fieldOption: 'admin_name', format: ColumnFormat.Tag },
        { title: 'required', name: 'is_required', filterType: 'select', format: ColumnFormat.Boolean },
        { title: 'unique', name: 'is_unique', filterType: 'select', format: ColumnFormat.Boolean },
    ]);

    @LocalStorage('scope.organization_id') organizationId: number;
    @LocalStorage('scope.brand_id') brandId: number;
    @LocalStorage('scope.outlet_id') outletId: number;

    constructor(
        private attributesRepository: AttributesRepository,
        private router: Router,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
    }

    onEdit(attribute: Attribute) {
        this.router.navigate(['..', attribute.id, 'edit'], { relativeTo: this.route });
    }

    onActivate(attribute: Attribute) {
        this.attributesRepository.activate(attribute)
            .subscribe();
    }

    onDeactivate(attribute: Attribute) {
        this.attributesRepository.deactivate(attribute)
            .subscribe();
    }

    onDelete(attribute: Attribute) {
        this.attributesRepository.destroy(attribute)
            .subscribe();
    }

}
