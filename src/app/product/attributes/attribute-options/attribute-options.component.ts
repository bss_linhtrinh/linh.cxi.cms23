import { Component, Inject, Input, OnInit, Optional, ViewChild } from '@angular/core';
import { ElementBase } from '../../../shared/custom-form/element-base/element-base';
import { AttributeOption } from '../../../shared/entities/attributeOption';
import { NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel } from '@angular/forms';
import { Locale } from '../../../shared/entities/locale';

@Component({
    selector: 'app-attribute-options',
    templateUrl: './attribute-options.component.html',
    styleUrls: ['./attribute-options.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: AttributeOptionsComponent, multi: true }
    ]
})
export class AttributeOptionsComponent extends ElementBase<AttributeOption[]> implements OnInit {
    @Input() name: string;
    @Input() locales: Locale[];
    @Input() showTranslate: string;

    @ViewChild(NgModel, { static: true }) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    addOption() {
        if (this.value) {
            const newAttributeOption = new AttributeOption();
            newAttributeOption.initOptionTranslations(this.locales);

            this.value.push(newAttributeOption);
        }
    }

    onRemove(index: number) {
        this.value.splice(index, 1);

        this.checkEmpty();
    }

    checkEmpty() {
        if (!this.value || this.value.length === 0) {
            this.addOption();
        }
    }
}
