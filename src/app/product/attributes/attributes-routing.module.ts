import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EditAttributeComponent} from './edit-attribute/edit-attribute.component';
import {CreateAttributeComponent} from './create-attribute/create-attribute.component';
import {ListAttributeComponent} from './list-attribute/list-attribute.component';
import {CheckPermissionsGuard} from '../../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: '', redirectTo: 'list', pathMatch: 'full'},
            {path: 'list', component: ListAttributeComponent, data: {breadcrumb: 'List attributes'}},
            {path: 'create', component: CreateAttributeComponent, data: {breadcrumb: 'Create attribute'}},
            {path: ':id/edit', component: EditAttributeComponent, data: {breadcrumb: 'Edit attribute'}},
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AttributesRoutingModule {
}
