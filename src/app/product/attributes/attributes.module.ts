import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AttributesRoutingModule} from './attributes-routing.module';
import {CreateAttributeComponent} from './create-attribute/create-attribute.component';
import {ListAttributeComponent} from './list-attribute/list-attribute.component';
import {EditAttributeComponent} from './edit-attribute/edit-attribute.component';
import {SharedModule} from 'src/app/shared/shared.module';
import {FormsModule} from '@angular/forms';
import {AttributeOptionsComponent} from './attribute-options/attribute-options.component';
import {AttributeOptionComponent} from './attribute-option/attribute-option.component';

@NgModule({
    declarations: [
        CreateAttributeComponent,
        ListAttributeComponent,
        EditAttributeComponent,
        AttributeOptionsComponent,
        AttributeOptionComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        AttributesRoutingModule,
        SharedModule,
    ]
})
export class AttributesModule {
}
