import {Component, OnInit} from '@angular/core';
import {Attribute} from 'src/app/shared/entities/attribute';
import {AttributesRepository} from 'src/app/shared/repositories/attributes.repository';
import {ActivatedRoute, Router} from '@angular/router';
import {AttributeTypes} from '../../../shared/constants/attribute';
import {yesOrNo} from '../../../shared/constants/yesorno';
import {LocalesRepository} from 'src/app/shared/repositories/locale.repository';
import {Locale} from 'src/app/shared/entities/locale';
import {AttributeTranslation} from 'src/app/shared/entities/attribute-translation';
import {map, switchMap, tap} from 'rxjs/operators';

@Component({
    selector: 'app-edit-attribute',
    templateUrl: './edit-attribute.component.html',
    styleUrls: ['./edit-attribute.component.scss']
})
export class EditAttributeComponent implements OnInit {

    attribute: Attribute = new Attribute();
    // options: AttributeOption[] = [];
    locales: Locale[] = [];
    id: string | number;
    baseAttributeTypes = AttributeTypes;
    yesOrNo = yesOrNo;
    tab: string = 'en';
    columns = [];

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private attributesRepository: AttributesRepository,
        private localeRepository: LocalesRepository
    ) {
    }

    ngOnInit() {
        this.id = this.route.snapshot.paramMap.get('id');
        this.getLocales();
    }

    getLocales() {
        this.localeRepository.all()
            .pipe(
                tap(
                    (locales: Locale[]) => this.locales = locales
                ),
                tap(
                    (locales: Locale[]) => {
                        locales.forEach(
                            (locale: Locale) => {
                                this.columns.push(
                                    {
                                        title: `${locale.name}(${locale.code})`,
                                        name: `translations`
                                    }
                                );
                            }
                        );
                    }
                ),
                map(
                    (locales: Locale[]): AttributeTranslation[] => {
                        return this.attribute.translations = locales.map(
                            (locale: Locale): AttributeTranslation => new AttributeTranslation().parseCodeLocale(locale)
                        );
                    }
                ),
                switchMap(
                    () => this.attributesRepository.find(this.id)
                        .pipe(
                            tap((attribute: Attribute) => this.attribute = attribute)
                        )
                )
            )
            .subscribe(
            );
    }

    cancel() {
        this.router.navigate(['../..', 'list'], {relativeTo: this.route});
    }

    onSubmit() {
        // this.attribute.options = this.options;
        // console.log(this.attribute);

        this.attributesRepository.save(this.attribute)
            .subscribe(
                (attribute: Attribute) => {
                    this.attribute = attribute;
                    this.router.navigate(['../..', 'list'], {relativeTo: this.route});
                }
            );
    }

    getLabelLocale(locale: Locale) {
        return `${locale.name} (${locale.code})`;
    }
}
