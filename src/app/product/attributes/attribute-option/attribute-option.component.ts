import { Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild } from '@angular/core';
import { ElementBase } from '../../../shared/custom-form/element-base/element-base';
import { AttributeOption } from '../../../shared/entities/attributeOption';
import { NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel } from '@angular/forms';

@Component({
    selector: 'app-attribute-option',
    templateUrl: './attribute-option.component.html',
    styleUrls: ['./attribute-option.component.scss'],
    providers: [
        { provide: NG_VALUE_ACCESSOR, useExisting: AttributeOptionComponent, multi: true }
    ]
})
export class AttributeOptionComponent extends ElementBase<AttributeOption> implements OnInit {
    @Input() name: string;
    @Input() showTranslate: string;
    @Output() remove = new EventEmitter();

    @ViewChild(NgModel, { static: true }) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onFileChanged(file: File) {
    }

    onRemove() {
        this.remove.emit();
    }
}
