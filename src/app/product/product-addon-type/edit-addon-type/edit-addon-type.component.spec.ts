import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAddonTypeComponent } from './edit-addon-type.component';

describe('EditAddonTypeComponent', () => {
  let component: EditAddonTypeComponent;
  let fixture: ComponentFixture<EditAddonTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAddonTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAddonTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
