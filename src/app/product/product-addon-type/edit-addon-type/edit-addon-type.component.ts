import { Component, OnInit } from '@angular/core';
import { Active } from 'src/app/shared/entities/active';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductAddonTypeRepository } from 'src/app/shared/repositories/product-addon-type';
import { AddOnType } from '../../../shared/entities/add-on-type';
import { filter, map, switchMap } from 'rxjs/operators';
import { DISPLAY_TYPES } from '../../../shared/constants/display-types';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
import { UserScope } from 'src/app/shared/entities/user-scope';

declare const helper: any;

@Component({
    selector: 'app-edit-addon-type',
    templateUrl: './edit-addon-type.component.html',
    styleUrls: ['./edit-addon-type.component.scss']
})
export class EditAddonTypeComponent implements OnInit {

    public addOnType: AddOnType = new AddOnType();
    public status: Active[] = Active.init();
    public displayTypes = DISPLAY_TYPES;
    userScope: UserScope;
    public brand = [];
    tab: string = 'en';
    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private addOnTypeRepository: ProductAddonTypeRepository,
        private scopeService: ScopeService,
        private brandRepository: BrandsRepository,
    ) {
    }

    ngOnInit() {
        this.loadAddonType();
        this.loadListBrand();
    }
    getListUserScope() {
        this.scopeService.getUserScope$()
            .subscribe(
                (res: any) => {
                    this.userScope = res;
                    if (this.userScope.brand) {
                        this.addOnType.brand_id = this.userScope.brand.id;
                    }
                }
            );
    }
    loadListBrand() {
        this.brandRepository.all()
            .subscribe(
                (res) => {
                    this.brand = res;
                    this.getListUserScope();
                }
            );
    }
    loadAddonType() {
        this.activatedRoute.params
            .pipe(
                filter(data => data.id),
                map(data => data.id),
                switchMap(
                    (id) => this.addOnTypeRepository.find(id)
                )
            )
            .subscribe((addOnType: AddOnType) => {
                this.addOnType = addOnType;
                this.addOnType.translations.forEach(item => {
                    if (item.locale == 'en') {
                        this.addOnType.en = item;
                    } else if (item.locale == 'vi') {
                        this.addOnType.vi = item;
                    }
                });
            });
    }
    onSubmit() {
        this.addOnType.translations = null;
        this.addOnTypeRepository.save(this.addOnType)
            .subscribe((res) => {
                this.cancel();
                helper.showNotification(`${this.addOnType.name} updated successfully!!`, 'done', 'success');
            });
    }
    cancel() {
        this.router.navigate(['../..', 'list'], { relativeTo: this.activatedRoute });
    }
}
