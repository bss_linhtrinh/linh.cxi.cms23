import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductAddonTypeRoutingModule } from './product-addon-type-routing.module';
import { CreateAddonTypeComponent } from './create-addon-type/create-addon-type.component';
import { ListAddonTypeComponent } from './list-addon-type/list-addon-type.component';
import { EditAddonTypeComponent } from './edit-addon-type/edit-addon-type.component';
import {SharedModule} from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [CreateAddonTypeComponent, ListAddonTypeComponent, EditAddonTypeComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ProductAddonTypeRoutingModule,
  ]
})
export class ProductAddonTypeModule { }
