import { Component, OnInit } from '@angular/core';
import { Active } from 'src/app/shared/entities/active';
import { ProductAddonTypeRepository } from 'src/app/shared/repositories/product-addon-type';
import { ActivatedRoute, Router } from '@angular/router';
import { AddOnType } from '../../../shared/entities/add-on-type';
import { DISPLAY_TYPES } from '../../../shared/constants/display-types';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';

declare const helper: any;

@Component({
    selector: 'app-create-addon-type',
    templateUrl: './create-addon-type.component.html',
    styleUrls: ['./create-addon-type.component.scss']
})
export class CreateAddonTypeComponent implements OnInit {
    public addonType: AddOnType = new AddOnType();
    public status: Active[] = Active.init();
    public displayTypes = DISPLAY_TYPES;
    userScope: UserScope;
    public brand = [];
    tab: string = 'en';
    constructor(
        private addOnTypeRepository: ProductAddonTypeRepository,
        private router: Router,
        private route: ActivatedRoute,
        private scopeService: ScopeService,
        private brandRepository: BrandsRepository,
    ) {
    }

    ngOnInit() {
        this.loadListBrand();
    }
    getListUserScope() {
        this.scopeService.getUserScope$()
            .subscribe(
                (res: any) => {
                    this.userScope = res;
                    if (this.userScope.brand) {
                        this.addonType.brand_id = this.userScope.brand.id;
                    }
                }
            );
    }
    loadListBrand() {
        this.brandRepository.all()
            .subscribe(
                (res) => {
                    this.brand = res;
                    this.addonType.brand_id = this.brand[0].id;
                    this.getListUserScope();
                }
            );
    }
    onSubmit() {
        this.addonType.translations = null;
        this.addOnTypeRepository.save(this.addonType)
            .subscribe(
                (res) => {
                    this.router.navigate(['../list'], { relativeTo: this.route });
                    helper.showNotification(`${this.addonType.name} has been added successfully!!`, 'done', 'success');
                }
            );
    }
}
