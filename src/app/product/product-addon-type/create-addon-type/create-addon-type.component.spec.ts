import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAddonTypeComponent } from './create-addon-type.component';

describe('CreateAddonTypeComponent', () => {
  let component: CreateAddonTypeComponent;
  let fixture: ComponentFixture<CreateAddonTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAddonTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAddonTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
