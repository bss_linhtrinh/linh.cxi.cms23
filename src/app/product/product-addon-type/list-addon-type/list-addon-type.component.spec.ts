import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAddonTypeComponent } from './list-addon-type.component';

describe('ListAddonTypeComponent', () => {
  let component: ListAddonTypeComponent;
  let fixture: ComponentFixture<ListAddonTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAddonTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAddonTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
