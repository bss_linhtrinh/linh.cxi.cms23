import {Component, OnInit} from '@angular/core';
import {ProductAddonTypeRepository} from 'src/app/shared/repositories/product-addon-type';
import {Router, ActivatedRoute} from '@angular/router';
import {AddOnType} from '../../../shared/entities/add-on-type';
import {LocalStorage} from 'ngx-webstorage';
import {CxiGridConfig} from '../../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../../shared/components/cxi-grid/cxi-grid-column';

@Component({
    selector: 'app-list-addon-type',
    templateUrl: './list-addon-type.component.html',
    styleUrls: ['./list-addon-type.component.scss']
})
export class ListAddonTypeComponent implements OnInit {
    // table
    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        pagingServer: true,
        repository: this.addonTypeRepository,
        translatable: true,
        actions: {
            onEdit: (addonType: AddOnType) => {
                return this.onEdit(addonType);
            },
            onDelete: (addonType: AddOnType) => {
                return this.onDelete(addonType);
            },
            onActivate: (addonType: AddOnType) => {
                this.onActivate(addonType);
            },
            onDeactivate: (addonType: AddOnType) => {
                this.onDeactivate(addonType);
            },
        }
    });
    cxiGridColumns = CxiGridColumn.from([
        {title: 'name', name: 'name'},
        {title: 'description', name: 'description'},
    ]);

    @LocalStorage('scope.organization_id') organizationId: number;
    @LocalStorage('scope.brand_id') brandId: number;
    @LocalStorage('scope.outlet_id') outletId: number;

    constructor(
        private addonTypeRepository: ProductAddonTypeRepository,
        private router: Router,
        public activatedRoute: ActivatedRoute,
    ) {
    }

    ngOnInit() {
    }

    onEdit(addonType: AddOnType) {
        this.router.navigate(['..', addonType.id, 'edit'], {relativeTo: this.activatedRoute});
    }

    onDelete(addonType: AddOnType) {
        this.addonTypeRepository.destroy(addonType).subscribe();
    }

    onActivate(addonType: AddOnType) {
        this.addonTypeRepository.activate(addonType).subscribe();
    }

    onDeactivate(addonType: AddOnType) {
        this.addonTypeRepository.deactivate(addonType).subscribe();
    }
}
