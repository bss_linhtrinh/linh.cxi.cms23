import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CreateAddonTypeComponent} from './create-addon-type/create-addon-type.component';
import {ListAddonTypeComponent} from './list-addon-type/list-addon-type.component';
import {EditAddonTypeComponent} from './edit-addon-type/edit-addon-type.component';
import {CheckPermissionsGuard} from '../../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: 'create', component: CreateAddonTypeComponent, data: {breadcrumb: 'Create new product add-on type'}},
            {path: 'list', component: ListAddonTypeComponent, data: {breadcrumb: 'List product add-on type'}},
            {path: ':id/edit', component: EditAddonTypeComponent, data: {breadcrumb: 'Edit product add-on type'}},
            {path: '', redirectTo: 'list', pathMatch: 'full'},
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProductAddonTypeRoutingModule {
}
