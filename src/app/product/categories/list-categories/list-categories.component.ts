import { Component, OnInit } from '@angular/core';
import { CategoriesRepository } from '../../../shared/repositories/categories.repository';
import { Category } from '../../../shared/entities/category';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorage } from 'ngx-webstorage';
import { CxiGridConfig } from '../../../shared/components/cxi-grid/cxi-grid-config';
import { CxiGridColumn } from '../../../shared/components/cxi-grid/cxi-grid-column';
import { ColumnFormat } from '../../../shared/types/column-format';

@Component({
    selector: 'app-list-categories',
    templateUrl: './list-categories.component.html',
    styleUrls: ['./list-categories.component.scss']
})
export class ListCategoriesComponent implements OnInit {

    public categories: Category[] = [];
    tab: string = 'en';
    // table
    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        pagingServer: true,
        repository: this.categoriesRepository,
        translatable: true,
        actions: {
            onEdit: (category: Category) => {
                return this.onEdit(category);
            },
            onDelete: (category: Category) => {
                return this.onDelete(category);
            },
            onActivate: (category: Category) => {
                this.onActivate(category);
            },
            onDeactivate: (category: Category) => {
                this.onDeactivate(category);
            },
        }
    });

    cxiGridColumns = CxiGridColumn.from([
        { title: 'name', name: 'name', avatar: 'images' },
        { title: 'category', name: 'children', fieldOption: 'name', format: ColumnFormat.Tag, sortable: false },
        { title: 'description', name: 'description' },
    ]);

    @LocalStorage('scope.organization_id') organizationId: number;
    @LocalStorage('scope.brand_id') brandId: number;
    @LocalStorage('scope.outlet_id') outletId: number;

    constructor(
        private categoriesRepository: CategoriesRepository,
        private router: Router,
        public activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit() {
    }

    onEdit(category: Category) {
        this.router.navigate(['..', category.id, 'edit'], { relativeTo: this.activatedRoute });
    }

    onDelete(category: Category) {
        this.categoriesRepository.destroy(category).subscribe();
    }

    onActivate(category: Category) {
        this.categoriesRepository.activate(category).subscribe();
    }

    onDeactivate(category: Category) {
        this.categoriesRepository.deactivate(category).subscribe();
    }
}
