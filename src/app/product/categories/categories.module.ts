import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CategoriesRoutingModule} from './categories-routing.module';
import {CreateCategoriesComponent} from './create-categories/create-categories.component';
import {ListCategoriesComponent} from './list-categories/list-categories.component';
import {EditCategoriesComponent} from './edit-categories/edit-categories.component';
import {SharedModule} from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [CreateCategoriesComponent, ListCategoriesComponent, EditCategoriesComponent],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        CategoriesRoutingModule,
    ]
})
export class CategoriesModule {
}
