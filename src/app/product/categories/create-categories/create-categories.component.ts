import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/shared/entities/category';
import { CategoriesRepository } from 'src/app/shared/repositories/categories.repository';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilitiesService } from 'src/app/shared/services/utilities/utilities.service';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { Status } from 'src/app/shared/entities/status';
import { environment } from 'src/environments/environment';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { UserScope } from 'src/app/shared/entities/user-scope';
declare const helper: any;

@Component({
  selector: 'app-create-categories',
  templateUrl: './create-categories.component.html',
  styleUrls: ['./create-categories.component.scss']
})
export class CreateCategoriesComponent implements OnInit {
  public category: Category;
  public categories: Category[] = [];
  public status: Status[] = Status.init();
  public isShow: boolean = false;
  public brand = [];
  tab: string = 'en';
  userScope: UserScope;
  selectedFile: File = null;
  currentUser: any;
  comingSoon: boolean;
  baseUrl: string = environment.baseUrl;
  constructor(
    public categoryReponsitory: CategoriesRepository,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public UtilitiesService: UtilitiesService,
    private authService: AuthService,
    private brandRepository: BrandsRepository,
    private scopeService: ScopeService,
  ) {
    this.currentUser = this.authService.currentUser();
  }

  ngOnInit() {
    this.category = new Category();
    this.getCategories();
    this.loadListBrand();
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.category.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  loadListBrand() {
    this.brandRepository.all()
      .subscribe(
        (res) => {
          this.brand = res;
          this.category.brand_id = this.brand[0].id;
          this.getListUserScope();
        }
      );
  }
  showCategory() {
    this.isShow = !this.isShow;
    if (this.isShow == false) {
      this.category.parent_id = 0;
    } else {
      this.category.parent_id = this.categories[0].id;
    }
  }
  onFileChanged(selectedFile: File) {
    //this.selectedFile = <File>event.target.files[0];
    this.selectedFile = selectedFile;
    //this.onUpload();
  }
  onUpload() {
    this.categoryReponsitory.uploadAvatar(this.category, this.selectedFile)
      .subscribe((res) => {
        this.category.image = res.data.path_string;
      },
        (error) => {
          console.log(error)
        });
  }
  getCategories() {
    this.categoryReponsitory.all({ pagination: 0 })
      .subscribe(
        (categories: Category[]) => {
          this.categories = categories;
        }
      );
  }
  onSubmit() {
    this.category.translations = null;
    // if (this.category.name != null) {
    //   this.category.slug = this.UtilitiesService.ChangeToSlug(this.category.name);
    // }
    this.categoryReponsitory.save(this.category).subscribe(
      (res) => {
        console.log(res);
        this.router.navigateByUrl('/categories');
        helper.showNotification(`${this.category.name} has been added successfully!!`, 'done', 'success');
      },
      (error) => {
        console.log(error);
      });
  }
}
