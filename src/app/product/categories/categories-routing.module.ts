import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CreateCategoriesComponent} from './create-categories/create-categories.component';
import {ListCategoriesComponent} from './list-categories/list-categories.component';
import {EditCategoriesComponent} from './edit-categories/edit-categories.component';
import {CheckPermissionsGuard} from '../../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: '', redirectTo: 'list', pathMatch: 'full'},
            {path: 'create', component: CreateCategoriesComponent, data: {breadcrumb: 'Create new category'}},
            {path: 'list', component: ListCategoriesComponent, data: {breadcrumb: 'List categories'}},
            {path: ':id/edit', component: EditCategoriesComponent, data: {breadcrumb: 'Edit category'}},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CategoriesRoutingModule {
}
