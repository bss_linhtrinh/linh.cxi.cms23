import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/shared/entities/category';
import { CategoriesRepository } from 'src/app/shared/repositories/categories.repository';
import { Router, ActivatedRoute } from '@angular/router';
import { Mode } from 'src/app/shared/entities/display_mode';
import { UtilitiesService } from 'src/app/shared/services/utilities/utilities.service';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { FilterStorageService } from 'src/app/shared/services/filterStorage/filterStorage.service';
import { Status } from 'src/app/shared/entities/status';
import { environment } from 'src/environments/environment';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
declare const helper: any;

@Component({
  selector: 'app-edit-categories',
  templateUrl: './edit-categories.component.html',
  styleUrls: ['./edit-categories.component.scss']
})
export class EditCategoriesComponent implements OnInit {
  public category: Category;
  public categories: Category[] = [];
  public mode: Mode[] = Mode.init();
  public status: Status[] = Status.init();
  public isShow: boolean = false;
  public brand = [];
  userScope: UserScope;
  currentUser: any;
  comingSoon: boolean;
  selectedFile: File = null;
  baseUrl: string = environment.baseUrl;
  tab: string = 'en';
  public translations = [
    {
      name: null,
      description: null,
      locale: 'en'
    },
    {
      name: null,
      description: null,
      locale: 'vi'
    }
  ];
  constructor(
    public categoryReponsitory: CategoriesRepository,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public UtilitiesService: UtilitiesService,
    private authService: AuthService,
    private filterStorage: FilterStorageService,
    private brandRepository: BrandsRepository,
    private scopeService: ScopeService,
  ) {
    this.currentUser = this.authService.currentUser();
  }

  ngOnInit() {
    this.category = new Category();
    this.loadEditCategories();
    this.getCategories();
    this.loadListBrand();
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.category.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  loadListBrand() {
    this.brandRepository.all()
      .subscribe(
        (res) => {
          this.brand = res;
          this.getListUserScope();
        }
      );
  }
  showCategory() {
    var vm = this;
    this.isShow = !this.isShow;
    if (this.isShow == false) {
      vm.category.parent_id = 0;
    } else {
      this.category.parent_id = this.categories[0].id;
    }
  }
  onFileChanged(selectedFile: File) {
    //this.selectedFile = <File>event.target.files[0];
    this.selectedFile = selectedFile;
    //this.onUpload();
  }
  onUpload() {
    this.categoryReponsitory.uploadAvatar(this.category, this.selectedFile)
      .subscribe((res) => {
        this.category.image = res.data.path_string;
      });
  }
  getCategories() {
    this.categoryReponsitory.all({ pagination: 0 })
      .subscribe(
        (categories: Category[]) => {
          this.categories = categories;
        }
      );
  }
  loadEditCategories() {
    this.activatedRoute.params.subscribe((data) => {
      let id = data.id;
      this.categoryReponsitory.find(id).subscribe((res) => {
        this.category = res;
        this.category.translations.forEach(item => {
          if (item.locale == 'en') {
            this.category.en = item;
          } else if (item.locale == 'vi') {
            this.category.vi = item;
          }
        });
        if (res.parent != null) {
          this.isShow = true;
          this.category.parent_id = res.parent.id;
        }
      })
    })
  }
  onSubmit() {
    // if (this.category.name != null) {
    //   this.category.slug = this.UtilitiesService.ChangeToSlug(this.category.name);
    // }
   // this.category.parent = null;
    this.category.translations = null;
    this.categoryReponsitory.save(this.category).subscribe((res) => {
      this.router.navigateByUrl('/categories');
      helper.showNotification(`${this.category.name} updated successfully!!`, 'done', 'success');
    })
  }
  Cancel() {
    this.router.navigate(['../..', 'list'], { relativeTo: this.activatedRoute });
  }
}
