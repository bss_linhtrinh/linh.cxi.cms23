import {Component, OnInit} from '@angular/core';
import {ProductAddonRepository} from 'src/app/shared/repositories/product-addon';
import {Router, ActivatedRoute} from '@angular/router';
import {AddOn} from '../../../shared/entities/add-on';
import {CxiGridConfig} from '../../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../../shared/components/cxi-grid/cxi-grid-column';
import {LocalStorage} from 'ngx-webstorage';

@Component({
    selector: 'app-list-product-addon',
    templateUrl: './list-product-addon.component.html',
    styleUrls: ['./list-product-addon.component.scss']
})
export class ListProductAddonComponent implements OnInit {

    cxiTableConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        pagingServer: true,
        repository: this.addonRepository,
        translatable: true,
        actions: {
            onEdit: (addon: AddOn) => {
                return this.onEdit(addon);
            },
            onDelete: (addon: AddOn) => {
                return this.onDelete(addon);
            },
            onActivate: (addon: AddOn) => {
                this.onActivate(addon);
            },
            onDeactivate: (addon: AddOn) => {
                this.onDeactivate(addon);
            },
        }
    });
    cxiTableColumns = CxiGridColumn.from([
        {title: 'name', name: 'name', avatar: 'images'},
        {title: 'description', name: 'description'},
    ]);

    @LocalStorage('scope.organization_id') organizationId: number;
    @LocalStorage('scope.brand_id') brandId: number;
    @LocalStorage('scope.outlet_id') outletId: number;

    constructor(
        private addonRepository: ProductAddonRepository,
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit() {
    }

    onEdit(addon: AddOn) {
        this.router.navigate(['..', addon.id, 'edit'], {relativeTo: this.activatedRoute});
    }

    onDelete(addon: AddOn) {
        this.addonRepository.destroy(addon).subscribe();
    }

    onActivate(addon: AddOn) {
        this.addonRepository.activate(addon).subscribe();
    }

    onDeactivate(addon: AddOn) {
        this.addonRepository.deactivate(addon).subscribe();
    }
}
