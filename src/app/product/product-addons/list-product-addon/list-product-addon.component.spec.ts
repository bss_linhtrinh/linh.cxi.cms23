import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListProductAddonComponent } from './list-product-addon.component';

describe('ListProductAddonComponent', () => {
  let component: ListProductAddonComponent;
  let fixture: ComponentFixture<ListProductAddonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListProductAddonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListProductAddonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
