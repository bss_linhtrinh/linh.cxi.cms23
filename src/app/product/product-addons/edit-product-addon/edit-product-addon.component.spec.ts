import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProductAddonComponent } from './edit-product-addon.component';

describe('EditProductAddonComponent', () => {
  let component: EditProductAddonComponent;
  let fixture: ComponentFixture<EditProductAddonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditProductAddonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProductAddonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
