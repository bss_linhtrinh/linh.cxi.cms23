import { Component, OnInit } from '@angular/core';
import { Active } from 'src/app/shared/entities/active';
import { ProductAddonTypeRepository } from 'src/app/shared/repositories/product-addon-type';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductAddonRepository } from 'src/app/shared/repositories/product-addon';
import { environment } from 'src/environments/environment';
import { AddOn } from '../../../shared/entities/add-on';
import { filter, map, switchMap } from 'rxjs/operators';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';

declare const helper: any;

@Component({
    selector: 'app-edit-product-addon',
    templateUrl: './edit-product-addon.component.html',
    styleUrls: ['./edit-product-addon.component.scss']
})
export class EditProductAddonComponent implements OnInit {

    public addOn: AddOn = new AddOn();
    public status: Active[] = Active.init();
    baseUrl: string = environment.baseUrl;
    selectedFile: File = null;
    userScope: UserScope;
    public brand = [];
    tab: string = 'en';
    constructor(
        private addonTypeRepository: ProductAddonTypeRepository,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private addonRepository: ProductAddonRepository,
        private scopeService: ScopeService,
        private brandRepository: BrandsRepository,
    ) {
    }

    ngOnInit() {
        this.loadEditAddon();
        this.loadListBrand();
    }
    getListUserScope() {
        this.scopeService.getUserScope$()
            .subscribe(
                (res: any) => {
                    this.userScope = res;
                    if (this.userScope.brand) {
                        this.addOn.brand_id = this.userScope.brand.id;
                    }
                }
            );
    }
    loadListBrand() {
        this.brandRepository.all()
            .subscribe(
                (res) => {
                    this.brand = res;
                    this.getListUserScope();
                }
            );
    }
    loadEditAddon() {
        this.activatedRoute.params
            .pipe(
                filter(data => data.id),
                map(data => data.id),
                switchMap(
                    (id) => this.addonRepository.find(id)
                )
            )
            .subscribe(
                (addOn: AddOn) => {
                    this.addOn = addOn;
                    this.addOn.translations.forEach(item => {
                        if (item.locale == 'en') {
                            this.addOn.en = item;
                        } else if (item.locale == 'vi') {
                            this.addOn.vi = item;
                        }
                    });
                }
            );
    }

    onFileChanged(event) {
        this.selectedFile = <File>event.target.files[0];
        this.onUpload();
    }

    onUpload() {
        this.addonRepository.uploadAvatar(this.addOn, this.selectedFile)
            .subscribe((res) => {
                this.addOn.image = res.data.path_string;
            });
    }

    onSubmit() {
        this.addOn.translations = null;
        this.addonRepository.save(this.addOn)
            .subscribe((res) => {
                this.cancel();
                helper.showNotification(`${this.addOn.name} updated successfully!!`, 'done', 'success');
            });
    }

    cancel() {
        this.router.navigate(['../..', 'list'], { relativeTo: this.activatedRoute });
    }
}
