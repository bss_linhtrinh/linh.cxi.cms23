import { Component, OnInit } from '@angular/core';
import { Active } from 'src/app/shared/entities/active';
import { ProductAddonTypeRepository } from 'src/app/shared/repositories/product-addon-type';
import { ProductAddonRepository } from 'src/app/shared/repositories/product-addon';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { AddOn } from '../../../shared/entities/add-on';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';

declare const helper: any;

@Component({
    selector: 'app-create-product-addon',
    templateUrl: './create-product-addon.component.html',
    styleUrls: ['./create-product-addon.component.scss']
})
export class CreateProductAddonComponent implements OnInit {

    public addOn: AddOn = new AddOn();
    public status: Active[] = Active.init();
    selectedFile: File = null;
    baseUrl: string = environment.baseUrl;
    userScope: UserScope;
    public brand = [];
    tab: string = 'en';
    constructor(
        private addonRepository: ProductAddonRepository,
        private router: Router,
        private route: ActivatedRoute,
        private scopeService: ScopeService,
        private brandRepository: BrandsRepository,
    ) {
    }

    ngOnInit() {
        this.loadListBrand();
    }
    getListUserScope() {
        this.scopeService.getUserScope$()
            .subscribe(
                (res: any) => {
                    this.userScope = res;
                    if (this.userScope.brand) {
                        this.addOn.brand_id = this.userScope.brand.id;
                    }
                }
            );
    }
    loadListBrand() {
        this.brandRepository.all()
            .subscribe(
                (res) => {
                    this.brand = res;
                    this.addOn.brand_id = this.brand[0].id;
                    this.getListUserScope();
                }
            );
    }
    onFileChanged(event) {
        this.selectedFile = <File>event.target.files[0];
        this.onUpload();
    }

    onUpload() {
        this.addonRepository.uploadAvatar(this.addOn, this.selectedFile)
            .subscribe((res) => {
                this.addOn.image = res.data.path_string;
            });
    }

    onSubmit() {
        this.addOn.translations = null;
        this.addonRepository.save(this.addOn)
            .subscribe(
                () => {
                    this.router.navigate(['../list'], { relativeTo: this.route });
                    helper.showNotification(`${this.addOn.name} has been added successfully!!`, 'done', 'success');
                }
            );
    }
}
