import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateProductAddonComponent } from './create-product-addon.component';

describe('CreateProductAddonComponent', () => {
  let component: CreateProductAddonComponent;
  let fixture: ComponentFixture<CreateProductAddonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateProductAddonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateProductAddonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
