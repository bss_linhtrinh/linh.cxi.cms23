import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductAddonsRoutingModule } from './product-addons-routing.module';
import { CreateProductAddonComponent } from './create-product-addon/create-product-addon.component';
import { EditProductAddonComponent } from './edit-product-addon/edit-product-addon.component';
import { ListProductAddonComponent } from './list-product-addon/list-product-addon.component';
import {SharedModule} from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [CreateProductAddonComponent, EditProductAddonComponent, ListProductAddonComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ProductAddonsRoutingModule,
  ]
})
export class ProductAddonsModule { }
