import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CreateProductAddonComponent} from './create-product-addon/create-product-addon.component';
import {ListProductAddonComponent} from './list-product-addon/list-product-addon.component';
import {EditProductAddonComponent} from './edit-product-addon/edit-product-addon.component';
import {CheckPermissionsGuard} from '../../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: '', redirectTo: 'list', pathMatch: 'full'},
            {path: 'create', component: CreateProductAddonComponent, data: {breadcrumb: 'Create new product add-on'}},
            {path: 'list', component: ListProductAddonComponent, data: {breadcrumb: 'List product add-ons'}},
            {path: ':id/edit', component: EditProductAddonComponent, data: {breadcrumb: 'Edit product add-on'}},
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProductAddonsRoutingModule {
}
