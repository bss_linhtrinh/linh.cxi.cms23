import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectSuperAttributesComponent } from './select-super-attributes.component';

describe('SelectSuperAttributesComponent', () => {
  let component: SelectSuperAttributesComponent;
  let fixture: ComponentFixture<SelectSuperAttributesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSuperAttributesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSuperAttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
