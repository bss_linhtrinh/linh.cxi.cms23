import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Attribute} from '../../../shared/entities/attribute';

@Component({
    selector: 'app-select-super-attributes',
    templateUrl: './select-super-attributes.component.html',
    styleUrls: ['./select-super-attributes.component.scss']
})
export class SelectSuperAttributesComponent implements OnInit {
    superAttributes: number[] = [];

    @Input() attributes: Attribute[];

    constructor(private ngbActiveModal: NgbActiveModal) {
    }

    ngOnInit() {
    }

    done() {
        this.ngbActiveModal.close(this.superAttributes);
    }

    cancel() {
        this.ngbActiveModal.dismiss('cancel');
    }
}
