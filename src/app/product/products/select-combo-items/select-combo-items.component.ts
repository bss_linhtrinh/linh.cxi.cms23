import {Component, EventEmitter, Inject, Input, OnChanges, OnInit, Optional, Output, SimpleChanges, ViewChild} from '@angular/core';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ProductComboItem} from '../../../shared/entities/product-combo-item';
import {Product} from '../../../shared/entities/product';

@Component({
    selector: 'cxi-select-combo-items',
    templateUrl: './select-combo-items.component.html',
    styleUrls: ['./select-combo-items.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectComboItemsComponent, multi: true}
    ]
})
export class SelectComboItemsComponent extends ElementBase<ProductComboItem[]> implements OnInit, OnChanges {
    @Input() name: string;
    @Input() products: any[];

    @Output() add = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.products) {
            if (changes.products.currentValue !== changes.products.previousValue) {
                this.checkDisabled();
            }
        }
    }

    checkDisabled() {
        if (!this.products || !this.value) {
            return;
        }

        this.products.forEach(product => {
            product.disabled = this.value.findIndex(comboItem => comboItem.product_id === product.id) !== -1;
        });
    }

    checkEmpty() {
        if (!this.value || this.value.length === 0) {
            this.addProductComboItem();
        }
    }

    addProductComboItem() {
        if (this.value && this.value instanceof Array) {
            this.value.push(new ProductComboItem());
        } else {
            this.value = [new ProductComboItem()];
        }
    }

    onChangeProduct(product: Product) {
        this.checkDisabled();
    }

    onRemove(index: number) {
        if (this.value && this.value.length > 0) {
            this.value.splice(index, 1);
        }

        this.checkEmpty();

        this.checkDisabled();
    }
}
