import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectComboItemsComponent } from './select-combo-items.component';

describe('SelectComboItemsComponent', () => {
  let component: SelectComboItemsComponent;
  let fixture: ComponentFixture<SelectComboItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectComboItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectComboItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
