import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ItemListComponent} from './item-list/item-list.component';
import {CreateProductComponent} from './create-product/create-product.component';
import {EditProductComponent} from './edit-product/edit-product.component';
import {CheckPermissionsGuard} from '../../shared/guards/check-permissions/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: '', redirectTo: 'list', pathMatch: 'full'},
            {path: 'list', component: ItemListComponent, data: {breadcrumb: 'List products'}},
            {path: 'create', component: CreateProductComponent, data: {breadcrumb: 'Create new product'}},
            {path: ':id/edit', component: EditProductComponent, data: {breadcrumb: 'Edit product'}},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProductsRoutingModule {
}
