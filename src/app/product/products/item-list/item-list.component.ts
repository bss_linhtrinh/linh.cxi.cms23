import {Component, OnInit} from '@angular/core';
import {Product} from 'src/app/shared/entities/product';
import {ProductsRepository} from 'src/app/shared/repositories/products.repository';
import {Router, ActivatedRoute} from '@angular/router';
import {AuthService} from 'src/app/shared/services/auth/auth.service';

import {User} from 'src/app/shared/entities/user';
import {CxiGridConfig} from '../../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../../shared/components/cxi-grid/cxi-grid-column';
import {LocalStorage} from 'ngx-webstorage';

@Component({
    selector: 'app-item-list',
    templateUrl: './item-list.component.html',
    styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {
    products: Product[];
    currentUser: User;

    // table
    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        pagingServer: true,
        translatable: true,
        repository: this.productsRepository,
        actions: {
            onEdit: (product: Product) => {
                this.onEdit(product);
            },
            onActivate: (product: Product) => {
                this.onActivate(product);
            },
            onDeactivate: (product: Product) => {
                this.onDeactivate(product);
            },
            onDelete: (product: Product) => {
                this.onDelete(product);
            },
            onStandOut: (product: Product) => {
                this.onStandOut(product);
            },
        }
    });
    cxiGridColumns = CxiGridColumn.from([
        {title: 'name', name: 'name', avatar: 'images', secondary_name: 'sku'},
        {title: 'description', name: 'description', format: 'rich-text'},
        {title: 'price', name: 'price', format: 'monetary'},
    ]);

    @LocalStorage('scope.organization_id') organizationId: number;
    @LocalStorage('scope.brand_id') brandId: number;
    @LocalStorage('scope.outlet_id') outletId: number;

    constructor(
        private productsRepository: ProductsRepository,
        private router: Router,
        private route: ActivatedRoute,
        private authService: AuthService
    ) {
    }

    ngOnInit() {
        this.currentUser = this.authService.currentUser();
    }

    onEdit(product: Product) {
        this.router.navigate(['..', product.id, 'edit'], {relativeTo: this.route});
    }

    onActivate(product: Product) {
        this.productsRepository.activate(product)
            .subscribe();
    }

    onDeactivate(product: Product) {
        this.productsRepository.deactivate(product)
            .subscribe();
    }

    onDelete(product: Product) {
        this.productsRepository.destroy(product)
            .subscribe();
    }

    onStandOut(product: Product) {
        this.productsRepository.standOut(product)
            .subscribe();
    }
}
