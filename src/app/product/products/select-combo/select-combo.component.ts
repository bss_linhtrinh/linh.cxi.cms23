import {Component, Inject, Input, OnInit, Optional, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {ProductCombo} from '../../../shared/entities/product-combo';
import {Product} from '../../../shared/entities/product';
import {AddOnType} from '../../../shared/entities/add-on-type';
import {AddOn} from '../../../shared/entities/add-on';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'cxi-select-combo',
    templateUrl: './select-combo.component.html',
    styleUrls: ['./select-combo.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectComboComponent, multi: true}
    ]
})
export class SelectComboComponent extends ElementBase<ProductCombo[]> implements OnInit {
    @Input() name: string;
    @Input() products: Product[];
    @Input() addOnTypes: AddOnType[];
    @Input() addOns: AddOn[];
    @Input() priceType: string;

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private translateService: TranslateService
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    checkEmpty() {
        if (!this.value || this.value.length === 0) {
            this.addProductCombo();
        }
    }

    addProductCombo() {
        if (!this.value) {
            this.value = [];
        }

        this.value.push(new ProductCombo());
    }

    onRemoveComboItem(index: number) {
        if (this.value && this.value.length > 0) {
            this.value.splice(index, 1);
        }

        this.checkEmpty();
    }

    // getTranslatedTitle$(index) {
    //     return this.translateService.get(`Product.Product {{index}}`, {index: index + 1});
    // }
}
