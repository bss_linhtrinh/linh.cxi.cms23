import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectComboItemComponent } from './select-combo-item.component';

describe('SelectComboItemComponent', () => {
  let component: SelectComboItemComponent;
  let fixture: ComponentFixture<SelectComboItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectComboItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectComboItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
