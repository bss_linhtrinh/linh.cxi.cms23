import {Component, EventEmitter, Inject, Input, OnDestroy, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {Product} from '../../../shared/entities/product';
import {ProductsRepository} from '../../../shared/repositories/products.repository';
import {ProductComboItem} from '../../../shared/entities/product-combo-item';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {ProductVariant} from '../../../shared/entities/product-variant';
import {of, Subscription} from 'rxjs';
import {delay, map, retryWhen, tap} from 'rxjs/operators';
import {ProductType} from '../../../shared/types/product-type';

@Component({
    selector: 'cxi-select-combo-item',
    templateUrl: './select-combo-item.component.html',
    styleUrls: ['./select-combo-item.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectComboItemComponent, multi: true}
    ]
})
export class SelectComboItemComponent extends ElementBase<ProductComboItem> implements OnInit, OnDestroy {
    @Input() name: string;
    @Input() products: any[];

    @Output() add = new EventEmitter();
    @Output() remove = new EventEmitter();
    @Output() changeProduct = new EventEmitter<Product>();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    selectedProduct: Product;
    selectedVariant: ProductVariant;
    addOnTypes: any[];
    subscriptions: Subscription[] = [];

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private productsRepository: ProductsRepository,
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
        this.doParseValue();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    doParseValue() {
        let count = 0;
        const source = of(null);
        const example = source.pipe(
            map(() => {
                if (!this.value || !this.value.product_id) {
                    throw 1;
                }
                return 1;
            }),
            retryWhen(errors =>
                errors.pipe(
                    // restart in 1 seconds
                    delay(500)
                )
            ),
            delay(100),
        );
        const doValueSub = example.subscribe(
            () => this.findProduct(this.value.product_id)
        );
        this.subscriptions.push(doValueSub);
    }

    onChangeProduct(productId: number) {
        if (productId) {
            this.findProduct(productId);
        }
    }

    findProduct(productId: number) {
        this.productsRepository.find(productId)
            .subscribe(
                (product: Product) => {

                    // check variant
                    if (!product) {
                        return;
                    }
                    this.selectedProduct = product;
                    this.value.type = product.type;


                    switch (this.selectedProduct.type) {
                        case ProductType.Simple:
                        default:
                            if (this.value.child) {
                                this.value.child = null;
                            }
                            break;
                        case ProductType.Configurable:
                            if (this.selectedProduct.variants &&
                                this.selectedProduct.variants.length > 0) {
                                if (this.value.child) {
                                    const variant = this.selectedProduct.variants.find(v => v.id === this.value.child.product_id);
                                    if (!variant) {
                                        this.value.child = new ProductComboItem();
                                    } else {
                                        this.selectedVariant = variant;
                                    }
                                } else {
                                    this.value.child = new ProductComboItem();
                                }
                            }
                            break;
                        case ProductType.Combo:
                            break;
                    }

                    // get Add-on types
                    this.getAddOnTypes();

                    // emit
                    this.changeProduct.emit(this.selectedProduct);
                }
            );
    }

    onChangeVariant(variantId: number) {
        if (!variantId) {
            return;
        }
        const variant = this.selectedProduct.variants.find(v => v.id === variantId);
        if (variant) {
            this.selectedVariant = variant;
        }

        // get Add-on types
        this.getAddOnTypes();
    }

    getAddOnTypes() {
        if (this.selectedVariant) {
            this.addOnTypes = this.selectedVariant.addon_types;
        } else if (this.selectedProduct) {
            this.addOnTypes = this.selectedProduct.addon_types;
        } else {
            this.addOnTypes = [];
        }
    }

    removeComboItem() {
        this.remove.emit();
    }
}
