import {AfterViewChecked, Component, Inject, Input, OnInit, Optional, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';

@Component({
    selector: 'cxi-select-variant-attributes',
    templateUrl: './select-variant-attributes.component.html',
    styleUrls: ['./select-variant-attributes.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectVariantAttributesComponent, multi: true}
    ]
})
export class SelectVariantAttributesComponent extends ElementBase<any[]> implements OnInit, AfterViewChecked {
    @Input() name: string;
    @Input() attributes: any[];
    @Input() superAttributes: number[];
    @Input() disabled: boolean;

    @ViewChild(NgModel, {static: true}) model: NgModel;

    selectedAttributes: any[];

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private utilities: UtilitiesService
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    ngAfterViewChecked(): void {
        this.getSelectedAttributes();
    }

    // super attributes
    getSelectedAttributes() {
        this.selectedAttributes = this.utilities.cloneDeep(this.attributes)
            .filter(
                item => this.superAttributes.includes(item.id)
            );
    }
}
