import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectVariantAttributesComponent } from './select-variant-attributes.component';

describe('SelectVariantAttributesComponent', () => {
  let component: SelectVariantAttributesComponent;
  let fixture: ComponentFixture<SelectVariantAttributesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectVariantAttributesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectVariantAttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
