import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectVariantAddonsComponent } from './select-variant-addons.component';

describe('SelectVariantAddonsComponent', () => {
  let component: SelectVariantAddonsComponent;
  let fixture: ComponentFixture<SelectVariantAddonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectVariantAddonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectVariantAddonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
