import {Component, Inject, Input, OnChanges, OnInit, Optional, SimpleChanges, ViewChild} from '@angular/core';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ProductAddOnTypeAddOn} from '../../../shared/entities/product-add-on-type-add-on';

@Component({
    selector: 'cxi-select-variant-addons',
    templateUrl: './select-variant-addons.component.html',
    styleUrls: ['./select-variant-addons.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectVariantAddonsComponent, multi: true}
    ]
})
export class SelectVariantAddonsComponent extends ElementBase<ProductAddOnTypeAddOn[]> implements OnInit, OnChanges {
    @Input() name: string;
    @Input() addOns: any[];
    @Input() applySamePrice: boolean;
    @Input() samePrice: number;

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {

    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.addOns) {
            if (changes.addOns.currentValue !== changes.addOns.previousValue) {
                this.checkDisabled();
            }
        }

    }

    // add-ons
    addAddOn() {
        this.value.push(new ProductAddOnTypeAddOn());
    }

    onRemoveAddOn(index: number) {
        this.value.splice(index, 1);

        // check disabled
        this.checkDisabled();

        // check empty
        this.checkEmpty();
    }

    onChangeVariantAddOn() {
        this.checkDisabled();
    }

    checkEmpty() {
        if (!this.value || this.value.length === 0) {
            this.addAddOn();
        }
    }

    checkDisabled() {
        if (!this.addOns || !this.value) {
            return;
        }

        this.addOns.forEach(addOn => {
            addOn.disabled = this.value.findIndex(v => v.id === addOn.id) !== -1;
        });
    }

    isDisabledAddNew() {
        return this.addOns && this.value && this.addOns.length === this.value.length;
    }
}
