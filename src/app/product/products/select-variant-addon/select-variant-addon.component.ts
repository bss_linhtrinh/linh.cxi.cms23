import {Component, EventEmitter, Inject, Input, OnChanges, OnInit, Optional, Output, SimpleChanges, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {ProductAddOnTypeAddOn} from '../../../shared/entities/product-add-on-type-add-on';

@Component({
    selector: 'cxi-select-variant-addon',
    templateUrl: './select-variant-addon.component.html',
    styleUrls: ['./select-variant-addon.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectVariantAddonComponent, multi: true}
    ]
})
export class SelectVariantAddonComponent extends ElementBase<ProductAddOnTypeAddOn> implements OnInit, OnChanges {

    @Input() name: string;
    @Input() addOns: any[];
    @Input() applySamePrice: boolean;
    @Input() samePrice: number;

    @Output() remove = new EventEmitter();
    @Output() change = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges): void {

    }

    removeAddOn() {
        this.remove.emit();
    }

    onChangeVariantAddon(addOnId: number) {
        const addOn = this.addOns.find(a => a.id === addOnId);
        this.change.emit(addOn);
    }
}
