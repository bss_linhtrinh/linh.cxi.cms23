import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectVariantAddonComponent } from './select-variant-addon.component';

describe('SelectVariantAddonComponent', () => {
  let component: SelectVariantAddonComponent;
  let fixture: ComponentFixture<SelectVariantAddonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectVariantAddonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectVariantAddonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
