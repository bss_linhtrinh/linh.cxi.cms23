import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectApplicationScopeComponent } from './select-application-scope.component';

describe('SelectApplicationScopeComponent', () => {
  let component: SelectApplicationScopeComponent;
  let fixture: ComponentFixture<SelectApplicationScopeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectApplicationScopeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectApplicationScopeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
