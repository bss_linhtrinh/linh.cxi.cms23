import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {map, switchMap, tap, toArray} from 'rxjs/operators';
import {Brand} from '../../../shared/entities/brand';
import {Outlet} from '../../../shared/entities/outlets';
import {ScopeService} from '../../../shared/services/scope/scope.service';
import {BrandsRepository} from '../../../shared/repositories/brands.repository';
import {OutletsRepository} from '../../../shared/repositories/outlets.repository';
import {TranslateService} from '@ngx-translate/core';
import {fromArray} from 'rxjs/internal/observable/fromArray';
import {LocalStorage} from 'ngx-webstorage';
import {Scope} from '../../../shared/types/scopes';
import {timer} from 'rxjs';
import {ProductOutletQuantity} from '../../../shared/entities/product-outlet-quantity';

@Component({
    selector: 'cxi-select-application-scope',
    templateUrl: './select-application-scope.component.html',
    styleUrls: ['./select-application-scope.component.scss']
})

export class SelectApplicationScopeComponent implements OnInit, OnChanges {
    @Input() name: string;

    @Input() applicationScope: string;
    @Input() brandId: number;
    @Input() outletsQuantities: ProductOutletQuantity[];

    @Output() applicationScopeChange = new EventEmitter();
    @Output() brandIdChange = new EventEmitter();
    @Output() outletsQuantitiesChange = new EventEmitter();

    brands: Brand[];
    outlets: Outlet[];
    productApplicationScopes = [
        {name: 'All', value: 'All'},
        {name: 'Specific outlet', value: 'Specific outlet'}
    ];

    isHideApplicationScope = false;
    isHideSelectBrand = false;

    @LocalStorage('scope.organization_id') scopeOrganizationId: number;
    @LocalStorage('scope.brand_id') scopeBrandId: number;
    @LocalStorage('scope.outlet_id') scopeOutletId: number;
    @LocalStorage('scope.current') scopeCurrent: Scope;

    constructor(
        private scopeService: ScopeService,
        private brandsRepository: BrandsRepository,
        private outletsRepository: OutletsRepository,
        private translateService: TranslateService
    ) {
        // translate productApplicationScopes
        fromArray(this.productApplicationScopes)
            .pipe(
                switchMap(
                    (option) => {
                        return this.translateService.get('scope.' + option.name)
                            .pipe(
                                map(
                                    (translatedName) => {
                                        return {name: translatedName, value: option.value};
                                    }
                                )
                            );
                    }
                ),
                toArray()
            )
            .subscribe(
                (productApplicationScopes: any[]) => this.productApplicationScopes = productApplicationScopes
            );
    }

    ngOnInit() {
        timer(100).pipe(tap(() => this.getBrands())).subscribe();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.brandId) {
            if (changes.brandId.currentValue) {
                this.getOutletsByBrand(+changes.brandId.currentValue);
            }
        }
    }

    getBrands() {
        switch (this.scopeCurrent) {
            case Scope.Organization:
                this.brandsRepository.all()
                    .subscribe(
                        (brands: Brand[]) => this.brands = brands
                    );
                break;

            case Scope.Brand:
                this.isHideSelectBrand = true;
                if (!this.brandId && this.scopeBrandId) {
                    this.brandId = this.scopeBrandId;
                    this.brandIdChange.emit(this.brandId);
                }
                this.getOutletsByBrand(this.brandId);
                break;

            case Scope.Outlet:
                this.isHideApplicationScope = true;
                if (!this.brandId && this.scopeBrandId) {
                    this.brandId = this.scopeBrandId;
                    this.brandIdChange.emit(this.brandId);
                }
                if ((!this.outletsQuantities || this.outletsQuantities.length === 0) && this.scopeOutletId) {
                    const outletQuantity = ProductOutletQuantity.create({outlet_id: this.scopeOutletId, quantity: null});
                    this.outletsQuantities = [outletQuantity];
                    this.outletsQuantitiesChange.emit(this.outletsQuantities);
                }
                break;
        }
    }

    getOutletsByBrand(brandId?: number) {
        return this.outletsRepository.getOutletsByBrand(brandId)
            .subscribe(
                (outlets: Outlet[]) => this.outlets = outlets
            );
    }

    onChangeBrand(brandId: number) {
        this.brandIdChange.emit(this.brandId);
        this.getOutletsByBrand(brandId);
    }

    onChangeIsAllStores(allStores: string) {
        this.applicationScopeChange.emit(this.applicationScope);
        if (allStores === 'All') {
            this.outletsQuantities = [];
        }
    }

    onOutletsChange() {
        this.outletsQuantitiesChange.emit(this.outletsQuantities);
    }
}
