import {
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    SimpleChanges
} from '@angular/core';
import * as _ from 'lodash';
import {ProductOutletQuantity} from '../../../shared/entities/product-outlet-quantity';

@Component({
    selector: 'app-select-outlets-quantity',
    templateUrl: './select-outlets-quantity.component.html',
    styleUrls: ['./select-outlets-quantity.component.scss']
})
export class SelectOutletsQuantityComponent implements OnInit, OnChanges, OnDestroy {
    @Input() name: string;
    @Input() label: string;
    @Input() outlets: any;
    @Input() disabled: boolean;
    @Input() clearable = false;
    @Input() searchable = false;
    @Input() public type: string;
    @Input() public placeholder: string;

    private _required: any;

    @Input()
    get required(): any {
        return this._required;
    }

    set required(value: any) {
        this._required = typeof value !== 'undefined';
    }

    private _outletsQuantities: ProductOutletQuantity[];
    @Input()
    get outletsQuantities(): ProductOutletQuantity[] {
        return this._outletsQuantities;
    }

    @Output() outletsQuantitiesChange = new EventEmitter<ProductOutletQuantity[]>();

    set outletsQuantities(outletsQuantities: ProductOutletQuantity[]) {
        if (this._outletsQuantities !== outletsQuantities) {
            this._outletsQuantities = outletsQuantities;
            this.outletsQuantitiesChange.emit(this.outletsQuantities);
        }
    }

    @Output() change = new EventEmitter();

    isAddNew = true;
    newOutletId: number;
    newQuantity: number;

    isDisabledAddNew: boolean;

    constructor() {
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes) {
            if (changes.outlets &&
                changes.outlets.currentValue !== changes.outlets.previousValue &&
                changes.outlets.currentValue) {
                this.checkRemoveOutletIdsIfNotExists();
                this.checkIsDisabledAddNew();
            }
            if (changes.outletsQuantities &&
                changes.outletsQuantities.currentValue !== changes.outletsQuantities.previousValue &&
                changes.outletsQuantities.currentValue) {
                this.checkIsDisabledAddNew();
            }
        }
    }

    ngOnDestroy(): void {
    }

    onChangeNewOutletId(outletId: number) {
        if (!outletId) {
            return;
        }

        if (!this.outletsQuantities) {
            this.outletsQuantities = [];
        }

        if (this.outletsQuantities.findIndex(outletQuantity => outletQuantity.outlet_id === outletId) === -1) {
            const outletQuantity = ProductOutletQuantity.create({outlet_id: outletId, quantity: 1});
            this.outletsQuantities.push(outletQuantity);
        }

        this.isAddNew = false;
        this.newOutletId = null;
        this.newQuantity = null;

        this.checkDisabled();
        this.checkIsDisabledAddNew();

        this.change.emit(this.outletsQuantities);
    }

    onChangeQuantity(quantity: any) {
        if (!quantity) {
            return;
        }

        this.change.emit(this.outletsQuantities);
    }

    checkRemoveOutletIdsIfNotExists() {
        // remove if not exist in new list.
        if (this.outletsQuantities) {
            _.remove(this.outletsQuantities, (outletQuantity: ProductOutletQuantity) => {
                return !(this.outlets.findIndex(outlet => outlet.id === outletQuantity.outlet_id) !== -1);
            });
            this.checkEmpty();
        }
        this.checkDisabled();
    }

    checkDisabled() {
        if (!this.outletsQuantities) {
            return;
        }

        this.outlets.forEach(outlet => {
            outlet.disabled = (this.outletsQuantities.findIndex(outletQuantity => outletQuantity.outlet_id === outlet.id) !== -1);
        });
    }

    checkEmpty() {
        if (this.outletsQuantities.length === 0) {
            this.addNew();
        }
    }

    addNew() {
        this.isAddNew = true;
    }

    removeOutletQuantity(index: number) {
        if (!this.outletsQuantities) {
            return;
        }

        this.outletsQuantities.splice(index, 1);

        this.checkEmpty();
        this.checkDisabled();
        this.checkIsDisabledAddNew();

        this.change.emit(this.outletsQuantities);
    }

    checkIsDisabledAddNew() {
        this.isDisabledAddNew = (
            this.outletsQuantities &&
            this.outlets &&
            this.outletsQuantities.length === this.outlets.length
        );
    }
}
