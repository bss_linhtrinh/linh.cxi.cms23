import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectOutletsQuantityComponent } from './select-outlets-quantity.component';

describe('SelectOutletsQuantityComponent', () => {
  let component: SelectOutletsQuantityComponent;
  let fixture: ComponentFixture<SelectOutletsQuantityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectOutletsQuantityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectOutletsQuantityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
