import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectVariantAddonTypeComponent } from './select-variant-addon-type.component';

describe('SelectVariantAddonTypeComponent', () => {
  let component: SelectVariantAddonTypeComponent;
  let fixture: ComponentFixture<SelectVariantAddonTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectVariantAddonTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectVariantAddonTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
