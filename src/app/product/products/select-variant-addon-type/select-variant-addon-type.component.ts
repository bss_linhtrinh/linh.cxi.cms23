import {Component, EventEmitter, Inject, Input, OnChanges, OnInit, Optional, Output, SimpleChanges, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import {ProductAddOnType} from '../../../shared/entities/product-add-on-type';
import {DISPLAY_TYPES} from '../../../shared/constants/display-types';

@Component({
    selector: 'cxi-select-variant-addon-type',
    templateUrl: './select-variant-addon-type.component.html',
    styleUrls: ['./select-variant-addon-type.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectVariantAddonTypeComponent, multi: true}
    ]
})
export class SelectVariantAddonTypeComponent extends ElementBase<ProductAddOnType> implements OnInit, OnChanges {
    @Input() name: string;
    @Input() addOnTypes: any[];
    @Input() addOns: any[];

    @Output() remove = new EventEmitter();
    @Output() change = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    clonedAddOns: any[];
    productDisplayTypes = DISPLAY_TYPES;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private utilities: UtilitiesService
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
        this.clonedAddOns = this.utilities.cloneDeep(this.addOns);
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.addOns) {
            if (changes.addOns.currentValue !== changes.addOns.previousValue) {
                this.clonedAddOns = this.utilities.cloneDeep(this.addOns);
            }
        }
    }

    onChangeApplySamePriceForAll() {
        if (this.value.apply_same_price && this.value.price) {
            this.value.addons.forEach(addOn => addOn.price = this.value.price);
        }
    }

    removeItem() {
        this.remove.emit();
    }

    onChangeVariantAddOnType() {
        this.change.emit();
    }
}
