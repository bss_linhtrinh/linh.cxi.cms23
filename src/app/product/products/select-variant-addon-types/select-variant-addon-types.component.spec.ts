import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectVariantAddonTypesComponent } from './select-variant-addon-types.component';

describe('SelectVariantAddonTypesComponent', () => {
  let component: SelectVariantAddonTypesComponent;
  let fixture: ComponentFixture<SelectVariantAddonTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectVariantAddonTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectVariantAddonTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
