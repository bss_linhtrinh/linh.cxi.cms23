import {Component, Inject, Input, OnInit, Optional, ViewChild} from '@angular/core';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ProductAddOnType} from '../../../shared/entities/product-add-on-type';

@Component({
    selector: 'cxi-select-variant-addon-types',
    templateUrl: './select-variant-addon-types.component.html',
    styleUrls: ['./select-variant-addon-types.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectVariantAddonTypesComponent, multi: true}
    ]
})
export class SelectVariantAddonTypesComponent extends ElementBase<ProductAddOnType[]> implements OnInit {
    @Input() name: string;
    @Input() addOnTypes: any[];
    @Input() addOns: any[];

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }


    // add-on types

    addAddOnType() {
        this.value.push(new ProductAddOnType());
    }

    onRemove(index: number) {
        this.value.splice(index, 1);

        // check empty
        this.checkEmpty();

        // check disabled
        this.checkDisabled();
    }

    onChangeVariantAddOnType() {
        // check disabled
        this.checkDisabled();
    }

    checkEmpty() {
        if (!this.value || this.value.length === 0) {
            this.addAddOnType();
        }
    }

    checkDisabled() {
        this.addOnTypes.forEach(addOnType => {
            addOnType.disabled = this.value.findIndex(v => v.id === addOnType.id) !== -1;
        });
    }

    isDisabledAddNew() {
        return this.addOnTypes && this.value && this.addOnTypes.length === this.value.length;
    }
}
