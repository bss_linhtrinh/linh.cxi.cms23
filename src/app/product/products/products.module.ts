import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProductsRoutingModule} from './products-routing.module';
import {ItemListComponent} from './item-list/item-list.component';
import {CreateProductComponent} from './create-product/create-product.component';
import {EditProductComponent} from './edit-product/edit-product.component';
import {SharedModule} from 'src/app/shared/shared.module';
import {FormsModule} from '@angular/forms';

import {SelectVariantComponent} from './select-variant/select-variant.component';
import {SelectVariantsComponent} from './select-variants/select-variants.component';
import {SelectVariantAttributesComponent} from './select-variant-attributes/select-variant-attributes.component';
import {SelectVariantAddonTypesComponent} from './select-variant-addon-types/select-variant-addon-types.component';
import {SelectVariantAddonTypeComponent} from './select-variant-addon-type/select-variant-addon-type.component';
import {SelectVariantAddonsComponent} from './select-variant-addons/select-variant-addons.component';
import {SelectVariantAddonComponent} from './select-variant-addon/select-variant-addon.component';
import {SelectSuperAttributesComponent} from './select-super-attributes/select-super-attributes.component';
import {SelectComboComponent} from './select-combo/select-combo.component';
import {SelectComboItemComponent} from './select-combo-item/select-combo-item.component';
import {SelectComboItemsComponent} from './select-combo-items/select-combo-items.component';
import {SelectApplicationScopeComponent} from './select-application-scope/select-application-scope.component';
import { SelectComboProductComponent } from './select-combo-product/select-combo-product.component';
import { UploadFilesComponent } from './upload-files/upload-files.component';
import { SelectOutletsQuantityComponent } from './select-outlets-quantity/select-outlets-quantity.component';
import {NgSelectModule} from '@ng-select/ng-select';

@NgModule({
    declarations: [
        ItemListComponent,
        CreateProductComponent,
        EditProductComponent,
        SelectVariantComponent,
        SelectVariantsComponent,
        SelectVariantAttributesComponent,
        SelectVariantAddonTypesComponent,
        SelectVariantAddonTypeComponent,
        SelectVariantAddonsComponent,
        SelectVariantAddonComponent,
        SelectSuperAttributesComponent,
        SelectComboComponent,
        SelectComboItemComponent,
        SelectComboItemsComponent,
        SelectApplicationScopeComponent,
        SelectComboProductComponent,
        UploadFilesComponent,
        SelectOutletsQuantityComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        NgSelectModule,
        SharedModule,
        ProductsRoutingModule,
    ],
    entryComponents: [
        SelectSuperAttributesComponent
    ]
})
export class ProductsModule {
}
