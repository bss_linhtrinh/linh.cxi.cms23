import {
    AfterViewChecked,
    Component,
    EventEmitter,
    Inject,
    Input,
    OnChanges,
    OnInit,
    Optional,
    Output,
    SimpleChanges,
    ViewChild
} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {ProductVariant} from '../../../shared/entities/product-variant';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import {ProductVariantAttribute} from '../../../shared/entities/product-variant-attribute';
import {AddOn} from '../../../shared/entities/add-on';
import {AddOnType} from '../../../shared/entities/add-on-type';
import {Attribute} from '../../../shared/entities/attribute';

@Component({
    selector: 'cxi-select-variant',
    templateUrl: './select-variant.component.html',
    styleUrls: ['./select-variant.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectVariantComponent, multi: true}
    ]
})
export class SelectVariantComponent extends ElementBase<ProductVariant> implements OnInit, OnChanges, AfterViewChecked {
    @Input() name: string;
    @Input() attributes: any[];
    @Input() addOnTypes: AddOnType[];
    @Input() addOns: AddOn[];
    @Input() superAttributes: number[];
    @Input() isEdit: boolean;

    @Output() add = new EventEmitter();
    @Output() remove = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    clonedAddOnTypes: AddOnType[];
    clonedAttributes: Attribute[];

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private utilities: UtilitiesService
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
        this.clonedAddOnTypes = this.utilities.cloneDeep(this.addOnTypes);
        this.clonedAttributes = this.utilities.cloneDeep(this.attributes);
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.addOnTypes) {
            if (changes.addOnTypes.currentValue !== changes.addOnTypes.previousValue) {
                this.clonedAddOnTypes = this.utilities.cloneDeep(this.addOnTypes);
            }
        }

        if (changes.attributes) {
            if (changes.attributes.currentValue !== changes.attributes.previousValue) {
                this.clonedAttributes = this.utilities.cloneDeep(this.attributes);
            }
        }
    }

    ngAfterViewChecked(): void {
        this.getSelectedAttributes();
    }

    addVariant() {
        this.add.emit();
    }

    removeVariant() {
        this.remove.emit();
    }


    // super attributes
    getSelectedAttributes() {
        if (this.value) {
            this.utilities.cloneDeep(this.attributes)
                .filter(
                    item => this.superAttributes.includes(item.id)
                )
                .forEach(attr => {
                    if (this.value.attributes.findIndex(item => item.id === attr.id) === -1) {
                        const newVariantAttribute = ProductVariantAttribute.create({
                            id: attr.id,
                            name: attr.name,
                            options: attr.options,
                            value: null
                        });
                        this.value.attributes.push(newVariantAttribute);
                    }
                });
        }
    }

    // isEdit & disabled edit attributes
    isDisabledAttributes() {
        return this.isEdit && this.value.id;
    }
}
