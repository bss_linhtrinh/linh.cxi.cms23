import {Component, OnInit} from '@angular/core';
import {Product} from 'src/app/shared/entities/product';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductsRepository} from 'src/app/shared/repositories/products.repository';
import {MessagesService} from '../../../messages/messages.service';
import {AttributesRepository} from '../../../shared/repositories/attributes.repository';
import {Attribute} from '../../../shared/entities/attribute';
import {CategoriesRepository} from '../../../shared/repositories/categories.repository';
import {Category} from '../../../shared/entities/category';
import {Filter} from 'src/app/shared/entities/filter';
import {ProductAddonRepository} from '../../../shared/repositories/product-addon';
import {ProductAddonTypeRepository} from '../../../shared/repositories/product-addon-type';
import {ScopeService} from '../../../shared/services/scope/scope.service';
import {AddOn} from '../../../shared/entities/add-on';
import {AddOnType} from '../../../shared/entities/add-on-type';
import {PRODUCT_TYPES} from '../../../shared/constants/product-types';
import {NotificationService} from '../../../shared/services/noitification/notification.service';
import {ProductVariant} from '../../../shared/entities/product-variant';
import {ProductVariantAttribute} from '../../../shared/entities/product-variant-attribute';
import {PRICE_TYPES} from '../../../shared/constants/price-types';
import {ProductType} from '../../../shared/types/product-type';
import {ProductCombo} from '../../../shared/entities/product-combo';
import {ProductComboItem} from '../../../shared/entities/product-combo-item';
import {tap} from 'rxjs/operators';
import {SessionStorage} from 'ngx-webstorage';
import {Language} from '../../../shared/entities/language';
import {StorageService} from '../../../shared/services/storage/storage.service';

@Component({
    selector: 'app-create-product',
    templateUrl: './create-product.component.html',
    styleUrls: ['./create-product.component.scss']
})
export class CreateProductComponent implements OnInit {
    product: Product;
    filter: Filter;
    selectedFile: File = null;

    products: Product[] = [];
    attributes: Attribute[] = [];
    categories: Category[] = [];
    addOns: AddOn[] = [];
    addOnTypes: AddOnType[] = [];

    productTypes = PRODUCT_TYPES;
    priceTypes = PRICE_TYPES;

    tab: any;

    @SessionStorage('languages') languages: Language[];

    schemaProduct: any;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private productsRepository: ProductsRepository,
        private attributesRepository: AttributesRepository,
        private categoriesRepository: CategoriesRepository,
        private messagesService: MessagesService,
        private storageService: StorageService,
        private productAddonRepository: ProductAddonRepository,
        private productAddonTypeRepository: ProductAddonTypeRepository,
        private scopeService: ScopeService,
        private notificationService: NotificationService,
    ) {
        const systemConfig = this.storageService.retrieve('system.config');
        this.schemaProduct = systemConfig && systemConfig.schemas && systemConfig.schemas.product
            ? systemConfig.schemas.product
            : null;
    }

    ngOnInit() {
        this.initProduct();
        this.getData();
    }

    initProduct() {
        this.product = new Product();
    }

    getData() {
        this.getProducts();
        this.getAttributes();
        this.getCategories();
        this.getProductAddOns();
        this.getProductAddOnTypes();
    }

    getProducts() {
        this.productsRepository.all()
            .subscribe(
                (products: Product[]) => {
                    this.products = products;
                }
            );
    }

    getCategories() {
        const params = {status: 1, pagination: 0};
        this.categoriesRepository
            .allAsTree(params)
            .subscribe(
                (categories: Category[]) => {
                    this.categories = categories;
                }
            );
    }

    reloadCategoriesByLanguage(lang: string) {
        this.categoriesRepository
            .allByLanguage(lang)
            .subscribe(
                (categories: Category[]) => {
                    this.categories = categories;
                }
            );
    }

    getAttributes() {
        this.attributesRepository
            .all({})
            .subscribe((attributes: Attribute[]) => {
                this.attributes = attributes;
            });
    }

    getProductAddOns() {
        this.productAddonRepository.all()
            .subscribe(
                (addOns: AddOn[]) => this.addOns = addOns
            );
    }

    getProductAddOnTypes() {
        this.productAddonTypeRepository.all()
            .subscribe(
                (addOnTypes: AddOnType[]) => this.addOnTypes = addOnTypes
            );
    }

    cancel() {
        this.back();
    }

    back() {
        this.router.navigate(['..', 'list'], {relativeTo: this.route});
    }

    onFileChanged(selectedFile: File) {
        this.selectedFile = selectedFile;
    }

    checkErrorBeforeSubmit() {
        const messages: string[] = [];

        if (this.product) {

            switch (this.product.type) {
                case ProductType.Simple:
                default:
                    break;

                case ProductType.Configurable:
                    if (!this.product.variants ||
                        this.product.variants.length === 0) {
                        messages.push(`products_configurable_must_have_at_least_one_variant`);
                    } else {
                        // first variant is empty
                        const firstVariant = this.product.variants[0];
                        if (
                            !firstVariant.name ||
                            !firstVariant.price ||
                            !firstVariant.attributes ||
                            firstVariant.attributes.length === 0
                        ) {
                            firstVariant.attributes.forEach(
                                (variantItemAttribute: ProductVariantAttribute) => {
                                    if (
                                        !variantItemAttribute.id ||
                                        !variantItemAttribute.value
                                    ) {
                                        messages.push(`products_configurable_must_have_the_first_variant_not_empty`);
                                    }
                                }
                            );
                        }

                        // check exist empty variant
                        let isEmptyIndex = -1;
                        for (const index in this.product.variants) {
                            const variantItem = this.product.variants[index];
                            if (
                                !variantItem.name ||
                                !variantItem.price ||
                                !variantItem.attributes ||
                                variantItem.attributes.length === 0
                            ) {
                                isEmptyIndex = parseInt(index, 10);
                            } else {
                                variantItem.attributes.forEach(
                                    (variantItemAttribute: ProductVariantAttribute) => {
                                        if (
                                            !variantItemAttribute.id ||
                                            !variantItemAttribute.value
                                        ) {
                                            isEmptyIndex = parseInt(index, 10);
                                        }
                                    }
                                );
                            }
                        }
                        if (isEmptyIndex !== -1) {
                            messages.push(`products_configurable_must_have_all_variants_not_empty`);
                        }


                        // duplicate variant name
                        for (let i = 0; i < this.product.variants.length - 1; i++) {
                            const item1 = this.product.variants[i];
                            for (let j = i + 1; j < this.product.variants.length; j++) {
                                const item2 = this.product.variants[j];
                                if (item1.name === item2.name) {
                                    messages.push(`Duplicate variant name ([${i}]:${item1.name} - [${j}]:${item2.name}).`);
                                }
                            }
                        }

                        // duplicate variant attribute-option
                        let variantsAttributeValue: string[];
                        variantsAttributeValue = this.product.variants.map(
                            (variant: ProductVariant) => {
                                return variant.attributes
                                    .sort( // sort attribute by attribute_id
                                        (a: ProductVariantAttribute, b: ProductVariantAttribute) => parseFloat(a.id) - parseFloat(b.id)
                                    )
                                    .map( // map to string `id:value`
                                        (productVariantAttribute: ProductVariantAttribute): string => {
                                            return `${productVariantAttribute.id}:${productVariantAttribute.value}`;
                                        }
                                    )
                                    .join('|'); // join array `id:value` to `id1:value1|id2:value2|...`
                            }
                        );
                        for (let k = 0; k < variantsAttributeValue.length - 1; k++) {
                            const variantAttribute1 = variantsAttributeValue[k];
                            for (let l = k + 1; l < variantsAttributeValue.length; l++) {
                                const variantAttribute2 = variantsAttributeValue[l];
                                if (variantAttribute1 === variantAttribute2) {
                                    messages.push(`Duplicate variant attribute-value ([${k}] - [${l}]).`);
                                }
                            }
                        }
                    }
                    break;

                // combos
                case ProductType.Combo:
                    if (!this.product.combos ||
                        !this.product.combos.length) {
                        messages.push(`products_combo_must_have_at_least_one_combination`);
                    } else {
                        // empty allowed items
                        this.product.combos.forEach(
                            (combo: ProductCombo) => {
                                if (!combo.items ||
                                    !combo.items.length ||
                                    !combo.items[0].product_id) {
                                    messages.push(`product_combo_at_least_one_item`);
                                }
                                combo.items.forEach(
                                    (comboItem: ProductComboItem) => {
                                        switch (comboItem.type) {
                                            case ProductType.Simple:
                                                break;
                                            case ProductType.Configurable:
                                                if (!comboItem.child ||
                                                    !comboItem.child.product_id) {
                                                    messages.push(`product_type_configurable_must_be_selected_variant`);
                                                }
                                                break;
                                            case ProductType.Combo:
                                                messages.push(`product_type_combo_cannot_selected_product_type_combo`);
                                                break;
                                        }
                                    }
                                );
                            }
                        );
                    }
                    break;
            }
        }

        const valid = !(messages && messages.length > 0);
        if (!valid) {
            this.notificationService.error(messages);
        }

        return valid;
    }

    onSubmit() {
        if (!this.checkErrorBeforeSubmit()) {
            return;
        }

        this.productsRepository.save(this.product)
            .pipe(
                tap(
                    () => this.back()
                )
            )
            .subscribe(
            );
    }

    onTabChange(lang: string) {
        this.reloadCategoriesByLanguage(lang);
    }
}
