import {Component, EventEmitter, Inject, Input, OnInit, Optional, Output, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {ProductVariant} from '../../../shared/entities/product-variant';
import {Attribute} from '../../../shared/entities/attribute';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {from, of} from 'rxjs';
import {SelectSuperAttributesComponent} from '../select-super-attributes/select-super-attributes.component';
import {catchError} from 'rxjs/operators';
import {AddOn} from '../../../shared/entities/add-on';
import {AddOnType} from '../../../shared/entities/add-on-type';

@Component({
    selector: 'cxi-select-variants',
    templateUrl: './select-variants.component.html',
    styleUrls: ['./select-variants.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectVariantsComponent, multi: true}
    ]
})
export class SelectVariantsComponent extends ElementBase<ProductVariant[]> implements OnInit {
    @Input() attributes: Attribute[];
    @Input() addOnTypes: AddOnType[];
    @Input() addOns: AddOn[];
    @Input() superAttributes: number[];
    @Input() isEdit: boolean;

    @Output() add = new EventEmitter();
    @Output() superAttributesChange = new EventEmitter<number[]>();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private ngbModal: NgbModal
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onAdd(index: number) {
        if (!this.value) {
            this.value = [];
        }

        const newVariant = new ProductVariant();
        this.value.splice(index + 1, 0, newVariant);
    }

    onRemove(index: number) {
        if (this.value && this.value.length > 0) {
            this.value.splice(index, 1);
        }
    }

    addVariant() {
        if (!this.superAttributes || this.superAttributes.length === 0) {
            const modalRef = this.ngbModal.open(SelectSuperAttributesComponent, {windowClass: 'select-super-attributes'});
            modalRef.componentInstance.superAttributes = this.superAttributes;
            modalRef.componentInstance.attributes = this.attributes;
            from(modalRef.result)
                .pipe(
                    catchError(() => of())
                )
                .subscribe(
                    (superAttributes: number[]) => {
                        if (superAttributes) {
                            this.superAttributes = superAttributes;
                            this.superAttributesChange.emit(superAttributes);
                        }

                        this.handleAddVariant();
                    },
                );
        } else {
            this.handleAddVariant();
        }
    }

    handleAddVariant() {
        if (!this.superAttributes) {
            return;
        }

        if (!this.value) {
            this.value = [];
        }

        const newVariant = new ProductVariant();
        this.value.push(newVariant);
    }
}
