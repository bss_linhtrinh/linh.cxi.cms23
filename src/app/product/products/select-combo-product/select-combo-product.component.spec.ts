import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectComboProductComponent } from './select-combo-product.component';

describe('SelectComboProductComponent', () => {
  let component: SelectComboProductComponent;
  let fixture: ComponentFixture<SelectComboProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectComboProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectComboProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
