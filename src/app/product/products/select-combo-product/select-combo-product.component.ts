import {Component, EventEmitter, Inject, Input, OnChanges, OnInit, Optional, Output, SimpleChanges, ViewChild} from '@angular/core';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';
import {ElementBase} from '../../../shared/custom-form/element-base/element-base';
import {ProductCombo} from '../../../shared/entities/product-combo';
import {PRODUCT_UNITS} from '../../../shared/constants/product-units';
import {Product} from '../../../shared/entities/product';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';

@Component({
    selector: 'app-select-combo-product',
    templateUrl: './select-combo-product.component.html',
    styleUrls: ['./select-combo-product.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: SelectComboProductComponent, multi: true}
    ]
})
export class SelectComboProductComponent extends ElementBase<ProductCombo> implements OnInit, OnChanges {

    @Input() name: string;
    @Input() index: number;
    @Input() products: Product[];
    @Input() priceType: string;

    @Output() remove = new EventEmitter();

    @ViewChild(NgModel, {static: true}) model: NgModel;

    productUnits = PRODUCT_UNITS;
    clonedProducts: Product[];
    translateParam: any;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
        private utilities: UtilitiesService
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
        this.clonedProducts = this.utilities.cloneDeep(this.products);
        this.translateParam = {index: this.index + 1};
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.products) {
            if (changes.products.currentValue !== changes.products.previousValue) {
                this.clonedProducts = this.utilities.cloneDeep(this.products);
            }
        }
    }

    removeComboItem() {
        this.remove.emit();
    }

    isFlexiblePrice() {
        return this.priceType === 'flexible';
    }
}
