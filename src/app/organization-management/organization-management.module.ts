import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OrganizationManagementRoutingModule} from './organization-management-routing.module';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';

@NgModule({
    // declarations: [OptionsComponent],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        OrganizationManagementRoutingModule
    ]
})
export class OrganizationManagementModule {
}
