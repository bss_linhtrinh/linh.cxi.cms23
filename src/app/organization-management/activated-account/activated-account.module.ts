import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActivatedAccountRoutingModule } from './activated-account-routing.module';
import { ActivatedComponent } from './activated/activated.component';
import {SharedModule} from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ActivatedComponent],
  imports: [
    CommonModule,
    ActivatedAccountRoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class ActivatedAccountModule { }
