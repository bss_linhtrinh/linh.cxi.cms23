import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CheckPermissionsGuard } from 'src/app/shared/guards/check-permissions/check-permissions.guard';
import { ActivatedComponent } from './activated/activated.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [CheckPermissionsGuard],
    component: ActivatedComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivatedAccountRoutingModule { }
