import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [

    {
        path: 'activated-account',
        loadChildren: () => import('./activated-account/activated-account.module').then(mod => mod.ActivatedAccountModule),
        data: { breadcrumb: 'Activated account' }
    },

    // settings
    {
        path: 'settings',
        loadChildren: () => import('./settings/settings.module').then(mod => mod.SettingsModule),
        data: { breadcrumb: 'Setting' }
    },
    // reports
    {
        path: 'reports',
        loadChildren: () => import('./reports/reports.module').then(mod => mod.ReportsModule),
        data: { breadcrumb: 'Reports' }
    },

    // options
    {
        path: 'options',
        loadChildren: () => import('./option/option.module').then(mod => mod.OptionModule),
        data: { breadcrumb: 'Options' }
    },
    {
        path: 'activity-log',
        loadChildren: () => import('./activity-log/activity-log.module').then(mod => mod.ActivityLogModule),
        data: { breadcrumb: 'Activity Logs' }
    },

    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OrganizationManagementRoutingModule {
}
