import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashManagmentFunctionalityComponent } from './cash-managment-functionality.component';

describe('CashManagmentFunctionalityComponent', () => {
  let component: CashManagmentFunctionalityComponent;
  let fixture: ComponentFixture<CashManagmentFunctionalityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashManagmentFunctionalityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashManagmentFunctionalityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
