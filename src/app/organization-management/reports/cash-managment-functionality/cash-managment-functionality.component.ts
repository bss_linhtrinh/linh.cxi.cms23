import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cash-managment-functionality',
  templateUrl: './cash-managment-functionality.component.html',
  styleUrls: ['./cash-managment-functionality.component.scss']
})
export class CashManagmentFunctionalityComponent implements OnInit {

  //tab
  stepy = ['manage_cash', 'safe_drop','safe_count','bank_deposit'];
  stepy_info = {
    manage_cash: 'Manage Cash Office',
    safe_drop: 'Safe Drop Report',
    safe_count: 'Safe Count Report',
    bank_deposit: 'Bank Deposit Report',
  };
  tab = "manage_cash";

  constructor() { }

  ngOnInit() {
  }
  change(tab) {
    var vm = this;
    switch (tab) {
      case 'manage_cash': {
        vm.tab = tab;
        break;
      }
      case 'safe_drop': {
        vm.tab = tab;
        break;
      }
      case 'safe_count': {
        vm.tab = tab;
        break;
      }
      case 'bank_deposit': {
        vm.tab = tab;
        break;
      }
    }
  }

}
