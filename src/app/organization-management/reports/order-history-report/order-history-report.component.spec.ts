import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderHistoryReportComponent } from './order-history-report.component';

describe('OrderHistoryReportComponent', () => {
  let component: OrderHistoryReportComponent;
  let fixture: ComponentFixture<OrderHistoryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderHistoryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderHistoryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
