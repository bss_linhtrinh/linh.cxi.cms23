import { Component, OnInit } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { orderHistoryReportRepository } from 'src/app/shared/repositories/order-history-report.repository';

@Component({
  selector: 'app-order-history-report',
  templateUrl: './order-history-report.component.html',
  styleUrls: ['./order-history-report.component.scss']
})
export class OrderHistoryReportComponent implements OnInit {
  @LocalStorage('dashboard.timeSelected') timeSelected: string;
  @LocalStorage('dashboard.dateStart') dateStart: string;
  @LocalStorage('dashboard.dateEnd') dateEnd: string;

  // pagination
  @LocalStorage('pagination.current_page') current_page: number = 1;
  @LocalStorage('pagination.per_page') per_page: number = 5;
  @LocalStorage('pagination.total') total: number;

  public orderHistory = [];
  constructor(
    private orderHistoryReportRepository: orderHistoryReportRepository,
  ) { }

  ngOnInit() {
    this.getListOrderHistory();
  }

  getListOrderHistory() {
    let sen_data = {
      date_start: this.dateStart,
      date_end: this.dateEnd,
      page: this.current_page,
      per_page: this.per_page
    }
    this.orderHistoryReportRepository.all(sen_data)
      .subscribe(
        (res: any) => {
          this.orderHistory = res;
          this.orderHistory.forEach(_o => {
            _o.isShow = true;
          });
        },
        (error) => {
          console.log(error);
        }
      );
  }
  showOrder(_item: any) {
    _item.isShow = !_item.isShow;
  }

  onChangePagination() {
    this.getListOrderHistory();
  }

  openDate() {
    this.getListOrderHistory();
  }

}
