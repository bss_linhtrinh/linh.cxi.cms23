import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceReportingComponent } from './invoice-reporting.component';

describe('InvoiceReportingComponent', () => {
  let component: InvoiceReportingComponent;
  let fixture: ComponentFixture<InvoiceReportingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceReportingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceReportingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
