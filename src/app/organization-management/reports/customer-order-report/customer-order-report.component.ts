import { Component, OnInit } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { customerOrdersReportRepository } from 'src/app/shared/repositories/customer-orders-report.repository';
import { UtilitiesService } from 'src/app/shared/services/utilities/utilities.service';


@Component({
  selector: 'app-customer-order-report',
  templateUrl: './customer-order-report.component.html',
  styleUrls: ['./customer-order-report.component.scss']
})
export class CustomerOrderReportComponent implements OnInit {
  @LocalStorage('dashboard.timeSelected') timeSelected: string;
  @LocalStorage('dashboard.dateStart') dateStart: string;
  @LocalStorage('dashboard.dateEnd') dateEnd: string;

  // pagination
  @LocalStorage('pagination.current_page') current_page: number = 1;
  @LocalStorage('pagination.per_page') per_page: number;
  @LocalStorage('pagination.total') total: number;

  // table
  public customerOrderReport: any = [];

  constructor(
    private customerOrdersReportRepository: customerOrdersReportRepository,
    private utilitiesService: UtilitiesService
  ) { }

  ngOnInit() {
    this.getListCustomerOrder();
    if (!this.dateEnd) {
      this.dateEnd = this.utilitiesService.parseDate(new Date());
    }
    if (!this.dateStart) {
      const dateStart = this.dateEnd ? new Date(this.dateEnd) : new Date();
      dateStart.setMonth(dateStart.getMonth() - 3);
      this.dateStart = this.utilitiesService.parseDate(dateStart);
    }
  }
  getListCustomerOrder() {
    let sen_data = {
      date_start: this.dateStart,
      date_end: this.dateEnd,
      page: this.current_page,
      per_page: this.per_page
    }
    this.customerOrdersReportRepository.all(sen_data)
      .subscribe(
        (res: any) => {
          this.customerOrderReport = res;
          this.customerOrderReport.forEach(_o => {
            _o.isShow = true;
          });
        },
        (error) => {
          console.log(error);
        }
      );
  }
  showOrder(_item: any) {
    _item.isShow = !_item.isShow;
  }
  openDate() {
    this.getListCustomerOrder();
  }
}
