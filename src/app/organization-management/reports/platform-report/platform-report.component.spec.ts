import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatformReportComponent } from './platform-report.component';

describe('PlatformReportComponent', () => {
  let component: PlatformReportComponent;
  let fixture: ComponentFixture<PlatformReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatformReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatformReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
