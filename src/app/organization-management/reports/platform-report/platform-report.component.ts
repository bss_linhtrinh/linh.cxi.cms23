import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartDataSets } from 'chart.js';

import { Label } from 'ng2-charts';
import { LocalStorage } from 'ngx-webstorage';
import { platformReportRepository } from 'src/app/shared/repositories/platform-report.repository';
import { UtilitiesService } from 'src/app/shared/services/utilities/utilities.service';
import { platformOverviewReportRepository } from 'src/app/shared/repositories/platform-overview-report';

declare const helper: any;

@Component({
  selector: 'app-platform-report',
  templateUrl: './platform-report.component.html',
  styleUrls: ['./platform-report.component.scss']
})
export class PlatformReportComponent implements OnInit {

  //tab
  stepy = ['dashboard', 'web_cms', 'app_user', 'app_pos'];
  stepy_info = {
    dashboard: 'Dashboard',
    web_cms: 'Website Platform',
    app_user: 'User App Platform',
    app_pos: 'Post App Platform',
  };
  tab = "dashboard";

  //chart
  chartOptions: ChartOptions = {
    responsive: true,
    responsiveAnimationDuration: 500,
    maintainAspectRatio: false,
    legend: {
      display: true,
      position: 'top',
      // align: 'start', waiting for chart.js 2.9.0
      fullWidth: false,

      labels: {
        boxWidth: 4,
        fontColor: '#16171a',
        fontSize: 14,
        fontFamily: 'sfprotextregular',

        padding: 30,

        usePointStyle: true
      }
    },
    elements: {
      point: {
        radius: 3,
        pointStyle: 'circle',
        borderWidth: 3
      }
    }
  };
  chartLabels: Label[] = [''];
  chartData: ChartDataSets[] = [
    { data: [], label: 'app_menu_digital' },
    { data: [], label: 'app_pos' },
    { data: [], label: 'app_user' },
    { data: [], label: 'web_cms' },
  ];
  chartColors = [
    { // blue-500,
      borderColor: '#0097ff',
      backgroundColor: '#0097ff',
    },
    { // purple-500,
      borderColor: '#6737e1',
      backgroundColor: '#6737e1',
    },
    { // green-500,
      borderColor: '#00cc00',
      backgroundColor: '#00cc00',
    },
    { // green-500,
      borderColor: '#15a5de',
      backgroundColor: '#15a5de',
    }
  ];
  chartLegend = true;
  chartType = 'horizontalBar';
  //
  public platforms: any = [];
  isShow: boolean = false;
  @LocalStorage('dashboard.timeSelected') timeSelected: string;
  @LocalStorage('dashboard.dateStart') dateStart: string;
  @LocalStorage('dashboard.dateEnd') dateEnd: string;

  // pagination
  @LocalStorage('pagination.current_page') current_page: number = 1;
  @LocalStorage('pagination.per_page') per_page: number;
  @LocalStorage('pagination.total') total: number;

  constructor(
    private platformReportRepository: platformReportRepository,
    private platformOverviewReportRepository: platformOverviewReportRepository,
    private utilitiesService: UtilitiesService
  ) { }

  ngOnInit() {
    this.getListPlatformOverview();
    if (!this.dateEnd) {
      this.dateEnd = this.utilitiesService.parseDate(new Date());
    }
    if (!this.dateStart) {
      const dateStart = this.dateEnd ? new Date(this.dateEnd) : new Date();
      dateStart.setMonth(dateStart.getMonth() - 3);
      this.dateStart = this.utilitiesService.parseDate(dateStart);
    }
  }
  change(tab) {
    var vm = this;
    switch (tab) {
      case 'dashboard': {
        vm.tab = tab;
        vm.current_page = 1;
        this.getListPlatformOverview();
        break;
      }
      case 'web_cms': {
        vm.tab = tab;
        vm.current_page = 1;
        this.getListPlatform();
        break;
      }
      case 'app_user': {
        vm.tab = tab;
        vm.current_page = 1;
        this.getListPlatform();
        break;
      }
      case 'app_pos': {
        vm.tab = tab;
        vm.current_page = 1;
        this.getListPlatform();
        break;
      }
    }
  }

  getListPlatform() {

    let sen_data = {
      date_start: this.dateStart,
      date_end: this.dateEnd,
      platform: this.tab,
      page: this.current_page,
      per_page: this.per_page
    }
    this.platformReportRepository.all(sen_data)
      .subscribe(
        (res: any) => {
          this.platforms = res;
          this.platforms.forEach(_o => {
            _o.isShow = true;
          });
        },
        (error) => {
          console.log(error);
        }
      );
  }

  showOrder(_item: any) {
    _item.isShow = !_item.isShow;
  }

  onChangePagination() {
    this.getListPlatform();
  }

  openDate() {

    if (this.tab == 'dashboard') {
      this.getListPlatformOverview();
    } else {
      this.getListPlatform();
    }
  }
  getListPlatformOverview() {
    let sen_data = {
      date_start: this.dateStart,
      date_end: this.dateEnd,
    }
    this.platformOverviewReportRepository.all(sen_data)
      .subscribe(
        (res: any) => {
          if (res && res.length > 0) {
            res.forEach(_item => {
              this.chartData.forEach(_item1 => {
                if (_item.platform == _item1.label) {
                  _item1.data.push(_item.total);
                }
              });
            });
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }
}
