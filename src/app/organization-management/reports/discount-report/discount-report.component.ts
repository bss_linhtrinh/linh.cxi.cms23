import { Component, OnInit } from '@angular/core';
import { discountReportRepository } from 'src/app/shared/repositories/discount-report.repository';
import { LocalStorage, SessionStorage } from 'ngx-webstorage';

@Component({
  selector: 'app-discount-report',
  templateUrl: './discount-report.component.html',
  styleUrls: ['./discount-report.component.scss']
})
export class DiscountReportComponent implements OnInit {
  @LocalStorage('dashboard.timeSelected') timeSelected: string;
  @LocalStorage('dashboard.dateStart') dateStart: string;
  @LocalStorage('dashboard.dateEnd') dateEnd: string;

  // pagination
  @LocalStorage('pagination.current_page') current_page: number = 1;
  @LocalStorage('pagination.per_page') per_page: number;
  @LocalStorage('pagination.total') total: number;

  // table
  public discounts: any = [];
  baseTableConfig = {
    paging: true,
    sorting: true,
    filtering: false,
    className: false,
  };
  baseTableColumns = [
    { title: 'discount_id', name: 'discount_id', className: 'discount-id' },
    { title: 'description', name: 'description', className: 'description' },
    { title: 'qty', name: 'qty', className: 'qty' },
  ];

  constructor(
    private discountReportRepository: discountReportRepository,
  ) { }

  ngOnInit() {
    this.getListDiscountReport();
  }
  getListDiscountReport() {
    let sen_data = {
      date_start: this.dateStart,
      date_end: this.dateEnd,
      page: this.current_page,
      per_page: this.per_page
    }
    this.discountReportRepository.all(sen_data)
      .subscribe(
        (res: any) => {
          this.discounts = res;
          this.discounts.forEach(_o => {
            _o.isShow = true;
          });
        },
        (error) => {
          console.log(error);
        }
      );
  }
  showOrder(_item: any) {
    _item.isShow = !_item.isShow;
  }
  openDate() {
    this.getListDiscountReport();
  }

}
