import { Component, OnInit } from '@angular/core';
import { deliveryReportRepository } from 'src/app/shared/repositories/delivery-report.repository';
import {LocalStorage} from 'ngx-webstorage';

declare var jQuery: any;
declare const helper: any;

@Component({
  selector: 'app-delivery-report',
  templateUrl: './delivery-report.component.html',
  styleUrls: ['./delivery-report.component.scss']
})
export class DeliveryReportComponent implements OnInit {

  public orderCanceled = [];
  public orderCompleted = [];
  //tab
  stepy = ['completed', 'canceled'];
  stepy_info = {
    completed: 'Completed',
    canceled: 'Canceled',
  };
  tab = "completed";
  //
  list_post = {
    current_page: 1,
    last_page: 0,
    total: 0,
    per_page: 0
  }
  //
  baseRadioValues = ['All (Default)', 'Yvonne Thompson', 'Harinder Mondi', 'Huo Shu', 'Otmar Doležal', 'Kondo Ieyasu', 'Noell Blue', 'Morganne Flaherty', 'Mar Rueda'];
  baseRadioValues1 = ['All (Default)', 'POS 1', 'POS 2', 'POS 3', 'POS 4', 'POS 5', 'POS 6', 'POS 7', 'POS 8', 'POS 9'];
  baseRadioValues2 = ['All (Default)', 'Item Deleted', 'Login Attempt', 'Tax Group Change', 'System Setting OptionChange', 'POS Station Settings Change', 'Peripherals Device SettingsChange'];
  baseRadio: string;
  baseRadio1: string;
  baseRadio2: string;
  //pagination
  @LocalStorage('pagination.current_page') current_page: number = 1;
  @LocalStorage('pagination.per_page') per_page: number = 5;
  @LocalStorage('pagination.total') total: number;

  constructor(
    private deliveryReportRepository: deliveryReportRepository
  ) { }

  ngOnInit() {
    this.filterReport();
    this.getListCompletedOrders();
  }
  change(tab) {
    var vm = this;
    switch (tab) {
      case 'completed': {
        vm.tab = tab;
        vm.current_page = 1;
        this.getListCompletedOrders();
        break;
      }
      case 'canceled': {
        vm.tab = tab;
        vm.current_page = 1;
        this.getListCanceledOrders();
        break;
      }
    }
  }
  filterReport() {
    (function ($) {
      $(document).ready(function () {
        $('body, html').on('click', function (event) {
          var target = $(event.target);
          if (!target.is('.filters , .filters *, .filters-report, .filters-report *, .export-btn, .export-btn *, .export, .export *')) {
            $('.filters').removeClass('active');
            $('.export').removeClass('active');
          }
        });
        $('.filters-report').click(function () {
          $('.filters').toggleClass('active');
          $('.export').removeClass('active');
        })
        $('.export-btn').click(function () {
          $('.export').toggleClass('active');
          $('.filters').removeClass('active');
        })
      });
    })(jQuery);
  }
  getListCanceledOrders() {
    this.deliveryReportRepository.all({ status: 'Canceled', page: this.current_page, per_page: this.per_page })
      .subscribe(
        (res: any) => {
          this.orderCanceled = res;
        },
        (error) => {
          helper.showNotification('Failed !!!', 'error', 'danger', 900);
        }
      );
  }
  getListCompletedOrders() {
    this.deliveryReportRepository.all({ status: 'ordered', page: this.current_page, per_page: this.per_page })
      .subscribe(
        (res: any) => {
          this.orderCompleted = res;
        },
        (error) => {
          helper.showNotification('Failed !!!', 'error', 'danger', 900);
        }
      );
  }
  onChangePagination() {
    if (this.tab = "canceled") {
      this.getListCanceledOrders();
    } else {
      this.getListCompletedOrders();
    }
  }
}
