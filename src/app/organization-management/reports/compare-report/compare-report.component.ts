import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartDataSets, ChartType } from 'chart.js';
import { CHART_TYPE } from '../../../shared/constants/charts';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-compare-report',
  templateUrl: './compare-report.component.html',
  styleUrls: ['./compare-report.component.scss']
})
export class CompareReportComponent implements OnInit {

  chartOptions: ChartOptions = {
    responsive: true,
    responsiveAnimationDuration: 500,
    maintainAspectRatio: false,
    legend: {
      display: true,
      position: 'top',
      // align: 'start', waiting for chart.js 2.9.0
      fullWidth: false,

      labels: {
        boxWidth: 4,
        fontColor: '#16171a',
        fontSize: 14,
        fontFamily: 'sfprotextregular',

        padding: 30,

        usePointStyle: true
      }
    },
    elements: {
      point: {
        radius: 3,
        pointStyle: 'circle',
        borderWidth: 3
      }
    }
  };
  chartLabels: Label[] = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  chartData: ChartDataSets[] = [
    { data: [], label: '01/01/2018 - 01/01/2019' },
    //{ data: [28, 48, 40, 19, 86, 27, 90, 65, 59, 80, 81, 56], label: 'Pick-up' },
    //{ data: [28, 48, 40, 19, 86, 27, 90, 40, 19, 86, 27, 90], label: 'Delivery' }
  ];
  chartColors = [
    { // blue-500,
      borderColor: '#0097ff',
      backgroundColor: '#0097ff',
    },
    { // purple-500,
      borderColor: '#6737e1',
      backgroundColor: '#6737e1',
    },
    { // green-500,
      borderColor: '#00cc00',
      backgroundColor: '#00cc00',
    }
  ];
  chartLegend = true;
  chartType = 'line';
  //

  constructor() { }

  ngOnInit() {
  }

}
