import { Component, OnInit } from '@angular/core';
import { Outlet } from 'src/app/shared/entities/outlets';
import { OutletsRepository } from 'src/app/shared/repositories/outlets.repository';
import { Product } from 'src/app/shared/entities/product';
import { ProductsRepository } from 'src/app/shared/repositories/products.repository';

@Component({
  selector: 'app-income-report',
  templateUrl: './income-report.component.html',
  styleUrls: ['./income-report.component.scss']
})
export class IncomeReportComponent implements OnInit {

  // public outlets: Outlet[] = [];
  // public products: Product[] = [];
  outlets: any = [
    { name: 'YCP Bui Vien', qty: '1', total_money: '150000', total_sale: '50000', total_revenues: '100000' },
    { name: 'YCP Dong Den', qty: '1', total_money: '200000', total_sale: '100000', total_revenues: '100000' },
    { name: ' YCP Nguyen Duc Canh ', qty: '1', total_money: '100000', total_sale: '50000', total_revenues: '100000' },
    { name: ' YCP Nguyen Van Huong ', qty: '1', total_money: '150000', total_sale: '50000', total_revenues: '100000' },
    { name: ' YCP Thong Nhat ', qty: '1', total_money: '150000', total_sale: '50000', total_revenues: '100000' },
  ];
  products: any = [
    { name: '#4 Cheese', qty: '1', total_money: '150000', total_sale: '50000', total_revenues: '100000' },
    { name: '7UP', qty: '1', total_money: '150000', total_sale: '50000', total_revenues: '100000' },
    { name: ' Aquafina ', qty: '1', total_money: '150000', total_sale: '50000', total_revenues: '100000' },
    { name: 'Baked Potato Halves', qty: '1', total_money: '150000', total_sale: '50000', total_revenues: '100000' },
    { name: 'Baked Potato Wedges', qty: '1', total_money: '150000', total_sale: '50000', total_revenues: '100000' },
    { name: 'BBQ Chicken    ', qty: '1', total_money: '150000', total_sale: '50000', total_revenues: '100000' },
    { name: 'BBQ Chicken Wings    ', qty: '1', total_money: '150000', total_sale: '50000', total_revenues: '100000' },
    { name: 'Charlie Chan Chicken    ', qty: '1', total_money: '150000', total_sale: '50000', total_revenues: '100000' },
    { name: 'Cheese Seafood    ', qty: '1', total_money: '150000', total_sale: '50000', total_revenues: '100000' },
    { name: 'Chicken Alfredo    ', qty: '1', total_money: '150000', total_sale: '50000', total_revenues: '100000' },
    { name: 'Chicken Garlic Parmesan    ', qty: '1', total_money: '150000', total_sale: '50000', total_revenues: '100000' },
    { name: 'Chicken Hot sauce    ', qty: '1', total_money: '150000', total_sale: '50000', total_revenues: '100000' },
    { name: 'Chicken Poppers BBQ    ', qty: '1', total_money: '150000', total_sale: '50000', total_revenues: '100000' },
  ]
  isShow = false;
  isShow1 = false;

  baseTableConfig = {
    paging: true,
    sorting: true,
    filtering: true,
    className: false,
  };
  baseTableColumns = [
    { title: 'name', name: 'name', className: 'name' },
    { title: 'qty order', name: 'qty', className: 'qty' },
    { title: 'total money', name: 'total_money', className: 'total-money' },
    { title: 'total sale', name: 'total_sale', className: 'total-sale' },
    { title: 'total revenues', name: 'total_revenues', className: 'total-revenues' },
  ];
  baseTableConfig1 = {
    paging: true,
    sorting: true,
    filtering: true,
    className: false,
  };
  baseTableColumns1 = [
    { title: 'name', name: 'name', className: 'name' },
    { title: 'qty order', name: 'qty', className: 'qty' },
    { title: 'total money', name: 'total_money', className: 'total-money' },
    { title: 'total sale', name: 'total_sale', className: 'total-sale' },
    { title: 'total revenues', name: 'total_revenues', className: 'total-revenues' },
  ];

  constructor(
    private outletRepositories: OutletsRepository,
    private proudtcRepositories: ProductsRepository,
  ) { }

  ngOnInit() {
    this.isShow = true;
    // this.getListOutlet();
  }
  // getListOutlet() {
  //   this.outletRepositories.all().subscribe(res => {
  //     console.log(res)
  //     this.outlets = res;
  //   })
  // }
  // getListProduct() {
  //   this.proudtcRepositories.all().subscribe(res => {
  //     console.log(res);
  //     this.products = res;
  //   })
  // }
  outletClick() {
    this.isShow = true;
    this.isShow1 = false;
    // this.getListOutlet();
  }
  productClick() {
    this.isShow1 = true;
    this.isShow = false;
    // this.getListProduct();
  }
}
