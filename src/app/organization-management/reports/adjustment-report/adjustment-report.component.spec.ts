import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdjustmentReportComponent } from './adjustment-report.component';

describe('AdjustmentReportComponent', () => {
  let component: AdjustmentReportComponent;
  let fixture: ComponentFixture<AdjustmentReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdjustmentReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdjustmentReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
