import { Component, OnInit } from '@angular/core';
declare var jQuery: any;

@Component({
  selector: 'app-adjustment-report',
  templateUrl: './adjustment-report.component.html',
  styleUrls: ['./adjustment-report.component.scss']
})
export class AdjustmentReportComponent implements OnInit {
  // table
  adjustment_report: any = [
    { item: 'Monogram Mug', sku: 'MM123', barcode: '1201 / 2000', qty: '1', created_date: '08/09/2019 12:59 PM', last_update: '09/09/2019 5:40 PM', reason: 'Changed mind', price: '15.000', discount: '0.00', employee: 'Shaamikh Al Hakim' },
    { item: 'Monogram Mug', sku: 'MM123', barcode: '1201 / 2000', qty: '1', created_date: '08/09/2019 12:59 PM', last_update: '09/09/2019 5:40 PM', reason: 'Changed mind', price: '15.000', discount: '0.00', employee: 'Shaamikh Al Hakim' },
    { item: 'Monogram Mug', sku: 'MM123', barcode: '360 / 1000', qty: '1', created_date: '08/09/2019 12:59 PM', last_update: '09/09/2019 5:40 PM', reason: 'Changed mind', price: '15.000', discount: '0.00', employee: 'Shaamikh Al Hakim' },
    { item: 'Monogram Mug', sku: 'MM123', barcode: '120 / 200', qty: '1', created_date: '08/09/2019 12:59 PM', last_update: '09/09/2019 5:40 PM', reason: 'Changed mind', price: '15.000', discount: '0.00', employee: 'Shaamikh Al Hakim' },
    { item: 'Monogram Mug', sku: 'MM123', barcode: '250 / 600', qty: '1', created_date: '08/09/2019 12:59 PM', last_update: '09/09/2019 5:40 PM', reason: 'Changed mind', price: '15.000', discount: '0.00', employee: 'Shaamikh Al Hakim' },
    { item: 'Monogram Mug', sku: '-', barcode: '120 / 200', qty: '1', created_date: '08/09/2019 12:59 PM', last_update: '09/09/2019 5:40 PM', reason: 'Changed mind', price: '15.000', discount: '0.00', employee: 'Shaamikh Al Hakim' },
    { item: 'Monogram Mug', sku: 'MM123', barcode: '120 / 200', qty: '1', created_date: '08/09/2019 12:59 PM', last_update: '09/09/2019 5:40 PM', reason: 'Changed mind', price: '15.000', discount: '0.00', employee: 'Shaamikh Al Hakim' },
    { item: 'Monogram Mug', sku: '-', barcode: '670 / 1500', qty: '1', created_date: '08/09/2019 12:59 PM', last_update: '09/09/2019 5:40 PM', reason: 'Changed mind', price: '15.000', discount: '0.00', employee: 'Shaamikh Al Hakim' },
    { item: 'Monogram Mug', sku: '-', barcode: '120 / 200', qty: '1', created_date: '08/09/2019 12:59 PM', last_update: '09/09/2019 5:40 PM', reason: 'Changed mind', price: '15.000', discount: '0.00', employee: 'Shaamikh Al Hakim' },
  ];
  baseTableConfig = {
    paging: true,
    sorting: true,
    filtering: false,
    className: false,
  };
  baseTableColumns = [
    { title: 'item', name: 'item', className: 'item' },
    { title: 'sku', name: 'sku', className: 'sku' },
    { title: 'barcode', name: 'barcode', className: 'barcode' },
    { title: 'qty', name: 'qty', className: 'qty' },
    { title: 'created date ', name: 'created_date', className: 'created-date' },
    { title: 'last update ', name: 'last_update', className: 'last-update' },
    { title: 'reason', name: 'reason', className: 'reason' },
    { title: 'price', name: 'price', className: 'price' },
    { title: 'discount', name: 'discount', className: 'discount' },
    { title: 'employee', name: 'employee', className: 'employee' },
  ];
  baseRadioValues = ['All (Default)', 'Yvonne Thompson', 'Harinder Mondi', 'Huo Shu', 'Otmar Doležal', 'Kondo Ieyasu', 'Noell Blue', 'Morganne Flaherty', 'Mar Rueda'];
  baseRadioValues1 = ['All (Default)', 'POS 1', 'POS 2', 'POS 3', 'POS 4', 'POS 5', 'POS 6', 'POS 7', 'POS 8', 'POS 9'];
  baseRadioValues2 = ['All (Default)', 'Item Deleted', 'Login Attempt', 'Tax Group Change', 'System Setting OptionChange', 'POS Station Settings Change', 'Peripherals Device SettingsChange'];
  baseRadio: string;
  baseRadio1: string;
  baseRadio2: string;

  constructor() { }

  ngOnInit() {
    this.filterReport();
  }
  filterReport() {
    (function ($) {
      $(document).ready(function () {
        $('body, html').on('click', function (event) {
          var target = $(event.target);
          if (!target.is('.filters , .filters *, .filters-report, .filters-report *, .export-btn, .export-btn *, .export, .export *')) {
            $('.filters').removeClass('active');
            $('.export').removeClass('active');
          }
        });
        $('.filters-report').click(function () {
          $('.filters').toggleClass('active');
          $('.export').removeClass('active');
        })
        $('.export-btn').click(function () {
          $('.export').toggleClass('active');
          $('.filters').removeClass('active');
        })
      });
    })(jQuery);
  }

}
