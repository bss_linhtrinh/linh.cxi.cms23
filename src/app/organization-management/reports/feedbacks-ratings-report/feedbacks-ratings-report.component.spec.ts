import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedbacksRatingsReportComponent } from './feedbacks-ratings-report.component';

describe('FeedbacksRatingsReportComponent', () => {
  let component: FeedbacksRatingsReportComponent;
  let fixture: ComponentFixture<FeedbacksRatingsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedbacksRatingsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedbacksRatingsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
