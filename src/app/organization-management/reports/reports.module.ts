import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChartsModule } from 'ng2-charts';

import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsComponent } from './reports/reports.component';
import { ActionLongComponent } from './action-long/action-long.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { DeliveryReportComponent } from './delivery-report/delivery-report.component';
import { CustomerOrderReportComponent } from './customer-order-report/customer-order-report.component';
import { DiscountReportComponent } from './discount-report/discount-report.component';
import { IncomeReportComponent } from './income-report/income-report.component';
import { GiftCardsReportComponent } from './gift-cards-report/gift-cards-report.component';
import { SalesReportComponent } from './sales-report/sales-report.component';
import { HourlySalesComponent } from './hourly-sales/hourly-sales.component';
import { OrderHistoryReportComponent } from './order-history-report/order-history-report.component';
import { InvoiceReportingComponent } from './invoice-reporting/invoice-reporting.component';
import { AdjustmentReportComponent } from './adjustment-report/adjustment-report.component';
import { CashManagmentFunctionalityComponent } from './cash-managment-functionality/cash-managment-functionality.component';
import { CompareReportComponent } from './compare-report/compare-report.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { PlatformReportComponent } from './platform-report/platform-report.component';
import { FeedbacksRatingsReportComponent } from './feedbacks-ratings-report/feedbacks-ratings-report.component';
import { TableReportSaleComponent } from './table-report-sale/table-report-sale.component';
import { TableProductSaleComponent } from './table-product-sale/table-product-sale.component';

@NgModule({
  declarations: [ReportsComponent, ActionLongComponent, DeliveryReportComponent, CustomerOrderReportComponent, DiscountReportComponent, IncomeReportComponent, GiftCardsReportComponent, SalesReportComponent, HourlySalesComponent, OrderHistoryReportComponent, InvoiceReportingComponent, AdjustmentReportComponent, CashManagmentFunctionalityComponent, CompareReportComponent, PlatformReportComponent, FeedbacksRatingsReportComponent, TableReportSaleComponent, TableProductSaleComponent],
  imports: [
    NgbModule,
    CommonModule,
    SharedModule,
    ChartsModule,
    FormsModule,
    ReportsRoutingModule
  ]
})
export class ReportsModule { }
