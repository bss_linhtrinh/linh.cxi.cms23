import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableProductSaleComponent } from './table-product-sale.component';

describe('TableProductSaleComponent', () => {
  let component: TableProductSaleComponent;
  let fixture: ComponentFixture<TableProductSaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableProductSaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableProductSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
