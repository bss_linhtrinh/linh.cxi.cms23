import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { actionLogReportRepository } from 'src/app/shared/repositories/action-log-report.repository';
declare const helper: any;

@Component({
  selector: 'app-table-product-sale',
  templateUrl: './table-product-sale.component.html',
  styleUrls: ['./table-product-sale.component.scss']
})
export class TableProductSaleComponent implements OnInit {
  public sales = [];
  public filterTable = [
    { name: 'CATEGORY', isShow: true, isShowAsc: true, isShowDesc: false, order_by: 'category_name' },
    { name: 'PRODUCT NAME', isShow: true, isShowAsc: true, isShowDesc: false, order_by: 'product_name' },
    { name: 'PRICE', isShow: true, isShowAsc: true, isShowDesc: false, order_by: 'price' },
    { name: 'QUANTITY', isShow: true, isShowAsc: true, isShowDesc: false, order_by: 'quantity' },
    { name: 'SALES TOTAL', isShow: true, isShowAsc: true, isShowDesc: false, order_by: 'sales_total' },
    { name: 'APPLICATION SCOPE', isShow: true, isShowAsc: true, isShowDesc: false, order_by: null, _disable: true },
  ];
  public filter_order: string = 'asc';
  public order_by_filter: string = 'category_name';
  @Input() dateStart: string;
  @Input() dateEnd: string;
  @Input() timeSelected: string;
  @LocalStorage('pagination.current_page') current_page: number = 1;
  @LocalStorage('pagination.per_page') per_page: number;
  @LocalStorage('pagination.total') total: number;
  constructor(
    private actionLogReportRepository: actionLogReportRepository,
  ) { }

  ngOnInit() {
    //this.getListSales();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dateStart || changes.dateEnd || changes.timeSelected) {
      this.getListSales();
    }
  }
  getListSales() {
    let sen_data = {
      date_start: this.dateStart,
      date_end: this.dateEnd,
      page: this.current_page,
      per_page: this.per_page,
      group_by: this.timeSelected,
      order: this.filter_order,
      order_by: this.order_by_filter,
    }
    this.actionLogReportRepository.all(sen_data)
      .subscribe(
        (res: any) => {
          this.sales = res;
          console.log(this.sales)
          this.sales.forEach(_i => {
            _i.price = helper.formatPrice(_i.price, 'VND');
            _i.sales_total = helper.formatPrice(_i.sales_total, 'VND');
          });
        },
        (error) => {
          console.log(error);
        }
      );
  }
  showListOrder(item?: any, index?: number) {
    this.order_by_filter = item.order_by;
    this.filterTable.forEach((o) => {
      if (o.name.toUpperCase() == item.name) {
        item.isShowDesc = true;
        item.isShowAsc = false;
        item.isShow = !item.isShow;
      } else {
        o.isShowAsc = true;
        o.isShowDesc = false;
        o.isShow = true;
      }
    });
    if (item.isShow == true) {
      this.filter_order = 'desc';
      this.getListSales();
    } else {
      this.filter_order = 'asc';
      this.getListSales();
    }
  }
  onChangePagination(){
    this.getListSales();
  }
}
