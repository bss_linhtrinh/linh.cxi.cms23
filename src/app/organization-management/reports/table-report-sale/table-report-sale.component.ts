import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { salesReportRepository } from 'src/app/shared/repositories/sales-report.repository';
import { LocalStorage } from 'ngx-webstorage';
declare const helper: any;

@Component({
  selector: 'app-table-report-sale',
  templateUrl: './table-report-sale.component.html',
  styleUrls: ['./table-report-sale.component.scss']
})
export class TableReportSaleComponent implements OnInit {
  public sales = [];
  public filterTable = [
    { name: 'APPLICATION SCOPE', isShow: true, isShowAsc: true, isShowDesc: false, order_by: 'name', _disable: true },
    { name: 'QUANTITY', isShow: true, isShowAsc: true, isShowDesc: false, order_by: 'quantity' },
    { name: 'SALES TOTAL', isShow: true, isShowAsc: true, isShowDesc: false, order_by: 'sales_total' },
  ];
  public filter_order: string = 'asc';
  public order_by_filter: string = 'name';
  @Input() dateStart: string;
  @Input() dateEnd: string;
  @Input() timeSelected: string;
  @LocalStorage('pagination.current_page') current_page: number = 1;
  @LocalStorage('pagination.per_page') per_page: number;
  @LocalStorage('pagination.total') total: number;
  constructor(
    private salesReportRepository: salesReportRepository,
  ) { }

  ngOnInit() {
    //this.getListSales();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.dateStart || changes.dateEnd || changes.timeSelected) {
      this.getListSales();
    }
  }
  getListSales() {
    let sen_data = {
      date_start: this.dateStart,
      date_end: this.dateEnd,
      page: this.current_page,
      per_page: this.per_page,
      group_by: this.timeSelected,
      order: this.filter_order,
      order_by: this.order_by_filter,
    }
    this.salesReportRepository.all(sen_data)
      .subscribe(
        (res: any) => {
          this.sales = res;
          this.sales.forEach(_i => {
            _i.sales_total = helper.formatPrice(_i.sales_total, 'VND');
          });
        },
        (error) => {
          console.log(error);
        }
      );
  }
  showListOrder(item?: any, index?: number) {
    this.order_by_filter = item.order_by;
    this.filterTable.forEach((o) => {
      if (o.name.toUpperCase() == item.name) {
        item.isShowDesc = true;
        item.isShowAsc = false;
        item.isShow = !item.isShow;
      } else {
        o.isShowAsc = true;
        o.isShowDesc = false;
        o.isShow = true;
      }
    });
    if (item.isShow == true) {
      this.filter_order = 'desc';
      this.getListSales();
    } else {
      this.filter_order = 'asc';
      this.getListSales();
    }
  }
  onChangePagination(){
    this.getListSales();
  }
}
