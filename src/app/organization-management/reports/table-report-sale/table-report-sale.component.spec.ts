import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableReportSaleComponent } from './table-report-sale.component';

describe('TableReportSaleComponent', () => {
  let component: TableReportSaleComponent;
  let fixture: ComponentFixture<TableReportSaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableReportSaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableReportSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
