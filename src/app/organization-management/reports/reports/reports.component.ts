import {Component, OnInit} from '@angular/core';
import {Scope} from '../../../shared/types/scopes';
import {LocalStorage, SessionStorage} from 'ngx-webstorage';

@Component({
    selector: 'app-reports',
    templateUrl: './reports.component.html',
    styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
    reports: {
        link: string,
        name: string,
        scopes: Scope[]
    }[];

    REPORTS = [
        {name: 'Action Log Report', link: '/reports/action-log', scopes: [Scope.Organization, Scope.Brand, Scope.Outlet]},
        {name: 'Delivery Reports', link: '/reports/delivery-report', scopes: [Scope.Organization, Scope.Brand, Scope.Outlet]},
        {name: 'Customer’s Orders Report', link: '/reports/customer-order', scopes: [Scope.Organization, Scope.Brand, Scope.Outlet]},
        {name: 'Discounts Report', link: '/reports/discount-order', scopes: [Scope.Organization, Scope.Brand, Scope.Outlet]},
        {name: 'Sales Report', link: '/reports/sales-report', scopes: [Scope.Organization, Scope.Brand, Scope.Outlet]},
        {name: 'Gift Cards Report', link: '/reports/gift-cards-report', scopes: [Scope.Organization, Scope.Brand, Scope.Outlet]},
        {name: 'Hourly Sales Report', link: '/reports/hourly-sales', scopes: [Scope.Organization, Scope.Brand, Scope.Outlet]},
        {name: 'Invoice Reporting', link: '/reports/invoice-reporting', scopes: [Scope.Organization, Scope.Brand]},
        {name: 'Order History Report', link: '/reports/order-history', scopes: [Scope.Organization, Scope.Brand, Scope.Outlet]},
        {name: 'Adjustments Report', link: '/reports/adjustment-report', scopes: [Scope.Organization, Scope.Brand]},
        {
            name: 'Cash Management Functionality',
            link: '/reports/cash-management-functionality',
            scopes: [Scope.Organization, Scope.Brand]
        },
        {name: 'Compare Report', link: '/reports/compare-report', scopes: [Scope.Organization, Scope.Brand]},
        {name: 'Income Report', link: '/reports/income-report', scopes: [Scope.Organization, Scope.Brand]},
        {name: 'Platform Report', link: '/reports/platform-report', scopes: [Scope.Organization, Scope.Brand]},
        {name: 'Feedback & Ratings', link: '/reports/feedback-ratings-report', scopes: [Scope.Organization, Scope.Brand]},
        {name: 'Ingredient Inventory Log', link: '/reports/index', scopes: [Scope.Organization, Scope.Brand]},
        {name: 'Ingredient Inventory Summary', link: '/reports/index', scopes: [Scope.Organization, Scope.Brand]},
        {name: 'Ingredient Inventory Summary Report', link: '/reports/index', scopes: [Scope.Organization, Scope.Brand]},
        {name: 'Item Tracking / Product Forecasting', link: '/reports/index', scopes: [Scope.Organization, Scope.Brand]},
        {name: 'Labor Report', link: '/reports/index', scopes: [Scope.Organization, Scope.Brand]},
        {
            name: 'Loss Management, Prevention and Auditing',
            link: '/reports/index',
            scopes: [Scope.Organization, Scope.Brand]
        },
    ];

    @LocalStorage('scope.current') currentScope: Scope;

    constructor() {
    }

    ngOnInit() {
        this.getReports();
    }

    getReports() {
        if (this.currentScope) {
            this.reports = this.REPORTS.filter(r => r.scopes.includes(this.currentScope));
        }
    }
}
