import { Component, OnInit } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { UtilitiesService } from 'src/app/shared/services/utilities/utilities.service';
import { formatDate } from '@angular/common';
@Component({
  selector: 'app-action-long',
  templateUrl: './action-long.component.html',
  styleUrls: ['./action-long.component.scss']
})
export class ActionLongComponent implements OnInit {
  today = new Date();
  @LocalStorage('dashboard.timeSelected') timeSelected: string;
  public dateEnd = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');
  public dateStart = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');

  constructor(
    private utilitiesService: UtilitiesService
  ) { }

  ngOnInit() {
    if (!this.dateEnd) {
      this.dateEnd = this.utilitiesService.parseDate(new Date());
    }
    if (!this.dateStart) {
      const dateStart = this.dateEnd ? new Date(this.dateEnd) : new Date();
      dateStart.setMonth(dateStart.getMonth() - 3);
      this.dateStart = this.utilitiesService.parseDate(dateStart);
    }
  }
  openDate() {
  }
  onChangeFilterData() {
  }
  selectTime(timeSelected: string) {
    this.timeSelected = timeSelected;
  }

}
