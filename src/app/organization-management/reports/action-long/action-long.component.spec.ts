import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionLongComponent } from './action-long.component';

describe('ActionLongComponent', () => {
  let component: ActionLongComponent;
  let fixture: ComponentFixture<ActionLongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionLongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionLongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
