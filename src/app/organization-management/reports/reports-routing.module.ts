import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CheckPermissionsGuard } from 'src/app/shared/guards/check-permissions/check-permissions.guard';
import { ReportsComponent } from './reports/reports.component';
import { ActionLongComponent } from './action-long/action-long.component';
import { DeliveryReportComponent } from './delivery-report/delivery-report.component';
import { CustomerOrderReportComponent } from './customer-order-report/customer-order-report.component';
import { DiscountReportComponent } from './discount-report/discount-report.component';
import { IncomeReportComponent } from './income-report/income-report.component';
import { GiftCardsReportComponent } from './gift-cards-report/gift-cards-report.component';
import { SalesReportComponent } from './sales-report/sales-report.component';
import { HourlySalesComponent } from './hourly-sales/hourly-sales.component';
import { OrderHistoryReportComponent } from './order-history-report/order-history-report.component';
import { InvoiceReportingComponent } from './invoice-reporting/invoice-reporting.component';
import { AdjustmentReportComponent } from './adjustment-report/adjustment-report.component';
import { CashManagmentFunctionalityComponent } from './cash-managment-functionality/cash-managment-functionality.component';
import { CompareReportComponent } from './compare-report/compare-report.component';
import { PlatformReportComponent } from './platform-report/platform-report.component';
import { FeedbacksRatingsReportComponent } from './feedbacks-ratings-report/feedbacks-ratings-report.component';

const routes: Routes = [
  {
    path: '',
    //canActivate: [CheckPermissionsGuard],
    children: [
      { path: 'product-sales-report', component: ActionLongComponent, data: { breadcrumb: 'Action Log Report' } },
      { path: 'delivery-report', component: DeliveryReportComponent, data: { breadcrumb: 'Delivery Reports' } },
      { path: 'customer-order', component: CustomerOrderReportComponent, data: { breadcrumb: 'Customer Orders Report' } },
      { path: 'discount-order', component: DiscountReportComponent, data: { breadcrumb: 'Discount Report' } },
      { path: 'income-report', component: IncomeReportComponent, data: { breadcrumb: 'Income Report' } },
      { path: 'gift-cards-report', component: GiftCardsReportComponent, data: { breadcrumb: 'Gift Cards Report' } },
      { path: 'sales-report', component: SalesReportComponent, data: { breadcrumb: 'Sales Report' } },
      { path: 'hourly-sales', component: HourlySalesComponent, data: { breadcrumb: 'Hourly Sales' } },
      { path: 'order-history', component: OrderHistoryReportComponent, data: { breadcrumb: 'Order History Report' } },
      { path: 'invoice-reporting', component: InvoiceReportingComponent, data: { breadcrumb: 'Invoice Reporting' } },
      { path: 'adjustment-report', component: AdjustmentReportComponent, data: { breadcrumb: 'Adjustment Report' } },
      { path: 'cash-managment-functionality', component: CashManagmentFunctionalityComponent, data: { breadcrumb: 'Cash Managment Functionality' } },
      { path: 'compare-report', component: CompareReportComponent, data: { breadcrumb: 'Compare Report' } },
      { path: 'platform-report', component: PlatformReportComponent, data: { breadcrumb: 'Platform Report' } },
      { path: 'feedback-ratings-report', component: FeedbacksRatingsReportComponent, data: { breadcrumb: 'Feedbacks & Ratings' } },
      { path: 'index', component: ReportsComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
