import { Component, OnInit } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { UtilitiesService } from 'src/app/shared/services/utilities/utilities.service';
import { formatDate } from '@angular/common';
@Component({
  selector: 'app-sales-report',
  templateUrl: './sales-report.component.html',
  styleUrls: ['./sales-report.component.scss']
})
export class SalesReportComponent implements OnInit {
  today = new Date();
  @LocalStorage('dashboard.timeSelected') timeSelected: string;
  public dateEnd = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');
  public dateStart = formatDate(this.today, 'yyyy-MM-dd', 'en-US', '+0530');
  // pagination

  constructor(
    private utilitiesService: UtilitiesService
  ) { }

  ngOnInit() {
    if (!this.dateEnd) {
      this.dateEnd = this.utilitiesService.parseDate(new Date());
    }
    if (!this.dateStart) {
      const dateStart = this.dateEnd ? new Date(this.dateEnd) : new Date();
      dateStart.setMonth(dateStart.getMonth() - 3);
      this.dateStart = this.utilitiesService.parseDate(dateStart);
    }
  }
  openDate() {
  }
  onChangeFilterData() {
  }
  selectTime(timeSelected: string) {
    this.timeSelected = timeSelected;
  }
}
