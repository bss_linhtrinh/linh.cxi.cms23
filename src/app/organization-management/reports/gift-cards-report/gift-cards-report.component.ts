import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gift-cards-report',
  templateUrl: './gift-cards-report.component.html',
  styleUrls: ['./gift-cards-report.component.scss']
})
export class GiftCardsReportComponent implements OnInit {

  // table
  delivery: any = [
    { gift_id: 'D20190320', description: '50% off total bill', qty: '120 / 200' },
    { gift_id: 'D20190320', description: 'Buy 1 get 1 free when purchase with a drink', qty: '1201 / 2000' },
    { gift_id: 'D20190320', description: 'Enter the code to get free ship', qty: '360 / 1000' },
    { gift_id: 'D20190320', description: '20% off for the new dish in town', qty: '120 / 200' },
    { gift_id: 'D20190320', description: 'Party with friend, pay 3 for 4 people', qty: '250 / 600' },
    { gift_id: 'D20190320', description: '50% off total bill', qty: '120 / 200' },
    { gift_id: 'D20190320', description: 'Buy 2 get 1 free', qty: '120 / 200' },
    { gift_id: 'D20190320', description: '20% off for the new dish in town', qty: '670 / 1500' },
    { gift_id: 'D20190320', description: '50% off total bill', qty: '120 / 200' },
  ];
  baseTableConfig = {
    paging: true,
    sorting: true,
    filtering: false,
    className: false,
  };
  baseTableColumns = [
    { title: 'GIFT CARD ID', name: 'gift_id', className: 'gift-id' },
    { title: 'description', name: 'description', className: 'description' },
    { title: 'qty', name: 'qty', className: 'qty' },
  ];

  constructor() { }

  ngOnInit() {
  }

    startDate() {

    }

    endDate() {

    }

    openDate() {

    }

}
