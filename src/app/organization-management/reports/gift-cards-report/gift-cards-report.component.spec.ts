import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftCardsReportComponent } from './gift-cards-report.component';

describe('GiftCardsReportComponent', () => {
  let component: GiftCardsReportComponent;
  let fixture: ComponentFixture<GiftCardsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftCardsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftCardsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
