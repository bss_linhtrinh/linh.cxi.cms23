import { Component, OnInit } from '@angular/core';
import { hourlySalesReportRepository } from 'src/app/shared/repositories/hourly-sales-report.repository';
import { LocalStorage } from 'ngx-webstorage';
import { UtilitiesService } from 'src/app/shared/services/utilities/utilities.service';

declare const helper: any;

@Component({
  selector: 'app-hourly-sales',
  templateUrl: './hourly-sales.component.html',
  styleUrls: ['./hourly-sales.component.scss']
})
export class HourlySalesComponent implements OnInit {

  public hourly_sales: any = [];
  isShow: boolean = false;
  @LocalStorage('dashboard.timeSelected') timeSelected: string;
  @LocalStorage('dashboard.dateStart') dateStart: string;
  @LocalStorage('dashboard.dateEnd') dateEnd: string;
  constructor(
    private hourlySaleReportRepository: hourlySalesReportRepository,
    private utilitiesService: UtilitiesService
  ) { }

  ngOnInit() {
    this.getListHourlySaleReport();
    if (!this.dateEnd) {
      this.dateEnd = this.utilitiesService.parseDate(new Date());
    }
    if (!this.dateStart) {
      const dateStart = this.dateEnd ? new Date(this.dateEnd) : new Date();
      dateStart.setMonth(dateStart.getMonth() - 3);
      this.dateStart = this.utilitiesService.parseDate(dateStart);
    }
  }
  getListHourlySaleReport() {
    let sen_data = {
      date_start: this.dateStart,
      date_end: this.dateEnd,
    }
    this.hourlySaleReportRepository.all(sen_data)
      .subscribe(
        (res: any) => {
          this.hourly_sales = res;
          this.hourly_sales.forEach(_o => {
            _o.isShow = true;
          });
        },
        (error) => {
          helper.showNotification('Failed !!!', 'error', 'danger', 900);
        }
      );
  }
  showOrder(_item: any) {
    _item.isShow = !_item.isShow;
  }
  openDate() {
    this.getListHourlySaleReport();
  }

}
