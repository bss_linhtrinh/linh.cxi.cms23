import {Component, OnInit} from '@angular/core';
import {Option} from '../../../shared/entities/option';
import {Brand} from '../../../shared/entities/brand';
import {SettingsRepository} from '../../../shared/repositories/settings.repository';
import {ActivatedRoute, Router} from '@angular/router';
import {ScopeService} from '../../../shared/services/scope/scope.service';
import {BrandsRepository} from '../../../shared/repositories/brands.repository';
import {switchMap, tap} from 'rxjs/operators';
import {UserScope} from '../../../shared/entities/user-scope';
import {of} from 'rxjs';

@Component({
    selector: 'app-create-option',
    templateUrl: './create-option.component.html',
    styleUrls: ['./create-option.component.scss']
})
export class CreateOptionComponent implements OnInit {

    option: Option = new Option();
    brands: Brand[] = [];
    isBrand = false;

    constructor(
        private settingsRepository: SettingsRepository,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private scopeService: ScopeService,
        private brandsRepository: BrandsRepository
    ) {
    }

    ngOnInit() {
        this.getBrands();
    }

    back() {
        this.router.navigate(['../list'], {relativeTo: this.activatedRoute});
    }

    cancel() {
        this.back();
    }

    getBrands() {
        this.scopeService.getUserScope$()
            .pipe(
                switchMap(
                    (userScope: UserScope) => {
                        if (userScope && userScope.brand && userScope.brand.id) {
                            this.option.brand_id = userScope.brand.id;
                            this.isBrand = true;
                            return of([userScope.brand]);
                        } else {
                            return this.brandsRepository.all();
                        }
                    }
                )
            )
            .subscribe(
                (brands: Brand[]) => {
                    this.brands = brands;
                }
            );
    }

    save() {
        this.option.scope = 'brands';
        if (this.option.is_json) {
            this.option.value = JSON.parse(this.option.value);
        }

        this.settingsRepository.save(this.option)
            .pipe(
                tap(
                    () => this.back()
                )
            )
            .subscribe();
    }

}
