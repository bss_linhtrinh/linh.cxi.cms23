import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CheckPermissionsGuard} from '../../shared/guards/check-permissions/check-permissions.guard';
import {ListOptionComponent} from './list-option/list-option.component';
import {CreateOptionComponent} from './create-option/create-option.component';
import {EditOptionComponent} from './edit-option/edit-option.component';


const routes: Routes = [
    {
        path: '',
        canActivate: [
            // CheckPermissionsGuard
        ],
        children: [
            {path: 'list', component: ListOptionComponent, data: {breadcrumb: 'List options'}},
            {path: 'create', component: CreateOptionComponent, data: {breadcrumb: 'Create new option'}},
            {path: ':id/edit', component: EditOptionComponent, data: {breadcrumb: 'Edit option'}},
            {path: '', redirectTo: 'list'},
        ]
    }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OptionRoutingModule { }
