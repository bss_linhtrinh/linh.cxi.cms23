import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Option} from '../../../shared/entities/option';
import {SettingsRepository} from '../../../shared/repositories/settings.repository';
import {CxiGridConfig} from '../../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../../shared/components/cxi-grid/cxi-grid-column';

@Component({
    selector: 'app-list-option',
    templateUrl: './list-option.component.html',
    styleUrls: ['./list-option.component.scss']
})
export class ListOptionComponent implements OnInit {
    cxiGridConfig = CxiGridConfig.from({
        paging: false,
        sorting: true,
        filtering: true,
        selecting: true,
        actions: {
            onEdit: (option: Option) => {
                this.onEdit(option);
            }
        }
    });
    cxiGridColumns = CxiGridColumn.from([
        {title: 'Key', name: 'key'},
        {title: 'Value', name: 'value'}
    ]);
    options: Option[] = [];

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private settingsRepository: SettingsRepository
    ) {
    }

    ngOnInit() {
        this.getOptions();
    }

    getOptions() {
        this.settingsRepository.all({scope: 'brands', pagination: 0})
            .subscribe(
                (options: Option[]) => this.options = options
            );
    }

    onEdit(option: Option) {
        this.router.navigate(['..', option.key, 'edit'], {relativeTo: this.route});
    }
}
