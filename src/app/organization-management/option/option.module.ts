import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OptionRoutingModule} from './option-routing.module';
import {ListOptionComponent} from './list-option/list-option.component';
import {CreateOptionComponent} from './create-option/create-option.component';
import {EditOptionComponent} from './edit-option/edit-option.component';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule} from '@angular/forms';


@NgModule({
    declarations: [ListOptionComponent, CreateOptionComponent, EditOptionComponent],
    imports: [
        FormsModule,
        CommonModule,
        SharedModule,
        OptionRoutingModule
    ]
})
export class OptionModule {
}
