import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from 'src/app/shared/shared.module';
import {SettingsRoutingModule} from './settings-routing.module';
import {SettingsComponent} from './settings/settings.component';

import {FormsModule} from '@angular/forms';
import {ShippingAreaComponent} from './shipping-area/shipping-area.component';
import {PaymentMethodComponent} from './payment-method/payment-method.component';
import {NotificationComponent} from './notification/notification.component';
import {RegionLanguageComponent} from './region-language/region-language.component';
import {OrderComponent} from './order/order.component';
import {CommissionsComponent} from './commissions/commissions.component';
import {CreateCommissionComponent} from './commissions/create-commission/create-commission.component';
import {EditCommissionComponent} from './commissions/edit-commission/edit-commission.component';
import { BannerComponent } from './banner/banner.component';
import { BannerLinksComponent } from './banner/banner-links/banner-links.component';
import { CurrencyComponent } from './currency/currency.component';
import { CreateCurrencyComponent } from './currency/create-currency/create-currency.component';
import { EditCurrencyComponent } from './currency/edit-currency/edit-currency.component';
import { SupportedCurrencyComponent } from './supported-currency/supported-currency.component';
import { CreateSupportedCurrencyComponent } from './supported-currency/create-supported-currency/create-supported-currency.component';
import { EditSupportedCurrencyComponent } from './supported-currency/edit-supported-currency/edit-supported-currency.component';
import {QRCodeModule} from 'angularx-qrcode';

@NgModule({
    declarations: [
        SettingsComponent,
        ShippingAreaComponent,
        PaymentMethodComponent,
        NotificationComponent,
        RegionLanguageComponent,
        OrderComponent,
        CommissionsComponent,
        CreateCommissionComponent,
        EditCommissionComponent,
        BannerComponent,
        BannerLinksComponent,
        CurrencyComponent,
        CreateCurrencyComponent,
        EditCurrencyComponent,
        SupportedCurrencyComponent,
        CreateSupportedCurrencyComponent,
        EditSupportedCurrencyComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        QRCodeModule,
        SettingsRoutingModule
    ],
    entryComponents: [
        CreateCommissionComponent,
        EditCommissionComponent,
        CreateCurrencyComponent,
        EditCurrencyComponent,
        CreateSupportedCurrencyComponent,
        EditSupportedCurrencyComponent
    ]
})
export class SettingsModule {
}
