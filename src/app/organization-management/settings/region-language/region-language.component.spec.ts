import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegionLanguageComponent } from './region-language.component';

describe('RegionLanguageComponent', () => {
  let component: RegionLanguageComponent;
  let fixture: ComponentFixture<RegionLanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegionLanguageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegionLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
