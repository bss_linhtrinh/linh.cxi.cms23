import {Component, OnInit} from '@angular/core';
import {LanguageService} from '../../../shared/services/language/language.service';
import {finalize, map, tap} from 'rxjs/operators';
import {BrandsRepository} from '../../../shared/repositories/brands.repository';
import {AuthService} from '../../../shared/services/auth/auth.service';
import {LocalStorage} from 'ngx-webstorage';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import {User} from '../../../shared/entities/user';
import {Provider} from '../../../shared/types/providers';
import {DEFAULT_LANGUAGE, LANGUAGES} from '../../../shared/constants/languages';
import {SettingsService} from '../settings.service';
import {RegionLanguage} from '../../../shared/entities/region-language';
import {MessagesService} from '../../../messages/messages.service';

@Component({
    selector: 'app-region-language',
    templateUrl: './region-language.component.html',
    styleUrls: ['./region-language.component.scss']
})
export class RegionLanguageComponent implements OnInit {
    languages = [];
    regionLanguage: RegionLanguage = new RegionLanguage();
    clonedRegionLanguage: RegionLanguage;

    currentUser: User;
    currentProvider: Provider;

    isLoading: boolean;

    @LocalStorage('settings.currentLanguage') currentLanguage: string;

    constructor(
        private languageService: LanguageService,
        private settingsService: SettingsService,
        private brandsRepository: BrandsRepository,
        private authService: AuthService,
        private utilities: UtilitiesService,
        private messagesService: MessagesService
    ) {
        this.currentUser = authService.currentUser();
        this.currentProvider = authService.getCurrentProvider();
    }

    ngOnInit() {
        this.getLanguages();
        this.getCurrentLanguage();
    }

    getLanguages() {
        this.languages = LANGUAGES;
    }

    getCurrentLanguage() {
        // clone
        this.regionLanguage.language = this.currentLanguage;
        this.clonedRegionLanguage = this.utilities.cloneDeep(this.regionLanguage);

        if (!this.regionLanguage.language) {
            this.regionLanguage.language = DEFAULT_LANGUAGE;
            this.currentLanguage = DEFAULT_LANGUAGE;
            this.setLanguage();
        }

        // remote (admin)
        if (this.currentUser &&
            this.currentProvider === Provider.Admin) {
            this.settingsService.getSetting('language', 'users', this.currentUser.id)
                .pipe(
                    map((regionLanguage: any) => RegionLanguage.create(regionLanguage)),
                    tap((regionLanguage: any) => {
                        this.regionLanguage = regionLanguage;
                        this.clonedRegionLanguage = this.utilities.cloneDeep(this.regionLanguage);
                    })
                )
                .subscribe(
                    () => this.handleChangeLanguage()
                );
        }
    }

    setLanguage() {
        if (this.currentProvider === Provider.Admin) {
            this.isLoading = true;
            this.settingsService.setSetting(this.regionLanguage, 'language', 'users', this.currentUser.id)
                .pipe(
                    finalize(() => this.isLoading = false),
                    map((regionLanguage: any) => RegionLanguage.create(regionLanguage)),
                    tap((regionLanguage: any) => {
                        this.regionLanguage = regionLanguage;
                        this.clonedRegionLanguage = this.utilities.cloneDeep(this.regionLanguage);
                    })
                )
                .subscribe(
                    () => this.handleChangeLanguage()
                );
        } else if (this.currentProvider === Provider.SuperAdmin) {
            this.handleChangeLanguage();
            this.clonedRegionLanguage = this.utilities.cloneDeep(this.regionLanguage);
        }
    }

    handleChangeLanguage() {
        // event change language
        if (this.regionLanguage.language !== this.currentLanguage) {
            this.languageService.changeLanguage(this.regionLanguage.language);
            this.currentLanguage = this.regionLanguage.language;
        }
    }

    save() {
        this.setLanguage();
    }

    labelButtonSave() {
        return (this.currentProvider === Provider.Admin) ? 'Save & Apply' : 'Apply';
    }

    isChangedData(): boolean {
        return !this.utilities.isEqual(this.clonedRegionLanguage, this.regionLanguage);
    }

    canDeactivate() {
        if (this.isChangedData()) {
            return this.messagesService.unsavedChanges();
        }
        return true;
    }
}
