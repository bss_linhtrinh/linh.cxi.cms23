import {Component, OnInit} from '@angular/core';
import {UserScope} from 'src/app/shared/entities/user-scope';
import {ScopeService} from 'src/app/shared/services/scope/scope.service';
import {LocalStorage} from 'ngx-webstorage';
import {Scope} from '../../../shared/types/scopes';
import * as _ from 'lodash';
import {tap} from 'rxjs/operators';
import {MessagesService} from '../../../messages/messages.service';
import {NotificationService} from '../../../shared/services/noitification/notification.service';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

    userScope: UserScope;
    clonedUserScope: UserScope;

    selectedFile: File = null;

    @LocalStorage('scope.current') currentScope: Scope;

    constructor(
        private scopeService: ScopeService,
        private messagesService: MessagesService,
        private notificationService: NotificationService
    ) {
    }

    ngOnInit() {
        this.getUserScope();
    }

    getUserScope() {
        this.scopeService.getUserScope$()
            .pipe(
                tap(
                    (userScope: UserScope) => {
                        this.userScope = userScope;
                        this.clonedUserScope = _.cloneDeep(this.userScope);
                    }
                )
            )
            .subscribe();
    }

    onSubmit() {
        this.scopeService.updateUserScope$(this.userScope)
            .pipe(
                tap((userScope) => {
                    this.userScope = userScope;
                    this.notificationService.success([`Update successful`]);
                }),
                tap(() => this.clonedUserScope = _.cloneDeep(this.userScope))
            )
            .subscribe(
            );
    }

    onFileChanged(selectedFile: File) {
        this.selectedFile = selectedFile;
    }

    canDeactivate() {
        if (this.isChangedData()) {
            return this.messagesService.unsavedChanges();
        }
        return true;
    }

    isChangedData(): boolean {
        return !_.isEqual(this.userScope, this.clonedUserScope);
    }
}
