import {Component, OnInit} from '@angular/core';
import {LocalStorage} from 'ngx-webstorage';
import {SettingsService} from '../settings.service';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import {MessagesService} from '../../../messages/messages.service';

@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
    settingOrder: {
        delete_pending_order_daily: boolean,
        time_to_delete: number,
        auto_confirm_order: boolean
    } = {
        delete_pending_order_daily: false,
        time_to_delete: 0,
        auto_confirm_order: false
    };
    cloneSettingOrder: any;
    timeToDeleteOptions: any[] = [
        {value: 0, name: '0 AM'},
        {value: 1, name: '1 AM'},
        {value: 2, name: '2 AM'},
        {value: 3, name: '3 AM'},
        {value: 4, name: '4 AM'},
        {value: 5, name: '5 AM'},
        {value: 6, name: '6 AM'},
        {value: 7, name: '7 AM'},
        {value: 8, name: '8 AM'},
        {value: 9, name: '9 AM'},
        {value: 10, name: '10 AM'},
        {value: 11, name: '11 AM'},
        {value: 12, name: '12 PM'},
        {value: 13, name: '1 PM'},
        {value: 14, name: '2 PM'},
        {value: 15, name: '3 PM'},
        {value: 16, name: '4 PM'},
        {value: 17, name: '5 PM'},
        {value: 18, name: '6 PM'},
        {value: 19, name: '7 PM'},
        {value: 20, name: '8 PM'},
        {value: 21, name: '9 PM'},
        {value: 22, name: '10 PM'},
        {value: 23, name: '11 PM'},
    ];

    @LocalStorage('settings.order') orderSetting: any;
    @LocalStorage('scope.brand_id') brandId: number;

    settingType = 'order';
    settingScope = 'brands';
    settingScopeId: number;

    constructor(
        private settingsService: SettingsService,
        private utilitiesService: UtilitiesService,
        private messagesService: MessagesService
    ) {
    }

    ngOnInit() {
        this.getSettings();
    }

    getSettings() {
        if (this.brandId) {
            this.settingScopeId = this.brandId;
        }

        this.settingsService.getSetting(this.settingType, this.settingScope, this.settingScopeId)
            .subscribe(
                (settingOrder: any) => {
                    this.settingOrder = Object.assign(this.settingOrder, settingOrder);
                    this.cloneSettingOrder = this.utilitiesService.cloneDeep(this.settingOrder);
                }
            );
    }

    saveSetting() {
        if (this.brandId) {
            this.settingScopeId = this.brandId;
        }

        this.settingsService.setSetting(this.settingOrder, this.settingType, this.settingScope, this.settingScopeId)
            .subscribe(
                (settingOrder: any) => {
                    this.settingOrder = Object.assign(this.settingOrder, settingOrder);
                    this.cloneSettingOrder = this.utilitiesService.cloneDeep(this.settingOrder);
                }
            );
    }

    canDeactivate() {
        if (this.isChangedData()) {
            return this.messagesService.unsavedChanges();
        }
        return true;
    }

    isChangedData(): boolean {
        return !this.utilitiesService.isEqual(this.cloneSettingOrder, this.settingOrder);
    }
}
