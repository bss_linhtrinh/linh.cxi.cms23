import {BaseEntity} from '../../../shared/entities/base.entity';

export class AdvanceSetting extends BaseEntity {
    outlet_id: number;
    radius: number;

    constructor() {
        super();
        this.outlet_id = null;
        this.radius = null;
    }

    static create(data) {
        return new AdvanceSetting().fill(data);
    }

    fill(data) {
        return super.fill(data);
    }
}
