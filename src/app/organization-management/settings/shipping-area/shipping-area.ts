import {BaseEntity} from '../../../shared/entities/base.entity';
import {AdvanceSetting} from './advance-setting';

export class ShippingArea extends BaseEntity {
    shipping_area_all_outlets: boolean;
    shipping_area_radius: number;
    shipping_area_advance_setting: AdvanceSetting[];

    constructor() {
        super();
        this.shipping_area_all_outlets = false;
        this.shipping_area_radius = null;
        this.shipping_area_advance_setting = [new AdvanceSetting()];
    }

    static create(data) {
        return new ShippingArea().fill(data);
    }

    fill(data) {
        if (data.shipping_area_advance_setting &&
            typeof data.shipping_area_advance_setting === 'string') {
            data.shipping_area_advance_setting = JSON.parse(data.shipping_area_advance_setting);

            if (data.shipping_area_advance_setting &&
                data.shipping_area_advance_setting.length > 0) {
                data.shipping_area_advance_setting = data.shipping_area_advance_setting.map(advance => AdvanceSetting.create(advance));
            }
        }

        return super.fill(data);
    }

    parse() {
        return {
            shipping_area_radius: this.shipping_area_radius,
            shipping_area_all_outlets: this.shipping_area_all_outlets,
            shipping_area_advance_setting: JSON.stringify(
                this.shipping_area_advance_setting.filter(
                    (advanceSetting: AdvanceSetting) => advanceSetting.radius && advanceSetting.outlet_id
                )
            ),
        };
    }
}
