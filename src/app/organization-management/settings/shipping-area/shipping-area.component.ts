import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Outlet} from '../../../shared/entities/outlets';
import {OutletsRepository} from '../../../shared/repositories/outlets.repository';
import {delay, map, tap} from 'rxjs/operators';
import {SettingsService} from '../settings.service';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import {MessagesService} from '../../../messages/messages.service';
import {LocalStorage} from 'ngx-webstorage';
import {AdvanceSetting} from './advance-setting';
import {ShippingArea} from './shipping-area';
import {environment} from '../../../../environments/environment';
import {NotificationService} from '../../../shared/services/noitification/notification.service';
import {SHIPPING_ADDRESS_MAXIMUM_RADIUS, SHIPPING_ADDRESS_MINIMUM_RADIUS} from '../../../shared/constants/constants';

declare const google: any;

@Component({
    selector: 'app-shipping-area',
    templateUrl: './shipping-area.component.html',
    styleUrls: ['./shipping-area.component.scss']
})
export class ShippingAreaComponent implements OnInit {

    outlets: Outlet[];
    settingShippingArea: ShippingArea = new ShippingArea();
    cloneSettingShippingArea: ShippingArea;

    settingType = 'shipping_area';
    settingScope = 'brands';
    settingScopeId: number;

    @ViewChild('mapContainer', {static: true}) gmap: ElementRef;

    map: google.maps.Map;

    @LocalStorage('scope.organization_id') organizationId: number;
    @LocalStorage('scope.brand_id') brandId: number;
    @LocalStorage('scope.outlet_id') outletId: number;

    constructor(
        private outletsRepository: OutletsRepository,
        private settingsService: SettingsService,
        private utilitiesService: UtilitiesService,
        private messagesService: MessagesService,
        private notificationService: NotificationService
    ) {
    }

    ngOnInit() {
        this.getSettings();
        this.getOutlets();
    }

    getSettings() {
        if (this.brandId) {
            this.settingScopeId = this.brandId;
        }

        this.settingsService.getSetting(this.settingType, this.settingScope, this.settingScopeId)
            .pipe(
                map(
                    (shippingArea: any) => ShippingArea.create(shippingArea)
                )
            )
            .subscribe(
                (settingShippingArea: ShippingArea) => {
                    this.settingShippingArea = settingShippingArea;
                    this.cloneSettingShippingArea = this.utilitiesService.cloneDeep(this.settingShippingArea);
                }
            );
    }

    getOutlets() {
        this.outletsRepository.all()
            .pipe(
                delay(100),
                tap((outlets: Outlet[]) => this.outlets = outlets),
                tap(() => this.mapInitializer())
            )
            .subscribe();
    }

    mapInitializer() {
        if (this.outlets && this.outlets.length > 0) {
            const coordinates = new google.maps.LatLng(this.outlets[0].latitude, this.outlets[0].longitude);
            const mapOptions: google.maps.MapOptions = {
                center: coordinates,
                zoom: 12,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            this.map = new google.maps.Map(this.gmap.nativeElement, mapOptions);
        }

        this.initMarker();
        this.mapDrawRadius();
    }

    initMarker() {
        // marker outlets
        this.outlets
            .filter(
                (outlet: Outlet) => {
                    return outlet && outlet.latitude && outlet.longitude;
                }
            )
            .forEach(
                (outlet: Outlet, index: number) => {
                    const isPrimary = index ? 1 : 0;
                    const icons = [
                        environment.baseCmsUrl + '/assets/img/map/' + 'pin.png',
                        environment.baseCmsUrl + '/assets/img/map/' + 'white-pin.png',
                    ];
                    const image = {
                        url: icons[isPrimary],
                        // This marker is 32 pixels wide by 32 pixels high.
                        size: new google.maps.Size(32, 32),
                        // The origin for this image is (0, 0).
                        origin: new google.maps.Point(0, 0),
                        // The anchor for this image is the base of the flagpole at (0, 32).
                        anchor: new google.maps.Point(16, 16)
                    };

                    const coordinates = new google.maps.LatLng(outlet.latitude, outlet.longitude);
                    const marker = new google.maps.Marker({
                        position: coordinates,
                        icon: image,
                        map: this.map,
                        label: {
                            text: outlet.name,
                            color: 'white',
                            fontSize: '14px',
                        },
                    });

                    // const markerWithLabel = new marker({
                    //     position: coordinates,
                    //     map: map,
                    //     draggable: true,
                    //     raiseOnDrag: true,
                    //     labelContent: outlet.name,
                    //     labelAnchor: new google.maps.Point(15, 65),
                    //     labelClass: 'labels', // the CSS class for the label
                    //     labelInBackground: true,
                    //     icon: {
                    //         path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
                    //         fillColor: 'red',
                    //         fillOpacity: 1,
                    //         strokeColor: '#000',
                    //         strokeWeight: 2,
                    //         scale: 2
                    //     }
                    // });

                    marker.setMap(this.map);
                }
            );
    }

    mapDrawRadius() {
        const styles = [
            {color: 'rgba(0, 151, 255)', strokeOpacity: 0.5, fillOpacity: 0.8},
            {color: 'rgba(0, 151, 255)', strokeOpacity: 0.1, fillOpacity: 0.2}
        ];
        this.outlets
            .filter(
                (outlet: Outlet) => {
                    return outlet && outlet.latitude && outlet.longitude;
                }
            )
            .forEach(
                (outlet: Outlet, index: number) => {
                    const coordinates = new google.maps.LatLng(outlet.latitude, outlet.longitude);

                    // draw
                    // get radius (km)
                    let radius = this.settingShippingArea.shipping_area_radius
                        ? this.settingShippingArea.shipping_area_radius
                        : 0;
                    if (this.settingShippingArea.shipping_area_advance_setting instanceof Array) {
                        const outletAdvanceSetting = this.settingShippingArea.shipping_area_advance_setting
                            .find(item => item.outlet_id === outlet.id);
                        if (outletAdvanceSetting && outletAdvanceSetting.radius) {
                            radius = outletAdvanceSetting.radius;
                        }
                    }

                    if (outlet.radiusCircle &&
                        outlet.radiusCircle instanceof google.maps.Circle) {
                        outlet.radiusCircle.setMap(null);
                    }

                    const isPrimary = index ? 1 : 0;
                    outlet.radiusCircle = new google.maps.Circle({
                        strokeColor: styles[isPrimary].color,
                        strokeOpacity: styles[isPrimary].strokeOpacity,
                        strokeWeight: 2,
                        fillColor: styles[isPrimary].color,
                        fillOpacity: styles[isPrimary].fillOpacity,
                        map: this.map,
                        center: coordinates,
                        radius: radius * 1000
                    });
                });
    }

    onChangeAllOutlets() {
        //
    }

    // ========== =============== ==========
    // ========== Advance setting ==========
    // ========== =============== ==========

    addAdvanceSetting() {
        const advanceSettings = this.settingShippingArea.shipping_area_advance_setting;

        if (!advanceSettings ||
            typeof advanceSettings === 'string'
        ) {
            return;
        }

        const lastItem = advanceSettings[advanceSettings.length - 1];
        if (lastItem &&
            !lastItem.outlet_id) {
            return;
        }

        this.settingShippingArea.shipping_area_advance_setting.push(new AdvanceSetting());

        this.checkDisabled();
    }

    removeAdvanceSetting(advanceSetting: AdvanceSetting) {
        const advanceSettings = this.settingShippingArea.shipping_area_advance_setting;
        if (!advanceSettings) {
            return;
        }

        const index = advanceSettings.findIndex(ad => ad.outlet_id === advanceSetting.outlet_id);
        if (index !== -1) {
            this.settingShippingArea.shipping_area_advance_setting.splice(index, 1);
        }

        this.checkEmpty();
        this.checkDisabled();
    }

    onChangeOutlet() {

    }

    checkEmpty() {
        if (!this.settingShippingArea.shipping_area_advance_setting) {
            this.settingShippingArea.shipping_area_advance_setting = [new AdvanceSetting()];
            return;
        }

        if (this.settingShippingArea.shipping_area_advance_setting.length === 0) {
            if (!(this.settingShippingArea.shipping_area_advance_setting instanceof Array)) {
                this.settingShippingArea.shipping_area_advance_setting = [];
            }
            this.settingShippingArea.shipping_area_advance_setting.push(new AdvanceSetting());
        }
    }

    checkDisabled() {
        this.outlets.forEach(
            (outlet: Outlet) => {
                if (!(this.settingShippingArea.shipping_area_advance_setting instanceof Array)) {
                    this.settingShippingArea.shipping_area_advance_setting = [];
                }

                const index = this.settingShippingArea.shipping_area_advance_setting.findIndex(
                    (advanceSetting: AdvanceSetting) => {
                        return advanceSetting.outlet_id === outlet.id;
                    }
                );
                outlet.disabled = index !== -1;

                return outlet;
            }
        );
    }

    // ========== ============ ==========
    // ========== Save setting ==========
    // ========== ============ ==========

    beforeSubmit(): boolean {
        let invalid = false;
        const messages = [];

        this.settingShippingArea.shipping_area_advance_setting
            .filter(
                (advanceSetting: AdvanceSetting) => {
                    return (
                        advanceSetting.outlet_id &&
                        advanceSetting.radius
                    );
                }
            )
            .forEach(
                (advanceSetting: AdvanceSetting) => {
                    if (!advanceSetting.outlet_id) {
                        invalid = true;
                        messages.push('shipping_area_advance_setting_outlet_required');
                    }
                    if (!advanceSetting.radius) {
                        invalid = true;
                        messages.push('shipping_area_advance_setting_radius_required');
                    } else if (advanceSetting.radius <= SHIPPING_ADDRESS_MINIMUM_RADIUS) {
                        invalid = true;
                        messages.push('shipping_area_advance_setting_radius_non_negative_non_zero');
                    } else if (advanceSetting.radius > SHIPPING_ADDRESS_MAXIMUM_RADIUS) {
                        invalid = true;
                        messages.push('shipping_area_advance_setting_radius_maximum');
                    }
                }
            );

        if (messages && messages.length) {
            this.notificationService.error(messages);
        }

        return invalid;
    }

    onSubmit() {
        if (this.beforeSubmit()) {
            return;
        }

        this.settingsService.setSetting(this.settingShippingArea.parse(), this.settingType, this.settingScope, this.settingScopeId)
            .pipe(
                map(
                    (shippingArea: any) => ShippingArea.create(shippingArea)
                ),
                tap(() => this.mapDrawRadius())
            )
            .subscribe(
                (settingShippingArea: any) => {
                    this.settingShippingArea = settingShippingArea;
                    this.cloneSettingShippingArea = this.utilitiesService.cloneDeep(this.settingShippingArea);
                }
            );
    }

    canDeactivate() {
        if (this.isChangedData()) {
            return this.messagesService.unsavedChanges();
        }
        return true;
    }

    isChangedData(): boolean {
        return !this.utilitiesService.isEqual(this.cloneSettingShippingArea, this.settingShippingArea);
    }

    isDisabledAddNew() {
        return this.settingShippingArea &&
            this.settingShippingArea.shipping_area_advance_setting &&
            this.outlets &&
            (this.settingShippingArea.shipping_area_advance_setting.length === this.outlets.length);
    }
}
