import { Component, OnInit } from '@angular/core';
import { UserScope } from 'src/app/shared/entities/user-scope';
import { ScopeService } from 'src/app/shared/services/scope/scope.service';
import { PaymentMethod } from 'src/app/shared/entities/payment-method';
import { PaymentMethodsRepository } from 'src/app/shared/repositories/payment-methods.repository';
import { BrandsRepository } from 'src/app/shared/repositories/brands.repository';
import { ActivatedRoute, Router } from '@angular/router';
import { supportedPaymentRepository } from 'src/app/shared/repositories/supported-payment.repository';
declare var jQuery: any;

@Component({
  selector: 'app-payment-method',
  templateUrl: './payment-method.component.html',
  styleUrls: ['./payment-method.component.scss']
})
export class PaymentMethodComponent implements OnInit {
  public payment_methods: PaymentMethod[] = [];
  public payment_method: PaymentMethod = new PaymentMethod();
  public payment_method_edit: any;
  public payment_value: any;
  public brand = [];
  public supportedPayments = [];
  public listSupported = [];

  userScope: UserScope;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private paymentMethodsRepository: PaymentMethodsRepository,
    private supportedPaymentRepository: supportedPaymentRepository,
    private brandRepository: BrandsRepository,
    private scopeService: ScopeService,
  ) { }

  ngOnInit() {
    this.getListUserScope();
    this.getListPayment();
  }
  getListSupportedPaymentt() {
    this.supportedPaymentRepository.all({ organization_id: -1 })
      .subscribe((res?: any) => {
        this.supportedPayments = res;
        this.payment_method.name = this.supportedPayments[0].name;
        this.payment_method.code = this.supportedPayments[0].code;
      });
  }
  getListUserScope() {
    this.scopeService.getUserScope$()
      .subscribe(
        (res: any) => {
          this.userScope = res;
          if (this.userScope.brand) {
            this.payment_method.brand_id = this.userScope.brand.id;
          }
        }
      );
  }
  addPaymentMethod() {
    for (let i = 0; i < this.payment_methods.length; i++) {
      this.payment_methods[i].isShow = false;
    }
    this.listSupported = null;
    this.getListSupportedPaymentt();
  }
  getListPayment() {
    this.paymentMethodsRepository.all()
      .subscribe(res => {
        this.payment_methods = res;
        this.payment_methods.forEach(_p => {
          _p.isShow = false;
        });
      });
  }
  addNewPayment() {
    // this.payment_value = this.listSupported.map(obj => {
    //   var rObj = {};
    //   rObj[obj.key] = obj.value;
    //   return rObj;
    // });
    this.payment_method.value = JSON.stringify(this.listSupported);
    this.paymentMethodsRepository.save(this.payment_method)
      .subscribe(res => {
        this.closeModal();
        this.getListPayment();
      });
  }
  loadPaymentEdit(_item?: any) {
    for (let i = 0; i < this.payment_methods.length; i++) {
      if (_item === this.payment_methods[i]) {
        _item.isShow = !_item.isShow;
      } else {
        this.payment_methods[i].isShow = false;
      }
    }
    if (_item.isShow == true) {
      this.loadListPaymentEdit(_item.id);
    }
  }
  loadListPaymentEdit(_id) {
    this.paymentMethodsRepository.find(_id).subscribe((res) => {
      this.payment_method_edit = res;
    })
  }
  UpdatePayment() {
    let value_payment = this.payment_method_edit;
    value_payment.value = JSON.stringify(value_payment.value);
    this.paymentMethodsRepository.save(value_payment)
      .subscribe((res) => {
        this.getListPayment();
      });
  }
  onDelete(item) {
    this.paymentMethodsRepository.destroy(item).subscribe(() => {
      this.getListPayment();
    });
  }
  closeModal() {
    (function ($) {
      $(document).ready(function () {
        $('#exampleModal').modal('hide');
      });
    })(jQuery);
  }
  changePayment($event) {
    console.log($event)
    this.supportedPayments.forEach(_i => {
      if (_i.name == $event) {
        this.listSupported = _i.value;
      }
    });
    console.log(this.listSupported)
  }
}
