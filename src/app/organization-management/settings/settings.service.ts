import {Injectable} from '@angular/core';
import {ApiService} from '../../shared/services/api/api.service';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class SettingsService {

    constructor(
        private apiService: ApiService
    ) {
    }

    public getSetting(type: string, scope: string, scopeId: number) {
        const params = this.getScopeQuery(scope, scopeId);
        return this.apiService.get(`admin/settings/${type}`, params)
            .pipe(
                map((res) => res.data),
                map((obj: object) => this.parse(obj))
            );
    }

    public setSetting(data: any, type: string, scope: string, scopeId: number) {
        const params = this.getScopeQuery(scope, scopeId);
        return this.apiService.post(`admin/settings/${type}`, data, params)
            .pipe(
                map((res) => res.data),
                map((obj: object) => this.parse(obj))
            );
    }

    getScopeQuery(scope, scopeId) {
        const params: any = {};
        params.scope = scope;
        params.scope_id = scopeId;
        return params;
    }

    parse(obj) {
        for (const key in obj) {
            const item = obj[key];
            if (
                (
                    typeof item === 'string' &&
                    !item.includes(',')
                ) &&
                !isNaN(parseInt(item, 10))
            ) {
                obj[key] = parseInt(item, 10);
            }
        }
        return obj;
    }
}
