import {Component, OnInit} from '@angular/core';
import {LocalStorage} from 'ngx-webstorage';
import {Scope} from '../../../../shared/types/scopes';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {TranslateService} from '@ngx-translate/core';
import {tap} from 'rxjs/operators';
import {Currency} from '../../../../shared/entities/currency';
import {CurrenciesRepository} from '../../../../shared/repositories/currencies.repository';
import {BaseMonetaryPipe} from '../../../../shared/pipes/base-monetary/base-monetary.pipe';
import * as _ from 'lodash';

@Component({
    selector: 'app-create-currency',
    templateUrl: './create-currency.component.html',
    styleUrls: ['./create-currency.component.scss']
})
export class CreateCurrencyComponent implements OnInit {
    currency: Currency;
    clonedCurrency: Currency;
    supportedCurrencies: Currency[];

    @LocalStorage('scope.brand_id') brandId: number;
    @LocalStorage('scope.current') currentScope: Scope;
    @LocalStorage('currency.is_changed') isChanged: boolean;

    constructor(
        private ngbActiveModal: NgbActiveModal,
        private translateService: TranslateService,
        private currenciesRepository: CurrenciesRepository,
        private baseMonetaryPipe: BaseMonetaryPipe
    ) {

    }

    ngOnInit() {
        this.reset();
    }

    reset() {
        this.currency = new Currency();
        this.clonedCurrency = _.cloneDeep(this.currency);
    }

    yes() {
        this.currenciesRepository.create(this.currency)
            .pipe(
                tap(() => this.ngbActiveModal.close())
            )
            .subscribe();
    }

    cancel() {
        this.ngbActiveModal.dismiss('cancel');
    }

    onChangeCurrency(currencyName: string) {
        this.detectIsChanged();

        const selectedCurrency = this.supportedCurrencies.find((cur: Currency) => cur.name === currencyName);
        if (selectedCurrency) {
            this.currency.code = selectedCurrency.code;
            this.currency.name = selectedCurrency.name;
            this.currency.symbol = selectedCurrency.symbol;
        }
    }

    onChangeThousandsSeparator(val: any) {
        this.detectIsChanged();
    }

    onChangeDecimalPlaces(val: any) {
        this.detectIsChanged();
    }

    onChangeDecimalSeparator(val: any) {
        this.detectIsChanged();
    }

    detectIsChanged() {
        this.isChanged = !(_.isEqual(this.currency, this.clonedCurrency));
    }

    transformPreview() {
        const DOT = '.';
        const COMMA = ',';

        if (
            this.currency.decimal_places === null ||
            !this.currency.decimal_separator ||
            !this.currency.thousands_separator ||
            (this.currency.decimal_separator !== DOT && this.currency.decimal_separator !== COMMA) ||
            (this.currency.thousands_separator !== DOT && this.currency.thousands_separator !== COMMA) ||
            this.currency.decimal_separator === this.currency.thousands_separator
        ) {
            return;
        }

        return this.baseMonetaryPipe.transform(123456789.0, this.currency);
    }
}
