import {Component, Input, OnInit} from '@angular/core';
import {Currency} from '../../../../shared/entities/currency';
import {LocalStorage, SessionStorage} from 'ngx-webstorage';
import {Scope} from '../../../../shared/types/scopes';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {CurrenciesRepository} from '../../../../shared/repositories/currencies.repository';
import {BaseMonetaryPipe} from '../../../../shared/pipes/base-monetary/base-monetary.pipe';
import {tap} from 'rxjs/operators';
import * as _ from 'lodash';

@Component({
    selector: 'app-edit-currency',
    templateUrl: './edit-currency.component.html',
    styleUrls: ['./edit-currency.component.scss']
})
export class EditCurrencyComponent implements OnInit {
    @Input() currency: Currency = new Currency();
    clonedCurrency: Currency;
    supportedCurrencies: Currency[];

    @LocalStorage('scope.brand_id') brandId: number;
    @LocalStorage('scope.current') currentScope: Scope;

    @SessionStorage('currency.is_changed') isChanged: boolean;

    constructor(
        private ngbActiveModal: NgbActiveModal,
        private currenciesRepository: CurrenciesRepository,
        private baseMonetaryPipe: BaseMonetaryPipe
    ) {

    }

    ngOnInit() {
        this.clonedCurrency = _.cloneDeep(this.currency);
    }

    reset() {
        if (!(_.isEqual(this.currency, this.clonedCurrency))) {
            this.currency = _.cloneDeep(this.clonedCurrency);
        }
    }

    yes() {
        this.currenciesRepository.update(this.currency.id, this.currency)
            .pipe(
                tap(() => this.ngbActiveModal.close())
            )
            .subscribe();
    }

    cancel() {
        this.ngbActiveModal.dismiss('cancel');
    }

    onChangeCurrency(currencyName: string) {
        this.detectIsChanged();

        const selectedCurrency = this.supportedCurrencies.find((cur: Currency) => cur.name === currencyName);
        if (selectedCurrency) {
            this.currency.code = selectedCurrency.code;
            this.currency.name = selectedCurrency.name;
            this.currency.symbol = selectedCurrency.symbol;
        }
    }

    onChangeThousandsSeparator(val: any) {
        this.detectIsChanged();
    }

    onChangeDecimalPlaces(val: any) {
        this.detectIsChanged();
    }

    onChangeDecimalSeparator(val: any) {
        this.detectIsChanged();
    }

    transformPreview() {
        const DOT = '.';
        const COMMA = ',';
        const EXAMPLE = 123456789;

        if (
            this.currency.decimal_places === null ||
            !this.currency.decimal_separator ||
            !this.currency.thousands_separator ||
            (this.currency.decimal_separator !== DOT && this.currency.decimal_separator !== COMMA) ||
            (this.currency.thousands_separator !== DOT && this.currency.thousands_separator !== COMMA) ||
            this.currency.decimal_separator === this.currency.thousands_separator
        ) {
            return EXAMPLE;
        }

        return this.baseMonetaryPipe.transform(EXAMPLE, this.currency);
    }

    detectIsChanged() {
        this.isChanged = !(_.isEqual(this.currency, this.clonedCurrency));
    }
}
