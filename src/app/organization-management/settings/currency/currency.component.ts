import { Component, OnDestroy, OnInit } from '@angular/core';
import { CxiGridConfig } from '../../../shared/components/cxi-grid/cxi-grid-config';
import { CxiGridColumn } from '../../../shared/components/cxi-grid/cxi-grid-column';
import { from, of, Subscription, throwError } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { catchError, filter, switchMap, tap } from 'rxjs/operators';
import { CurrenciesRepository } from '../../../shared/repositories/currencies.repository';
import { Currency } from '../../../shared/entities/currency';
import { CreateCurrencyComponent } from './create-currency/create-currency.component';
import { EditCurrencyComponent } from './edit-currency/edit-currency.component';
import { SupportedCurrenciesRepository } from '../../../shared/repositories/supported-currencies.repository';
import { SessionStorage } from 'ngx-webstorage';
import { NotificationService } from '../../../shared/services/noitification/notification.service';
import { MessagesService } from '../../../messages/messages.service';
import * as _ from 'lodash';
import { ColumnType } from '../../../shared/types/column-type';

@Component({
    selector: 'app-currency',
    templateUrl: './currency.component.html',
    styleUrls: ['./currency.component.scss']
})
export class CurrencyComponent implements OnInit, OnDestroy {

    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        pagingServer: true,
        repository: this.currenciesRepository,
        actions: {
            onActivate: (currency: Currency) => {
                this.onActivate(currency);
            },
            onDeactivate: (currency: Currency) => {
                this.onDeactivate(currency);
            },
            onEdit: (currency: Currency) => {
                this.onEdit(currency);
            },
            // onDelete: (currency: Currency) => {
            //     this.onDelete(currency);
            // },
        }
    });
    cxiGridColumns = CxiGridColumn.from([
        { title: 'ISO code', name: 'code' },
        { title: 'Name', name: 'name' },
        { title: 'Symbol', name: 'symbol' },
        { title: 'Default Currency', name: 'is_default', type: ColumnType.Switch, onSwitch: (row: any) => this.onSwitch(row) },
    ]);

    subscriptions: Subscription[] = [];
    availableSupportedCurrencies: Currency[];

    @SessionStorage('currencies') currencies: Currency[];
    @SessionStorage('supported-currencies') supportedCurrencies: Currency[];
    @SessionStorage('currency.is_changed') isChanged: boolean;


    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private ngbModal: NgbModal,
        private messagesService: MessagesService,
        private currenciesRepository: CurrenciesRepository,
        private supportedCurrenciesRepository: SupportedCurrenciesRepository,
        private notificationService: NotificationService
    ) {
        const sub = this.currenciesRepository.created$
            .pipe(
                tap((currency: Currency) => this.autoSetDefault(currency))
            )
            .subscribe();
        this.subscriptions.push(sub);
    }

    ngOnInit() {
        this.getAvailableSupportedCurrencies();
    }

    getAvailableSupportedCurrencies() {
        this.supportedCurrenciesRepository.getAvailable(this.currencies)
            .pipe(
                tap(
                    (availableSupportedCurrencies: Currency[]) => this.availableSupportedCurrencies = availableSupportedCurrencies
                )
            )
            .subscribe();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    onEdit(currency: Currency) {
        this.supportedCurrenciesRepository.getAvailable(this.currencies, currency)
            .pipe(
                switchMap(
                    (availableCurrencies: Currency[]) => {
                        if (!availableCurrencies ||
                            availableCurrencies.length === 0) {
                            this.notificationService.error([`There is no available supported currency`]);
                            return of();
                        }
                        return this.openEdit$(currency);
                    }
                )
            )
            .subscribe();
    }

    openEdit$(currency: Currency) {
        this.isChanged = false;
        const modalRef = this.ngbModal.open(EditCurrencyComponent, {
            centered: true,
            windowClass: 'currencies',
            beforeDismiss: () => {
                if (this.isChanged) {
                    const obsUnsavedChange = this.messagesService.unsavedChanges();
                    return obsUnsavedChange.toPromise();
                } else {
                    return true;
                }
            }
        });
        modalRef.componentInstance.supportedCurrencies = this.availableSupportedCurrencies;
        modalRef.componentInstance.currency = _.cloneDeep(currency);
        return from(modalRef.result)
            .pipe(
                catchError((e) => throwError(e)),
                filter((isConfirmed: boolean) => isConfirmed)
            );
    }

    onDelete(currency: Currency) {
        if (currency.is_default) {
            const msg = `It must be at least one default currency`;
            const options = {
                buttons: [
                    { label: 'Close' }
                ]
            };
            this.messagesService.statusConfirmation(msg, options);
            return;
        }

        this.currenciesRepository.destroy(currency)
            .subscribe();
    }

    addNewCurrency() {
        this.supportedCurrenciesRepository.getAvailable(this.currencies)
            .pipe(
                switchMap(
                    (availableSupportedCurrencies: Currency[]) => {
                        if (!availableSupportedCurrencies || availableSupportedCurrencies.length === 0) {
                            this.notificationService.error([`There is no available supported currency`]);
                            return of();
                        }
                        return this.openAddNewCurrency$(availableSupportedCurrencies);
                    }
                )
            )
            .subscribe();
    }

    openAddNewCurrency$(availableSupportedCurrencies: Currency[]) {
        this.isChanged = false;
        const modalRef = this.ngbModal.open(CreateCurrencyComponent, {
            centered: true,
            windowClass: 'currencies',
            beforeDismiss: () => {
                if (this.isChanged) {
                    const obsUnsavedChange = this.messagesService.unsavedChanges();
                    return obsUnsavedChange.toPromise();
                } else {
                    return true;
                }
            }
        });
        modalRef.componentInstance.supportedCurrencies = availableSupportedCurrencies;
        return from(modalRef.result)
            .pipe(
                catchError((e) => throwError(e)),
                filter((isConfirmed: boolean) => isConfirmed)
            );
    }

    onSwitch(currency: Currency) {
        if (currency.status == 1) {
            const defaultCurrency = this.currencies.find(cur => cur.code === currency.code);
            if (defaultCurrency) {
                if (!defaultCurrency.is_default) {
                    this.setDefaultCurrency(defaultCurrency);
                } else {
                    const msg = `It must be at least one default currency`;
                    const options = {
                        buttons: [
                            {
                                label: 'Close'
                            }
                        ]
                    };
                    this.messagesService.statusConfirmation(msg, options)
                        .subscribe();
                }
            }
        } else {
            const msg = `The deactivated currency can not be set as default currency`;
            const options = {
                buttons: [
                    {
                        label: 'Close'
                    }
                ]
            };
            this.messagesService.statusConfirmation(msg, options)
                .subscribe();
        }
    }

    setDefaultCurrency(currency: Currency) {
        currency.is_default = true;
        this.currenciesRepository.update(currency.id, currency)
            .pipe(
                tap(
                    (updatedCurrency: Currency) => this.triggerCheckDuplicateDefaultCurrency(updatedCurrency)
                )
            )
            .subscribe();
    }

    triggerCheckDuplicateDefaultCurrency(defaultCurrency: Currency) {
        this.currencies
            .filter(cur => cur.is_default && cur.id !== defaultCurrency.id)
            .forEach(
                (otherCurrency: Currency) => this.unsetDefaultCurrency(otherCurrency)
            );
    }

    unsetDefaultCurrency(currency: Currency) {
        if (currency.is_default) {
            currency.is_default = false;
            this.currenciesRepository.update(currency.id, currency)
                .subscribe();
        }
    }

    autoSetDefault(createdCurrency: Currency) {
        if (this.currencies &&
            this.currencies.length === 1 &&
            createdCurrency) {
            this.setDefaultCurrency(createdCurrency);
        }
    }
    onActivate(currency: Currency) {
        this.currenciesRepository.activate(currency).subscribe();
    }
    onDeactivate(currency?: any) {
        if (currency.is_default !== 1) {
            this.currenciesRepository.deactivate(currency).subscribe();
        } else {
            const msg = `It must be at least one default currency`;
            const options = {
                buttons: [
                    {
                        label: 'Close'
                    }
                ]
            };
            this.messagesService.statusConfirmation(msg, options)
                .subscribe();
        }
    }
}
