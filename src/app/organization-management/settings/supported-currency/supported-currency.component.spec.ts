import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupportedCurrencyComponent } from './supported-currency.component';

describe('SupportedCurrencyComponent', () => {
  let component: SupportedCurrencyComponent;
  let fixture: ComponentFixture<SupportedCurrencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupportedCurrencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupportedCurrencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
