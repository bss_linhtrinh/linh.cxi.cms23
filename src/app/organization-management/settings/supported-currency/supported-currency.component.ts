import {Component, OnDestroy, OnInit} from '@angular/core';
import {CxiGridConfig} from '../../../shared/components/cxi-grid/cxi-grid-config';
import {Currency} from '../../../shared/entities/currency';
import {CxiGridColumn} from '../../../shared/components/cxi-grid/cxi-grid-column';
import {from, Subscription, throwError} from 'rxjs';
import {SessionStorage} from 'ngx-webstorage';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MessagesService} from '../../../messages/messages.service';
import {SupportedCurrenciesRepository} from '../../../shared/repositories/supported-currencies.repository';
import {catchError, filter} from 'rxjs/operators';
import {CreateSupportedCurrencyComponent} from './create-supported-currency/create-supported-currency.component';
import {EditSupportedCurrencyComponent} from './edit-supported-currency/edit-supported-currency.component';
import * as _ from 'lodash';

@Component({
    selector: 'app-supported-currency',
    templateUrl: './supported-currency.component.html',
    styleUrls: ['./supported-currency.component.scss']
})
export class SupportedCurrencyComponent implements OnInit, OnDestroy {
    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        pagingServer: true,
        repository: this.supportedCurrenciesRepository,
        actions: {
            onEdit: (currency: Currency) => {
                this.onEdit(currency);
            },
            onDelete: (currency: Currency) => {
                this.onDelete(currency);
            },
        }
    });
    cxiGridColumns = CxiGridColumn.from([
        {title: 'ISO code', name: 'code'},
        {title: 'Name', name: 'name'},
        {title: 'Symbol', name: 'symbol'},
    ]);

    subscriptions: Subscription[] = [];

    @SessionStorage('currencies') currencies: Currency[];
    @SessionStorage('supported-currencies') supportedCurrencies: Currency[];
    @SessionStorage('currency.is_changed') isChanged: boolean;

    constructor(
        private ngbModal: NgbModal,
        private messagesService: MessagesService,
        private supportedCurrenciesRepository: SupportedCurrenciesRepository,
    ) {
    }

    ngOnInit() {
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    onEdit(currency: Currency) {
        this.isChanged = false;
        const modalRef = this.ngbModal.open(EditSupportedCurrencyComponent, {
            centered: true,
            windowClass: 'currencies',
            beforeDismiss: () => {
                if (this.isChanged) {
                    const obsUnsavedChange = this.messagesService.unsavedChanges();
                    return obsUnsavedChange.toPromise();
                } else {
                    return true;
                }
            }
        });
        modalRef.componentInstance.currency = _.cloneDeep(currency);

        from(modalRef.result)
            .pipe(
                catchError((e) => throwError(e)),
                filter((isConfirmed: boolean) => isConfirmed)
            )
            .subscribe();
    }


    onDelete(currency: Currency) {
        this.supportedCurrenciesRepository.destroy(currency)
            .subscribe();
    }

    addNewCurrency() {
        this.isChanged = false;
        const modalRef = this.ngbModal.open(CreateSupportedCurrencyComponent, {
            centered: true,
            windowClass: 'currencies',
            beforeDismiss: () => {
                if (this.isChanged) {
                    const obsUnsavedChange = this.messagesService.unsavedChanges();
                    return obsUnsavedChange.toPromise();
                } else {
                    return true;
                }
            }
        });

        from(modalRef.result)
            .pipe(
                catchError((e) => throwError(e)),
                filter((isConfirmed: boolean) => isConfirmed)
            )
            .subscribe();
    }
}
