import {Component, OnInit} from '@angular/core';
import {Currency} from '../../../../shared/entities/currency';
import {SessionStorage} from 'ngx-webstorage';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import * as _ from 'lodash';
import {tap} from 'rxjs/operators';
import {SupportedCurrenciesRepository} from '../../../../shared/repositories/supported-currencies.repository';
import {Form, NgForm} from '@angular/forms';

@Component({
    selector: 'app-create-supported-currency',
    templateUrl: './create-supported-currency.component.html',
    styleUrls: ['./create-supported-currency.component.scss']
})
export class CreateSupportedCurrencyComponent implements OnInit {
    currency: Currency;
    clonedCurrency: Currency;
    supportedCurrencies: Currency[];

    @SessionStorage('currency.is_changed') isChanged: boolean;

    constructor(
        private ngbActiveModal: NgbActiveModal,
        private supportedCurrenciesRepository: SupportedCurrenciesRepository
    ) {
    }

    ngOnInit() {
        this.reset();
    }

    reset() {
        this.currency = new Currency();
        this.clonedCurrency = _.cloneDeep(this.currency);
    }

    yes() {
        this.supportedCurrenciesRepository.create(this.currency)
            .pipe(
                tap(() => this.ngbActiveModal.close())
            )
            .subscribe();
    }

    cancel() {
        this.ngbActiveModal.dismiss('cancel');
    }

    onChangeCurrencyCode() {
        this.detectIsChanged();
    }

    onChangeCurrencyName() {
        this.detectIsChanged();
    }

    onChangeCurrencySymbol() {
        this.detectIsChanged();
    }

    detectIsChanged() {
        this.isChanged = !(_.isEqual(this.currency, this.clonedCurrency));
    }
}
