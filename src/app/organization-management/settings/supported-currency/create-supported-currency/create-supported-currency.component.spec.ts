import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSupportedCurrencyComponent } from './create-supported-currency.component';

describe('CreateSupportedCurrencyComponent', () => {
  let component: CreateSupportedCurrencyComponent;
  let fixture: ComponentFixture<CreateSupportedCurrencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSupportedCurrencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSupportedCurrencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
