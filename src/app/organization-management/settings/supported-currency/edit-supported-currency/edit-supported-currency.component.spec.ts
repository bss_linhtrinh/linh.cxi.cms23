import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSupportedCurrencyComponent } from './edit-supported-currency.component';

describe('EditSupportedCurrencyComponent', () => {
  let component: EditSupportedCurrencyComponent;
  let fixture: ComponentFixture<EditSupportedCurrencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSupportedCurrencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSupportedCurrencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
