import {Component, Input, OnInit} from '@angular/core';
import {Currency} from '../../../../shared/entities/currency';
import {SessionStorage} from 'ngx-webstorage';
import {Scope} from '../../../../shared/types/scopes';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {TranslateService} from '@ngx-translate/core';
import {CurrenciesRepository} from '../../../../shared/repositories/currencies.repository';
import {BaseMonetaryPipe} from '../../../../shared/pipes/base-monetary/base-monetary.pipe';
import * as _ from 'lodash';
import {tap} from 'rxjs/operators';
import {SupportedCurrenciesRepository} from '../../../../shared/repositories/supported-currencies.repository';

@Component({
  selector: 'app-edit-supported-currency',
  templateUrl: './edit-supported-currency.component.html',
  styleUrls: ['./edit-supported-currency.component.scss']
})
export class EditSupportedCurrencyComponent implements OnInit {
    @Input() currency: Currency = new Currency();
    clonedCurrency: Currency;
    supportedCurrencies: Currency[];

    @SessionStorage('currency.is_changed') isChanged: boolean;

    constructor(
        private ngbActiveModal: NgbActiveModal,
        private supportedCurrenciesRepository: SupportedCurrenciesRepository,
    ) {
    }

    ngOnInit() {
        this.clonedCurrency = _.cloneDeep(this.currency);
    }

    reset() {
        if (!(_.isEqual(this.currency, this.clonedCurrency))) {
            this.currency = _.cloneDeep(this.clonedCurrency);
        }
    }

    yes() {
        this.supportedCurrenciesRepository.update(this.currency.id, this.currency)
            .pipe(
                tap(() => this.ngbActiveModal.close())
            )
            .subscribe();
    }

    cancel() {
        this.ngbActiveModal.dismiss('cancel');
    }

    onChangeCurrencyCode() {
        this.detectIsChanged();
    }

    onChangeCurrencyName() {
        this.detectIsChanged();
    }

    onChangeCurrencySymbol() {
        this.detectIsChanged();
    }

    detectIsChanged() {
        this.isChanged = !(_.isEqual(this.currency, this.clonedCurrency));
    }
}
