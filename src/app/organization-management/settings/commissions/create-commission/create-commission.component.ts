import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Commission} from '../../../../shared/entities/commission';
import {Brand} from '../../../../shared/entities/brand';
import {BrandsRepository} from '../../../../shared/repositories/brands.repository';
import {fromArray} from 'rxjs/internal/observable/fromArray';
import {map, switchMap, tap, toArray} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {Outlet} from '../../../../shared/entities/outlets';
import {OutletsRepository} from '../../../../shared/repositories/outlets.repository';
import {of} from 'rxjs';
import {CommissionOutlet} from '../../../../shared/entities/commission-outlet';
import {CommissionsRepository} from '../../../../shared/repositories/commissions.repository';
import {LocalStorage} from 'ngx-webstorage';
import {Scope} from '../../../../shared/types/scopes';
import {NotificationService} from '../../../../shared/services/noitification/notification.service';

@Component({
    selector: 'app-setting-commission-create-commission',
    templateUrl: './create-commission.component.html',
    styleUrls: ['./create-commission.component.scss']
})
export class CreateCommissionComponent implements OnInit {
    commission: Commission = new Commission();
    brands: Brand[] = [];
    outlets: Outlet[] = [];
    productApplicationScopes = [
        {name: 'All', value: 1},
        {name: 'Specific outlet', value: 0}
    ];
    uploadFile: File;
    isHideSelectBrand: boolean;
    isHideSelectOutlet: boolean;

    @LocalStorage('scope.brand_id') brandId: number;
    @LocalStorage('scope.current') currentScope: Scope;

    constructor(
        private ngbActiveModal: NgbActiveModal,
        private brandsRepository: BrandsRepository,
        private outletsRepository: OutletsRepository,
        private translateService: TranslateService,
        private commissionsRepository: CommissionsRepository,
        private notificationService: NotificationService
    ) {

    }

    ngOnInit() {
        this.getApplicationScope();
    }

    getApplicationScope() {
        switch (this.currentScope) {
            case Scope.Organization:
                this.isHideSelectBrand = false;
                this.isHideSelectOutlet = false;
                this.getBrands();
                break;

            case Scope.Brand:
                this.isHideSelectBrand = true;
                this.isHideSelectOutlet = false;
                if (this.brandId) {
                    this.commission.brand_id = this.brandId;
                }
                this.getOutlets();
                break;

            case Scope.Outlet:
                this.isHideSelectBrand = true;
                this.isHideSelectOutlet = true;
                if (this.brandId) {
                    this.commission.brand_id = this.brandId;
                }
                break;

            default:
                break;
        }
    }

    getBrands() {
        this.brandsRepository.all()
            .pipe(
                tap(
                    (brands: Brand[]) => {
                        this.brands = brands;
                        if (this.brands && this.brands.length && this.brands[0] && this.brands[0].id) {
                            this.getOutletsByBrand(this.brands[0].id);
                        }
                    }
                )
            )
            .subscribe(
            );
    }

    getOutletsByBrand(brandId: number) {
        const params = {brand_id: brandId};
        this.outletsRepository.all(params)
            .pipe(
                tap((outlets: Outlet[]) => this.outlets = outlets),
                tap(() => this.checkExistSelectedOutlet())
            )
            .subscribe(
            );
    }

    getOutlets() {
        this.outletsRepository.all()
            .pipe(
                tap((outlets: Outlet[]) => this.outlets = outlets),
                tap(() => this.checkExistSelectedOutlet())
            )
            .subscribe(
            );
    }

    translate() {
        // translate productApplicationScopes
        fromArray(this.productApplicationScopes)
            .pipe(
                switchMap(
                    (option: any) => {
                        return this.translateService.get('scope.' + option.name)
                            .pipe(
                                map(
                                    (translatedName) => {
                                        return {name: translatedName, value: option.value};
                                    }
                                )
                            );
                    }
                ),
                toArray()
            )
            .subscribe(
                (productApplicationScopes: any[]) => this.productApplicationScopes = productApplicationScopes
            );
    }

    reset() {
        this.commission = new Commission();
        this.outlets.forEach(outlet => {
            outlet.disabled = false;
        });
        this.uploadFile = null;
    }

    yes() {
        if (!this.commission.image) {
            this.notificationService.error([`Image is required for Commission`]);
            return;
        }
        if (!this.commission.all_outlets) {
            if (
                !this.commission.outlets ||
                this.commission.outlets.length === 0 ||
                !this.commission.outlets[0].outlet_id ||
                !this.commission.outlets[0].percentage
            ) {
                this.notificationService.error([`User must choose at least one outlets and its commission value.`]);
                return;
            }
        } else {
            if (
                !this.commission.all_outlets_percentage &&
                !this.commission.all_outlets_amount
            ) {
                this.notificationService.error([`The all outlets radius value is required`]);
                return;
            }
        }

        this.commissionsRepository.create(this.commission)
            .pipe(
                tap(() => this.ngbActiveModal.close())
            )
            .subscribe();
    }

    cancel() {
        this.ngbActiveModal.dismiss('cancel');
    }

    onFileChanged(file: any) {
        this.uploadFile = file;
    }

    getLabelAddress(index: number) {
        return of(index)
            .pipe(
                switchMap(
                    (i: number) => {
                        return i ? of(null) : this.translateService.get(`scope.Address`);
                    }
                )
            );
    }

    getLabelValue(index: number) {
        return of(index)
            .pipe(
                map((i: number) => i ? null : ''),
            );
    }

    onRemoveOutlet(commissionOutlet: CommissionOutlet) {
        const index = this.commission.outlets.findIndex(outlet => outlet.outlet_id === commissionOutlet.outlet_id);
        if (index !== -1) {
            this.commission.outlets.splice(index, 1);
        }

        this.checkEmpty();
        this.checkDisabled();
    }

    addNewOutlet() {
        if (!this.isFull()) {
            this.commission.outlets.push(new CommissionOutlet());
        }
    }

    onChangeBrand(brandId: number) {
        this.getOutletsByBrand(brandId);
    }

    onChangeOutlet() {
        this.checkDisabled();
    }

    checkExistSelectedOutlet() {
        if (this.commission &&
            this.commission.outlets &&
            this.outlets &&
            this.outlets.length) {
            for (const index in this.commission.outlets) {
                const commissionOutlet = this.commission.outlets[index];
                if (commissionOutlet) {
                    if (this.outlets.findIndex(outlet => outlet.id === commissionOutlet.outlet_id) === -1) {
                        this.commission.outlets.splice(parseInt(index, 10), 1);
                    }
                }
            }
        }
        this.checkEmpty();
        this.checkDisabled();
    }

    checkDisabled() {
        this.outlets.forEach(outlet => {
            outlet.disabled = (this.commission.outlets.findIndex(o => o.outlet_id === outlet.id) !== -1);
        });
    }

    checkEmpty() {
        if (this.commission.outlets && this.commission.outlets.length === 0) {
            this.commission.outlets.push(new CommissionOutlet());
        }
    }

    isFull() {
        return (this.commission.outlets && this.outlets && this.commission.outlets.length === this.outlets.length);
    }

    isDisableAddOutletButton(): boolean {
        return this.commission && this.commission.outlets &&
            this.outlets &&
            (
                this.commission.outlets
                    .filter(
                        commissionOutlet => commissionOutlet.outlet_id
                    )
                    .length === this.outlets.length
            );
    }
}
