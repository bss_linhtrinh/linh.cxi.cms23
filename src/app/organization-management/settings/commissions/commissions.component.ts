import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CxiGridConfig} from '../../../shared/components/cxi-grid/cxi-grid-config';
import {CxiGridColumn} from '../../../shared/components/cxi-grid/cxi-grid-column';
import {CommissionsRepository} from '../../../shared/repositories/commissions.repository';
import {Commission} from '../../../shared/entities/commission';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CreateCommissionComponent} from './create-commission/create-commission.component';
import {from, Subscription, throwError} from 'rxjs';
import {catchError, filter, tap} from 'rxjs/operators';
import {ColumnFormat} from '../../../shared/types/column-format';
import {EditCommissionComponent} from './edit-commission/edit-commission.component';
import * as _ from 'lodash';

@Component({
    selector: 'app-commissions',
    templateUrl: './commissions.component.html',
    styleUrls: ['./commissions.component.scss']
})
export class CommissionsComponent implements OnInit, OnDestroy {

    cxiGridConfig = CxiGridConfig.from({
        paging: true,
        sorting: true,
        filtering: true,
        selecting: true,
        className: false,
        pagingServer: true,
        repository: this.commissionsRepository,
        actions: {
            onEdit: (commission: Commission) => {
                this.onEdit(commission);
            },
            onActivate: (commission: Commission) => {
                this.onActivate(commission);
            },
            onDeactivate: (commission: Commission) => {
                this.onDeactivate(commission);
            },
            onDelete: (commission: Commission) => {
                this.onDelete(commission);
            },
        }
    });
    cxiGridColumns = CxiGridColumn.from([
        {title: 'Source', name: 'name', avatar: 'image'},
        {title: 'Commission', name: 'commission', format: ColumnFormat.Percentage},
        {title: 'Application Scope', name: 'application_scope'},
    ]);

    subscriptions: Subscription[] = [];

    constructor(
        private commissionsRepository: CommissionsRepository,
        private router: Router,
        private route: ActivatedRoute,
        private ngbModal: NgbModal
    ) {
        const subscription = this.commissionsRepository.created$
            .pipe(
                tap((commission: Commission) => {
                    this.router.navigate(['/settings/commissions']);
                })
            )
            .subscribe();
        this.subscriptions.push(subscription);
    }

    ngOnInit() {
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    onEdit(commission: Commission) {
        const modalRef = this.ngbModal.open(EditCommissionComponent, {centered: true, windowClass: 'commission'});
        modalRef.componentInstance.commission = _.cloneDeep(commission); // clone for edit
        return from(modalRef.result)
            .pipe(
                catchError((e) => throwError(e)),
                filter((isConfirmed: boolean) => isConfirmed)
            );
    }

    onActivate(commission: Commission) {
        this.commissionsRepository.activate(commission)
            .subscribe();
    }

    onDeactivate(commission: Commission) {
        this.commissionsRepository.deactivate(commission)
            .subscribe();
    }

    onDelete(commission: Commission) {
        this.commissionsRepository.destroy(commission)
            .subscribe();
    }

    addNewCommission() {
        const modalRef = this.ngbModal.open(CreateCommissionComponent, {centered: true, windowClass: 'commission'});
        // modalRef.componentInstance.message = message;
        return from(modalRef.result)
            .pipe(
                catchError((e) => throwError(e)),
                filter((isConfirmed: boolean) => isConfirmed)
            );
    }
}
