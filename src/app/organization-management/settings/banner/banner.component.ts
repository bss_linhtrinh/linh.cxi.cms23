import {Component, OnInit} from '@angular/core';
import {Banner} from '../../../shared/entities/banner';
import {ShippingArea} from '../shipping-area/shipping-area';
import {map, tap} from 'rxjs/operators';
import {SettingsService} from '../settings.service';
import {LocalStorage, SessionStorage} from 'ngx-webstorage';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';
import {MessagesService} from '../../../messages/messages.service';

@Component({
    selector: 'app-banner',
    templateUrl: './banner.component.html',
    styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {

    banner: Banner = new Banner();
    cloneBanner: Banner;

    settingType = 'banner';
    settingScope = 'brands';
    settingScopeId: number;

    @LocalStorage('scope.brand_id') brandId: number;

    constructor(
        private settingsService: SettingsService,
        private utilitiesService: UtilitiesService,
        private messagesService: MessagesService
    ) {
    }

    ngOnInit() {
        this.settingType = 'banner';
        this.settingScope = 'brands';
        this.settingScopeId = this.brandId;
        this.cloneBanner = this.utilitiesService.cloneDeep(this.banner);

        this.getBanner();
    }

    getBanner() {

        this.settingsService.getSetting(this.settingType, this.settingScope, this.settingScopeId)
            .pipe(
                map((banner: any) => Banner.create(banner))
            )
            .subscribe(
                (banner: Banner) => {
                    this.banner = banner;
                    this.cloneBanner = this.utilitiesService.cloneDeep(this.banner);
                }
            );
    }

    save() {
        const dataBanner = this.banner.parse();

        this.settingsService.setSetting(dataBanner, this.settingType, this.settingScope, this.settingScopeId)
            .pipe(
                map((banner: any) => Banner.create(banner)),
            )
            .subscribe(
                (banner: Banner) => {
                    this.banner = banner;
                    this.cloneBanner = this.utilitiesService.cloneDeep(this.banner);
                }
            );
    }

    isChangedData(): boolean {
        return !this.utilitiesService.isEqual(this.cloneBanner, this.banner);
    }

    canDeactivate() {
        if (this.isChangedData()) {
            return this.messagesService.unsavedChanges();
        }
        return true;
    }
}
