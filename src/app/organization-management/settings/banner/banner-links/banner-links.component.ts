import {Component, Inject, Input, OnInit, Optional, ViewChild} from '@angular/core';
import {BannerLink} from '../../../../shared/entities/banner-link';
import {ElementBase} from '../../../../shared/custom-form/element-base/element-base';
import {NG_ASYNC_VALIDATORS, NG_VALIDATORS, NG_VALUE_ACCESSOR, NgModel} from '@angular/forms';

@Component({
    selector: 'app-banner-links',
    templateUrl: './banner-links.component.html',
    styleUrls: ['./banner-links.component.scss'],
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: BannerLinksComponent, multi: true}
    ]
})
export class BannerLinksComponent extends ElementBase<BannerLink[]> implements OnInit {
    @Input() name: string;

    @ViewChild(NgModel, {static: true}) model: NgModel;

    constructor(
        @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
        @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
    ) {
        super(validators, asyncValidators);
    }

    ngOnInit() {
    }

    onFileChanged(file: File) {
    }

    onRemoveBanner(index: number) {
        this.value.splice(index, 1);
        this.checkEmpty();
    }

    addNewBanner() {
        if (this.value &&
            this.value instanceof Array) {
            this.value.push(new BannerLink());
        }
    }

    checkEmpty() {
        if (this.value && this.value.length === 0) {
            this.addNewBanner();
        }
    }
}
