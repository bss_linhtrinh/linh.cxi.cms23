import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CheckPermissionsGuard} from 'src/app/shared/guards/check-permissions/check-permissions.guard';
import {SettingsComponent} from './settings/settings.component';
import {PaymentMethodComponent} from './payment-method/payment-method.component';
import {ShippingAreaComponent} from './shipping-area/shipping-area.component';
import {NotificationComponent} from './notification/notification.component';
import {RegionLanguageComponent} from './region-language/region-language.component';
import {OrderComponent} from './order/order.component';
import {CanDeactivateGuard} from '../../shared/guards/can-deactivate/can-deactivate.guard';
import {Commission} from '../../shared/entities/commission';
import {CommissionsComponent} from './commissions/commissions.component';
import {BannerComponent} from './banner/banner.component';
import {CurrencyComponent} from './currency/currency.component';
import {SupportedCurrencyComponent} from './supported-currency/supported-currency.component';

const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        children: [
            {path: 'profile', component: SettingsComponent, data: {breadcrumb: 'Profile'}, canDeactivate: [CanDeactivateGuard]},
            {path: 'payment-method', component: PaymentMethodComponent, data: {breadcrumb: 'Payment Method'}},
            {
                path: 'shipping-area',
                component: ShippingAreaComponent,
                data: {breadcrumb: 'Shipping Area'},
                canDeactivate: [CanDeactivateGuard]
            },
            {path: 'notification', component: NotificationComponent, data: {breadcrumb: 'Notification'}},
            {
                path: 'region-language',
                component: RegionLanguageComponent,
                data: {breadcrumb: 'Region & Language'},
                canDeactivate: [CanDeactivateGuard]
            },
            {
                path: 'order',
                component: OrderComponent,
                data: {breadcrumb: 'Order'},
                canDeactivate: [CanDeactivateGuard]
            },
            {path: 'commissions', component: CommissionsComponent, data: {breadcrumb: 'Commission'}},
            {path: 'banner', component: BannerComponent, data: {breadcrumb: 'Banner'}},
            {path: 'currencies', component: CurrencyComponent, data: {breadcrumb: 'Currency'}},
            {path: 'supported-currencies', component: SupportedCurrencyComponent, data: {breadcrumb: 'Supported Currency'}},
            {path: '', redirectTo: 'settings'}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SettingsRoutingModule {
}
