import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListActivityLogComponent } from './list-activity-log.component';

describe('ListActivityLogComponent', () => {
  let component: ListActivityLogComponent;
  let fixture: ComponentFixture<ListActivityLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListActivityLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListActivityLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
