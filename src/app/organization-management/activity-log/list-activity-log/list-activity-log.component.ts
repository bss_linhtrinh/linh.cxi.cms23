import {Component, OnInit} from '@angular/core';
import {ActivityLogRepository} from '../../../shared/repositories/activity-log.repository';
import {LocalStorage} from 'ngx-webstorage';
import {UtilitiesService} from '../../../shared/services/utilities/utilities.service';

@Component({
    selector: 'app-list-activity-log',
    templateUrl: './list-activity-log.component.html',
    styleUrls: ['./list-activity-log.component.scss']
})
export class ListActivityLogComponent implements OnInit {

    @LocalStorage('dashboard.timeSelected') timeSelected: string;
    @LocalStorage('dashboard.dateStart') dateStart: string;
    @LocalStorage('dashboard.dateEnd') dateEnd: string;
    baseTableConfig = {
        paging: true,
        sorting: true,
        filtering: false,
        selecting: false,
        pagingServer: true,
        repository: this.activityLogRepository

    };
    baseTableColumns = [
        {title: 'date', name: 'date', format: 'date'},
        {title: 'type', name: 'type'},
        {title: 'logger', name: 'logger'},
        {title: 'value', name: 'value'},
    ];

    constructor(private activityLogRepository: ActivityLogRepository,
                private utilitiesService: UtilitiesService) {
    }

    ngOnInit() {
        if (!this.timeSelected) {
            this.timeSelected = 'year';
        }
        if (!this.dateEnd) {
            this.dateEnd = this.utilitiesService.parseDate(new Date());
        }
        if (!this.dateStart) {
            const dateStart = this.dateEnd ? new Date(this.dateEnd) : new Date();
            dateStart.setMonth(dateStart.getMonth() - 3);
            this.dateStart = this.utilitiesService.parseDate(dateStart);
        }

    }

    selectTime(timeSelected: string) {
        this.timeSelected = timeSelected;
    }

    openDate(){

    }

}
