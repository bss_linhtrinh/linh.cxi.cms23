import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ActivityLogRoutingModule} from './activity-log-routing.module';
import {ListActivityLogComponent} from './list-activity-log/list-activity-log.component';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
    declarations: [ListActivityLogComponent],
    imports: [
        NgbModule,
        FormsModule,
        CommonModule,
        SharedModule,
        ActivityLogRoutingModule
    ]
})
export class ActivityLogModule {
}
