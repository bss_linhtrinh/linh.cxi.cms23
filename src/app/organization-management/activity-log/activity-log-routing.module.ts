import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CheckPermissionsGuard} from '../../shared/guards/check-permissions/check-permissions.guard';
import {ListActivityLogComponent} from './list-activity-log/list-activity-log.component';


const routes: Routes = [
    {
        path: '',
        canActivate: [CheckPermissionsGuard],
        component: ListActivityLogComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivityLogRoutingModule { }
