FROM node:10-alpine

WORKDIR /src/app
COPY ./package.json .
RUN npm install --quiet
RUN npm install -g @angular/cli

COPY . .

EXPOSE 4222

CMD ["ng", "serve", "--host", "0.0.0.0", "--disableHostCheck"]
